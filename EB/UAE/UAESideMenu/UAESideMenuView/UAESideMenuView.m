//
//  UAESideMenuView.m
//  EB
//
//  Created by webwerks on 8/17/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAESideMenuView.h"
#import "UAESideMenuCell.h"
#import "MyTreeNode.h"
#import "MyTreeViewCell.h"
#import "UAEUserClass.h"
#import "UAELoginSignUpViewController.h"
#import "UAEMyAccountViewController.h"
#import "UAEHomeViewController.h"

#define kHOME_TAG 1000

@interface UAESideMenuView () <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *arrayMenuNames;
    NSMutableArray *arrayMenuImages;
    MyTreeNode *treenode;
    NSMutableArray* arrSlideMenu1;
}
@end

@implementation UAESideMenuView

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self registerSideMenuCell];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(menuStateEventOccurred:)
                                                 name:MFSideMenuStateNotificationEvent
                                               object:nil];
    
}

- (void)menuStateEventOccurred:(NSNotification *)notification {
    MFSideMenuStateEvent event = [[[notification userInfo] objectForKey:@"eventType"] intValue];
    
    
    if(event == MFSideMenuStateEventMenuWillOpen)
    {
        [self setUpView];
        [_menuNamesTableView reloadData];
    }
}

-(void)setUpView{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:UAE_USER])
    {
        
        UAEUserClass *objUserClass = [UAEUserClass loadCustomObjectWithKey:UAE_USER];
        _labelName.text = objUserClass.store_name;
        _mobileNumberLabel.text = objUserClass.mobile;
        
        arrayMenuNames = [NSMutableArray arrayWithObjects:@"Setting",@"My Account",@"About Us",@"Contact Us",@"Logout", nil];
        
        arrayMenuImages = [NSMutableArray arrayWithObjects: @"blue_home_icon",@"blue_my_account_icon",@"blue_home_icon",@"blue_contact_us_icon",@"blue_logout_icon", nil];
        
    }
    else
    {
        arrayMenuNames = [NSMutableArray arrayWithObjects:@"Setting",@"My Account",@"About Us",@"Contact Us",@"SignUp",@"Login", nil];
        
        arrayMenuImages = [NSMutableArray arrayWithObjects: @"blue_home_icon",@"blue_my_account_icon",@"blue_home_icon",@"blue_contact_us_icon",@"blue_sign_up_icon",@"blue_login_icon",  nil];
        _labelName.text = @"Guest";
        _mobileNumberLabel.text = @"";
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setUpView];
    [self CallMenuListService];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerSideMenuCell
{
    [self.menuNamesTableView registerNib:[UINib nibWithNibName:@"UAESideMenuCell" bundle:nil] forCellReuseIdentifier:@"UAESideMenuCell"];
}

#pragma mark - TableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return (section==0)?[treenode descendantCount]:arrayMenuImages.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 40.0;
    return rowHeight;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (section==1)?1.0:0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, [self tableView:tableView heightForHeaderInSection:section])];
    
    if (section == 1) {
        view.frame = CGRectMake(5.0, 0.0, self.view.frame.size.width- 10, 1);
        [view setBackgroundColor:[[AppDelegate getAppDelegateObj] colorWithHexString:APP_THEME_COLOR]];
    }
    else{
        //check header height is valid
        if([self tableView:tableView heightForHeaderInSection:section] == 0.0)
        {
            //bail
            return nil;
        }
    }
    return view;
}


- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section == 0)
    {
        static NSString *CellIdentifier = @"Cell";
        MyTreeNode *node = [[treenode flattenElements] objectAtIndex:indexPath.row + 1];
        MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                     reuseIdentifier:CellIdentifier
                                                               level:[node levelDepth] - 1
                                                            expanded:node.inclusive];
        cell.lblItem.textColor = [[AppDelegate getAppDelegateObj]colorWithHexString:@"333333"];
        cell.lblItem.font =karlaFontRegular(14.0);
        
        if (indexPath.row == 0) {
            cell.lblItem.text =@"Home" ;
            cell.arrowImage.frame = CGRectMake(10, 10, 15, 15);
            cell.arrowImage.image = [UIImage imageNamed:@"blue_home_icon"];
            cell.arrowImage.contentMode = UIViewContentModeScaleAspectFit;
            cell.btnPlus.hidden = YES;
        }
        else{
            cell.lblItem.text =[NSString stringWithFormat:@"%@",[node.menuDataDict objectForKey:@"name"]] ;
            cell.arrowImage.frame = CGRectMake(10, 10, 15, 15);
            cell.arrowImage.contentMode = UIViewContentModeScaleAspectFit;
            if([node.menuDataDict objectForKey:@"sub_cat"])
            {
                cell.btnPlus.hidden = NO;
            }
            else
            {
                cell.btnPlus.hidden = YES;
                cell.arrowImage.image = [UIImage imageNamed:@"empty_circle"];
                cell.arrowImage.frame = CGRectMake(12, 10, 12, 12);
                cell.lblItem.font =karlaFontRegular(12.0);
            }
            
            if(node.inclusive)
            {
                [cell.btnPlus setBackgroundImage:[UIImage imageNamed:@"blue_up_arrow"] forState:UIControlStateNormal];
                
            }
            else
            {
                [cell.btnPlus setBackgroundImage:[UIImage imageNamed:@"blue_down_arrow"] forState:UIControlStateNormal];
            }
            
            if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Laptops"])
            {
                cell.arrowImage.image = [UIImage imageNamed:@"blue_laptop_icon"];
            }
            else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Mobiles"])
            {
                cell.arrowImage.image = [UIImage imageNamed:@"blue_mobile_icon"];
            }
            
            else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Accessories"])
            {
                cell.arrowImage.image = [UIImage imageNamed:@"blue_accessories_icon"];
            }
        }
        return cell;
    }
    
    else
    {
        UAESideMenuCell* cell = [self.menuNamesTableView dequeueReusableCellWithIdentifier:@"UAESideMenuCell"];
        cell.labelMenuName.text = [arrayMenuNames objectAtIndex:indexPath.row];
        cell.menuImageView.image = [UIImage imageNamed: [arrayMenuImages objectAtIndex:indexPath.row]];
        if(indexPath.row==0){
            cell.flagImageViewRef.hidden=NO;
        }
        else{
            cell.flagImageViewRef.hidden=YES;
        }
        return cell;
    }
    return nil;
}


#pragma mark -
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.menuNamesTableView deselectRowAtIndexPath:[self.menuNamesTableView indexPathForSelectedRow] animated:YES];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            // 1111
            //[self navigateToController:kHOME_TAG cat_val:nil];
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
        }
        else
        {
            MyTreeNode *node = [[treenode flattenElements] objectAtIndex:indexPath.row + 1];
            if (!node.hasChildren)
            {
                NSString *strTagVal = [NSString stringWithFormat:@"%@",[node.menuDataDict objectForKey:@"id"]];// sending cat_id for category screen
                
                NSString *strCatVal = [NSString stringWithFormat:@"%@",[node.menuDataDict objectForKey:@"name"]];// sending cat_id for category screen
                //[self navigateToController:[strTagVal intValue] cat_val:strCatVal];
                
                // [self.slideMenuDelegate SlidingMethod:[strTagVal intValue] cat_name:strCatVal];
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
                return;
            }
            node.inclusive = !node.inclusive;
            [treenode flattenElementsWithCacheRefresh:YES];
            [self.menuNamesTableView reloadData];
        }
    }
    else if (indexPath.section == 1)
    {
        UINavigationController *nav = self.menuContainerViewController.centerViewController;
        if (indexPath.row == 0)
        {
            if([[nav.viewControllers objectAtIndex:0] isKindOfClass:[UAEHomeViewController class]])
            {
                UAEHomeViewController *objHomeVc =(UAEHomeViewController *) [nav.viewControllers objectAtIndex:0];
                [objHomeVc countryFlagButtonClicked:nil];
            }
        }
        else if (indexPath.row == 1) {
            if ([defaults valueForKey:UAE_USER]){
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"UAEDesign" bundle:nil];
                UAEMyAccountViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"UAEMyAccountViewController"];
                [nav pushViewController:vc animated:YES];
            }
            else
                [UAEUtilityClass navigateToLoginSignUpView];
            
        }
        
        else if (indexPath.row == 2){
            
        }
        else if (indexPath.row == 3){
            
        }
        else if (indexPath.row == 4){
            if ([UAEUtilityClass checkUAEUserLogin]){
                //logout
                [defaults removeObjectForKey:UAE_USER];
            }
            else
                [UAEUtilityClass navigateToLoginSignUpView];
        }
        
        else if (indexPath.row == 5){
            [UAEUtilityClass navigateToLoginSignUpView];
            
        }
        
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
        
    }
    
}

#pragma mark - CallMenuListService

-(void)CallMenuListService
{
    //[FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:UAE_MENU_LIST_WS MathodName:UAE_MENU_LIST_WS];
        
    }
}



-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:UAE_MENU_LIST_WS]){
        //NSLog(@"%@",response);
        
        arrSlideMenu1 = [NSMutableArray new];
        arrSlideMenu1 = [response valueForKey:@"menu_item"];
        if (arrSlideMenu1.count>0) {
            [self drawTreeNodeView:arrSlideMenu1];
        }
        
        
    }
}
#pragma mark - drawTreeNodeView

-(void)drawTreeNodeView:(NSMutableArray *)treeviewArr
{
    treenode = [[MyTreeNode alloc] initWithValue:@"Root"];
    for(int i=0;i<treeviewArr.count+1;i++)
    {
        if (i == 0) {
            NSString *titleNode = [NSString stringWithFormat:@"Home"];
            MyTreeNode *node=[[MyTreeNode alloc]initWithValue:titleNode];
            node.value = [NSString stringWithFormat:@"%d",kHOME_TAG];
            [treenode addChild:node];
            node.inclusive=NO;
        }
        else
        {
            NSString *titleNode = [NSString stringWithFormat:@"%@",[[treeviewArr objectAtIndex:i-1]objectForKey:@"name"]];
            MyTreeNode *node=[[MyTreeNode alloc]initWithValue:titleNode];
            node.value = [[treeviewArr objectAtIndex:i-1]objectForKey:@"name"];
            node.menuDataDict = [treeviewArr objectAtIndex:i-1];
            NSMutableArray *subMenuArr=[NSMutableArray new];
            subMenuArr = [[treeviewArr objectAtIndex:i-1]objectForKey:@"sub_cat"];
            
            if (subMenuArr.count > 0)
            {
                for(int j=0;j<subMenuArr.count;j++)
                {
                    NSString *submenuChildtitle = [NSString stringWithFormat:@"%@",[[subMenuArr objectAtIndex:j] objectForKey:@"name"]];
                    MyTreeNode *childnode=[[MyTreeNode alloc]initWithValue:submenuChildtitle];
                    childnode.value = [NSString stringWithFormat:@"%@",[[subMenuArr objectAtIndex:j]objectForKey:@"name"]];
                    childnode.menuDataDict = [subMenuArr objectAtIndex:j];
                    [node addChild:childnode];
                }
            }
            [treenode addChild:node];
            node.inclusive=NO;
        }
    }
    [self.menuNamesTableView reloadData];
}


@end
