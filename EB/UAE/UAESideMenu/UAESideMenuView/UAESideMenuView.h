//
//  UAESideMenuView.h
//  EB
//
//  Created by webwerks on 8/17/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAESideMenuView : UIViewController<FinishLoadingData>
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;

@property (weak, nonatomic) IBOutlet UITableView *menuNamesTableView;

@end
