//
//  UAESideMenuCell.h
//  EB
//
//  Created by webwerks on 8/17/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAESideMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;
@property (weak, nonatomic) IBOutlet UILabel *labelMenuName;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageViewRef;

@end
