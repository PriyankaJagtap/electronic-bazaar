//
//  UAESignUpViewController.m
//  EB
//
//  Created by webwerks on 8/16/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAESignUpViewController.h"
#define MAX_LENGTH 10
#import "UAEHomeViewController.h"
#import "UAEUserClass.h"
#import "UAELoginSignUpViewController.h"

@interface UAESignUpViewController ()

@end

@implementation UAESignUpViewController
@synthesize storeNameTextField, mobileTextField, passwordTextField, confirmPasswordTextField, emailTextField, managerOneTextField, whatsAppNoOneTextField, managerTwoTextField, whatsAppTwoTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [CommonSettings addPaddingAndImageToTextField: storeNameTextField withImageName:@"gray_profile_icon"];
    [CommonSettings addPaddingAndImageToTextField: mobileTextField withImageName:@"gray_mobile_icon"];
    [CommonSettings addPaddingAndImageToTextField: passwordTextField withImageName:@"gray_pwd_icon"];
    [CommonSettings addPaddingAndImageToTextField: confirmPasswordTextField withImageName:@"gray_pwd_icon"];
    [CommonSettings addPaddingAndImageToTextField: emailTextField withImageName:@"gray_email_icon"];
    [CommonSettings addPaddingAndImageToTextField: managerOneTextField withImageName:@"gray_manager_icon"];
    [CommonSettings addPaddingAndImageToTextField: whatsAppNoOneTextField withImageName:@"gray_whatsapp_icon"];
    [CommonSettings addPaddingAndImageToTextField: managerTwoTextField withImageName:@"gray_manager_icon"];
    [CommonSettings addPaddingAndImageToTextField: whatsAppTwoTextField withImageName:@"gray_whatsapp_icon"];
    [self setRegisterButton];
    [CommonSettings setAsterixToTextField:storeNameTextField withPlaceHolderName:@"Store Name"];
    [CommonSettings setAsterixToTextField:mobileTextField withPlaceHolderName:@"Mobile"];
    [CommonSettings setAsterixToTextField:passwordTextField withPlaceHolderName:@"Password"];
    [CommonSettings setAsterixToTextField:confirmPasswordTextField withPlaceHolderName:@"Confirm Password"];
    [CommonSettings setAsterixToTextField:emailTextField withPlaceHolderName:@"Email"];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setRegisterButton
{
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                            NSFontAttributeName:[UIFont fontWithName:@"Karla-Regular" size:15.0],
                            NSForegroundColorAttributeName:[UIColor darkGrayColor],
                            NSParagraphStyleAttributeName:style}; // Added line
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Already Registered? Login" attributes:dict1]];
    [self.buttonAlreadyRegistered setAttributedTitle:attString forState:UIControlStateNormal];
}



#pragma mark - IBAction methods
- (IBAction)buttonSubmitAction:(id)sender
{
    NSLog(@"buttonSubmitAction");
    [self.view endEditing:NO];
    
    if (self.storeNameTextField.text.length == 0)
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter store name."];
        return;
    }
    
    
        if (self.mobileTextField.text.length == 0)
        {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Mobile Number"];
            return;
        }

    
    if (self.passwordTextField.text)
    {
        if (self.passwordTextField.text.length == 0)
        {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter password."];
            return;
        }
        if (self.confirmPasswordTextField.text.length == 0)
        {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter confirm password."];
            return;
        }
        if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])
        {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Confirm Password does not match"];
            return;
        }
    }
    
    
        if (self.emailTextField.text.length == 0)
        {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
            return;
        }
        if (![CommonSettings validEmail:self.emailTextField.text])
        {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
            return;
        }
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    // Call webservice
    NSMutableDictionary * dict = [NSMutableDictionary new];
    
    [dict setObject:self.storeNameTextField.text forKey:@"store_name"];
    [dict setObject:self.mobileTextField.text forKey:@"mobile"];
    [dict setObject:self.passwordTextField.text forKey:@"password"];
    [dict setObject:self.emailTextField.text forKey:@"email"];
    [dict setObject:self.managerOneTextField.text forKey:@"manager1"];
    [dict setObject:self.whatsAppNoOneTextField.text forKey:@"whatsapp1"];
    [dict setObject:self.managerTwoTextField.text forKey:@"manager2"];
    [dict setObject:self.whatsAppTwoTextField.text forKey:@"whatsapp2"];
    [dict setObject:[CommonSettings getAppCurrentVersion] forKey:@"version"];
    [dict setObject:@"iphone" forKey:@"device_type"];
    NSString *pushID=[defaults valueForKey:@"DEVICE_TOKEN"];
    [dict setObject:pushID?:@"" forKey:@"push_id"];
    
    NSLog(@"registeration dic %@",dict);
    [self callRegistrationWS:dict];
    
}
- (IBAction)buttonAlreadyRegisteredAction:(id)sender
{
    NSLog(@"buttonAlreadyRegisteredAction");
    UAELoginSignUpViewController *objVc = (UAELoginSignUpViewController*)self.parentViewController;
    [objVc LoginBtnClicked:objVc.buttonLogin];
}

#pragma mark - UAE RegisterWS
-(void) mobileValueChanged:(id)sender
{
    if(self.mobileTextField.text.length == 10)
    {
        [self checkMobileNumberExist];
    }
}
-(void)checkMobileNumberExist
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate = self;
        [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",CHECK_MOBILE_WS,self.mobileTextField.text] MathodName:CHECK_MOBILE_WS];
    }
}

-(void)checkEmailExist
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate = self;
        [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",CHECK_EMAIL_WS,self.emailTextField.text] MathodName:CHECK_EMAIL_WS];
    }
}

#pragma mark - RegisterWS

-(void)callRegistrationWS:(NSMutableDictionary*)userdata{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    [userdata setValue:[CommonSettings getAppCurrentVersion] forKey:@"version"];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:userdata url:UAE_REGISTRATION_WS string:UAE_REGISTRATION_WS];
    }
    
}

/*-(void)receivedResponseForRegistration:(id)receiveData stringResponse:(NSString *)responseType
 {
 [FVCustomAlertView hideAlertFromView:self.view fading:NO];
 
 }*/


#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:UAE_REGISTRATION_WS])
    {
        NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            UAEUserClass *objUserClass = [[UAEUserClass alloc] init];
            [objUserClass saveRegisteredUserData:response];
            [objUserClass saveCustomObject:objUserClass key:UAE_USER];
            
            [[NSUserDefaults standardUserDefaults] setValue:mobileTextField.text forKey:UAE_LOGIN_USER_MOBILE];
            [[NSUserDefaults standardUserDefaults] setValue:passwordTextField.text forKey:UAE_LOGIN_USER_PASSWORD];
            [[AppDelegate getAppDelegateObj] switchToUAEStoreApplication];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
        }
        
    }
    else if ([strName isEqualToString:CHECK_MOBILE_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            alert.tag = 500;
            [alert show];
        }
    }
    else if ([strName isEqualToString:CHECK_EMAIL_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
}

#pragma mark - UITextField delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //                if(textField == self.mobileTextField)
        //                {
        //                        if(textField.text.length < 10)
        //                        {
        //                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        //                                [alert show];
        //                        }
        //                }
        //                else
        
        if (textField == self.emailTextField)
        {
            if (self.emailTextField.text.length == 0)
            {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
                return;
            }
            if (![CommonSettings validEmail:self.emailTextField.text])
            {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
                return;
            }
            else
            {
                [self checkEmailExist];
            }
        }
    });
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.emailTextField)
    {
        return YES;
    }
    //        else if(textField == self.mobileTextField)
    //        {
    //                NSUInteger newLength = [textField.text length] + [string length];
    //                if(newLength > MAX_LENGTH)
    //                {
    //                        return NO;
    //                }
    //                else
    //                {
    //                        return YES;
    //                }
    //        }
    else
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 25;
    }
}

#pragma mark - Email / Phone Validation

-(BOOL) validatePhoneNumber:(NSString *)enterNumber
{
    NSString *phoneRegex = @"[12356789][0-9]{6}([0-9]{3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:enterNumber];
    return matches;
}
@end
