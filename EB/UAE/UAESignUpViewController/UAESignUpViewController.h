//
//  UAESignUpViewController.h
//  EB
//
//  Created by webwerks on 8/16/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAESignUpViewController : UIViewController<FinishLoadingData>
@property (weak, nonatomic) IBOutlet UITextField *storeNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *managerOneTextField;
@property (weak, nonatomic) IBOutlet UITextField *whatsAppNoOneTextField;
@property (weak, nonatomic) IBOutlet UITextField *managerTwoTextField;
@property (weak, nonatomic) IBOutlet UITextField *whatsAppTwoTextField;
@property (weak, nonatomic) IBOutlet UIButton *buttonAlreadyRegistered;

@end
