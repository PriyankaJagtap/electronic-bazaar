//
//  UAEChangePasswordViewController.m
//  EB
//
//  Created by webwerks on 22/09/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEChangePasswordViewController.h"
#import "UAEUserClass.h"

@interface UAEChangePasswordViewController ()<FinishLoadingData,UIGestureRecognizerDelegate>
{
    BOOL isKeyBoardDisplay;
}
@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *nwPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIView *subView;


@end

@implementation UAEChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tap.delegate = self;
    [_subView addGestureRecognizer:tap];
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    isKeyBoardDisplay = NO;
}

-(void)viewTapped:(UITapGestureRecognizer *)sender
{
   // [self.navigationController popViewControllerAnimated:NO];
    if(!isKeyBoardDisplay)
    {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - keyboard methods
- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    isKeyBoardDisplay = YES;
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    isKeyBoardDisplay = NO;
}

#pragma mark - tap gesture delegates
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return touch.view == gestureRecognizer.view;
}

#pragma mark - IBAction Method

- (IBAction)resetPasswordBtnClicked:(id)sender {
    
    [self.view endEditing:YES];
    if (![Validation required:_oldPasswordTextField withCaption:@"Old Password"])
        return;
    if (![Validation required:_nwPasswordTextField withCaption:@"New Password"])
    {
        return;
    }
    if (![Validation required:_confirmPasswordTextField withCaption:@"Confirm Password"])
        return;
    
    if (![_nwPasswordTextField.text isEqualToString:_confirmPasswordTextField.text])
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"New Password and Confirm Password does not match."];
        return;
    }
    
    [self callChangePasswordWeservice];
    
}

#pragma mark - call change pwd
-(void)callChangePasswordWeservice
{
    Webservice  *callRegionList = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callRegionList.delegate =self;
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
        UAEUserClass *objUAEUserClass = [UAEUserClass loadCustomObjectWithKey:UAE_USER];
        NSString *url = [NSString stringWithFormat:@"%@customerId=%@&new_password=%@&old_password=%@",UAE_CHANGE_PASSWORD_WS,objUAEUserClass.userId,_nwPasswordTextField.text,_oldPasswordTextField.text];
        [callRegionList GetWebServiceWithURL:url MathodName:UAE_CHANGE_PASSWORD_WS];
        
    }


}


-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:UAE_CHANGE_PASSWORD_WS]) {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _oldPasswordTextField.text=@"";
            _nwPasswordTextField.text=@"";
            _confirmPasswordTextField.text=@"";
        });
    }
}

@end
