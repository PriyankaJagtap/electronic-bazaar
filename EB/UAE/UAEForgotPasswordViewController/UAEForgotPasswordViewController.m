//
//  UAEForgotPasswordViewController.m
//  EB
//
//  Created by Neosoft on 9/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEForgotPasswordViewController.h"
#import <UIKit/UIKit.h>
@interface UAEForgotPasswordViewController ()<FinishLoadingData>

@end

@implementation UAEForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [CommonSettings addPaddingAndImageToTextField: _mobileTextField withImageName:@"gray_mobile_icon"];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [AppDelegate setCustomNavigationWithBackButton:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBAction method

- (IBAction)submitBtnClicked:(id)sender {
    [self.view endEditing:YES];
    
    if (self.mobileTextField.text.length == 0)
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide Mobile Number"];
        return;
    }
    [self callForgotPasswordWebservice];
}

#pragma mark - navigation button actions
-(void)cartButtonTapped{

}
-(void)backButtonTapped{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Forgot password webservice
-(void)callForgotPasswordWebservice
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        NSString *url = [NSString stringWithFormat:@"%@%@",UAE_FORGOT_PASSWORD_WS,_mobileTextField.text];
        [callLoginService GetWebServiceWithURL:url MathodName:UAE_FORGOT_PASSWORD_WS];
        
    }


}

#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:UAE_FORGOT_PASSWORD_WS]){
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            [UIView animateWithDuration:0.25 animations:^{
                _mobileTextField.hidden = YES;
                _submitBtn.hidden = YES;
                _titleLabel.text = [response valueForKey:@"message"];
            }];
        }
        else
        {
            [UIView animateWithDuration:0.25 animations:^{
                _titleLabel.text = [response valueForKey:@"message"];
            }];
        }
    }
}
@end
