//
//  UAEProductSplashCell.h
//  EB
//
//  Created by webwerks on 22/08/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEProductSplashCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *productImageViewRef;

@end
