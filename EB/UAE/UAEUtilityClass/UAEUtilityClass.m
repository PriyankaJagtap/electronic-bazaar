//
//  UAEUtilityClass.m
//  EB
//
//  Created by Neosoft on 9/14/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEUtilityClass.h"
#import "UAELoginSignUpViewController.h"

@implementation UAEUtilityClass

+(BOOL)checkUAEUserLogin
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults valueForKey:UAE_USER])
        return YES;
    else
        return FALSE;
}


+(BOOL)checkUAEStore
{
    if(([[[NSUserDefaults standardUserDefaults] objectForKey:SELECTED_STORE] isEqualToString:UAE_STROE])){
        return YES;
    }
    else{
        return NO;
    }
}

+(void)navigateToLoginSignUpView
{
    UAELoginSignUpViewController *vc = [[UAELoginSignUpViewController alloc] initWithNibName:@"UAELoginSignUpViewController" bundle:nil];
    UINavigationController *navigationController=[[UINavigationController alloc]initWithRootViewController:vc];
    [APP_DELEGATE.window setRootViewController:navigationController];
    [APP_DELEGATE.window makeKeyAndVisible];
}
@end
