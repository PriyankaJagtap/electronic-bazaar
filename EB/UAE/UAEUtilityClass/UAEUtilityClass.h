//
//  UAEUtilityClass.h
//  EB
//
//  Created by Neosoft on 9/14/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UAEUtilityClass : NSObject
+(BOOL)checkUAEUserLogin;
+(BOOL)checkUAEStore;
+(void)navigateToLoginSignUpView;
@end
