//
//  UAEMyAccountViewController.h
//  EB
//
//  Created by webwerks on 21/09/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEMyAccountViewController : UIViewController
{
    NSDictionary *dashboardResponse;
}

@property (weak, nonatomic) IBOutlet UITableView *tblView;
- (IBAction)backAction:(id)sender;
- (IBAction)rightMenuAction:(id)sender;
@end
