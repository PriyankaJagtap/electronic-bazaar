//
//  UAEMyAccountViewController.m
//  EB
//
//  Created by webwerks on 21/09/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEMyAccountViewController.h"
#import "AccountHeaderTableCell.h"
#import "EditAddressViewController.h"
#import "MyPurchasesViewController.h"
#import "MyAddressViewController.h"
#import "AccountInfoTableCell.h"
#import "AccountHeaderTableCell.h"
#import "AddAddressTableCell.h"
#import "AccountOrderCell.h"
#import "AddressCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UAEUserClass.h"
#import "UAEEditProfileViewController.h"
#import "UAEChangePasswordViewController.h"

@interface UAEMyAccountViewController ()<FinishLoadingData>
{
    NSMutableArray *sectionTitleArr;
    NSMutableArray *addressListArr;
    
}
@end

@implementation UAEMyAccountViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    sectionTitleArr = [NSMutableArray arrayWithObjects:@"Account Information",@"Recent Orders",@"My Address", nil];
    _tblView.estimatedRowHeight = 200;
    _tblView.rowHeight = UITableViewAutomaticDimension;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [AppDelegate setCustomNavigationWithBackButton:self];
    [self getMyAccountFromWs];
    
    
    // add gesture to hide keyboard
    UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
    tapScroll.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapScroll];
}


#pragma mark - UITextField Delegate

-(void)hideKeyBoard:(UIGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}


#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (dashboardResponse == nil) {
        return 0;
    }
    return 1;
    //return sectionTitleArr.count;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *CellIdentifier = @"AccountHeaderTableCell";
    AccountHeaderTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AccountHeaderTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //NSLog(@"%@",dashboardResponse);
    
    cell.titleLabel.text = [sectionTitleArr objectAtIndex:section];
    if(section != 0)
    {
        
        cell.viewAllBtn.tag=section;
        [cell.viewAllBtn addTarget:self action:@selector(seeMoreButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.viewAllBtn setTitle:@"View All" forState:UIControlStateNormal];
    }
    else
    {
        [cell.viewAllBtn setTitle:@"Edit" forState:UIControlStateNormal];
        [cell.viewAllBtn addTarget:self action:@selector(editProfileAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell.contentView;
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        //return 209;
        return UITableViewAutomaticDimension;
    }else if(indexPath.section==1)
    {
        return [[dashboardResponse valueForKey:@"recent_orders"]count]*36+36;
    }
    else if (indexPath.section ==2 && indexPath.row == 0)
    {
        
        return 55;
    }
    else if (indexPath.section ==2 && indexPath.row == 1)
    {
        return 47;
    }
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 40.0;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 2)
    {
        if(addressListArr.count == 0)
            return 2;
        else
            return addressListArr.count +1;
    }
    if (dashboardResponse == nil) {
        return 0;
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if(indexPath.section == 0)
    {
        static NSString *CellIdentifier = @"AccountInfoTableCell";
        AccountInfoTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[AccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        [cell.chngPwdBtn addTarget:self action:@selector(navigateToChangePassword) forControlEvents:UIControlEventTouchUpInside];
        
        cell.storeNameTxtLbl.text = [dashboardResponse valueForKey:@"store_name"];
        cell.mobileNoTxtLbl.text = [dashboardResponse valueForKey:@"mobile"];
        cell.emailIdTxtLbl.text = [dashboardResponse valueForKey:@"email"];

        CommonSettings *objCommonSetting = [CommonSettings sharedInstance];
//        if (![objCommonSetting checkNullValue:[dashboardResponse valueForKey:@"email"]]) {
//            cell.emailIdTxtLbl.text = [dashboardResponse valueForKey:@"email"];
//            cell.emailIdLbl.text = @"Email :";
//        }
//        else
//        {
//            cell.emailIdLbl.text = @"";
//            cell.emailIdTxtLbl.text = @"";
//        }
       
        if ([dashboardResponse valueForKey:@"manager1"] && ![objCommonSetting checkNullValue:[dashboardResponse valueForKey:@"manager1"]]) {
            cell.manager1TxtLabel.text = [dashboardResponse valueForKey:@"manager1"];
            cell.manager1Label.text = @"Manager 1 :";
        }
        else
        {
            cell.manager1TxtLabel.text = @"";
            cell.manager1Label.text = @"";
        }
        
        if ([dashboardResponse valueForKey:@"whatsapp1"] && ![objCommonSetting checkNullValue:[dashboardResponse valueForKey:@"whatsapp1"]]) {
            cell.whatsapp1TxtLabel.text = [dashboardResponse valueForKey:@"whatsapp1"];
            cell.whatsApp1Label.text = @"WhatsApp No. 1 :";
        }
        else
        {
            cell.whatsApp1Label.text = @"";
            cell.whatsapp1TxtLabel.text = @"";
        }
        
        if ([dashboardResponse valueForKey:@"manager2"] && ![objCommonSetting checkNullValue:[dashboardResponse valueForKey:@"manager2"]]) {
            cell.manager2TxtLabel.text = [dashboardResponse valueForKey:@"manager2"];
            cell.manager2Label.text = @"Manager 2 :";

        }
        else
        {
            cell.manager2Label.text = @"";
            cell.manager2TxtLabel.text = @"";
        }
        
        
        if ([dashboardResponse valueForKey:@"whatsapp2"] && ![objCommonSetting checkNullValue:[dashboardResponse valueForKey:@"whatsapp2"]]) {
            cell.whatsApp2TxtLabel.text = [dashboardResponse valueForKey:@"whatsapp2"];
            cell.whatsApp2Label.text = @"WhatsApp No. 2 :";

        }
        else
        {
            cell.whatsApp2TxtLabel.text = @"";
            cell.whatsApp2Label.text = @"";
        }

        return cell;
    }
    
    else if(indexPath.section == 1)
    {
        static NSString *CellIdentifier = @"AccountOrderCell";
        AccountOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[AccountOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.orderDetailArray=[dashboardResponse valueForKey:@"recent_orders"];
        [cell setUpOrderDetailTable];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    else if (indexPath.section == 2){
        
        NSInteger totalRow = [tableView numberOfRowsInSection:indexPath.section];//first get total rows in that section by current indexPath.
        if(indexPath.row == totalRow -1){
            static NSString *CellIdentifier = @"AddAddressTableCell";
            AddAddressTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[AddAddressTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            [cell.addAddressBtn addTarget:self action:@selector(addNewAddress) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        else
            
        {
            static NSString *CellIdentifier = @"AddressCell";
            AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[AddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            
            if(addressListArr.count == 0)
            {
                NSString *strBillingAdd=[NSString stringWithFormat:@"%@ %@ %@-%@",[[dashboardResponse valueForKey:@"default_billing_address"] valueForKey:@"street"]?:@"",[[dashboardResponse valueForKey:@"default_billing_address"] valueForKey:@"city"]?:@"",[[dashboardResponse valueForKey:@"default_billing_address"] valueForKey:@"region"]?:@"",[[dashboardResponse valueForKey:@"default_billing_address"] valueForKey:@"postcode"]?:@""];
                cell.billing_Add_Lbl.text=strBillingAdd;
            }
            else
            {
                cell.billing_Add_Lbl.text = [[addressListArr objectAtIndex:indexPath.row] valueForKey:@"complete_address"];
            }
            
            
            
            //            if(indexPath.row == totalRow -2)
            //            {
            //                [[CommonSettings sharedInstance] setCornerRadiusToBottomLeftAndBottomRight:cell.contentView];
            //            }
            //            else if(indexPath.row == 0)
            //            {
            //                 [[CommonSettings sharedInstance] setCornerRadiusToTopLeftAndTopRight:cell.contentView];
            //            }
            
            return cell;
            
        }
    }
    
    return Nil;
}
-(void)addNewAddress
{
    
    EditAddressViewController *editAddressObjc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditAddressViewController"];
    [self.navigationController pushViewController:editAddressObjc animated:YES];
    
}
-(void)navigateToChangePassword
{
    UAEChangePasswordViewController *popUpController=[[UAEChangePasswordViewController alloc] initWithNibName:@"UAEChangePasswordViewController" bundle:nil];
    [self.navigationController addChildViewController:popUpController];
    popUpController.view.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    [self.navigationController.view addSubview:popUpController.view];
    [popUpController didMoveToParentViewController:self];
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^
     {
         popUpController.view.alpha = 1;
     }
                     completion:nil];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - get address list

-(void)callAddressListWS{
    Webservice  *callAddlistWs = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callAddlistWs.delegate =self;
        int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
        
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
        [callAddlistWs webServiceWitParameters:[NSString stringWithFormat:@"userid=%d",userId] andURL:GET_CUSTOMER_ADDRESS_LIST_WS MathodName:@"ADDRESS_LIST"];
        //[callAddlistWs operationRequestToApi:dict url:DASHBOARD_WS string:@"Dashboard"];
        
    }
}
#pragma mark - navigation button actions
-(void)cartButtonTapped{
    
}
-(void)backButtonTapped{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IBAction methods

- (IBAction)editProfileAction:(id)sender {
    UAEEditProfileViewController *vc = [[UAEEditProfileViewController alloc] initWithNibName:@"UAEEditProfileViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)rightMenuAction:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
    
}

- (IBAction)changePhotoAction:(id)sender {
}

-(void)seeMoreButtonPressed:(UIButton *)btn{
    switch (btn.tag) {
        case 1:
        {
            MyPurchasesViewController *objMyPurchaseVC ;
            if(checkIsIpad)
                objMyPurchaseVC = [[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"MyPurchasesViewController"];
            else
                objMyPurchaseVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyPurchasesViewController"];
            
            [self.navigationController pushViewController:objMyPurchaseVC animated:YES];
        }
            break;
        case 2:
        {
            
            
            MyAddressViewController *objMyAddressVC;
            if(checkIsIpad)
                objMyAddressVC = [[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"MyAddressViewController"];
            else
                objMyAddressVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyAddressViewController"];
            
            [self.navigationController pushViewController:objMyAddressVC animated:YES];
        }
            break;
        default:
            break;
    }
}

-(void)getMyAccountFromWs
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@""withBlur:NO allowTap:NO];
    Webservice  *callMyAccountWS = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callMyAccountWS.delegate =self;
        UAEUserClass *objUserClass = [UAEUserClass loadCustomObjectWithKey:UAE_USER];
        NSString *url = [NSString stringWithFormat:@"%@%@",UAE_MY_ACCOUNT_WS,objUserClass.userId];
        [callMyAccountWS GetWebServiceWithURL:url MathodName:UAE_MY_ACCOUNT_WS];

    }
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:UAE_MY_ACCOUNT_WS])
    {
        dashboardResponse=[[response valueForKey:@"data"] valueForKey:@"customer"];
        [self.tblView reloadData];
    }
    
}


@end
