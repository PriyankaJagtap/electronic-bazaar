//
//  UAEProductListingViewController.h
//  EB
//
//  Created by webwerks on 8/17/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEProductListingViewController : UIViewController 
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *productListingTableView;

@end
