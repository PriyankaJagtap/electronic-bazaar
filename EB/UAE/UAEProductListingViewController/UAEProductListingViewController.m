//
//  UAEProductListingViewController.m
//  EB
//
//  Created by webwerks on 8/17/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEProductListingViewController.h"
#import "UAEProductListingCell.h"

@interface UAEProductListingViewController () <UITableViewDelegate, UITableViewDataSource>
{
        NSMutableArray* arrayProductList;
}
@end

@implementation UAEProductListingViewController

- (void)viewDidLoad
{
        [super viewDidLoad];
        // Do any additional setup after loading the view from its nib.
        [self registerProductListingCell];
}

- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

-(void)registerProductListingCell
{
    [self.productListingTableView registerNib:[UINib nibWithNibName:@"UAEProductListingCell" bundle:nil] forCellReuseIdentifier:@"UAEProductListingCell"];
    self.productListingTableView.estimatedRowHeight = 110;
    self.productListingTableView.rowHeight = UITableViewAutomaticDimension;
}

#pragma mark - TableView methods
- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
        return 5;
        //return [arrayProductList count];
}


- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
        UAEProductListingCell* cell = [self.productListingTableView dequeueReusableCellWithIdentifier:@"UAEProductListingCell"];
        
        cell.labelTitle.text = @"iPhone 8";
        cell.labelSubTitle.text = @"256 Gb (12)";
        return cell;
}


@end
