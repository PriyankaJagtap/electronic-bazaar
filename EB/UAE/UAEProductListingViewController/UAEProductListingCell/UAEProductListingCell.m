//
//  UAEProductListingCell.m
//  EB
//
//  Created by webwerks on 8/17/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEProductListingCell.h"

@implementation UAEProductListingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
        self.imageView.layer.masksToBounds = YES;
    // Configure the view for the selected state
}

@end
