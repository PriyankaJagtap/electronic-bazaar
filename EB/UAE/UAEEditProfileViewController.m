//
//  UAEEditProfileViewController.m
//  EB
//
//  Created by webwerks on 22/09/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEEditProfileViewController.h"
#import "UAEUserClass.h"

@interface UAEEditProfileViewController ()<FinishLoadingData,UITextFieldDelegate>
{
    UAEUserClass *objUAEUserClass;
}
@end

@implementation UAEEditProfileViewController
@synthesize storeNameTextField, mobileTextField, emailTextField, managerOneTextField, whatsAppNoOneTextField, managerTwoTextField, whatsAppTwoTextField;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    [AppDelegate setCustomNavigationWithBackButton:self];
   self.navigationItem.title = @"Edit Profile";
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView{
    [CommonSettings addPaddingAndImageToTextField: storeNameTextField withImageName:@"gray_profile_icon"];
    [CommonSettings addPaddingAndImageToTextField: mobileTextField withImageName:@"gray_mobile_icon"];
   
    [CommonSettings addPaddingAndImageToTextField: emailTextField withImageName:@"gray_email_icon"];
    [CommonSettings addPaddingAndImageToTextField: managerOneTextField withImageName:@"gray_manager_icon"];
    [CommonSettings addPaddingAndImageToTextField: whatsAppNoOneTextField withImageName:@"gray_whatsapp_icon"];
    [CommonSettings addPaddingAndImageToTextField: managerTwoTextField withImageName:@"gray_manager_icon"];
    [CommonSettings addPaddingAndImageToTextField: whatsAppTwoTextField withImageName:@"gray_whatsapp_icon"];
    
    [CommonSettings setAsterixToTextField:storeNameTextField withPlaceHolderName:@"Store Name"];
    [CommonSettings setAsterixToTextField:mobileTextField withPlaceHolderName:@"Mobile"];
  
    [CommonSettings setAsterixToTextField:emailTextField withPlaceHolderName:@"Email"];
    
    objUAEUserClass = [UAEUserClass loadCustomObjectWithKey:UAE_USER];
    if(objUAEUserClass)
    {
        storeNameTextField.text = objUAEUserClass.store_name;
        mobileTextField.text = objUAEUserClass.mobile;
        emailTextField.text = objUAEUserClass.email;
        managerOneTextField.text = objUAEUserClass.manager1;
        whatsAppNoOneTextField.text = objUAEUserClass.whatsapp1;
        managerTwoTextField.text = objUAEUserClass.manager2;
        whatsAppTwoTextField.text = objUAEUserClass.whatsapp2;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UITextField delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //                if(textField == self.mobileTextField)
        //                {
        //                        if(textField.text.length < 10)
        //                        {
        //                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        //                                [alert show];
        //                        }
        //                }
        //                else
        
        if (textField == self.emailTextField)
        {
            if (self.emailTextField.text.length == 0)
            {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
                return;
            }
            if (![CommonSettings validEmail:self.emailTextField.text])
            {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
                return;
            }
//            else
//            {
//                [self checkEmailExist];
//            }
        }
    });
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.emailTextField)
    {
        return YES;
    }
    //        else if(textField == self.mobileTextField)
    //        {
    //                NSUInteger newLength = [textField.text length] + [string length];
    //                if(newLength > MAX_LENGTH)
    //                {
    //                        return NO;
    //                }
    //                else
    //                {
    //                        return YES;
    //                }
    //        }
    else
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 25;
    }
}


#pragma mark - navigation button actions
-(void)cartButtonTapped{
    
}
-(void)backButtonTapped{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IBAction methods

- (IBAction)saveBtnClicked:(id)sender {
    
    [self.view endEditing:NO];
    
    if (self.storeNameTextField.text.length == 0)
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter store name."];
        return;
    }

    if (self.mobileTextField.text.length == 0)
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Mobile Number"];
        return;
    }
    
    
    if (self.emailTextField.text.length == 0)
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
        return;
    }
    if (![CommonSettings validEmail:self.emailTextField.text])
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
        return;
    }
    
    [self callEditProfileWebservice];
}

#pragma mark - call edit profile webservice
-(void)callEditProfileWebservice{
       [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@""withBlur:NO allowTap:NO];
    Webservice  *callMyAccountWS = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callMyAccountWS.delegate =self;
        NSString *url = [NSString stringWithFormat:@"%@store_name=%@&whatsapp2=%@&whatsapp1=%@&manager1=%@&mobile=%@&email=%@&manager2=%@&userid=%@",UAE_EDIT_PROFILE_WS,storeNameTextField.text,whatsAppTwoTextField.text,whatsAppNoOneTextField.text,managerOneTextField.text,mobileTextField.text,emailTextField.text,managerTwoTextField.text,objUAEUserClass.userId];
        [callMyAccountWS GetWebServiceWithURL:url MathodName:UAE_EDIT_PROFILE_WS];
        
    }
}

#pragma mark - handle response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:UAE_EDIT_PROFILE_WS])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
    
}
@end
