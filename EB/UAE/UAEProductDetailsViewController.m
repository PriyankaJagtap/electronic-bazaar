//
//  UAEProductDetailsViewController.m
//  EB
//
//  Created by webwerks on 8/17/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEProductDetailsViewController.h"
#import "UAEProductSizeCell.h"
#import "UAEProductColorCell.h"
#import "UAEProductSplashCell.h"

#import "Constant.h"

@interface UAEProductDetailsViewController () <UICollectionViewDelegate, UICollectionViewDataSource>
{
        NSMutableArray *arrayProductSizes;
        NSMutableArray *arrayProductImages;
        BOOL isCellSelected;
    __weak IBOutlet UICollectionView *productCoverCollectionViewRef;
}
@end

@implementation UAEProductDetailsViewController

- (void)viewDidLoad
{
        [super viewDidLoad];
        // Do any additional setup after loading the view from its nib.
        [self setGradePolicyButton];
        [self registerCollectionCell];
        arrayProductSizes = [NSMutableArray arrayWithObjects:@"64 Gb",@"128 Gb",@"256 Gb", nil];
        arrayProductImages = [NSMutableArray arrayWithObjects:@"iPhoneImage",@"iPhoneImage", @"iPhoneImage",@"iPhoneImage",@"iPhoneImage", nil];
        [self initializeView];
        [AppDelegate setCustomNavigation:self];
        [self configureProductCoverImageCollectionView];
    
       // [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
}

-(void)configureProductCoverImageCollectionView{
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    layout.itemSize=CGSizeMake([[UIScreen mainScreen] bounds].size.width, 150.0);
    layout.minimumInteritemSpacing=0;
    layout.minimumLineSpacing=0;
    productCoverCollectionViewRef.collectionViewLayout=layout;
}



-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int pageNumber = scrollView.contentOffset.x/scrollView.frame.size.width;
    self.pageControl.currentPage = pageNumber;
}


-(void)setScrollViewImages
{
        int countOfImages = 4;
        self.imageScrollView.pagingEnabled = YES;
        for (int i=0; i<countOfImages; i++)
        {
                UIImageView *imgView =[[UIImageView alloc] initWithFrame:CGRectMake(i*self.imageScrollView.frame.size.width, 0, self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height)];
               // imgView.image=[UIImage imageNamed:[NSString stringWithFormat:@"%d",i]];
                //imgView.tag=i+1;
            if(i==0){
                imgView.backgroundColor=[UIColor redColor];
            }
            if(i==1){
                imgView.backgroundColor=[UIColor greenColor];
            }
            if(i==2){
                imgView.backgroundColor=[UIColor blueColor];
            }
            if(i==3){
                imgView.backgroundColor=[UIColor whiteColor];
            }
                [self.imageScrollView addSubview:imgView];
        }
        [self.imageScrollView setContentSize:CGSizeMake(countOfImages*self.view.frame.size.width, self.imageScrollView.frame.size.height)];
    

}

- (void)scrollingTimer
{
        // access the scroll view with the tag
        // same way, access pagecontroll access
//        UIPageControl *pgCtr = (UIPageControl *) [self.view viewWithTag:12];
        self.pageControl.numberOfPages = 4;
        
        // get the current offset ( which page is being displayed )
        CGFloat contentOffset = self.imageScrollView.contentOffset.x;
        // calculate next page to display
        int nextPage = (int)(contentOffset/self.imageScrollView.frame.size.width) + 1 ;
        // if page is not 3, display it
        if( nextPage!=4 )
        {
                [self.imageScrollView scrollRectToVisible:CGRectMake(nextPage*self.imageScrollView.frame.size.width, 0, self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height) animated:YES];
                self.pageControl.currentPage=nextPage;
        }
        else
        {
                [self.imageScrollView scrollRectToVisible:CGRectMake(0, 0, self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height) animated:YES];
                self.pageControl.currentPage = 0;
        }
}

- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

#pragma mark - Other methods
-(void)registerCollectionCell
{
        [self.sizeCollectionView registerNib:[UINib nibWithNibName:@"UAEProductSizeCell" bundle:nil] forCellWithReuseIdentifier:@"UAEProductSizeCell"];
        
        [self.colorCollectionView registerNib:[UINib nibWithNibName:@"UAEProductColorCell" bundle:nil] forCellWithReuseIdentifier:@"UAEProductColorCell"];
    
         [productCoverCollectionViewRef registerNib:[UINib nibWithNibName:@"UAEProductSplashCell" bundle:nil] forCellWithReuseIdentifier:@"UAEProductSplashCell"];
    
}

-(void)initializeView
{
        self.labelTitle.text = @"Apple iPhone 7 (128 Gb, Black)";
        self.labelNewPrice.text = @"AED 58,990";
        self.labelOldPrice.text = @"AED 61,499";
        self.labelProductDescription.text = @"Lorem ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.";
        self.labelSize.text = @"128 Gb";
        self.labelColor.text = @"Black";
        self.labelGrade.text = @"Grade A";
        
        self.specificationLable1.text = @"● RAM : 2Gb";
        self.specificationLable2.text = @"● ROM : 16Gb";
        self.specificationLable3.text = @"● Camera : 1.9 Megapixel";
        self.specificationLable4.text = @"● Battery : 3100mAh";
}
-(void) setGradePolicyButton
{
        NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        [style setAlignment:NSTextAlignmentCenter];
        [style setLineBreakMode:NSLineBreakByWordWrapping];
        
        NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                                NSFontAttributeName:[UIFont fontWithName:@"Karla-Regular" size:13.0],
                                NSParagraphStyleAttributeName:style}; // Added line
        
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
        [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Click here to know Grading Policy" attributes:dict1]];
        [self.buttonGradingPolicy setAttributedTitle:attString forState:UIControlStateNormal];
}

#pragma mark - IBAction methods
- (IBAction)buttonBuyNowAction:(id)sender
{
        NSLog(@"buttonBuyNowAction");
}

- (IBAction)buttonGradeAction:(id)sender {
        NSLog(@"buttonGradeAction");
}

- (IBAction)buttonGradingPolicyAction:(id)sender
{
        NSLog(@"buttonGradingPolicyAction");
}

- (IBAction)buttonCheckAction:(id)sender
{
        NSLog(@"buttonCheckAction");
}

- (IBAction)buttonShareAction:(id)sender
{
        NSLog(@"buttonShareAction");
}

- (IBAction)buttonCallAction:(id)sender
{
        NSLog(@"buttonCallAction");
}

- (IBAction)buttonViewMoreAction:(id)sender
{
        NSLog(@"buttonViewMoreAction");
}

#pragma mark - UICollectionView methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
        if (collectionView == self.sizeCollectionView)
        {
                return [arrayProductSizes count];
        }
        else if(collectionView==productCoverCollectionViewRef){
                return 4;
        }
        else
        {
                return [arrayProductImages count];
        }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
        if (collectionView == self.sizeCollectionView)
        {
                UAEProductSizeCell *sizeCell = [self.sizeCollectionView dequeueReusableCellWithReuseIdentifier:@"UAEProductSizeCell" forIndexPath:indexPath];
                sizeCell.labelTitle.text = [arrayProductSizes objectAtIndex:indexPath.row];
                if ([sizeCell.labelTitle.text isEqualToString: self.labelSize.text])
                {
                        isCellSelected = YES;
                }
                else
                {
                        isCellSelected = NO;
                }
                if (isCellSelected)
                {
                        [self setupCellSelected: sizeCell];
                }
                else
                {
                        [self setupCellDeselect: sizeCell];
                }
                return sizeCell;
        }
    
        else if(collectionView==productCoverCollectionViewRef){
            UAEProductSplashCell *productCoverCell = [productCoverCollectionViewRef dequeueReusableCellWithReuseIdentifier:@"UAEProductSplashCell" forIndexPath:indexPath];
            productCoverCell.productImageViewRef.image = [UIImage imageNamed: [NSString stringWithFormat:@"%d",1] ];
            return productCoverCell;
        }
        else
        {
                 UAEProductColorCell *colorCell = [self.colorCollectionView dequeueReusableCellWithReuseIdentifier:@"UAEProductColorCell" forIndexPath:indexPath];
                colorCell.colorImageView.image = [UIImage imageNamed: [arrayProductImages objectAtIndex:indexPath.row] ];
                
                if (colorCell.selected)
                {
                        [self setupCellSelected: colorCell];
                }
                else
                {
                        [self setupCellDeselect:colorCell];
                }
                return colorCell;
        }
        return nil;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        if (collectionView == self.sizeCollectionView)
        {
                UAEProductSizeCell *sizeCell = (UAEProductSizeCell *) [self.sizeCollectionView cellForItemAtIndexPath:indexPath];
                [self setupCellSelected: sizeCell];
        }
        else
        {
                UAEProductColorCell *colorCell =  (UAEProductColorCell *) [self.colorCollectionView cellForItemAtIndexPath:indexPath];
                 [self setupCellSelected: colorCell];
        }
}

//respond to deselected cell
-(void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
        if (collectionView == self.sizeCollectionView)
        {
                UAEProductSizeCell *sizeCell = (UAEProductSizeCell *) [self.sizeCollectionView cellForItemAtIndexPath:indexPath];
                [self setupCellDeselect:sizeCell];
        }
        else
        {
                UAEProductColorCell *colorCell =  (UAEProductColorCell *) [self.colorCollectionView cellForItemAtIndexPath:indexPath];
                [self setupCellDeselect:colorCell];
        }
}

-(void)setupCellSelected: (UICollectionViewCell *)cell
{
        cell.layer.borderWidth=1.0f;
        cell.layer.cornerRadius = 5;
        cell.layer.borderColor = UIColorFromRGB(0x205B9A).CGColor;
}

-(void)setupCellDeselect: (UICollectionViewCell *)cell
{
        cell.layer.borderWidth= 1.0f;
        cell.layer.cornerRadius = 5;
        cell.layer.borderColor=[UIColor lightGrayColor].CGColor;
}

@end
