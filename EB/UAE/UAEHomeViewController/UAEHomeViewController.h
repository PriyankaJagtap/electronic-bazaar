//
//  UAEHomeViewController.h
//  EB
//
//  Created by webwerks on 21/08/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAEChooseCountryViewController.h"

@interface UAEHomeViewController : UIViewController<StorePopUpDelegate>

- (IBAction)countryFlagButtonClicked:(id)sender;

@end
