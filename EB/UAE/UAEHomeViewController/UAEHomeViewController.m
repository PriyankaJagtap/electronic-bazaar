//
//  UAEHomeViewController.m
//  EB
//
//  Created by webwerks on 21/08/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEHomeViewController.h"
#import "AppDelegate.h"
#import "UAEChooseCountryViewController.h"
#import "UAECheckoutViewController.h"
#import "UAELoginSignUpViewController.h"
#import "UAEHomeSlidingBannersTableCell.h"
#import "UAEHomeBannerTableCell.h"
#import "UAEForgotPasswordViewController.h"
#import "UAEProductDetailsViewController.h"
#import "UAEBrandViewController.h"

#define ASPECT_RATIO_SLIDER_IMG 1.7684

@interface UAEHomeViewController ()<UITableViewDataSource,UITableViewDelegate,FinishLoadingData>
{
    UIView *containBannerview;
    NSMutableArray *sliderImagesArr;
    NSTimer *autoScrollTimer;
    NSArray *bannerArr;
    NSArray *categoryBannerArr;
    int scrollBannerAfter;
    
}
@property (weak, nonatomic) IBOutlet UIView *searchBarContainerViewRef;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBarRef;
@property (weak, nonatomic) IBOutlet UIButton *flagBtnRef;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintSearchBar;

@property (weak, nonatomic) IBOutlet UITableView *homeTableView;

@end

@implementation UAEHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchBarRef.backgroundImage = [[UIImage alloc] init];
    [AppDelegate setCustomNavigation:self];
    _homeTableView.estimatedRowHeight = 100;
    _homeTableView.rowHeight = UITableViewAutomaticDimension;
    [self.homeTableView registerNib:[UINib nibWithNibName:@"UAEHomeSlidingBannersTableCell" bundle:nil] forCellReuseIdentifier:@"UAEHomeSlidingBannersTableCell"];
    [self.homeTableView registerNib:[UINib nibWithNibName:@"UAEHomeBannerTableCell" bundle:nil] forCellReuseIdentifier:@"UAEHomeBannerTableCell"];
    scrollBannerAfter = 0;
    [self callGetAppVersionWebservice];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if ([autoScrollTimer isValid]) {
        [autoScrollTimer invalidate];
    }
    autoScrollTimer = nil;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self scrollBanners];
    
}
-(void)scrollBanners
{
    if(autoScrollTimer == nil && bannerArr.count > 1)
    {
        autoScrollTimer = [NSTimer scheduledTimerWithTimeInterval:scrollBannerAfter target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
    }
}
-(void)onTimer
{
    UAEHomeSlidingBannersTableCell *cell = [_homeTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (cell) {
        NSArray *visibleItems = [cell.objCollecionView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
        NSInteger numberOfCells = [cell.objCollecionView numberOfItemsInSection:0];
        NSIndexPath *nextItem;
        if(currentItem.item + 1 == numberOfCells)
        {
            nextItem = [NSIndexPath indexPathForItem:0 inSection:currentItem.section];
            [cell.objCollecionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        }
        else
        {
            nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
            [cell.objCollecionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
        cell.objPageControl.currentPage = nextItem.item;

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - TableView methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    
//    static NSString *simpleTableIdentifier = @"UAEHomeSlidingBannersTableCell";
//    UAEHomeSlidingBannersTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//    if (cell == nil) {
//        cell = [[UAEHomeSlidingBannersTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
//    }
//    
//    [cell.objCollecionView registerNib:[UINib nibWithNibName:@"UAEHomeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"UAEHomeCollectionViewCell"];
//    [cell.objCollecionView reloadData];
//    return cell.contentView;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0
    ;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return kSCREEN_WIDTH/ASPECT_RATIO_SLIDER_IMG;
//}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return categoryBannerArr.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0)
        return kSCREEN_WIDTH/ASPECT_RATIO_SLIDER_IMG;
    else
        return UITableViewAutomaticDimension;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        static NSString *simpleTableIdentifier = @"UAEHomeSlidingBannersTableCell";
        UAEHomeSlidingBannersTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[UAEHomeSlidingBannersTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.bannerArr = bannerArr;
        cell.objPageControl.numberOfPages = bannerArr.count;
        [cell.objCollecionView registerNib:[UINib nibWithNibName:@"UAEHomeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"UAEHomeCollectionViewCell"];
        [cell.objCollecionView reloadData];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    else
    {
        
        static NSString *simpleTableIdentifier = @"UAEHomeBannerTableCell";
        UAEHomeBannerTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[UAEHomeBannerTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSDictionary *dic = [categoryBannerArr objectAtIndex:indexPath.row-1];
        [cell.bannerImageView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"banner_image"]]];
        cell.backgroundColor = [UIColor clearColor];

        return cell;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // NSDictionary *dic = [categoryBannerArr objectAtIndex:indexPath.row-1];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"UAEDesign" bundle:nil];
    UAEBrandViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"UAEBrandViewController"];
    //objVc.categoryID = [dic valueForKey:@"category_id"];
    objVc.categoryID = @"89";
    [self.navigationController pushViewController:objVc animated:YES];
}
#pragma mark -Navigation button click action


-(void)backButtonTapped{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
- (IBAction)countryFlagButtonClicked:(id)sender {

    UAEChooseCountryViewController *popUpController=[[UAEChooseCountryViewController alloc] initWithNibName:@"UAEChooseCountryViewController" bundle:nil];
    popUpController.popUpDelegate= self;
    [self.navigationController addChildViewController:popUpController];
    popUpController.view.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    [self.navigationController.view addSubview:popUpController.view];
    [popUpController didMoveToParentViewController:self];
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^
     {
         popUpController.view.alpha = 1;
         [popUpController uaeRadioButtonAction:nil];
     }
                     completion:nil];
    
}

#pragma mark - getHome data
-(void)callGetHomeDataWebservice{
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:UAE_HOME_WS MathodName:UAE_HOME_WS];
    }
}

#pragma mark - get app version
-(void)callGetAppVersionWebservice{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
    NSLog(@"current version %f",[currentVersion floatValue]);
    
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:NO];
    // Call Home screen webservice
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:GET_APP_VERSION_WS MathodName:GET_APP_VERSION_WS];
    }
}

#pragma mark - respose webservice
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    
    if([strName isEqualToString:GET_APP_VERSION_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==1) {
            NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
            NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
            float version = [currentVersion floatValue];
            NSLog(@"version %f",version);
            NSLog(@"version from backend %f",[[[response valueForKey:@"data"] valueForKey:@"appversion"] floatValue]);
            if(version < [[[response valueForKey:@"data"] valueForKey:@"appversion"] floatValue])
            {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                [self displayUpdateAppAlert];
            }
            else
            {
                [self callGetHomeDataWebservice];
            }
        }
            
    }
    else if ([strName isEqualToString:UAE_HOME_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==1) {
            scrollBannerAfter = [[response valueForKey:@"time"] intValue];
            bannerArr = [[response valueForKey:@"data"] valueForKey:@"banners"];
            categoryBannerArr = [[response valueForKey:@"data"] valueForKey:@"categoriesBanners"];
            [_homeTableView reloadData];
            [self scrollBanners];
        }
        else{
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
    
    }

}


-(void)displayUpdateAppAlert
{
    self.view.userInteractionEnabled = NO;
//    _navigationBarHtConstraint.constant = 0;
//    _leftSideMenuBtn.hidden = YES;
//    _rightSideMenuBtn.hidden = YES;
//    _logoImageView.hidden = YES;
//    _searchBarHtConstraint.constant = 0;
    self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New Version!!" message: @"We have introduced new features in the app, click ok to upgrade to newer version now." delegate:self cancelButtonTitle:@"Not Now" otherButtonTitles: @"Ok", nil];
    alert.tag = 500;
    [alert show];
    
    
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 500)
    {
        if (buttonIndex==1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:App_Link]];
        }
    }
}

#pragma mark - Store Popup delegate method

-(void)storeClicked:(NSString*)storeName{
    if ([storeName isEqualToString:UAE_STROE]) {
        [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:SELECTED_STORE];
        //[APP_DELEGATE switchToUAEStoreApplication];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:SELECTED_STORE];
        [APP_DELEGATE switchToINDIAStoreApplication];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
