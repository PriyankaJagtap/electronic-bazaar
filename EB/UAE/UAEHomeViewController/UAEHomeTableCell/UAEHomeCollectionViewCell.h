//
//  UAEHomeCollectionViewCell.h
//  EB
//
//  Created by Neosoft on 9/19/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEHomeCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bannerImageView;

@end
