//
//  UAEHomeSlidingBannersTableCell.h
//  EB
//
//  Created by Neosoft on 9/19/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEHomeSlidingBannersTableCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *objCollecionView;
@property (weak, nonatomic) IBOutlet UIPageControl *objPageControl;
@property (nonatomic, strong) NSArray *bannerArr;

@end
