//
//  UAEHomeSlidingBannersTableCell.m
//  EB
//
//  Created by Neosoft on 9/19/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEHomeSlidingBannersTableCell.h"
#import "UAEHomeCollectionViewCell.h"

#define ASPECT_RATIO_SLIDER_IMG 1.7684

@implementation UAEHomeSlidingBannersTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   

}

//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//        //objCollecionView = [UICollectionView alloc] initWithFrame://( your frame) ];
//        
//        _objCollecionView.dataSource = self;
//        _objCollecionView.delegate = self;
//        // set other properties as per tour needs .
//        
//        //[self.contentView addSubview:objCollecionView];
//    }
//    return self;
//}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - collectionView delegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    for (UICollectionViewCell *cell in [self.objCollecionView visibleCells]) {
        NSIndexPath *indexPath = [self.objCollecionView indexPathForCell:cell];
        //NSLog(@"visible index %ld",(long)indexPath.item);
        self.objPageControl.currentPage = indexPath.item;
        
    }
}
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _bannerArr.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(kSCREEN_WIDTH, kSCREEN_WIDTH/ASPECT_RATIO_SLIDER_IMG);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0,0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"UAEHomeCollectionViewCell";
    UAEHomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *dic = [_bannerArr objectAtIndex:indexPath.row];
    [cell.bannerImageView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"banner_image"]]];
    
    return cell;
    
}



@end
