//
//  UAEEditProfileViewController.h
//  EB
//
//  Created by webwerks on 22/09/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEEditProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *storeNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *managerOneTextField;
@property (weak, nonatomic) IBOutlet UITextField *whatsAppNoOneTextField;
@property (weak, nonatomic) IBOutlet UITextField *managerTwoTextField;
@property (weak, nonatomic) IBOutlet UITextField *whatsAppTwoTextField;
@end
