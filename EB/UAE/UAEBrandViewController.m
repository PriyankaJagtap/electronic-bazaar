//
//  UAEBrandViewController.m
//  EB
//
//  Created by webwerks on 25/09/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEBrandViewController.h"
//#import "SellGadgetBrandCollectionViewCell.h"
#import "UAEProductListingViewController.h"

@interface UAEBrandViewController ()<FinishLoadingData>
{
    NSArray *brandsDataArray;

}

@end

@implementation UAEBrandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [AppDelegate setCustomNavigationWithBackButton:self];
    self.navigationItem.title = @"Brands";
    [self getUAEBrands];
        
        
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - collectionView delegate methods
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    for (UICollectionViewCell *cell in [self.objCollectionView visibleCells]) {
//        NSIndexPath *indexPath = [self.objCollectionView indexPathForCell:cell];
//        [segmentControl setSelectedSegmentIndex:indexPath.item];
//
//    }
//    [self setSelectedSegment];
//}
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    
    return 1;
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    return brandsDataArray.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float width = (kSCREEN_WIDTH-30)/2;
    NSLog(@"collection width %f and height %f",width,width+28);
    return CGSizeMake(width, width+28);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 10, 0,10);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    static NSString *identifier = @"SellGadgetBrandCollectionViewCell";
//    SellGadgetBrandCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    NSDictionary *dic = [brandsDataArray objectAtIndex:indexPath.item];
//    //[cell.brandImageView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"img_url"]]];
//    cell.brandLabel.text = [dic valueForKey:@"name"];
//    cell.brandBgView.layer.borderWidth = 2.0f;
//    cell.brandBgView.layer.borderColor = [UIColor colorWithRed:218/255.0f green:218/255.0f blue:218/255.0f alpha:1.0].CGColor;
//    return cell;
        
        return nil;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UAEProductListingViewController *objVc = [[UAEProductListingViewController alloc] initWithNibName:@"UAEProductListingViewController" bundle:nil];
    [self.navigationController pushViewController:objVc animated:YES];
}


#pragma mark - get sell gadget category

-(void)getUAEBrands
{
    // Loader
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    
    // Call Home screen webservice
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate = self;
        NSString *url = [NSString stringWithFormat:@"%@%@",UAE_GET_BRANDS_WS,_categoryID];
        [callLoginService GetWebServiceWithURL:url MathodName:UAE_GET_BRANDS_WS];
    }
    
}
#pragma mark - request finish method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:UAE_GET_BRANDS_WS]) {
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            brandsDataArray = [[response valueForKey:@"data"] valueForKey:@"category_brands"];
            [_brandsCollectionView reloadData];
        }
        else
        {
            [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
        }
    }
    
}


#pragma mark - navigation button actions
-(void)cartButtonTapped{
    
}
-(void)backButtonTapped{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
