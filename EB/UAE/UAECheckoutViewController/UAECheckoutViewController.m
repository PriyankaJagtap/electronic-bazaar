//
//  UAECheckoutViewController.m
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAECheckoutViewController.h"

#import "UAEProductCell.h"
#import "UAECouponCodeCell.h"
#import "UAEGrandTotalCell.h"
#import "UAEAddressCell.h"
#import "UAEShipToThisAddressCell.h"
#import "UAEPlaceOrderCell.h"

@interface UAECheckoutViewController ()<UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource>
{
        UIPickerView *pickerView;
        UIView *bgPickerView;
        NSMutableArray *arrayBillingAddress;
        NSMutableArray *arrayShippingAddress;
        NSInteger billingPickerSelectedIndex;
        NSInteger shippingPickerSelectedIndex;
        BOOL isBillingPickerSelected;
        BOOL isShippingPickerSelected;
        BOOL isTapped;
}
@end

@implementation UAECheckoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
        [self registerCells];
        [AppDelegate setCustomNavigation:self];
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 440;
        arrayBillingAddress = [NSMutableArray arrayWithObjects:@"09, Midas, Sahar Plaza Complex, Andheri Kurla Road, Near J.B Nagar Metro Station, Andheri East, Mumbai.",@"NeoSOFT Technologies, Pune.", nil];
         arrayShippingAddress = [NSMutableArray arrayWithObjects:@"USA",@"INDIA", nil];
        isBillingPickerSelected = NO;
        isShippingPickerSelected = NO;
        billingPickerSelectedIndex = 0;
        shippingPickerSelectedIndex = 0;
        //self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerCells
{
        [self.tableView registerNib:[UINib nibWithNibName:@"UAEProductCell" bundle:nil] forCellReuseIdentifier:@"UAEProductCell"];
        
         [self.tableView registerNib:[UINib nibWithNibName:@"UAECouponCodeCell" bundle:nil] forCellReuseIdentifier:@"UAECouponCodeCell"];
        
         [self.tableView registerNib:[UINib nibWithNibName:@"UAEGrandTotalCell" bundle:nil] forCellReuseIdentifier:@"UAEGrandTotalCell"];
        
         [self.tableView registerNib:[UINib nibWithNibName:@"UAEAddressCell" bundle:nil] forCellReuseIdentifier:@"UAEAddressCell"];
        
         [self.tableView registerNib:[UINib nibWithNibName:@"UAEShipToThisAddressCell" bundle:nil] forCellReuseIdentifier:@"UAEShipToThisAddressCell"];
        
         [self.tableView registerNib:[UINib nibWithNibName:@"UAEPlaceOrderCell" bundle:nil] forCellReuseIdentifier:@"UAEPlaceOrderCell"];
}

#pragma mark - TableView methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 2;
}
- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
        if (section == 0)
        {
                return 4;
        }
        return 6;
        //return [arrayProductList count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        if (section == 0)
        {
                return 40;
        }
        return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        if (section == 0)
        {
                return self.headerView;
        }
        return nil;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
        if (indexPath.section == 0)
        {
                UAEProductCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"UAEProductCell"];
                cell.labelProductTitle.text = @"Apple iPhone 7 (128 GB, Black)";
                cell.labelStock.text = @"Only 2 units in stock";
                cell.labelTotal.text = @"AED 25,500";
                cell.productQuantityTextField.delegate = self;
                cell.productQuantityTextField.text = @"1";
                cell.buttonClose.tag = indexPath.row;
                [cell.buttonClose addTarget:self action:@selector(buttonCloseAction:) forControlEvents:UIControlEventTouchUpInside];
                [self.view layoutIfNeeded];
                return cell;
        }
        else
        {
                if (indexPath.row == 0)
                {
                        UAECouponCodeCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"UAECouponCodeCell"];
                        if ([cell.couponCodeTextField hasText])
                        {
                                cell.couponCodeTextField.delegate = self;
                                cell.buttonCouponApply.tag = indexPath.row;
                                [cell.buttonCouponApply addTarget:self action:@selector(buttonCouponApplyAction:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        else
                        {
                                // show popup : first enter coupon code
                        }
                        return cell;
                }
                else if(indexPath.row == 1)
                {
                        UAEGrandTotalCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"UAEGrandTotalCell"];
                        return cell;
                }
                else if(indexPath.row == 2)
                {
                        UAEAddressCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"UAEAddressCell"];
                        cell.labelAddressType.text = @"Billing Address";
                        if (isBillingPickerSelected)
                        {
                                cell.labelCompleteAddress.text = [arrayBillingAddress objectAtIndex: billingPickerSelectedIndex];
                        }
                        else
                        {
                                cell.labelCompleteAddress.text = [arrayBillingAddress objectAtIndex: 0];
                        }

                        cell.buttonAddNewAddress.tag = indexPath.row;
                         [cell.buttonAddNewAddress addTarget:self action:@selector(buttonAddNewAddressAction:) forControlEvents:UIControlEventTouchUpInside];
                        
                        cell.buttonAddressDownArrow.tag = indexPath.row;
                        [cell.buttonAddressDownArrow addTarget:self action:@selector(buttonAddressDownArrowAction:) forControlEvents:UIControlEventTouchUpInside];
                        
                        [self.view layoutIfNeeded];
                        return cell;
                }
                else if(indexPath.row == 3)
                {
                        UAEShipToThisAddressCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"UAEShipToThisAddressCell"];
                        
                        cell.buttonShipToThisAddress.tag = indexPath.row;
                        [cell.buttonShipToThisAddress addTarget:self action:@selector(buttonShipToThisAddressAction:) forControlEvents:UIControlEventTouchUpInside];
                                                
                        return cell;
                }
                else if(indexPath.row == 4)
                {
                        UAEAddressCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"UAEAddressCell"];
                        cell.labelAddressType.text = @"Shipping Address";
                        if (isShippingPickerSelected)
                        {
                                cell.labelCompleteAddress.text = [arrayShippingAddress objectAtIndex: shippingPickerSelectedIndex];
                        }
                        else
                        {
                                cell.labelCompleteAddress.text = [arrayShippingAddress objectAtIndex: 0];
                        }

                        cell.buttonAddNewAddress.tag = indexPath.row;
                        [cell.buttonAddNewAddress addTarget:self action:@selector(buttonAddNewAddressAction:) forControlEvents:UIControlEventTouchUpInside];
                        
                        cell.buttonAddressDownArrow.tag = indexPath.row;
                        [cell.buttonAddressDownArrow addTarget:self action:@selector(buttonAddressDownArrowAction:) forControlEvents:UIControlEventTouchUpInside];
                       
                        [self.view layoutIfNeeded];
                        return cell;
                }
                else
                {
                        UAEPlaceOrderCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"UAEPlaceOrderCell"];
                        cell.buttonPlaceOrder.tag = indexPath.row;
                        [cell.buttonPlaceOrder addTarget:self action:@selector(buttonPlaceOrderAction:) forControlEvents:UIControlEventTouchUpInside];
                        return cell;
                }
        }
}

#pragma MARK :- Selector Methods
-(void)buttonCloseAction:(id)sender
{
        NSLog(@"buttonCloseAction");
}

-(void)buttonCouponApplyAction:(id)sender
{
        NSLog(@"buttonCouponApplyAction");
}

-(void)buttonAddNewAddressAction:(id)sender
{
        NSLog(@"buttonAddNewAddressAction");
}

-(void)buttonShipToThisAddressAction:(id)sender
{
        NSLog(@"buttonShipToThisAddressAction");
        [sender setSelected: ![sender isSelected]];
}
- (void)buttonAddressDownArrowAction:(id)sender
{
        NSLog(@"%ld", (long)[sender tag]);
        switch( [sender tag])
        {
                case 2:
                        NSLog(@"Billing Address");
                        [self setPickerView:sender];
                        break;
                case 4:
                        NSLog(@"Shipping Address");
                        [self setPickerView:sender];
                        break;
        }
}

-(void)buttonPlaceOrderAction:(id)sender
{
        NSLog(@"buttonPlaceOrderAction");
}

#pragma mark - set custom picker
-(void)setPickerView: (id)sender
{
        if(bgPickerView == nil)
        {
                bgPickerView =[[UIView alloc]init];
                bgPickerView.frame = CGRectMake(0, self.view.frame.size.height - 200
                                                , self.view.frame.size.width, 200);
                
                UIButton *doneButton = [[UIButton alloc]init];
                doneButton.frame =CGRectMake(0, 0, bgPickerView.frame.size.width, 30);
                [doneButton setTitle:@"Done" forState:UIControlStateNormal];
                doneButton.titleLabel.font = [UIFont fontWithName:@"Karla-Regular" size:15.0f];
                doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                doneButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
                doneButton.backgroundColor =[UIColor lightGrayColor];
                [doneButton addTarget:self action:@selector(doneTouched:) forControlEvents:UIControlEventTouchUpInside];
                [bgPickerView addSubview:doneButton];
                [self.view addSubview:bgPickerView];
        }
        pickerView = [[UIPickerView alloc]init];
        pickerView.frame = CGRectMake(0, 30, self.view.frame.size.width, 170);
        pickerView.dataSource = self;
        pickerView.delegate = self;

        pickerView.tag = [sender tag];
       // pickerView.tag = ((UITapGestureRecognizer *)sender).view.tag;
        if (isBillingPickerSelected)
        {
                [pickerView selectRow: billingPickerSelectedIndex inComponent: 0 animated:YES];
        }
        if (isShippingPickerSelected)
        {
                [pickerView selectRow: shippingPickerSelectedIndex inComponent: 0 animated:YES];
        }
        pickerView.backgroundColor = [UIColor whiteColor];
        pickerView.showsSelectionIndicator = YES;
        [bgPickerView addSubview:pickerView];
        [pickerView reloadAllComponents];
}


- (void)doneTouched:(UIBarButtonItem *)sender
{
        //NSLog(@"picker selected indeex %ld and %@",(long)pickerSelectedIndex,[[pickerArray objectAtIndex:pickerSelectedIndex] valueForKey:@"customer_address_id"]);
        bgPickerView.hidden = YES;
        [bgPickerView removeFromSuperview];
        bgPickerView = nil;
        [self.tableView setContentOffset:CGPointMake(0, 0)];
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
        return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
        if ([pickerView tag] == 2)
        {
                return [arrayBillingAddress count];
        }
        else
        {
                return [arrayShippingAddress count];
        }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
        UILabel* pickerLabel = (UILabel*)view;
        
        if (!pickerLabel)
        {
                pickerLabel = [[UILabel alloc] init];
                
                pickerLabel.font = [UIFont fontWithName:@"Karla-Regular" size:14];
                
                pickerLabel.textAlignment=NSTextAlignmentCenter;
        }
        if ([pickerView tag] == 2)
        {
                [pickerLabel setText:[arrayBillingAddress objectAtIndex:row]];
        }
        else
        {
                 [pickerLabel setText:[arrayShippingAddress objectAtIndex:row]];
        }

        return pickerLabel;
}

#pragma mark- Picker View Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
         if ([pickerView tag] == 2)
         {
                 billingPickerSelectedIndex  = row;
                 isBillingPickerSelected = YES;
         }
        else
        {
                shippingPickerSelectedIndex  = row;
                isShippingPickerSelected = YES;
        }
        UAEAddressCell *cell;
        if ([pickerView tag] == 2)
        {
                cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow: 2 inSection: 1]];
                cell.labelCompleteAddress.text = [arrayBillingAddress objectAtIndex: row];
        }
        else
        {
                cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow: 4 inSection: 1]];
                cell.labelCompleteAddress.text = [arrayShippingAddress objectAtIndex: row];
        }
        
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow: (NSInteger)row forComponent:(NSInteger)component
{
        //return [[pickerArray objectAtIndex:row]valueForKey:@"complete_address"];
         if ([pickerView tag] == 2)
         {
                 return [arrayBillingAddress objectAtIndex:row];
         }
        else
        {
                return [arrayShippingAddress objectAtIndex:row];
        }
}
/*
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
        CGPoint pointInTable = [textField convertPoint:textField.frame.origin toView:self.tableView];
        CGPoint contentOffset = self.tableView.contentOffset;
        contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
        
        NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
        [self.tableView setContentOffset:contentOffset animated:YES];
        return YES;
}
*/
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
        [textField resignFirstResponder];
        return YES;
}
@end
