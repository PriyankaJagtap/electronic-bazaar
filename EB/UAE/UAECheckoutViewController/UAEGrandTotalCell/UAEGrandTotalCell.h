//
//  UAEGrandTotalCell.h
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEGrandTotalCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelSubTotal;
@property (weak, nonatomic) IBOutlet UILabel *labelDiscount;
@property (weak, nonatomic) IBOutlet UILabel *labelGrandTotal;
@property (weak, nonatomic) IBOutlet UIView *grandTotalView;

@end
