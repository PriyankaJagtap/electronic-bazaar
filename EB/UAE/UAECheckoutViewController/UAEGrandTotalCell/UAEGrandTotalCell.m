//
//  UAEGrandTotalCell.m
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEGrandTotalCell.h"

@implementation UAEGrandTotalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
        self.grandTotalView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.grandTotalView.layer.borderWidth = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
