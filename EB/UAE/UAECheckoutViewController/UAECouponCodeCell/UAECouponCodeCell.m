//
//  UAECouponCodeCell.m
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAECouponCodeCell.h"

@implementation UAECouponCodeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
        self.couponView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.couponView.layer.borderWidth = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
