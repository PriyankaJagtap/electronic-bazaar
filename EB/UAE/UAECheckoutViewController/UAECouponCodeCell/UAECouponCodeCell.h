//
//  UAECouponCodeCell.h
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAECouponCodeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *couponCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *buttonCouponApply;
@property (weak, nonatomic) IBOutlet UIView *couponView;

@end
