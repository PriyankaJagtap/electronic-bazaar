//
//  UAEAddressCell.m
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEAddressCell.h"

@implementation UAEAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
        
        self.addressView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.addressView.layer.borderWidth = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
