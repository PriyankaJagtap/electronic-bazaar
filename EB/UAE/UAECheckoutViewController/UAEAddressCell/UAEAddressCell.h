//
//  UAEAddressCell.h
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelAddressType;
@property (weak, nonatomic) IBOutlet UILabel *labelCompleteAddress;
@property (weak, nonatomic) IBOutlet UIButton *buttonAddNewAddress;
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIButton *buttonAddressDownArrow;

@end
