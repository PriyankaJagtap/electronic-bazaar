//
//  UAEPlaceOrderCell.h
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEPlaceOrderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelPlaceOrderTotal;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlaceOrder;
@property (weak, nonatomic) IBOutlet UIView *placeOrderView;

@end
