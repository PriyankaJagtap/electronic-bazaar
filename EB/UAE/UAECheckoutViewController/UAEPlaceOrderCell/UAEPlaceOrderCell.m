//
//  UAEPlaceOrderCell.m
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEPlaceOrderCell.h"

@implementation UAEPlaceOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
        
        self.placeOrderView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.placeOrderView.layer.borderWidth = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
