//
//  UAEProductCell.h
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEProductCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *labelProductTitle;
@property (weak, nonatomic) IBOutlet UITextField *productQuantityTextField;
@property (weak, nonatomic) IBOutlet UILabel *labelStock;
@property (weak, nonatomic) IBOutlet UILabel *labelTotal;
@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

@end
