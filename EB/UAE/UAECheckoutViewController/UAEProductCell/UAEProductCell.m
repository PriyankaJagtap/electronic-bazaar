//
//  UAEProductCell.m
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEProductCell.h"

@implementation UAEProductCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
        
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.layer.borderWidth = 0.5;
        //self.buttonClose.layer.cornerRadius = self.buttonClose.frame.size.width / 2;
        //self.buttonClose.layer.borderColor = UIColorFromRGB(0xF95A69).CGColor;
       // self.buttonClose.layer.borderWidth = 1;
        //self.buttonClose.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
