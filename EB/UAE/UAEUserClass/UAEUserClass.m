//
//  UAEUserClass.m
//  EB
//
//  Created by Neosoft on 9/14/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEUserClass.h"

@implementation UAEUserClass

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.quoteId forKey:@"quoteId"];
    [encoder encodeObject:[NSNumber numberWithInt:self.cart_total_qty] forKey:@"cart_total_qty"];
    [encoder encodeObject:self.customer_id forKey:@"customer_id"];
    
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.firstname forKey:@"firstname"];
    [encoder encodeObject:self.lastname forKey:@"lastname"];
    [encoder encodeObject:self.store_name forKey:@"store_name"];
    [encoder encodeObject:self.manager1 forKey:@"manager1"];
    [encoder encodeObject:self.manager2 forKey:@"manager2"];
    [encoder encodeObject:self.whatsapp1 forKey:@"whatsapp1"];
    [encoder encodeObject:self.whatsapp2 forKey:@"whatsapp2"];
    [encoder encodeObject:self.mobile forKey:@"mobile"];

}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.quoteId = [decoder decodeObjectForKey:@"quoteId"];
        self.cart_total_qty = [[decoder decodeObjectForKey:@"cart_total_qty"] intValue];
        self.customer_id = [decoder decodeObjectForKey:@"customer_id"];
        
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.firstname = [decoder decodeObjectForKey:@"firstname"];
        self.lastname = [decoder decodeObjectForKey:@"lastname"];
        self.store_name = [decoder decodeObjectForKey:@"store_name"];
        self.manager1 = [decoder decodeObjectForKey:@"manager1"];
        self.manager2 = [decoder decodeObjectForKey:@"manager2"];
        self.whatsapp1 = [decoder decodeObjectForKey:@"whatsapp1"];
        self.whatsapp2 = [decoder decodeObjectForKey:@"whatsapp2"];
        self.mobile = [decoder decodeObjectForKey:@"mobile"];


    }
    return self;
}


-(void)setObjectFromDictionary:(NSDictionary *)response{
    
    CommonSettings *objCommonSetting = [CommonSettings sharedInstance];
    NSDictionary *userDic = [response valueForKey:@"user"];
    self.quoteId = [response valueForKey:@"quoteId"];
    self.cart_total_qty = [[response valueForKey:@"cart_total_qty"] intValue];
    self.customer_id = [response valueForKey:@"customer_id"];
    
    self.userId = [objCommonSetting checkNullValue:[userDic valueForKey:@"id"]]?@"0":[userDic valueForKey:@"id"];
    
    self.email = [objCommonSetting checkNullValue:[userDic valueForKey:@"email"]]?@"":[userDic valueForKey:@"email"];
    
    self.firstname = [objCommonSetting checkNullValue:[userDic valueForKey:@"firstname"]]?@"":[userDic valueForKey:@"firstname"];
    
    self.lastname = [objCommonSetting checkNullValue:[userDic valueForKey:@"lastname"]]?@"":[userDic valueForKey:@"lastname"];
    self.store_name = [objCommonSetting checkNullValue:[userDic valueForKey:@"store_name"]]?@"":[userDic valueForKey:@"store_name"];
    self.manager1 = [objCommonSetting checkNullValue:[userDic valueForKey:@"manager1"]]?@"":[userDic valueForKey:@"manager1"];
    self.manager2 = [objCommonSetting checkNullValue:[userDic valueForKey:@"manager2"]]?@"":[userDic valueForKey:@"manager2"];
    self.whatsapp1 = [objCommonSetting checkNullValue:[userDic valueForKey:@"whatsapp1"]]?@"":[userDic valueForKey:@"whatsapp1"];
    self.whatsapp2 = [objCommonSetting checkNullValue:[userDic valueForKey:@"whatsapp2"]]?@"":[userDic valueForKey:@"whatsapp2"];
    self.mobile = [objCommonSetting checkNullValue:[userDic valueForKey:@"mobile"]]?@"":[userDic valueForKey:@"mobile"];
}

-(void)saveRegisteredUserData:(NSDictionary *)response{
    CommonSettings *objCommonSetting = [CommonSettings sharedInstance];
    NSDictionary *userDic = [response valueForKey:@"user"];
    self.cart_total_qty = 0;
    self.userId = [objCommonSetting checkNullValue:[userDic valueForKey:@"id"]]?@"0":[userDic valueForKey:@"id"];
    
    self.email = [objCommonSetting checkNullValue:[userDic valueForKey:@"email"]]?@"":[userDic valueForKey:@"email"];
    
    self.quoteId = [objCommonSetting checkNullValue:[userDic valueForKey:@"quoteId"]]?@"":[userDic valueForKey:@"quoteId"];
    
    self.firstname = [objCommonSetting checkNullValue:[userDic valueForKey:@"firstname"]]?@"":[userDic valueForKey:@"firstname"];
    
    self.lastname = [objCommonSetting checkNullValue:[userDic valueForKey:@"lastname"]]?@"":[userDic valueForKey:@"lastname"];
    self.store_name = [objCommonSetting checkNullValue:[userDic valueForKey:@"store_name"]]?@"":[userDic valueForKey:@"store_name"];
    self.manager1 = [objCommonSetting checkNullValue:[userDic valueForKey:@"manager1"]]?@"":[userDic valueForKey:@"manager1"];
    self.manager2 = [objCommonSetting checkNullValue:[userDic valueForKey:@"manager2"]]?@"":[userDic valueForKey:@"manager2"];
    self.whatsapp1 = [objCommonSetting checkNullValue:[userDic valueForKey:@"whatsapp1"]]?@"":[userDic valueForKey:@"whatsapp1"];
    self.whatsapp2 = [objCommonSetting checkNullValue:[userDic valueForKey:@"whatsapp2"]]?@"":[userDic valueForKey:@"whatsapp2"];
    self.mobile = [objCommonSetting checkNullValue:[userDic valueForKey:@"mobile"]]?@"":[userDic valueForKey:@"mobile"];

}

- (void)saveCustomObject:(UAEUserClass *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
    
}

+(UAEUserClass *)loadCustomObjectWithKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    UAEUserClass *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

@end
