//
//  UAEUserClass.h
//  EB
//
//  Created by Neosoft on 9/14/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UAEUserClass : NSObject
//{
//    "status":1,
//    "message":"Logged In Successfully",
//    "user":{
//        "id":"30074",
//        "email":"xsxsas@aasa.com",
//        "firstname":"ASCASC",
//        "lastname":"ASCASC",
//        "store_name":"ASCASC",
//        "manager1":null,
//        "manager2":null,
//        "whatsapp1":null,
//        "whatsapp2":null,
//        "mobile":"4254555"
//    },
//    "login_type":"normal",
//    "quoteId":"114836",
//    "cart_total_qty":0,
//    "customer_id":"1"
//}

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *store_name;
@property (nonatomic, strong) NSString *manager1;
@property (nonatomic, strong) NSString *manager2;
@property (nonatomic, strong) NSString *whatsapp1;
@property (nonatomic, strong) NSString *whatsapp2;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *quoteId;
@property (nonatomic) int cart_total_qty;
@property (nonatomic, strong) NSString *customer_id;

-(void)setObjectFromDictionary:(NSDictionary *)dic;
-(void)saveRegisteredUserData:(NSDictionary *)response;
- (void)saveCustomObject:(UAEUserClass *)object key:(NSString *)key;
+(UAEUserClass *)loadCustomObjectWithKey:(NSString *)key;

@end
