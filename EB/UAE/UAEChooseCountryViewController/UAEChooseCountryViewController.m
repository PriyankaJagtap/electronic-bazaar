//
//  UAEChooseCountryViewController.m
//  EB
//
//  Created by webwerks on 8/16/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAEChooseCountryViewController.h"

@interface UAEChooseCountryViewController ()

@end

@implementation UAEChooseCountryViewController{
    NSString *storageClicked;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        // Do any additional setup after loading the view from its nib.
        
}


- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction methods
- (IBAction)buttonDoneAction:(id)sender
{
    [self.popUpDelegate storeClicked:storageClicked];
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (IBAction)buttonCloseAction:(id)sender
{
    [self.view removeFromSuperview];
}
- (IBAction)indiaRadioButtonAction:(id)sender
{
    [self.indiaRadioButton setBackgroundImage:[UIImage imageNamed:@"btn_radio_on"] forState:0];
    [self.uaeRadioButton setBackgroundImage:[UIImage imageNamed:@"btn_radio_off"] forState:0];
    storageClicked = INDIAN_STORE;
}
- (IBAction)uaeRadioButtonAction:(id)sender
{
    [self.uaeRadioButton setBackgroundImage:[UIImage imageNamed:@"btn_radio_on"] forState:0];
    [self.indiaRadioButton setBackgroundImage:[UIImage imageNamed:@"btn_radio_off"] forState:0];
    storageClicked = UAE_STROE;
    
}




@end
