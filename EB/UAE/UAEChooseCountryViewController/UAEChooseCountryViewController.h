//
//  UAEChooseCountryViewController.h
//  EB
//
//  Created by webwerks on 8/16/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StorePopUpDelegate <NSObject>

-(void)storeClicked:(NSString*)storeName;

@end

@interface UAEChooseCountryViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *indiaRadioButton;
@property (weak, nonatomic) IBOutlet UIButton *uaeRadioButton;
@property (weak, nonatomic) id<StorePopUpDelegate> popUpDelegate;
- (IBAction)indiaRadioButtonAction:(id)sender;
- (IBAction)uaeRadioButtonAction:(id)sender;
@end
