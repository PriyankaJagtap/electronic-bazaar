//
//  UAELoginViewController.h
//  EB
//
//  Created by webwerks on 8/16/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAELoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *showPasswordBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn;

//@property (nonatomic) BOOL isSellGadgetBannerClicked;

//@property (strong,nonatomic)ShowTabbars *tabBarObj;

@end
