//
//  UAELoginViewController.m
//  EB
//
//  Created by webwerks on 8/16/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAELoginViewController.h"
#import "UAEUserClass.h"
#import "UAEHomeViewController.h"
#import "UAEForgotPasswordViewController.h"

@interface UAELoginViewController ()<FinishLoadingData>

@end

@implementation UAELoginViewController
@synthesize mobileTextField, passwordTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [CommonSettings addPaddingAndImageToTextField: mobileTextField withImageName:@"gray_mobile_icon"];
    [CommonSettings addPaddingAndImageToTextField: passwordTextField withImageName:@"gray_pwd_icon"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults valueForKey:UAE_LOGIN_USER_MOBILE])
        mobileTextField.text = [defaults valueForKey:UAE_LOGIN_USER_MOBILE];
    
    if([defaults valueForKey:UAE_LOGIN_USER_PASSWORD])
        passwordTextField.text = [defaults valueForKey:UAE_LOGIN_USER_PASSWORD];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - call login webservice
-(void)callLoginWS:(NSMutableDictionary *)userdata{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:userdata url:UAE_LOGIN_WS string:UAE_LOGIN_WS];
    }
}


#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:UAE_LOGIN_WS]){
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            UAEUserClass *objUserClass = [[UAEUserClass alloc] init];
            [objUserClass setObjectFromDictionary:response];
            [objUserClass saveCustomObject:objUserClass key:UAE_USER];
            [[AppDelegate getAppDelegateObj] switchToUAEStoreApplication];
            [[NSUserDefaults standardUserDefaults] setValue:mobileTextField.text forKey:UAE_LOGIN_USER_MOBILE];
            [[NSUserDefaults standardUserDefaults] setValue:passwordTextField.text forKey:UAE_LOGIN_USER_PASSWORD];
            [[NSUserDefaults standardUserDefaults] synchronize];

        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
        }
        
    }
}
#pragma mark - IBAction methods
- (IBAction)forgotPasswordAction:(id)sender
{
    NSLog(@"forgotPasswordAction");
    UAEForgotPasswordViewController *vc = [[UAEForgotPasswordViewController alloc] initWithNibName:@"UAEForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)submitAction:(id)sender
{
    NSLog(@"submitAction");
    [self.view endEditing:YES];
    if (self.mobileTextField.text.length == 0)
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide Mobile Number"];
        return;
    }
    
    if (self.passwordTextField.text.length == 0)
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter password."];
        return;
    }
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary * dict = [NSMutableDictionary new];
    [dict setObject:@"iphone" forKey:@"device_type"];
    [dict setObject:self.passwordTextField.text forKey:@"password"];
    [dict setObject:self.mobileTextField.text forKey:@"mobile"];
    NSString *pushID=[defaults valueForKey:@"DEVICE_TOKEN"];
    [dict setObject:pushID?:@"" forKey:@"push_id"];
    [dict setObject:[CommonSettings getAppCurrentVersion] forKey:@"version"];
    
    NSLog(@"login dic %@",dict);
    [self callLoginWS:dict];
    
    
}

- (IBAction)showPasswordAction:(id)sender
{
    NSLog(@"showPasswordAction");
    _showPasswordBtn.selected = ! _showPasswordBtn.selected;
    
    if (_showPasswordBtn.selected) {
        self.passwordTextField.secureTextEntry = NO;
        self.passwordTextField.text =self.passwordTextField.text ;
    }
    else{
        self.passwordTextField.secureTextEntry = YES;
        self.passwordTextField.text =self.passwordTextField.text ;
    }

}

- (IBAction)navigateToHomeScreen:(id)sender
{
    NSLog(@"navigateToHomeScreen");
    [[AppDelegate getAppDelegateObj] switchToUAEStoreApplication];
}
@end
