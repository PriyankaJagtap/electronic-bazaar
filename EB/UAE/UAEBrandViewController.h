//
//  UAEBrandViewController.h
//  EB
//
//  Created by webwerks on 25/09/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEBrandViewController : UIViewController
@property (nonatomic, strong) NSString *categoryID;
@property (weak, nonatomic) IBOutlet UICollectionView *brandsCollectionView;
@end
