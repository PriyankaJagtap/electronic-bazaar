//
//  UAELoginSignUpViewController.h
//  EB
//
//  Created by webwerks on 8/16/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAELoginSignUpViewController : UIViewController

- (IBAction)LoginBtnClicked:(id)sender;
- (IBAction)SignUpBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *loginContainerView;
@property (weak, nonatomic) IBOutlet UIView *signUpContainerView;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogin;
@property (weak, nonatomic) IBOutlet UIButton *buttonSignUp;

@end
