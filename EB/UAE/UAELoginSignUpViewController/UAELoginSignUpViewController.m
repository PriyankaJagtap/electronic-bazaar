//
//  UAELoginSignUpViewController.m
//  EB
//
//  Created by webwerks on 8/16/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UAELoginSignUpViewController.h"
#import "UAELoginViewController.h"
#import "UAESignUpViewController.h"

@interface UAELoginSignUpViewController ()

@end

@implementation UAELoginSignUpViewController

- (void)viewDidLoad
{
        [super viewDidLoad];
        // Do any additional setup after loading the view from its nib.
        _loginContainerView.hidden = NO;
        _signUpContainerView.hidden = YES;
        [self displayContentController:[[UAELoginViewController alloc] initWithNibName:@"UAELoginViewController" bundle:nil] toView:_loginContainerView];
        
        [self displayContentController:[[UAESignUpViewController alloc] initWithNibName:@"UAESignUpViewController" bundle:nil] toView:_signUpContainerView];
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        self.buttonLogin.backgroundColor = UIColorFromRGB(0xFBDB10);
        [self.buttonLogin setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.buttonSignUp.backgroundColor = [UIColor clearColor];
        self.buttonSignUp.layer.borderColor = [UIColor whiteColor].CGColor;
        self.buttonSignUp.layer.borderWidth = 1;
}

- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction methods
- (IBAction)LoginBtnClicked:(id)sender
{
        self.buttonLogin.backgroundColor = UIColorFromRGB(0xFBDB10);
        [self.buttonLogin setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.buttonLogin.layer.borderColor = [UIColor clearColor].CGColor;
        self.buttonLogin.layer.borderWidth = 0;
        
        self.buttonSignUp.backgroundColor = [UIColor clearColor];
        [self.buttonSignUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.buttonSignUp.layer.borderColor = [UIColor whiteColor].CGColor;
        self.buttonSignUp.layer.borderWidth = 1;
        
        [UIView animateWithDuration:0.25 animations:^{
                [self.view layoutIfNeeded];
                _loginContainerView.hidden = NO;
                _signUpContainerView.hidden = YES;
                [self.view layoutIfNeeded];
        }];
}

- (IBAction)SignUpBtnClicked:(id)sender
{
        self.buttonSignUp.backgroundColor = UIColorFromRGB(0xFBDB10);
         [self.buttonSignUp setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.buttonSignUp.layer.borderColor = [UIColor clearColor].CGColor;
        self.buttonSignUp.layer.borderWidth = 0;

        
        self.buttonLogin.backgroundColor = [UIColor clearColor];
         [self.buttonLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.buttonLogin.layer.borderColor = [UIColor whiteColor].CGColor;
        self.buttonLogin.layer.borderWidth = 1;
        
        [UIView animateWithDuration:0.25 animations:^{
                [self.view layoutIfNeeded];
                _loginContainerView.hidden = YES;
                _signUpContainerView.hidden = NO;
                [self.view layoutIfNeeded];
        }];
        
}

#pragma mark - display views
- (void) displayContentController: (UIViewController*) content toView:(UIView *)toView;
{
        content.view.frame = toView.bounds;
        [toView addSubview:content.view];
        [self addChildViewController:content];
        [content didMoveToParentViewController:self];
}


@end
