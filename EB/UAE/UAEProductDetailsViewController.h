//
//  UAEProductDetailsViewController.h
//  EB
//
//  Created by webwerks on 8/17/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEProductDetailsViewController : UIViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelNewPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelOldPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelProductDescription;
@property (weak, nonatomic) IBOutlet UILabel *labelSize;

@property (weak, nonatomic) IBOutlet UICollectionView *sizeCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *labelColor;
@property (weak, nonatomic) IBOutlet UICollectionView *colorCollectionView;
@property (weak, nonatomic) IBOutlet UITextField *quantityTextField;
@property (weak, nonatomic) IBOutlet UITextField *pincodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *buttonGradingPolicy;
@property (weak, nonatomic) IBOutlet UILabel *specificationLable1;
@property (weak, nonatomic) IBOutlet UILabel *specificationLable2;
@property (weak, nonatomic) IBOutlet UILabel *specificationLable3;
@property (weak, nonatomic) IBOutlet UILabel *specificationLable4;

@property (weak, nonatomic) IBOutlet UILabel *labelGrade;
@property (weak, nonatomic) IBOutlet UIImageView *gradeImageView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end
