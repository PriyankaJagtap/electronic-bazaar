//
//  UAEProductSizeCell.h
//  EB
//
//  Created by webwerks on 8/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEProductSizeCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@end
