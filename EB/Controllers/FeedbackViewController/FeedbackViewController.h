//
//  FeedbackViewController.h
//  EB
//
//  Created by Aishwarya Rai on 25/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackViewController : UIViewController{
    __weak IBOutlet UIButton *submitFeedbackButton;
    __weak IBOutlet UIButton *maybeLaterButton;
    __weak IBOutlet UITextView *feedbackTextView;
}

@property (weak,nonatomic) IBOutlet UIView *rateView;
-(IBAction)onClickof_Feedback_SubmitBtn:(id)sender;
-(IBAction)onClickof_laterBtn:(id)sender;
- (IBAction)clickOnRatingBtnAction:(id)sender;

@end
