//
//  FeedbackViewController.m
//  EB
//
//  Created by Aishwarya Rai on 25/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "FeedbackViewController.h"
#import "HomeViewController.h"

@interface FeedbackViewController ()<FinishLoadingData>{
    NSString *ratingString;
    NSString *orderID;
}

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    orderID =[defaults valueForKey:@"Order_ID"];
    ratingString = @"";
    feedbackTextView.text = @"Enter Feedback (optional)";
    feedbackTextView.textColor = [UIColor lightGrayColor];
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:submitFeedbackButton];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextview delegate
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    feedbackTextView.text = @"";
    feedbackTextView.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(feedbackTextView.text.length == 0){
        feedbackTextView.textColor = [UIColor lightGrayColor];
        feedbackTextView.text = @"Enter Feedback (optional)";
        [feedbackTextView resignFirstResponder];
    }
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    if(feedbackTextView.text.length == 0){
        feedbackTextView.textColor = [UIColor lightGrayColor];
        feedbackTextView.text = @"Enter Feedback (optional)";
        [feedbackTextView resignFirstResponder];
    }
    
}

#pragma mark - Button Click Action Methids
-(IBAction)onClickof_laterBtn:(id)sender{
    NSLog(@"Maybe Later Button clicked");
    [self willMoveToParentViewController:nil];
    [self removeFromParentViewController];
    [self.view removeFromSuperview];
}

-(IBAction)onClickof_Feedback_SubmitBtn:(id)sender{
    NSLog(@"Feedback Submit Button click");
    
        if(![ratingString isEqualToString:@""])
        {
                [self callSubmitFeedbackWs];
        }
        else
        {
                [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Please Rate Us!!"];
        }
}

#pragma mark : Rating Button Action Method

- (IBAction)clickOnRatingBtnAction:(UIButton *)sender {
    
    ratingString = [NSString stringWithFormat:@"%ld",sender.tag];
    [sender setSelected:true];
    
    feedbackTextView.layer.borderColor = [UIColor blackColor].CGColor;
    feedbackTextView.layer.borderWidth = 1.0;
    feedbackTextView.hidden = false;
}

#pragma mark - Web service methods

-(void)callSubmitFeedbackWs
{
    Webservice  *submitFeedback = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        submitFeedback.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        if (!(quoteID==0)) {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
            NSString *strParam=[NSString stringWithFormat:@"cid=%@&feedback=%@&scale=%@&orderid=%@",userId,feedbackTextView.text,ratingString,orderID];
            
            [submitFeedback webServiceWitParameters:strParam andURL:SUBMIT_FEEDBACK_WS
                                         MathodName:@"SUBMIT_FEEDBACK"];
        }
    }
    
}

#pragma mark - Webservice Requeset Finished
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:@"SUBMIT_FEEDBACK"]){
        if ([[response valueForKey:@"status"]intValue]==0) {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Could not submit. Try Again Later"];
        }
        else{
            
//            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//            [defaults setObject:0 forKey:@"QuoteID"];
//            [defaults synchronize];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Feedback Submitted Sucessfully" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         
                                         [self removeFromParentViewController];
                                         [self willMoveToParentViewController:nil];
                                         [self.view removeFromSuperview];
                                             
                                             if(![self.parentViewController isKindOfClass:[HomeViewController class]])
                                             {
                                                     AppDelegate *appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
                                                     [[appdelegate tabBarObj]TabClickedIndex:0];
                                             }
                                         
                                    
                                     });
                                     
                                 }];
            [alert addAction:ok];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
    }
}

@end
