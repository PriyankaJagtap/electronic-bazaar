//
//  EditAddressViewController.m
//  EB
//
//  Created by webwerks on 9/11/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "EditAddressViewController.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "Webservice.h"
#import "Validation.h"
#import "CommonSettings.h"
#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

@interface EditAddressViewController ()<FinishLoadingData>
{
        NSString *countryID;
        NSString *region;
        int isDefaultBilling;
        int isDefaultShipping;
        NSInteger *selectedCountryAtIndex;
        NSInteger *selectedStateAtIndex;
        NSString *regionId;
        
}

@end

@implementation EditAddressViewController
@synthesize firstNameTxt,lastNameTxt,countryTxt,pincodeTxt,cityTxt,stateTxt,streetAddressTxt,streetAddress2Txt,mobileTxt;

- (void)viewDidLoad {
        [super viewDidLoad];
        [super setViewControllerTitle:@"Add New Address"];
        // set wrap view for text field
    //self.navigationController.navigationBar.hidden = true;
        mobileTxt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        //     _telephoneTxt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        //    telephone_Txt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        pincodeTxt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        
        [self createWrapView:_gstNumberTextField];
        [self createWrapView:_storeNameTextField];
        [self createWrapView:mobileTxt];
        [self createWrapView:streetAddressTxt];
        [self createWrapView:streetAddress2Txt];
        [self createWrapView:countryTxt];
        [self createWrapView:cityTxt];
        [self createWrapView:pincodeTxt];
        [self createWrapView:stateTxt];
        //[self setUpNavigationBar];
        [self countryListRequest];
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.saveAddress];
    
        [pincodeTxt addTarget:self
                       action:@selector(pincodeValueChanged:)
             forControlEvents:UIControlEventEditingChanged];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *userDic = [defaults valueForKey:@"user"];
        if([userDic valueForKey:@"pincode"])
        {
                pincodeTxt.text = [userDic valueForKey:@"pincode"];
                // [self pinCodeWSCall];
                
        }
        
        if(userDic)
        {
                _storeNameTextField.text = [userDic valueForKey:@"firstname"];
                mobileTxt.text = [userDic valueForKey:@"mobile"];
        }
}

-(void) pincodeValueChanged:(id)sender {
        // your code
        
        if(pincodeTxt.text.length  == 6)
                [self pinCodeWSCall];
        
}


-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear: YES];
        //[self setUpNavigationBar];
        [[[AppDelegate getAppDelegateObj]tabBarObj]hideTabbar];
}
- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[[AppDelegate getAppDelegateObj]tabBarObj]unhideTabbar];
}
#pragma mark - Webservices

-(void)callAddAddressWS
{
        Webservice  *callRegionList = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callRegionList.delegate =self;
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callRegionList webServiceWitParameters:[NSString stringWithFormat:@"user_id=%d&firstname=%@&lastname=%@,country_id=%@&region=%@&city=%@&street_1=%@&street_2=%@&telephone=%@postcode=%@&is_default_billing=%d&is_default_shipping=%d",userId,firstNameTxt.text,lastNameTxt.text,countryID,region,cityTxt.text,streetAddressTxt.text,streetAddress2Txt.text,mobileTxt.text,pincodeTxt.text,isDefaultBilling,isDefaultShipping] andURL:ADD_ADDRESS_WS MathodName:@"ADDADDRESS"];
        }
}

-(void)countryListRequest
{
        Webservice  *callCountryList = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callCountryList.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callCountryList webServiceWitParameters:nil andURL:COUNTRYLIST_WS MathodName:@"GETCOUNTRYLIST"];
        }
        
}
-(void)pinCodeWSCall
{
        Webservice  *callRegionList = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callRegionList.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callRegionList webServiceWitParameters:[NSString stringWithFormat:@"pincode=%@",pincodeTxt.text] andURL:PINCODE_WS MathodName:@"PINCODE"];
        }
}


-(void)regionRequestWithCountryId:(NSString *)countryId
{
        Webservice  *callRegionList = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callRegionList.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callRegionList webServiceWitParameters:[NSString stringWithFormat:@"country_id=%@",countryId] andURL:GET_REGION_LISTING_WS MathodName:@"GETREGION"];
        }
        
}



#pragma mark - manage UX method

-(void)createWrapView:(UITextField *)textfield{
        UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 10, textfield.frame.size.height)];
        textfield.leftViewMode = UITextFieldViewModeAlways;
        textfield.leftView = wrapView;
}


- (IBAction)onClickOfUse_As_Default_Billing_Add:(UIButton*)sender {
        if (isDefaultBilling==0)
        {
                [sender setBackgroundImage:[UIImage imageNamed:@"selected_box"] forState:UIControlStateNormal];
                isDefaultBilling=1;
        }else{
                isDefaultBilling=0;
                [sender setBackgroundImage:[UIImage imageNamed:@"Unselected_box"] forState:UIControlStateNormal];
        }
}

- (IBAction)onClickOfUse_As_Default_Shipping_Add:(UIButton*)sender {
        if (isDefaultShipping==0)
        {
                [sender setBackgroundImage:[UIImage imageNamed:@"selected_box"] forState:UIControlStateNormal];
                isDefaultShipping=1;
        }else{
                isDefaultShipping=0;
                [sender setBackgroundImage:[UIImage imageNamed:@"Unselected_box"] forState:UIControlStateNormal];
        }
        
}


- (IBAction)saveAction:(id)sender {
        
        Webservice  *callSaveAddressWS = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                if ([self validateTextfields])
                {
                        NSString *countryId=[[countryListArray objectAtIndex:(int)selectedCountryAtIndex]valueForKey:@"value"];
                        
                        int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                        
                        NSString *lastName = @"";
                        NSString *strParam=[NSString stringWithFormat:@"user_id=%d&firstname=%@&lastname=%@&country_id=%@&region=%@&city=%@&street_1=%@&street_2=%@&telephone=%@&postcode=%@&is_default_billing=%d&is_default_shipping=%d&company=%@&mobile=%@&gstin=%@",userId,_storeNameTextField.text,lastName,countryId,regionId,cityTxt.text,streetAddressTxt.text,streetAddress2Txt.text,mobileTxt.text,pincodeTxt.text,isDefaultBilling,isDefaultShipping,_storeNameTextField.text,mobileTxt.text,_gstNumberTextField.text];
                        
                        
                        callSaveAddressWS.delegate =self;
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        [callSaveAddressWS webServiceWitParameters:strParam andURL:CREATE_CUSTOMER_ADDRESS MathodName:@"CREATE_ADDRESS"];
                }
        }
        
        
        
        
}
-(void)doneButtonDidPressed:(id)sender
{
        dispatch_async(dispatch_get_main_queue(), ^{
                [self.view endEditing:YES];
                
        });
}
#pragma mark - textField delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [textField resignFirstResponder];
        return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if(textField == lastNameTxt)
                {
                        if(textField.text.length < 3)
                        {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:LAST_NAME_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                        }
                }if(textField == firstNameTxt)
                {
                        if(textField.text.length < 3)
                        {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:FIRST_NAME_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                        }
                }
                else if(textField == mobileTxt)
                {
                        if(textField.text.length < 10)
                        {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                        }
                }
                
        });
        
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
        if(textField == firstNameTxt || textField == lastNameTxt)
        {
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                
                return [string isEqualToString:filtered];
        }
        
        else if(textField == _gstNumberTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_GST_NUMBER_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                if(newLength <= 15)
                {
                        return [string isEqualToString:filtered];
                }
                
                if(newLength > 15){
                        
                        return NO;
                }
        }
        
        else if(textField == mobileTxt)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 10){
                        
                        return NO;
                }
                else{
                        return YES;
                }
        }
        else if(textField == pincodeTxt)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 6){
                        
                        return NO;
                }
                else{
                        cityTxt.text=@"";
                        stateTxt.text=@"";
                        countryTxt.text=@"";
                        return YES;
                }
        }
        
        return YES;
}

#pragma mark- Validate Textfields

-(BOOL)validateTextfields{
        //    if (![Validation required:firstNameTxt withCaption:@"First name"])
        //        return NO;
        //    if (![Validation required:lastNameTxt withCaption:@"Last name"])
        //        return NO;
        //    if (![Validation required:mobileTxt withCaption:@"Mobile Number"])
        //        return NO;
        
        //    if(![Validation checkFirstName:firstNameTxt])
        //        return NO;
        //    if(![Validation checkLastName:lastNameTxt])
        //        return NO;
        
        if(![Validation required:_storeNameTextField withCaption:@"Store Name"])
                return NO;
        
        
        if (_gstNumberTextField.text.length != 0){
                if (_gstNumberTextField.text.length < 15) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide valid GST Number"];
                        return NO;
                }
        }
        
        if(![Validation checkMobileNumber:mobileTxt])
                return NO;
        
        if (![Validation required:streetAddressTxt withCaption:@"Street Name"])
                return NO;
        
        if (![Validation required:pincodeTxt withCaption:@"Pin Code"])
                return NO;
        
        if(pincodeTxt.text.length != 6)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter 6 digit picode number" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                return NO;
        }
        if (![Validation required:cityTxt withCaption:@"City name"])
                return NO;
        
        if (![Validation required:countryTxt withCaption:@"Country name"])
                return NO;
        
        if (![Validation required:stateTxt withCaption:@"State name"])
                return NO;
        return YES;
}

-(void)setUpNavigationBar
{
        [self.navigationController setNavigationBarHidden:NO];
        self.navigationController.navigationBar.translucent=NO;
        UILabel *titleLabel=[[UILabel alloc]init];
        titleLabel.text = @"Add Address";
        titleLabel.font =karlaFontRegular(18.0);
        
        titleLabel.frame = CGRectMake(0, 0, 100, 30);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor whiteColor];
        self.navigationItem.titleView = titleLabel;
        //    UIColor *bgColor=[UIColor colorWithRed:22/255.0f green:70/255.0f blue:134/255.0f alpha:1.0f];
        
        UIColor *bgColor=[[AppDelegate getAppDelegateObj] colorWithHexString:APP_THEME_COLOR];
        
        self.navigationController.navigationBar.barTintColor = bgColor;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        self.navigationController.navigationBar.backItem.title =@"";
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"dropdown_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(rightMenuAction)];
        [[self navigationItem] setRightBarButtonItem:rightItem];
}

- (void)rightMenuAction {
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:@"GETREGION"]) {
                stateArray=[response valueForKey:@"region"];
        }
        else if ([strName isEqualToString:@"GETCOUNTRYLIST"]){
                countryListArray=[response valueForKey:@"countries"];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSDictionary *userDic = [defaults valueForKey:@"user"];
                if([userDic valueForKey:@"pincode"])
                {
                        [self pinCodeWSCall];
                        
                }
        }else if ([strName isEqualToString:@"PINCODE"]){
                //cityTxt.text=[[response valueForKey:@"address_data"]valueForKey:@"city"];
                stateDropdownbutton.enabled=NO;
                countryDropdownButton.enabled=NO;
                cityTxt.enabled=NO;
                
                if ([[response valueForKey:@"status"]intValue]==0) {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Pincode not found"];
                        cityTxt.text=@"";
                        stateTxt.text=@"";
                        countryTxt.text=@"";
                }else{
                        selectedCountryAtIndex=5;
                        
                        
                        NSDictionary *addressDataDic = [response valueForKey:@"address_data"];
                        
                        if([[addressDataDic valueForKey:@"city"] isKindOfClass:[NSNull class]] || [[addressDataDic valueForKey:@"default_name"] isKindOfClass:[NSNull class]] || [[addressDataDic valueForKey:@"region_id"] isKindOfClass:[NSNull class]])
                        {
                                cityTxt.text = @"";
                                stateTxt.text = @"";
                                regionId = @"";
                        }
                        else
                        {
                        cityTxt.text=[[response valueForKey:@"address_data"]valueForKey:@"city"];
                        stateTxt.text=[[response valueForKey:@"address_data"]valueForKey:@"default_name"];
                        regionId=[[response valueForKey:@"address_data"]valueForKey:@"region_id"];
                        }
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"value CONTAINS[cd] %@",[[response valueForKey:@"address_data"]valueForKey:@"country_code"]];
                        NSArray *filterdArray = [[countryListArray filteredArrayUsingPredicate:predicate] mutableCopy];
                        NSDictionary *dict=[[NSDictionary alloc]init];
                        dict=[[filterdArray objectAtIndex:0]mutableCopy];
                        selectedCountryAtIndex=[countryListArray indexOfObject:dict];
                        
                        countryTxt.text=[[filterdArray objectAtIndex:0]valueForKey:@"label"];
                        
                }
                
        }else if ([strName isEqualToString:@"CREATE_ADDRESS"])
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                [self.navigationController popViewControllerAnimated:YES];
        }
}

#pragma mark - UIActionsheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
        
        if (buttonIndex!=0)
        {
                if(actionSheet.tag==STATE_ACTIONSHEET_TAG){
                        stateTxt.text=[[stateArray objectAtIndex:buttonIndex-1]valueForKey:@"label"];
                        regionId=[[stateArray objectAtIndex:buttonIndex-1] valueForKey:@"value"];
                        selectedStateAtIndex=buttonIndex-1;
                }else if(actionSheet.tag==COUNTRY_ACTIONSHEET_TAG){
                        countryTxt.text=[[countryListArray objectAtIndex:buttonIndex-1]valueForKey:@"label"];
                        
                        if (countryListArray.count>0) {
                                [self regionRequestWithCountryId:[[countryListArray objectAtIndex:buttonIndex-1]valueForKey:@"value"]];
                                selectedCountryAtIndex=buttonIndex-1;
                        }
                }
        }
}

- (IBAction)onClickOf_State_Dropdown:(id)sender {
        [self.view endEditing:YES];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles: nil];
        
        actionSheet.tag=STATE_ACTIONSHEET_TAG;
        for (int i = 0; i < [stateArray count]; i++) {
                [actionSheet addButtonWithTitle:[[stateArray objectAtIndex:i]valueForKey:@"label"]];
        }
        
        [actionSheet showInView:self.view];
        
}

- (IBAction)onclickof_Pincode:(id)sender {
        [self.view endEditing:YES];
        if(pincodeTxt.text.length == 6)
                [self pinCodeWSCall];
        else
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter 6 digit pincode number" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
        }
}

- (IBAction)onClickOf_Country_Dropdown:(id)sender {
        [self.view endEditing:YES];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles: nil];
        
        actionSheet.tag=COUNTRY_ACTIONSHEET_TAG;
        for (int i = 0; i < [countryListArray count]; i++) {
                [actionSheet addButtonWithTitle:[[countryListArray objectAtIndex:i]valueForKey:@"label"]];
        }
        
        [actionSheet showInView:self.view];
        
}

@end
