//
//  SellGadgetTermsAndConditionViewController.m
//  EB
//
//  Created by Neosoft on 3/2/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellGadgetTermsAndConditionViewController.h"

@interface SellGadgetTermsAndConditionViewController ()

@end

@implementation SellGadgetTermsAndConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[CommonSettings sharedInstance] setShadow:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBAction method
- (IBAction)closeBtnClicked:(id)sender {
    [UIView animateWithDuration:1.5 animations:^ {
        self.view.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
   
}
@end
