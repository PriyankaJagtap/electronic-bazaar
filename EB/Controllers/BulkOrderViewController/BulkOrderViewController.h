//
//  BulkOrderViewController.h
//  EB
//
//  Created by Neosoft on 5/9/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BulkOrderViewController : UIViewController<UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)goToCartBtnClicked:(id)sender;

@end
