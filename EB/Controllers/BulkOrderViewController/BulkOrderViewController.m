//
//  BulkOrderViewController.m
//  EB
//
//  Created by Neosoft on 5/9/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "BulkOrderViewController.h"
#import "BulkProductDetailTableCell.h"
#import "BulkProductHeaderTableCell.h"
#import "ProductDetail.h"
#define ACCEPTABLE_NUMBERS @"0123456789"
#import "MyCartViewController.h"
@interface BulkOrderViewController ()<FinishLoadingData>
{
    NSMutableArray *bulkOrderData;
    NSTimer *timer;
    NSString *searchStr;
}
@end

@implementation BulkOrderViewController
@synthesize searchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _tableView.estimatedRowHeight = 155;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedSectionHeaderHeight = 40;
    _tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    
    [APP_DELEGATE.tabBarObj hideTabbar];
    [self setUpSearchBar];
       UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [gestureRecognizer setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    [self getBulkProductDetails];
    [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:4];

  
}
- (void) hideKeyboard {
    if([self.searchBar isFirstResponder])
        [self.searchBar resignFirstResponder];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpSearchBar
{
    self.searchBar.barTintColor = [UIColor blackColor];
    [self.searchBar setBackgroundImage:[[UIImage alloc] init]];
    for (UIView *subView in self.searchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                searchBarTextField.font = [UIFont fontWithName:@"Karla-Regular" size:14.0];
                //set font color here
                searchBarTextField.textColor = [UIColor blackColor];
                
                break;
            }
        }
    }
    


}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [bulkOrderData count];
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
        static NSString *simpleTableIdentifier = @"BulkProductHeaderTableCell";
        BulkProductHeaderTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[BulkProductHeaderTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        return cell.contentView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *simpleTableIdentifier = @"BulkProductDetailTableCell";
        BulkProductDetailTableCell*cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[BulkProductDetailTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
            
        }
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        ProductDetail *objProduct = [bulkOrderData objectAtIndex:indexPath.row];
    cell.productNameLabel.text = objProduct.name;
    cell.productPriceLabel.text = [[CommonSettings sharedInstance] formatPrice:[objProduct.price floatValue]];
    cell.totalPriceLabel.text = [[CommonSettings sharedInstance] formatPrice:[objProduct.totalPrice floatValue]];
    cell.quantityTextField.text = objProduct.qty;
    cell.quantityTextField.tag = indexPath.row;
    cell.addToCartBtn.tag = indexPath.row;
    cell.quantityTextField.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
    
    if(objProduct.canAddToCart)
    {
        [cell.addToCartBtn setBackgroundImage:[UIImage imageNamed:@"add_to_cart_yellow_icon"] forState:UIControlStateNormal];
        cell.addToCartBtn.userInteractionEnabled = YES;
        cell.quantityTextField.userInteractionEnabled = YES;
        [cell.addToCartBtn addTarget:self action:@selector(addToCart:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [cell.addToCartBtn setBackgroundImage:[UIImage imageNamed:@"add_to_cart_green_icon"] forState:UIControlStateNormal];
        cell.addToCartBtn.userInteractionEnabled = NO;
        cell.quantityTextField.userInteractionEnabled = NO;
    }
    return cell;
       
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark - TextField delegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    CGPoint pointInTable = [textField.superview.superview.superview convertPoint:textField.frame.origin toView:self.tableView];
    CGPoint contentOffset = self.tableView.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    
    NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    
    [self.tableView setContentOffset:contentOffset animated:YES];
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    ProductDetail *objProductDetail = [bulkOrderData objectAtIndex:textField.tag];
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
//    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_NUMBERS] invertedSet];
//    
//    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    if ([newString intValue] <= objProductDetail.availableQty)
    {
        return YES;

    }
    else
    {
//        [[CommonSettings sharedInstance] displayCustomPopup:@"Maximum Quantity Alert" subTitle:[NSString stringWithFormat:@"Maximum available quantity for this product is %d. \n You can't enter qty more than available qty.",objProductDetail.availableQty]];
//        
//        NSString *message = @"Some message...";
        
        NSString *message = [NSString stringWithFormat:@"Only %d left",objProductDetail.availableQty];
//        UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
//                                                        message:[NSString stringWithFormat:@"Maximum available quantity for this product is %d. \n You can't enter qty more than available qty.",objProductDetail.availableQty]
//                                                       delegate:nil
//                                              cancelButtonTitle:nil
//                                              otherButtonTitles:nil, nil];
        
        UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:nil, nil];
        [toast show];
        
        int duration = 1; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [toast dismissWithClickedButtonIndex:0 animated:YES];
        });
        return NO;
    }
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    ProductDetail *objProductDetail = [bulkOrderData objectAtIndex:textField.tag];
    float totalPrice;
    if([textField.text intValue] == 0)
        totalPrice = [objProductDetail.price floatValue];
    else
        totalPrice = [objProductDetail.price floatValue] * [textField.text intValue];
    objProductDetail.totalPrice = [NSString stringWithFormat:@"%f",totalPrice];
    
    objProductDetail.qty =[textField.text intValue] ? [NSString stringWithFormat:@"%d",[textField.text intValue]] : @"1";
    
    [self reloadTableWithData:textField.tag objProductDetail:objProductDetail];
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - IBAction methods
- (IBAction)backBtnClicked:(id)sender
{

    [APP_DELEGATE.tabBarObj unhideTabbar];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addToCart:(id)sender
{
    [self.view endEditing:YES];
    UIButton *btn = (UIButton *)sender;
    ProductDetail *objProductDetail = [bulkOrderData objectAtIndex:btn.tag];
    objProductDetail.canAddToCart = NO;
    [self reloadTableWithData:btn.tag objProductDetail:objProductDetail];
    [self addBulkOfProductsToCart:objProductDetail];
    
}
-(void)doneButtonDidPressed:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view endEditing:YES];
        
    });
}
- (IBAction)goToCartBtnClicked:(id)sender {
    [APP_DELEGATE.tabBarObj unhideTabbar];
    [APP_DELEGATE.tabBarObj TabClickedIndex:2];
}

#pragma mark - add product to cart
-(void)addBulkOfProductsToCart:(ProductDetail *)objProduct
{
    Webservice  *callAddToCartService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
        
        callAddToCartService.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
       
        NSString *quotId;
        if (quoteID==0) {
            quotId=@"";
        }else{
            quotId=[NSString stringWithFormat:@"%d",quoteID];
        }
        
        NSDictionary *dictAddToCart=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:objProduct.productId],@"ProductID",quotId,@"QuoteID",[NSNumber numberWithInt:[objProduct.qty intValue]],@"Qty", nil];
        [callAddToCartService operationRequestToApi:dictAddToCart url:ADD_BULK_PRODUCTS_CART_WS string:ADD_BULK_PRODUCTS_CART_WS];
    }
    
    
    //[self addPRoductDetailsToCart:dictProdDetail];
    
}
#pragma mark - custom method

-(void)reloadTableWithData:(NSInteger)index objProductDetail:(ProductDetail *)objProductDetail
{
    [bulkOrderData replaceObjectAtIndex:index withObject:objProductDetail];
    [_tableView beginUpdates];
    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:index inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}
#pragma mark - search bar delegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self hideKeyboard];

}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if(searchText.length<=0) {
        [self performSelector:@selector(hideKeyboard) withObject:self.searchBar afterDelay:0.2];
    }
    
  
    [timer invalidate];
    NSDictionary *userInfoDic = [[NSDictionary alloc] initWithObjectsAndKeys:searchText,@"SearchText", nil];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(getSearchProducts:) userInfo:userInfoDic repeats:NO];
}
#pragma mark - call search product webservice
-(void)getSearchProducts:(NSTimer *)searchTimer
{
    NSDictionary *dic = [searchTimer userInfo];
    NSLog(@"search text %@",[dic valueForKey:@"SearchText"]);
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        NSString *urlStr = [NSString stringWithFormat:@"%@%@",BULK_PRODUCT_SEARCH_WS,[dic valueForKey:@"SearchText"]];
        [callLoginService GetWebServiceWithURL:urlStr MathodName:BULK_PRODUCT_SEARCH_WS];
        
        
    }
}
#pragma mark - get model data
-(void)getBulkProductDetails
{
    
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:BULK_PRODUCT_WS MathodName:BULK_PRODUCT_WS];
    }
}

#pragma mark - request finish method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:BULK_PRODUCT_WS] || [strName isEqualToString:BULK_PRODUCT_SEARCH_WS]){
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            [self setTableData:[response valueForKey:@"data"]];
        }
            else
                     [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
    }
    else if ([strName isEqualToString:ADD_BULK_PRODUCTS_CART_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            int quoteID=[[response valueForKey:@"quoteId"]intValue];
            [defaults setObject:[NSNumber numberWithInt:quoteID] forKey:@"QuoteID"];
            [defaults synchronize];
            [CommonSettings incrementMyCartCountByAmount:[[response valueForKey:@"qty"] intValue]];
            
        }
        
        [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
        
    }
    
}


-(void)setTableData:(NSArray *)responseArr
{
    
    if(bulkOrderData == nil)
        bulkOrderData = [NSMutableArray new];
    else
        [bulkOrderData removeAllObjects];

    
    NSArray *arr = [responseArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(qty != %@)",@"0.0000"]];
    
    for (NSDictionary *dic in arr) {
        ProductDetail *obj = [ProductDetail new];
        obj.name = [dic valueForKey:@"name"];
        obj.price =  [dic valueForKey:@"price"];
        obj.totalPrice =  [dic valueForKey:@"price"];
        obj.productId = [[dic valueForKey:@"entity_id"] intValue];
        obj.qty = @"1" ;
        obj.canAddToCart = YES;
        obj.availableQty = [[dic valueForKey:@"qty"] intValue];
        [bulkOrderData addObject:obj];
        
    }
    [_tableView reloadData];
}



@end
