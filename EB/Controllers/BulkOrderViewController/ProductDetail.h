//
//  ProductDetail.h
//  EB
//
//  Created by Neosoft on 5/9/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductDetail : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *price;

@property (nonatomic, strong) NSString *totalPrice;
@property (nonatomic, strong) NSString * qty;
@property (nonatomic) bool canAddToCart;
@property (nonatomic) int availableQty;
@property (nonatomic) int productId;


@end
