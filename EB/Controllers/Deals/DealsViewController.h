//
//  DealsViewController.h
//  EB
//
//  Created by webwerks on 8/27/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"
@interface DealsViewController : UIViewController<FinishLoadingData,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *productListArr;
    UIView *noDealsAvailableView;
}
@property (weak, nonatomic) IBOutlet UITableView *dealsTableView;
@property (weak, nonatomic) IBOutlet UILabel *remaining_Time_Lbl;
@property (strong, nonatomic)UINavigationController *navController;


- (IBAction)onClickOfBack_Button:(id)sender;
- (IBAction)rightSlideMenuAction:(id)sender;


@end
