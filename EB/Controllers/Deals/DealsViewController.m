//
//  DealsViewController.m
//  EB
//
//  Created by webwerks on 8/27/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "DealsViewController.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "DealsCell.h"
#import "FVCustomAlertView.h"
#import "ProductDetailsViewController.h"
#import "CommonSettings.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"



@interface DealsViewController ()
{
    NSDate *newDate;
    NSTimer *timer;
    NSDate *frmDate;
    NSDate *toDate;
}

@end

@implementation DealsViewController
@synthesize dealsTableView,remaining_Time_Lbl,navController;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.dealsTableView.contentInset = UIEdgeInsetsMake(-33, 0, 50, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated
}
//viewDidAppear viewWillAppear
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES];
    [CommonSettings sendScreenName:@"DealsView"];
    

    dispatch_async(dispatch_get_main_queue(), ^{
        [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:3];
    });

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Deals"];
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [noDealsAvailableView removeFromSuperview];
    NSMutableDictionary *dictData = [NSMutableDictionary new];
    [dictData setObject:@"hotdeals" forKey:@"type"];
    [dictData setObject:[NSNumber numberWithInt:1] forKey:@"page_no"];
    [dictData setObject:[NSNumber numberWithInt:12] forKey:@"page_size"];

    [self callViewHotDealsProductWS:dictData];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [timer invalidate];
}

-(void)updateLabelFromDate:(NSTimer*)timer1
{
    if (!frmDate) {
        NSDictionary *dict = [timer1 userInfo];
        frmDate=[dict valueForKey:@"Current_Date"];
        toDate=[dict valueForKey:@"Expiry_Date"];
    }
    
    switch ([frmDate compare:toDate]){
        case NSOrderedAscending:
            break;
        case NSOrderedSame:
            [self addNodealsAvailableView];
            [timer invalidate];
            break;
        case NSOrderedDescending:
            [self addNodealsAvailableView];
            [timer invalidate];
            break;
    }

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    toDate=[toDate initWithTimeInterval:-1 sinceDate:toDate];
    
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar]components:NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:frmDate toDate:toDate options:0];
    
    //NSLog(@"%02ld:%02ld:%02ld:%02ld",(long)[dateComponents day],(long)dateComponents.hour,(long)dateComponents.minute,(long)dateComponents.second);
    NSString *strTime=[NSString stringWithFormat:@"%02ld  :  %02ld  :  %02ld  :  %02ld",(long)[dateComponents day],(long)dateComponents.hour,(long)dateComponents.minute,(long)dateComponents.second];
    remaining_Time_Lbl.text=strTime;

}

#pragma mark - UITableview Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [productListArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"DealsCell";
    DealsCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    //discount
    cell.prod_Discount.text =[NSString stringWithFormat:@"%@",[[productListArr objectAtIndex:indexPath.row]objectForKey:@"discount"]];
    
    //name
    cell.product_Name.text = [NSString stringWithFormat:@"%@",[[productListArr objectAtIndex:indexPath.row]objectForKey:@"name"]];
    
    
    //Price
    NSString *strSpecialPrice=[NSString stringWithFormat:@"Rs %@",[[productListArr objectAtIndex:indexPath.row]valueForKey:@"special_price"]];
    NSString *strPrice=[NSString stringWithFormat:@"Rs %@",[[productListArr objectAtIndex:indexPath.row]valueForKey:@"price"]];

    cell.prod_Prev_Price.text = [NSString stringWithFormat:@"Rs %ld",(long)[[[productListArr objectAtIndex:indexPath.row]objectForKey:@"price"] integerValue]];
    if ([NSNull null]==[[productListArr objectAtIndex:indexPath.row]valueForKey:@"special_price"]){
        cell.prod_Prev_Price.text=@"";
        cell.prod_Price.text=strPrice;
    }else{
        cell.prod_Price.text=[NSString stringWithFormat:@" %@",strSpecialPrice];
        cell.prod_Prev_Price.text=[NSString stringWithFormat:@"%@",strPrice];
    }
    
    // image
    NSString*imageLink = [NSString stringWithFormat:@"%@",[[productListArr objectAtIndex:indexPath.row]objectForKey:@"image"]];
    //[cell.product_Img setImageURL:[NSURL URLWithString:imageLink]];
    [cell.product_Img sd_setImageWithURL:[NSURL URLWithString:imageLink]];
    return cell;
    
}

#pragma mark - UITableview delegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 68;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    int prodId = [[[productListArr valueForKey:@"product_id"]objectAtIndex:indexPath.row]intValue];
    objProductDetailVC.productID=prodId;
    
    [self.navigationController pushViewController:objProductDetailVC animated:YES];

}

-(void)callViewHotDealsProductWS:(NSMutableDictionary *)dictdata
{
    Webservice  *callhotDealsWs = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];

        callhotDealsWs.delegate =self;
        [callhotDealsWs operationRequestToApi:dictdata url:VIEWALL_WS string:@"ViewAllWs"];
    }
    
}

#pragma mark - Webservice delegate

-(void)receivedResponseForViewAllProducts:(id)receiveData stringResponse:(NSString *)responseType{
    
    if ([responseType isEqualToString:@"success"]) {
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        
        NSString *status = [NSString stringWithFormat:@"%ld",(long)[[jsonObject valueForKey:@"status"]integerValue]];
        if ([status isEqualToString:@"1"])
        {
            if ([NSNull null]==[jsonObject valueForKey:@"count"]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self addNodealsAvailableView];
                });
            }else{
                NSMutableArray *dataArr=[jsonObject valueForKey:@"data"];
                NSLocale* formatterLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setLocale:formatterLocale];
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *currentDate=[dateFormatter dateFromString:[dataArr valueForKey:@"current_date"]];
                NSDate *expiryDate=[dateFormatter dateFromString:[dataArr valueForKey:@"expiry_date"]];
                
                NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
                [dictData setObject:currentDate forKey:@"Current_Date"];
                [dictData setObject:expiryDate forKey:@"Expiry_Date"];
                
                
                productListArr = [NSMutableArray new];
                productListArr = [dataArr valueForKey:@"product_collection"];
                
                newDate=[NSDate date];
                timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateLabelFromDate:) userInfo:dictData repeats:YES];
                [dealsTableView reloadData];

            }
        }
        else{
            
        }
        
    }
}

-(void)addNodealsAvailableView
{
    noDealsAvailableView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-200)];
    UILabel *emptyDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 200, 40)];
    emptyDataLabel.text = @"No Deals Available";
    emptyDataLabel.textAlignment=NSTextAlignmentCenter;
    emptyDataLabel.alpha = 0.5;
    emptyDataLabel.center = noDealsAvailableView.center;
    emptyDataLabel.textColor = [UIColor blackColor];
    emptyDataLabel.backgroundColor = [UIColor clearColor];
    emptyDataLabel.textAlignment = NSTextAlignmentCenter;
    [noDealsAvailableView setBackgroundColor:[UIColor whiteColor]];
    [noDealsAvailableView addSubview:emptyDataLabel];
    [self.view addSubview:noDealsAvailableView];

}

- (IBAction)onClickOfBack_Button:(id)sender{
    [self leftBarButtonItemPressed];
}
- (IBAction)rightSlideMenuAction:(id)sender
{
    [self rightBarButtonItemPressed];
}

-(void)leftBarButtonItemPressed{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

-(void)rightBarButtonItemPressed
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

@end
