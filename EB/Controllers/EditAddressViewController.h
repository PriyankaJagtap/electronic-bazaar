//
//  EditAddressViewController.h
//  EB
//
//  Created by webwerks on 9/11/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditAddressViewController : BaseNavigationControllerWithBackBtn<UIActionSheetDelegate>
{
    NSMutableArray *countryListArray;
    NSMutableArray *stateArray;
    
    
    __weak IBOutlet UIButton *countryDropdownButton;
    
    __weak IBOutlet UIButton *stateDropdownbutton;
}

/*
 stateDropdownbutton
 countryDropdownButton
 */
@property (weak, nonatomic) IBOutlet UITextField *storeNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *mobileTxt;
@property (weak, nonatomic) IBOutlet UITextField *streetAddressTxt;
@property (weak, nonatomic) IBOutlet UITextField *streetAddress2Txt;
@property (weak, nonatomic) IBOutlet UITextField *pincodeTxt;
@property (weak, nonatomic) IBOutlet UITextField *countryTxt;
@property (weak, nonatomic) IBOutlet UITextField *cityTxt;
@property (weak, nonatomic) IBOutlet UITextField *stateTxt;
@property (weak, nonatomic) IBOutlet UITextField *gstNumberTextField;

- (IBAction)onClickOfUse_As_Default_Billing_Add:(id)sender;
- (IBAction)onClickOfUse_As_Default_Shipping_Add:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *telephoneTxt;

- (IBAction)onClickOf_State_Dropdown:(id)sender;
- (IBAction)onClickOf_Country_Dropdown:(id)sender;

- (IBAction)saveAction:(id)sender;
- (IBAction)onclickof_Pincode:(id)sender;

@property (weak,nonatomic) IBOutlet UIButton *saveAddress;
@end
