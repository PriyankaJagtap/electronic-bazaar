//
//  VowDelightOrderDetailTableCell.h
//  EB
//
//  Created by webwerks on 20/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VowDelightOrderDetailTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *imeiLabel;
@property (weak, nonatomic) IBOutlet UILabel *reasonLabel;

@property (weak, nonatomic) IBOutlet UILabel *commentsValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentSeparatorLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsLabel;
@property (weak, nonatomic) IBOutlet UIButton *removeOrderBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end
