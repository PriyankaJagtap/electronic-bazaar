//
//  VowDelightViewController.m
//  EB
//
//  Created by webwerks on 19/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "VowDelightViewController.h"
#import "NIDropDown.h"
#import "VowDelightOrderDetailTableCell.h"
#import "VowDelightSuccessViewController.h"
#import "SGInfoWebViewController.h"


@interface VowDelightViewController ()<FinishLoadingData,NIDropDownDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
        NSArray *returnDataArr;
        NSArray *reasonDataArr;
        NIDropDown *selectReasonDropDown;
        NSMutableArray *orderDataArr;

}

@end

@implementation VowDelightViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        [super setViewControllerTitle:@"Vow Delight"];
        [self getReasonData];
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_addBtn];
        _tableView.estimatedRowHeight = 120;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        orderDataArr = [NSMutableArray new];
}

-(void)viewDidAppear:(BOOL)animated
{
        [super viewDidAppear:animated];
        dispatch_async(dispatch_get_main_queue(), ^{
                [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:4];
        });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource and Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        if (orderDataArr.count != 0) {
                return orderDataArr.count +1 ;
        }
        else
                return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if(indexPath.row == orderDataArr.count)
        {
                static NSString *simpleTableIdentifier = @"VowDelightOrderDetailTableCellWithSubmitBtn";
                VowDelightOrderDetailTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil) {
                        cell = [[VowDelightOrderDetailTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                }
                [[GradientColorClass sharedInstance] setGradientBackgroundToButton:cell.submitBtn];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                
                return cell;
        }
        else
        {
                static NSString *simpleTableIdentifier = @"VowDelightOrderDetailTableCell";
                VowDelightOrderDetailTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil) {
                        cell = [[VowDelightOrderDetailTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.removeOrderBtn.tag = indexPath.row;
                NSDictionary *dic = [orderDataArr objectAtIndex:indexPath.row];
                
                cell.orderNumberLabel.text = [dic valueForKey:@"order_no"];
                cell.imeiLabel.text = [dic valueForKey:@"imei"];
                cell.reasonLabel.text = [dic valueForKey:@"reason"];

                if(![[dic valueForKey:@"comments"] isEqualToString:@""])
                {
                        cell.commentsLabel.text = @"Comments";
                        cell.commentSeparatorLabel.text = @":";
                        cell.commentsValueLabel.text = [dic valueForKey:@"comments"];
                }
                else
                {
                        cell.commentsLabel.text = @"";
                        cell.commentSeparatorLabel.text = @"";
                        cell.commentsValueLabel.text = @"";
                }
                return cell;
        }
        
}

#pragma mark - TextField Delegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        
        if(textField == _imeiTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_IMEI_NUMBER] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                if(newLength <= 15)
                {
                        return [string isEqualToString:filtered];
                }
                
                if(newLength > 15){
                        
                        return NO;
                }
        }
        
        return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [textField resignFirstResponder];
        return  YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
}
#pragma mark - Custom methods
-(void)getReasonData
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:RETURN_REASONS_WS MathodName:RETURN_REASONS_WS];
                
        }
}

-(void)callValidateIMEINumberWebservice
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                NSString *url = [NSString stringWithFormat:@"%@%@&&order_no=%@",VALIDATE_IMEI_WS,_imeiTextField.text,_orderNoTextField.text];
                [callLoginService GetWebServiceWithURL:url MathodName:VALIDATE_IMEI_WS];
                
        }
}


-(void)callSaveRequestWebservice
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                NSError * error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:orderDataArr options:NSJSONWritingPrettyPrinted error:&error];
                NSString *orderDetailsString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
                NSString *url = [NSString stringWithFormat:@"%@%@&user_id=%@",SAVE_REQUEST_WS,orderDetailsString,userId];
                [callLoginService GetWebServiceWithURL:url MathodName:SAVE_REQUEST_WS];
                
        }
}


#pragma mark - IBAction methods

- (IBAction)imeiInfoBtnClicked:(id)sender {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SGInfoWebViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"SGInfoWebViewController"];
        
    viewController.webViewUrl = SYG_IMEI_INFO_CONTENT; //@"http://electronicsbazaar.com/warranty-app/popup-mobile.html";
        [self addChildViewController:viewController];
        viewController.view.frame = self.view.frame;
        [self.view addSubview:viewController.view];
        viewController.view.alpha = 0;
        [viewController didMoveToParentViewController:self];
        
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^
         {
                 viewController.view.alpha = 1;
         }
                         completion:nil];
        
}

- (IBAction)removeOrderBtnClicked:(id)sender {
        
        UIButton *btn = (UIButton *)sender;
        if(orderDataArr.count != 0)
        {
                [orderDataArr removeObjectAtIndex:btn.tag];
                [_tableView reloadData];
        }
}
- (IBAction)submitBtnClicked:(id)sender {
      if(orderDataArr.count != 0)
      {
              [self callSaveRequestWebservice];
      }
}
- (IBAction)selectReasonBtnClicked:(id)sender {
        
        NSArray * arr = [reasonDataArr valueForKey:@"label"];
      
        if( selectReasonDropDown == nil){
                CGFloat f= 100.0;
                if(arr.count <= 3)
                        f = 30 * arr.count;
                selectReasonDropDown = [[NIDropDown alloc] showDropDown:_selectReasonBtn withHeight:&f withData:arr withImages:Nil WithDirection:@"down"];
                
                selectReasonDropDown.displayDropDownBool = YES;
                selectReasonDropDown.delegate = self;
                
        }
        else{
                [selectReasonDropDown hideDropDown:sender];
                [self rel];
        }
}
- (IBAction)addBtnClicked:(UIButton *)sender {
        [self.view endEditing:NO];
        
     
        
        if(_imeiTextField.text.length < 15)
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter valid IMEI"];
                return;
        }
        
        NSArray *checkIMEIExistArr  = [orderDataArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(imei == %@)",_imeiTextField.text]];
        
        if(checkIMEIExistArr.count > 0)
        {
                [[AppDelegate getAppDelegateObj] showAlert:@"" withMessage:@"This IMEI Number already present in the list"];
        }
      
        
        
        if(_orderNoTextField.text.length == 0)
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter Order No."];
                return;
        }
        else{
                [self callValidateIMEINumberWebservice];
        }
        //        NSArray *checkOrderNoExistArr  = [orderDataArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(order_no == %@)",_orderNoTextField.text]];
        //
        //        if(checkOrderNoExistArr.count > 0)
        //        {
        //                [[AppDelegate getAppDelegateObj] showAlert:@"" withMessage:@"This Order Number already present in the list"];
        //                return;
        //        }
        
}



#pragma mark - NIdropDown methods

- (void)rel
{
     selectReasonDropDown = nil;
}

- (void)niDropDownDelegateMethod:(NIDropDown *) sender
{
      [self rel];
        _selectReasonBtnHeightConstraint.constant = [[CommonSettings sharedInstance] calculateBtnHeight:_selectReasonBtn] +20;
        
}

- (void) getindexValue: (NIDropDown *)sender withindex:(NSIndexPath*)index{
        DLog(@"index %ld",(long)index.row);
}



#pragma mark - webservice response method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:RETURN_REASONS_WS]){
                //NSLog(@"%@",response);
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        returnDataArr = [response valueForKey:@"return"];
                        reasonDataArr = [response valueForKey:@"reason"];
                        [_selectReasonBtn setTitle:[[reasonDataArr objectAtIndex:0] valueForKey:@"label"] forState:UIControlStateNormal];
                        _selectReasonBtnHeightConstraint.constant = [[CommonSettings sharedInstance] calculateBtnHeight:_selectReasonBtn] + 20;

                }
        }
        else if ([strName isEqualToString:VALIDATE_IMEI_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        
                        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:_commentsTextField.text,@"comments",_imeiTextField.text,@"imei",_orderNoTextField.text,@"order_no",
                                             [_selectReasonBtn titleForState:UIControlStateNormal],@"reason", nil];
                        [orderDataArr addObject:dic];
                        _commentsTextField.text = @"";
                        _imeiTextField.text = @"";
                        _orderNoTextField.text = @"";
                        [_tableView reloadData];
                }
                else
                {
                        [[CommonSettings sharedInstance] showAlertTitle:@"" message:[response valueForKey:@"message"]];
                        _imeiTextField.text = @"";
                        _orderNoTextField.text = @"";

                }
        }
        else if ([strName isEqualToString:SAVE_REQUEST_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
                        VowDelightSuccessViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"VowDelightSuccessViewController"];
                      
                        vc.orderNumber = [NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] valueForKey:@"increment_id"]];
                         vc.awbNumber = [NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] valueForKey:@"awb_number"]];
                         vc.entityId = [NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] valueForKey:@"entity_id"]];
                        vc.message = [response valueForKey:@"message"];
                        [self.navigationController pushViewController:vc animated:YES];
                }
                else
                {
                        [[CommonSettings sharedInstance] showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                
        }
}
@end
