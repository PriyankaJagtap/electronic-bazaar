//
//  SalesReturnTermsTableCell.h
//  EB
//
//  Created by webwerks on 11/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesReturnTermsTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;

@end
