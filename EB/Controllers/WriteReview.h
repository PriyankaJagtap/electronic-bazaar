//
//  WriteReview.h
//  EB
//
//  Created by webwerks on 9/9/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
@interface WriteReview : BaseNavigationControllerWithBackBtn<UITextFieldDelegate,UITextViewDelegate,StarRatingDelegate>

{
    __weak IBOutlet ASStarRatingView *starRatingView;
    __weak IBOutlet UITextField *summary_Txt;
}
@property (weak, nonatomic) IBOutlet UITextView *write_Review_Txt;
@property (weak, nonatomic) IBOutlet UIButton *submitReviewBtn;
@property(strong,nonatomic)NSString *prodName;
@property(nonatomic)int prodID;
@property (weak, nonatomic) IBOutlet UILabel *product_Name_Lbl;
- (IBAction)onClickOf_Submit:(id)sender;
- (IBAction)leftButtonAction:(id)sender;
- (IBAction)rightSlideMenuAction:(id)sender;


@end
