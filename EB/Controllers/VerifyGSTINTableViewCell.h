//
//  VerifyGSTINTableViewCell.h
//  EB
//
//  Created by webwerks on 04/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyGSTINTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *partnerStoreNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *propNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLine1Label;
@property (weak, nonatomic) IBOutlet UILabel *addressLine2Label;
@property (weak, nonatomic) IBOutlet UILabel *postCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *panLabel;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
