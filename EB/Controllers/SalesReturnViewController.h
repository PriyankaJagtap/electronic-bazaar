//
//  SalesReturnViewController.h
//  EB
//
//  Created by webwerks on 03/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesReturnProductClass.h"


@interface SalesReturnViewController : BaseNavigationControllerWithBackBtn
@property (nonatomic, strong) SalesReturnProductClass *objSalesReturn;
- (IBAction)actionBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end
