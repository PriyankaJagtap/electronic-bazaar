//
//  SGMobileDetailsViewController.m
//  EB
//
//  Created by webwerks on 11/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SGMobileDetailsViewController.h"
#import "SellYourMobileViewController.h"
#import "SellGadgetCheckOutViewController.h"

@interface SGMobileDetailsViewController ()

@end

@implementation SGMobileDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        [super setViewControllerTitle:@"Your Device"];
    // Do any additional setup after loading the view.
           [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_submitBtn];
        [self setInitialValues];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - set initial values
-(void)setInitialValues
{
        _brandLabel.text = [_mobileDetailsDic valueForKey:BRAND];
        _modelLabel.text = [_mobileDetailsDic valueForKey:MODEL];
        //_memoryLabel.text = [_mobileDetailsDic valueForKey:MEMORY];
        _conditionLabel.text = [_mobileDetailsDic valueForKey:CONDITION];
        _IMEILabel.text = [_mobileDetailsDic valueForKey:IMEI_NO];
        _priceLabel.text = [[CommonSettings sharedInstance] formatIntegerPrice:[[_mobileDetailsDic valueForKey:PRICE] integerValue]];
        _chargerLabel.text = [_mobileDetailsDic valueForKey:CHARGER];
}



#pragma mark - IBACtion methods
- (IBAction)backBtnClicked:(id)sender {
        
        NSArray *viewControllers = [self.navigationController viewControllers];
        for (UIViewController *vc in viewControllers) {
                if([vc isKindOfClass:[SellYourMobileViewController class]])
                {
                        [self.navigationController popToViewController:vc animated:YES];
                }
        }
}
- (IBAction)rightMenuClicked:(id)sender {
         [CommonSettings displaySellYourGadgetRightMenu:self];
}

- (IBAction)sellNowBtnClicked:(id)sender {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SellGadgetCheckOutViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"SellGadgetCheckOutViewController"];
        objVc.mobileDataDic = _mobileDetailsDic;
        objVc.productType = @"Mobile";
        [self.navigationController pushViewController:objVc animated:YES];
}

@end
