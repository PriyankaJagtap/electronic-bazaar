//
//  SGInfoWebViewController.m
//  EB
//
//  Created by webwerks on 11/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SGInfoWebViewController.h"

@interface SGInfoWebViewController ()

@end

@implementation SGInfoWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        [[CommonSettings sharedInstance] displayLoading];
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_webViewUrl]] ];
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        [self.view addGestureRecognizer:tap];
}

-(void)viewTapped:(UITapGestureRecognizer *)gesture
{
        [self removeViewFromParent];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - web view delegate methods
-(void)webViewDidFinishLoad:(UIWebView *)webView {
        [[CommonSettings sharedInstance] removeLoading];
        
        NSLog(@"finish");
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
        [[CommonSettings sharedInstance] removeLoading];
        
        NSLog(@"Error for WEBVIEW: %@", [error description]);
}


//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
//{
//       
//        return YES;
//}

#pragma mark - IBAction methods
- (IBAction)closeBtnClicked:(id)sender {
        [self removeViewFromParent];
}

-(void)removeViewFromParent
{
        [self willMoveToParentViewController:nil];
        [self removeFromParentViewController];
        [self.view removeFromSuperview];
}
@end
