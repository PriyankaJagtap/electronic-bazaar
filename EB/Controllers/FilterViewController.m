//
//  FilterViewController.m
//  EB
//
//  Created by webwerks on 8/25/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "FilterViewController.h"
#import "ElectronicsViewController.h"
#import "OptionsCell.h"
#import "Constant.h"
#import "AppDelegate.h"

@interface FilterViewController(){
    NSMutableArray *optionsArr;
    //NSMutableArray *selectedOptionArr;
    UILabel *emptyDataLabel;
    
    NSString *filterCodeStr;
}

@end

@implementation FilterViewController
@synthesize delegate;
@synthesize arrFilter;
@synthesize finalFilterDict,selectedOptionArr;


- (void)viewDidLoad {
    [super viewDidLoad];
    //optionsArr = [NSMutableArray new];
        [super setViewControllerTitle:@"Filter"];
        
    if (optionsArr.count == 0) {
        
    }
    NSLog(@" Dict %@",finalFilterDict);
    NSLog(@" Arr %@",selectedOptionArr);
    
//objSearchbar.backgroundColor = [[AppDelegate getAppDelegateObj] colorWithHexString:@"192C64"]
    _tblView.backgroundColor = [[AppDelegate getAppDelegateObj] colorWithHexString:@"ECECEC"];
}








-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //finalFilterDict = [[NSMutableDictionary alloc]init];
    filterCodeStr= @"";
    if (arrFilter.count>0) {
        
        [_tblView selectRowAtIndexPath:0 animated:YES scrollPosition:UITableViewScrollPositionTop];

      //  selectedOptionArr = [[NSMutableArray alloc]init];
        optionsArr = [[[arrFilter objectAtIndex:0]objectForKey:@"options"]mutableCopy];
        if (optionsArr.count > 0) {
            filterCodeStr = [NSString stringWithFormat:@"%@",[[[arrFilter objectAtIndex:0]objectForKey:@"code"]mutableCopy]];
            [_collectionView reloadData];//..
        }

    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




#pragma mark - UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrFilter count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    //if (isIpad) {
       // cell.textLabel.font =[UIFont fontWithName:@"Karla-Regular" size:15.0];

    //}else{
        cell.textLabel.font =[UIFont fontWithName:@"Karla-Regular" size:12.0];
    //}
    cell.textLabel.textColor =[UIColor blackColor];
    //_tblView.backgroundColor = [[AppDelegate getAppDelegateObj] colorWithHexString:@"ECECEC"];
    cell.backgroundColor = [[AppDelegate getAppDelegateObj] colorWithHexString:@"ECECEC"];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text =[[arrFilter objectAtIndex:indexPath.row]objectForKey:@"title"];
      //  cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
//    UIView *bgColorView = [[UIView alloc] init];
//    bgColorView.backgroundColor = [UIColor darkGrayColor];
//    [cell setSelectedBackgroundView:bgColorView];

    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0;
}




-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.contentView.backgroundColor = [[AppDelegate getAppDelegateObj] colorWithHexString:@"ECECEC"];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

     
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor darkGrayColor];
        //selectedOptionArr = [NSMutableArray new];
    optionsArr = [[[arrFilter objectAtIndex:indexPath.row]objectForKey:@"options"]mutableCopy];
    if (optionsArr.count > 0) {
        emptyDataLabel.hidden=YES;
        filterCodeStr = [NSString stringWithFormat:@"%@",[[[arrFilter objectAtIndex:indexPath.row]objectForKey:@"code"]mutableCopy]];
        [_collectionView reloadData];
    }else{
        [optionsArr removeAllObjects];
        [_collectionView reloadData];
    }
    
}


#pragma mark - UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return optionsArr.count;

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"OptionsCell";
    
    OptionsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    //if (isIpad) {
       // cell.optionTitle.font =[UIFont fontWithName:@"Karla-Regular" size:14.0];
    //}else{
        cell.optionTitle.font =[UIFont fontWithName:@"Karla-Regular" size:10.0];
    //}
    cell.optionTitle.textColor =[UIColor darkGrayColor];
    cell.backgroundColor = [ UIColor whiteColor];
    cell.optionTitle.numberOfLines = 0;
    NSString *str = [NSString stringWithFormat:@"%@ (%@)",[[[optionsArr objectAtIndex:indexPath.row]objectForKey:@"label"]mutableCopy], [[[optionsArr objectAtIndex:indexPath.row]objectForKey:@"count"]mutableCopy]];
    cell.optionTitle.text = str;
    cell.optionSelected.tag = indexPath.row;
    [cell.optionSelected addTarget:self action:@selector(checkOptionSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[selectedOptionArr valueForKey:@"label"] containsObject:[[optionsArr objectAtIndex:indexPath.row]valueForKey:@"label"]]) {
        cell.optionSelected.selected = YES;
    }
    else{
        cell.optionSelected.selected = NO;
    }
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionView.frame.size.width, 50);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 3.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section;
{
    return UIEdgeInsetsMake(0, 1, 0, 1);
}

#pragma mark - back button

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
//valueForKey:@"label"
-(void)checkOptionSelected:(UIButton *)sender{
    DLog(@"%@",filterCodeStr);
    NSMutableDictionary *dict = [NSMutableDictionary new];
    dict = [[optionsArr objectAtIndex:[sender tag]]mutableCopy];
    if (![[selectedOptionArr valueForKey:@"label"] containsObject:[dict valueForKey:@"label"]]) {
        [selectedOptionArr addObject:dict];
        sender.selected = YES;
        
        if (![finalFilterDict objectForKey:filterCodeStr]){
          
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[[finalFilterDict objectForKey:filterCodeStr]mutableCopy]];
            [tempArr addObject:dict];
            [finalFilterDict setObject:tempArr forKey:filterCodeStr];
            
            
            //  [finalFilterDict setObject:selectedOptionArr forKey:filterCodeStr];
        }
        else{
            DLog(@"dict%@", dict);
            
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[finalFilterDict objectForKey:filterCodeStr]];
            [tempArr addObject:dict];
            [finalFilterDict setObject:tempArr forKey:filterCodeStr];
            
        }
        DLog(@"finalFilterDict%@", finalFilterDict);
    }
    else{
        [selectedOptionArr removeObject:dict];
            if ([finalFilterDict objectForKey:filterCodeStr])
            {
                //label indexofobject
//                int index=[[[finalFilterDict objectForKey:filterCodeStr]valueForKey:@"label"]indexOfObject:[dict
//                                                                                                 valueForKey:@"label"]];
//                [[finalFilterDict objectForKey:filterCodeStr]removeObjectAtIndex:index];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"label CONTAINS[cd] %@",[dict valueForKey:@"label"]];
                
               NSArray *filterdArray = [[[finalFilterDict objectForKey:filterCodeStr] filteredArrayUsingPredicate:predicate] mutableCopy];
                for (NSDictionary *dict in filterdArray) {
                    [[finalFilterDict objectForKey:filterCodeStr]removeObject:dict];
                    [selectedOptionArr removeObject:dict];
                }
                
                
                //[[finalFilterDict objectForKey:filterCodeStr] removeObject:dict];
            }
            else{
            }
        sender.selected = NO;
    }
    [_collectionView reloadData];
}

- (IBAction)clearButtonAction:(id)sender{
    [finalFilterDict removeAllObjects];
    [selectedOptionArr removeAllObjects];
    [self.collectionView reloadData];
    //[self.tblView reloadData];
}


- (IBAction)rightSlideAction:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}



- (IBAction)doneFilterAction:(id)sender {
    DLog(@"%@",finalFilterDict);

    //[self.delegate getFilterParams:finalFilterDict];
    [self.delegate getFilterParams:[finalFilterDict mutableCopy] andSelectedParam:[selectedOptionArr mutableCopy]];
    
    [self.navigationController popViewControllerAnimated:YES];
}






@end
