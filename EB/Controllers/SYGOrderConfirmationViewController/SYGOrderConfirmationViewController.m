//
//  SYGOrderConfirmationViewController.m
//  EB
//
//  Created by webwerks on 09/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "SYGOrderConfirmationViewController.h"

@interface SYGOrderConfirmationViewController ()<FinishLoadingData>


@end

@implementation SYGOrderConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        [super setViewControllerTitle:@"  "];
    // Do any additional setup after loading the view.
        [self callOrderConfirmationWebwervice];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - order confirmation method
-(void)callOrderConfirmationWebwervice
{
                Webservice  *objWebService = [[Webservice alloc] init];
                BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
                if(!chkInternet)
                {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                }
                else
                {
                        objWebService.delegate =self;
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        NSString *url = [NSString stringWithFormat:@"%@userid=%@&orderid=%@",SG_ORDER_CONFIRM_WS,_userId,_orderId];
                        [objWebService GetWebServiceWithURL:url MathodName:SG_ORDER_CONFIRM_WS];
                        
                        
                }
        
}
#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if([strName isEqualToString:SG_ORDER_CONFIRM_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==0)
                {
                        _messageLabel.textColor = [UIColor redColor];
                }
                _messageLabel.text = [response valueForKey:@"message"];
        }
}
@end
