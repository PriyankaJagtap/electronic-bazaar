//
//  SYGOrderConfirmationViewController.h
//  EB
//
//  Created by webwerks on 09/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SellYourGadgetNavigationViewController.h"

@interface SYGOrderConfirmationViewController : SellYourGadgetNavigationViewController


@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property(nonatomic, strong) NSString *orderId;
@property(nonatomic, strong) NSString *userId;
@end
