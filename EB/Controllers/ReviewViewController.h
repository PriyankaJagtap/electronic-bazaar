//
//  ReviewViewController.h
//  EB
//
//  Created by webwerks on 8/17/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewViewController : BaseNavigationControllerWithBackBtn<UITableViewDataSource,UITableViewDelegate>

@property(strong,nonatomic)NSArray *reviewDetails;
@property(strong,nonatomic)NSString *productName;
@property(nonatomic)int productPrice;
@property (strong, nonatomic) IBOutlet UILabel *product_Price_Lbl;

@property (strong, nonatomic) IBOutlet UILabel *productTitle;
@property (strong, nonatomic) IBOutlet UILabel *reviewsCountLbl;

@property (strong, nonatomic) IBOutlet UITableView *reviewsTableview;

- (IBAction)backButtonAction:(id)sender;
- (IBAction)rightSlideMenuAction:(id)sender;

@end
