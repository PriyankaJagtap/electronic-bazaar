//
//  SalesReturnViewController.m
//  EB
//
//  Created by webwerks on 03/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SalesReturnViewController.h"
#import "BankInfoTableViewCell.h"
#import "SalesReturnProductDetailTableCell.h"
#import "PickUpAddressTableViewCell.h"
#import "SellGadgetTermsAndConditionViewController.h"
#import "SalesReturnProductDetailTableCell.h"
#import "SalesReturnTermsTableCell.h"

#define ACCEPTABLE_CUSTOMER_NAME_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define ACCEPTABLE_BANK_NAME_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._-"

#define ACCEPTABLE_IFSC_CODE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

#define ACCEPTABLE_BANK_NUMBER_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"
@interface SalesReturnViewController ()<FinishLoadingData,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
{
        NSArray *reasonsIdArr;
        NSArray *reasonsArr;
        NSArray *actionArr;
        NSArray *pickerDataArr;
        NSArray *actionIdArr;

        UIPickerView *pickerView;
        UIView *bgPickerView;
        NSInteger pickerSelectedIndex;
        UIButton *reasonForReturnBtn;
        UIButton *actionBtn;
        UIButton *acceptBtn;
        
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SalesReturnViewController

- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        _tableView.estimatedRowHeight = 200;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj hideTabbar];
        pickerSelectedIndex = 0;
        if([_objSalesReturn.returnStatus isEqualToString:@"can_return"])
        {
                _submitBtn.hidden = NO;
        }
        else
                _submitBtn.hidden = YES;
        [self getReturnReasonData];
    [super setViewControllerTitle:@"Sales Return"];
}

- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        if([_objSalesReturn.returnStatus isEqualToString:@"can_return"])
        {
                return 4;
        }
        else
                return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if(indexPath.row == 0)
        {
                static NSString *simpleTableIdentifier = @"SalesReturnProductDetailTableCell";
                SalesReturnProductDetailTableCell*cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil) {
                        cell = [[SalesReturnProductDetailTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                        
                }
                [[CommonSettings sharedInstance] setShadow:cell.bgView];
                cell.selectionStyle = UITableViewCellEditingStyleNone;
                cell.orderNoLabel.text = _objSalesReturn.orderNo;
                if([_objSalesReturn.returnStatus isEqualToString:@"can_return"])
                {

                        cell.statusTitleTopConstraint.constant = 0;
                        cell.reasonViewHeightConstraint.constant = 163;
                        cell.statusLabel.text = @"";
                        cell.statusTitleLabel.text = @"";
                }
                else
                {
                        cell.statusTitleTopConstraint.constant = 8;
                        cell.reasonViewHeightConstraint.constant = 0;
                        cell.statusLabel.text = _objSalesReturn.returnStatus;
                        cell.statusTitleLabel.text = @"Status:";
                }
                cell.productNameLabel.text = _objSalesReturn.productName;
                cell.priceLabel.text = [[CommonSettings sharedInstance] formatPrice:[_objSalesReturn.price floatValue]];
                cell.IMEILabel.text = _objSalesReturn.IMEINo;
                if(reasonForReturnBtn == nil)
                {
                     reasonForReturnBtn = cell.reasonForReturnBtn;
                }
                
                if(actionBtn == nil)
                {
                     actionBtn = cell.actionBtn;
                }
                return cell;
        }
        else if (indexPath.row == 1)
        {
                static NSString *simpleTableIdentifier = @"BankInfoTableViewCell";
                BankInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil) {
                        cell = [[BankInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                        
                }
                [[CommonSettings sharedInstance] setShadow:cell.bgView];
                cell.bgView.layer.cornerRadius = 5.0f;
                cell.selectionStyle = UITableViewCellEditingStyleNone;
                
                return cell;
        }
        else if (indexPath.row == 2)
        {
                
                static NSString *simpleTableIdentifier = @"PickUpAddressTableViewCell";
                PickUpAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil) {
                        cell = [[PickUpAddressTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                        
                }
                [[CommonSettings sharedInstance] setShadow:cell.bgView];
                cell.selectionStyle = UITableViewCellEditingStyleNone;
                cell.addressLabel.text = _objSalesReturn.shippingAddress;
                return cell;
        }
        else
        {
                static NSString *simpleTableIdentifier = @"SalesReturnTermsTableCell";
                SalesReturnTermsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil) {
                        cell = [[SalesReturnTermsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                        
                }
                
                if(acceptBtn == nil)
                {
                   acceptBtn = cell.acceptBtn;
                }
                
                 [cell.acceptBtn addTarget:self action:@selector(acceptBtnClicked) forControlEvents:UIControlEventTouchUpInside];
                cell.selectionStyle = UITableViewCellEditingStyleNone;
                return cell;
        }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        
}


#pragma mark - text field delegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        BankInfoTableViewCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        
        if(textField == cell.customerNameTxtField)
        {
                return [[CommonSettings sharedInstance] allParticularCharInTextFieldWithString:string acceptableCharStr:ACCEPTABLE_CUSTOMER_NAME_CHARACTERS];
        }
        else  if(textField == cell.bankNameTxtField)
        {
                return [[CommonSettings sharedInstance] allParticularCharInTextFieldWithString:string acceptableCharStr:ACCEPTABLE_BANK_NAME_CHARACTERS];
        }
        else if (textField == cell.ifscCodeTxtField)
        {
                return [[CommonSettings sharedInstance] allParticularCharInTextFieldWithString:string acceptableCharStr:ACCEPTABLE_IFSC_CODE_CHARACTERS];
        }
        else if (textField == cell.accountNumberTxtField)
        {
                return [[CommonSettings sharedInstance] allParticularCharInTextFieldWithString:string acceptableCharStr:ACCEPTABLE_BANK_NUMBER_CHARACTERS];
        }
        return YES;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
        CGPoint pointInTable = [textField.superview.superview convertPoint:textField.frame.origin toView:_tableView];
        CGPoint contentOffset = _tableView.contentOffset;
        contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
        
        NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
        [_tableView setContentOffset:contentOffset animated:YES];
        return YES;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
        [textField resignFirstResponder];
        if ([textField.superview.superview.superview isKindOfClass:[UITableViewCell class]])
        {
                CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                          toView: _tableView];
                NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
                
                [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        }
        else if ([textField.superview.superview.superview.superview isKindOfClass:[UITableViewCell class]])
        {
                CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                          toView: _tableView];
                NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
                
                [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
                
        }
        
        return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [textField resignFirstResponder];
        return YES;
}


#pragma mark - get resgisteration data
-(void)getReturnReasonData
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:RETURN_REASONS_WS MathodName:RETURN_REASONS_WS];
                
        }
}


#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:RETURN_REASONS_WS]){
                //NSLog(@"%@",response);
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        reasonsIdArr = [[response valueForKey:@"reason"] valueForKey:@"id"];
                        reasonsArr = [[response valueForKey:@"reason"] valueForKey:@"label"];
                        actionArr = [[response valueForKey:@"return"] valueForKey:@"label"];
                        actionIdArr = [[response valueForKey:@"return"] valueForKey:@"id"];
                        [actionBtn setTitle:[actionArr objectAtIndex:0] forState:UIControlStateNormal];
                        [reasonForReturnBtn setTitle:[reasonsArr objectAtIndex:0] forState:UIControlStateNormal];
                }
                else
                {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                }
        }
        else if ([strName isEqualToString:SAVE_RETURN_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        alert.tag = 500;
                        [alert show];

                }
                else
                {
                        [[AppDelegate getAppDelegateObj] showAlert:@"" withMessage:[response valueForKey:@"message"]];
                }
        }
}
#pragma mark - IBAction methods
- (IBAction)backBtnClicked:(id)sender {
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj unhideTabbar];
        [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submitBtnClicked:(id)sender {
        [self.view endEditing:YES];
        
        BankInfoTableViewCell *objBankInfoCell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        if(objBankInfoCell)
        {
                if (![Validation required:objBankInfoCell.customerNameTxtField withCaption:@"Customer Name"])
                        return ;
                if (![Validation required:objBankInfoCell.bankNameTxtField withCaption:@"Bank Name"])
                        return ;
                
                if (![Validation required:objBankInfoCell.accountNumberTxtField withCaption:@"Account Number"])
                        return ;
                
                if (![Validation required:objBankInfoCell.ifscCodeTxtField withCaption:@"IFSC Code"])
                        return ;
                
        }
        
        
        if(![acceptBtn isSelected])
        {

                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please confirm that all details filled by you are correct." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                return;
        }
        [self callSaveReturn];
}


- (IBAction)reasonForReturnBtnClicked:(id)sender {
        //        UIButton *btn = (UIButton *)sender;
        //        CGPoint pointInTable = [btn.superview.superview convertPoint:btn.frame.origin toView:_tableView];
        //        CGPoint contentOffset = _tableView.contentOffset;
        //        contentOffset.y = (pointInTable.y - 200);
        //        NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
        //        [_tableView setContentOffset:contentOffset animated:YES];
        [self setPickerView];
}


-(void)acceptBtnClicked
{
        if([acceptBtn isSelected])
                acceptBtn.selected = NO;
        else
                acceptBtn.selected = YES;
}

- (IBAction)actionBtnClicked:(id)sender {
        
        if(bgPickerView == nil)
        {
                bgPickerView =[[UIView alloc]init];
                bgPickerView.frame = CGRectMake(0, self.view.frame.size.height - 200
                                                , self.view.frame.size.width, 200);
                UIButton *doneButton = [[UIButton alloc]init];
                doneButton.frame =CGRectMake(0, 0, bgPickerView.frame.size.width, 30);
                [doneButton setTitle:@"Done" forState:UIControlStateNormal];
                doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                doneButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
                doneButton.backgroundColor =[UIColor lightGrayColor];
                [doneButton addTarget:self action:@selector(doneTouched:) forControlEvents:UIControlEventTouchUpInside];
                [bgPickerView addSubview:doneButton];
                [self.view addSubview:bgPickerView];
        }
        pickerView = [[UIPickerView alloc]init];
        pickerView.tag = 20;
        pickerView.frame = CGRectMake(0, 30, self.view.frame.size.width, 170);
        pickerDataArr = actionArr;
        pickerView.dataSource = self;
        pickerView.delegate = self;
        
        [pickerView selectRow:0 inComponent:0 animated:YES];
        pickerView.backgroundColor = [UIColor whiteColor];
        pickerView.showsSelectionIndicator = YES;
        [bgPickerView addSubview:pickerView];
        [pickerView reloadAllComponents];
}

#pragma mark - save salesReturn
-(void)callSaveReturn
{
        Webservice  *callAddlistWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callAddlistWs.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                BankInfoTableViewCell *objBankInfoCell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];

                
                NSDictionary *userDic = [[NSUserDefaults standardUserDefaults] valueForKey:@"user"];
                NSInteger actionIDIndex = [actionArr indexOfObject:[actionBtn titleForState:UIControlStateNormal]];
                NSInteger returnReasonIDIndex = [reasonsArr indexOfObject:[reasonForReturnBtn titleForState:UIControlStateNormal]];
                NSString *param = [NSString stringWithFormat:@"reason=%@&order_number=%@&imei=%@&bank_name=%@&account_no=%@&beneficiary_name=%@&bank_ifsc=%@&product_name=%@&auth_by_customer=%@&return_action=%@&userid=%@",[reasonsIdArr objectAtIndex:returnReasonIDIndex],_objSalesReturn.orderNo,_objSalesReturn.IMEINo,objBankInfoCell.bankNameTxtField.text,objBankInfoCell.accountNumberTxtField.text,objBankInfoCell.customerNameTxtField.text,objBankInfoCell.ifscCodeTxtField.text,_objSalesReturn.productName,@"1",[actionIdArr objectAtIndex:actionIDIndex],[userDic valueForKey:@"user_id"]];
                NSString *url = [NSString stringWithFormat:@"%@%@",SAVE_RETURN_WS,param];
                [callAddlistWs GetWebServiceWithURL:url MathodName:SAVE_RETURN_WS];
        }
}

#pragma mark - set custom picker
-(void)setPickerView{
        
        if(bgPickerView == nil)
        {
                bgPickerView =[[UIView alloc]init];
                bgPickerView.frame = CGRectMake(0, self.view.frame.size.height - 200
                                                , self.view.frame.size.width, 200);
                UIButton *doneButton = [[UIButton alloc]init];
                doneButton.frame =CGRectMake(0, 0, bgPickerView.frame.size.width, 30);
                [doneButton setTitle:@"Done" forState:UIControlStateNormal];
                doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                doneButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
                doneButton.backgroundColor =[UIColor lightGrayColor];
                [doneButton addTarget:self action:@selector(doneTouched:) forControlEvents:UIControlEventTouchUpInside];
                [bgPickerView addSubview:doneButton];
                [self.view addSubview:bgPickerView];
        }
        pickerView = [[UIPickerView alloc]init];
        pickerView.tag = 10;
        pickerView.frame = CGRectMake(0, 30, self.view.frame.size.width, 170);
        pickerDataArr = reasonsArr;

        pickerView.dataSource = self;
        pickerView.delegate = self;
        
        [pickerView selectRow:0 inComponent:0 animated:YES];
        pickerView.backgroundColor = [UIColor whiteColor];
        pickerView.showsSelectionIndicator = YES;
        [bgPickerView addSubview:pickerView];
        [pickerView reloadAllComponents];
}


- (void)doneTouched:(UIBarButtonItem *)sender{
        bgPickerView.hidden = YES;
        [bgPickerView removeFromSuperview];
        bgPickerView = nil;
        // [_tableView setContentOffset:CGPointMake(0, 0)];
}
#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
        return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
        return [pickerDataArr count];
}

#pragma mark- Picker View Delegate
-(void)pickerView:(UIPickerView *)pickerView1 didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
        pickerSelectedIndex = row;
        if(pickerView1.tag == 10)
                [reasonForReturnBtn setTitle:[reasonsArr objectAtIndex:row] forState:UIControlStateNormal];
        else if (pickerView1.tag == 20)
                [actionBtn setTitle:[actionArr objectAtIndex:row] forState:UIControlStateNormal];
                
        
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
        return [pickerDataArr objectAtIndex:row];
}



#pragma mark - Alertview Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
        if (alertView.tag==500  && buttonIndex == 0) {
                
                AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
                [objAppdelegate.tabBarObj unhideTabbar];
                [self.navigationController popViewControllerAnimated:YES];
        }
}


@end
