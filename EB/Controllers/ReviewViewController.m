//
//  ReviewViewController.m
//  EB
//
//  Created by webwerks on 8/17/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "ReviewViewController.h"
#import "ReviewCell.h"
#import "AppDelegate.h"


@interface ReviewViewController ()

@end

@implementation ReviewViewController
@synthesize reviewDetails,productName,productTitle,reviewsCountLbl,reviewsTableview;
@synthesize product_Price_Lbl,productPrice;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [super setTitle:@"Reviews"];
    productTitle.text=productName;
    reviewsCountLbl.text=[NSString stringWithFormat:@"(%lu reviews)",(unsigned long)[reviewDetails count]];
    self.reviewsTableview.contentInset = UIEdgeInsetsMake(-36, 0, 15, 0);

    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterDecimalStyle]; // to get commas (or locale equivalent)
    [fmt setMaximumFractionDigits:0]; // to avoid any decimal
    
    NSInteger value = productPrice;
    
   // NSString *result = [fmt stringFromNumber:@(value)];
//    product_Price_Lbl.text=[NSString stringWithFormat:@"Rs.%@",result];
         product_Price_Lbl.text= [[CommonSettings sharedInstance] formatIntegerPrice:value];
    
}

-(void)rightBarButtonItemPressed
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [reviewDetails count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"Cell";
    ReviewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    //cell.brandName=[[[[specificationDetails objectAtIndex:indexPath.row]valueForKey:@"specs"]objectAtIndex:1]valueForKey:@"value"];
    cell.titleReview.text=[[reviewDetails objectAtIndex:indexPath.row]valueForKey:@"title"];
    cell.detailReview.text=[[reviewDetails objectAtIndex:indexPath.row]valueForKey:@"detail"];
    cell.name.text=[[reviewDetails objectAtIndex:indexPath.row]valueForKey:@"nickname"];
    NSString *createdDate=[self formatDateFromString:[[reviewDetails objectAtIndex:indexPath.row]valueForKey:@"created_at"]];
    cell.date.text=createdDate;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 113;
    
}
//    [orderDateFormat setDateFormat: @"yyyy-MM-dd HH:mm:ss"];

-(NSString*)formatDateFromString:(NSString *)dateStr
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *date=[formatter dateFromString:dateStr];
    [formatter setDateFormat:@"dd MMM YYYY"];
    NSString *formattedDateStr=[formatter stringFromDate:date];
    return formattedDateStr;
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightSlideMenuAction:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}


@end
