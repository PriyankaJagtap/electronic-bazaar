//
//  SalesReturnProductClass.h
//  EB
//
//  Created by webwerks on 03/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SalesReturnProductClass : NSObject
@property (nonatomic, weak) NSString *status;
@property (nonatomic, weak) NSString *orderNo;
@property (nonatomic, weak) NSString *productName;
@property (nonatomic, strong) NSString *IMEINo;
@property (nonatomic, strong) NSString *returnStatus;
@property (nonatomic, weak) NSString *price;
@property (nonatomic, weak) NSString *shippingAddress;


@end
