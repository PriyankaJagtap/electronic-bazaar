//
//  SelectIMEIViewController.h
//  EB
//
//  Created by webwerks on 03/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesReturnProductClass.h"

@protocol SelectIMEIDelegate <NSObject>
-(void)IMEISelected:(SalesReturnProductClass *)objSalesReturnProdClass;

@end

@interface SelectIMEIViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *IMEINumberDataArr;
@property (strong, nonatomic) NSArray *returnStatusDataArr;

@property (nonatomic, strong) SalesReturnProductClass *objSalesReturn;


@property (nonatomic)id<SelectIMEIDelegate> deleagte;


@end
