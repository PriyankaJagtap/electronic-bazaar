//
//  TrackingDetailsViewController.m
//  EB
//
//  Created by webwerks on 16/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "TrackingDetailsViewController.h"
#import "TrackingDetailsHeaderTableCell.h"
#import "TrackingDetailTableCell.h"

@interface TrackingDetailsViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation TrackingDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        _tblView.estimatedRowHeight = 80;
        _tblView.rowHeight = UITableViewAutomaticDimension;
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj hideTabbar];
    [super setViewControllerTitle:@"Tracking Details"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - TableView methods
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        return 50.0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return _trackingDataArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        NSDictionary *dic = [_trackingDataArr objectAtIndex:section];
        return [[dic valueForKey:@"scans"] count];
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        NSDictionary *dic = [_trackingDataArr objectAtIndex:section];
        static NSString *simpleTableIdentifier = @"TrackingDetailsHeaderTableCell";
        TrackingDetailsHeaderTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
                cell = [[TrackingDetailsHeaderTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                
        }
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        
        cell.titleLabel.text = [NSString stringWithFormat:@"%@,%@",[dic valueForKey:@"date"],[dic valueForKey:@"city"]];
        
        UIView *contentView = cell.contentView;
        return contentView;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *simpleTableIdentifier = @"TrackingDetailTableCell";
        TrackingDetailTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
                cell = [[TrackingDetailTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                
        }
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        NSArray *scansArr = [[_trackingDataArr objectAtIndex:indexPath.section] valueForKey:@"scans"];
        NSDictionary *dic = [scansArr objectAtIndex:indexPath.row];
        cell.dateLabel.text = [dic valueForKey:@"date"];
        cell.titleLabel.text = [dic valueForKey:@"status"];
        
        return cell;
}
#pragma mark - IBAction methods
- (IBAction)backBtnClicked:(id)sender {
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj unhideTabbar];
        [self.navigationController popViewControllerAnimated:YES];
}
@end
