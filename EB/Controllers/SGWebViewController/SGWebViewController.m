//
//  SGWebViewController.m
//  EB
//
//  Created by webwerks on 25/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "SGWebViewController.h"

@interface SGWebViewController ()

@end

@implementation SGWebViewController

- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj hideTabbar];
        [super setViewControllerTitle:_titleLabel];
                
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:_webViewUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
        [_webView loadRequest:request ];
        [[CommonSettings sharedInstance] displayLoading];
        
}
- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - web view delegate methods
-(void)webViewDidFinishLoad:(UIWebView *)webView {
        [[CommonSettings sharedInstance] removeLoading];
        
        NSLog(@"finish");
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
        [[CommonSettings sharedInstance] removeLoading];
        
        NSLog(@"Error for WEBVIEW: %@", [error description]);
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
        if (navigationType == UIWebViewNavigationTypeLinkClicked){
                
                NSURL *url = request.URL;
                
                NSArray *arr = [url.absoluteString componentsSeparatedByString:@"://"];
                
                if([[arr objectAtIndex:0] isEqualToString:@"signup"])
                {
                        [[AppDelegate getAppDelegateObj] navigateToRegistration];
                }
                else if([[arr objectAtIndex:0] isEqualToString:@"login"])
                {
                        [[AppDelegate getAppDelegateObj] navigateToLoginViewController];
                        
                }
                else if([[arr objectAtIndex:0] isEqualToString:@"pan"])
                {
                }
                else if([[arr objectAtIndex:0] isEqualToString:@"ebwarranty"])
                {
                        //                        WarrantyViewController *objVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
                        //
                        //                        objVc.warrantyBannerClicked = YES;
                        //                        [self.navigationController pushViewController:objVc animated:YES];
                }
        }
        return YES;
}


@end
