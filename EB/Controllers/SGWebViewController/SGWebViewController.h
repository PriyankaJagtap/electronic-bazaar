//
//  SGWebViewController.h
//  EB
//
//  Created by webwerks on 25/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGWebViewController : SellYourGadgetNavigationViewController<UIWebViewDelegate>

{
}
@property (nonatomic, strong) NSString *webViewUrl;
@property (nonatomic, strong) NSString *titleLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
