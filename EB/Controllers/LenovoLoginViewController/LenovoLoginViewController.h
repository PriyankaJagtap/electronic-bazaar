//
//  LenovoLoginViewController.h
//  EB
//
//  Created by Neosoft on 3/3/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LenovoLoginViewController : UIViewController
- (IBAction)loginBtnClicked:(id)sender;
- (IBAction)backBtnClicked:(id)sender;
- (IBAction)showPwdBtnClicked:(id)sender;
- (IBAction)forgotPwdBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *objScrollView;
@property (weak, nonatomic) IBOutlet UIView *objScrollViewBg;

@property (weak, nonatomic) IBOutlet UITextField *pwdtextField;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *pwdBtn;

@end
