//
//  LenovoLoginViewController.m
//  EB
//
//  Created by Neosoft on 3/3/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "LenovoLoginViewController.h"
#import "ForgotPassword.h"


@interface LenovoLoginViewController ()<UITextFieldDelegate,FinishLoadingData>

@end

@implementation LenovoLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.    
    AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    [objAppdelegate.tabBarObj hideTabbar];
    [self setTextFieldBottomLayer:_emailTextField];
    [self setTextFieldBottomLayer:_pwdtextField];
    [_objScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    self.automaticallyAdjustsScrollViewInsets = false;


}


-(void)setTextFieldBottomLayer:(UITextField *)textField
{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - text field delegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    CGPoint pointInTable = [textField.superview.superview convertPoint:textField.frame.origin toView:_objScrollView];
    CGPoint contentOffset = _objScrollView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    
    NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    [_objScrollView setContentOffset:contentOffset animated:YES];
 
    return YES;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    [_objScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
   
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    if (textField == self.pwdtextField) {
        [textField resignFirstResponder];
    } else if (textField == self.emailTextField) {
        [self.pwdtextField becomeFirstResponder];
    }
    return YES;
}

#pragma mark - IBAction methods
- (IBAction)loginBtnClicked:(id)sender {
    
//    if ([_emailTextField.text isEqualToString:@""]) {
//        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please insert email id"];
//        return;
//    }
    
    if (_emailTextField.text){
        
        if (_emailTextField.text.length == 0) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
        }
        if (![self validEmail:_emailTextField.text]) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
            return;
            
        }
    }
    
    
    if([_pwdtextField.text isEqualToString:@""]){
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please insert password"];
        return;
    }
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictData = [NSMutableDictionary new];
    [dictData setObject:_emailTextField.text forKey:@"username"];
    [dictData setObject:_pwdtextField.text forKey:@"password"];
    [dictData setObject:@"iphone" forKey:@"device_type"];
    
    [dictData setObject:@"normal" forKey:@"social_type"];
    [dictData setObject:@"" forKey:@"social_login_id"];
    NSLog(@"device token %@",[defaults valueForKey:@"DEVICE_TOKEN"]);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DEVICE_TOKEN"]);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"DEVICE_TOKEN"]);
    
    NSString *pushID=[defaults objectForKey:@"DEVICE_TOKEN"];
    [dictData setObject:pushID?:@"" forKey:@"push_id"];
    [dictData setObject:@"" forKey:@"firstname"];
    [dictData setObject:@"" forKey:@"lastname"];
    [dictData setObject:@"" forKey:@"dob"];
    [dictData setObject:@"" forKey:@"mobile"];
    [dictData setObject:@"" forKey:@"profile_picture"];
    
    [self callLoginWS:dictData];
}

- (IBAction)backBtnClicked:(id)sender {
    
    AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    [objAppdelegate.tabBarObj unhideTabbar];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)showPwdBtnClicked:(id)sender {
    _pwdBtn.selected = ! _pwdBtn.selected;
    
    if (_pwdBtn.selected) {
        _pwdtextField.secureTextEntry = NO;
        _pwdtextField.text =_pwdtextField.text ;
    }
    else{
        _pwdtextField.secureTextEntry = YES;
        _pwdtextField.text =_pwdtextField.text ;
    }
}

- (IBAction)forgotPwdBtnClicked:(id)sender {

     ForgotPassword *objc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ForgotPassword"];
    
    [self.navigationController pushViewController:objc animated:YES];
}

#pragma mark - Email Validation

-(BOOL)validEmail:(NSString*)emailString
{
    NSString *emailid = emailString;
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL myStringMatchesRegEx=[emailTest evaluateWithObject:emailid];
    
    if(myStringMatchesRegEx)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
#pragma mark - LoginWS
-(void)callLoginWS:(NSMutableDictionary *)userdata{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:userdata url:LOGIN_WS string:@"LoginWS"];
    }
}

-(void)receivedResponseForLogin:(id)receiveData stringResponse:(NSString *)responseType{
    [FVCustomAlertView hideAlertFromView:self.view fading:NO];
    if ([responseType isEqualToString:@"success"]) {
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        NSMutableDictionary *dictUserDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
        
        DLog(@"userDetail %@",jsonObject);
        if ([dictUserDetail isKindOfClass:[NSDictionary class]]) {
            NSString *status = [NSString stringWithFormat:@"%ld",(long)[[dictUserDetail objectForKey:@"status"]integerValue]];
            if ([status isEqualToString:@"1"])
            {

                if([[dictUserDetail valueForKey:@"customer_id"] integerValue] != 5)
                {
                    [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Only Lenovo users can login from here"];
                    _emailTextField.text = nil;
                    _pwdtextField.text = nil;
                    return;
                }
                NSString *user_id =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"id"]];
                [[NSUserDefaults standardUserDefaults]setObject:LENOVO_LOGIN forKey:LENOVO_LOGIN];
                [[NSUserDefaults standardUserDefaults]setObject:user_id forKey:LENOVO_USERID];
                  [[NSUserDefaults standardUserDefaults]setObject:[dictUserDetail valueForKey:@"customer_id"] forKey:LENOVO_CUSTOMER_ID];
                
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [[[AppDelegate getAppDelegateObj] tabBarObj] unhideTabbar];
               
                [AppDelegate getAppDelegateObj].lenovo_userId = [user_id intValue];
                
                [AppDelegate getAppDelegateObj].isFromLenovoLogin = YES;
              

                
            }
            else if ( [status isEqualToString:@"2"])
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[dictUserDetail valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alert show];
                
            }
            else{
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:[dictUserDetail valueForKey:@"message"]];
            }
        }
        else{
            [[AppDelegate getAppDelegateObj]showAlert:@"Sorry" withMessage:@"Login couldn't succeed."];
        }
    }
}

@end
