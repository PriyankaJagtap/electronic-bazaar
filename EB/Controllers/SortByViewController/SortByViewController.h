//
//  SortByViewController.h
//  EB
//
//  Created by Neosoft on 5/25/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SortByDelegate <NSObject>

-(void)sortProductByValue:(NSString *)value;

@end

@interface SortByViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *sortDataArray;
@property (nonatomic, retain) id <SortByDelegate> delegate;

@end
