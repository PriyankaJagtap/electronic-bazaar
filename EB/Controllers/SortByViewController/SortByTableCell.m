//
//  SortByTableCell.m
//  EB
//
//  Created by Neosoft on 5/26/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SortByTableCell.h"

@implementation SortByTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
