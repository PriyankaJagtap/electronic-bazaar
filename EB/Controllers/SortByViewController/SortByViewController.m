//
//  SortByViewController.m
//  EB
//
//  Created by Neosoft on 5/25/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SortByViewController.h"

#import "SortByTableCell.h"

@interface SortByViewController () <UIGestureRecognizerDelegate>
{
    NSMutableArray *selectedValueArr;
}

@end

@implementation SortByViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    selectedValueArr = [NSMutableArray new];
    for (int i = 0; i < _sortDataArray.count; i++) {
        [selectedValueArr addObject:[NSNumber numberWithBool:NO]];
    }
    
    self.tableView.estimatedRowHeight = 40;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - handle tap
-(void)viewTapped:(UITapGestureRecognizer *)sender
{
    [UIView animateWithDuration:1.0 animations:^ {
        self.view.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
    } completion:^(BOOL finished) {
            [self willMoveToParentViewController:nil];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
    }];
}
#pragma mark - table view methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return _sortDataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SortByTableCell";
    SortByTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[SortByTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.titleLabel.text = [[_sortDataArray objectAtIndex:indexPath.row] valueForKey:@"name"];
    
    if([[selectedValueArr objectAtIndex:indexPath.row] boolValue])
    {
        [cell.tickBtn setSelected:YES];
    }
    else
    {
        [cell.tickBtn setSelected:NO];

    }
    
    cell.tickBtn.tag = indexPath.row;
    [cell.tickBtn addTarget:self action:@selector(radioBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self handleSorting:indexPath.row];

}


-(void)radioBtnClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self handleSorting:btn.tag];
}


-(void)handleSorting:(NSInteger)cellIndex
{
    if(![[selectedValueArr objectAtIndex:cellIndex] boolValue])
    {
        [selectedValueArr removeAllObjects];
        for (int i = 0; i < _sortDataArray.count; i++) {
            if(i == cellIndex)
                [selectedValueArr addObject:[NSNumber numberWithBool:YES]];
            else
                [selectedValueArr addObject:[NSNumber numberWithBool:NO]];
            
            
        }
        
        [_tableView reloadData];
    }
    
    [UIView animateWithDuration:1.0 animations:^ {
        self.view.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [_delegate sortProductByValue:[[_sortDataArray objectAtIndex:cellIndex] valueForKey:@"value"]];
    }];

}
#pragma mark - gesture delegate method
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_tableView]) {
        return NO;
    }
    return YES;
}

@end
