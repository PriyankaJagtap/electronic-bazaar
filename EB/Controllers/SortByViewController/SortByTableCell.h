//
//  SortByTableCell.h
//  EB
//
//  Created by Neosoft on 5/26/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SortByTableCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *tickBtn;

@end
