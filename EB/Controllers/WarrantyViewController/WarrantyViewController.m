//
//  WarrantyViewController.m
//  EB
//
//  Created by Neosoft on 4/6/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "WarrantyViewController.h"
#import "UIButton+UIButtonWithSubTitle.h"
#import "ServiceCenterViewController.h"

#define WARRANTY_WS @"http://electronicsbazaar.com/mobile-app/warranty-app.html"


@interface WarrantyViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewLeftConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewRightConstraint;


@end

@implementation WarrantyViewController
static NSString *webViewBannerType = @"webview";

//static NSString *warrantyHtml = @"The information contained herein is subject to change without notice. The only warranties for Electronics Bazaar products and services are set forth in the express warranty statements accompanying such products and services. Nothing herein should be construed as constituting an additional warranty. Electronics Bazaar shall not be liable for technical or editorial errors or omissions contained herein. This document contains proprietary information that is protected by copyright. No part of this document may be photocopied, reproduced, or translated to another language without the prior written consent of Electronics Bazaar.<br>Electronics Bazaar supports lawful use of technology and does not endorse or encourage the use of its products for purposed other than those permitted by copyright law. The information in this document is subject to change without notice.<br><br><b><u>Electronics Bazaar Warranty </u></b><br><b>Hardware </b><br>-Open Box: 6 months<br>-    Pre-Owned: 1 year <br>-    Battery: 1 month<br>-    Accessories: Not provided<br><b>Software/Operating System </b><br>-    Not Available / Not Provided<br><br><b><u>General Terms and Conditions:</u></b><br>No change in the warranty is valid unless it is made in writing and signed by an authorized EB/Service partner representatives.<br>There is no onsite warranty support. Only carry in warranty support.<br>1.    The warranty coverage<br>•    This warranty is only valid in India and the products are not eligible for any international warranty service. Warrantyservice may only be performed by Electronics Bazaar authorized service partner.\•    Electronics Bazaar warrants that the products that you have purchased from Electronics Bazaar are free from defects in materials or workmanship under normal use during the warranty period. The warranty period starts on the date of purchase from Electronics Bazaar. The warranty will be validated through hardware serial number or IMEI number. You are entitledto hardware warranty service according to the terms and conditions of this document if a repair is required within the limited warranty period.<br>•    Electronics Bazaar may repair or replace products with new or previously used products or parts equivalent to new in performance and reliabilityor with equivalent products to an original product.<br>•    This warranty is only valid in India and the products are not eligible for any international warranty service. Warrantyservice may only be performed by Electronics Bazaar authorized service partner.•    Replacement parts are warranted to be free from defects in material or workmanship and warranty on replaced unit/part will be for the remainder of the original warranty period as per invoice of the Electronics Bazaar Hardware Product they are repairing.<br>•    During the Warranty Period, Electronics Bazaar will, at its discretion, repair or replace any defective component.All component parts or hardware products removed under this warranty become the property of Electronics Bazaar.<br><br>2.    Electronics Bazaar does not warrant that the operation of the product will be uninterrupted or error-free and it does not coverthe following:<br>•    Cosmetic appearances such as scratches or normal wear and tear, breakages, dents.<br>•    Damage from accidents, acts of God, fire and water.<br>•    Minor white patches in the display.<br>•    Operating the product internationally with different technical standards for which it was not originally designed.<br>•    Charging the battery with charger other than originally provided.<br>•    Cost of recovering any program or data (If applicable).<br><br>3.    The warranty is not applicable in following cases:<br>•    The product is not used as mentioned in owner’s manual.<br>•    Modification in Hardware and Operating System.•    As a result of accident, misuse, abuse, contamination, improper or inadequate maintenance or calibration, or other external causes. <br>•    Operating outside the usage parameters stated in the user documentation that shipped with the product. <br>•    Software, interfacing, parts, or supplies not supplied by Electronics Bazaar.<br>•    Virus infection.<br>•    Loss or damage in transit.<br>•    This warranty does not apply to expendable or consumable parts and does not extend to any product from which the serial numberhas been removed or that has been damaged or rendered defective.<br>•    Modification or service by anyone other than:<br>•    Electronics Bazaar,<br>•    Electronics Bazaar authorized service provider.The warranty is limited to the repair of defect or replacement of part.Warranty repairs must be carried out by Electronics Bazaar authorized service partner only.If repair is attempted by anyone other than Electronics Bazaar authorized service partner, the warranty will be void.<br><br>4.    Electronics Bazaar /Service partner is not responsible for the following:<br>•    Any restoration or reinstallation of any data or programs other than software installed when the product was manufactured.<br>•    Damage or loss of any program or data. <br>•    Accessories such as removable storage media, screen protectors, cases, covers, etc. Kindly ensure to remove them before handing overthe product to Electronics Bazaar Authorized service center.<br><br>YOU SHOULD MAKE PERIODIC BACKUP COPIES OF THE DATA STORED ON YOUR HARD DRIVE OR OTHER STORAGE DEVICES AS A PRECAUTION AGAINST POSSIBLE FAILURES, ALTERATION, OR LOSS OF THE DATA. BEFORE RETURNING ANY UNIT FOR SERVICE, BE SURE TO BACK UP DATA AND REMOVE ANY CONFIDENTIAL, PROPRIETARY, OR PERSONAL INFORMATION. ELECTRONICS BAZAAR IS NOT RESPONSIBLE FOR DAMAGE TO OR LOSS OF ANY PROGRAMS, DATA, OR REMOVABLE STORAGE MEDIA. ELECTRONICS BAZAAR IS NOT RESPONSIBLE FOR THE RESTORATION OR REINSTALLATION OF ANY PROGRAMS OR DATA OR SOFTWARE OR OPERATING SYSTEM INSTALLED BY USER. UNITS SENT IN FOR SERVICE MAY HAVE THE DATA ERASED FROM THE HARD DRIVE AND THE PROGRAMS RESTORED TO THEIR ORIGINAL STATE.<br><br><b> Carry-in Warranty Service </b><br> Under the terms only carry-in service is available for your product, you will be required to deliver your Electronics Bazaar Hardware Product to an authorized service location for warranty repair. No onsite support.<br><br> <b>Obtaining Warranty Repair Service</b><br> To obtain hardware warranty service, call Customer Care or a participating authorized Electronics Bazaar Service Repair Center at one of the numbers listed later in this document.<br><br> <b>Customer Care Phone Numbers</b><br> Use the Electronics Bazaar Customer Care number during and after your product\'s warranty period.Support is provided free of charge during the warranty period. A per-incident charge applies after the warranty period.<br><br> <b> Warning:</b><br> When using the device, basic safety precautions should always be followed to reduce the risk of fire, electric shock, and injury to persons,including the following:<br> • Do not use the product near water, for example, wash bowl or kitchen sink <br>• Avoid using the product during an electrical storm. There may be a remote risk of electric shock from lightning.<br><br> <b>Power Safety Warning:</b><br> <b>WARNING:</b><br> Install the Adaptor near a Power outlet. The AC power cord is the Adaptor main AC disconnecting device and must be easily accessible at all times. For your safety, the power cord provided with your Adaptor has a grounded plug. Always use the power cord with a properly grounded wall outlet, to avoid the risk of electrical shock.<br> WARNING: Your Adaptor is provided with a voltage select switch for use in a 115 or 230V power system. The voltage select switch has been pre-set to the correct voltage setting for use in the particular country/region where it was initially sold. Changing the voltage select switch to the incorrect position can damage your Adaptor and void any implied warranty.<br> WARNING: Do not operate the Adaptor with the cover removed.<br><br> <b>Warning:</b> <br> When using the device, basic safety precautions should always be followed to reduce the risk of fire, electric shock,and injury to persons, including the following:• Do not use the product near water, for example, wash bowl or kitchen sink • Avoid using the product during an electrical storm.There may be a remote risk of electric shock from lightning.<br><br><b>Power Safety Warning:</b> <br> WARNING: Install the Adaptor near a Power outlet. The AC power cord is the Adaptor main AC disconnecting device and must be easily accessible at all times. For your safety, the power cord provided with your Adaptor has a grounded plug. Always use the power cord with a properly grounded wall outlet, to avoid the risk of electrical shock.<br> WARNING: Your Adaptor is provided with a voltage select switch for use in a 115 or 230V power system.The voltage select switch has been pre-set to the correct voltage setting for use in the particular country/region where it was initially sold. Changing the voltage select switch to the incorrect position can damage your Adaptor and void any implied warranty. <br> WARNING: Do not operate the Adaptor with the cover removed.<br> ";


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    [objAppdelegate.tabBarObj hideTabbar];
    _ebServiceBtn.titleLabel.textColor = [UIColor whiteColor];
     if(_warrantyBannerClicked)
    {
        [self.view layoutIfNeeded];
        _ebServiceBtbHtConstraint.constant = 60;
        _webViewTopConstraint.constant = 0;
        [self.view layoutIfNeeded];
        //_warrantyLabel.text = @"Warranty";
            [super setViewControllerTitle:@""];
        
         [_ebServiceBtn  setupButton:@"CLICK HERE" image:[UIImage imageNamed:@"location_blue_icon"] subTitle:@"To know your nearest EB Service Partner Center"];
         NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:WARRANTY_WS] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
        [_webView loadRequest:request ];
    }
    else
    {
        if([_bannerType isEqualToString:webViewBannerType])
        {
            [self.view layoutIfNeeded];
            _webViewTopConstraint.constant = 0;
            _webViewLeftConstraint.constant = 0;
            _webViewRightConstraint.constant = 0;
            [self.view layoutIfNeeded];
        }
        _ebServiceBtbHtConstraint.constant = 0;
        [super setViewControllerTitle: self.titleLabel];
//        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:_webViewUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
            
            if (request){
                    [[NSURLCache sharedURLCache] removeCachedResponseForRequest:request];
                    [[NSURLCache sharedURLCache] removeAllCachedResponses];
                    
            }
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:_webViewUrl] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
        [_webView loadRequest:request ];
            
    }
    
   
    
    //load file into webView
   
    //[_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]] ];
    [[CommonSettings sharedInstance] displayLoading];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[[AppDelegate getAppDelegateObj] tabBarObj] unhideTabbar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - web view delegate methods
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [[CommonSettings sharedInstance] removeLoading];

    NSLog(@"finish");
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[CommonSettings sharedInstance] removeLoading];

    NSLog(@"Error for WEBVIEW: %@", [error description]);
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        
        NSURL *url = request.URL;
        
        NSArray *arr = [url.absoluteString componentsSeparatedByString:@"://"];
        
        if([[arr objectAtIndex:0] isEqualToString:@"signup"])
        {
            [[AppDelegate getAppDelegateObj] navigateToRegistration];
        }
        else if([[arr objectAtIndex:0] isEqualToString:@"login"])
        {
            [[AppDelegate getAppDelegateObj] navigateToLoginViewController];

        }
        else if([[arr objectAtIndex:0] isEqualToString:@"pan"])
        {
            [self ebServiceBtnClicked:nil];
        }
        else if([[arr objectAtIndex:0] isEqualToString:@"ebwarranty"])
        {
            WarrantyViewController *objVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
            
            objVc.warrantyBannerClicked = YES;
            [self.navigationController pushViewController:objVc animated:YES];
        }
    }
    return YES;
}

#pragma mark - IBAction methods
- (IBAction)rightSlideMenuAction:(id)sender {
    AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    [objAppdelegate.tabBarObj unhideTabbar];
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (IBAction)backBtnClicked:(id)sender
{
    
    if(!_isFromSellYourGadgetCheckout)
    {
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj unhideTabbar];
    }
    [[APP_DELEGATE tabBarObj] TabClickedIndex:0];
    //[self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)ebServiceBtnClicked:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
    ServiceCenterViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"ServiceCenterViewController"];
    [self.navigationController pushViewController:objVc animated:YES];
}
@end
