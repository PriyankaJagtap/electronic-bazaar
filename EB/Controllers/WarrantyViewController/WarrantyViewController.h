//
//  WarrantyViewController.h
//  EB
//
//  Created by Neosoft on 4/6/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WarrantyViewController : BaseNavigationControllerWithBackBtn<UIWebViewDelegate>
{
        NSURLRequest* request;
}
@property (weak, nonatomic) IBOutlet UIButton *ebServiceBtn;
@property (weak, nonatomic) IBOutlet UILabel *warrantyLabel;
- (IBAction)ebServiceBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ebServiceBtbHtConstraint;


@property (nonatomic, strong) NSString *webViewUrl;
@property (nonatomic, strong) NSString *titleLabel;
@property (nonatomic, strong) NSString *bannerType;
@property (nonatomic) BOOL warrantyBannerClicked;
@property (nonatomic) BOOL isFromSellYourGadgetCheckout;


@end
