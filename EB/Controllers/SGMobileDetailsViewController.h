//
//  SGMobileDetailsViewController.h
//  EB
//
//  Created by webwerks on 11/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGMobileDetailsViewController : SellYourGadgetNavigationViewController
@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelLabel;
@property (weak, nonatomic) IBOutlet UILabel *memoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *conditionLabel;
@property (weak, nonatomic) IBOutlet UILabel *IMEILabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *chargerLabel;

@property (nonatomic, strong) NSDictionary *mobileDetailsDic;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@end
