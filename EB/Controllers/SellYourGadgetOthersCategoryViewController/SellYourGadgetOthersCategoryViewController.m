//
//  SellYourGadgetOthersCategoryViewController.m
//  EB
//
//  Created by Neosoft on 8/3/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellYourGadgetOthersCategoryViewController.h"

@interface SellYourGadgetOthersCategoryViewController ()<FinishLoadingData>

@property (weak, nonatomic) IBOutlet UILabel *conditionLabel;
@end

@implementation SellYourGadgetOthersCategoryViewController

static NSString *conditionHtml = @"*<b>Condition</b>\n\n\u2022<u>Good:</u> Like new (powers on, no missing keys, no scratches, dents or cracks)\n\u2022<u>Fair:</u> Signs of normal use (powers on, some scratches but no dents or cracks, no missing keys)\n\u2022<u>Poor:</u> Powers on/off but has significant scratches, dents, cracked casing, and/or missing keys";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[conditionHtml dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    _conditionLabel.attributedText = attrStr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - IBAction methods
- (IBAction)submitBtnClicked:(id)sender {
    
    if ([_brandTextField.text isEqualToString:@""]) {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please Enter Brand"];
        return;
    }
    
    if ([_modelTextField.text isEqualToString:@""]) {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please Enter Model"];
        return;
    }
    
    if ([_descriptionTextView.text isEqualToString:@""]) {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please Enter Description"];
        return;
    }
    
    [self callOthersWS];
    
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)rightMenuBtnClicked:(id)sender {
}

#pragma mark - call others webservice
-(void)callOthersWS{
    [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
        NSString *paramStr = [NSString stringWithFormat:@"brand=%@&model=%@&description=%@&user_id=%@",_brandTextField.text,_modelTextField.text,_descriptionTextView.text,[defaults valueForKey:SELL_GADGET_LOGIN_USERID]];
        
        [callLoginService webServiceWitParameters:paramStr andURL:SELL_GADGET_OTHRES_BRAND_WS MathodName:SELL_GADGET_OTHRES_BRAND_WS];
    }
    
}



#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:SELL_GADGET_OTHRES_BRAND_WS]){
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            alert.tag = 500;
            [alert show];
            
        }
        else
        {
            [[CommonSettings sharedInstance] showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
    }
    
}


#pragma mark - Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 500 && buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
