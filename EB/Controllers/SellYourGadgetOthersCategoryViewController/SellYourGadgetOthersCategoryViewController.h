//
//  SellYourGadgetOthersCategoryViewController.h
//  EB
//
//  Created by Neosoft on 8/3/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellYourGadgetOthersCategoryViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *brandTextField;
@property (weak, nonatomic) IBOutlet UITextField *modelTextField;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
- (IBAction)submitBtnClicked:(id)sender;
- (IBAction)backBtnClicked:(id)sender;

@end
