//
//  ContactUs.m
//  EB
//
//  Created by webwerks on 9/25/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "ContactUs.h"
#import "Constant.h"
#import "CommonSettings.h"
#import "AppDelegate.h"
#import "FVCustomAlertView.h"

@interface ContactUs ()

@end

@implementation ContactUs
@synthesize objContactUsWebView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    [super setViewControllerTitle:@"Contact Us"];
    NSString *strUrl=@"http://electronicsbazaar.com/mobile-app/contact-us.html";

    NSURL *contactUsUrl=[NSURL URLWithString:strUrl];
    NSURLRequest *urlRequest=[NSURLRequest requestWithURL:contactUsUrl];
    [objContactUsWebView loadRequest:urlRequest];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [CommonSettings sendScreenName:@"ContactUsView"];
    [[AppDelegate getAppDelegateObj].tabBarObj hideTabbar];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Utility methods
//-(void)setUpNavigationBar
//{
//    [self.navigationController setNavigationBarHidden:NO];
//    self.navigationController.navigationBar.translucent=normal;
//    UILabel *titleLabel=[[UILabel alloc]init];
//    titleLabel.text = @"Contact Us";
//    titleLabel.font =karlaFontRegular(18.0);
//    titleLabel.frame = CGRectMake(0, 0, 100, 30);
//    titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleLabel.textColor = [UIColor whiteColor];
//    self.navigationItem.titleView = titleLabel;
//    UIColor *bgColor=[UIColor colorWithRed:23/255.0f green:70/255.0f blue:135/255.0f alpha:1.0f];
//    self.navigationController.navigationBar.barTintColor = bgColor;
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    
//    self.navigationController.navigationBar.backItem.title =@"";
//    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"dropdown_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemPressed)];
//    
//    [[self navigationItem] setRightBarButtonItem:rightItem];
//}

//-(void)rightBarButtonItemPressed
//{
//    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
//}

- (IBAction)onClickOf_Nav_Back:(id)sender {
    [[APP_DELEGATE tabBarObj] TabClickedIndex:0];

    //[self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickOf_Nav_Right_Btn:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}
@end
