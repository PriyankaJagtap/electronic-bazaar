//
//  SelectIMEIViewController.m
//  EB
//
//  Created by webwerks on 03/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SelectIMEIViewController.h"
#import "SortByTableCell.h"
#import "SalesReturnViewController.h"
#import "SalesReturnProductClass.h"

@interface SelectIMEIViewController ()<UIGestureRecognizerDelegate>

@end

@implementation SelectIMEIViewController
@synthesize IMEINumberDataArr;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        self.tableView.estimatedRowHeight = 40;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        tap.delegate = self;
        [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - handle tap
-(void)viewTapped:(UITapGestureRecognizer *)sender
{
        [UIView animateWithDuration:1.0 animations:^ {
                self.view.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
        } completion:^(BOOL finished) {
                [self willMoveToParentViewController:nil];
                [self.view removeFromSuperview];
                [self removeFromParentViewController];
                
        }];
}
#pragma mark - table view methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return IMEINumberDataArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *simpleTableIdentifier = @"SortByTableCell";
        SortByTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
                cell = [[SortByTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.titleLabel.text =[NSString stringWithFormat:@"%@",[IMEINumberDataArr objectAtIndex:indexPath.row]] ;
        
        return cell;
        
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        _objSalesReturn.IMEINo = [NSString stringWithFormat:@"%@",[IMEINumberDataArr objectAtIndex:indexPath.row]];
        _objSalesReturn.returnStatus = [_returnStatusDataArr objectAtIndex:indexPath.row];
        [_deleagte IMEISelected:_objSalesReturn];
        
        
        
        
}

#pragma mark - gesture delegate method
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
        if ([touch.view isDescendantOfView:_tableView]) {
                return NO;
        }
        return YES;
}


@end
