//
//  SaleHistoryDetailViewController.h
//  EB
//
//  Created by Neosoft on 3/23/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaleHistoryDetailViewController : UIViewController
@property (nonatomic, strong) NSString *orderId;
@property (weak, nonatomic) IBOutlet UIButton *conditionBtn;
@property (weak, nonatomic) IBOutlet UILabel *gadgetOderNoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *gadgetImageView;
@property (weak, nonatomic) IBOutlet UIButton *statusBtn;
@property (weak, nonatomic) IBOutlet UILabel *gadgetTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *addressBtn;
@property (weak, nonatomic) IBOutlet UIButton *priceBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressBtnHt;
@property (weak, nonatomic) IBOutlet UIButton *orderBtn;

@property (weak, nonatomic) IBOutlet UIButton *chargerBtn;

@end
