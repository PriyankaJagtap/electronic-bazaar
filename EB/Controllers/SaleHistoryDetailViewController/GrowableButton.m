//
//  GrowableButton.m
//  EB
//
//  Created by Neosoft on 3/23/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "GrowableButton.h"

@implementation GrowableButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(CGSize)intrinsicContentSize {
    return CGSizeMake(self.frame.size.width, self.titleLabel.frame.size.height);
}
@end
