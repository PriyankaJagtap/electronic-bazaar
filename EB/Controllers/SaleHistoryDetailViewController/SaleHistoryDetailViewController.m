//
//  SaleHistoryDetailViewController.m
//  EB
//
//  Created by Neosoft on 3/23/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SaleHistoryDetailViewController.h"

@interface SaleHistoryDetailViewController ()<FinishLoadingData>

@end

@implementation SaleHistoryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self getSalesHistoryDeatils];
    [[CommonSettings sharedInstance] setShadow:_gadgetOderNoLabel];
    [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:4];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - get sales history deatils
-(void)getSalesHistoryDeatils
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        //NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",SELL_GADGET_HISTORY_DETAILS_WS,_orderId] MathodName:SELL_GADGET_HISTORY_DETAILS_WS];
        
    }
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:SELL_GADGET_HISTORY_DETAILS_WS]){
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            
            NSDictionary *dic = [response valueForKey:@"data"];
            [self.addressBtn setTitle:[dic valueForKey:@"address"] forState:UIControlStateNormal];
            [self.addressBtn layoutIfNeeded]; // need this to update the button's titleLabel's size
            self.addressBtnHt.constant = self.addressBtn.titleLabel.frame.size.height+10;
           
            [_orderBtn setTitle:[dic valueForKey:@"id"] forState:UIControlStateNormal];
            _gadgetTitleLabel.text = [dic valueForKey:@"proname"];
            NSString *price = [[CommonSettings sharedInstance] formatPrice:[[dic valueForKey:@"price"] floatValue]];
            [_priceBtn setTitle:price forState:UIControlStateNormal];
            [_statusBtn setTitle:[dic valueForKey:@"status"] forState:UIControlStateNormal];
               [_gadgetImageView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"image"]]];
            
            NSDictionary *conditionDic = [[dic valueForKey:@"options"] objectAtIndex:0];
            [_conditionBtn setTitle:[conditionDic valueForKey:@"Condition"] forState:UIControlStateNormal];
            
            if ([[conditionDic valueForKey:@"Charger"] isEqualToString:@"Yes"]) {
                [_chargerBtn setTitle:@"Available" forState:UIControlStateNormal];
            }
            else
            {
                [_chargerBtn setTitle:@"Not Available" forState:UIControlStateNormal];
            }
            
        }
        else{
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
        
    }
}
#pragma mark -IBActions

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
