//
//  ServiceCenterAddressTableCell.m
//  EB
//
//  Created by Neosoft on 4/26/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "ServiceCenterAddressTableCell.h"

@implementation ServiceCenterAddressTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setUpCellWithData:(NSDictionary *)dic
{
    self.serviceCenterNameLabel.text = [dic valueForKey:@"asp_name"];
    self.addressLabel.text = [dic valueForKey:@"address"];
    self.nameLabel.text = [dic valueForKey:@"contact_person"];
    self.mobileNumberLabel.text = [dic valueForKey:@"contact_number"];
    self.emailLabel.text = [dic valueForKey:@"mail_id"];

}
@end
