//
//  ServiceCenterViewController.h
//  EB
//
//  Created by Aishwarya Rai on 15/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "BaseNavigationControllerWithBackBtn.h"

@interface ServiceCenterViewController : BaseNavigationControllerWithBackBtn <UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) IBOutlet UITableView *tblView;


@end
