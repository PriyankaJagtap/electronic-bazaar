//
//  ServiceCenterViewController.m
//  EB
//
//  Created by Aishwarya Rai on 15/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "ServiceCenterViewController.h"
#import "SerialTableViewCell.h"
#import "ServiceCenterAddressTableCell.h"
#import "AppDelegate.h"
#import "Constant.h"
#define NO_SERVICE_CENTER_AVAILABLE_TEXT @"Sorry, No EB Service Partner Center near your location"

@interface ServiceCenterViewController ()<FinishLoadingData,UITextFieldDelegate>
{
    UITextField *cityTextField;
    NSArray *serviceCenterDataArr;
    NSTimer *timer;
    UIView *footerView;
}

@end

@implementation ServiceCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _tblView.estimatedRowHeight = 124;
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedSectionHeaderHeight = 46;
    _tblView.sectionHeaderHeight = UITableViewAutomaticDimension;
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [gestureRecognizer setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:gestureRecognizer];
    
      [super setViewControllerTitle:@"EB Service Center"];
    [self initFooterView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [APP_DELEGATE.tabBarObj hideTabbar];
  
}
- (void) hideKeyboard {
    if([cityTextField isFirstResponder])
        [cityTextField resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - set table footer view
-(void)initFooterView
{
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, kSCREEN_WIDTH, 60.0)];
    UILabel *noDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, kSCREEN_WIDTH -20, 40)];
    noDataLabel.numberOfLines = 0;
    noDataLabel.font = [UIFont fontWithName:@"Karla-Regular" size:14];
    noDataLabel.text = NO_SERVICE_CENTER_AVAILABLE_TEXT;
    [footerView addSubview:noDataLabel];
}
#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return serviceCenterDataArr.count;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    static NSString *cellIdentifier=@"SerialTableViewCell";
    SerialTableViewCell *cell=(SerialTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[SerialTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [[CommonSettings sharedInstance] setBorderToView:cell.serialNumberTxtField];
    if(cityTextField == nil)
        cityTextField = cell.serialNumberTxtField;
    else
        cell.serialNumberTxtField.text = cityTextField.text;
    
    cell.serialNumberTxtField.delegate = self;
    cityTextField.delegate= self;
    [cityTextField addTarget:self action:@selector(cityTextFieldValueChanged) forControlEvents:UIControlEventEditingChanged];
    
    [cell.forwordArrowBtn addTarget:self action:@selector(cityTextFieldBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    return cell.contentView;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"ServiceCenterAddressTableCell";
    ServiceCenterAddressTableCell*cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[ServiceCenterAddressTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    cell.selectionStyle = UITableViewCellEditingStyleNone;
    [[CommonSettings sharedInstance] setBorderToView:cell.bgView];
    NSDictionary *dic = [serviceCenterDataArr objectAtIndex:indexPath.row];
    [cell setUpCellWithData:dic];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark -  UITextfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return [[CommonSettings sharedInstance] allParticularCharInTextFieldWithString:string acceptableCharStr:ACCEPTABLE_CHARACTERS];
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



#pragma mark - Get service center data
-(void)getServiceCenterData
{
    Webservice  *callProductDetailService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    
    
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        //        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
        callProductDetailService.delegate =self;
        //        NSString *urlStr = [NSString stringWithFormat:@"%@%@",GET_SERVICE_CENTER_DATA_WS, [cityTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        NSString *city = [cityTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@",GET_SERVICE_CENTER_DATA_WS, city];
        [callProductDetailService GetWebServiceWithURL:urlStr MathodName:GET_SERVICE_CENTER_DATA_WS];
        
    }
    
}

#pragma mark - city text changed
-(void)cityTextFieldValueChanged
{
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(getServiceCenterData) userInfo:nil repeats:NO];
}
#pragma mark - IBAction methods
-(void)cityTextFieldBtnClicked
{
    
    if([cityTextField.text isEqualToString:@""])
    {
        [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Please enter city or pincode"];
    }
    else
    {
        [self.view endEditing:YES];
        [self getServiceCenterData];
    }
}
- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - webservice delegate method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([[response valueForKey:@"status"]intValue]==1) {
        serviceCenterDataArr = [response valueForKey:@"data"];
        
        [_tblView reloadData];
        if(serviceCenterDataArr.count != 0)
            _tblView.tableFooterView = nil;
        else
            _tblView.tableFooterView = footerView;
    }
    else{
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
    }
}

@end
