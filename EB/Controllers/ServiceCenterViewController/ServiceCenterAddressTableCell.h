//
//  ServiceCenterAddressTableCell.h
//  EB
//
//  Created by Neosoft on 4/26/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceCenterAddressTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceCenterNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
-(void)setUpCellWithData:(NSDictionary *)dic;

@end
