//
//  SuperSaleViewController.h
//  EB
//
//  Created by Neosoft on 2/14/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperSaleViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *mobileNoTextField;
- (IBAction)submitBtnClicked:(id)sender;
- (IBAction)backBtnClicked:(id)sender;
- (IBAction)rightSideMenuBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *thankYouView;
- (IBAction)goBackBtnClicked:(id)sender;

@end
