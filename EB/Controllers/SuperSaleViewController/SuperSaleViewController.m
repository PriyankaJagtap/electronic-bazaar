//
//  SuperSaleViewController.m
//  EB
//
//  Created by Neosoft on 2/14/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SuperSaleViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "AppDelegate.h"
#define MAX_LENGTH 10


@interface SuperSaleViewController ()<UITextFieldDelegate,FinishLoadingData>

@end

@implementation SuperSaleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _mobileNoTextField.inputAccessoryView = [self setToolBarNumberKeyBoard:self];
    [self createWrapView:_mobileNoTextField];
    [self createWrapView:_nameTextField];
    [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:4];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TextField delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}




- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if(textField == _nameTextField)
    {
        return YES;
    }
    else if(textField == _mobileNoTextField)
    {
        NSUInteger newLength = [textField.text length] + [string length];
        
        if(newLength > MAX_LENGTH){
            
            return NO;
        }
        else{
            return YES;
        }
    }
    return YES;
}

#pragma mark - IBACtion methods

- (IBAction)submitBtnClicked:(id)sender {
    [self.view endEditing:YES];
    
    if (_nameTextField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter name."];
        return;
    }
    
    
    if (_mobileNoTextField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit Mobile Number."];
        return;
    }

    
    if (_mobileNoTextField.text){
        if (_mobileNoTextField.text.length < 10) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit Mobile Number."];
            return;
        }
    }
    
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:NO];
    
    // Call Home screen webservice
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        NSString *str = [NSString stringWithFormat:@"%@name=%@&mobile=%@",SAVE_SUPER_SALE_WS,_nameTextField.text,_mobileNoTextField.text];
        [callLoginService GetWebServiceWithURL:str MathodName:SAVE_SUPER_SALE_WS];
    }
}


- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightSideMenuBtnClicked:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (IBAction)goBackBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

-(UIToolbar *)setToolBarNumberKeyBoard:(id)sender
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:sender action:@selector(doneButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Done" forState:UIControlStateNormal];
    button.frame = CGRectMake(0 ,0,60,40);
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 40)];
    [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
    
    return toolbar;
}

-(void)doneButtonDidPressed:(id)sender
{
    [_mobileNoTextField resignFirstResponder];
    [_scrollView setContentOffset:CGPointMake(0, 0)];
}
-(void)createWrapView:(UITextField *)textfield{
    UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 10, textfield.frame.size.height)];
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = wrapView;
}

#pragma mark - webservice delegate 
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    [FVCustomAlertView hideAlertFromView:self.view fading:NO];

    if ([strName isEqualToString:SAVE_SUPER_SALE_WS]){
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            [self dispalyThankYouPage];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
       
    }
}


-(void)dispalyThankYouPage {
    
    [_thankYouView setHidden:NO];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.4f;
    transition.type = kCATransitionMoveIn;
//  transition.subtype = kCATransitionFromTop;
    transition.subtype = kCATransitionFromLeft;
    [_thankYouView.layer addAnimation:transition forKey:nil];
   
    
    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 1.0f;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionFade;
//    [_thankYouView.layer addAnimation:transition forKey:nil];
    
//    double delayInSeconds = 5.0f;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        
//        CATransition *transition = [CATransition animation];
//        transition.duration = 2.0f;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionFade;
//        [_thankYouView.layer addAnimation:transition forKey:nil];
//        [_thankYouView setHidden:YES];
//    });
}
#pragma mark - UIAlertView delegate

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    
//    [self.navigationController popViewControllerAnimated:YES];
//}

@end
