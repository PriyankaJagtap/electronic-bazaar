//
//  ApplyCoupon.m
//  EB
//
//  Created by webwerks on 10/31/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "ApplyCoupon.h"
#import "Webservice.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "CommonSettings.h"
#import "Checkout.h"




@interface ApplyCoupon ()<FinishLoadingData>

@end

@implementation ApplyCoupon
@synthesize isAffiliateUser,mobileNo,emailId;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [couponResultBgView setHidden:YES];
    if (isAffiliateUser) {
        _appDiscountlbl.text = APP_DISCOUNT_LABEL;
    }
    [self callCouponListing];
}

- (IBAction)onClickOfProceed_To_Payment:(id)sender {
    [self.view endEditing:YES];
    

    
    Checkout *objCheckout = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Checkout"];
    objCheckout.userMobileNo=mobileNo;
    objCheckout.isAffiliateUser=isAffiliateUser;
    objCheckout.userEmailId=emailId;
    [self.navigationController pushViewController:objCheckout animated:YES];
}

- (IBAction)onClickOfDecline_Coupon:(id)sender{
    [self removeCouponRequest];
    
    
}

- (IBAction)onClickOfApply_Coupon:(id)sender{
    [self.view endEditing:YES];
    if (![coupont_Txt.text isEqualToString:@""]) {
        [self addCouponRequest];
    }else{
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter coupon code."];
    }

}

- (IBAction)onClickOfRight_Nav_Button:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
- (IBAction)onClickOfLeft_Nav_Button:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if([strName isEqualToString:@"ADDCOUPONCODE"]){
        if (response) {
            [couponResultBgView setHidden:NO];
            [couponResultBgView setHidden:NO];
            if ([[response valueForKey:@"status"]intValue]==1) {
                lblResult.text=@"Coupon is applied";
                [lblResult setTextColor:[UIColor lightGrayColor]];
                declineCouponButton.hidden=NO;
                [self setSummary:response];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Coupon applied successfully!"];
                });
            }else{
                lblResult.text=@"Coupon code is not valid";
                [lblResult setTextColor:[UIColor redColor]];
                declineCouponButton.hidden=YES;
                [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Coupon code is not valid!"];

            }
        }
    }
    else if([strName isEqualToString:@"COUPONLISTING"]){
        [self setSummary:response];
    
    }else if ([strName isEqualToString:@"REMOVECOUPONCODE"]){
        [self setSummary:response];
        [couponResultBgView setHidden:YES];
    }
}

-(void)setSummary:(id)response
{
    base_Price_Lbl.text=[NSString stringWithFormat:@"%ld",(long)[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"subtotal"]integerValue]];
    total_Payble_Lbl.text=[NSString stringWithFormat:@"%ld",(long)[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"grand_total"]integerValue]];
        
    vat_Price_Lbl.text=[NSString stringWithFormat:@"%ld",(long)[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"tax_amount"]integerValue]];

}


-(void)callCouponListing
{
    Webservice  *getCouponListing = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        getCouponListing.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int userId=[[[defaults valueForKey:@"user"]valueForKey:@"user_id"]intValue];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        if (quoteID!=0)
        {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            NSString *param=[NSString stringWithFormat:@"quoteId=%d&user_id=%d",quoteID,userId];
            
            [getCouponListing webServiceWitParameters:param andURL:COUPON_LISTING MathodName:@"COUPONLISTING"];
        }
    }
}
-(void)addCouponRequest
{
    Webservice  *addCouponWs = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        addCouponWs.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        if (quoteID !=0)
        {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            NSString *param=[NSString stringWithFormat:@"quote_id=%d&couponCode=%@",quoteID,coupont_Txt.text];
            
            [addCouponWs webServiceWitParameters:param andURL:ADD_COUPON_CODE MathodName:@"ADDCOUPONCODE"];
        }
    }
}

-(void)removeCouponRequest
{
    Webservice  *removeCouponWs = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        removeCouponWs.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        if (quoteID !=0)
        {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            NSString *param=[NSString stringWithFormat:@"quote_id=%d",quoteID];
            [removeCouponWs webServiceWitParameters:param andURL:REMOVE_COUPON MathodName:@"REMOVECOUPONCODE"];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
