//
//  ApplyCoupon.h
//  EB
//
//  Created by webwerks on 10/31/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplyCoupon : UIViewController<UITextFieldDelegate>{
    __weak IBOutlet UILabel *base_Price_Lbl;
    __weak IBOutlet UILabel *vat_Price_Lbl;
    __weak IBOutlet UILabel *total_Payble_Lbl;
    __weak IBOutlet UIView *couponResultBgView;
    __weak IBOutlet UITextField *coupont_Txt;
    __weak IBOutlet UILabel *lblResult;
    __weak IBOutlet UIButton *declineCouponButton;
}


@property (weak, nonatomic) IBOutlet UILabel *appDiscountlbl;
@property(strong,nonatomic)NSString *mobileNo;
@property(strong,nonatomic)NSString *emailId;
@property(nonatomic)BOOL isAffiliateUser;

- (IBAction)onClickOfProceed_To_Payment:(id)sender;
- (IBAction)onClickOfRight_Nav_Button:(id)sender;
- (IBAction)onClickOfLeft_Nav_Button:(id)sender;
- (IBAction)onClickOfApply_Coupon:(id)sender;
- (IBAction)onClickOfDecline_Coupon:(id)sender;



@end
