//
//  TrackingDetailsViewController.h
//  EB
//
//  Created by webwerks on 16/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackingDetailsViewController : BaseNavigationControllerWithBackBtn
- (IBAction)backBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (nonatomic, strong) NSArray *trackingDataArr;

@end
