//
//  TrackingDetailsHeaderTableCell.h
//  EB
//
//  Created by webwerks on 16/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackingDetailsHeaderTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
