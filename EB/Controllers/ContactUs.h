//
//  ContactUs.h
//  EB
//
//  Created by webwerks on 9/25/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUs : BaseNavigationControllerWithBackBtn<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *objContactUsWebView;
- (IBAction)onClickOf_Nav_Back:(id)sender;
- (IBAction)onClickOf_Nav_Right_Btn:(id)sender;

@end
