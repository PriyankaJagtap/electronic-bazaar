//
//  PopupViewController.m
//  EB
//
//  Created by webwerks on 26/02/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "PopupViewController.h"
#import "ProductDetailsViewController.h"


@interface PopupViewController ()

@end

@implementation PopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        if(_homePopupResponseDic != nil)
        {
                //[_productImageView sd_setImageWithURL:[NSURL URLWithString:[_homePopupResponseDic valueForKey:@"img_url"]]];
                [_productImageView sd_setImageWithURL:[NSURL URLWithString:[_homePopupResponseDic valueForKey:@"img_url"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        _closeBtn.hidden = NO;
                }];
        }
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
        
}
- (IBAction)closeBtnClicked:(id)sender {

        [self removeFromSuperParentViewController];
}

-(void)removeFromSuperParentViewController
{
        [self willMoveToParentViewController:nil];
        [self removeFromParentViewController];
        [self.view removeFromSuperview];
}
- (IBAction)productImageClicked:(id)sender {
        
         NSLog(@"image clicked");
         [CommonSettings eventTrackingForHomeScreen:[_homePopupResponseDic valueForKey:@"event_click_label"]];
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ProductDetailsViewController *objProductDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
        int productId=[[_homePopupResponseDic valueForKey:@"product_id"] intValue ];
        objProductDetailVC.productID=productId;
        [self.parentViewController.navigationController pushViewController:objProductDetailVC animated:YES];
        
        [self removeFromSuperParentViewController];
        
}
@end
