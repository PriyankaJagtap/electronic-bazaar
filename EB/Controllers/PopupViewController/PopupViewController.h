//
//  PopupViewController.h
//  EB
//
//  Created by webwerks on 26/02/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (strong, nonatomic) NSDictionary *homePopupResponseDic;
- (IBAction)closeBtnClicked:(id)sender;
- (IBAction)productImageClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@end
