//
//  SaleBackCheckoutViewController.h
//  EB
//
//  Created by webwerks on 16/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaleBackCheckoutViewController : SellYourGadgetNavigationViewController


- (IBAction)chekOutBtnClicked:(id)sender;
@property (nonatomic,weak) IBOutlet UITableView *tblView;
@property (nonatomic,strong) NSDictionary *productDetailDic;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblBottomConstraint;
@property (nonatomic,strong) NSString *productType;;
- (IBAction)acceptTermsAndConditionBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;

@property (nonatomic,strong) NSDictionary *mobileDataDic;
@end
