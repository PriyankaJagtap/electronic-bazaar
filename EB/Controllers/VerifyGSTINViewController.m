//
//  VerifyGSTINViewController.m
//  EB
//
//  Created by webwerks on 04/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "VerifyGSTINViewController.h"
#import "VerifyGSTINTableViewCell.h"
#import "SerialTableViewCell.h"

@interface VerifyGSTINViewController ()<FinishLoadingData,UITextFieldDelegate,UITableViewDataSource>
{
        bool isGSTINEntered;
        UITextField *GSTINTextField;
        NSDictionary *responseDic;
}

@end

@implementation VerifyGSTINViewController

- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        _tblView.estimatedRowHeight = 124;
        _tblView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
        if(textField == GSTINTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 15){
                        
                        return NO;
                }
                else{
                        return YES;
                }
        }
        
        return YES;
}
#pragma mark - UITableViewDataSource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        
        if (isGSTINEntered) {
                return 2;
        }
        else
                return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        
        if(indexPath.row == 0)
        {
                static NSString *simpleTableIdentifier = @"SerialTableViewCell";
                SerialTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil) {
                        cell = [[SerialTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                        
                }
                cell.selectionStyle = UITableViewCellEditingStyleNone;
                if(GSTINTextField == nil)
                {
                        GSTINTextField = cell.serialNumberTxtField;
                }
                [[CommonSettings sharedInstance] setBorderToView:cell.serialNumberTxtField];
                
                return cell;
        }
        else
        {
                static NSString *simpleTableIdentifier = @"VerifyGSTINTableViewCell";
                VerifyGSTINTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil) {
                        cell = [[VerifyGSTINTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                        
                }
                cell.selectionStyle = UITableViewCellEditingStyleNone;
                [[CommonSettings sharedInstance] setBorderToView:cell.bgView];
                
                cell.partnerStoreNameLabel.text = [responseDic valueForKey:@"partner_store_name"];
                cell.propNameLabel.text = [responseDic valueForKey:@"director_name"];
                cell.addressLine1Label.text = [responseDic valueForKey:@"address_line_1"];
                
                if([responseDic valueForKey:@"address_line_2"])
                {
                        if(![[responseDic valueForKey:@"address_line_2"] isEqualToString:@""])
                                cell.addressLine2Label.text = [responseDic valueForKey:@""];
                        else
                                cell.addressLine2Label.text = @" ";
                }
                cell.postCodeLabel.text = [responseDic valueForKey:@"postcode"];
                cell.cityLabel.text = [responseDic valueForKey:@"city"];
                cell.stateLabel.text = [responseDic valueForKey:@"state"];
                cell.emailLabel.text = [responseDic valueForKey:@"email"];
                cell.mobileNoLabel.text = [responseDic valueForKey:@"mobile"];
                cell.panLabel.text = [responseDic valueForKey:@"pan"];
                
                return cell;
        }
        
}
#pragma mark - IBAction Methods
- (IBAction)leftSlideMenuAction:(id)sender {
        [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
- (IBAction)verifyGSTINTextFieldChanged:(UITextField *)sender {
        
        if(sender.text.length == 15)
        {
                [self getGSTINDetails];
        }
}


#pragma mark - get GSTIN detail
-(void)getGSTINDetails
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                NSString *url = [NSString stringWithFormat:@"%@%@",GSTIN_DETAIL_WS,GSTINTextField.text];
                [callLoginService GetWebServiceWithURL:url MathodName:GSTIN_DETAIL_WS];
                
        }
        
}
#pragma mark - webservice delegate method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        
        if([strName isEqualToString:GSTIN_DETAIL_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1) {
                        responseDic = [response valueForKey:@"data"];
                        isGSTINEntered = YES;
                        [_tblView reloadData];
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
}
@end
