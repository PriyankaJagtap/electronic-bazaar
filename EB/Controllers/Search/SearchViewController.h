//
//  SearchViewController.h
//  EB
//
//  Created by webwerks on 10/23/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//45854600 0544 7827
//05 22 016

#import <UIKit/UIKit.h>
#import "Webservice.h"
#import "Constant.h"



@interface SearchViewController : UIViewController<UISearchBarDelegate,FinishLoadingData,UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UISearchBar *objSearchbar;
    __weak IBOutlet UITableView *objTableView;
    UILabel *lblNoProducts;
    NSMutableArray *productArr;
    
}
@property(strong,nonatomic)UINavigationController *navController;
@property (nonatomic, strong) NSString* searchText;


- (IBAction)leftSlideMenuAction:(id)sender;

@end
