//
//  SearchViewController.m
//  EB
//
//  Created by webwerks on 10/23/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "SearchViewController.h"
#import "AppDelegate.h"
#import "ProductListCell.h"
#import "ProductListViewController.h"
#import "ProductDetailsViewController.h"

@interface SearchViewController ()<UIScrollViewDelegate>
{
        int pageNo;
        UIView *footerView;
        BOOL isLoading,hasNext;
}
@end

@implementation SearchViewController
@synthesize navController,searchText;

- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        objTableView.contentInset = UIEdgeInsetsMake(-33, 0, 0, 0);
        
        if(searchText != nil)
        {
                objSearchbar.text = searchText;
                [self searchBarSearchButtonClicked:objSearchbar];
        }
        
        objSearchbar.backgroundColor = [[AppDelegate getAppDelegateObj] colorWithHexString:APP_THEME_COLOR];
        [objSearchbar setBackgroundImage:[[UIImage alloc]init]];
        
        objSearchbar.delegate=self;
        
}

-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:YES];
        [[[AppDelegate getAppDelegateObj] tabBarObj] hideTabbar];
        
        if(searchText == nil)
                [objSearchbar becomeFirstResponder];
        
}
- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}


- (IBAction)leftSlideMenuAction:(id)sender{
        //    ProductListViewController *prodListObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductListViewController"];
        //    prodListObj.viewAllListTitle.text = @"Hot Deals";
        //    prodListObj.type = @"hotdeals";
        //    prodListObj.page_no = @"1";
        //    prodListObj.page_size = @"10";
        //    //[self.navigationController pushViewController:prodListObj animated:YES];
        //
        //    [self dismissViewControllerAnimated:YES completion:^{
        //        [navController pushViewController:prodListObj animated:YES];
        //    }];
        
        //[self dismissViewControllerAnimated:YES completion:nil];
        [[[AppDelegate getAppDelegateObj] tabBarObj] unhideTabbar];
        [self.navigationController popViewControllerAnimated:NO];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - create cutom footer view
-(void)initFooterView
{
        footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, kSCREEN_WIDTH, 40.0)];
        
        UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        //    actInd.tag = 10;
        actInd.color = [UIColor darkGrayColor];
        
        actInd.frame = CGRectMake(150.0, 5.0, 20.0, 20.0);
        
        actInd.hidesWhenStopped = YES;
        
        UIImageView *rotatingImg = [[UIImageView alloc] initWithFrame:CGRectMake((kSCREEN_WIDTH/2)-15, 5, 30, 30)];
        rotatingImg.image = [UIImage imageNamed:@"rotate_1.png"];
        rotatingImg.tag = 10;
        
        [footerView addSubview:rotatingImg];
        rotatingImg = nil;
        
        
        //    [footerView addSubview:actInd];
        //    actInd = nil;
}

#pragma mark - tableView Delegate and DataSource methods
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
        BOOL endOfTable = (objTableView.contentOffset.y >= ((objTableView.contentSize.height) - objTableView.frame.size.height));
        if(endOfTable && !isLoading && !scrollView.dragging && !scrollView.decelerating && hasNext)
        {
                isLoading = YES;
                objTableView.tableFooterView = footerView;
                //        [(UIActivityIndicatorView *)[footerView viewWithTag:10] startAnimating];
                [CommonSettings rotateLayerInfinite:[(UIImageView *)[footerView viewWithTag:10] layer]];
                
                [self getProductsFromPageNumber:pageNo];
                
        }
        
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *CellIdentifier = @"ProductListCell";
        
        ProductListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
                cell = [[ProductListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.backgroundColor = [UIColor clearColor];
        
        // set corner radius to background view
        cell.bgView.layer.cornerRadius = 10;
        
        //name
        cell.productNameLbl.text = [NSString stringWithFormat:@"%@",[[productArr objectAtIndex:indexPath.row]objectForKey:@"name"]];
        //cell.productNameLbl.textColor = [[AppDelegate getAppDelegateObj]colorWithHexString:@"#4f4f4f"];
        cell.favProdBtn.tag=indexPath.row;
        
        // Price
        cell.productPriceLbl.text = [[CommonSettings sharedInstance] formatPrice:[[[productArr objectAtIndex:indexPath.row]objectForKey:@"price"] floatValue]];
        //****
        NSString *strSpecialPrice=[NSString stringWithFormat:@"Rs %@",[[productArr objectAtIndex:indexPath.row]objectForKey:@"special_price"]];
        
        NSString *priceStr = [[CommonSettings sharedInstance] formatPrice:[[[productArr objectAtIndex:indexPath.row]objectForKey:@"price"] floatValue]];
        
        if ([NSNull null]==[[productArr objectAtIndex:indexPath.row]objectForKey:@"special_price"])
        {
                cell.productPrevPriceLbl.text=@"";
                cell.productPriceLbl.text=priceStr;
        }
        else
        {
                strSpecialPrice=  [[CommonSettings sharedInstance] formatPrice:[[[productArr objectAtIndex:indexPath.row]objectForKey:@"special_price"] floatValue]];
                cell.productPriceLbl.text=[NSString stringWithFormat:@"  %@",strSpecialPrice];
                cell.productPrevPriceLbl.text=priceStr;
        }
        
        //image
        NSString*imageLink = [NSString stringWithFormat:@"%@",[[productArr objectAtIndex:indexPath.row]objectForKey:@"image"]];
        [cell.productImg sd_setImageWithURL:[NSURL URLWithString:imageLink]];
        cell.productImg.contentMode = UIViewContentModeScaleAspectFit;
        
        int isFav=[[[productArr objectAtIndex:indexPath.row]valueForKey:@"is_favourite"]intValue];
        if (isFav)
        {
                [cell.favProdBtn setImage:[UIImage imageNamed:@"favorate_icon_selected"] forState:UIControlStateNormal];
        }
        else
        {
                [cell.favProdBtn setImage:[UIImage imageNamed:@"favorate_icon"] forState:UIControlStateNormal];
        }
        
        int isProductInStock=[[[productArr objectAtIndex:indexPath.row]valueForKey:@"is_instock"]intValue];
        if (isProductInStock)
        {
                cell.outofProductImg.hidden=YES;
                NSInteger qty = [[[productArr objectAtIndex:indexPath.row] valueForKey:@"qty"] integerValue];
                NSInteger max_qty = [[[productArr objectAtIndex:indexPath.row] valueForKey:@"max_qty"] integerValue];
                
                if (qty > max_qty)
                {
                        qty = max_qty;
                }
                if( qty <= 10 && qty != 0)
                {
                        cell.itemLeftLabel.text = [NSString stringWithFormat:@"Hurry, Only %ld left!",qty];
                }
                else
                {
                        cell.itemLeftLabel.text = @"";
                }
        }
        else
        {
                cell.outofProductImg.hidden=NO;
                cell.itemLeftLabel.text = @"";
        }
        return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return productArr.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
        //    if ((indexPath.row == [productArr count] - 1)  && [productArr count] % 12 == 0) {
        //        [self getProductsFromPageNumber:pageNo];
        //    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
        return 90;
}
#pragma mark - Webservice delegate



-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if (pageNo == 1) {
                productArr =[NSMutableArray arrayWithArray:[response valueForKey:@"data"]];
        }
        else{
                [productArr addObjectsFromArray:[response valueForKey:@"data"]];
        }
        
        if(lblNoProducts)
                [lblNoProducts removeFromSuperview];
        if ([[response valueForKey:@"data"] count]==0 ||  [[response valueForKey:@"data"] count] < 12)
        {
                hasNext = NO;
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 40)];
                lbl.textAlignment = NSTextAlignmentCenter;
                lbl.font = [UIFont systemFontOfSize:15];
                lbl.textColor = [UIColor blackColor];
                lbl.text = @"No More Data";
                
                pageNo +=  1;
                isLoading = YES;
                //               [(UIActivityIndicatorView *)[footerView viewWithTag:10] stopAnimating];
                [[(UIImageView *)[footerView viewWithTag:10] layer] removeAllAnimations];
                [(UIImageView *)[footerView viewWithTag:10] removeFromSuperview];
                // [footerView addSubview:lbl];
                [objTableView reloadData];
                
        }
        else
        {
                pageNo += 1;
                isLoading = NO;
                //                [(UIActivityIndicatorView *)[footerView viewWithTag:10] stopAnimating];
                [[(UIImageView *)[footerView viewWithTag:10] layer] removeAllAnimations];
                
                objTableView.tableFooterView = nil;
                [objTableView reloadData];
        }
        if (productArr.count==0) {
                isLoading = NO;
                hasNext = YES;
                lblNoProducts=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 300, 40)];
                lblNoProducts.text=@"Product not found.";
                lblNoProducts.textAlignment=NSTextAlignmentCenter;
                lblNoProducts.center=self.view.center;
                [self.view addSubview:lblNoProducts];
        }
        
}

#pragma mark - UISearchBar delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
        [lblNoProducts removeFromSuperview];
        [self.view endEditing:YES];
        productArr=[[NSMutableArray alloc]init];
        footerView = nil;
        [self initFooterView];
        hasNext = YES;
        [self getProductsFromPageNumber:1];
}

//Websrvice call
-(void)getProductsFromPageNumber:(int)page_No
{
        Webservice  *callSearchWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                pageNo = page_No;
                callSearchWs.delegate =self;
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                NSString *strParam;
                if (userId==0)
                        //            strParam=[NSString stringWithFormat:@"user_id=&keyword=%@&page_size=12&page_no=%d",objSearchbar.text,page_No];
                        strParam=[NSString stringWithFormat:@"user_id=&keyword=%@&pagesize=12&pageno=%d",objSearchbar.text,page_No];
                else
                        //            strParam=[NSString stringWithFormat:@"user_id=%d&keyword=%@&page_size=12&page_no=%d",userId,objSearchbar.text,page_No];
                        
                        strParam=[NSString stringWithFormat:@"user_id=%d&keyword=%@&pagesize=12&pageno=%d",userId,objSearchbar.text,page_No];
                if(pageNo == 1)
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callSearchWs GetWebServiceWithURL:[NSString stringWithFormat:@"%@?%@",SEARCH_PRODUCTS,strParam] MathodName:@"SEARCHPRODUCTS"];
        }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
        int prodId = [[[productArr objectAtIndex:indexPath.row]valueForKey:@"product_id"]intValue];
        objProductDetailVC.productID=prodId;
        [[[AppDelegate getAppDelegateObj] tabBarObj] unhideTabbar];
        [self.navigationController pushViewController:objProductDetailVC animated:YES];
}

@end
