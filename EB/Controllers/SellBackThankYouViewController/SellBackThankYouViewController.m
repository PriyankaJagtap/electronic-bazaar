//
//  SellBackThankYouViewController.m
//  EB
//
//  Created by webwerks on 19/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "SellBackThankYouViewController.h"

@interface SellBackThankYouViewController ()

@end

@implementation SellBackThankYouViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        _messageLabel.text = _message;
          [super setViewControllerTitle:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - IBAction methods
- (IBAction)goToHomeBtnClicked:(id)sender {
        
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj unhideTabbar];
        [CommonSettings navigateToSellYourGadgetView];
}
@end
