//
//  SellBackThankYouViewController.h
//  EB
//
//  Created by webwerks on 19/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellBackThankYouViewController : SellYourGadgetNavigationViewController
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) NSString *message;

@end
