//
//  WebViewController.h
//  EB
//
//  Created by Neosoft on 9/13/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>
@property (nonatomic, strong) NSString *webViewUrl;

@end
