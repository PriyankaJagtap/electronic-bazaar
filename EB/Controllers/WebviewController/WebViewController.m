//
//  WebViewController.m
//  EB
//
//  Created by Neosoft on 9/13/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_webViewUrl]] ];
    [self.navigationController.navigationBar setHidden:YES];
    //[_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]] ];
    [[CommonSettings sharedInstance] displayLoading];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - web view delegate methods
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [[CommonSettings sharedInstance] removeLoading];
    
    NSLog(@"finish");
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[CommonSettings sharedInstance] removeLoading];
    
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    return YES;
}
@end
