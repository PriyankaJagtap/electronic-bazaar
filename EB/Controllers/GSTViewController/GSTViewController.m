//
//  GSTViewController.m
//  EB
//
//  Created by Neosoft on 6/16/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "GSTViewController.h"
#define MAX_LENGTH 10
#import <QuartzCore/QuartzCore.h>

@interface GSTViewController ()<UITextFieldDelegate, UIGestureRecognizerDelegate,FinishLoadingData>

@end

@implementation GSTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _mobileNoTextField.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
    [self createWrapView:_partnerStoreNameTxtField];
    [self createWrapView:_emailIdTextField];
    [self createWrapView:_mobileNoTextField];
    [self createWrapView:_gstNumberTextField];
    [self createWrapView:_contactNameTextField];
    [self createWrapView:_posCodeTextField];
    [self createWrapView:_cityTextField];
    [self createWrapView:_stateTextField];
    [self createWrapView:_countryTextField];
    [self createWrapView:_directorNameTextField];
    [self createWrapView:_address1TextField];
    [self createWrapView:_address2TextField];
    [self createWrapView:_emailIdTextField];
    [self createWrapView:_mobileNoTextField];
    [self createWrapView:_gstNumberTextField];
    
    [_posCodeTextField addTarget:self
                         action:@selector(pincodeValueChanged:)
               forControlEvents:UIControlEventEditingChanged];
   
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
//    tap.delegate = self;
//    [self.view addGestureRecognizer:tap];
}


#pragma mark - method to get city from post code

-(void) pincodeValueChanged:(id)sender {
    // your code
    
    if(_posCodeTextField.text.length  == 6)
    {
        [self getCityFromPincode];
    }
    else
    {
       _cityTextField.text = @"";
       _stateTextField.text = @"";
       _countryTextField.text = @"";
        
        [self.view layoutIfNeeded];
        
        _countryCityStateViewHtConstraint.constant = 0;
        [UIView animateWithDuration:1
                         animations:^{
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
   
}

-(void)getCityFromPincode
{
    
    [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        NSDictionary *userdata = [[NSDictionary alloc] initWithObjectsAndKeys:_posCodeTextField.text,@"pincode", nil];
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:userdata url:GET_CITY_FROM_PINCODE string:@"GET_CITY_FROM_PINCODE"];
        
    }
}



-(void)receivedResponseForCityData:(id)receiveData stringResponse:(NSString*)responseType
{
    [FVCustomAlertView hideAlertFromView:self.view fading:NO];
    NSData *receiveLoginData = [NSData dataWithData:receiveData];
    id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
    NSMutableDictionary *dictUserDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
    
    NSLog(@"userDetail %@",jsonObject);
    if([[dictUserDetail valueForKey:@"status"] boolValue])
    {
        _cityTextField.text = [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"city"];
        _stateTextField.text = [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"default_name"];

        
        NSString *identifier = [NSLocale localeIdentifierFromComponents: [NSDictionary dictionaryWithObject: [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"country_code"] forKey: NSLocaleCountryCode]];
        NSString *country = [[NSLocale currentLocale] displayNameForKey: NSLocaleIdentifier value: identifier];
        
        
        _countryTextField.text = country;

        [self.view layoutIfNeeded];
        _countryCityStateViewHtConstraint.constant = 195;
        [UIView animateWithDuration:1
                         animations:^{
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
    else
    {
        [APP_DELEGATE showAlert:@"" withMessage:[dictUserDetail valueForKey:@"message"]];
        _cityTextField.text = @"";
        _stateTextField.text = @"";
        _countryTextField.text = @"";
    }
    
    
}

#pragma mark - manage UX method


-(void)createWrapView:(UITextField *)textfield{
    UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, textfield.frame.size.height)];
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = wrapView;
}


-(void)doneButtonDidPressed:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_gstNumberTextField becomeFirstResponder];
        
    });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TextField Delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[scrollView setContentOffset:CGPointMake(scrollView.frame.origin.x, 0) animated:YES];
    
    [textField resignFirstResponder];
    return  YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //    if(textField == mobileTxt)
    //    {
    //        if (![self validatePhoneNumber:mobileTxt.text]) {
    //            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please add a valid Mobile Number"];
    //            return;
    //        }
    //    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(textField == _mobileNoTextField)
        {
            if(textField.text.length < 10)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
        }
        else if (textField == _emailIdTextField)
        {
            if (_emailIdTextField.text.length == 0) {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
                return;
            }
            if (![self validEmail:_emailIdTextField.text]) {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
                return;
                
            }
            
        }
        
    });
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if(textField == _emailIdTextField)
    {
        return YES;
    }
    else if(textField == _mobileNoTextField)
    {
        NSUInteger newLength = [textField.text length] + [string length];
        
        if(newLength > MAX_LENGTH){
            
            return NO;
        }
        else{
            return YES;
        }
    }
    
    else if(textField == _posCodeTextField)
    {
        NSUInteger newLength = [textField.text length] + [string length];
        
        if(newLength > 6){
            
            return NO;
        }
        else{
            return YES;
        }
    }
    else if(textField == _gstNumberTextField)
    {
        NSUInteger newLength = [textField.text length] + [string length];
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_GST_NUMBER_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if(newLength <= 15)
        {
            return [string isEqualToString:filtered];
        }
        
        if(newLength > 15){
            
            return NO;
        }
    }
    
    else if (textField == _panNumberTextField)
    {
    
        NSUInteger newLength = [textField.text length] + [string length];
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_PAN_NUMBER_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if(newLength <= 10)
        {
            return [string isEqualToString:filtered];
        }
        
        if(newLength > 10){
            
            return NO;
        }

    }
    
    return YES;
  
}

#pragma mark - Email/ Phone Validation

-(BOOL)validEmail:(NSString*)emailString
{
    NSString *emailid = emailString;
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL myStringMatchesRegEx=[emailTest evaluateWithObject:emailid];
    
    if(myStringMatchesRegEx)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(BOOL)validatePhoneNumber:(NSString *)enterNumber
{
    NSString *phoneRegex = @"[12356789][0-9]{6}([0-9]{3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:enterNumber];
    return matches;
}


#pragma mark - handle tap
-(void)viewTapped:(UITapGestureRecognizer *)sender
{
   [self removeView];
}


-(void)removeView
{
    [UIView animateWithDuration:1.0 animations:^ {
        self.view.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        
    }];
}
#pragma mark - gesture delegate method
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
//{
//    if ([touch.view isDescendantOfView:_bgView]) {
//        return NO;
//    }
//    return YES;
//}
#pragma mark - IBAction methods
- (IBAction)rightSlideAction:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (IBAction)backAction:(id)sender
{
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        ShowTabbars *tabBarController = self.menuContainerViewController.centerViewController;
        UINavigationController *navigationController1 = (UINavigationController* )tabBarController.selectedViewController;
        
        
        HomeViewController *homeObjc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
        NSArray *controllers = [NSArray arrayWithObject:homeObjc];
        navigationController1.viewControllers = controllers;
    }
    
    
    
}

- (IBAction)submitBtnClicked:(id)sender {
    
    [self.view endEditing:NO];
    
    
    if (_partnerStoreNameTxtField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter partner store name."];
        return;
    }
    
    if (_directorNameTextField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Prop/Partner/Director Name."];
        return;
    }
    
    if (_contactNameTextField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Contact Name."];
        return;
    }
  
    
    if (_address1TextField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Address Line 1."];
        return;
    }
    
    
    if (_address2TextField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Address line 2."];
        return;
    }
   
    
    
    if (_posCodeTextField.text.length != 0){
        if (_posCodeTextField.text.length < 6) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 6 Digit Post Code."];
            return;
        }
        if(_posCodeTextField.text.length == 6 && _cityTextField.text.length == 0)
        {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide valid Post Code."];
            return;
        }
    }
    else
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Post Code."];
        return;
    
    }
    
    
    if (_emailIdTextField.text){
        
        if (_emailIdTextField.text.length == 0) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
            return;
        }
        if (![self validEmail:_emailIdTextField.text]) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
            return;
            
        }
    }
    
    if (_mobileNoTextField.text.length != 0){
        if (_mobileNoTextField.text.length < 10) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit Mobile Number"];
            return;
        }
    }
    else
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit Mobile Number"];
        return;
    }
    
    if (_gstNumberTextField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 15 Digit GST Identification number."];
        return;
    }
    else if(_gstNumberTextField.text.length < 15)
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 15 Digit GST Identification number."];
        return;
    }
    
    
//    if (_gstNumberTextField.text){
//        
//        if (![CommonSettings validateGSTNumber:_gstNumberTextField.text]) {
//            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check GST Identification Number format."];
//            return;
//            
//        }
//    }
    
    if (_panNumberTextField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter PAN number."];
        return;
    }else if(_panNumberTextField.text.length < 10)
    {
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit PAN number."];
        return;
    }
    
    
    
    if (_panNumberTextField.text){
       
        if (![CommonSettings validatePanCardNumber:_panNumberTextField.text]) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check PAN  Number format."];
            return;
            
        }
    }
    [self saveGstData];
 
}

#pragma mark - call GST webservice
-(void)saveGstData
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        //store_name=test&email=xyz@gmail.com&mobile=9867447960&gstin=123456789562&user_id=0
        
        
        // Check for User_id
        NSString *strUser_id = [[[NSUserDefaults standardUserDefaults]objectForKey:@"user"]objectForKey:@"user_id"];
        
        if(strUser_id.length == 0)
            strUser_id = @"0";

//        director_name,contact_name,address_line1,
//        address_line2,post_code,city,state,country,pan_number
        
        NSString *urlStr = [NSString stringWithFormat:@"%@store_name=%@&email=%@&mobile=%@&gstin=%@&user_id=%@&director_name=%@&contact_name=%@&address_line1=%@&address_line2=%@&post_code=%@&city=%@&state=%@&country=%@&pan_number=%@",GST_WS,_partnerStoreNameTxtField.text,_emailIdTextField.text,_mobileNoTextField.text,_gstNumberTextField.text,strUser_id,_directorNameTextField.text,_contactNameTextField.text,_address1TextField.text,_address2TextField.text,_posCodeTextField.text,_cityTextField.text,_stateTextField.text,_countryTextField.text,_panNumberTextField.text];
        [callLoginService GetWebServiceWithURL:urlStr MathodName:GST_WS];
        
    }
}


#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    
    if ([strName isEqualToString:GST_WS]){
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:NO] forKey:SHOW_GSTIN];
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:REMIND_ME_LATER_GSTIN];
        }
        else
        {
            [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
        }
        
    }
    
}


@end
