//
//  GSTViewController.h
//  EB
//
//  Created by Neosoft on 6/16/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSTViewController : BaseNavigationControllerWithBackBtn
@property (weak, nonatomic) IBOutlet UITextField *mobileNoTextField;
@property (weak, nonatomic) IBOutlet UITextField *gstNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *gstNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *partnerStoreNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *emailIdTextField;
- (IBAction)submitBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *directorNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *contactNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *address1TextField;
@property (weak, nonatomic) IBOutlet UITextField *address2TextField;
@property (weak, nonatomic) IBOutlet UITextField *posCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *countryTextField;
@property (weak, nonatomic) IBOutlet UITextField *stateTextField;
@property (weak, nonatomic) IBOutlet UITextField *panNumberTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *countryCityStateViewHtConstraint;

@end
