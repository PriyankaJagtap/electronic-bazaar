//
//  SerialTableViewCell.h
//  EB
//
//  Created by Neosoft on 2/24/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SerialTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *forwordArrowBtn;
@property (weak, nonatomic) IBOutlet UITextField *serialNumberTxtField;

@end
