//
//  VowDelightSuccessViewController.m
//  EB
//
//  Created by webwerks on 20/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "VowDelightSuccessViewController.h"
#import "MyOrdersDetails.h"

@interface VowDelightSuccessViewController ()

@end

@implementation VowDelightSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        [super setViewControllerTitle:@""];
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_trackYourOrderBtn];
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_shoppingBtn];
        _orderNumberLabel.text =[NSString stringWithFormat:@"Your new order no is %@",_orderNumber] ;
        //_awbNumberLabel.text = _awbNumber;
        
        if(_message != nil)
        {
                NSArray *arr = [_message componentsSeparatedByString:@":"];
                if(arr.count >= 2)
                {
                        _successMessageLabel.text = [arr objectAtIndex:0];
                        _replacementMessageLabel.text = [arr objectAtIndex:1];
                }
               
        }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - IBAction methods

- (IBAction)shoppingBtnClicked:(id)sender {
        AppDelegate *appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [[appdelegate tabBarObj]TabClickedIndex:0];
}
- (IBAction)trackYourOrderBtnClicked:(id)sender {
        
        MyOrdersDetails *objMyOrdersDetail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyOrdersDetails"];
        objMyOrdersDetail.orderID=[_orderNumber intValue];
        objMyOrdersDetail.isFromVowDelight = YES;
        objMyOrdersDetail.incrementID = [_entityId intValue];
        [self.navigationController pushViewController:objMyOrdersDetail animated:YES];
}
@end
