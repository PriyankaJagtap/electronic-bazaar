//
//  VowDelightSuccessViewController.h
//  EB
//
//  Created by webwerks on 20/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VowDelightSuccessViewController : SellYourGadgetNavigationViewController
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *trackYourOrderBtn;
@property (weak, nonatomic) IBOutlet UIButton *shoppingBtn;
@property (weak, nonatomic) IBOutlet UILabel *awbNumberLabel;
@property (nonatomic, strong) NSString *orderNumber;
@property (nonatomic, strong) NSString *entityId;
@property (nonatomic, strong) NSString *awbNumber;
@property (weak, nonatomic) IBOutlet UILabel *successMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *replacementMessageLabel;
@property (nonatomic, strong) NSString *message;

@end
