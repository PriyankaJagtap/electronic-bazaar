//
//  PartnerProgramViewController.m
//  EB
//
//  Created by Neosoft on 2/17/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "PartnerProgramViewController.h"
#import "MFSideMenu.h"
#import "MFSideMenuContainerViewController.h"
#import "ElectronicsViewController.h"


#define PARTNER_PROGRAM_WS @"hhttp://electronicsbazaar.com/warranty-app/partner-program-app.html"
@interface PartnerProgramViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation PartnerProgramViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //load file into webView
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:PARTNER_PROGRAM_WS]]];
    [[CommonSettings sharedInstance] displayLoading];
    [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:4];


}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - web view delegate methods
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [[CommonSettings sharedInstance] removeLoading];
    
    NSLog(@"finish");
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[CommonSettings sharedInstance] removeLoading];
    
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}

#pragma mark - IBAction methods
- (IBAction)rightSlideMenuAction:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)preOwnedMobilesBtnClicked:(id)sender
{
    ElectronicsViewController *electronicviewObjc = [self.storyboard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    electronicviewObjc.categoryVal =@"Pre Owned";
    electronicviewObjc.category_id = @"97";
    [self.navigationController pushViewController:electronicviewObjc animated:YES];

}

-(IBAction)preOwnedLaptopsBtnClicked:(id)sender
{
    ElectronicsViewController *electronicviewObjc = [self.storyboard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    electronicviewObjc.categoryVal =@"Pre Owned" ;
    electronicviewObjc.category_id = @"11";
    [self.navigationController pushViewController:electronicviewObjc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
