//
//  PartnerProgramViewController.h
//  EB
//
//  Created by Neosoft on 2/17/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PartnerProgramViewController : UIViewController<UIWebViewDelegate>

@end
