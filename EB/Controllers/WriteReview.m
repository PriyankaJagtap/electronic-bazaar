//
//  WriteReview.m
//  EB
//
//  Created by webwerks on 9/9/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "WriteReview.h"
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "Validation.h"
#import "Webservice.h"
#import "CommonSettings.h"


@interface WriteReview ()<FinishLoadingData>
{
    int ratingStarValue;
}
@end

@implementation WriteReview
@synthesize write_Review_Txt;
@synthesize product_Name_Lbl;
@synthesize prodID;

- (void)viewDidLoad {
    [super viewDidLoad];
    [super setTitle:@"Write a review"];
    // Do any additional setup after loading the view.
    [[self.write_Review_Txt layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[self.write_Review_Txt layer] setBorderWidth:1];
    [[self.write_Review_Txt layer] setCornerRadius:2];
    starRatingView.delegate=self;
    product_Name_Lbl.text=self.prodName;
    write_Review_Txt.text = @"Let us know your thoughts *";
    write_Review_Txt.textColor = [UIColor lightGrayColor];
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_submitReviewBtn];
    
    
    UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, summary_Txt.frame.size.height)];
    summary_Txt.leftViewMode = UITextFieldViewModeAlways;
    summary_Txt.leftView = wrapView;

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    //[self setUpNavigationBar];
    ratingStarValue=16;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextfield delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Star rating delegate
-(void)getSelectedStarRating:(int)starCount
{
    switch (starCount) {
        case 1:
            ratingStarValue=16;
            break;
         case 2:
            ratingStarValue=17;
            break;
        case 3:
            ratingStarValue=16;
            break;
        case 4:
            ratingStarValue=17;
            break;
        case 5:
            ratingStarValue=18;
            break;
        default:
            break;
    }
}

#pragma mark - UITextview delegate
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    write_Review_Txt.text = @"";
    write_Review_Txt.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(write_Review_Txt.text.length == 0){
        write_Review_Txt.textColor = [UIColor lightGrayColor];
        write_Review_Txt.text = @"Let us know your thoughts *";
        [write_Review_Txt resignFirstResponder];
    }
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    if(write_Review_Txt.text.length == 0){
        write_Review_Txt.textColor = [UIColor lightGrayColor];
        write_Review_Txt.text = @"Let us know your thoughts *";
        [write_Review_Txt resignFirstResponder];
    }

}

#pragma mark - Utility Methods

-(BOOL)validateTextfields{
    if (![Validation requiredTextView:write_Review_Txt withCaption:@"Your thoughts"])
        return NO;
    if (![Validation required:summary_Txt withCaption:@"Summary"])
        return NO;
    
    return YES;
}

- (IBAction)onClickOf_Submit:(id)sender {
    [self.view endEditing:YES];
    if ([self validateTextfields])
    {
        Webservice  *callSendReviewWS = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];

            callSendReviewWS.delegate =self;
            int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
            NSDictionary *userdataDict = [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];

            NSString *userName=[NSString stringWithFormat:@"%@",[userdataDict objectForKey:@"firstname"]];

            NSString *param=[NSString stringWithFormat:@"product_id=%d&user_id=%d&nick_name=%@&title=%@&detail=%@&rating_value=%d",prodID,userId,userName,summary_Txt.text,write_Review_Txt.text,ratingStarValue];
            [callSendReviewWS webServiceWitParameters:param andURL:ADD_REVIEW_WS MathodName:@"ADDREVIEWWS"];
        }
    }
}

- (IBAction)leftButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightSlideMenuAction:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}


#pragma mark - Webservice delegate
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    [COMMON_SETTINGS showAlertTitle:@"" message:[response valueForKey:@"message"]];
    summary_Txt.text=@"";
    write_Review_Txt.text=@"";
}

@end
