//
//  FilterViewController.h
//  EB
//
//  Created by webwerks on 8/25/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseNavigationControllerWithBackBtn.h"
@protocol filter_product_delegate <NSObject>

//-(void)getFilterParams:(NSMutableArray *)selectedParam;
-(void)getFilterParams:(NSMutableDictionary *)productParams andSelectedParam:(NSMutableArray *)param;

@end








@interface FilterViewController : BaseNavigationControllerWithBackBtn <UITableViewDataSource, UITableViewDelegate,UICollectionViewDelegate>
{
    
    __weak IBOutlet UIButton *clearButton;
}
@property (nonatomic, strong)NSMutableArray *arrFilter;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;



@property(strong,nonatomic)NSMutableDictionary *finalFilterDict;
@property(strong,nonatomic)NSMutableArray *selectedOptionArr;



@property(weak,nonatomic) id<filter_product_delegate> delegate;




- (IBAction)backAction:(id)sender;
- (IBAction)doneFilterAction:(id)sender;
- (IBAction)clearButtonAction:(id)sender;




@end
