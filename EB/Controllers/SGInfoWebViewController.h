//
//  SGInfoWebViewController.h
//  EB
//
//  Created by webwerks on 11/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGInfoWebViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic ,strong) NSString *webViewUrl;

@end
