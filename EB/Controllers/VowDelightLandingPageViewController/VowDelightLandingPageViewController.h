//
//  VowDelightLandingPageViewController.h
//  EB
//
//  Created by webwerks on 26/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VowDelightLandingPageViewController :BaseNavigationControllerWithBackBtn<UIWebViewDelegate>
{
        NSURLRequest* request;
}

@property (nonatomic, strong) NSString *webViewUrl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *vowDelightBtn;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
