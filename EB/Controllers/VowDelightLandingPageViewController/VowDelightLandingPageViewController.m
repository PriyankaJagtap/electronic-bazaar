//
//  VowDelightLandingPageViewController.m
//  EB
//
//  Created by webwerks on 26/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "VowDelightLandingPageViewController.h"
#define VOW_DELIGHT_LANDING_PAGE_WS @"http://electronicsbazaar.com/mobile-app/vowdelight.html"
#import "VowDelightViewController.h"

@interface VowDelightLandingPageViewController ()

@end

@implementation VowDelightLandingPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        
        [super setViewControllerTitle:@""];
      
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:VOW_DELIGHT_LANDING_PAGE_WS] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
        [_webView loadRequest:request ];
        [[CommonSettings sharedInstance] displayLoading];

        
}

-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj hideTabbar];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
        [super viewWillDisappear:animated];
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj unhideTabbar];
        
        
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - web view delegate methods
-(void)webViewDidFinishLoad:(UIWebView *)webView {
        CGFloat contentHeight = webView.scrollView.contentSize.height;
        _webViewHeightConstraint.constant = contentHeight;
        _vowDelightBtn.hidden = false;
        [[CommonSettings sharedInstance] removeLoading];
        
        NSLog(@"finish");
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
        [[CommonSettings sharedInstance] removeLoading];
        
        NSLog(@"Error for WEBVIEW: %@", [error description]);
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
      
        return YES;
}

#pragma mark - IBAction methods

- (IBAction)vowDelightBtnClicked:(id)sender {
        
        if ([CommonSettings isLoggedInUser]){
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
                VowDelightViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"VowDelightViewController"];
                [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
                AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [appDelegate navigateToLogin:VOW_DELIGHT];
        }
}

@end
