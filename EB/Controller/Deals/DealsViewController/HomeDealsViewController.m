//
//  HomeDealsViewController.m
//  EB
//
//  Created by Neosoft on 12/12/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "HomeDealsViewController.h"
#import "ProductListCell.h"
#import "AppDelegate.h"
#import "CommonSettings.h"
#import "ProductDetailsViewController.h"
#import "HomeDealsHeaderCell.h"

#define SECTION_HEADER_HEIGHT 44.0

@interface HomeDealsViewController ()
{
        NSMutableArray *sectionArr;
        NSMutableDictionary *responseDic;
        UIButton *buttonFavourite;
}
@end

@implementation HomeDealsViewController

#pragma mark - view life cycle
- (void)viewDidLoad
{
        [super viewDidLoad];
        [super setViewControllerTitle:@"Deals"];
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 115;
        [self getDeals];
        // [CommonSettings addBorderLayer:_bgView];
        
        //_bgView.layer.borderWidth = 0.5;
        // _bgView.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        [self.tabBarController.tabBar setHidden:YES];
        [[[AppDelegate getAppDelegateObj]tabBarObj] TabClickedIndex:3];
        [[[AppDelegate getAppDelegateObj]tabBarObj] unhideTabbar];
        [self.navigationController setNavigationBarHidden:YES];
        [CommonSettings sendScreenName:@"Deals"];
        
        if (responseDic != nil)
        {
                [self refreshView];
        }
}

-(void)viewDidAppear:(BOOL)animated
{
        [super viewDidAppear:animated];
        dispatch_async(dispatch_get_main_queue(), ^{
                //[[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:5];
        });
}

#pragma mark - UITableViewDataSource and Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return sectionArr.count;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        static NSString *CellIdentifier = @"HomeDealsHeaderCell";
        HomeDealsHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
                cell = [[HomeDealsHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.titleLabel.text = sectionArr[section];
        return cell.contentView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        return SECTION_HEADER_HEIGHT;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [[responseDic valueForKey:[sectionArr objectAtIndex:section]] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
        NSString *strTitle = [sectionArr objectAtIndex:section];
        return strTitle;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *CellIdentifier = @"ProductListCell";
        ProductListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
                cell = [[ProductListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.backgroundColor = [UIColor clearColor];
        
        [CommonSettings addBorderLayer:cell.bgView];
        [cell configureCartButtons];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //openbox cell
        NSDictionary *dict = [[responseDic valueForKey:[sectionArr objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        cell.productNameLbl.text = [NSString stringWithFormat:@"%@",dict[@"name"]];
        if ([[dict valueForKey:@"is_instock"] boolValue])
        {
                cell.outofProductImg.hidden = YES;
                NSInteger qty = [[dict valueForKey:@"qty"] integerValue];
                NSInteger max_qty = [[dict valueForKey:@"max_qty"] integerValue];
                
                if (qty > max_qty)
                {
                        qty = max_qty;
                }
                
                if( qty <= 10 && qty != 0)
                {
                        cell.itemLeftLabel.text = [NSString stringWithFormat:@"Hurry, Only %ld left!",qty];
                }
                else
                {
                        cell.itemLeftLabel.text = @"";
                }
        }
        else
        {
                cell.outofProductImg.hidden = NO;
                cell.itemLeftLabel.text = @"";
        }
        
        NSString *strSpecialPrice;
        NSString *priceStr = [[CommonSettings sharedInstance] formatPrice:[[dict objectForKey:@"price"] floatValue]];
        
        if ([NSNull null]==[dict objectForKey:@"special_price"])
        {
                cell.productPrevPriceLbl.text=@"";
                cell.productPriceLbl.text=priceStr;
        }
        else
        {
                strSpecialPrice=  [[CommonSettings sharedInstance] formatPrice:[[dict objectForKey:@"special_price"] floatValue]];
                cell.productPriceLbl.text=[NSString stringWithFormat:@"  %@",strSpecialPrice];
                cell.productPrevPriceLbl.text=priceStr;
        }
        //}

        NSString*imageLink = [NSString stringWithFormat:@"%@",dict[@"product_image"]];
        [cell.productImg sd_setImageWithURL:[NSURL URLWithString:imageLink]] ;
        cell.productImg.contentMode = UIViewContentModeScaleAspectFit;
        
        cell.favProdBtn.tag=indexPath.row;
        if ([[dict valueForKey:@"is_favourite"] boolValue])
        {
                [cell.favProdBtn setSelected:YES];
        }
        else
        {
                [cell.favProdBtn setSelected:NO];
        }
        
        if(![[dict valueForKey:@"new_launch"] isKindOfClass:[NSNull class]])
        {
                if([[dict valueForKey:@"new_launch"] intValue] == 1)
                {
                        cell.comingSoonView.hidden = NO;
                        cell.comingSoonViewHeightConstraint.constant = 29;
                        cell.itemLeftLabel.text = @"";
                }
                else
                {
                        cell.comingSoonViewHeightConstraint.constant = 0;
                        cell.comingSoonView.hidden = YES;
                }
        }
        else
        {
                cell.comingSoonViewHeightConstraint.constant = 0;
                cell.comingSoonView.hidden = YES;
        }
        
        [cell.favProdBtn addTarget:self action:@selector(favouriteButtonPresses:) forControlEvents:UIControlEventTouchUpInside];
        buttonFavourite=cell.favProdBtn;
        
        if([dict valueForKey:@"category_type_img"] && ![[dict valueForKey:@"category_type_img"] isEqualToString:@""])
        {
                [cell.categoryTypeImgView sd_setImageWithURL:[NSURL URLWithString:[dict valueForKey:@"category_type_img"]]];
        }
        else
        {
                cell.categoryTypeImgView.image = nil;
        }
        [cell.addToCartBtn addTarget:self action:@selector(addToCartButtonPresses:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell configureCartButtons];
        return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        [self navigateToProductDetailScreen:indexPath];
}

#pragma mark - login view delegate method
-(void)refreshView
{
        [self.tableView reloadData] ;
}

#pragma mark - get Deals Webservice
-(void)getDeals
{
        // Call Home screen webservice
        Webservice  *callDealsService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                callDealsService.delegate =self;
                [callDealsService GetWebServiceWithURL:GET_HOME_PAGE_BANNER_DEALS MathodName:@"GET_HOME_PAGE_BANNER_DEALS"];
        }
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:@"GET_HOME_PAGE_BANNER_DEALS"])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        NSMutableDictionary *data = [response objectForKey:@"data"];
                        NSString *imageUrl = [data objectForKey:@"deals_banner_img"];
                        
                        sectionArr = [NSMutableArray new];
                        responseDic = [NSMutableDictionary new];

                        if([data objectForKey:@"seal_packed_mobile"])
                        {
                                [responseDic setValue:[data objectForKey:@"seal_packed_mobile"] forKey:@"Brand New Mobiles"];
                                [sectionArr addObject:@"Brand New Mobiles"];
                        }
                        if([data objectForKey:@"open_box_mobile"])
                        {
                                [responseDic setValue:[data objectForKey:@"open_box_mobile"] forKey:@"Open Box Mobiles"];
                                [sectionArr addObject:@"Open Box Mobiles"];
                        }
                        
                        if([data objectForKey:@"pre_owned_mobile"])
                        {
                                [responseDic setValue:[data objectForKey:@"pre_owned_mobile"] forKey:@"Pre-Owned Mobiles"];
                                [sectionArr addObject:@"Pre-Owned Mobiles"];
                        }
                        if([data objectForKey:@"pre_owned_laptops"])
                        {
                                [responseDic setValue:[data objectForKey:@"pre_owned_laptops"] forKey:@"Pre-Owned Laptops"];
                                [sectionArr addObject:@"Pre-Owned Laptops"];
                        }
                        
                        if([data objectForKey:@"accessories"])
                        {
                                [responseDic setValue:[data objectForKey:@"accessories"] forKey:@"Accessories"];
                                [sectionArr addObject:@"Accessories"];
                        }
                        
                        if(sectionArr.count == 0)
                                [self addNodealsAvailableView];
                        
                        [self setImageToTopView:imageUrl];
                        [_tableView reloadData];
                }
                else
                {
                        
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        if ([strName isEqualToString:@"ADDTOCART"])
        {
                //NSLog(@"%@",response);
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                        int quoteID=[[response valueForKey:@"quoteId"]intValue];
                        [defaults setObject:[NSNumber numberWithInt:quoteID] forKey:@"QuoteID"];
                        [defaults synchronize];
                        [super incrementMyCartCount:[[response valueForKey:@"qty"] intValue]];
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Product Successfully Added To Cart"];
                }
                else
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                
        }
        else if([strName isEqualToString:@"ADDTOWISHLIST"] || [strName isEqualToString:REMOVE_FROM_WISHLIST_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        // [favouriteButton setImage:[UIImage imageNamed:@"favorate_icon_selected"] forState:UIControlStateNormal];
                        NSNumber *isfavBool;
                        if([strName isEqualToString:@"ADDTOWISHLIST"])
                                isfavBool = [NSNumber numberWithBool:YES];
                        else if([strName isEqualToString:REMOVE_FROM_WISHLIST_WS])
                                isfavBool = [NSNumber numberWithBool:NO];
                        
                        CGPoint buttonPosition = [buttonFavourite convertPoint:CGPointZero toView:self.tableView];
                        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
                        
                        NSMutableDictionary *dict = [[[responseDic valueForKey:[sectionArr objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] mutableCopy];
                        [dict setValue:isfavBool forKey:@"is_favourite"];
                        [[responseDic valueForKey:[sectionArr objectAtIndex:indexPath.section]] replaceObjectAtIndex:indexPath.row withObject:dict];
                        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
}

-(void)setImageToTopView:(NSString *)strImageURL
{
        [_topImageView sd_setImageWithURL:[NSURL URLWithString:strImageURL]] ;
}

#pragma mark Button click
- (IBAction)loginToViewPriceBtnClicked:(id)sender
{
        [APP_DELEGATE navigateToLogin:DEALS];
}

- (IBAction)btnBackClicked:(UIButton *)sender
{
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)buyNow_Action:(UIButton *)sender
{
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tableView];
        NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
        [self navigateToProductDetailScreen:indexPath];
        NSLog(@"index path %@",indexPath);
}

-(void)addToCartButtonPresses:(UIButton*)cartBtn
{
        CGPoint buttonPosition = [cartBtn convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        NSDictionary *dict = [[responseDic valueForKey:[sectionArr objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        int productId = [[dict valueForKey:@"product_id"] intValue];
        [self callAddToCartWebservice:productId];
}

-(void)callAddToCartWebservice:(int)productId
{
        if ([CommonSettings isLoggedInUser])
        {
                Webservice  *callAddToCartService = [[Webservice alloc] init];
                BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
                if(!chkInternet)
                {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                }
                else
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
                        
                        callAddToCartService.delegate =self;
                        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                        NSString *quotId;
                        if (quoteID==0)
                        {
                                quotId=@"";
                        }
                        else
                        {
                                quotId=[NSString stringWithFormat:@"%d",quoteID];
                        }
                        NSDictionary *dictAddToCart=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:productId],@"ProductID",quotId,@"QuoteID", nil];
                        [callAddToCartService operationRequestToApi:dictAddToCart url:ADD_TO_CART_WS string:@"ADDTOCART"];
                }
        }
        else
        {
                [[CommonSettings sharedInstance] showAlertTitle:@"" message:CART_LOGIN_REQUIRED_MSG];
        }
}

-(void)favouriteButtonPresses:(UIButton*)btnFav
{
        CGPoint buttonPosition = [btnFav convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
        buttonFavourite = btnFav;
        if ([CommonSettings isLoggedInUser])
        {
                NSDictionary *dict = [[responseDic valueForKey:[sectionArr objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
                
                int productId = [[dict valueForKey:@"product_id"] intValue];
                if(![[dict valueForKey:@"is_favourite"] boolValue])
                {
                        [self didselectFavouriteProduct:productId];
                }
                else
                {
                        [self removeProductFromFavourite:productId];
                }
                
        }
        else
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
        }
}

-(void)didselectFavouriteProduct:(int)productId
{
        NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
        Webservice  *addToWishlistService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                if (userId==0)
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
                }
                else
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        addToWishlistService.delegate =self;
                        [addToWishlistService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%@&productId=%d",ADDTOWISHLIST,userId,productId] MathodName:@"ADDTOWISHLIST"];
                }
        }
}

-(void)removeProductFromFavourite:(int)productId
{
        NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
        Webservice  *addToWishlistService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                if (userId==0)
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
                }
                else
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        addToWishlistService.delegate =self;
                        [addToWishlistService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%@&product_id=%d",REMOVE_FROM_WISHLIST_WS,userId,productId] MathodName:REMOVE_FROM_WISHLIST_WS];
                }
        }
}

#pragma mark - side menu
- (IBAction)leftSlideMenuAction:(id)sender
{
        [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)rightSlideMenuAction:(id)sender
{
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

#pragma mark - show product detail
-(void)navigateToProductDetailScreen:(NSIndexPath *)indexPath
{
        ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
        
        NSDictionary *dict = [[responseDic valueForKey:[sectionArr objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        int prodId = [dict[@"product_id"]intValue];
        objProductDetailVC.productID=prodId;
        
        [self.navigationController pushViewController:objProductDetailVC animated:YES];
}

#pragma mark - no deals availableView
-(void)addNodealsAvailableView
{
        UIView *noDealsAvailableView=[[UIView alloc]initWithFrame:CGRectMake(0, _topImageView.frame.size.height + _topImageView.frame.origin.y, self.view.frame.size.width, 40
                                                                             )];
        UILabel *emptyDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 200, 40)];
        emptyDataLabel.text = @"No Deals Available";
        emptyDataLabel.textAlignment=NSTextAlignmentCenter;
        emptyDataLabel.alpha = 0.5;
        emptyDataLabel.center = noDealsAvailableView.center;
        emptyDataLabel.textColor = [UIColor blackColor];
        emptyDataLabel.backgroundColor = [UIColor clearColor];
        emptyDataLabel.textAlignment = NSTextAlignmentCenter;
        [noDealsAvailableView setBackgroundColor:[UIColor clearColor]];
        [noDealsAvailableView addSubview:emptyDataLabel];
        [self.view addSubview:noDealsAvailableView];
        
}

@end
