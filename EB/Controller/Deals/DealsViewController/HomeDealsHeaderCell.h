//
//  HomeDealsHeaderCell.h
//  EB
//
//  Created by Neosoft on 5/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeDealsHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
