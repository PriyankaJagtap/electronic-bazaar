//
//  HomeDealsViewController.h
//  EB
//
//  Created by Neosoft on 12/12/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"
@interface HomeDealsViewController : BaseNavigationController<UITableViewDelegate,UITableViewDataSource,FinishLoadingData,LoginsViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
- (IBAction)btnBackClicked:(UIButton *)sender;

@end
