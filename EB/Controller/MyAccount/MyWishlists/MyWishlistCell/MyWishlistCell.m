//
//  MyWishlistCell.m
//  EB
//
//  Created by webwerks on 8/24/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "MyWishlistCell.h"

@implementation MyWishlistCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
