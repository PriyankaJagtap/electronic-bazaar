//
//  MyWishlistCell.h
//  EB
//
//  Created by webwerks on 8/24/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWishlistCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *product_Img;
@property (strong, nonatomic) IBOutlet UILabel *product_Price;
@property (strong, nonatomic) IBOutlet UILabel *product_Special_Price;
@property (strong, nonatomic) IBOutlet UILabel *product_Name;
@property (strong, nonatomic) IBOutlet UIImageView *imgOutofStock;

@property (weak, nonatomic) IBOutlet UIButton *addToCartButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteFromWishlistButton;

@property (weak, nonatomic) IBOutlet UIImageView *categoryTypeImgView;
@property (weak, nonatomic) IBOutlet UIView *comingSoonView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *comingSoonViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonDeleteTopConstraint;




@end
