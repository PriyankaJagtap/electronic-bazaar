//
//  MyWishlist.m
//  EB
//
//  Created by Aishwarya Rai on 15/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "MyWishlist.h"
#import "MyWishlistCell.h"
#import "UIImageView+WebCache.h"
#import "Webservice.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "FVCustomAlertView.h"
#import "CommonSettings.h"
#import "ProductDetailsViewController.h"
#import "ShareWishlistCell.h"

#import "GAIFields.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

@interface MyWishlist ()<FinishLoadingData>
{
        NSInteger selectedProductAtIndex;
        BOOL showView;
        BOOL isShareWishlistAvailable;
        NSString *emailFieldValue;
        NSString *messageFieldValue;
}

@end

@implementation MyWishlist
@synthesize wishListTableView;
#pragma mark - UIViewContoller Methods
- (void)viewDidLoad
{
        [super viewDidLoad];
        self.wishListTableView.contentInset = UIEdgeInsetsMake(-36, 0, 50, 0);
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
        showView = 0;
        isShareWishlistAvailable = FALSE;
        emailFieldValue = @"";
        messageFieldValue = @"Check out my wishlist";
        [super setViewControllerTitle:@"My Wishlist"];
        wishListTableView.delegate = self;
        wishListTableView.dataSource = self;
        //    wishListTableView.rowHeight = 92;
        //    wishListTableView.estimatedRowHeight = UITableViewAutomaticDimension;
}

-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        [CommonSettings sendScreenName:@"WishlistView"];
        
        [[[AppDelegate getAppDelegateObj]tabBarObj] unhideTabbar];
        [[[AppDelegate getAppDelegateObj]tabBarObj] TabClickedIndex:4];
        
        [self getWishListFromWs];
        
}

#pragma mark - UITableview Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        if (isShareWishlistAvailable)
        {
                return 2;
        }
        return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        if (section == 0)
        {
                return [productFromCartsArr count];
        }
        else if (section == 1)
        {
                return 1;
        }
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.section == 0)
        {
                static NSString *cellIdentifier=@"MyWishlistCell";
                MyWishlistCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                
                cell.product_Special_Price.text= [[CommonSettings sharedInstance] formatIntegerPrice:[[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"price"]integerValue]];
                
                cell.product_Name.text=[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"name"];
                [cell.product_Img sd_setImageWithURL:[NSURL URLWithString:[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"image"]]];
                NSString *strSpecialPrice=[NSString stringWithFormat:@"%@",[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"special_price"]];
                
                
                NSString *strPrice=[[CommonSettings sharedInstance] formatIntegerPrice:[[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"price"]integerValue]];
                
                cell.product_Special_Price.text=strPrice;
                cell.product_Price.text=@"";
                
                if ([NSNull null]==[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"special_price"])
                {
                        cell.product_Price.text=@"";
                        cell.product_Special_Price.text=strPrice;
                }
                else
                {
                        cell.product_Price.text=strPrice;
                        cell.product_Special_Price.text= [[CommonSettings sharedInstance] formatIntegerPrice:[strSpecialPrice integerValue]];
                }
                //is_inStock
                NSString *is_InStock = [NSString stringWithFormat:@"%@",[[productFromCartsArr objectAtIndex:indexPath.row]objectForKey:@"is_instock"]];
                
                if ([is_InStock isEqualToString:@"0"])
                {
                        [cell.imgOutofStock setHidden:NO];
                }
                else
                {
                        [cell.imgOutofStock setHidden:YES];
                }
                
                NSDictionary *dic = [productFromCartsArr objectAtIndex:indexPath.row];
                
                if([dic valueForKey:@"category_type_img"] && ![[dic valueForKey:@"category_type_img"] isEqualToString:@""])
                {
                        [cell.categoryTypeImgView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"category_type_img"]]];
                }
                else
                {
                        cell.categoryTypeImgView.image = nil;
                }
                
                cell.deleteFromWishlistButton.tag = indexPath.row;
                [cell.deleteFromWishlistButton addTarget:self action:@selector(deleteFromWishlist:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.addToCartButton.tag=indexPath.row;
                [cell.addToCartButton addTarget:self action:@selector(addToCart:) forControlEvents:UIControlEventTouchUpInside];
                
                
                if(![[dic valueForKey:@"new_launch"] isKindOfClass:[NSNull class]])
                {
                        if([[dic valueForKey:@"new_launch"] intValue] == 1)
                        {
                                cell.comingSoonView.hidden = NO;
                                cell.comingSoonViewHeightConstraint.constant = 29;
                                cell.addToCartButton.hidden = YES;
                        }
                        else
                        {
                                cell.comingSoonViewHeightConstraint.constant = 0;
                                cell.comingSoonView.hidden = YES;
                                cell.addToCartButton.hidden = NO;
                        }
                }
                else
                {
                        cell.comingSoonViewHeightConstraint.constant = 0;
                        cell.comingSoonView.hidden = YES;
                        cell.addToCartButton.hidden = NO;
                }
                
                return cell;
        }
        else if (indexPath.section == 1)
        {
                static NSString *cellIdentifier=@"ShareWishlistCell";
                ShareWishlistCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                
                if (showView == 0)
                {
                        cell.viewHeightConst.constant = 0;
                }
                else
                {
                        cell.viewHeightConst.constant = 130;
                }
                
                [cell.shareWishlistBtn addTarget:self action:@selector(on_ClickOf_Share_Wishlist:) forControlEvents:UIControlEventTouchUpInside];
                
                [[GradientColorClass sharedInstance] setGradientBackgroundToButton:cell.shareWishlistBtn];
                
                [cell.addAllToCartBtn addTarget:self action:@selector(on_ClickOf_Add_All_To_Cart:) forControlEvents:UIControlEventTouchUpInside];
                
                [[GradientColorClass sharedInstance]setGradientBackgroundToButton:cell.addAllToCartBtn];
                
                cell.shareWishlistBtn.tag = indexPath.row;
                return cell;
                
        }
        return Nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.section == 1) {
                if (showView == 0) {
                        return 50;
                }
                else{
                        return 185;
                }
        }
        return 120;
}

#pragma mark - UITableview Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.section == 0)
        {
                ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
                
                int productID=[[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"product_id"]intValue];
                objProductDetailVC.productID=productID;
                [self.navigationController pushViewController:objProductDetailVC animated:YES];
        }
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.section == 1)
        {
                return NO;
        }
        return YES;
}

#pragma mark - Text Field Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [textField resignFirstResponder];
        return  YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
        // this should return you your current indexPath
        ShareWishlistCell *buttonCell = (ShareWishlistCell *)[[[textField superview]superview] superview];
        
        if (textField == buttonCell.emailTF)
        {
                emailFieldValue = textField.text;
        }
        else if (textField == buttonCell.messageTF)
        {
                messageFieldValue = textField.text;
        }
        
        if(buttonCell.emailTF.text.length == 0)
        {
                [textField resignFirstResponder];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter email." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
        }
        else if (buttonCell.messageTF.text.length > 0)
        {
                messageFieldValue = buttonCell.messageTF.text;
        }
        else
        {
                if (![self validEmail:buttonCell.emailTF.text])
                {
                        [textField resignFirstResponder];
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
                        return;
                }
                else
                {
                        emailFieldValue = buttonCell.emailTF.text;
                }
        }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
        ShareWishlistCell *buttonCell = (ShareWishlistCell *)[[[textField superview]superview] superview];
        
        if (textField == buttonCell.emailTF)
        {
                emailFieldValue = textField.text;
        }
        else if (textField == buttonCell.messageTF)
        {
                messageFieldValue = textField.text;
        }
        
        if (buttonCell.messageTF.text.length > 0)
        {
                messageFieldValue = buttonCell.messageTF.text;
        }
        
        if (buttonCell.emailTF.text.length>0)
        {
                emailFieldValue = buttonCell.emailTF.text;
        }
}

#pragma mark - Table View Btn Action
-(void)deleteFromWishlist:(UIButton *)btn
{
        selectedProductAtIndex=btn.tag;
        int productID=[[[productFromCartsArr objectAtIndex:selectedProductAtIndex]valueForKey:@"product_id"]intValue];
        [self didSelectRemovedFromCart:productID];
}

-(void)addToCart:(UIButton *)btn
{
        int isProductInStock=[[[productFromCartsArr objectAtIndex:btn.tag]valueForKey:@"is_instock"]intValue];
        if (isProductInStock)
        {
                int productID=[[[productFromCartsArr objectAtIndex:btn.tag]valueForKey:@"product_id"]intValue];
                [self didSelectAddToCart:productID wishListId:[[productFromCartsArr objectAtIndex:btn.tag]valueForKey:WISHLIST_ITEM_ID]];
        }
        else
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"This product is out of stock."];
        }
}

-(void)on_ClickOf_Share_Wishlist:(id)sender
{
        UIButton *senderButton = (UIButton *)sender;
        ShareWishlistCell *buttonCell = (ShareWishlistCell *)[senderButton superview];
        if (showView == 0)
        {
                showView = 1;
        }
        else
        {
                [self shareWishlistwithEmail:emailFieldValue message:messageFieldValue];
        }
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:0 inSection:1];
        [self.wishListTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects: rowToReload, nil] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)on_ClickOf_Add_All_To_Cart:(id)sender{
        [self addAllToWishListWS];
}
/*
 -(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
 
 UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"      " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
 {
 int productID=[[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"product_id"]intValue];
 [self didSelectRemovedFromCart:productID];
 selectedProductAtIndex=indexPath.row;
 
 }];
 button.backgroundColor=[UIColor whiteColor];
 CGSize size=CGSizeMake(200, 200);
 UIGraphicsBeginImageContext(size);
 [[UIImage imageNamed:@"delet_icon-btn"] drawInRect:CGRectMake(10, 15, 40 ,40)];
 UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
 UIGraphicsEndImageContext();
 button.backgroundColor=[UIColor colorWithPatternImage:image];
 
 UITableViewRowAction *button2 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"      " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
 {
 int isProductInStock=[[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"is_instock"]intValue];
 if (isProductInStock) {
 int productID=[[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"product_id"]intValue];
 [self didSelectAddToCart:productID];
 }else{
 [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"This product is out of stock."];
 }
 }];
 UIGraphicsBeginImageContext(size);
 [[UIImage imageNamed:@"addToCart_btn"] drawInRect:CGRectMake(10, 15, 40 ,40)];
 UIImage *imageAddtocart = UIGraphicsGetImageFromCurrentImageContext();
 UIGraphicsEndImageContext();
 button2.backgroundColor=[UIColor colorWithPatternImage:imageAddtocart];
 
 return @[button, button2];
 }
 
 
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 // you need to implement this method too or nothing will work:
 
 }
 
 
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return YES; //tableview must be editable or nothing will work...
 }
 */

#pragma mark - Webservice delegate

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:@"ADDTOCART"])
        {
                //NSLog(@"%@",response);
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        [super incrementMyCartCount:[[response valueForKey:@"qty"] intValue]];
                        [self getWishListFromWs];
                }
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
        
        else if ([strName isEqualToString:@"REMOVEFROMWISHLIST"])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        [productFromCartsArr removeObjectAtIndex:selectedProductAtIndex];
                }
                [wishListTableView reloadData];
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
        else if ([strName isEqualToString:@"SHARE_WISHLIST"])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        showView = 0;
                }
                [wishListTableView reloadData];
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
        else if ([strName isEqualToString:ADD_ALL_TO_CART_WS])
        {
                if ([[response valueForKey:@"status"] intValue] == 1)
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                        //cart_total_qty
                        
                        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                        [defaults setValue:[NSNumber numberWithInteger:[[response valueForKey:@"cart_total_qty"] integerValue] ] forKey:MY_CART_COUNT_KEY];
                        [defaults synchronize];
                        [super showCartValue];
                        [self getWishListFromWs];
                }
                else
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
        else
        {
                productFromCartsArr = [response valueForKey:@"data"];
                isShareWishlistAvailable = [[response valueForKey:@"share"] boolValue];
                
                if ([productFromCartsArr count]==0)
                {
                        [self noProductAvailableView];
                }
                else
                {
                        [bgView removeFromSuperview];
                }
                [wishListTableView reloadData];
        }
}

#pragma mark - Utility Methods
-(void)getProductDetails
{
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        productFromCartsArr=[(NSMutableArray*)[defaults valueForKey:@"ProductFromCart"]mutableCopy];
}

-(void)noProductAvailableView
{
        bgView=[[UIView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height - 150)];
        
        [bgView setBackgroundColor:[UIColor whiteColor]];
        UILabel *lblNoProduct=[[UILabel alloc]initWithFrame:CGRectMake((bgView.frame.size.width-275)/2,bgView.frame.size.width/2, 275, 50)];
        lblNoProduct.textAlignment=NSTextAlignmentCenter;
        lblNoProduct.text=@"No product available in the Wishlist.";
        [lblNoProduct setFont:karlaFontRegular(16)];
        [self.view addSubview:bgView];
        // lblNoProduct.center=bgView.center;
        
        [bgView addSubview:lblNoProduct];
}



- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}


#pragma mark - Webservice Calls
-(void)getWishListFromWs
{
        Webservice  *callProductDetailService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callProductDetailService.delegate =self;
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                if (userId !=0)
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        [callProductDetailService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%d",GETWISHLIST,userId] MathodName:@"WISHLIST"];
                }
                else
                {
                        [self noProductAvailableView];
                }
        }
}

-(void)addAllToWishListWS
{
        Webservice  *addAllToCartService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                addAllToCartService.delegate =self;
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                NSString *quotId;
                if (quoteID==0)
                {
                        quotId=@"";
                }
                else
                {
                        quotId=[NSString stringWithFormat:@"%d",quoteID];
                }
                
                if (userId !=0)
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        [addAllToCartService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%d&quoteId=%d",ADD_ALL_TO_CART_WS,userId,quoteID] MathodName:ADD_ALL_TO_CART_WS];
                }
        }
}

-(void)shareWishlistwithEmail:(NSString *)email message:(NSString *)msg
{
        Webservice  *shareWislistService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                shareWislistService.delegate =self;
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                
                if (userId !=0)
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        [shareWislistService GetWebServiceWithURL:[NSString stringWithFormat:@"%@emails=%@&message=%@&cid=%d",SHARE_WISHLIST_WS,email,msg,userId] MathodName:@"SHARE_WISHLIST"];
                }
        }
}
- (void) didSelectAddToCart:(int)productId wishListId:(NSString *)wishListID
{
        if ([CommonSettings isLoggedInUser])
        {
                Webservice  *callAddToCartService = [[Webservice alloc] init];
                BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
                if(!chkInternet)
                {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                }
                else
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
                        
                        callAddToCartService.delegate =self;
                        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                        NSString *quotId;
                        if (quoteID==0)
                        {
                                quotId=@"";
                        }
                        else
                        {
                                quotId=[NSString stringWithFormat:@"%d",quoteID];
                        }
                        
                        NSDictionary *dictAddToCart=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:productId],@"ProductID",quotId,@"QuoteID", wishListID,WISHLIST_ITEM_ID,nil];
                        
                        [callAddToCartService operationRequestToApi:dictAddToCart url:ADD_TO_CART_WS string:@"ADDTOCART"];
                }
        }
        else
        {
                [[CommonSettings sharedInstance] showAlertTitle:@"" message:CART_LOGIN_REQUIRED_MSG];
        }
}


-(void)didSelectRemovedFromCart:(int)productId
{
        Webservice  *callRemovedFromCartWS = [[Webservice alloc] init];
        callRemovedFromCartWS.delegate=self;
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                
                [callRemovedFromCartWS webServiceWitParameters:[NSString stringWithFormat:@"userid=%d&product_id=%d",userId,productId] andURL:REMOVE_FROM_WISHLIST_WS MathodName:@"REMOVEFROMWISHLIST"];
        }
}

-(BOOL)validEmail:(NSString*)emailString
{
        NSString *emailid = emailString;
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        BOOL myStringMatchesRegEx=[emailTest evaluateWithObject:emailid];
        
        if(myStringMatchesRegEx)
        {
                return YES;
        }
        else
        {
                return NO;
        }
}
@end
