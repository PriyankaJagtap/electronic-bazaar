//
//  MyWishlist.h
//  EB
//
//  Created by Aishwarya Rai on 15/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "BaseNavigationControllerWithBackBtn.h"

@interface MyWishlist : BaseNavigationControllerWithBackBtn <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    NSMutableArray *productFromCartsArr;
    UIView *bgView;
}
@property (weak, nonatomic) IBOutlet UITableView *wishListTableView;



@end
