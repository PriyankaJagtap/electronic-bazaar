//
//  AccountOrderCell.h
//  EB
//
//  Created by webwerks on 9/1/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SendDataDelegate.h"

@interface AccountOrderCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *objOrderTableView;
@property(strong,nonatomic)NSArray *orderDetailArray;
-(void)setUpOrderDetailTable;
@property (nonatomic) id <SendDataDelegate> delegate;
@end
