//
//  AccountOrderCell.m
//  EB
//
//  Created by webwerks on 9/1/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "AccountOrderCell.h"
#import "PurchaseCell.h"


@implementation AccountOrderCell
@synthesize orderDetailArray,objOrderTableView;

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}
-(void)setUpOrderDetailTable
{
    [objOrderTableView reloadData];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[orderDetailArray valueForKey:@"order_items"]count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PurchaseCell";
    PurchaseCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[PurchaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //cell.selectionStyle =UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    
    //Order Id
    NSString *strOrderId = [NSString stringWithFormat:@"%@",[[orderDetailArray objectAtIndex:indexPath.row]objectForKey:@"increment_id"]];
    cell.orderIdLbl.text=strOrderId;
    
    //Order Date
    NSString *strOrderDate =[NSString stringWithFormat:@"%@",[[orderDetailArray objectAtIndex:indexPath.row]objectForKey:@"created_at"]];
    NSDateFormatter *orderDateFormat = [[NSDateFormatter alloc]init];
    [orderDateFormat setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSDate *publishdate = [orderDateFormat dateFromString:strOrderDate];
    [orderDateFormat setDateFormat:@"dd-MMM-yyyy"];
    NSString *orderdateformatStr = [orderDateFormat stringFromDate:publishdate];
    cell.orderDateLbl.text=orderdateformatStr;
    
    //Total Price
    NSString *strPrice = [NSString stringWithFormat:@"%.02f",[[[orderDetailArray objectAtIndex:indexPath.row]objectForKey:@"grand_total"]floatValue]];
    cell.totalAmtLbl.text =  [NSString stringWithFormat:@"₹ %@", strPrice];
    
    return cell;

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       NSDictionary *orderDict = [orderDetailArray objectAtIndex:indexPath.row];
        [self.delegate acceptRecord: orderDict ];
}


@end
