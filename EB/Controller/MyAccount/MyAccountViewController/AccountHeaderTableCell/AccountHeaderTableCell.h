//
//  AccountHeaderTableCell.h
//  EB
//
//  Created by Neosoft on 7/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountHeaderTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewAllBtn;
@property (weak, nonatomic) IBOutlet UILabel *blueViewLbl;

@end
