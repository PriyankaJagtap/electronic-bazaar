//
//  AddAddressTableCell.h
//  EB
//
//  Created by Neosoft on 7/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAddressTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *addAddressBtn;

@end
