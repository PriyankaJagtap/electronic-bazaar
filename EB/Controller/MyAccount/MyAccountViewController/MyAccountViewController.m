//
//  MyAccountViewController.m
//  EB
//
//  Created by webwerks on 9/1/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "MyAccountViewController.h"
#import "AccountOrderCell.h"
#import "ProductListCell.h"
#import "MyWishlistCell.h"
#import "ChangePassword.h"
#import "AddressCell.h"
#import "SettingsCell.h"
#import "AppDelegate.h"
#import "EditProfile.h"
#import "EditAddressViewController.h"
#import "MyPurchasesViewController.h"
#import "MyAddressViewController.h"
#import "AccountInfoTableCell.h"
#import "AccountHeaderTableCell.h"
#import "AddAddressTableCell.h"
#import <QuartzCore/QuartzCore.h>
#import "MyWishlist.h"
#import "EmptyTableViewCell.h"
#import "ProductDetailsViewController.h"
#import "MyOrdersDetails.h"

#define NOT_AVAILABLE @"Not Available"
@interface MyAccountViewController ()<FinishLoadingData>
{
        NSMutableArray *sectionTitleArr;
        NSMutableArray *addressListArr;
        NSInteger selectedProductAtIndex;
}
@end

@implementation MyAccountViewController

- (void)viewDidLoad
{
        [super viewDidLoad];
        
        [super setViewControllerTitle:@"My Account"];
        sectionTitleArr = [NSMutableArray arrayWithObjects:@"ACCOUNT INFORMATION", @"DOWNLOAD LIST WITH PRICE", @"MY ADDRESSES", @"RECENT ORDERS", @"MY WISHLIST", nil];
}

- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        [self.navigationController setNavigationBarHidden:YES];
        [self getMyAccountFromWs];
        [self getWishListFromWs];
        [self.tabBarController.tabBar setHidden:YES];
        [[AppDelegate getAppDelegateObj].tabBarObj unhideTabbar];
        [[[AppDelegate getAppDelegateObj]tabBarObj]TabClickedIndex:2];
        // add gesture to hide keyboard
        UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
        tapScroll.cancelsTouchesInView = NO;
        [self.view addGestureRecognizer:tapScroll];
}

#pragma mark - UITextField Delegate
-(void)hideKeyBoard:(UIGestureRecognizer *) sender
{
        [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [textField resignFirstResponder];
        return  YES;
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return sectionTitleArr.count;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        static NSString *CellIdentifier = @"AccountHeaderTableCell";
        AccountHeaderTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
                cell = [[AccountHeaderTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell.titleLabel.text = [sectionTitleArr objectAtIndex:section];
        if(section != 0)
        {
                cell.titleLabel.textAlignment = NSTextAlignmentLeft;
                cell.blueViewLbl.hidden = true;
                if (section == 1)
                {
                        [cell.viewAllBtn setTitle:@"" forState:UIControlStateNormal];
                }
                else
                {
                        [cell.viewAllBtn setTitle:@"View All" forState:UIControlStateNormal];
                }
        }
        else
        {
                UIImage *btnImage = [UIImage imageNamed:@"edit_blue_icon"];
                cell.titleLabel.textColor = [[AppDelegate getAppDelegateObj] colorWithHexString:@"192C64"];
                cell.blueViewLbl.hidden = false;
                cell.titleLabel.textAlignment = NSTextAlignmentCenter;
                [cell.viewAllBtn setImage:btnImage forState:UIControlStateNormal];
                [cell.viewAllBtn setTitle:@"" forState:UIControlStateNormal];
        }
        
        cell.viewAllBtn.tag=section;
        [cell.viewAllBtn addTarget:self action:@selector(seeMoreButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        return cell.contentView;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(-5, 0, self.view.frame.size.width, 10)];
        UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
        CGRect sepFrame = CGRectMake(-5, 0,self.view.frame.size.width, 10);
        UIView *seperatorView =[[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = [UIColor colorWithWhite:226.0/255.0 alpha:1.0];
        [header addSubview:seperatorView];
        return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.section==0)
        {
                return 300;
        }
        else if (indexPath.section == 1)
        {
                return 45;
        }
        else if (indexPath.section == 4)
        {
                return 120;
        }
        else if(indexPath.section==3)
        {
                return [[dashboardResponse valueForKey:@"recent_orders"]count]*36+36;
        }
        else if (indexPath.section ==2 && indexPath.row == 0)
        {
                return 55;
        }
        else if (indexPath.section ==2 && indexPath.row == 1)
        {
                return 47;
        }
        return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        return 40.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
        return 10;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        if (section == 4)
        {
                if (productFromCartsArr.count>3)
                {
                        return 3;
                }
                else if (productFromCartsArr.count == 0)
                {
                        return 1;
                }
                else
                {
                        return productFromCartsArr.count;
                }
                //return 3;
        }
        else if(section == 2)
        {
                if(addressListArr.count == 0)
                        return 2;
                else
                        return addressListArr.count +1;
        }
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if(indexPath.section == 0)
        {
                static NSString *CellIdentifier = @"AccountInfoTableCell";
                AccountInfoTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil)
                {
                        cell = [[AccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
                
                [cell.chngPwdBtn addTarget:self action:@selector(navigateToChangePassword) forControlEvents:UIControlEventTouchUpInside];
                [[GradientColorClass sharedInstance] setGradientBackgroundToButton:cell.chngPwdBtn];
                NSMutableDictionary *dict = [[NSUserDefaults standardUserDefaults]objectForKey:@"user"] ;
                
                if (dict.count > 0)
                {
                        if([dict objectForKey:@"firstname"])
                                cell.storeNameTxtLbl.text = [dict objectForKey:@"firstname"];
                        else
                                cell.storeNameTxtLbl.text = NOT_AVAILABLE;
                        
                        //Mobile
                        NSString *mobileStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"mobile"]];
                        cell.mobileNoTxtLbl.text = mobileStr?mobileStr:NOT_AVAILABLE;
                        if ([mobileStr isEqualToString:@"<null>"])
                        {
                                cell.mobileNoTxtLbl.text=NOT_AVAILABLE;
                        }
                        
                        //email
                        NSString *emailStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"email"]];
                        cell.emailIdTxtLbl.text = emailStr?emailStr:NOT_AVAILABLE;
                        
                        if([dict objectForKey:@"business_type"])
                                cell.businessTypeTxtLabel.text = [dict objectForKey:@"business_type"];
                        else
                                cell.businessTypeTxtLabel.text = NOT_AVAILABLE;
                        
                        if([dict objectForKey:@"referred_by"])
                                cell.referredByTxtLabel.text = [dict objectForKey:@"referred_by"];
                        else
                                cell.referredByTxtLabel.text = NOT_AVAILABLE;
                        
                        if([dict objectForKey:@"pincode"])
                                cell.pincodetxtLabel.text = [dict objectForKey:@"pincode"];
                        else
                        {
                                cell.pincodetxtLabel.text = NOT_AVAILABLE;
                        }
                        
                        if([dict objectForKey:@"gstin"])
                                cell.gstinTxtLabel.text = [dict objectForKey:@"gstin"];
                        else
                                cell.gstinTxtLabel.text = NOT_AVAILABLE;
                        
                        if([dict objectForKey:@"taxvat"])
                                cell.panNumberLabel.text = [dict objectForKey:@"taxvat"];
                        else
                                cell.panNumberLabel.text = NOT_AVAILABLE;
                }
                else
                {
                        cell.storeNameTxtLbl.text = @"Guest";
                        cell.emailIdTxtLbl.text = NOT_AVAILABLE;
                        cell.mobileNoTxtLbl.text = NOT_AVAILABLE;
                }
                return cell;
        }
        else if (indexPath.section == 1)
        {
                static NSString *CellIdentifier = @"AddAddressTableCell";
                AddAddressTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil)
                {
                        cell = [[AddAddressTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
                [cell.addAddressBtn setTitle:@"Click here to download" forState:UIControlStateNormal];
                [cell.addAddressBtn addTarget:self action:@selector(downloadFile) forControlEvents:UIControlEventTouchUpInside];
                [[GradientColorClass sharedInstance] setGradientBackgroundToButton:cell.addAddressBtn];
                return cell;
        }
        else if (indexPath.section == 4)
        {
                if ([productFromCartsArr count]==0)
                {
                        static NSString *cellId = @"EmptyTableViewCell";
                        EmptyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
                        cell.noProductLbl.text = @"No Product available in Wishlist";
                        return cell;
                }
                else
                {
                        static NSString *cellIdentifier=@"MyWishlistCell";
                        MyWishlistCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                        
                        cell.product_Name.text=[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"name"];
                        [cell.product_Img sd_setImageWithURL:[NSURL URLWithString:[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"image"]]];
                        NSString *strSpecialPrice=[NSString stringWithFormat:@"%@",[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"special_price"]];
                        NSString *strPrice=[NSString stringWithFormat:@"%@",[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"price"]];
                        
                        if ([NSNull null]==[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"special_price"])
                        {
                                cell.product_Price.text=@"";
                                cell.product_Special_Price.text=[[CommonSettings sharedInstance] formatIntegerPrice:[strPrice integerValue]];
                        }
                        else
                        {
                                cell.product_Price.text=strPrice;
                                cell.product_Special_Price.text=[[CommonSettings sharedInstance] formatIntegerPrice:[strSpecialPrice integerValue]];
                        }
                        //is_inStock
                        NSString *is_InStock = [NSString stringWithFormat:@"%@",[[productFromCartsArr objectAtIndex:indexPath.row]objectForKey:@"is_instock"]];
                        if ([is_InStock isEqualToString:@"0"])
                        {
                                [cell.imgOutofStock setHidden:NO];
                        }
                        else
                        {
                                [cell.imgOutofStock setHidden:YES];
                        }
                        
                        cell.deleteFromWishlistButton.tag=indexPath.row;
                        [cell.deleteFromWishlistButton addTarget:self action:@selector(deleteFromWishlist:) forControlEvents:UIControlEventTouchUpInside];
                        cell.addToCartButton.tag=indexPath.row;
                        [cell.addToCartButton addTarget:self action:@selector(addToCart:) forControlEvents:UIControlEventTouchUpInside];
                        
                        if(![[[productFromCartsArr objectAtIndex:indexPath.row] valueForKey:@"new_launch"] isKindOfClass:[NSNull class]])
                        {
                                if( [[[productFromCartsArr objectAtIndex:indexPath.row] valueForKey:@"new_launch"] intValue] == 1)
                                {
                                        cell.comingSoonView.hidden = NO;
                                        cell.comingSoonViewHeightConstraint.constant = 29;
                                        cell.addToCartButton.hidden = YES;
                                }
                                else
                                {
                                        cell.comingSoonViewHeightConstraint.constant = 0;
                                        cell.comingSoonView.hidden = YES;
                                        cell.addToCartButton.hidden = NO;
                                }
                        }
                        else
                        {
                                cell.comingSoonViewHeightConstraint.constant = 0;
                                cell.comingSoonView.hidden = YES;
                                cell.addToCartButton.hidden = NO;
                        }
                        return cell;
                }
        }
        else if(indexPath.section == 3)
        {
                static NSString *CellIdentifier = @"AccountOrderCell";
                AccountOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil)
                {
                        cell = [[AccountOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
                cell.orderDetailArray=[dashboardResponse valueForKey:@"recent_orders"];
                [cell setUpOrderDetailTable];
                cell.backgroundColor = [UIColor clearColor];
                cell.delegate = self;
                return cell;
        }
        else if (indexPath.section == 2)
        {
                NSInteger totalRow = [tableView numberOfRowsInSection:indexPath.section];//first get total rows in that section by current indexPath.
                if (indexPath.row == totalRow -1)
                {
                        static NSString *CellIdentifier = @"AddAddressTableCell";
                        AddAddressTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                        if (cell == nil)
                        {
                                cell = [[AddAddressTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                        }
                        [cell.addAddressBtn setTitle:@"Add New Address" forState:UIControlStateNormal];
                        [cell.addAddressBtn addTarget:self action:@selector(addNewAddress) forControlEvents:UIControlEventTouchUpInside];
                        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:cell.addAddressBtn];
                        return cell;
                }
                else
                {
                        static NSString *CellIdentifier = @"AddressCell";
                        AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                        if (cell == nil)
                        {
                                cell = [[AddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                        }
                        
                        if(addressListArr.count == 0)
                        {
                                NSString *strBillingAdd=[NSString stringWithFormat:@"%@ %@ %@-%@",[[dashboardResponse valueForKey:@"default_billing_address"] valueForKey:@"street"]?:@"",[[dashboardResponse valueForKey:@"default_billing_address"] valueForKey:@"city"]?:@"",[[dashboardResponse valueForKey:@"default_billing_address"] valueForKey:@"region"]?:@"",[[dashboardResponse valueForKey:@"default_billing_address"] valueForKey:@"postcode"]?:@""];
                                cell.billing_Add_Lbl.text=strBillingAdd;
                        }
                        else
                        {
                                if(![ [[addressListArr objectAtIndex:indexPath.row] valueForKey:@"complete_address"] isKindOfClass:[NSNull class]])
                                {
                                        cell.billing_Add_Lbl.text = [[addressListArr objectAtIndex:indexPath.row] valueForKey:@"complete_address"];
                                }
                        }
                        
                        return cell;
                }
        }
        return Nil;
}

#pragma mark - SendDataDelegate Method
- (void)acceptRecord:(NSDictionary*) orderDict
{
        MyOrdersDetails *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyOrdersDetails"];
        vc.orderID=[[ orderDict objectForKey:@"order_id"]intValue];
        vc.incrementID=[[ orderDict objectForKey:@"increment_id"]intValue];
        [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Button click methods
-(void)deleteFromWishlist:(UIButton *)btn
{
        selectedProductAtIndex=btn.tag;
        int productID=[[[productFromCartsArr objectAtIndex:selectedProductAtIndex]valueForKey:@"product_id"]intValue];
        [self didSelectRemovedFromCart:productID];
}

-(void)addToCart:(UIButton *)btn
{
        int isProductInStock=[[[productFromCartsArr objectAtIndex:btn.tag]valueForKey:@"is_instock"]intValue];
        if (isProductInStock)
        {
                int productID=[[[productFromCartsArr objectAtIndex:btn.tag]valueForKey:@"product_id"]intValue];
                [self didSelectAddToCart:productID wishListId:[[productFromCartsArr objectAtIndex:btn.tag]valueForKey:WISHLIST_ITEM_ID]];
        }
        else
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"This product is out of stock."];
        }
}

-(void)addNewAddress
{
        [self.navigationController pushViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditAddressViewController"] animated:YES];
}

-(void)downloadFile
{
        NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,DOWNLOAD_LIST_WITH_PRICE_WS];//testURL
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

-(void)navigateToChangePassword
{
        ChangePassword *objChangePassword = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChangePassword"];
        [self.navigationController pushViewController:objChangePassword animated:YES];
}


#pragma mark - Table view methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        if ((indexPath.section == 4) && ([productFromCartsArr count] > 0))
        {
                ProductDetailsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
                
                int productID=[[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"product_id"]intValue];
                vc.productID=productID;
                [self.navigationController pushViewController:vc animated:YES];
        }
}
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.section == 0)
        {
                return NO;
        }
        else if (indexPath.section == 2)
        {
                return NO;
        }
        return YES;
}

#pragma mark - get address list
-(void)callAddressListWS
{
        Webservice  *callAddlistWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callAddlistWs.delegate =self;
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callAddlistWs webServiceWitParameters:[NSString stringWithFormat:@"userid=%d",userId] andURL:GET_CUSTOMER_ADDRESS_LIST_WS MathodName:@"ADDRESS_LIST"];
        }
}

#pragma mark - IBAction methods
- (IBAction)editProfileAction:(id)sender
{
        EditProfile *objEditProfile = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditProfile"];
        [self.navigationController pushViewController:objEditProfile animated:YES];
}

- (IBAction)backAction:(id)sender
{
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightMenuAction:(id)sender
{
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

-(void)getProductDetails
{
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        productFromCartsArr=[(NSMutableArray*)[defaults valueForKey:@"ProductFromCart"]mutableCopy];
}

- (IBAction)changePhotoAction:(id)sender
{
        
}

#pragma mark : Button Actions
- (void) didSelectAddToCart:(int)productId wishListId:(NSString *)wishListID
{
        if ([CommonSettings isLoggedInUser])
        {
                Webservice  *callAddToCartService = [[Webservice alloc] init];
                BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
                if(!chkInternet)
                {
                        //[FVCustomAlertView hideAlertFromView:self.view fading:NO];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                }
                else
                {
                        //[FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@"" withBlur:YES allowTap:YES];
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
                        
                        callAddToCartService.delegate =self;
                        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                        NSString *quotId;
                        if (quoteID==0)
                        {
                                quotId=@"";
                        }
                        else
                        {
                                quotId=[NSString stringWithFormat:@"%d",quoteID];
                        }
                        
                        NSDictionary *dictAddToCart=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:productId],@"ProductID",quotId,@"QuoteID", wishListID,WISHLIST_ITEM_ID,nil];
                        
                        [callAddToCartService operationRequestToApi:dictAddToCart url:ADD_TO_CART_WS string:@"ADDTOCART"];
                }
        }
        else
        {
                [[CommonSettings sharedInstance] showAlertTitle:@"" message:CART_LOGIN_REQUIRED_MSG];
        }
}

-(void)didSelectRemovedFromCart:(int)productId
{
        Webservice  *callRemovedFromCartWS = [[Webservice alloc] init];
        callRemovedFromCartWS.delegate=self;
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                
                [callRemovedFromCartWS webServiceWitParameters:[NSString stringWithFormat:@"userid=%d&product_id=%d",userId,productId] andURL:REMOVE_FROM_WISHLIST_WS MathodName:@"REMOVEFROMWISHLIST"];
        }
}

#pragma mark : View All Btn redirection
-(void)seeMoreButtonPressed:(UIButton *)btn
{
        switch (btn.tag)
        {
                case 0:
                {
                        EditProfile *objEditProfile = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditProfile"];
                        //objEditProfile.userdict = ;
                        [self.navigationController pushViewController:objEditProfile animated:YES];
                }
                        break;
                case 4:
                {
                        MyWishlist *objMyOrder = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyWishlist"];
                        [self.navigationController pushViewController:objMyOrder animated:YES];
                        break;
                }
                case 3:
                {
                        MyPurchasesViewController *objMyPurchaseVC ;
                        if(checkIsIpad)
                                objMyPurchaseVC = [[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"MyPurchasesViewController"];
                        else
                                objMyPurchaseVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyPurchasesViewController"];
                        
                        [self.navigationController pushViewController:objMyPurchaseVC animated:YES];
                }
                        break;
                case 2:
                {
                        MyAddressViewController *objMyAddressVC;
                        if(checkIsIpad)
                                objMyAddressVC = [[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"MyAddressViewController"];
                        else
                                objMyAddressVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyAddressViewController"];
                        
                        [self.navigationController pushViewController:objMyAddressVC animated:YES];
                }
                        break;
                
                default:
                        break;
        }
}

#pragma mark - Webservice Methods
-(void)getMyAccountFromWs
{
        Webservice  *callMyAccountWS = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callMyAccountWS.delegate =self;
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                if (userId !=0)
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        [callMyAccountWS webServiceWitParameters:[NSString stringWithFormat:@"userid=%d",userId] andURL:DASHBOARD_WS MathodName:@"DASHBOARD"];
                }
        }
}

-(void)getWishListFromWs
{
        Webservice  *callProductDetailService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callProductDetailService.delegate =self;
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                if (userId !=0)
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        [callProductDetailService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%d",GETWISHLIST,userId] MathodName:@"WISHLIST"];
                }
        }
}

#pragma mark - Service Response handling
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:@"ADDRESS_LIST"])
        {
                //NSLog(@"RESPONSE=%@",response);
                NSArray *addressArr=[response valueForKey:@"result"];
                
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"created_at" ascending: NO];
                NSArray *sortedArray = [[response valueForKey:@"result"] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                
                addressListArr = [NSMutableArray new];
                if (addressArr.count < 3)
                {
                        [addressListArr addObjectsFromArray:[sortedArray subarrayWithRange:NSMakeRange(0, addressArr.count)]];
                }
                else
                {
                        [addressListArr addObjectsFromArray:[sortedArray subarrayWithRange:NSMakeRange(0, 3)]];
                }
                [self.tblView reloadData];
        }
        else if ([strName isEqualToString:@"ADDTOCART"])
        {
                //NSLog(@"%@",response);
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                        int quoteID=[[response valueForKey:@"quoteId"]intValue];
                        [defaults setObject:[NSNumber numberWithInt:quoteID] forKey:@"QuoteID"];
                        [defaults synchronize];
                        [super incrementMyCartCount:[[response valueForKey:@"qty"] intValue]];
                        //[[CommonSettings sharedInstance]showAlertTitle:@"Success" message:@"Product Successfully Added To Cart"];
                        MyCartViewController  *electronicVC= [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyCartViewController"];
                        
                        [self.navigationController pushViewController:electronicVC animated:YES];
                        //[self.tblView reloadData];
                }
        }
        else if ([strName isEqualToString:@"REMOVEFROMWISHLIST"])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        [productFromCartsArr removeObjectAtIndex:selectedProductAtIndex];
                }
                [self.tblView reloadData];
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
        else if ([strName isEqualToString:@"WISHLIST"])
        {
                productFromCartsArr= [response valueForKey:@"data"];
                [self.tblView reloadData];
        }
        else
        {
                //NSLog(@"RESPONSE :%@",response);
                dashboardResponse=[response valueForKey:@"data"];
                NSString *user_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]objectForKey:@"user"]objectForKey:@"user_id"]];
                if (user_id.length > 0)
                {
                        [self callAddressListWS];
                }
                else
                {
                        [self.tblView reloadData];
                }
        }
}

@end
