//
//  AccountInfoTableCell.h
//  EB
//
//  Created by Neosoft on 7/10/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountInfoTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *storeNameTxtLbl;
@property (weak, nonatomic) IBOutlet UILabel *mobileNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileNoTxtLbl;
@property (weak, nonatomic) IBOutlet UILabel *emailIdLbl;
@property (weak, nonatomic) IBOutlet UILabel *emailIdTxtLbl;
@property (weak, nonatomic) IBOutlet UILabel *pincodeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pincodeTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *pincodetxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *referredByTxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *businessTypeLabel;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chngPwdtopConstraint;
@property (weak, nonatomic) IBOutlet UIButton *chngPwdBtn;
@property (weak, nonatomic) IBOutlet UILabel *referredByLabel;
@property (weak, nonatomic) IBOutlet UILabel *businessTypeTxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *gstinLabel;
@property (weak, nonatomic) IBOutlet UILabel *gstinTxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *whatsApp2Label;
@property (weak, nonatomic) IBOutlet UILabel *manager2Label;
@property (weak, nonatomic) IBOutlet UILabel *whatsApp1Label;
@property (weak, nonatomic) IBOutlet UILabel *manager1Label;
@property (weak, nonatomic) IBOutlet UILabel *manager1TxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *whatsapp1TxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *manager2TxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *whatsApp2TxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *panNumberLabel;

@end
