//
//  EmptyTableViewCell.h
//  EB
//
//  Created by Aishwarya Rai on 19/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *noProductLbl;

@end
