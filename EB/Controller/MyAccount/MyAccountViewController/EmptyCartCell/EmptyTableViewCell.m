//
//  EmptyTableViewCell.m
//  EB
//
//  Created by Aishwarya Rai on 19/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "EmptyTableViewCell.h"

@implementation EmptyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
