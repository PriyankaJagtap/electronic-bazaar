//
//  MyAccountViewController.h
//  EB
//
//  Created by webwerks on 9/1/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SendDataDelegate.h"

@interface MyAccountViewController : BaseNavigationController <SendDataDelegate, UIImagePickerControllerDelegate,UITableViewDelegate>
{
    NSArray *dashboardResponse;
    NSMutableArray *productFromCartsArr;
    UIView *bgView;
}

@property (weak, nonatomic) IBOutlet UITableView *tblView;
- (IBAction)backAction:(id)sender;
- (IBAction)rightMenuAction:(id)sender;

@end

