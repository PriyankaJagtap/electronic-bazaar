//
//  AddressCell.h
//  EB
//
//  Created by webwerks on 9/1/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *billing_Add_Lbl;
@property (weak, nonatomic) IBOutlet UIButton *add_address_btn;
@property (weak, nonatomic) IBOutlet UILabel *shipping_Add_Lbl;

@end
