//
//  MyAddressViewController.h
//  EB
//
//  Created by webwerks on 9/1/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"

@interface MyAddressViewController : BaseNavigationControllerWithBackBtn<FinishLoadingData>
{
    NSMutableArray *addressListArr;
}
@property (weak, nonatomic) IBOutlet UITableView *tblView;
- (IBAction)addAddressAction:(id)sender;
- (IBAction)backAction:(id)sender;
- (IBAction)rightMenuAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *addAddressBtn;

@end
