//
//  BillingAddressCell.h
//  EB
//
//  Created by webwerks on 9/1/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillingAddressCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *addressDetailTxt;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIButton *deleteAddBtn;



@end
