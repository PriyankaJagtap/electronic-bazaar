//
//  MyAddressViewController.m
//  EB
//
//  Created by webwerks on 9/1/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "MyAddressViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "EditAddressViewController.h"
#import "BillingAddressCell.h"
#import "AppDelegate.h"
#import "Constant.h"

@interface MyAddressViewController (){
        NSMutableArray *defaultBillingAddArr;
        NSMutableArray *defaultShippingAddArr;
        NSInteger selectedAddressIndex;
}

@end

@implementation MyAddressViewController
@synthesize tblView;
- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        [self.navigationController setNavigationBarHidden:YES];
        self.tblView.contentInset = UIEdgeInsetsMake(-36, 0,0, 0);
        //[[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:5];
        [super setViewControllerTitle:@"My Addresses"];
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.addAddressBtn];
}

- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:animated];
        [self.navigationController setNavigationBarHidden:YES];
        
        NSString *user_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]objectForKey:@"user"]objectForKey:@"user_id"]];
        if (user_id.length > 0) {
                [self callAddressListWS];
        }
        [[[AppDelegate getAppDelegateObj]tabBarObj]hideTabbar];
}
-(void)viewDidAppear:(BOOL)animated{
        [super viewDidAppear:YES];
        [self.navigationController setNavigationBarHidden:YES];
}


#pragma mark -
#pragma mark - UITableViewDataSource

//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 2;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 65.0;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        
        //    if (section == 0) {
        //        return 2;
        //    }
        //
        return [addressListArr count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if(indexPath.section == 0 )
        {
                static NSString *CellIdentifier = @"BillingAddressCell";
                BillingAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                        cell = [[BillingAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
                cell.backgroundColor = [UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1];
                cell.bgView.backgroundColor = [UIColor whiteColor];
                //cell.bgView.layer.cornerRadius = 10.0;
                //cell.bgView.clipsToBounds = YES;
                
                // Format title
                if(![ [[addressListArr objectAtIndex:indexPath.row] valueForKey:@"complete_address"] isKindOfClass:[NSNull class]])
                {
                         cell.addressDetailTxt.text=[[addressListArr objectAtIndex:indexPath.row]valueForKey:@"complete_address"];
                }
               
                cell.deleteAddBtn.tag = indexPath.row;
                [cell.deleteAddBtn addTarget:self action:@selector(deleteAddButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
        }
        /* else{
         static NSString *simpleTableIdentifier = @"SimpleTableItem";
         
         UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
         
         if (cell == nil) {
         cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
         }
         cell.textLabel.font =[UIFont fontWithName:@"Karla-Regular" size:14.0];
         cell.textLabel.textColor =[UIColor darkGrayColor];
         cell.backgroundColor = [ UIColor whiteColor];
         cell.textLabel.numberOfLines = 0;
         cell.textLabel.text =@"404, 17B, Customs Colony, Kaveri, Powai Vihar, Mumbai - 400076";
         return cell;
         }*/
        
        return Nil;
}



//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//        
//}

#pragma mark - Add Address

- (IBAction)addAddressAction:(id)sender
{
        
        
        EditAddressViewController *editAddressObjc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditAddressViewController"];
        [self.navigationController pushViewController:editAddressObjc animated:YES];
}



#pragma mark - Back Action
- (IBAction)backAction:(id)sender
{
        [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Right Menu
- (IBAction)rightMenuAction:(id)sender
{
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

#pragma mark - Webservice

-(void)callDashboardWebservice:(NSString*)user_id{
        Webservice  *callMyOrdersWS = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callMyOrdersWS.delegate =self;
                
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict setValue:user_id?user_id:@"" forKey:@"user_id"];
                [callMyOrdersWS operationRequestToApi:dict url:DASHBOARD_WS string:@"Dashboard"];
                
        }
}

-(void)callAddressListWS{
        Webservice  *callAddlistWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callAddlistWs.delegate =self;
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callAddlistWs webServiceWitParameters:[NSString stringWithFormat:@"userid=%d",userId] andURL:GET_CUSTOMER_ADDRESS_LIST_WS MathodName:@"ADDRESS_LIST"];
                //[callAddlistWs operationRequestToApi:dict url:DASHBOARD_WS string:@"Dashboard"];
                
        }
}


-(void)receivedResponseForDashboard:(id)receiveData stringResponse:(NSString *)responseType{
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        
        if ([responseType isEqualToString:@"success"])
        {
                NSData *receiveOrderData = [NSData dataWithData:receiveData];
                id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveOrderData options:kNilOptions error:nil];
                NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
                if ([[dict valueForKey:@"status"]intValue] == 1 )
                {
                        defaultBillingAddArr = [dict objectForKey:@"default_billing_address"];
                        defaultShippingAddArr = [dict objectForKey:@"default_shipping_address"];
                }
                else{
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:[dict objectForKey:@"message"]];
                }
        }
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:@"ADDRESS_LIST"])
        {
                //NSLog(@"RESPONSE=%@",response);
                NSArray *addressArr = [response valueForKey:@"result"];
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"created_at" ascending: NO];
                NSArray *sortedArray = [addressArr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                
                [addressListArr removeAllObjects];
                addressListArr = [NSMutableArray arrayWithArray:sortedArray];
                [self.tblView reloadData];
        }
        else if ([strName isEqualToString:@"REMOVE_ADDRESS"]){
                //[self callAddressListWS];
                if ([[response valueForKey:@"status"]intValue]==1) {
                        [addressListArr removeObjectAtIndex:selectedAddressIndex];
                        //addressListArr=[[response valueForKey:@"result"]mutableCopy];
                        [self.tblView reloadData];
                        //            [self initializePayment];
                }else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Unable to Delete. Try Again"];
                        
                }
                
        }
}

-(void)deleteAddButtonClicked:(UIButton *)btn{
        selectedAddressIndex = btn.tag;
        int addressIdString = [[[addressListArr objectAtIndex:selectedAddressIndex]valueForKey:@"customer_address_id"] intValue];
        [self deleteAddressAction:addressIdString];
}

-(void)deleteAddressAction:(int)addID{
        Webservice  *callRemoveAddress = [[Webservice alloc] init];
        callRemoveAddress.delegate=self;
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                NSString *url = [NSString stringWithFormat:@"%@address_id=%d&user_id=%d",REMOVE_ADDRESS,addID,userId];
                [callRemoveAddress GetWebServiceWithURL:url MathodName:@"REMOVE_ADDRESS"];
                //[callRemoveAddress webServiceWitParameters:[NSString stringWithFormat:@"address_id=%d&userid=%d",addID,userId] andURL:REMOVE_ADDRESS MathodName:@"REMOVE_ADDRESS"];
        }
}//https://www.electronicsbazaar.com/customapiv6/customer/deleteAddress?address_id=27968&user_id=29080
@end
