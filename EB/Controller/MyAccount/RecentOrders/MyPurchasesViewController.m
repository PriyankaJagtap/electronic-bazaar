//
//  MyPurchasesViewController.m
//  EB
//
//  Created by webwerks on 9/2/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "MyPurchasesViewController.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "PurchaseCell.h"
#import "MyOrdersDetails.h"
#import "ShowTabbars.h"
#import "SaleHistoryDetailViewController.h"
#import "SGHistoryViewController.h"

@interface MyPurchasesViewController ()
{
    NSMutableArray *orderArr;
    UILabel *emptyDataLabel;
    NSInteger page_no;
    NSInteger totalCount;
}
@end

@implementation MyPurchasesViewController
@synthesize isSelectable;
- (void)viewDidLoad {
    [super viewDidLoad];
//    self.headerView.layer.cornerRadius = 10.0;
//    self.headerView.clipsToBounds = YES;
    
    orderArr = [NSMutableArray new];
    
    //Empty Label
    emptyDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
    emptyDataLabel.text = @"No order available";
    emptyDataLabel.alpha = 0.5;
    emptyDataLabel.center = self.view.center;
    emptyDataLabel.textColor = [UIColor blackColor];
    emptyDataLabel.backgroundColor = [UIColor clearColor];
    emptyDataLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:emptyDataLabel];
    emptyDataLabel.hidden = YES;
    //[self.navigationController setNavigationBarHidden:YES];
    page_no=1;
    
    if(_isSellGadgetHistory)
    {
        [_rightMenuBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        _tableViewBottomConstraint.constant = 0;
            [[APP_DELEGATE tabBarObj] hideTabbar];
        [self getSellGadgetData];
//        _orderLabel.text = @"Order #";
            _orderLabel.text = @"Ticket Id";
        _orderStatus.text = @"Status";
        _orderPriceLabel.text = @"Price";
        _titleLabel.text = SALES_HISTORY;
    }
    else
    {
        //_titleLabel.text = PURCAHSE_HISTORY;
        [super setViewControllerTitle:@"Purchase History"];
        [self callWebserviceMyOrder];
    }
    
    [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:4];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
//    AppDelegate *objappDel=[[UIApplication sharedApplication]delegate];
//    [objappDel.tabBarObj TabClickedIndex:1];
//    ShowTabbars *objShowTabbars=[[ShowTabbars alloc]init];
//    [objShowTabbars showTabbars];
//    [objShowTabbars TabClickedIndex:1];
    
    // Check for data exist
    if (orderArr.count > 0)
    {
        emptyDataLabel.hidden = YES;
        _tableView.hidden = NO;
        _headerView.hidden = NO;
    }
    else
    {
        emptyDataLabel.hidden = NO;
        _tableView.hidden = YES;
        _headerView.hidden = YES;
    }
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark -
#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return orderArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PurchaseCell";
    PurchaseCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[PurchaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //cell.selectionStyle =UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    
    if(_isSellGadgetHistory)
    {
            cell.orderIdLbl.text = [[orderArr objectAtIndex:indexPath.row]objectForKey:@"id"];
            NSDictionary * linkAttributes = @{NSForegroundColorAttributeName:cell.orderIdLbl.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)};
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:cell.orderIdLbl.text attributes:linkAttributes];
            [cell.orderIdLbl setAttributedText:attributedString];
            
         cell.orderDateLbl.text =[[CommonSettings sharedInstance] formatIntegerPrice:[[[orderArr objectAtIndex:indexPath.row]objectForKey:@"price"] integerValue]] ;
            
      cell.totalAmtLbl.text = [[orderArr objectAtIndex:indexPath.row]objectForKey:@"status"];
    }
    else
    {
    //Order Id
    NSString *strOrderId = [NSString stringWithFormat:@"%@",[[orderArr objectAtIndex:indexPath.row]objectForKey:@"increment_id"]];
        
    //NSString *strOrderId = [NSString stringWithFormat:@"%@",  [[orderArr objectAtIndex:indexPath.row]objectForKey:@"order_id"]];
    cell.orderIdLbl.text=strOrderId;
    
    //Order Date
    NSString *strOrderDate =[NSString stringWithFormat:@"%@",[[orderArr objectAtIndex:indexPath.row]objectForKey:@"created_at"]];
//    NSDateFormatter *orderDateFormat = [[NSDateFormatter alloc]init];
//    [orderDateFormat setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
//    NSDate *publishdate = [orderDateFormat dateFromString:strOrderDate];
//    [orderDateFormat setDateFormat:@"dd-MMM-yyyy"];
//    NSString *orderdateformatStr = [orderDateFormat stringFromDate:publishdate];
//    cell.orderDateLbl.text=orderdateformatStr;
            cell.orderDateLbl.text = strOrderDate;
    
    //Total Price
    NSString *strPrice = [NSString stringWithFormat:@"%.02f",[[[orderArr objectAtIndex:indexPath.row]objectForKey:@"grand_total"]floatValue]];
           
    cell.totalAmtLbl.text = [NSString stringWithFormat:@"₹ %@", strPrice];
    }

    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isSellGadgetHistory)
    {
//        SaleHistoryDetailViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SaleHistoryDetailViewController"];
          SGHistoryViewController *vc = [[UIStoryboard storyboardWithName:@"Product" bundle:nil] instantiateViewControllerWithIdentifier:@"SGHistoryViewController"];
            
        vc.orderId=[[orderArr objectAtIndex:indexPath.row]objectForKey:@"id"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        MyOrdersDetails *objMyOrdersDetail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyOrdersDetails"];
        objMyOrdersDetail.orderID=[[[orderArr objectAtIndex:indexPath.row]objectForKey:@"order_id"]intValue];
        objMyOrdersDetail.incrementID=[[[orderArr objectAtIndex:indexPath.row]objectForKey:@"increment_id"]intValue];
        [self.navigationController pushViewController:objMyOrdersDetail animated:YES];
    }
  
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //NSLog(@"indexpath:%ld",(long)indexPath.row);
    if (indexPath.row == [orderArr count] - 1 ) {
        [self loadMoreProducts:page_no];
    }
}


-(void)loadMoreProducts:(int)page_no_val{
    if (totalCount>(page_no*12)) {
        page_no = page_no_val;
        if(_isSellGadgetHistory)
            [self getSellGadgetData];
        else
            [self callWebserviceMyOrder];
    }else{
      //  [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"No more order available."];
    }
}

#pragma mark - 
#pragma mark -Actions
- (IBAction)backAction:(id)sender {
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        ShowTabbars *tabBarController = self.menuContainerViewController.centerViewController;
        UINavigationController *navigationController1 = (UINavigationController* )tabBarController.selectedViewController;
//        HomeViewController *homeObjc = [[UIStoryboard storyboardWithName:isIpad?@"Main_iPad":@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
        
        
           HomeViewController *homeObjc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
        
        NSArray *controllers = [NSArray arrayWithObject:homeObjc];
        navigationController1.viewControllers = controllers;
    }
}

- (IBAction)rightMenuAction:(id)sender {
        if(_isSellGadgetHistory)
        {
                [CommonSettings displaySellYourGadgetRightMenu:self];
        }
        else
        {
                [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
        }
}

#pragma mark - get sell gadget history
-(void)getSellGadgetData
{
    Webservice  *callMyOrdersWS = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        callMyOrdersWS.delegate =self;
        NSString *userId=[[NSUserDefaults standardUserDefaults]valueForKey:SELL_GADGET_LOGIN_USERID];
        
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
        NSMutableDictionary *dict = [NSMutableDictionary new];
        [dict setValue:userId?userId:@"" forKey:@"user_id"];
        [dict setObject:[NSNumber numberWithInteger:page_no] forKey:@"page_no"];
        [dict setObject:[NSNumber numberWithInt:12] forKey:@"page_size"];
        
        [callMyOrdersWS operationRequestToApi:dict url:SELL_GADGET_HISTORY_WS string:SELL_GADGET_HISTORY_WS];
        
    }
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:SELL_GADGET_HISTORY_WS]){
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            [self handleSellAndOrderHistoryData:response];

        }
        else{
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
        
    }
}
#pragma mark - get my orders

-(void)callWebserviceMyOrder{
    Webservice  *callMyOrdersWS = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {

        callMyOrdersWS.delegate =self;
        NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
       
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
        NSMutableDictionary *dict = [NSMutableDictionary new];
        [dict setValue:userId?userId:@"" forKey:@"user_id"];
        [dict setObject:[NSNumber numberWithInteger:page_no] forKey:@"page_no"];
        [dict setObject:[NSNumber numberWithInt:12] forKey:@"page_size"];

        [callMyOrdersWS operationRequestToApi:dict url:MY_ORDERS string:@"MYORDER"];
      
    }
}

-(void)receivedResponseForMyOrder:(id)receiveData stringResponse:(NSString *)responseType{
    [FVCustomAlertView hideAlertFromView:self.view fading:NO];
    
    if ([responseType isEqualToString:@"success"])
    {
        NSData *receiveOrderData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveOrderData options:kNilOptions error:nil];
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
    
        if ([[dict valueForKey:@"status"]intValue] == 1 )
        {
            [self handleSellAndOrderHistoryData:dict];
        }
        else{
            //[[CommonSettings sharedInstance]showAlertTitle:@"" message:[dict valueForKey:@"message"]];
        }
    }

}


-(void)handleSellAndOrderHistoryData:(NSMutableDictionary *)dict
{
    if (!([NSNull null]==[[dict objectForKey:@"data"]objectForKey:@"recent_orders"])) {
        [orderArr addObjectsFromArray:[[dict objectForKey:@"data"]objectForKey:@"recent_orders"]];
        totalCount=[[[dict objectForKey:@"data"]objectForKey:@"count"]intValue];
    }
    
    if (orderArr.count > 0)
    {
        emptyDataLabel.hidden = YES;
        _tableView.hidden = NO;
        _headerView.hidden = NO;
    }
    else
    {
        emptyDataLabel.hidden = NO;
        _tableView.hidden = YES;
        _headerView.hidden = YES;
    }
    
    
    [_tableView reloadData];
    page_no = page_no + 1;

}
@end
