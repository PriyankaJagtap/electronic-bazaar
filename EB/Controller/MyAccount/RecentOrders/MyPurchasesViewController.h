//
//  MyPurchasesViewController.h
//  EB
//
//  Created by webwerks on 9/2/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"
#import "CommonSettings.h"

@interface MyPurchasesViewController : BaseNavigationControllerWithBackBtn<FinishLoadingData>

@property (weak, nonatomic) IBOutlet UILabel *orderPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(nonatomic)BOOL isSelectable;
@property (weak, nonatomic) IBOutlet UILabel *orderStatus;
@property(nonatomic)BOOL isSellGadgetHistory;
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backAction:(id)sender;
- (IBAction)rightMenuAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIButton *rightMenuBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;

@end
