//
//  EditProfile.m
//  EB
//
//  Created by webwerks on 9/30/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "EditProfile.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "CommonSettings.h"

#define MAX_LENGTH 10

@implementation EditProfile
@synthesize tabBarObj;
@synthesize mobileTxt,emailTxt;
@synthesize cityTextField,refByTextField,nameTextField;
@synthesize pincodeTextField;

- (void)viewDidLoad {
        [super viewDidLoad];
    
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton: self.submitBtn];
        [_businessTypeBtn.layer setCornerRadius:5.0];
        [super setViewControllerTitle:@"Edit Profile"];
        // Set Place holder color
        ebPin = @"";
        UIColor *color = [UIColor grayColor];
        NSString *str = @"First Name";
        
        
        str = @"Store Name";
        self.nameTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.nameTextField withPlaceHolderName:str];
        
        str = @"EB PIN";
        self.ebPINTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.ebPINTextField withPlaceHolderName:str];
        
        str = @"Mobile No.";
        self.mobileTxt.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.mobileTxt withPlaceHolderName:str];
        
        
        str = @"Email Id";
        self.emailTxt.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.emailTxt withPlaceHolderName:str];
        
        str = @"Pincode";
        self.pincodeTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.pincodeTextField withPlaceHolderName:str];
        str = @"GST Identification Number";
        _gstNumberTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
       // [CommonSettings setAsterixToTextField:_gstNumberTextField withPlaceHolderName:str];
        
        
        str = @"PAN Number";
        _panNumberTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        
        mobileTxt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        
        pincodeTextField.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        
        mobileTxt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        
        [self createWrapView:_gstNumberTextField];
        [self createWrapView:_panNumberTextField];
        
        
        [self createWrapView:self.nameTextField];
        [self createWrapView:pincodeTextField];
        [pincodeTextField addTarget:self
                             action:@selector(pincodeValueChanged:)
                   forControlEvents:UIControlEventEditingChanged];
        
        [mobileTxt addTarget:self
                      action:@selector(mobileValueChanged:)
            forControlEvents:UIControlEventEditingChanged];
        
        // set wrap view for text field
        
        [self createWrapView:mobileTxt];
        [self createWrapView:emailTxt];
        
        [self createWrapView:cityTextField];
        [self createWrapView:refByTextField];
        [self createWrapView:_ebPINTextField];
        
        str = @"City";
        self.cityTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        
        [self setAllFieldValues];
}

-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:YES];
        
}

-(void)viewDidAppear:(BOOL)animated
{
        [super viewDidAppear:animated];
        [self getRegisterationData];
        
}

#pragma mark - utility methods
-(void) mobileValueChanged:(id)sender
{
        NSDictionary *dict = [[NSUserDefaults standardUserDefaults]objectForKey:@"user"] ;
        
        if(mobileTxt.text.length == 10 && ![mobileTxt.text isEqualToString:[dict objectForKey:@"mobile"]])
        {
                
                [self checkMobileNumberExist];
                
        }
}
-(void) pincodeValueChanged:(id)sender {
        // your code
        NSDictionary *userDic = [[NSUserDefaults standardUserDefaults] valueForKey:@"user"];
        if(pincodeTextField.text.length  == 6 && ![pincodeTextField.text isEqualToString:[userDic valueForKey:@"pincode"]])
        {
                [self getCityFromPincode];
        }
        else
        {
        }
}



-(void)checkMobileNumberExist
{
        [self.view endEditing:YES];
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",CHECK_MOBILE_WS,mobileTxt.text] MathodName:CHECK_MOBILE_WS];
                
        }
}


-(void)checkEmailExist
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",CHECK_EMAIL_WS,emailTxt.text] MathodName:CHECK_EMAIL_WS];
                
        }
        
}
-(void)getCityFromPincode
{
        
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                
                NSDictionary *userdata = [[NSDictionary alloc] initWithObjectsAndKeys:pincodeTextField.text,@"pincode", nil];
                callLoginService.delegate =self;
                [callLoginService operationRequestToApi:userdata url:GET_CITY_FROM_PINCODE string:@"GET_CITY_FROM_PINCODE"];
                
        }
}

-(void)setAllFieldValues
{
        //        {
        //                dob = "01/01/1970";
        //                email = "rakshita.hegde@wwindia.com";
        //                firstname = RakshitaStore;
        //                "is_arm" = 0;
        //                lastname = "<null>";
        //                mobile = 9867447961;
        //                password = password;
        //                pincode = 400089;
        //                "profile_picture" = "";
        //                taxvat = "<null>";
        //                "user_id" = 29080;
        //        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *dict = [[NSUserDefaults standardUserDefaults]objectForKey:@"user"] ;
        if (dict.count > 0) {
                
                if([dict objectForKey:@"firstname"])
                        self.nameTextField.text = [dict objectForKey:@"firstname"];
                
                
                //Mobile
                NSString *mobileStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"mobile"]];
                mobileTxt.text = mobileStr?mobileStr:@"";
                if ([mobileStr isEqualToString:@"<null>"]) {
                        mobileTxt.text=@"";
                }
                
                //email
                NSString *emailStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"email"]];
                emailTxt.text = emailStr?emailStr:@"";
                
                if([dict objectForKey:@"business_type"])
                        [_businessTypeBtn setTitle:[dict objectForKey:@"business_type"] forState:UIControlStateNormal];
                
                if([dict objectForKey:@"referred_by"])
                        [_refByBtn setTitle:[dict objectForKey:@"referred_by"] forState:UIControlStateNormal];
                
                
                if([dict objectForKey:@"pincode"])
                        pincodeTextField.text = [dict objectForKey:@"pincode"];
                
                
                if([dict objectForKey:@"gstin"])
                        _gstNumberTextField.text = [dict objectForKey:@"gstin"];
                
                if([dict objectForKey:@"taxvat"])
                        _panNumberTextField.text = [dict objectForKey:@"taxvat"];
        }
        NSLog(@"user data %@",[defaults valueForKey:@"user"]);
}
-(NSAttributedString *)String:(NSString *)str Range:(int )Rannge
{
        NSMutableAttributedString *attString =
        [[NSMutableAttributedString alloc]
         initWithString:str];
        
        [attString addAttribute: NSForegroundColorAttributeName
                          value: [UIColor grayColor]
                          range: NSMakeRange(0,Rannge)];
        
        //    [attString addAttribute: NSForegroundColorAttributeName
        //                      value: [UIColor redColor]
        //                      range: NSMakeRange(Rannge,1)];
        
        
        return attString;
        
}




-(void)callEditProfileWS
{
        Webservice  *callEditProfile = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callEditProfile.delegate =self;
                int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                
                NSString *businessTypeBtnTitile = [_businessTypeBtn titleForState:UIControlStateNormal];
                NSArray *filterBusinessArr =[businessTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(label == %@)",businessTypeBtnTitile ]];
                
                businessType = [NSString stringWithFormat:@"%@",[[filterBusinessArr objectAtIndex:0] valueForKey:@"id"]];
                
                NSString *refBy=[[[refByArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(asm_name == %@)", [_refByBtn titleForState:UIControlStateNormal]]] objectAtIndex:0] valueForKey:@"asm_id"];
                
                NSString *url = [NSString stringWithFormat:@"%@business_type=%@&asm_id=%@&pincode=%@&mobile=%@&gstin=%@&email=%@&pan=%@&firstname=%@&userid=%d",EDIT_PROFILE,businessType,refBy,pincodeTextField.text,mobileTxt.text,_gstNumberTextField.text,emailTxt.text,_panNumberTextField.text,nameTextField.text,userId];
                
                [callEditProfile GetWebServiceWithURL:url MathodName:EDIT_PROFILE];
                //        [callEditProfile webServiceWitParameters:[NSString stringWithFormat:@"userid=%d&mobile=%@&firstname=%@&lastname=%@&company=%@&dob=%@",userId,mobileTxt.text,firstNameTxt.text,lastNameTxt.text,companyTxt.text,dobTxt.text] andURL:EDIT_PROFILE MathodName:@"EditProfile"];
        }
}


#pragma mark - TextField delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{        [textField resignFirstResponder];
        return  YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if(textField == mobileTxt)
                {
                        if(textField.text.length < 10)
                        {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                        }
                }
                else if (textField == emailTxt)
                {
                        NSDictionary *userDic = [[NSUserDefaults standardUserDefaults] valueForKey:@"user"];
                        if ([self validEmail:emailTxt.text] && ![[userDic valueForKey:@"email"] isEqualToString:emailTxt.text]) {
                                [self checkEmailExist];
                        }
                        
                }
                
        });
        
        
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
        
        [self hideAllDropDown];
        
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
        
        if(textField == emailTxt)
        {
                return YES;
        }
        else if(textField == mobileTxt)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > MAX_LENGTH){
                        
                        return NO;
                }
                else{
                        return YES;
                }
        }
        else if(textField == _gstNumberTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_GST_NUMBER_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                if(newLength <= 15)
                {
                        return [string isEqualToString:filtered];
                }
                
                if(newLength > 15){
                        
                        return NO;
                }
        }
        else if (textField == _panNumberTextField)
        {
                
                NSUInteger newLength = [textField.text length] + [string length];
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_PAN_NUMBER_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                if(newLength <= 10)
                {
                        return [string isEqualToString:filtered];
                }
                
                if(newLength > 10){
                        
                        return NO;
                }
                
        }
        else if(textField == pincodeTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 6){
                        
                        return NO;
                }
                else{
                        cityTextField.text = nil;
                        return YES;
                }
        }
        else
        {
                if(range.length + range.location > textField.text.length)
                {
                        return NO;
                }
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return newLength <= 25;
        }
        
        return YES;
}



#pragma mark - Email/ Phone Validation
-(void)hideAllDropDown
{
        [self hideAllGroupDropDown];
}

-(void)hideAllGroupDropDown
{
        [refByDropDown hideDropDown:_refByBtn];
        [refByDropDown removeFromSuperview];
        refByDropDown = nil;
}
-(BOOL)validEmail:(NSString*)emailString
{
        NSString *emailid = emailString;
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        BOOL myStringMatchesRegEx=[emailTest evaluateWithObject:emailid];
        
        if(myStringMatchesRegEx)
        {
                return YES;
        }
        else
        {
                return NO;
        }
}

-(BOOL) validatePhoneNumber:(NSString *)enterNumber
{
        NSString *phoneRegex = @"[12356789][0-9]{6}([0-9]{3})?";
        NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
        BOOL matches = [test evaluateWithObject:enterNumber];
        return matches;
}

#pragma mark - NIDropdown

- (void)rel
{
        
        refByDropDown = nil;
        businessTypeDropDown = nil;
}

- (void)niDropDownDelegateMethod:(NIDropDown *) sender
{
        [self rel];
        
}

- (void) getindexValue: (NIDropDown *)sender withindex:(NSIndexPath*)index{
        DLog(@"index %ld",(long)index.row);
        
        //    if(sender.tag == 11){
        //        if (dateArr.count > 0) {
        //            NSString *strDay = [NSString stringWithFormat:@"%@",[dateArr objectAtIndex:(long)index.row]];
        //            ddTxt.text = strDay;
        //        }
        //    }
        //    else if (sender.tag == 22 ){
        //        if (monthArr.count > 0) {
        //            NSString *strMonth = [NSString stringWithFormat:@"%@",[monthArr objectAtIndex:(long)index.row]];
        //            mmTxt.text = strMonth;
        //        }
        //    }
        //    else if (sender.tag == 33 ){
        //        if (yearArr.count > 0) {
        //            NSString *strYear = [NSString stringWithFormat:@"%@",[yearArr objectAtIndex:(long)index.row]];
        //            yyyyTxt.text = strYear;
        //        }
        //    }
        
}


- (IBAction)refByBtnClicked:(id)sender {
        
        
        //        if(self.nameTextField.text.length == 0 || mobileTxt.text.length == 0 || _ebPINTextField.text.length == 0 || emailTxt.text.length == 0 )
        //        {
        //               // [self displayAlertForPreviousFields];
        //                return;
        //        }
        //
        //        if(pincodeTextField.text.length == 0)
        //        {
        //                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter pincode." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //                [alert show];
        //                return;
        //        }
        //
        NSArray * arr = [refByArray valueForKey:@"asm_name"];
        _refByBtn.tag = 512;
        if(refByDropDown == nil){
                CGFloat f= 100.0;
                if(arr.count <= 3)
                        f = 30 * arr.count;
                refByDropDown = [[NIDropDown alloc] showDropDown:_refByBtn withHeight:&f withData:arr withImages:Nil WithDirection:@"down"];
                
                refByDropDown.displayDropDownBool = YES;
                refByDropDown.delegate = self;
                
        }
        else{
                [refByDropDown hideDropDown:sender];
                [self rel];
        }
}
#pragma mark - get resgisteration data
-(void)getRegisterationData
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:GET_REGISTRATION_DATA MathodName:@"GET_REGISTRATION_DATA"];
                
        }
}


#pragma mark - get EB PIN
-(void)generateEBPin
{
        
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                NSString *url = [NSString stringWithFormat:@"%@%@",GENERATE_EB_PIN_WS,mobileTxt.text];
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:url MathodName:@"GENERATE_EB_PIN_DATA"];
        }
}


#pragma mark - IBAction Methods
-(void)doneButtonDidPressed:(id)sender
{
        dispatch_async(dispatch_get_main_queue(), ^{
                [self.view endEditing:YES];
                
        });
}

- (IBAction)backAction:(id)sender {
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightMenuAction:(id)sender {
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (IBAction)retailerBtnClicked:(id)sender {
        
        UIButton *btn = (UIButton *)sender;
        
        if(businessTypeArr.count != 0)
        {
                NSArray * arr = [businessTypeArr valueForKey:@"label"];
                if(businessTypeDropDown == nil){
                        CGFloat f= 100.0;
                        if(arr.count <= 3)
                                f = 30 * arr.count;
                        businessTypeDropDown = [[NIDropDown alloc] showDropDown:btn withHeight:&f withData:arr withImages:Nil WithDirection:@"down"];
                        businessTypeDropDown.delegate = self;
                        
                }
                else{
                        [businessTypeDropDown hideDropDown:sender];
                        [self rel];
                }
                
        }
        
}

- (IBAction)generateEBPinBtnClicked:(id)sender {
        [self.view endEditing:YES];
        [self performSelector:@selector(display)  withObject:nil afterDelay:1.0];
        
        
}
-(void) display{
        if(mobileTxt.text.length < 10)
        {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit Mobile Number"];
        }
        else
        {
                [self generateEBPin];
        }
}
- (IBAction)updateAction:(id)sender {
        
        [self.view endEditing:NO];
        
        NSDictionary *dict = [[NSUserDefaults standardUserDefaults]objectForKey:@"user"] ;
        if (self.nameTextField.text.length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter store name."];
                return;
        }
        
        
        if (mobileTxt.text){
                if (mobileTxt.text.length < 10) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit Mobile Number"];
                        return;
                }
        }
        if (![mobileTxt.text isEqualToString:[dict objectForKey:@"mobile"]] ){
                
                if(_ebPINTextField.text.length == 0)
                {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter EB PIN."];
                        return;
                }
                else if(![_ebPINTextField.text isEqualToString:ebPin])
                {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Valid EB PIN."];
                        return;
                }
        }
        
        if (emailTxt.text){
                
                if (emailTxt.text.length == 0) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
                        return;
                }
                if (![self validEmail:emailTxt.text]) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
                        return;
                        
                }
        }
        
        if (pincodeTextField.text.length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter pincode."];
                return;
        }
        
        //                if (cityTextField.text.length == 0){
        //                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter valid pincode."];
        //                        return;
        //                }
        
        
        if ([_refByBtn titleForState:UIControlStateNormal].length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please select Referred by."];
                return;
        }
        else if ([[_refByBtn titleForState:UIControlStateNormal] isEqualToString:[[refByArray objectAtIndex:0] valueForKey:@"asm_name"]])
        {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please select Referred by."];
                return;
        }
        
        
        if ([_businessTypeBtn titleForState:UIControlStateNormal].length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please select Business Type."];
                return;
        }
        else if ([[_businessTypeBtn titleForState:UIControlStateNormal] isEqualToString:[[businessTypeArr objectAtIndex:0] valueForKey:@"label"]])
        {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please select Business Type."];
                return;
        }
        
        if (_gstNumberTextField.text.length != 0){
                if (_gstNumberTextField.text.length < 15) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide valid GST Number"];
                        return ;
                }
        }
        
        if (_panNumberTextField.text.length != 0){
                
                if (![CommonSettings validatePanCardNumber:_panNumberTextField.text]) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check PAN Number format."];
                        return;
                        
                }
        }
        
        //        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        //        // Call webservice
        //        NSMutableDictionary * dict = [NSMutableDictionary new];
        //
        //        NSString *refBy=[[[refByArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(asm_name == %@)", [_refByBtn titleForState:UIControlStateNormal]]] objectAtIndex:0] valueForKey:@"asm_id"];
        //
        //        [dict setObject:refBy forKey:@"asm_id"];
        //        //[dict setObject:addressTextField.text forKey:@"landmark"];
        //        [dict setObject:cityTextField.text forKey:@"cus_city"];
        //        [dict setObject:emailTxt.text forKey:@"email"];
        //        [dict setObject:passwordTxt.text forKey:@"password"];
        //        [dict setObject:@"iphone" forKey:@"device_type"];
        //        NSString *pushID=[defaults valueForKey:@"DEVICE_TOKEN"];
        //        [dict setObject:pushID?:@"" forKey:@"push_id"];
        //        [dict setObject:self.nameTextField.text forKey:@"name"];
        //        [dict setObject:self.nameTextField.text forKey:@"store_name"];
        //        [dict setObject:mobileTxt.text forKey:@"mobile"];
        //        [dict setObject:pincodeTextField.text forKey:@"pincode"];
        //        NSString *businessTypeBtnTitile = [_businessTypeBtn titleForState:UIControlStateNormal];
        //        NSArray *filterBusinessArr =[businessTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(label == %@)",businessTypeBtnTitile ]];
        //
        //        businessType = [NSString stringWithFormat:@"%@",[[filterBusinessArr objectAtIndex:0] valueForKey:@"id"]];
        //        [dict setObject:businessType forKey:@"businessType"];
        //        [dict setObject:_ebPINTextField.text forKey:@"EB_PIN_USER"];
        //        [dict setObject:ebPin forKey:@"EB_PIN_SERVER"];
        //        [dict setObject:_gstNumberTextField.text forKey:@"gstin"];
        //        [dict setObject:_panNumberTextField.text forKey:@"taxvat"];
        //        NSLog(@"registeration dic %@",dict);
        //        //[self callRegistrationWS:dict];
        
        
        [self callEditProfileWS];
}

#pragma mark - Utility Methods
- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}
-(void)receivedResponseForCityData:(id)receiveData stringResponse:(NSString*)responseType
{
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        NSMutableDictionary *dictUserDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
        
        NSLog(@"userDetail %@",jsonObject);
        if([[dictUserDetail valueForKey:@"status"] boolValue])
        {
                cityTextField.text = [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"city"];
                if([[[dictUserDetail valueForKey:@"address_data"] valueForKey:@"city"] isKindOfClass:[NSNull class]] ||[[[dictUserDetail valueForKey:@"address_data"] valueForKey:@"default_name"] isKindOfClass:[NSNull class]] ||[ [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"country_code"] isKindOfClass:[NSNull class]] )
                {
                        cityTextField.text = @"";
                }
        }
        else
        {
                [APP_DELEGATE showAlert:@"" withMessage:[dictUserDetail valueForKey:@"message"]];
                //_stateBtn.userInteractionEnabled = true;
                cityTextField.text = nil;
                pincodeTextField.text = nil;
        }
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        
        if ([strName isEqualToString:@"GET_REGISTRATION_DATA"]){
                //NSLog(@"%@",response);
                
                NSLog(@"user dic %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"user"]);
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        
                        refByArray = [response valueForKey:@"customer_referedby"];
                        businessTypeArr = [response valueForKey:@"business_type"];
                        
                        if (refByArray.count != 0 && [_refByBtn titleForState:UIControlStateNormal].length == 0)
                                [_refByBtn setTitle:[[refByArray objectAtIndex:0] valueForKey:@"asm_name"] forState:UIControlStateNormal];
                        
                        if (businessTypeArr.count != 0 && [_businessTypeBtn titleForState:UIControlStateNormal].length == 0)
                        {
                                [_businessTypeBtn setTitle:[[businessTypeArr objectAtIndex:0] valueForKey:@"label"] forState:UIControlStateNormal];
                                businessType = [NSString stringWithFormat:@"%@",[[businessTypeArr objectAtIndex:0] valueForKey:@"id"]];
                        }
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                
        }
        else if ([strName isEqualToString:@"GENERATE_EB_PIN_DATA"]){
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        ebPin = [response valueForKey:@"ebpin"];
                        _ebPinSendLabel.text = [response valueForKey:@"message"];
                        [_generateEBpinBtn setTitle:@"Resend EB Pin" forState:UIControlStateNormal];
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        
        else if ([strName isEqualToString:CHECK_MOBILE_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==0)
                {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        alert.tag = 500;
                        [alert show];
                        
                }
                else if([[response valueForKey:@"status"]intValue]==1)
                {
                        [self.view layoutIfNeeded];
                        _generateEbPinTopConstraint.constant = 8;
                        _generateEBPinHeightConstraint.constant = 30;
                        _generateEbPinBottomConstraint.constant = 8;
                        _ebPinTextFieldHtConstraint.constant = 30;
                        _ebPinTextFieldBottomConstraint.constant = 8;
                        [self.view layoutIfNeeded];
                }
                
        }
        else if ([strName isEqualToString:CHECK_EMAIL_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==0)
                {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                        emailTxt.text = nil;
                }
                
        }
        
        else if([strName isEqualToString:EDIT_PROFILE])
        {
                if ([[response valueForKey:@"status"]integerValue]==1)
                {
                        NSDictionary *dictUserDetail = response;
                        NSString *loginType = [dictUserDetail objectForKey:@"login_type"];
                        NSMutableDictionary *userdict = [NSMutableDictionary new];
                        
                        NSString *uid =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"id"]];
                        NSString *firstname = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"firstname"]];
                        NSString *lastname = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"lastname"]];
                        NSString *dob = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"dob"]];
                        NSString *mobile = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"mobile"]];
                        NSString *profile_picture = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"profile_picture"]];
                        NSString *quote_ID = [NSString stringWithFormat:@"%@",[dictUserDetail objectForKey:@"quoteId"]];
                        
                        // NSString *company_Name=[]
                        
                        NSString *email = @"";
                        if ([loginType isEqualToString:@"normal"]) {
                                email = emailTxt.text;
                        }
                        else{
                                email =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"email"]];
                        }
                        
                        [userdict setObject:uid?uid:@"" forKey:@"user_id"];
                        [userdict setObject:firstname?firstname:@"" forKey:@"firstname"];
                        [userdict setObject:lastname?lastname:@"" forKey:@"lastname"];
                        [userdict setObject:dob?dob:@"" forKey:@"dob"];
                        [userdict setObject:mobile?mobile:@"" forKey:@"mobile"];
                        [userdict setObject:profile_picture?profile_picture:@"" forKey:@"profile_picture"];
                        [userdict setObject:email forKey:@"email"];
                        // [userdict setObject:passwordTxt.text forKey:@"password"];
                        [userdict setObject:[NSNumber numberWithBool:[[[dictUserDetail objectForKey:@"user"] objectForKey:@"is_arm"] boolValue]] forKey:@"is_arm"];
                        
                        NSString *businessType1 = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"business_type"]];
                        
                        if(![businessType1 isEqualToString:@""])
                                [userdict setValue:businessType1 forKey:@"business_type"];
                        
                        
                        if([[dictUserDetail objectForKey:@"user"] objectForKey:@"gstin"])
                        {
                                
                                NSString *gstin = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"gstin"]];
                                if(![gstin isEqualToString:@""])
                                {
                                        [userdict setValue:gstin forKey:@"gstin"];
                                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:NO] forKey:SHOW_GSTIN];
                                }
                                else
                                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:YES] forKey:SHOW_GSTIN];
                        }
                        
                        if([[dictUserDetail objectForKey:@"user"] objectForKey:@"pan_number"] &&![[[dictUserDetail objectForKey:@"user"] objectForKey:@"pan_number"] isKindOfClass:[NSNull class]] && ![[[dictUserDetail objectForKey:@"user"] objectForKey:@"pan_number"] isEqualToString:@""])
                        {
                                NSString *taxVat = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"pan_number"]];
                                [userdict setValue:taxVat forKey:@"taxvat"];
                        }
                        
                        //
                        NSString *referredBy =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"referred_by"]];
                        if(![referredBy isEqualToString:@""])
                                [userdict setValue:referredBy forKey:@"referred_by"];
                        
                        
                        
                        
                        //                if(![[[dictUserDetail valueForKey:@"user"] valueForKey:@"pincode"] isKindOfClass:[NSNull class]])
                        if(![[[dictUserDetail valueForKey:@"user"] valueForKey:@"pincode"] isEqualToString:@""])
                        {
                                [userdict setValue:[[dictUserDetail valueForKey:@"user"] valueForKey:@"pincode"] forKey:@"pincode"];
                        }
                        
                        
                        if(![[[dictUserDetail valueForKey:@"user"] valueForKey:@"store_name"] isKindOfClass:[NSNull class]])
                        {
                                [userdict setValue:[[dictUserDetail valueForKey:@"user"] valueForKey:@"store_name"] forKey:@"storeName"];
                        }
                        //
                        if([[dictUserDetail valueForKey:@"customer_id"] integerValue] == 4)
                        {
                                NSInteger i = [[dictUserDetail valueForKey:@"customer_id"] integerValue];
                                [[NSUserDefaults standardUserDefaults]setObject: [NSNumber numberWithInteger:i]forKey:@"AffiliateStatus"];
                        }
                        else
                        {
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:0] forKey:@"AffiliateStatus"];
                        }
                        
                        [[NSUserDefaults standardUserDefaults]setObject:quote_ID forKey:@"QuoteID"];
                        [[NSUserDefaults standardUserDefaults] setObject:userdict forKey:@"user"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        alert.tag = 100;
                        [alert show];
                        
                        //                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                else
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                
        }else{
                
        }
        
        //NSLog(@"Response= %@",response);
}



-(void)createWrapView:(UITextField *)textfield{
        UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 10, textfield.frame.size.height)];
        textfield.leftViewMode = UITextFieldViewModeAlways;
        textfield.leftView = wrapView;
}

#pragma mark - Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
        if (alertView.tag == 100 && buttonIndex == 0) {
                [self.navigationController popViewControllerAnimated:YES];
        }
}

@end
