//
//  EditProfile.h
//  EB
//
//  Created by webwerks on 9/30/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"
#import "NIDropDown.h"

@interface EditProfile : BaseNavigationControllerWithBackBtn<FinishLoadingData,NIDropDownDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
{
        NSString *ebPin;
        NSString *cityText;
        NSArray *businessTypeArr;
        NSArray *refByArray;
        NSString *businessType;
        
        
        NIDropDown *refByDropDown;
        NIDropDown *businessTypeDropDown;
}

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UITextField *gstNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *panNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *ebPinSendLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ebPinLabelHt;
@property (weak, nonatomic) IBOutlet UIButton *generateEBpinBtn;

@property (weak, nonatomic) IBOutlet UITextField *refByTextField;
@property (weak, nonatomic) IBOutlet UIButton *refByBtn;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;

@property (weak, nonatomic) IBOutlet UITextField *pincodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *ebPINTextField;
@property (nonatomic) BOOL isFromMycartView;
@property (nonatomic) BOOL isFromHomeView;

@property (weak, nonatomic) IBOutlet UITextField *mobileTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;



@property (strong,nonatomic)ShowTabbars *tabBarObj;
- (IBAction)retailerBtnClicked:(id)sender;
//@property (nonatomic, strong) NSDictionary *userdict;
@property (weak, nonatomic) IBOutlet UIButton *businessTypeBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *generateEBPinHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *generateEbPinBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *generateEbPinTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ebPinTextFieldHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ebPinTextFieldBottomConstraint;


@end
