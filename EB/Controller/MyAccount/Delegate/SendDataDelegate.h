//
//  SendDataDelegate.h
//  EB
//
//  Created by webwerks on 23/05/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#ifndef SendDataDelegate_h
#define SendDataDelegate_h

#import <UIKit/UIKit.h>

@protocol SendDataDelegate <NSObject>

-(void)acceptRecord:(NSDictionary *) orderDict;

@end

#endif /* SendDataDelegate_h */

