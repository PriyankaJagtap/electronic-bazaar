//
//  ChangePassword.m
//  EB
//
//  Created by Aishwarya Rai on 10/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "ChangePassword.h"

#define PASSWORD_TEXTFIELD_LENGTH 8

#import "MFSideMenuContainerViewController.h"
#import "AppDelegate.h"
#import "Validation.h"
#import "CommonSettings.h"
#import "Webservice.h"

@interface ChangePassword ()<FinishLoadingData,UITextFieldDelegate>

@end

@implementation ChangePassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    //[self setUpNavigationBar];
    [super setViewControllerTitle:@"Change Password"];
        
    self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
    [self createWrapView:password_Txt];
    [self createWrapView:confirm_Password_Txt];
    [self createWrapView:old_Password_Txt];
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_submitBtn];
        
    UIColor *color = [UIColor grayColor];
    NSString *str = @"New Password";
    password_Txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName: color}];
    [CommonSettings setAsterixToTextField:password_Txt withPlaceHolderName:str];
      
    str = @"Confirm Password";
    confirm_Password_Txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName: color}];
   [CommonSettings setAsterixToTextField:confirm_Password_Txt withPlaceHolderName:str];

      
    str = @"Old Password";
    old_Password_Txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName: color}];
        [CommonSettings setAsterixToTextField:old_Password_Txt withPlaceHolderName:str];

    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UItextField Delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [textField resignFirstResponder];
        return YES;
}

#pragma mark - Utility Methods
-(void)setUpNavigationBar
{
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent=NO;
    UILabel *titleLabel=[[UILabel alloc]init];
    titleLabel.text = @"Change Password";
    titleLabel.font =KarlaFontBold(17.0);
    titleLabel.frame = CGRectMake(0, 0, 100, 30);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabel;
    //    UIColor *bgColor=[UIColor colorWithRed:22/255.0f green:70/255.0f blue:134/255.0f alpha:1.0f];
    
    UIColor *bgColor=[[AppDelegate getAppDelegateObj] colorWithHexString:@"193C64"];
    
    self.navigationController.navigationBar.barTintColor = bgColor;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.backItem.title =@"";
    
    // Set Place holder color
    UIColor *color = [UIColor whiteColor];
    password_Txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter new password" attributes:@{NSForegroundColorAttributeName: color}];
    confirm_Password_Txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];
    old_Password_Txt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Old password" attributes:@{NSForegroundColorAttributeName: color}];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 18, 18);
    [backButton addTarget:self action:@selector(backButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    backButton.showsTouchWhenHighlighted = YES;
    UIImage *backButtonImage = [UIImage imageNamed:@"ios_back_btn"];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItems = @[backBarButtonItem];
}

-(void)backButtonTapped{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)callChangePasswordWs
{
    Webservice  *callRegionList = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callRegionList.delegate =self;
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
        [callRegionList webServiceWitParameters:[NSString stringWithFormat:@"customerId=%d&new_password=%@&old_password=%@",[CommonSettings getUserID],confirm_Password_Txt.text,old_Password_Txt.text] andURL:CHANGE_PASSWORD_WS MathodName:@"CHANGEPASSWORD"];
    }
    
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:@"CHANGEPASSWORD"]) {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            old_Password_Txt.text=@"";
            password_Txt.text=@"";
            confirm_Password_Txt.text=@"";
        });
    }
}


- (IBAction)onClickOf_Submit:(id)sender {
    [self.view endEditing:YES];
    if ([self validateForm]) {
        if ([password_Txt.text isEqualToString:confirm_Password_Txt.text])
        {
            [self callChangePasswordWs];
        }else{
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Confirm password does not match to new password"];
        }
    }
}


-(BOOL)validateForm
{
        
        if ([old_Password_Txt.text isEqualToString:@""]) {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter old password"];
                return NO;
        }
        if([password_Txt.text isEqualToString:@""]){
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter new  password"];
                return NO;
        }
        
        if([confirm_Password_Txt.text isEqualToString:@""]){
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter confirm password"];
                return NO;
        }
        
   
    
    return YES;
}

-(void)createWrapView:(UITextField *)textfield{
    UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 10, textfield.frame.size.height)];
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = wrapView;
}



@end
