//
//  ChangePassword.h
//  EB
//
//  Created by Aishwarya Rai on 10/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "BaseNavigationControllerWithBackBtn.h"

@interface ChangePassword : BaseNavigationControllerWithBackBtn
{
    __weak IBOutlet UITextField *old_Password_Txt;
    __weak IBOutlet UITextField *confirm_Password_Txt;
    __weak IBOutlet UITextField *password_Txt;
}
- (IBAction)onClickOf_Submit:(id)sender;

@property (weak,nonatomic) IBOutlet UIButton *submitBtn;
@end
