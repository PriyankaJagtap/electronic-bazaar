//
//  ProductListViewController.m
//  EB
//
//  Created by webwerks on 8/19/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductDetailsViewController.h"
#import "AppDelegate.h"
#import "CommonSettings.h"

@interface ProductListViewController ()
{
    NSMutableArray *productListArr;
    int page_no_val;
    int page_size_val;
    int totalCount;
    UIButton *favouriteButton;
}

@end

@implementation ProductListViewController
@synthesize page_size, page_no, type,viewAllListTitle;

#pragma mark - ViewController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    productListArr = [NSMutableArray new];
    
    // initialize
    page_size_val = [page_size intValue];
    page_no_val = [page_no intValue];
    
    [[[AppDelegate getAppDelegateObj] tabBarObj] unhideTabbar];
    
    if ([type isEqualToString:@"hotdeals"])
    {
        self.title = @"Hot Deals";
        viewAllListTitle.text = @"Hot Deals";
    }
    else if ([type isEqualToString:@"featuredproducts"])
    {
        viewAllListTitle.text = @"Featured Products";
    }
    else if ([type isEqualToString:@"bestsellers"])
    {
        viewAllListTitle.text = @"Best Sellers";
    }
    
    NSMutableDictionary *dictData = [NSMutableDictionary new];
    [dictData setObject:type forKey:@"type"];
    [dictData setObject:page_no forKey:@"page_no"];
    [dictData setObject:page_size forKey:@"page_size"];
    
    [self callViewAllProductWS:dictData];   //Call Webserice
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [CommonSettings sendScreenName:@"CategoryView"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 10 < totalCount? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0 ? [productListArr count] : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        
    }
    else
    {
        if ([type isEqualToString:@"hotdeals"])
        {
            static NSString *CellIdentifier = @"ProductListCell";
            ProductListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[ProductListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.backgroundColor = [UIColor clearColor];
            
            // set corner radius to background view
            cell.bgView.layer.cornerRadius = 10;
            cell.productNameLbl.textColor = [[AppDelegate getAppDelegateObj]colorWithHexString:@"#4f4f4f"];
            cell.productNameLbl.numberOfLines = 0;
            
            if (productListArr.count > 0)
            {
                cell.discount.text =[NSString stringWithFormat:@"%@%@ off",[[productListArr objectAtIndex:indexPath.row]objectForKey:@"discount"],@"%"];
                cell.productNameLbl.text = [NSString stringWithFormat:@"%@",[[productListArr objectAtIndex:indexPath.row]objectForKey:@"name"]];
                cell.productPriceLbl.text = [[CommonSettings sharedInstance] formatIntegerPrice:[[[productListArr objectAtIndex:indexPath.row]objectForKey:@"price"] integerValue]];
                
                int isProductinStock = [[[productListArr objectAtIndex:indexPath.row]valueForKey:@"is_instock"]intValue];
                if (isProductinStock)
                {
                    cell.outofProductImg.hidden = YES;
                }
                else
                {
                    cell.outofProductImg.hidden = NO;
                }
                
                // image
                NSString*imageLink = [NSString stringWithFormat:@"%@",[[productListArr objectAtIndex:indexPath.row]objectForKey:@"image"]];
                [cell.productImg sd_setImageWithURL:[NSURL URLWithString:imageLink]] ;
                cell.productImg.contentMode = UIViewContentModeScaleAspectFit;
            }
            return cell;
        }
        else if ([type isEqualToString:@"featuredproducts"] || [type isEqualToString:@"bestsellers"] )
        {
            static NSString *CellIdentifier = @"ViewAllProductCell";
            ViewAllProductCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[ViewAllProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.backgroundColor = [UIColor clearColor];
            
            // set corner radius to background view
            cell.bgView.layer.cornerRadius = 10;
            cell.product_nameLbl.textColor = [[AppDelegate getAppDelegateObj]colorWithHexString:@"#4f4f4f"];
            cell.product_nameLbl.numberOfLines = 0;
            
            if (productListArr.count > 0)
            {
                cell.product_nameLbl.text = [NSString stringWithFormat:@"%@",[[productListArr objectAtIndex:indexPath.row]objectForKey:@"name"]];
                
                //Price
                //                cell.product_priceLbl.text = [NSString stringWithFormat:@"Rs %ld",(long)[[[productListArr objectAtIndex:indexPath.row]objectForKey:@"price"] integerValue]];
                
                cell.product_priceLbl.text = [[CommonSettings sharedInstance] formatIntegerPrice:[[[productListArr objectAtIndex:indexPath.row]objectForKey:@"price"] integerValue]];
                // image
                NSString*imageLink = [NSString stringWithFormat:@"%@",[[productListArr objectAtIndex:indexPath.row]objectForKey:@"image"]];
                [cell.product_img sd_setImageWithURL:[NSURL URLWithString:imageLink]];
                cell.product_img.contentMode = UIViewContentModeScaleAspectFit;
                
                int isFav=[[[productListArr objectAtIndex:indexPath.row]valueForKey:@"is_favourite"]intValue];
                if (isFav)
                {
                    [cell.favBtn setImage:[UIImage imageNamed:@"favorate_icon_selected"] forState:UIControlStateNormal];
                }
                else
                {
                    [cell.favBtn setImage:[UIImage imageNamed:@"favorate_icon"] forState:UIControlStateNormal];
                }
                
                int isProductinStock=[[[productListArr objectAtIndex:indexPath.row]valueForKey:@"is_instock"]intValue];
                if (isProductinStock)
                {
                    cell.outOfProduct_img.hidden=YES;
                }
                else
                {
                    cell.outOfProduct_img.hidden=NO;
                }
                favouriteButton=cell.favBtn;
                cell.favBtn.tag=indexPath.row;
                [cell.favBtn addTarget:self action:@selector(favouriteButtonPresses:) forControlEvents:UIControlEventTouchUpInside];
            }
            return cell;
        }
        if (indexPath.row == [productListArr count] - 1 )
        {
            [self loadMoreProducts:page_no_val];
        }
    }
    return nil;
}

-(void)favouriteButtonPresses:(UIButton*)btnFav
{
    int productId=[[[productListArr objectAtIndex:btnFav.tag]valueForKey:@"product_id"]intValue];
    [self didselectFavouriteProduct:productId];
    [btnFav setImage:[UIImage imageNamed:@"favorate_icon_selected"] forState:UIControlStateNormal];
}

-(void)didselectFavouriteProduct:(int)productId
{
    NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
    Webservice  *addToWishlistService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        if (userId==0)
        {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
        }
        else
        {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            addToWishlistService.delegate =self;
            [addToWishlistService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%@&productId=%d",ADDTOWISHLIST,userId,productId] MathodName:@"ADDTOWISHLIST"];
        }
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:isIpad?@"Main_iPad":@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    int prodId = [[[productListArr objectAtIndex:indexPath.row]objectForKey:@"product_id"]intValue];
    objProductDetailVC.productID = prodId;
    [self.navigationController pushViewController:objProductDetailVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [productListArr count] - 1 )
    {
        [self loadMoreProducts:page_no_val];
    }
}

#pragma mark - Back Button
- (IBAction)RightSlideAction:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (IBAction)backBtnAction:(id)sender
{
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        ShowTabbars *tabBarController = self.menuContainerViewController.centerViewController;
        UINavigationController *navigationController1 = (UINavigationController* )tabBarController.selectedViewController;
        //        HomeViewController *homeObjc = [[UIStoryboard storyboardWithName:isIpad?@"Main_iPad":@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
        
        HomeViewController *homeObjc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
        NSArray *controllers = [NSArray arrayWithObject:homeObjc];
        navigationController1.viewControllers = controllers;
    }
}

#pragma mark - viewAll WS
-(void)callViewAllProductWS:(NSMutableDictionary *)dictdata
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:dictdata url:VIEWALL_WS string:@"ViewAllWs"];
    }
}

-(void)receivedResponseForViewAllProducts:(id)receiveData stringResponse:(NSString *)responseType
{
    [FVCustomAlertView hideAlertFromView:self.view fading:NO];
    if ([responseType isEqualToString:@"success"])
    {
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        
        NSString *status = [NSString stringWithFormat:@"%ld",(long)[[jsonObject valueForKey:@"status"]integerValue]];
        if ([status isEqualToString:@"1"])
        {
            NSMutableArray *dataArr=[jsonObject valueForKey:@"data"];
            totalCount = (int)[dataArr valueForKey:@"count"];
            
            if (page_no_val == 1)
            {
                productListArr =[NSMutableArray arrayWithArray:[dataArr valueForKey:@"product_collection"]];
            }
            else
            {
                [productListArr addObjectsFromArray:[dataArr valueForKey:@"product_collection"]];
            }
            if (productListArr.count==0)
            {
                UILabel *lblNoProducts=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 300, 40)];
                lblNoProducts.text=@"No Product Available for this category.";
                lblNoProducts.textAlignment=NSTextAlignmentCenter;
                lblNoProducts.center=self.view.center;
                [self.view addSubview:lblNoProducts];
            }
            
            [_tableView reloadData];
            page_no_val = page_no_val + 1;
        }
    }
}

-(void)loadMoreProducts:(int)pageNo
{
    NSMutableDictionary *dictData = [NSMutableDictionary new];
    [dictData setObject:type forKey:@"type"];
    [dictData setObject:[NSString stringWithFormat:@"%d",page_no_val] forKey:@"page_no"];
    [dictData setObject:page_size forKey:@"page_size"];
    
    [self callViewAllProductWS:dictData];
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:@"ADDTOWISHLIST"])
    {
        //NSLog(@"%@",response);
        [favouriteButton setImage:[UIImage imageNamed:@"favorate_icon_selected"] forState:UIControlStateNormal];
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
    }
}

@end
