//
//  ProductListViewController.h
//  EB
//
//  Created by webwerks on 8/19/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightMenuViewController.h"
#import "AppDelegate.h"
#import "Webservice.h"
#import "ProductListCell.h"
#import "ViewAllProductCell.h"
#import "FVCustomAlertView.h"


@interface ProductListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,FinishLoadingData>

@property (strong, nonatomic) IBOutlet UILabel *viewAllListTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(strong, nonatomic)NSString *type;
@property(strong, nonatomic)NSString *page_no;
@property(strong, nonatomic)NSString *page_size;

- (IBAction)RightSlideAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;

@end
