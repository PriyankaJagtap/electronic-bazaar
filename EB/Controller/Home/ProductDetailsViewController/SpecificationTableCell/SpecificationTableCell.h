//
//  SpecificationTableCell.h
//  EB
//
//  Created by Neosoft on 4/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecificationTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *specificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *specificationTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewMoreBtn;

@end
