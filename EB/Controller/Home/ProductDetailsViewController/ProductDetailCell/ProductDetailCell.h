//
//  ProductDetailCell.h
//  EB
//
//  Created by webwerks on 8/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "ASStarRatingView.h"

@interface ProductDetailCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>
{
    
}
@property (weak, nonatomic) IBOutlet UIImageView *selectedProductImage;
@property (weak, nonatomic) IBOutlet UIImageView *outOfStockProductImage;

@property (weak, nonatomic) IBOutlet UILabel *currentPrice;
@property (weak, nonatomic) IBOutlet UnderLineLabel *previousPrice;
@property (weak, nonatomic) IBOutlet UICollectionView *productListCollectionView;
@property (strong, nonatomic) IBOutlet UIButton *btnFav;
@property (strong, nonatomic) IBOutlet UIButton *btnShare;
@property (strong, nonatomic) IBOutlet UITextField *pinCode_Txt;
@property (strong, nonatomic) IBOutlet UIButton *pinCode_Btn;
@property (strong, nonatomic) IBOutlet UIButton *callNow_Btn;
@property (weak, nonatomic) IBOutlet UILabel *warranty_Lbl;
@property (weak, nonatomic) IBOutlet UILabel *pinCode_Lbl;
@property (weak, nonatomic) IBOutlet UILabel *quantitySoldLabel;

@property (weak, nonatomic) IBOutlet UILabel *itemLeftLabel;
@property (weak, nonatomic) IBOutlet ASStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UIImageView *homeDeliveryImageView;
@property (weak, nonatomic) IBOutlet UILabel *freeHomeDeliveryLabel;
@property (weak, nonatomic) IBOutlet UIPageControl *objPageControl;
@property (weak, nonatomic) IBOutlet UILabel *codLabel;
@property (weak, nonatomic) IBOutlet UIView *freeHomeDeliveryView;
@property (weak, nonatomic) IBOutlet UIImageView *codImgView;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *codHt;
@property (weak, nonatomic) IBOutlet UIView *codView;
@property (weak, nonatomic) IBOutlet UILabel *verifyOnDeliveryLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *codWidth;
@property (weak, nonatomic) IBOutlet UILabel *emiOptionLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emiHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verifyOnDeliveryHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *wrongPriceLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *codHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *homeDeliveryHeightConstraint;

//@property(strong,nonatomic)NSArray *productImgArray;
@property(strong,nonatomic)NSArray *urlProductImageArray;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property(nonatomic)BOOL isInStock;
@property (weak, nonatomic) IBOutlet UILabel *warrantyTitleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *warrantyTitleLblTopConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *categoryTypeImgView;
@property (weak, nonatomic) IBOutlet UIButton *loginToViewPriceBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginToViewPriceBtnHtConstraint;
@property (weak, nonatomic) IBOutlet UILabel *remarkTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remarkTitleLabelTopConstraint;
@property (weak, nonatomic) IBOutlet UIButton *vowDelightBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vowDelightBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *discountImageView;

-(void)configureLoginToViewPriceBtn;
@end
