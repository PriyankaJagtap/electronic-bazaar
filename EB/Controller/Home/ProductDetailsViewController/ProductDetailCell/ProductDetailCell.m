//
//  ProductDetailCell.m
//  EB
//
//  Created by webwerks on 8/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "ProductDetailCell.h"
#import "ProductListCollectionCell.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
@implementation ProductDetailCell
@synthesize productListCollectionView,warranty_Lbl;
@synthesize urlProductImageArray,selectedProductImage,btnShare,starRatingView,callNow_Btn;
- (void)awakeFromNib {
        // Initialization code
    
//    UICollectionViewFlowLayout *flowLayout =[[UICollectionViewFlowLayout alloc] init];
//    if (isIpad)
//        [flowLayout setItemSize:CGSizeMake(222, 220)];
//    else
//        [flowLayout setItemSize:CGSizeMake(170,150)];
//
//    
//    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
//    [productListCollectionView setCollectionViewLayout:flowLayout];
//    starRatingView.canEdit=NO;
//    starRatingView.backgroundColor=[UIColor clearColor];

    [super awakeFromNib];
   // [[CommonSettings sharedInstance] setBorderToView:_bgView];
        [[CommonSettings sharedInstance] createWrapView:self.pinCode_Txt];
        

}

-(void)configureLoginToViewPriceBtn
{
//        if([CommonSettings isLoggedInUser])
//        {
//                self.loginToViewPriceBtnHtConstraint.constant = 0;
//        }
//        else
//        {
//                self.loginToViewPriceBtnHtConstraint.constant = 24;
//                
//        }

}

#pragma mark - collectionView delegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{

    for (UICollectionViewCell *cell in [self.productListCollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.productListCollectionView indexPathForCell:cell];
        //NSLog(@"visible index %ld",(long)indexPath.item);
        self.objPageControl.currentPage = indexPath.item;
        
    }
}
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    return urlProductImageArray.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    return CGSizeMake(kSCREEN_WIDTH- 20, 180);
        return CGSizeMake(kSCREEN_WIDTH, 180);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0,0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *identifier = @"Cell";
    ProductListCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (_isInStock){
        cell.ouOfStockProductImageView.hidden=YES;
    }
    else{
        cell.ouOfStockProductImageView.hidden=NO;
        
    }
    [cell.productImage sd_setImageWithURL:[NSURL URLWithString:[urlProductImageArray objectAtIndex:indexPath.item]]];


    return cell;
    
}


@end
