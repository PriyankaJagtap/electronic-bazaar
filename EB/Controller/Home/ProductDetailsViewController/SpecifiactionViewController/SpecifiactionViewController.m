//
//  SpecifiactionViewController.m
//  EB
//
//  Created by webwerks on 8/17/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "SpecifiactionViewController.h"
#import "SpecificationCell.h"
#import "AppDelegate.h"
#import "SpecificationProductCell.h"

@interface SpecifiactionViewController ()

@end

@implementation SpecifiactionViewController
@synthesize specificationDetails,productTitle,productTitleLbl;
@synthesize productPrice,product_price_Lbl,specificationTableview;

- (void)viewDidLoad {
        [super viewDidLoad];
        [super setViewControllerTitle:@"Specifications"];
        productTitleLbl.text=productTitle;
        //    product_price_Lbl.text=[NSString stringWithFormat:@"Rs.%d",productPrice];
        
        product_price_Lbl.text= [[CommonSettings sharedInstance] formatIntegerPrice:(NSInteger) productPrice];
        
        
        specificationTableview.estimatedRowHeight = 40;
        specificationTableview.rowHeight = UITableViewAutomaticDimension;
        specificationTableview.estimatedSectionHeaderHeight = 40;
        specificationTableview.sectionHeaderHeight = UITableViewAutomaticDimension;
}

- (void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        [[[AppDelegate getAppDelegateObj] tabBarObj] hideTabbar];
         self.specificationTableview.layer.borderWidth = 1;
        self.specificationTableview.layer.borderColor = [[UIColor darkGrayColor] CGColor];           
}
- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

#pragma mark - Table datasource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return [specificationDetails count];;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [[[specificationDetails objectAtIndex:section]valueForKey:@"specs"] count];
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        
        static NSString *simpleTableIdentifier = @"Cell";
        SpecificationCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
                cell = [[SpecificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        cell.productName.text=[[specificationDetails objectAtIndex:section]valueForKey:@"title"];
        return cell.contentView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        
        static NSString *cellIdentifier=@"SpecificationProductCell";
        SpecificationProductCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        NSArray *specificationDetailsArr = [[specificationDetails objectAtIndex:indexPath.section]valueForKey:@"specs"];
        cell.labelTitle.text=[[specificationDetailsArr objectAtIndex:indexPath.row] objectForKey:@"key"];
        cell.labelDescription.text=[[specificationDetailsArr objectAtIndex:indexPath.row] objectForKey:@"value"];
        
        
        return cell;
}

#pragma mark - IBAction methods
- (IBAction)backButtonAction:(id)sender
{
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightSlideMenuAction:(id)sender {
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}


@end
