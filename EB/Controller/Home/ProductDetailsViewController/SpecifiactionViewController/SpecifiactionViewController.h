//
//  SpecifiactionViewController.h
//  EB
//
//  Created by webwerks on 8/17/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecifiactionViewController : BaseNavigationControllerWithBackBtn<UITableViewDataSource,UITableViewDelegate>
{
    
}
@property (weak, nonatomic) IBOutlet UITableView *specificationTableview;
@property(strong,nonatomic)NSArray *specificationDetails;
@property(strong,nonatomic)NSString *productTitle;
@property(nonatomic)int productPrice;
@property (strong, nonatomic) IBOutlet UILabel *product_price_Lbl;

@property (strong, nonatomic) IBOutlet UILabel *productTitleLbl;

- (IBAction)backButtonAction:(id)sender;
- (IBAction)rightSlideMenuAction:(id)sender;

@end
