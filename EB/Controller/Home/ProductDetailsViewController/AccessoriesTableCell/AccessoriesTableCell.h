//
//  AccessoriesTableCell.h
//  EB
//
//  Created by Neosoft on 4/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AccessoryViewDelegate

- (void) CollectionViewDidselctProduct:(int)productId;
- (void) CollectionViewDidSelectAddToCart:(int)productId;
- (void) addProductToWishList:(int)productId withCollectionViewTag:(int)collectionViewTag andFavBtnTag:(int)favBtnTag;



@end

@interface AccessoriesTableCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *objCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *objCollectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (strong, nonatomic) NSMutableArray *accessoryDataArr;
@property (nonatomic, retain) id <AccessoryViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *backwardForwardArrowLabel;

@end
