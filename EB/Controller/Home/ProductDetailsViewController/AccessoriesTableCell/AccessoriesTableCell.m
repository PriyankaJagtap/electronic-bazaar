//
//  AccessoriesTableCell.m
//  EB
//
//  Created by Neosoft on 4/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "AccessoriesTableCell.h"
#import "AccessoriesCollectionCell.h"
#define WIDTH_HEIGHT_RATIO 0.714

@implementation AccessoriesTableCell

- (void)awakeFromNib
{
        [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
        [super setSelected:selected animated:animated];
}

#pragma mark - collectionView delegate methods
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
        return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
        return _accessoryDataArr.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
        CGFloat cellWidth = (kSCREEN_WIDTH-15)/2;
        CGFloat cellHeight = 130 + (cellWidth * 0.53);
        return CGSizeMake(cellWidth,cellHeight );
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *identifier = @"AccessoriesCollectionCell";
        AccessoriesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        [cell.productImage sd_setImageWithURL:[NSURL URLWithString:[[_accessoryDataArr objectAtIndex:indexPath.item]valueForKey:@"image"]]];
        
        NSLog(@"image width %f",cell.productImage.frame.size.width);
        cell.productLabel.text = [[_accessoryDataArr objectAtIndex:indexPath.item]valueForKey:@"name"];
        
        NSString *is_InStock = [NSString stringWithFormat:@"%@",[[_accessoryDataArr objectAtIndex:indexPath.item]valueForKey:@"is_instock"]];
        if ([is_InStock isEqualToString:@"0"])
        {
                cell.outOfProductImage.hidden = NO;
                cell.addToCartBtn.accessibilityLabel = @"0";
        }
        else
        {
                cell.outOfProductImage.hidden = YES;
                cell.addToCartBtn.accessibilityLabel = @"1";
        }
        
        NSString *actualPrice= [[CommonSettings sharedInstance] formatPrice:[[[_accessoryDataArr objectAtIndex:indexPath.item]valueForKey:@"price"] floatValue]];
        if ([NSNull null]==[[_accessoryDataArr objectAtIndex:indexPath.item]objectForKey:@"special_price"])
        {
                cell.productPriceLabel.text=[NSString stringWithFormat:@"%@",actualPrice];
                cell.previousPriceLabel.text=@"";
                cell.wrongLabel.hidden = YES;
        }
        else
        {
                NSString *specialPriceStr = [[CommonSettings sharedInstance] formatPrice:[[[_accessoryDataArr objectAtIndex:indexPath.item]objectForKey:@"special_price"] floatValue]];
                cell.productPriceLabel.text=[NSString stringWithFormat:@"%@",specialPriceStr];
                cell.previousPriceLabel.text=[NSString stringWithFormat:@"%@",actualPrice];
                cell.wrongLabel.hidden = NO;
        }
        
        NSDictionary *dic = [_accessoryDataArr objectAtIndex:indexPath.row];
        if ([[dic valueForKey:@"is_favourite"] boolValue])
        {
                [cell.favProdBtn setSelected:YES];
        }
        else
        {
                [cell.favProdBtn setSelected:NO];
        }
        
        if([dic valueForKey:@"category_type_img"] && ![[dic valueForKey:@"category_type_img"] isEqualToString:@""])
        {
                [cell.categoryTypeImgView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"category_type_img"]]];
        }
        else
        {
                cell.categoryTypeImgView.image = nil;
        }
        cell.favProdBtn.tag = indexPath.item;
        [cell.favProdBtn addTarget:self action:@selector(favBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.addToCartBtn.tag = indexPath.item;
        [cell.addToCartBtn addTarget:self action:@selector(cartButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        if(![[dic valueForKey:@"new_launch"] isKindOfClass:[NSNull class]])
        {
                if([[dic valueForKey:@"new_launch"] intValue] == 1)
                {
                        cell.comingSoonView.hidden = NO;
                        cell.addToCartBtn.hidden = YES;
                }
                else
                {
                        cell.comingSoonView.hidden = YES;
                        cell.addToCartBtn.hidden = NO;
                }
        }
        else
        {
                cell.comingSoonView.hidden = YES;
                cell.addToCartBtn.hidden = NO;
        }
        return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        int productId = [[[_accessoryDataArr objectAtIndex:indexPath.row]valueForKey:@"product_id"]intValue];;
        [self.delegate CollectionViewDidselctProduct:productId];
}

#pragma mark - IBAction methods
-(void)cartButtonPressed:(UIButton*)btn
{
        if ([btn.accessibilityLabel isEqualToString:@"0"])
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"This product is out of stock."];
        }
        else
        {
                int productId=[[[_accessoryDataArr objectAtIndex:btn.tag]valueForKey:@"product_id"]intValue];
                [self.delegate CollectionViewDidSelectAddToCart:productId];
        }
}

-(void)favBtnClicked:(UIButton*)btn
{
        int productId=[[[_accessoryDataArr objectAtIndex:btn.tag]valueForKey:@"product_id"]intValue];
        [self.delegate addProductToWishList:productId withCollectionViewTag:(int)self.objCollectionView.tag andFavBtnTag:(int)btn.tag];
}

@end
