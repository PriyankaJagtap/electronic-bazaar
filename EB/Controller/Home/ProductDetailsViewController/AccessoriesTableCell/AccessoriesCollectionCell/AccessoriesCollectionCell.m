//
//  AccessoriesCollectionCell.m
//  EB
//
//  Created by Neosoft on 4/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "AccessoriesCollectionCell.h"

@implementation AccessoriesCollectionCell

- (void)awakeFromNib {
        [super awakeFromNib];
        // Initialization code
        self.contentView.layer.borderWidth= 0.5;
        self.contentView.layer.borderColor =[UIColor lightGrayColor].CGColor;
        [self.contentView setClipsToBounds:YES];
//        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.addToCartBtn];
//        if([CommonSettings isLoggedInUser])
//        {
//                self.loginToViewPriceBtnHtConstraint.constant = 0;
//                self.addToCartBtnHtConstraint.constant = 24;
//        }
//        else
//        {
//                self.loginToViewPriceBtnHtConstraint.constant = 24;
//                self.addToCartBtnHtConstraint.constant = 0;
//
//        }
        
}

@end
