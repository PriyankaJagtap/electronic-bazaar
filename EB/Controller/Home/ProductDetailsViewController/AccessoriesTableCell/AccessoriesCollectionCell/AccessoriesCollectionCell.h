//
//  AccessoriesCollectionCell.h
//  EB
//
//  Created by Neosoft on 4/18/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccessoriesCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *outOfProductImage;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet UIButton *addToCartBtn;
@property (weak, nonatomic) IBOutlet UnderLineLabel *previousPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *wrongLabel;
@property (weak, nonatomic) IBOutlet UIButton *favProdBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginToViewPriceBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginToViewPriceBtnHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addToCartBtnHtConstraint;


@property (weak, nonatomic) IBOutlet UIImageView *categoryTypeImgView;
@property (weak, nonatomic) IBOutlet UIView *comingSoonView;


@end
