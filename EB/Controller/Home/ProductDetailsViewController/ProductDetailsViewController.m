//
//  ProductDetailsViewController.m
//  EB
//
//  Created by webwerks on 8/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#define MAX_PINCODE_LENGTH 6

#import "ProductDetailsViewController.h"
#import "Webservice.h"
#import "Constant.h"
#import "CommonSettings.h"
#import "ProductDetailCell.h"
#import "ProductTableCell.h"
#import "AppDelegate.h"
#import "FVCustomAlertView.h"
#import "SpecifiactionViewController.h"
#import "ReviewViewController.h"
#import "StarRatingView.h"
#import "MyCartViewController.h"
#import "WriteReview.h"
#import "UIImageView+WebCache.h"
#import "AccessoriesTableCell.h"
#import "SpecificationTableCell.h"
#import "AvailblePaymentOptionTableCell.h"
#import "GSTViewController.h"
#import "ProductDetailServiceCenterTableCell.h"
#import "WarrantyViewController.h"
#import "MyWishlist.h"
#import "MyCartViewController.h"
#import "VowDelightLandingPageViewController.h"

@interface ProductDetailsViewController () <FinishLoadingData, AccessoryViewDelegate>
{
    UIButton *btnFavourite;
    UILabel *pincode_Status_Lbl;
    BOOL isProduct_InStock;
    BOOL isServiceCenterAvailable;
    int selectedFavBtnTag;
    int selectedCollectionViewTag;
    bool addToWishListBtnClciked;
}

@property (weak, nonatomic) IBOutlet UITableView *productTableView;

@end

@implementation ProductDetailsViewController
@synthesize productTableView,productID;
#pragma mark - ViewController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //    sectionArr = [NSMutableArray arrayWithObjects:@"",@"   YOU MAY ALSO BE INTERESTED IN",@"   ACCESORIES RECOMMENDED",@"   RECENTLY VIEWED",@"", nil];
    
    productTableView.rowHeight = UITableViewAutomaticDimension;
    productTableView.estimatedRowHeight = 440;
    
    [self callProductDetailWsWithProductID:productID];
    
    // [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:5];
    [super setViewControllerTitle:@"Product Details"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:4];
    [[[AppDelegate getAppDelegateObj] tabBarObj] hideTabbar];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [CommonSettings sendScreenName:@"ProductDetailView"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[AppDelegate getAppDelegateObj] tabBarObj] hideTabbar];
        [productTableView setBackgroundColor:[UIColor whiteColor]];
    });
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[[AppDelegate getAppDelegateObj] tabBarObj] unhideTabbar];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return productDetailResponse == nil ? 0 : sectionArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
            return [self getProductDetailCell:indexPath tableView:tableView];
            break;
        case 1:
            return [self getSpecificationTableCell:indexPath tableView:tableView];
            break;
        case 2:
            return [self getAccessoriesTableCell:indexPath tableView:tableView];
            break;
        default:
            return nil;
            break;
    }
}

#pragma mark - Get Cell Methods
- (ProductDetailCell * _Nonnull)getProductDetailCell:(NSIndexPath * _Nonnull)indexPath tableView:(UITableView * _Nonnull)tableView
{
    static NSString *cellIdentifier=@"ProductDetailCell";
    ProductDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    int isInStock = [[[productDetailResponse valueForKey:@"data"]valueForKey:@"is_instock"]intValue];
    
    if (isInStock)
    {
        isProduct_InStock=YES;
        NSInteger qty = [[[productDetailResponse valueForKey:@"data"] valueForKey:@"qty"] integerValue];
        NSInteger max_qty = [[[productDetailResponse valueForKey:@"data"] valueForKey:@"max_qty"] integerValue];
        if (qty > max_qty)
        {
            qty = max_qty;
        }
        if( qty <= 10 && qty != 0)
        {
            cell.itemLeftLabel.text = [NSString stringWithFormat:@"Hurry, Only %ld left!",qty];
        }
        else
        {
            cell.itemLeftLabel.text = @"";
        }
    }
    else
    {
        isProduct_InStock=NO;
        cell.itemLeftLabel.text = @"";
    }
    cell.isInStock = isInStock;
    cell.urlProductImageArray=[[productDetailResponse valueForKey:@"data"]valueForKey:@"product_images"];
    cell.objPageControl.numberOfPages =  cell.urlProductImageArray.count;
    
    [cell configureLoginToViewPriceBtn];
    
    if([[productDetailResponse valueForKey:@"data"] valueForKey:@"category_type_img"])
    {
        if(![[[productDetailResponse valueForKey:@"data"] valueForKey:@"category_type_img"] isKindOfClass:[NSNull class]] || ![[[productDetailResponse valueForKey:@"data"] valueForKey:@"category_type_img"] isEqualToString:@""] || [[productDetailResponse valueForKey:@"data"] valueForKey:@"category_type_img"] != nil )
        {
            [cell.categoryTypeImgView sd_setImageWithURL:[NSURL URLWithString:[[productDetailResponse valueForKey:@"data"] valueForKey:@"category_type_img"]]];
        }
        else
        {
            cell.categoryTypeImgView.image = nil;
        }
    }
    [cell.productListCollectionView reloadData];
    cell.productName.text=[[productDetailResponse valueForKey:@"data"]valueForKey:@"name"];
    cell.pinCode_Txt.delegate=self;
    
    if ([[productDetailResponse valueForKey:@"data"] valueForKey:@"discount_image"]  && ![[[productDetailResponse valueForKey:@"data"] valueForKey:@"discount_image"] isKindOfClass:[NSNull class]])
    {
        [cell.discountImageView sd_setImageWithURL:[NSURL URLWithString:[[productDetailResponse valueForKey:@"data"] valueForKey:@"discount_image"]]];
    }
    else
    {
        cell.discountImageView.image = nil;
    }
    
    if ([[productDetailResponse valueForKey:@"data"] valueForKey:@"brand"] && [[[productDetailResponse valueForKey:@"data"] valueForKey:@"brand"] isEqualToString:@"Vow"])
    {
        cell.vowDelightBtnHeightConstraint.constant = 30;
    }
    if (pincode_Status_Lbl == nil)
    {
        pincode_Status_Lbl=cell.pinCode_Lbl;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        cell.pinCode_Txt.text = [[defaults valueForKey:@"user"] valueForKey:@"pincode"];
    }
    if ([NSNull null]==[[productDetailResponse valueForKey:@"data"]valueForKey:@"warrenty"])
    {
        cell.warranty_Lbl.text=@"";
        cell.warrantyTitleLabel.text = @"";
        cell.warrantyTitleLblTopConstraint.constant = 0;
    }
    else
    {
        cell.warrantyTitleLabel.text = @"Warranty : ";
        cell.warranty_Lbl.text=[[productDetailResponse valueForKey:@"data"]valueForKey:@"warrenty"];
        cell.warrantyTitleLblTopConstraint.constant = 8;
    }
    
    [cell.pinCode_Txt addTarget:self action:@selector(pincodeValueChanged:) forControlEvents:UIControlEventEditingChanged];
    [cell.pinCode_Btn addTarget:self action:@selector(callPinCodeCheckWs) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnShare addTarget:self action:@selector(shareProduct) forControlEvents:UIControlEventTouchUpInside];
    [cell.callNow_Btn addTarget:self action:@selector(callNow) forControlEvents:UIControlEventTouchUpInside];
    [cell.objPageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventTouchUpInside];
    
    //Price
    NSString *actualPrice= [[CommonSettings sharedInstance] formatPrice:[[[productDetailResponse valueForKey:@"data"]valueForKey:@"price"] floatValue]];
    
    if ([NSNull null]==[[productDetailResponse valueForKey:@"data"]objectForKey:@"special_price"])
    {
        cell.currentPrice.text=[NSString stringWithFormat:@"%@",actualPrice];
        cell.previousPrice.text=@"";
        cell.wrongPriceLabel.hidden = YES;
    }
    else
    {
        NSString *specialPriceStr = [[CommonSettings sharedInstance] formatPrice:[[[productDetailResponse valueForKey:@"data"]objectForKey:@"special_price"] floatValue]];
        cell.currentPrice.text=[NSString stringWithFormat:@"%@",specialPriceStr];
        cell.previousPrice.text=[NSString stringWithFormat:@"%@",actualPrice];
        cell.wrongPriceLabel.hidden = NO;
    }
    
    if ([[productDetailResponse valueForKey:@"data"] valueForKey:@"remarks"] && ![[[productDetailResponse valueForKey:@"data"]  valueForKey:@"remarks"] isEqualToString:@""] && ![[[productDetailResponse valueForKey:@"data"] valueForKey:@"remarks"] isKindOfClass:[NSNull class]])
    {
        cell.remarkTitleLabel.text = @"Remarks:";
        cell.remarkLabel.text = [[productDetailResponse valueForKey:@"data"] valueForKey:@"remarks"];
    }
    else
    {
        cell.remarkTitleLabel.text = @"";
        cell.remarkLabel.text = @"";
        cell.remarkTitleLabelTopConstraint.constant = 0;
    }

    //productDetailResponse
    if(![[[productDetailResponse valueForKey:@"data"] valueForKey:@"emi"] boolValue])
    {
        cell.emiHeightConstraint.constant = 0;
        cell.emiOptionLabel.hidden = YES;
    }
    else
    {
        cell.emiHeightConstraint.constant = 50;
        cell.emiOptionLabel.hidden = NO;
    }
    
    if (![[[productDetailResponse valueForKey:@"data"] valueForKey:@"cod"] boolValue])
    {
        cell.codHeightConstraint.constant = 0;
        cell.codLabel.hidden = YES;
    }
    else
    {
        cell.codHeightConstraint.constant = 50;
        cell.codLabel.hidden = NO;
    }
    if (![[[productDetailResponse valueForKey:@"data"] valueForKey:@"home_delivery"] boolValue])
    {
        cell.homeDeliveryHeightConstraint.constant = 0;
        cell.freeHomeDeliveryLabel.hidden = YES;
    }
    else
    {
        cell.homeDeliveryHeightConstraint.constant = 50;
        cell.freeHomeDeliveryLabel.hidden = NO;
    }
    if (![[[productDetailResponse valueForKey:@"data"] valueForKey:@"verify_ondelivery"] boolValue])
    {
        cell.verifyOnDeliveryHeightConstraint.constant = 0;
        cell.verifyOnDeliveryLabel.hidden = YES;
    }
    else
    {
        cell.verifyOnDeliveryHeightConstraint.constant = 50;
        cell.verifyOnDeliveryLabel.hidden = NO;
    }
    
    int qtySold = [[[productDetailResponse valueForKey:@"data"]valueForKey:@"qty_sold"]intValue];
    if (qtySold > 200)
    {
        NSString *strQtySold = [NSString stringWithFormat:@"%d",qtySold];
        NSString *text = [NSString stringWithFormat:@"%d unit(s) sold", qtySold];
        UIFont *normalFont = [UIFont fontWithName:@"Karla-Regular" size:16.0];
        
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:normalFont forKey:NSFontAttributeName];
        NSMutableAttributedString *qtyText = [[NSMutableAttributedString alloc] initWithString:text attributes:attrsDictionary];
        [qtyText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Karla-Bold" size:16.0] range:NSMakeRange(0, strQtySold.length)];
        
        cell.quantitySoldLabel.attributedText = qtyText;
    }
    else
    {
        cell.quantitySoldLabel.text = @"";
    }
    
    [cell.contentView layoutIfNeeded];
    return cell;
}

- (SpecificationTableCell * _Nonnull)getSpecificationTableCell:(NSIndexPath * _Nonnull)indexPath tableView:(UITableView * _Nonnull)tableView
{
    static NSString *cellIdentifier=@"SpecificationTableCell";
    SpecificationTableCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *str = @"";
    
    if ([[productDetailResponse valueForKey:@"specifications"] count] > 0)
    {
        NSArray *specArr = [[[productDetailResponse valueForKey:@"specifications"] objectAtIndex:0] valueForKey:@"specs"];
        NSUInteger cnt = 4;
        if(specArr.count < 4)
            cnt = specArr.count;
        
        for (int i = 0; i < cnt; i++)
        {
            NSDictionary *dic = [specArr objectAtIndex:i];
            NSString *specStr;
            if(i == cnt-1)
                specStr = [NSString stringWithFormat:@"\u2022 %@ - %@",[dic objectForKey:@"key"],[dic objectForKey:@"value"]];
            else
                
                specStr = [NSString stringWithFormat:@"\u2022 %@ - %@\n",[dic objectForKey:@"key"],[dic objectForKey:@"value"]];
            
            str = [str stringByAppendingString:specStr];
        }
        
        cell.specificationLabel.text = str;
    }
    [cell.viewMoreBtn addTarget:self action:@selector(viewMoreBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (AccessoriesTableCell * _Nonnull)getAccessoriesTableCell:(NSIndexPath * _Nonnull)indexPath tableView:(UITableView * _Nonnull)tableView
{
    static NSString *cellIdentifier=@"AccessoriesTableCell";
    AccessoriesTableCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.accessoryDataArr = [[productDetailResponse valueForKey:@"related_products"] mutableCopy];
    if (cell.accessoryDataArr.count > 2)
    {
        cell.backwardForwardArrowLabel.hidden = NO;
    }
    else
    {
        cell.backwardForwardArrowLabel.hidden = YES;
    }
    cell.delegate = self;
    cell.titleLabel.text = @"Related Products";
    cell.objCollectionView.tag = indexPath.row;
    [cell.objCollectionView reloadData];
    
    CGFloat cellWidth = (kSCREEN_WIDTH-15)/2;
    CGFloat cellHeight = 140 + (cellWidth * 0.53);
    [self.view layoutIfNeeded];
    //                cell.objCollectionViewHeightConstraint.constant = cell.objCollectionView.collectionViewLayout.collectionViewContentSize.height;
    cell.objCollectionViewHeightConstraint.constant = cellHeight;
    [self.view layoutIfNeeded];
    return cell;
}

#pragma mark - Other Methods
-(void)serviceBtnClicked
{
    WarrantyViewController *objVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
    objVc.webViewUrl = [[productDetailResponse objectForKey:@"data"] valueForKey:@"service_url"];
    objVc.titleLabel = @"";
    objVc.bannerType = @"webview";
    [self.navigationController pushViewController:objVc animated:YES];
}

-(NSMutableAttributedString *) getAttributedText:(NSString *)str
{
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                            NSFontAttributeName:[UIFont fontWithName:@"Karla-Regular" size:15.0],
                            NSForegroundColorAttributeName:[[UIColor alloc] initWithRed:50.0f/255 green:146.0f/255  blue:207.0f/255 alpha:1],
                            NSParagraphStyleAttributeName:style}; // Added line
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:str attributes:dict1]];
    return attString;
}

#pragma mark - UIAlertView Methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 200)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self.navigationController pushViewController:[[UIStoryboard storyboardWithName:@"Product" bundle:nil] instantiateViewControllerWithIdentifier:@"GSTViewController"] animated:YES];
            }
                break;
            case 1:
            {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:REMIND_ME_LATER_GSTIN];
                [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:REMIND_ME_LATER_CLICK_DATE];
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:SHOW_GSTIN];
                [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
            }
                break;
                
            case 2:
            {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:SHOW_GSTIN];
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:REMIND_ME_LATER_GSTIN];
                [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
            }
                break;
                
            default:
                break;
        }
    }
    else if(alertView.tag == 500)
    {
        // [APP_DELEGATE navigateToLoginViewController];
    }
    else
    {
        if(buttonIndex==0)
        {
            //        WriteReview *objWriteReview = [[UIStoryboard storyboardWithName:isIpad?@"Main_iPad":@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"WriteReview"];
            WriteReview *objWriteReview = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"WriteReview"];
            objWriteReview.prodName=[[productDetailResponse valueForKey:@"data"]valueForKey:@"name"];
            objWriteReview.prodID=[[[productDetailResponse valueForKey:@"data"]valueForKey:@"product_id"]intValue];
            [self.navigationController pushViewController:objWriteReview animated:YES];
        }
    }
}

#pragma mark - CustomCollectionViewDelegate
- (void) CollectionViewDidselctProduct:(int)productId
{
    [self callProductDetailWsWithProductID:productId];
}

- (void) CollectionViewDidSelectAddToCart:(int)productId;
{
    if ([CommonSettings isLoggedInUser])
    {
        Webservice  *callAddToCartService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if (!chkInternet)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
            
            callAddToCartService.delegate =self;
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
            NSString *quotId;
            if (quoteID==0)
            {
                quotId = @"";
            }
            else
            {
                quotId = [NSString stringWithFormat:@"%d",quoteID];
            }
            
            NSDictionary *dictAddToCart=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:productId],@"ProductID",quotId,@"QuoteID", @"",WISHLIST_ITEM_ID,nil];
            
            [callAddToCartService operationRequestToApi:dictAddToCart url:ADD_TO_CART_WS string:@"ADDTOCART"];
        }
    }
    else
    {
        [[CommonSettings sharedInstance] showAlertTitle:@"" message:CART_LOGIN_REQUIRED_MSG];
    }
}

- (void) addProductToWishList:(int)productId withCollectionViewTag:(int)collectionViewTag andFavBtnTag:(int)favBtnTag
{
    selectedCollectionViewTag = collectionViewTag;
    selectedFavBtnTag = favBtnTag;
    
    if ([CommonSettings isLoggedInUser])
    {
        NSMutableDictionary *productDic = [[productDetailResponse valueForKey:@"related_products"] objectAtIndex:favBtnTag];
        
        if (![[productDic valueForKey:@"is_favourite"] boolValue])
        {
            [self didselectFavouriteProduct:productId];
            //                        [productDic setValue:[NSNumber numberWithBool:YES] forKey:@"is_favourite"];
            //                        [arrCategoryList replaceObjectAtIndex:btnFav.tag withObject:productDic];
        }
        else
        {
            [self removeProductFromFavourite:productId];
        }
    }
    else
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
    }
}

-(void)reloadParticularCollectionViewItem:(NSMutableDictionary *)productDic
{
    AccessoriesTableCell *objCell = (AccessoriesTableCell *)[productTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedCollectionViewTag inSection:0]];
    [objCell.accessoryDataArr replaceObjectAtIndex:selectedFavBtnTag withObject:productDic];
    [objCell.objCollectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedFavBtnTag inSection:0]]];
}

-(void)didselectFavouriteProduct:(int)productId
{
    NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
    Webservice  *addToWishlistService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        if (userId==0)
        {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
        }
        else
        {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            addToWishlistService.delegate =self;
            [addToWishlistService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%@&productId=%d",ADDTOWISHLIST,userId,productId] MathodName:@"ADDTOWISHLIST"];
        }
    }
}

-(void)removeProductFromFavourite:(int)productId
{
    NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
    Webservice  *addToWishlistService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        if (userId==0)
        {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
        }
        else
        {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            addToWishlistService.delegate =self;
            [addToWishlistService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%@&product_id=%d",REMOVE_FROM_WISHLIST_WS,userId,productId] MathodName:REMOVE_FROM_WISHLIST_WS];
        }
    }
}

#pragma mark - Webservice Delegate
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:@"ProductDetailWS"])
    {
        if ([[response valueForKey:@"status"]integerValue]==1)
        {
            productDetailResponse=[response mutableCopy];
            NSMutableArray *data = [productDetailResponse valueForKey:@"data"];
            [[data valueForKey:@"is_favourite"] boolValue] ? [self.addToWishlistButton setTitle:@"Remove from Wishlist" forState:UIControlStateNormal] : [self.addToWishlistButton setTitle:@"Add to Wishlist" forState:UIControlStateNormal];
            
            if (![[data valueForKey:@"new_launch"] isKindOfClass:[NSNull class]])
            {
                if([[data valueForKey:@"new_launch"] intValue] == 1)
                {
                    [self.addToCartButton setTitle: @"Coming Soon" forState: UIControlStateNormal];
                    [self.addToCartButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:222.0/255.0 blue:5.0/255.0 alpha:1.0]];
                    [self.addToCartButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    self.addToCartButton.enabled = NO;
                }
                else
                {
                    [self.addToCartButton setTitle: @"Add to Cart" forState: UIControlStateNormal];
                    self.addToCartButton.enabled = YES;
                    [self.addToCartButton setBackgroundColor:[UIColor colorWithRed:29.0/255.0 green:29.0/255.0 blue:83.0/255.0 alpha:1.0]];
                    [self.addToCartButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
            }
            else
            {
                [self.addToCartButton setTitle: @"Add to Cart" forState: UIControlStateNormal];
                self.addToCartButton.enabled = YES;
                [self.addToCartButton setBackgroundColor:[UIColor colorWithRed:29.0/255.0 green:29.0/255.0 blue:83.0/255.0 alpha:1.0]];
                [self.addToCartButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }
            sectionArr = [NSMutableArray arrayWithObjects:@"",@"", nil];
            if ([[productDetailResponse valueForKey:@"related_products"] count] != 0)
            {
                [sectionArr addObject:@"Related Products"];
            }
            
            [productTableView reloadData];
            [self callPinCodeCheckWs];
        }
        else
        {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
    }
    else if ([strName isEqualToString:@"ADDTOCART"])
    {
        if ([[response valueForKey:@"status"]intValue] == 1)
        {
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            int quoteID=[[response valueForKey:@"quoteId"]intValue];
            [defaults setObject:[NSNumber numberWithInt:quoteID] forKey:@"QuoteID"];
            [defaults synchronize];
            
            [super incrementMyCartCount:[[response valueForKey:@"qty"] intValue]];
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Product Successfully Added To Cart"];
        }
        else
        {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
    }
    else if ([strName isEqualToString:@"CHECKPINCODE"])
    {
        //NSLog(@"RESPONSE %@",response);
        if ([[response valueForKey:@"status"]integerValue]==1)
        {
            pincode_Status_Lbl.text=[response valueForKey:@"message"];
            pincode_Status_Lbl.textColor = [UIColor colorWithRed:34/255.0f green:191/255.0f blue:100/255.0f alpha:1.0];
            //[[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
        else
        {
            pincode_Status_Lbl.text=@"Delivery not available in your location";
            pincode_Status_Lbl.textColor = [UIColor redColor];
            //[[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
        [self.productTableView reloadData];
    }
    else if([strName isEqualToString:@"ADDTOWISHLIST"] || [strName isEqualToString:REMOVE_FROM_WISHLIST_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            // [favouriteButton setImage:[UIImage imageNamed:@"favorate_icon_selected"] forState:UIControlStateNormal];
            NSNumber *isfavBool;
            if([strName isEqualToString:@"ADDTOWISHLIST"])
                isfavBool = [NSNumber numberWithBool:YES];
            else if([strName isEqualToString:REMOVE_FROM_WISHLIST_WS])
                isfavBool = [NSNumber numberWithBool:NO];
            
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
            if (addToWishListBtnClciked)
            {
                addToWishListBtnClciked = NO;
                NSMutableArray *data = [productDetailResponse valueForKey:@"data"];
                [data setValue:isfavBool forKey:@"is_favourite"];
                [productDetailResponse setValue:data forKey:@"data"];
                [isfavBool boolValue] ? [self.addToWishlistButton setTitle:@"Remove from Wishlist" forState:UIControlStateNormal] : [self.addToWishlistButton setTitle:@"Add to Wishlist" forState:UIControlStateNormal];
                return;
            }
            
            NSMutableDictionary *productDic = [[[productDetailResponse valueForKey:@"related_products"] objectAtIndex:selectedFavBtnTag] mutableCopy];
            [productDic setValue:isfavBool forKey:@"is_favourite"];
            [[productDetailResponse valueForKey:@"related_products"] replaceObjectAtIndex:selectedFavBtnTag withObject:productDic];
            [self reloadParticularCollectionViewItem:[productDic mutableCopy]];
        }
    }
    else if ([strName isEqualToString:@"ADDTOCARTBUY"])
    {
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            int quoteID=[[response valueForKey:@"quoteId"]intValue];
            [defaults setObject:[NSNumber numberWithInt:quoteID] forKey:@"QuoteID"];
            [defaults synchronize];
            [super incrementMyCartCount:[[response valueForKey:@"qty"] intValue]];
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Product Successfully Added To Cart"];
            [self.addToCartButton setTitle:@"Checkout" forState:UIControlStateNormal];
            self.addToCartButton.backgroundColor = [[AppDelegate getAppDelegateObj] colorWithHexString:@"FFB200"];
        }
        else
        {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
    }
}

#pragma mark -  UITextfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [productTableView setContentOffset:CGPointMake(0, 200) animated:YES];
    CGPoint pointInTable = [textField.superview.superview convertPoint:textField.frame.origin toView:productTableView];
    CGPoint contentOffset = productTableView.contentOffset;
    
    //    if(textField.tag == 10)
    //        contentOffset.y = (pointInTable.y - (textField.inputAccessoryView.frame.size.height-40));
    //    else
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    [productTableView setContentOffset:contentOffset animated:YES];
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: productTableView];
        NSIndexPath *indexPath = [productTableView indexPathForRowAtPoint:buttonPosition];
        [productTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= MAX_PINCODE_LENGTH && range.length == 0)
        return NO; // return NO to not change text
    else
        return YES;
}

#pragma mark - Button Pressed
- (IBAction)loginToViewPriceBtnClicked:(id)sender
{
    [APP_DELEGATE navigateToLogin:PRODUCT_DETAIL];
}

- (IBAction)vowDelightBtnClicked:(UIButton *)sender
{
    VowDelightLandingPageViewController *vc = [[UIStoryboard storyboardWithName:@"Product" bundle:nil] instantiateViewControllerWithIdentifier:@"VowDelightLandingPageViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void) pincodeValueChanged:(id)sender
{
    // your code
    UITextField *pincodeTextField = (UITextField *)sender;
    if(pincodeTextField.text.length  == 6)
    {
        [self callPinCodeCheckWs];
    }
}

- (void)viewMoreBtnClicked
{
    //    SpecifiactionViewController *objSpecificationVC = [[UIStoryboard storyboardWithName:isIpad?@"Main_iPad":@"Main"  bundle:nil] instantiateViewControllerWithIdentifier:@"SpecifiactionViewController"];
    
    if([[productDetailResponse valueForKey:@"specifications"] count] > 0)
    {
        SpecifiactionViewController *objSpecificationVC = [[UIStoryboard storyboardWithName:@"Main"  bundle:nil] instantiateViewControllerWithIdentifier:@"SpecifiactionViewController"];
        
        objSpecificationVC.specificationDetails = [productDetailResponse valueForKey:@"specifications"];
        objSpecificationVC.productPrice = [[[productDetailResponse valueForKey:@"data"]valueForKey:@"price"]intValue];
        objSpecificationVC.productTitle = [[productDetailResponse valueForKey:@"data"]valueForKey:@"name"];
        
        [self.navigationController pushViewController:objSpecificationVC animated:YES];
    }
    else
        [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Specifications are not available for this product"];
}

- (void)changePage:(id)sender
{
    ProductDetailCell *cell = [productTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UIPageControl *pager=sender;
    NSUInteger page = pager.currentPage;
    
    [cell.productListCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:page inSection:0]
                                           atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                                   animated:YES];
}

- (IBAction)btnBuyProductPressed:(id)sender
{
    
}

- (IBAction)btnWriteaReviewPressed:(id)sender
{
    //    WriteReview *objWriteReview = [[UIStoryboard storyboardWithName:isIpad?@"Main_iPad":@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"WriteReview"];
    
    WriteReview *objWriteReview = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"WriteReview"];
    objWriteReview.prodName=[[productDetailResponse valueForKey:@"data"]valueForKey:@"name"];
    //NSLog(@"%@",[productDetailResponse valueForKey:@"data"]);
    objWriteReview.prodID=[[[productDetailResponse valueForKey:@"data"]valueForKey:@"product_id"]intValue];
    //objWriteReview.prodID=[]
    [self.navigationController pushViewController:objWriteReview animated:YES];
}

- (IBAction)callUsBtnClicked:(id)sender
{
    [self callNow];
}

#pragma mark - login view delegate method
-(void)refreshView
{
    [self.productTableView reloadData] ;
}

#pragma mark - Utility Methods
-(void)addProductDetailsToRecentlyViewed:(NSDictionary*)dictProduct
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSMutableArray *productFromCart=[[NSMutableArray alloc]init];
    productFromCart=[[defaults valueForKey:@"RecentlyViewedProduct"]mutableCopy];
    if (productFromCart==NULL)
    {
        productFromCart=[[NSMutableArray alloc]init];
    }
    if (![productFromCart containsObject:dictProduct])
    {
        if (productFromCart.count>3)
        {
            [productFromCart removeObjectAtIndex:productFromCart.count-1];
        }
        [productFromCart insertObject:dictProduct atIndex:0];
        [defaults setValue:productFromCart forKey:@"RecentlyViewedProduct"];
    }
    recentlyViewedArr=[[defaults valueForKey:@"RecentlyViewedProduct"]mutableCopy];
    [defaults synchronize];
}

-(void)addProductToRecentlyViewed
{
    NSString *productName =[[productDetailResponse valueForKey:@"data"]valueForKey:@"name"];
    NSString *productImageURL;
    if ([[[productDetailResponse valueForKey:@"data"]valueForKey:@"product_images"]count]>0)
    {
        productImageURL=[[[productDetailResponse valueForKey:@"data"]valueForKey:@"product_images"]objectAtIndex:0];
    }
    int productId=[[[productDetailResponse  valueForKey:@"data"]valueForKey:@"product_id"]intValue];
    int productPrice=[[[productDetailResponse  valueForKey:@"data"]valueForKey:@"price"]intValue];
    
    NSDictionary *dictProdDetail=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:productId],@"ProductId",productName,@"ProductName",productImageURL,@"ProductImage",[NSNumber numberWithInt:productPrice],@"ProductPrice", nil];
    
    //NSLog(@"%@",dictProdDetail);
    [self addProductDetailsToRecentlyViewed:dictProdDetail];
}

-(void)addPRoductToCart
{
    Webservice  *callAddToCartService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
        
        callAddToCartService.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        int productId=[[[productDetailResponse  valueForKey:@"data"]valueForKey:@"product_id"]intValue];
        NSString *quotId;
        if (quoteID==0)
        {
            quotId=@"";
        }
        else
        {
            quotId=[NSString stringWithFormat:@"%d",quoteID];
        }
        
        NSDictionary *dictAddToCart=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:productId],@"ProductID",quotId,@"QuoteID", @"",WISHLIST_ITEM_ID,nil];
        [callAddToCartService operationRequestToApi:dictAddToCart url:ADD_TO_CART_WS string:@"ADDTOCARTBUY"];
    }
    //[self addPRoductDetailsToCart:dictProdDetail];
}

- (void)checkGSTINEntered
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:REMIND_ME_LATER_GSTIN] boolValue])
    {
        NSDate *prevRemindMeLaterDate = [defaults valueForKey:REMIND_ME_LATER_CLICK_DATE];
        NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:prevRemindMeLaterDate];
        double secondsInAnHour = 3600;
        NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
        
        if(hoursBetweenDates >= 24)
        {
            [defaults setObject:[NSNumber numberWithBool:YES] forKey:SHOW_GSTIN];
            [defaults setObject:[NSNumber numberWithBool:NO] forKey:REMIND_ME_LATER_GSTIN];
            [defaults synchronize];
        }
    }
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:SHOW_GSTIN] boolValue])
    {
        UIAlertView *addCartAlert = [[UIAlertView alloc] initWithTitle:@"" message:ADD_TO_CART_POPUP_MSG delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"Remind me later",@"Ignore to ask this question", nil];
        addCartAlert.tag = 200;
        [addCartAlert show];
    }
    else
    {
        [self addPRoductToCart];
    }
}

-(void)rightBarButtonItemPressed
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

-(void)callProductDetailWsWithProductID:(int)productId
{
    Webservice  *callProductDetailService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
    
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
        callProductDetailService.delegate =self;
        NSString *userID;
        userID = userId == 0 ? @"" : [NSString stringWithFormat:@"%d",userId];
        
        [callProductDetailService GetWebServiceWithURL:[NSString stringWithFormat:@"%@user_id=%@&product_id=%d",PRODUCT_DETAIL_WS,userID,productId] MathodName:@"ProductDetailWS"];
    }
}

-(void)shareProduct
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
    
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        // Perform long running process
        NSString *productImageUrl=[[[productDetailResponse valueForKey:@"data"]valueForKey:@"product_images"]objectAtIndex:0];
        NSString *textToshare=[[productDetailResponse
                                valueForKey:@"data"]valueForKey:@"name"];
        UIImage *imageToshare=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:productImageUrl]]];
        NSArray *objectsToShare = @[textToshare,imageToshare];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        NSArray *excludeActivities = @[UIActivityTypePostToWeibo,UIActivityTypePrint,UIActivityTypeSaveToCameraRoll,UIActivityTypeAssignToContact,UIActivityTypeAirDrop];
        activityVC.excludedActivityTypes = excludeActivities;
        //activityVC.excludedActivityTypes = @[UIActivityTypePrint,
        //  UIActivityTypeCopyToPasteboard,
        // UIActivityTypeAssignToContact,
        // UIActivityTypeSaveToCameraRoll];
        
        
        if (checkIsIpad)
        {
            // Change Rect to position Popover
            UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
            [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.width/2, 100, 100) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else
        {
            //            dispatch_async(dispatch_get_main_queue(), ^{
            //                activityVC.popoverPresentationController.sourceView=self.view;
            //
            //            });
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:activityVC animated:YES completion:nil];
            });
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
        });
    });
}

-(void)callNow
{
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"])
    {
        //        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:18002664000"]]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:+918291599914"]]];
    }
    else
    {
        UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [notPermitted show];
    }
}

-(void)callPinCodeCheckWs
{
    //[self.view endEditing:YES];
    Webservice  *callProductDetailService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        NSIndexPath *indexpath=[NSIndexPath indexPathForRow:0 inSection:0];
        ProductDetailCell *cell = (ProductDetailCell*)[productTableView cellForRowAtIndexPath:indexpath];
        
        NSString *pincodeStr=cell.pinCode_Txt.text;
        if ([pincodeStr isEqualToString:@""])
        {
            //[[CommonSettings sharedInstance]showAlertTitle:@"" message:@"No Pincode Entered"];
        }
        else
        {
            //[FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@"" withBlur:NO allowTap:YES];
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            
            callProductDetailService.delegate =self;
            [callProductDetailService GetWebServiceWithURL:[NSString stringWithFormat:@"%@pincode=%@",CHECK_PINCODE,pincodeStr] MathodName:@"CHECKPINCODE"];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
//- (IBAction)leftSlideMenuPressed:(id)sender{
//    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
//}
- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightSlideMenuAction:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (IBAction)addToCartAction:(id)sender
{
    if ([[self.addToCartButton titleForState:UIControlStateNormal] isEqualToString:@"Add to Cart"])
    {
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int userId=[[[defaults valueForKey:@"user"]valueForKey:@"user_id"]intValue];
        if (userId==0)
        {
            //        [[CommonSettings sharedInstance]showAlertTitle:@"" message:LOGIN_AFFILIATE_USER_MSG];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:LOGIN_AFFILIATE_USER_MSG delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            alert.tag = 500;
            [alert show];
            return;
        }
        else
        {
            if (isProduct_InStock)
            {
                [CommonSettings addGoogleAnalyticsTrackEvent:@"shopping" label:@"add-cart-product-page"];
                //[self checkGSTINEntered];
                [self addPRoductToCart];
            }
            else
            {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"This product is out of stock"];
            }
        }
    }
    else if ([[self.addToCartButton titleForState:UIControlStateNormal] isEqualToString:@"Checkout"])
    {
        MyCartViewController  *myCart= [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyCartViewController"];
        [self.navigationController pushViewController:myCart animated:YES];
    }
}

- (IBAction)addToWishListAction:(id)sender
{
    addToWishListBtnClciked = YES;
    [[[productDetailResponse valueForKey:@"data"] valueForKey:@"is_favourite"] boolValue] ? [self removeProductFromFavourite:productID] : [self didselectFavouriteProduct:productID];
}
@end
