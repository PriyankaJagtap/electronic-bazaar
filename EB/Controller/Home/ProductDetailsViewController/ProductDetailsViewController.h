//
//  ProductDetailsViewController.h
//  EB
//
//  Created by webwerks on 8/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarRatingView.h"
#import "ProductTableCell.h"
#import "ASStarRatingView.h"

@interface ProductDetailsViewController : BaseNavigationControllerWithBackBtn<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CustomCollectionViewDelegate,UIAlertViewDelegate,LoginsViewDelegate>
{
    NSMutableArray *sectionArr;
    NSMutableDictionary *productDetailResponse;
    StarRatingView *objStarRatingView;
    NSMutableArray *recentlyViewedArr;
    
    
}

@property(nonatomic)int productID;

- (IBAction)btnWriteaReviewPressed:(id)sender;
- (IBAction)backButtonAction:(id)sender;
- (IBAction)rightSlideMenuAction:(id)sender;

- (IBAction)addToCartAction:(id)sender;
- (IBAction)addToWishListAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addToCartButton;
@property (weak, nonatomic) IBOutlet UIButton *addToWishlistButton;

@end
