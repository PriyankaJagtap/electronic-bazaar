//
//  ShippingAddress.m
//  EB
//
//  Created by webwerks on 9/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "ShippingAddress.h"
#import "Validation.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "CommonSettings.h"
#import "Checkout.h"
#define MOBILE_NO_MAX 10
#define LAST_NAME_MAX 3
#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"



@interface ShippingAddress ()<FinishLoadingData>
{
        NSInteger selectedStateAtIndex;
        NSInteger selectedCountryAtIndex;
        NSString *regionId;
        
}
@end

@implementation ShippingAddress
@synthesize objScrollViewbg,strBillingAddParameter,countryListArray;
@synthesize isAff,userEmail,mobNo;


- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        [super setViewControllerTitle:@"Shipping Address"];
        [self setUpTextFields];
        
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.proceedToCheckoutBtn];
        [pinCode_Txt addTarget:self
                        action:@selector(pincodeValueChanged:)
              forControlEvents:UIControlEventEditingChanged];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *userDic = [defaults valueForKey:@"user"];
        if([userDic valueForKey:@"pincode"])
        {
                pinCode_Txt.text = [userDic valueForKey:@"pincode"];
                [self pinCodeWSCall];
        }
        if(userDic)
        {
                _storeNameTextField.text = [userDic valueForKey:@"firstname"];
                mobile_Txt.text = [userDic valueForKey:@"mobile"];
        }
        
        if([userDic valueForKey:@"gstin"]){
                _gstNumberTextField.text = [userDic valueForKey:@"gstin"];
        }
        else
        {
                _gstNumberTextField.text  = @"";
        }
        
        
        
}

-(void)viewWillDisappear:(BOOL)animated{
        [super viewWillDisappear:YES];
        //[self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:YES];
        // [self.navigationController setNavigationBarHidden:NO];
        
        [[[AppDelegate getAppDelegateObj]tabBarObj] hideTabbar];
}
- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

-(void) pincodeValueChanged:(id)sender {
        // your code
        
        if(pinCode_Txt.text.length  == 6)
                [self pinCodeWSCall];
        
}
-(BOOL)validateForm
{
        [self.view endEditing:YES];
        //    if (![Validation required:fName_Txt withCaption:@"First Name"])
        //        return NO;
        //    if (![Validation required:lName_Txt withCaption:@"Last Name"])
        //        return NO;
        //    if(![Validation checkFirstName:fName_Txt])
        //        return NO;
        //    if(![Validation checkLastName:lName_Txt])
        //        return NO;
        //    if (![Validation required:telephone_Txt withCaption:@"Telephone"])
        //        return NO;
        
        if (![Validation required:_storeNameTextField withCaption:@"Store Name"])
                return NO;
        
        if (_gstNumberTextField.text.length != 0){
                if (_gstNumberTextField.text.length < 15) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide valid GST Number"];
                        return NO;
                }
        }
        if(![Validation checkMobileNumber:mobile_Txt])
                return NO;
        
        if (![Validation required:add_Txt withCaption:@"Address"])
                return NO;
        
        if (![Validation required:pinCode_Txt withCaption:@"Pin code"])
                return NO;
        if(pinCode_Txt.text.length != 6)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter 6 digit picode number" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                return NO;
        }
        
        if (![Validation required:country_Txt withCaption:@"Country"])
                return NO;
        if (![Validation required:state_Txt withCaption:@"State"])
                return NO;
        if (![Validation required:city_Txt withCaption:@"City"])
                return NO;
        
        return YES;
}

#pragma mark - Button Action

- (IBAction)onClickOf_Proceed_To_Checkout:(id)sender {
        [self.view endEditing:YES];
        if ([self validateForm])
        {
                [self saveShippingAddressWsCall];
        }
        
}

- (IBAction)onClickOf_State_Dropdown:(id)sender {
        [self.view endEditing:YES];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles: nil];
        
        actionSheet.tag=STATE_ACTIONSHEET_TAG;
        for (int i = 0; i < [stateArray count]; i++) {
                [actionSheet addButtonWithTitle:[[stateArray objectAtIndex:i]valueForKey:@"label"]];
        }
        
        [actionSheet showInView:self.view];
        
}

- (IBAction)onClickOf_Country_Dropdown:(id)sender {
        [self.view endEditing:YES];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles: nil];
        
        actionSheet.tag=COUNTRY_ACTIONSHEET_TAG;
        for (int i = 0; i < [countryListArray count]; i++) {
                [actionSheet addButtonWithTitle:[[countryListArray objectAtIndex:i]valueForKey:@"label"]];
        }
        [actionSheet showInView:self.view];
        
}

- (IBAction)onclickof_Pincode:(id)sender{
        [self.view endEditing:YES];
        if(pinCode_Txt.text.length == 6)
                [self pinCodeWSCall];
        else
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter 6 digit pincode number" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
        }
        
}

- (IBAction)backBtnClicked:(id)sender {
        [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UIActionsheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
        //NSLog(@"%ld",(long)buttonIndex);
        
        if (buttonIndex!=0)
        {
                if(actionSheet.tag==STATE_ACTIONSHEET_TAG){
                        state_Txt.text=[[stateArray objectAtIndex:buttonIndex-1]valueForKey:@"label"];
                        regionId=[[stateArray objectAtIndex:buttonIndex-1]valueForKey:@"value"];
                        
                        selectedStateAtIndex=buttonIndex-1;
                }else if(actionSheet.tag==COUNTRY_ACTIONSHEET_TAG){
                        if (countryListArray.count>0) {
                                country_Txt.text=[[countryListArray objectAtIndex:buttonIndex-1]valueForKey:@"label"];
                                [self regionRequestWithCountryId:[[countryListArray objectAtIndex:buttonIndex-1]valueForKey:@"value"]];
                                selectedCountryAtIndex=buttonIndex-1;
                        }
                }
                
        }
}

-(void)regionRequestWithCountryId:(NSString *)countryId
{
        Webservice  *callRegionList = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callRegionList.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callRegionList webServiceWitParameters:[NSString stringWithFormat:@"country_id=%@",countryId] andURL:GET_REGION_LISTING_WS MathodName:@"GETREGION"];
        }
        
}



-(void)pinCodeWSCall
{
        Webservice  *callRegionList = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callRegionList.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callRegionList webServiceWitParameters:[NSString stringWithFormat:@"pincode=%@",pinCode_Txt.text] andURL:PINCODE_WS MathodName:@"PINCODE"];
        }
}
#pragma mark - Webservice Delegate
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:@"GETREGION"]) {
                stateArray=[response valueForKey:@"region"];
        }
        else if ([strName isEqualToString:@"SAVE_BILLING_ADDRESS"]){
                //NSLog(@"RESPONSE %@",response);
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        [self saveShippingAddressWsCall];
                }else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        else if ([strName isEqualToString:@"PINCODE"]){
                stateDropdownbutton.enabled=NO;
                countryDropdownButton.enabled=NO;
                city_Txt.enabled=NO;
                
                if ([[response valueForKey:@"status"]intValue]==0) {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Pincode not found"];
                        city_Txt.text=@"";
                        state_Txt.text=@"";
                        country_Txt.text=@"";
                }else{
                        selectedCountryAtIndex=5;
                        
                        NSDictionary *addressDataDic = [response valueForKey:@"address_data"];
                        
                        if([[addressDataDic valueForKey:@"city"] isKindOfClass:[NSNull class]] || [[addressDataDic valueForKey:@"default_name"] isKindOfClass:[NSNull class]] || [[addressDataDic valueForKey:@"region_id"] isKindOfClass:[NSNull class]])
                        {
                                city_Txt.text = @"";
                                state_Txt.text = @"";
                                regionId = @"";
                        }
                        else
                        {
                                city_Txt.text=[[response valueForKey:@"address_data"]valueForKey:@"city"];
                                state_Txt.text=[[response valueForKey:@"address_data"]valueForKey:@"default_name"];
                                regionId=[[response valueForKey:@"address_data"]valueForKey:@"region_id"];
                        }
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"value CONTAINS[cd] %@",[[response valueForKey:@"address_data"]valueForKey:@"country_code"]];
                        NSArray *filterdArray = [[countryListArray filteredArrayUsingPredicate:predicate] mutableCopy];
                        NSDictionary *dict=[[NSDictionary alloc]init];
                        dict=[[filterdArray objectAtIndex:0]mutableCopy];
                        selectedCountryAtIndex=[countryListArray indexOfObject:dict];
                        
                        country_Txt.text=[[filterdArray objectAtIndex:0]valueForKey:@"label"];
                        
                }
                
                // city_Txt.text=[[response valueForKey:@"address_data"]valueForKey:@"city"];
        }
        else if ([strName isEqualToString:@"SAVE_SHIPPING_ADDRESS"])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        
                        
                        Checkout *objCheckout = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Checkout"];
                        objCheckout.userMobileNo=mobile_Txt.text;
                        objCheckout.isAffiliateUser=isAff;
                        objCheckout.userEmailId=userEmail;
                        [self.navigationController pushViewController:objCheckout animated:YES];
                        
                        
                }else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                
        }
}


#pragma mark - UITextfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [objScrollViewbg setContentOffset:CGPointZero animated:YES];
        [objScrollViewbg setContentSize:CGSizeMake(objScrollViewbg.frame.size.width, 600)];
        [textField resignFirstResponder];
        return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
        [self autoScrolTextField:textField onScrollView:self.objScrollViewbg];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if(textField == lName_Txt)
                {
                        if(textField.text.length < LAST_NAME_MAX)
                        {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:LAST_NAME_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                        }
                }if(textField == fName_Txt)
                {
                        if(textField.text.length < LAST_NAME_MAX)
                        {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:FIRST_NAME_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                        }
                }
                else if(textField == mobile_Txt)
                {
                        if(textField.text.length < MOBILE_NO_MAX)
                        {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                        }
                }
                
        });
        
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if(textField == _storeNameTextField)
    {
        NSUInteger newLength = [textField.text length] + [string length];
        
        if(newLength > 30){
            return NO;
        }
        else
        {
            return YES;
        }
    }
        else if(textField == fName_Txt || textField == lName_Txt)
        {
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                
                return [string isEqualToString:filtered];
        }
        else if(textField == mobile_Txt)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 10){
                        
                        return NO;
                }
                else{
                        return YES;
                }
        }
        
        else if(textField == _gstNumberTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_GST_NUMBER_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                if(newLength <= 15)
                {
                        return [string isEqualToString:filtered];
                }
                
                if(newLength > 15){
                        
                        return NO;
                }
        }
        else if(textField == pinCode_Txt)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 6){
                        
                        return NO;
                }
                else{
                        city_Txt.text=@"";
                        state_Txt.text=@"";
                        country_Txt.text=@"";
                        return YES;
                }
        }
        return YES;
}


-(UIToolbar *)setToolBarNumberKeyBoard:(id)sender
{
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:sender action:@selector(doneButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Done" forState:UIControlStateNormal];
        button.frame = CGRectMake(0 ,0,60,40);
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 40)];
        [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
        
        return toolbar;
}

-(void)doneButtonDidPressed:(id)sender
{
        dispatch_async(dispatch_get_main_queue(), ^{
                //[mobile_Txt resignFirstResponder];
                [self.view endEditing:YES];
                [objScrollViewbg setContentOffset:CGPointMake(0, 0) animated:YES];
        });
}
#pragma mark - Utility Methods

-(void)setUpTextFields
{
        mobile_Txt.inputAccessoryView = [self setToolBarNumberKeyBoard:self];
        //    telephone_Txt.inputAccessoryView = [self setToolBarNumberKeyBoard:self];
        pinCode_Txt.inputAccessoryView = [self setToolBarNumberKeyBoard:self];
        
        
        //    [self createWrapView:fName_Txt];
        //    [self createWrapView:lName_Txt];
        [self createWrapView:_gstNumberTextField];
        
        [self createWrapView:_streetAddress2TextField];
        [self createWrapView:_storeNameTextField];
        [self createWrapView:add_Txt];
        [self createWrapView:pinCode_Txt];
        [self createWrapView:country_Txt];
        [self createWrapView:city_Txt];
        //[self createWrapView:telephone_Txt];
        [self createWrapView:state_Txt];
        [self createWrapView:mobile_Txt];
}
- (void) autoScrolTextField: (UITextField *) textField onScrollView: (UIScrollView *) scrollView {
        //    float slidePoint = 0.0f;
        //    float keyBoard_Y_Origin = self.view.bounds.size.height - 216.0f;
        //    float textFieldButtomPoint = textField.superview.frame.origin.y + (textField.frame.origin.y + textField.frame.size.height);
        //
        //    if (keyBoard_Y_Origin < textFieldButtomPoint+40 - scrollView.contentOffset.y) {
        //        slidePoint = textFieldButtomPoint - keyBoard_Y_Origin +40+10.0f;
        //        CGPoint point = CGPointMake(0.0f, slidePoint);
        //        scrollView.contentOffset = point;
        //    }
}

-(void)createWrapView:(UITextField *)textfield{
        UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 10, textfield.frame.size.height)];
        textfield.leftViewMode = UITextFieldViewModeAlways;
        textfield.leftView = wrapView;
}
/*
 -(void)saveBillingAddressWsCall
 {
 Webservice  *callBillingAddWs = [[Webservice alloc] init];
 BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
 if(!chkInternet)
 {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
 [alert show];
 }
 else
 {
 callBillingAddWs.delegate =self;
 [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
 [callBillingAddWs webServiceWitParameters:strBillingAddParameter andURL:SAVE_BILLING_ADDRESS_WS MathodName:@"SAVE_BILLING_ADDRESS"];
 }
 }
 */
-(void)saveShippingAddressWsCall
{
        Webservice  *callBillingAddWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callBillingAddWs.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callBillingAddWs webServiceWitParameters:[self getShippingAddressParam] andURL:SAVE_SHIPPING_ADDRESS_WS MathodName:@"SAVE_SHIPPING_ADDRESS"];
        }
}



-(NSString*)getShippingAddressParam
{
        if ([self validateForm])
        {
                int userId=(int)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]integerValue];
                NSString *countryId=[[countryListArray objectAtIndex:selectedCountryAtIndex]valueForKey:@"value"];
                //NSString *regionId=[[stateArray objectAtIndex:selectedStateAtIndex]valueForKey:@"value"];
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                //        NSString *strParam=[NSString stringWithFormat:@"quote_id=%d&customer_id=%d&shippingAddressId=&firstName=%@&lastname=%@&email=&street=%@&postcode=%@&city=%@&telephone=%@&country_id=%@&region_id=%@&mobile=%@&company=%@&street1=%@",quoteID,userId,fName_Txt.text,lName_Txt.text,add_Txt.text,pinCode_Txt.text,city_Txt.text,telephone_Txt.text,countryId,regionId,mobile_Txt.text,_storeNameTextField.text,_streetAddress2TextField.text];
                
                NSString *strParam=[NSString stringWithFormat:@"quote_id=%d&customer_id=%d&shippingAddressId=&firstName=%@&lastname=%@&email=&street=%@&postcode=%@&city=%@&telephone=%@&country_id=%@&region_id=%@&mobile=%@&company=%@&street1=%@&gstin=%@",quoteID,userId,_storeNameTextField.text,@"",add_Txt.text,pinCode_Txt.text,city_Txt.text,mobile_Txt.text,countryId,regionId,mobile_Txt.text,_storeNameTextField.text,_streetAddress2TextField.text,_gstNumberTextField.text];
                return strParam;
        }
        return 0;
}





@end
