//
//  ShippingAddress.h
//  EB
//
//  Created by webwerks on 9/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface ShippingAddress : BaseNavigationControllerWithBackBtn<UIActionSheetDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UITextField *fName_Txt;
    __weak IBOutlet UITextField *lName_Txt;
    __weak IBOutlet UITextField *add_Txt;
    __weak IBOutlet UITextField *pinCode_Txt;
    __weak IBOutlet UITextField *country_Txt;
    __weak IBOutlet UITextField *city_Txt;
    __weak IBOutlet UITextField *telephone_Txt;
    __weak IBOutlet UITextField *state_Txt;
    __weak IBOutlet UITextField *mobile_Txt;
    NSMutableArray *stateArray;
  //  NSMutableArray *countryListArray;
    
    __weak IBOutlet UIButton *countryDropdownButton;
    
    __weak IBOutlet UIButton *stateDropdownbutton;
    
}

@property (weak, nonatomic) IBOutlet UITextField *streetAddress2TextField;
@property(strong,nonatomic)NSString *strBillingAddParameter;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *objScrollViewbg;
@property(strong,nonatomic)NSMutableArray *countryListArray;
@property (weak, nonatomic) IBOutlet UITextField *storeNameTextField;

@property(strong,nonatomic)NSString *mobNo;
@property(nonatomic)BOOL isAff;
@property(strong,nonatomic)NSString *userEmail;

@property (weak, nonatomic) IBOutlet UITextField *gstNumberTextField;

@property (weak, nonatomic) IBOutlet UIButton *proceedToCheckoutBtn;
- (IBAction)onclickof_Pincode:(id)sender;
- (IBAction)onClickOf_State_Dropdown:(id)sender;
- (IBAction)onClickOf_Proceed_To_Checkout:(id)sender;
- (IBAction)onClickOf_Country_Dropdown:(id)sender;








@end
