//
//  ProceedToPayment.h
//  EB
//
//  Created by webwerks on 9/12/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface ProceedToPayment : BaseNavigationControllerWithBackBtn<UITextFieldDelegate,UIActionSheetDelegate>
{
    
    __weak IBOutlet NSLayoutConstraint *pStoreNameTFHeight;
    __weak IBOutlet NSLayoutConstraint *pStoreNameHeight;
    __weak IBOutlet UITextField *partnersStoreName_Txt;
    __weak IBOutlet UITextField *fName_Txt;
    __weak IBOutlet UITextField *lName_Txt;
    __weak IBOutlet UITextField *add_Txt;
    __weak IBOutlet UITextField *pinCode_Txt;
    __weak IBOutlet UITextField *country_Txt;
    __weak IBOutlet UITextField *city_Txt;
    __weak IBOutlet UITextField *telephone_Txt;
    __weak IBOutlet UITextField *state_Txt;
    __weak IBOutlet UITextField *mobile_Txt;
    __weak IBOutlet UITextField *address_Dropdown_Txt;
    __weak IBOutlet UITextField *password_Txt;
    __weak IBOutlet UITextField *email_Txt;
    __weak IBOutlet UITextField *confirm_Password_Txt;
    __weak IBOutlet UITextField *cform_Txt;
    __weak IBOutlet UIView *objAddNewView;
    __weak IBOutlet NSLayoutConstraint *heightBgView;
    __weak IBOutlet UIView *objNewUserView;
    __weak IBOutlet UIImageView *imgCheckbox;

    
    
    __weak IBOutlet UIView *objTextfieldBgView;
    __weak IBOutlet NSLayoutConstraint *newUserViewHeight;
    NSMutableArray *stateArray;
    NSMutableArray *countryListArray;
    
    __weak IBOutlet NSLayoutConstraint *cformBgView_Height;
    
    __weak IBOutlet UIButton *stateDropdownButton;
    
    __weak IBOutlet UIButton *countryDropdownButton;
    
    //for apply coupon
    __weak IBOutlet UILabel *base_Price_Lbl;
    __weak IBOutlet UILabel *vat_Price_Lbl;
    __weak IBOutlet UILabel *total_Payble_Lbl;
    __weak IBOutlet UIView *couponResultBgView;
    __weak IBOutlet UITextField *coupont_Txt;
    __weak IBOutlet UILabel *lblResult;
    __weak IBOutlet UIButton *declineCouponButton;

    
    
    
    __weak IBOutlet NSLayoutConstraint *discountLblTopConstraint;
    __weak IBOutlet UILabel *subtotalLabel;
    __weak IBOutlet UILabel *grandTotalLabel;
    __weak IBOutlet UILabel *taxLabel;
    __weak IBOutlet UILabel *discountLabel;
    __weak IBOutlet UILabel *discountAmountLabel;

    __weak IBOutlet UILabel *partnerStoreNameKeyLabel;
    __weak IBOutlet UILabel *gstInsubLabel;
    __weak IBOutlet UILabel *gstInLabel;
    __weak IBOutlet NSLayoutConstraint *gstInSubLblHeightConstraint;
}
@property (weak, nonatomic) IBOutlet UITextField *storeNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *streetAddress2TextField;

@property(strong,nonatomic)NSMutableArray *addressArray;
@property(strong,nonatomic)NSMutableArray *detailAddressArray;
@property(strong,nonatomic)NSDictionary *titleLabelDict;

@property(nonatomic)BOOL isAffiliateUser;
@property(nonatomic)BOOL isCformUser;
@property(strong,nonatomic)NSArray *billingOptionsArr;
@property(strong,nonatomic)NSString *gstNumString;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *objScrollViewbg;

- (IBAction)onclickof_Pincode:(id)sender;
- (IBAction)onClickOf_State_Dropdown:(id)sender;
- (IBAction)onClickOf_Country_Dropdown:(id)sender;
- (IBAction)onClickOfCform_Dropdown:(id)sender;

- (IBAction)onClickOfDropdown_Add:(id)sender;
- (IBAction)onClickOfProceed_To_Checkout:(id)sender;
- (IBAction)onClickOfCheckMark:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *addressGstNumberTxtField;
@property (weak, nonatomic) IBOutlet UITextField *gstNumberTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gstNumTxtFieldHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gstNumLabelHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *applyCouponHt;
@property (weak, nonatomic) IBOutlet UIView *applyCouponView;

@property (weak, nonatomic) IBOutlet UILabel *appDiscountlbl;

@property (weak,nonatomic) IBOutlet UIButton *proceedToCheckoutBtn;
- (IBAction)onClickOfApply_Coupon:(id)sender;
- (IBAction)onClickOfDecline_Coupon:(id)sender;
@end
