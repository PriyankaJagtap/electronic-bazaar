//
//  ProceedToPayment.m
//  EB
//
//  Created by webwerks on 9/12/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//


#import "ProceedToPayment.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "CommonSettings.h"
#import "Webservice.h"
#import "Checkout.h"
#import "ShippingAddress.h"
#import "Validation.h"

#import "GAIDictionaryBuilder.h"
#define LAST_NAME_MAX 3
#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

@interface ProceedToPayment ()<FinishLoadingData>
{
        UITextField *activatedTf;
        NSInteger selectedAddessAtIndex;
        NSInteger selectedCountryAtIndex;
        NSInteger selectedStateAtIndex;
        NSInteger selectedCformAtIndex;
        NSInteger userId;
        //Pass to Checkout
        NSString *userEmailId;
        NSString *userMobileNo;
        NSString *regionId;
        BOOL isDiscountedPriceAdded;
        NSString *loginUserGst;
}
@end

@implementation ProceedToPayment
@synthesize objScrollViewbg,addressArray,detailAddressArray,isAffiliateUser,isCformUser,billingOptionsArr,titleLabelDict;

//for apply coupon


- (void)viewDidLoad {
        [super viewDidLoad];
        [super setViewControllerTitle:@"Checkout"];
        [self setUpTextFields];
        // [self setUpNavigationBar];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([[defaults valueForKey:@"user"] valueForKey:@"gstin"]){
                loginUserGst = [[defaults valueForKey:@"user"] valueForKey:@"gstin"];
        }
        else
        {
                loginUserGst = @"";
        }
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.proceedToCheckoutBtn];
        //NSLog(@"%@",billingOptionsArr);
        if (addressArray.count>1)
        {
                address_Dropdown_Txt.text=[addressArray objectAtIndex:0];
                if([[detailAddressArray objectAtIndex:0] objectForKey:@"gstin"])
                        _gstNumberTextField.text = [NSString stringWithFormat:@"%@",[[detailAddressArray objectAtIndex:0] objectForKey:@"gstin"]];
                else
                        _gstNumberTextField.text = loginUserGst;
                
                heightBgView.constant = 0;
                [objAddNewView setHidden:YES];
                if ([[[detailAddressArray objectAtIndex:0]allKeys]containsObject:@"company"]) {
                        partnersStoreName_Txt.text=[[detailAddressArray objectAtIndex:0]objectForKey:@"company"];
                        partnersStoreName_Txt.enabled=NO;
                }
                else
                {
                     partnersStoreName_Txt.text = @"";
                     partnersStoreName_Txt.enabled=YES;
                }
                
        }else{
                heightBgView.constant=354;
                address_Dropdown_Txt.text=@"New Address";
                pStoreNameHeight.constant=0;
                pStoreNameTFHeight.constant=0;
                _gstNumLabelHeightConstraint.constant =0;
                _gstNumTxtFieldHeightConstraint.constant = 0;
                _addressGstNumberTxtField.text = loginUserGst;
                
        }
        //[self.view setFrame:CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, 700)];
        
        [[AppDelegate getAppDelegateObj].tabBarObj hideTabbar];
        
        
        if (isCformUser)
                cformBgView_Height.constant=30;
        [self countryListRequest];
        
        [couponResultBgView setHidden:YES];
        if (isAffiliateUser) {
                _appDiscountlbl.text = APP_DISCOUNT_LABEL;
        }
        [self callCouponListing];
        
        [pinCode_Txt addTarget:self
                        action:@selector(pincodeValueChanged:)
              forControlEvents:UIControlEventEditingChanged];
        
        NSDictionary *userDic = [defaults valueForKey:@"user"];
        if([userDic valueForKey:@"pincode"])
        {
                pinCode_Txt.text = [userDic valueForKey:@"pincode"];
                //[self pinCodeWSCall];
                
        }
        
        if(userDic)
        {
                if( [userDic valueForKey:@"firstname"])
                        _storeNameTextField.text = [userDic valueForKey:@"firstname"];
                mobile_Txt.text = [userDic valueForKey:@"mobile"];
        }
}

-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:YES];
        // [self.navigationController setNavigationBarHidden:NO];
        
        userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
        partnerStoreNameKeyLabel.text=[titleLabelDict valueForKey:@"partner_store_label"];
        gstInsubLabel.text=[titleLabelDict valueForKey:@"gstin_sub_label"];
        NSLog(@"%d",[[titleLabelDict valueForKey:@"gstin_label"]length]);
        
        //    if ([[titleLabelDict valueForKey:@"gstin_label"]length]==0) {
        //        gstInSubLblHeightConstraint.constant=10;
        //    }else{
        //        gstInSubLblHeightConstraint.constant=30;
        //    }
        gstInLabel.text=[titleLabelDict valueForKey:@"gstin_label"];
        //gstInsubLabel.text=@"Abcdedf";
        if (userId==0)
        {
                newUserViewHeight.constant=120;
                [objNewUserView setHidden:NO];
                
        }else{
                newUserViewHeight.constant=0;
                [objNewUserView setHidden:YES];
        }
        
}

-(void)viewDidAppear:(BOOL)animated{
        [super viewDidAppear:YES];
        [self.navigationItem setTitle:@""];
        
        
}
-(void) pincodeValueChanged:(id)sender {
        // your code
        
        if(pinCode_Txt.text.length  == 6)
                [self pinCodeWSCall];
        
}
#pragma mark - textField delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
        if(textField != coupont_Txt)
                [objScrollViewbg setContentOffset:CGPointMake(0, 0) animated:YES];
        [textField resignFirstResponder];
        return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if(textField == lName_Txt)
                {
                        if(textField.text.length < LAST_NAME_MAX)
                        {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:LAST_NAME_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                        }
                }if(textField == fName_Txt)
                {
                        if(textField.text.length < LAST_NAME_MAX)
                        {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:FIRST_NAME_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                        }
                }
                else if(textField == mobile_Txt)
                {
                        if(textField.text.length < 10)
                        {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                        }
                }
                
        });
        
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
        
        
        if(textField == fName_Txt || textField == lName_Txt)
        {
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                
                return [string isEqualToString:filtered];
        }
        else if(textField == partnersStoreName_Txt || textField == _storeNameTextField)
        {
            NSUInteger newLength = [textField.text length] + [string length];
            
            if(newLength > 30){
                return NO;
            }
            else
            {
                return YES;
            }
        }
        else if(textField == mobile_Txt)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 10){
                        
                        return NO;
                }
                else{
                        return YES;
                }
        }
        else if(textField == _gstNumberTextField ||  textField == _addressGstNumberTxtField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_GST_NUMBER_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                if(newLength <= 15)
                {
                        return [string isEqualToString:filtered];
                }
                
                if(newLength > 15){
                        
                        return NO;
                }
                
        }
        else if(textField == pinCode_Txt)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 6){
                        
                        return NO;
                }
                else{
                        city_Txt.text=@"";
                        state_Txt.text=@"";
                        country_Txt.text=@"";
                        return YES;
                }
        }
        return YES;
}

#pragma mark - UIActionsheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
        
        if (buttonIndex!=0)
        {
                if (actionSheet.tag==ADDRESS_ACTIONSHEET_TAG)
                {
                        NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
                        if ([title isEqualToString:@"New Address"])
                        {
                                [objAddNewView setHidden:NO];
                                heightBgView.constant=354;
                                _applyCouponHt.constant = 0;
                                _applyCouponView.hidden = YES;
                                pStoreNameHeight.constant=0;
                                pStoreNameTFHeight.constant=0;
                                _gstNumLabelHeightConstraint.constant =0;
                                gstInLabel.text = @"";
                                gstInsubLabel.text = @"";
                                _gstNumTxtFieldHeightConstraint.constant = 0;
                                _addressGstNumberTxtField.text = loginUserGst;
                                
                        }
                        else{
                                [objAddNewView setHidden:YES];
                                heightBgView.constant=0;
                                _applyCouponHt.constant = 270;
                                pStoreNameHeight.constant=18;
                                pStoreNameTFHeight.constant=30;
                                _gstNumLabelHeightConstraint.constant =18;
                                _gstNumTxtFieldHeightConstraint.constant = 30;
                                _applyCouponView.hidden = NO;
                                
                                  gstInLabel.text=[titleLabelDict valueForKey:@"gstin_label"];
                                gstInsubLabel.text=[titleLabelDict valueForKey:@"gstin_sub_label"];
                        }
                        address_Dropdown_Txt.text= [addressArray objectAtIndex:buttonIndex-1];
                        selectedAddessAtIndex = buttonIndex-1; //To add proper index of selected address.
                        
                        NSString *companyName = [[detailAddressArray objectAtIndex:buttonIndex-1]objectForKey:@"company"];
                        if(companyName != nil && ![companyName isEqualToString:@""])
                        {
                                partnersStoreName_Txt.text=[[detailAddressArray objectAtIndex:buttonIndex-1]objectForKey:@"company"];
                                partnersStoreName_Txt.enabled = NO;
                        }
                        else
                        {
                                 partnersStoreName_Txt.enabled = YES;
                                partnersStoreName_Txt.text =@"";
                        }
                       
                        
                        if([[detailAddressArray objectAtIndex:buttonIndex-1] objectForKey:@"gstin"])
                                _gstNumberTextField.text = [NSString stringWithFormat:@"%@",[[detailAddressArray objectAtIndex:buttonIndex-1] objectForKey:@"gstin"]];
                        else
                                _gstNumberTextField.text = loginUserGst;
                        
                        
                }
                else if(actionSheet.tag==STATE_ACTIONSHEET_TAG){
                        state_Txt.text=[[stateArray objectAtIndex:buttonIndex-1]valueForKey:@"label"];
                        regionId=[[stateArray objectAtIndex:buttonIndex-1]valueForKey:@"value"];
                        selectedStateAtIndex=buttonIndex-1;
                }
                else if(actionSheet.tag==COUNTRY_ACTIONSHEET_TAG){
                        country_Txt.text=[[countryListArray objectAtIndex:buttonIndex-1]valueForKey:@"label"];
                        
                        if (countryListArray.count>0) {
                                [self regionRequestWithCountryId:[[countryListArray objectAtIndex:buttonIndex-1]valueForKey:@"value"]];
                                selectedCountryAtIndex=buttonIndex-1;
                        }
                }
                else if(actionSheet.tag==CFORM_ACTIONSHEET_TAG){
                        cform_Txt.text=[[billingOptionsArr objectAtIndex:buttonIndex-1]valueForKey:@"label"];
                        selectedCformAtIndex=buttonIndex-1;
                }
        }
}


-(void)regionRequestWithCountryId:(NSString *)countryId
{
        Webservice  *callRegionList = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callRegionList.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callRegionList webServiceWitParameters:[NSString stringWithFormat:@"country_id=%@",countryId] andURL:GET_REGION_LISTING_WS MathodName:@"GETREGION"];
        }
        
}

-(void)countryListRequest
{
        Webservice  *callCountryList = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callCountryList.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callCountryList webServiceWitParameters:nil andURL:COUNTRYLIST_WS MathodName:@"GETCOUNTRYLIST"];
        }
        
}

- (IBAction)onClickOfDropdown_Add:(id)sender {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles: nil];
        actionSheet.tag=ADDRESS_ACTIONSHEET_TAG;
        
        for (int i = 0; i < [addressArray count]; i++) {
                [actionSheet addButtonWithTitle:[addressArray objectAtIndex:i]];
        }
        actionSheet.viewForBaselineLayout.tintColor=[UIColor whiteColor];
        [actionSheet showInView:self.view];
        
}

- (IBAction)onClickOfProceed_To_Checkout:(id)sender {
        
        if(pStoreNameHeight.constant != 0)
        {
                if(![Validation required:partnersStoreName_Txt withCaption:@"Partner's Store Name"])
                        return;
                
                if (_gstNumberTextField.text.length != 0){
                        if (_gstNumberTextField.text.length < 15) {
                                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide valid GST Number"];
                                return;
                        }
                }
        }
        
        [self.view endEditing:YES];
        if ([address_Dropdown_Txt.text isEqualToString:@"New Address"])
        {
                if ([self validateForm]) {
                        if ([imgCheckbox.image isEqual:[UIImage imageNamed:@"checked"]]) {
                                [self saveBillingAddressWsCall];
                        }else{
                                [self saveBillingAddressOnlyWsCallWithParam:[self getUrlParameterWithBillingAddressSame:NO]];
                        }
                }
        }else{
                if ([imgCheckbox.image isEqual:[UIImage imageNamed:@"checked"]]){
                        [self saveBillingAddressWsCall];
                }else{
                        //            ShippingAddress *objShipping = [[UIStoryboard storyboardWithName:isIpad?@"Main_iPad":@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ShippingAddress"];
                        
                        ShippingAddress *objShipping = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ShippingAddress"];
                        
                        objShipping.strBillingAddParameter=[self getUrlParameterWithBillingAddressSame:NO];
                        
                        objShipping.countryListArray=countryListArray;
                        objShipping.mobNo=userMobileNo;
                        objShipping.isAff=isAffiliateUser;
                        objShipping.userEmail=userEmailId;
                        [self.navigationController pushViewController:objShipping animated:YES];
                }
        }
        
}

- (IBAction)onClickOfCheckMark:(UIButton*)sender {
        if ([sender tag]==0) {
                imgCheckbox.image=[UIImage imageNamed:@"checked"];
                sender.tag=1;
                
        }else{
                imgCheckbox.image=[UIImage imageNamed:@"unchecked"];
                sender.tag=0;
        }
}

- (IBAction)onclickof_Pincode:(id)sender {
        [self.view endEditing:YES];
        if(pinCode_Txt.text.length == 6)
                [self pinCodeWSCall];
        else
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter 6 digit pincode number" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
        }
        
}

- (IBAction)onClickOf_State_Dropdown:(id)sender {
        [self.view endEditing:YES];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles: nil];
        
        actionSheet.tag=STATE_ACTIONSHEET_TAG;
        
        for (int i = 0; i < [stateArray count]; i++) {
                [actionSheet addButtonWithTitle:[[stateArray objectAtIndex:i]valueForKey:@"label"]];
        }
        [actionSheet showInView:self.view];
        
}

- (IBAction)onClickOf_Country_Dropdown:(id)sender {
        [self.view endEditing:YES];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles: nil];
        
        actionSheet.tag=COUNTRY_ACTIONSHEET_TAG;
        for (int i = 0; i < [countryListArray count]; i++) {
                [actionSheet addButtonWithTitle:[[countryListArray objectAtIndex:i]valueForKey:@"label"]];
        }
        
        [actionSheet showInView:self.view];
        
}

- (IBAction)onClickOfCform_Dropdown:(id)sender {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles: nil];
        actionSheet.tag=CFORM_ACTIONSHEET_TAG;
        
        for (int i = 0; i < [billingOptionsArr count]; i++) {
                [actionSheet addButtonWithTitle:[[billingOptionsArr objectAtIndex:i]valueForKey:@"label"]];
        }
        actionSheet.viewForBaselineLayout.tintColor=[UIColor whiteColor];
        [actionSheet showInView:self.view];
}
-(void)doneButtonDidPressed:(id)sender
{
        dispatch_async(dispatch_get_main_queue(), ^{
                [self.view endEditing:YES];
                [objScrollViewbg setContentOffset:CGPointMake(0, 0) animated:YES];
                
        });
}

#pragma mark - Webservice delegate
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:@"GETREGION"]) {
                stateArray=[response valueForKey:@"region"];
        }
        else if ([strName isEqualToString:@"SAVE_BILLING_ADDRESS"]){
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        Checkout *objCheckout = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Checkout"];
                        
                        
                        objCheckout.userMobileNo=userMobileNo;
                        objCheckout.isAffiliateUser=isAffiliateUser;
                        objCheckout.userEmailId=userEmailId;
                        [self.navigationController pushViewController:objCheckout animated:YES];
                        //            ShowTabbars *tabBarController = self.menuContainerViewController.centerViewController;
                        //            UINavigationController *objNavigation = (UINavigationController *)tabBarController.selectedViewController;
                        //              [objNavigation pushViewController:objCheckout animated:YES];
                        
                        
                }else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        else if ([strName isEqualToString:@"PINCODE"]){
                stateDropdownButton.enabled=NO;
                countryDropdownButton.enabled=NO;
                city_Txt.enabled=NO;
                
                if ([[response valueForKey:@"status"]intValue]==0) {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Pincode not found"];
                        city_Txt.text=@"";
                        state_Txt.text=@"";
                        country_Txt.text=@"";
                }
                else{
                        selectedCountryAtIndex=5;
                        
                        NSDictionary *addressDataDic = [response valueForKey:@"address_data"];
                        
                        if([[addressDataDic valueForKey:@"city"] isKindOfClass:[NSNull class]] || [[addressDataDic valueForKey:@"default_name"] isKindOfClass:[NSNull class]] || [[addressDataDic valueForKey:@"region_id"] isKindOfClass:[NSNull class]])
                        {
                                city_Txt.text = @"";
                                state_Txt.text = @"";
                                regionId = @"";
                        }
                        else
                        {
                                city_Txt.text=[[response valueForKey:@"address_data"]valueForKey:@"city"];
                                if([[response valueForKey:@"address_data"]valueForKey:@"default_name"])
                                        state_Txt.text=[[response valueForKey:@"address_data"]valueForKey:@"default_name"];
                                
                                regionId=[[response valueForKey:@"address_data"]valueForKey:@"region_id"];
                        }
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"value CONTAINS[cd] %@",[[response valueForKey:@"address_data"]valueForKey:@"country_code"]];
                        NSArray *filterdArray = [[countryListArray filteredArrayUsingPredicate:predicate] mutableCopy];
                        NSDictionary *dict=[[NSDictionary alloc]init];
                        dict=[[filterdArray objectAtIndex:0]mutableCopy];
                        selectedCountryAtIndex=[countryListArray indexOfObject:dict];
                        country_Txt.text=[[filterdArray objectAtIndex:0]valueForKey:@"label"];
                }
        }
        else if ([strName isEqualToString:@"GETCOUNTRYLIST"]){
                countryListArray=[response valueForKey:@"countries"];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSDictionary *userDic = [defaults valueForKey:@"user"];
                if([userDic valueForKey:@"pincode"])
                {
                        [self pinCodeWSCall];
                        
                }
        }
        else if ([strName isEqualToString:@"SAVE_BILLING_ADDRESS_ONLY"]){
                if ([[response valueForKey:@"status"]intValue]==1) {
                        //            ShippingAddress *objShipping = [[UIStoryboard storyboardWithName:isIpad?@"Main_iPad":@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ShippingAddress"];
                        
                        
                        ShippingAddress *objShipping = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ShippingAddress"];
                        
                        objShipping.countryListArray=countryListArray;
                        objShipping.mobNo=userMobileNo;
                        objShipping.isAff=isAffiliateUser;
                        objShipping.userEmail=userEmailId;
                        [self.navigationController pushViewController:objShipping animated:YES];
                        
                }else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        else if([strName isEqualToString:@"ADDCOUPONCODE"]){
                if (response) {
                        [couponResultBgView setHidden:NO];
                        if ([[response valueForKey:@"status"]intValue]==1) {
                                lblResult.text=@"Coupon is applied";
                                [lblResult setTextColor:[UIColor lightGrayColor]];
                                declineCouponButton.hidden=NO;
                                isDiscountedPriceAdded=YES;
                                [self setSummary:response];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                        [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Coupon applied successfully!"];
                                });
                        }else{
                                lblResult.text=@"Coupon code is not valid";
                                [lblResult setTextColor:[UIColor redColor]];
                                declineCouponButton.hidden=YES;
                                [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Coupon code is not valid!"];
                                
                        }
                }
        }
        else if([strName isEqualToString:@"COUPONLISTING"]){
                if ([[response valueForKey:@"status"]intValue]==1) {
                        [self setSummary:response];
                }
                else
                {
                        [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
                }
                
                
        }else if ([strName isEqualToString:@"REMOVECOUPONCODE"]){
                if ([[response valueForKey:@"status"]intValue]==1) {
                        
                        [self setSummary:response];
                }
                else
                {
                        [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
                }
                [couponResultBgView setHidden:YES];
        }
        
        [objScrollViewbg setContentOffset:CGPointMake(0, 0) animated:YES];
}


#pragma mark: Webservice Calls
-(void)saveBillingAddressWsCall
{
        Webservice  *callBillingAddWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callBillingAddWs.delegate =self;
                if ([self validateCFormUser]) {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        [callBillingAddWs webServiceWitParameters:[self getUrlParameterWithBillingAddressSame:YES] andURL:SAVE_BILLING_ADDRESS_WS MathodName:@"SAVE_BILLING_ADDRESS"];
                }
        }
}

-(void)saveBillingAddressOnlyWsCallWithParam:(NSString *)param
{
        Webservice  *callBillingAddWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callBillingAddWs.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callBillingAddWs webServiceWitParameters:param andURL:SAVE_BILLING_ADDRESS_WS MathodName:@"SAVE_BILLING_ADDRESS_ONLY"];
        }
}



-(BOOL)validateCFormUser
{
        if (isCformUser) {
                if (![Validation required:cform_Txt withCaption:@"Cform option"])
                        return NO;
        }
        return YES;
}

-(NSString*)getUrlParameterWithBillingAddressSame:(BOOL)val
{
        NSString *strParam=@"";
        userEmailId=@"";
        NSString *strPassword=@"";
        NSString *strCform;
        
        int billing_use_for_shipping;
        if (val){
                billing_use_for_shipping=1;
        }
        else{
                billing_use_for_shipping=0;
        }
        
        if (userId==0) {
                userEmailId=email_Txt.text;
                strPassword=password_Txt.text;
        }else{
                NSDictionary *userdataDict = [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
                userEmailId=[userdataDict objectForKey:@"email"];
        }
        [CommonSettings setDefaultValue:userEmailId forKey:@"Email"];
        
        if (isCformUser)
                strCform=[[billingOptionsArr objectAtIndex:selectedCformAtIndex]valueForKey:@"key"];
        else
                strCform=@"";
        
        if ([address_Dropdown_Txt.text isEqualToString:@"New Address"]){
                NSString *countryId=[[countryListArray objectAtIndex:selectedCountryAtIndex]valueForKey:@"value"];
                //        if(stateArray.count>0)
                //           regionId =[[stateArray objectAtIndex:selectedStateAtIndex]valueForKey:@"value"];
                
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                userMobileNo=mobile_Txt.text;
                int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                
                strParam=[NSString stringWithFormat:@"quote_id=%d&customer_id=%ld&billingAddressId=&firstName=%@&lastname=%@&email=%@&street=%@&postcode=%@&city=%@&telephone=%@&country_id=%@&region_id=%@&mobile=%@&password=%@&confirm_password=%@&billing_use_for_shipping_yes=%d&customer_cform=%@&company=%@&street1=%@&gstin=%@",quoteID,(long)userId,_storeNameTextField.text,@"",email_Txt.text,add_Txt.text,pinCode_Txt.text,city_Txt.text,mobile_Txt.text,countryId,regionId,mobile_Txt.text,strPassword,strPassword,billing_use_for_shipping,strCform,_storeNameTextField.text,_streetAddress2TextField.text,_addressGstNumberTxtField.text];
        }
        else{
                //  NSString *fName=[[detailAddressArray objectAtIndex:selectedAddessAtIndex]valueForKey:@"firstname"];
                //  NSString *lName=[[detailAddressArray objectAtIndex:selectedAddessAtIndex]valueForKey:@"lastname"];
                //  NSString *street=[[detailAddressArray objectAtIndex:selectedAddessAtIndex]valueForKey:@"street"];
                // NSString *postCode=[[detailAddressArray objectAtIndex:selectedAddessAtIndex]valueForKey:@"postcode"];
                // NSString *city=[[detailAddressArray objectAtIndex:selectedAddessAtIndex]valueForKey:@"city"];
                // NSString *telephone=[[detailAddressArray objectAtIndex:selectedAddessAtIndex]valueForKey:@"telephone"];
                // NSString *countryId=[[detailAddressArray objectAtIndex:selectedAddessAtIndex]valueForKey:@"country_id"];
                userMobileNo=[[detailAddressArray objectAtIndex:selectedAddessAtIndex]valueForKey:@"fax"];
                //  NSString *regionID=[[detailAddressArray objectAtIndex:selectedAddessAtIndex]valueForKey:@"region_id"];
                
                NSString *billinAddID=[NSString stringWithFormat:@"%@",[[detailAddressArray objectAtIndex:selectedAddessAtIndex]valueForKey:@"customer_address_id"]];
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                
                //        strParam=[NSString stringWithFormat:@"quote_id=%d&customer_id=%ld&billingAddressId=&firstName=%@&lastname=%@&street=%@&postcode=%@&city=%@&telephone=%@&country_id=%@&region_id=%@&mobile=%@&password=&confirm_password=&billing_use_for_shipping_yes=%d&customer_cform=%@",quoteID,(long)userId,fName,lName,street,postCode,city,telephone,countryId,regionID,userMobileNo,billing_use_for_shipping,strCform];
                //..
                strParam=[NSString stringWithFormat:@"firstName=%@&customer_id=%ld&billingAddressId=%@&quote_id=%d&customer_cform=%@&billing_use_for_shipping_yes=%d&gstin=%@",partnersStoreName_Txt.text,(long)userId,billinAddID,quoteID,strCform,billing_use_for_shipping,_gstNumberTextField.text];
                
                
        }
        return strParam;
}

-(void)pinCodeWSCall
{
        Webservice  *callRegionList = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callRegionList.delegate =self;
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callRegionList webServiceWitParameters:[NSString stringWithFormat:@"pincode=%@",pinCode_Txt.text] andURL:PINCODE_WS MathodName:@"PINCODE"];
        }
        
}


#pragma mark - Apply coupon methods
-(void)setSummary:(id)response
{
       //base_Price_Lbl.text=[NSString stringWithFormat:@"Rs %ld",(long)[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"subtotal"]integerValue]];
        
       base_Price_Lbl.text= [[CommonSettings sharedInstance] formatIntegerPrice:[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"subtotal"]integerValue]];
        
        
        //total_Payble_Lbl.text=[NSString stringWithFormat:@"Rs %ld",(long)[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"grand_total"]integerValue]];
        
        total_Payble_Lbl.text=[[CommonSettings sharedInstance] formatIntegerPrice:[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"grand_total"]integerValue]];
        
        //vat_Price_Lbl.text=[NSString stringWithFormat:@"Rs %ld",(long)[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"tax_amount"]integerValue]];
        
        vat_Price_Lbl.text=[[CommonSettings sharedInstance] formatIntegerPrice:[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"tax_amount"]integerValue]];
        
        subtotalLabel.text=[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"subtotal_label"];
        taxLabel.text=[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"tax_label"];
        grandTotalLabel.text=[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"grandtotal_label"];
        if (isDiscountedPriceAdded) {
                discountLabel.text=[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"discount_label"];
//discountAmountLabel.text=[NSString stringWithFormat:@"- Rs %ld",(long)[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"discount_amount"]integerValue]];
                
                NSString *discountAmountValue = [[CommonSettings sharedInstance] formatIntegerPrice:[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"discount_amount"]integerValue]];
                
                discountAmountLabel.text=[NSString stringWithFormat:@"- %@",discountAmountValue];
                discountLblTopConstraint.constant=30;
        }else{
                discountLabel.text=@"";
                discountAmountLabel.text=@"";
                discountLblTopConstraint.constant=4;
        }
        
        
        isDiscountedPriceAdded=NO;
}


-(void)callCouponListing
{
        Webservice  *getCouponListing = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                getCouponListing.delegate =self;
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                int userId=[[[defaults valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                if (quoteID!=0)
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        NSString *param=[NSString stringWithFormat:@"quoteId=%d&user_id=%d",quoteID,userId];
                        
                        [getCouponListing webServiceWitParameters:param andURL:COUPON_LISTING MathodName:@"COUPONLISTING"];
                }
        }
}
-(void)addCouponRequest
{
        Webservice  *addCouponWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                addCouponWs.delegate =self;
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                if (quoteID !=0)
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        NSString *param=[NSString stringWithFormat:@"quote_id=%d&couponCode=%@",quoteID,coupont_Txt.text];
                        
                        [addCouponWs webServiceWitParameters:param andURL:ADD_COUPON_CODE MathodName:@"ADDCOUPONCODE"];
                }
        }
}

-(void)removeCouponRequest
{
        Webservice  *removeCouponWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                removeCouponWs.delegate =self;
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                if (quoteID !=0)
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        NSString *param=[NSString stringWithFormat:@"quote_id=%d",quoteID];
                        [removeCouponWs webServiceWitParameters:param andURL:REMOVE_COUPON MathodName:@"REMOVECOUPONCODE"];
                }
        }
}
#pragma mark - IBAction methods of Apply coupon
- (IBAction)onClickOfDecline_Coupon:(id)sender{
        [self removeCouponRequest];
}


- (IBAction)backBtnClicked:(id)sender {
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickOfApply_Coupon:(id)sender{
        [self.view endEditing:YES];
        if (![coupont_Txt.text isEqualToString:@""]) {
                [self addCouponRequest];
        }else{
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter coupon code."];
        }
        
}

#pragma mark - Utility methods

-(void)setUpNavigationBar
{
        [self.navigationController setNavigationBarHidden:NO];
        self.navigationController.navigationBar.translucent=normal;
        UILabel *titleLabel=[[UILabel alloc]init];
        titleLabel.text = @"Checkout";
        titleLabel.font =karlaFontRegular(18.0);
        titleLabel.frame = CGRectMake(0, 0, 100, 30);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor whiteColor];
        self.navigationItem.titleView = titleLabel;
        //    UIColor *bgColor=[UIColor colorWithRed:23/255.0f green:70/255.0f blue:135/255.0f alpha:1.0f];
        
        UIColor *bgColor=[[AppDelegate getAppDelegateObj] colorWithHexString:APP_THEME_COLOR];
        
        self.navigationController.navigationBar.barTintColor = bgColor;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        self.navigationController.navigationBar.backItem.title =@"";
        
        //    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"dropdown_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemPressed)];
        //    
        //    [[self navigationItem] setRightBarButtonItem:rightItem];
}


-(void)setUpTextFields
{
        mobile_Txt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        //    telephone_Txt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        pinCode_Txt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        
        //    [self createWrapView:fName_Txt];
        //    [self createWrapView:lName_Txt];
        
        [self createWrapView:_gstNumberTextField];
        [self createWrapView:_addressGstNumberTxtField];
        [self createWrapView:partnersStoreName_Txt];
        [self createWrapView:_storeNameTextField];
        [self createWrapView:_streetAddress2TextField];
        [self createWrapView:add_Txt];
        [self createWrapView:pinCode_Txt];
        [self createWrapView:country_Txt];
        [self createWrapView:city_Txt];
        //[self createWrapView:telephone_Txt];
        [self createWrapView:state_Txt];
        [self createWrapView:mobile_Txt];
        [self createWrapView:address_Dropdown_Txt];
        [self createWrapView:email_Txt];
        [self createWrapView:password_Txt];
        [self createWrapView:confirm_Password_Txt];
}

-(void)createWrapView:(UITextField *)textfield{
        UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 10, textfield.frame.size.height)];
        textfield.leftViewMode = UITextFieldViewModeAlways;
        textfield.leftView = wrapView;
}

-(BOOL)validateForm
{
        //    if (![Validation required:fName_Txt withCaption:@"First Name"])
        //        return NO;
        //    if (![Validation required:lName_Txt withCaption:@"Last Name"])
        //        return NO;
        //    if(![Validation checkFirstName:fName_Txt])
        //        return NO;
        //    if(![Validation checkLastName:lName_Txt])
        //        return NO;
        if(![Validation required:_storeNameTextField withCaption:@"Store Name"])
                return NO;
        
        if (_addressGstNumberTxtField.text.length != 0){
                if (_addressGstNumberTxtField.text.length < 15) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide valid GST Number"];
                        return NO;
                }
        }
        
        if(![Validation checkMobileNumber:mobile_Txt])
                return NO;
        //    if (![Validation required:telephone_Txt withCaption:@"Telephone"])
        //        return NO;
        
        
        if (![Validation required:address_Dropdown_Txt withCaption:@"Address"])
                return NO;
        if (![Validation required:add_Txt withCaption:@"Address"])
                return NO;
        if (![Validation required:pinCode_Txt withCaption:@"Pin code"])
                return NO;
        
        if (![Validation required:city_Txt withCaption:@"City"])
                return NO;
        if (![Validation required:country_Txt withCaption:@"Country"])
                return NO;
        if (![Validation required:state_Txt withCaption:@"State"])
                return NO;
        //    if (![Validation required:mobile_Txt withCaption:@"Mobile"])
        //        return NO;
        
        
        
        //    if ([mobile_Txt.text length]<10) {
        //        [[CommonSettings sharedInstance]showAlertTitle:@"Title" message:@"Please enter correct Mobile Number."];
        //        return NO;
        //    }
        
        if (isCformUser)
        {
                if (![Validation required:cform_Txt withCaption:@"Cform option"])
                        return NO;
        }
        
        
        if (userId==0) {
                if (![Validation required:email_Txt withCaption:@"Email"])
                        return NO;
                if (![Validation ValidEmail:email_Txt.text]) {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter correct email address"];
                        return NO;
                }
                
                if (![Validation required:password_Txt withCaption:@"Password"])
                        return NO;
                if ([password_Txt.text length]<6) {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Password must be more that 6 character"];
                        return NO;
                }
                if(![Validation required:confirm_Password_Txt withCaption:@"Confirm password"])
                {
                        return NO;
                }else if (![password_Txt.text isEqualToString:confirm_Password_Txt.text])
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Password does not match with confirm password."];
                        return NO;
                }
        }
        return YES;
}
-(void)viewWillDisappear:(BOOL)animated{
        [super viewWillDisappear:YES];
        [self.navigationController setNavigationBarHidden:YES];
}
- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}
@end
