//
//  Checkout.m
//  EB
//
//  Created by webwerks on 9/13/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "Checkout.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "PlaceOrder.h"
#import "CommonSettings.h"
#import "Validation.h"
#import "KLCPopup.h"

#define MOBILE_NO_MAX 10

static NSString *cod_code = @"cashondelivery";
static NSString *cod_disabled_code = @"cashondelivery_disabled";

static NSString *advance_payment_code = @"custompayment";
static NSString *creditdays_code = @"banktransfer";
static NSString *paytm_code = @"paytm_cc";
static NSString *netbanking_code = @"billdesk_ccdc_net";
static NSString *payU_code = @"payubiz";
static NSString *order_replacement_code = @"replace";

@interface Checkout ()<FinishLoadingData,UIActionSheetDelegate,UITextFieldDelegate>
{
    NSString *selectedPayment;
    int totalAmount;
    int selectedCreditDays;
    NSArray *creditDaysDetailArray;
    KLCPopup *popup1;
    NSString *ebPin;
}

@property (weak, nonatomic) IBOutlet UIButton *codBtn;
@property (weak, nonatomic) IBOutlet UIButton *advancedPaymentBtn;
@property (weak, nonatomic) IBOutlet UIButton *paytmBtn;
@property (weak, nonatomic) IBOutlet UIButton *creditDaysBtn;
@property (weak, nonatomic) IBOutlet UIButton *netBankingBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditDaysHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paytmDescriptionHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *UTRHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *UTRTextFieldHtConstarint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *advancePaymentViewHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *codHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *netBankingBtnHtConstraint;
@property (weak, nonatomic) IBOutlet UITextField *bankNameTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paytmBtnHtConstraint;
@property (weak, nonatomic) IBOutlet UITextField *utrNoTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *advancePaymentHtConstraint;
@end

@implementation Checkout
@synthesize userMobileNo,isAffiliateUser,userEmailId;
#pragma mark - UIViewController Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    ebPinAlertShowStatus = NO;
    [self initializeECODAlertView];
    [super setViewControllerTitle:@"Payment Options"];
    [self getAvailablePaymentRequest];
    //    [emi_Payment_Btn setBackgroundImage:[UIImage imageNamed:@"credit-card-yellow.png"] forState:UIControlStateSelected];
    //    [cash_On_Delivery_Btn setBackgroundImage:[UIImage imageNamed:@"cash-on_delev_yellow.png"] forState:UIControlStateSelected];
    selectedCreditDays=0;
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.proceedToYourOrderBtn];
    cust_Mob_No_Txt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
    if (isAffiliateUser)
    {
        //        affUserDetailBgView.hidden=NO;
        ////        affUserDetailsBgViewHeight.constant=175;
        //        affUserDetailsBgViewHeight.constant=145;
        //
        //        scrollView_Bg_Height.constant=175;
        _appDiscountlbl.text= APP_DISCOUNT_LABEL;
        
    }
    
    [[AppDelegate getAppDelegateObj].tabBarObj hideTabbar];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self callWSForShowEBPinAlert];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - initialization Methods
-(void) initializeECODAlertView
{
    popup1 = [[KLCPopup alloc]init];
    self.popUpScrollView.hidden = YES;
    self.popUpScrollView.userInteractionEnabled = YES;
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.buttonCancel];
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.buttonSubmit];
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.buttonResendEBPin];
}
#pragma mark - Webservice delegate

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:@"AVAILABLEPAYMENT"])
    {
        if([response valueForKey:@"status"])
        {
            NSString *feedback_status = [response valueForKey:@"feedback_status"];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:feedback_status forKey:@"feedBackViewStatus"];
            [defaults synchronize];
            
            totalAmount=[[[response valueForKey:@"summary"]valueForKey:@"grand_total"]intValue];
            //total_Lbl.text=[NSString stringWithFormat:@"Rs.%@",[[response valueForKey:@"summary"]valueForKey:@"grand_total"]];
            
            NSString *priceStr = [[CommonSettings sharedInstance] formatPrice:[[[response valueForKey:@"summary"]valueForKey:@"grand_total"]floatValue]];
            total_Lbl.text = priceStr;
            
            NSArray *availablePaymentArray = [response valueForKey:@"payment"];
            NSArray *arr  = [availablePaymentArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(code == %@)",cod_code]];
            if(arr.count != 0)
            {
                _codBtn.hidden = NO;
                _codHtConstraint.constant = 25;
                [_codBtn setTitle:[[arr objectAtIndex:0] valueForKey:@"title"] forState:UIControlStateNormal];
                _codHtConstraint.constant = [[CommonSettings sharedInstance] calculateBtnHeight:_codBtn] + 10;
                _codBtn.enabled = YES;
            }
            else
            {
                _codBtn.hidden = YES;
                _codHtConstraint.constant = 0;
            }
            
            arr  = [availablePaymentArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(code == %@)",cod_disabled_code]];
            if(arr.count != 0)
            {
                _codBtn.hidden = NO;
                _codBtn.enabled = NO;
                
                [_codBtn setTitle:[[arr objectAtIndex:0] valueForKey:@"title"] forState:UIControlStateNormal];
                [_codBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [_codBtn setImage:[UIImage imageNamed:@"empty_circle"] forState:UIControlStateNormal];
                _codHtConstraint.constant = [[CommonSettings sharedInstance] calculateBtnHeight:_codBtn] +15;
                
            }
            
            
            
            arr  = [availablePaymentArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(code == %@)",advance_payment_code]];
            
            if(arr.count != 0)
            {
                _advancedPaymentBtn.hidden = NO;
                _advancePaymentHtConstraint.constant = 25;
                [_advancedPaymentBtn setTitle:[[arr objectAtIndex:0] valueForKey:@"title"] forState:UIControlStateNormal];
            }
            else
            {
                _advancedPaymentBtn.hidden = YES;
                _advancePaymentHtConstraint.constant = 0;
            }
            
            
            
            arr  = [availablePaymentArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(code == %@)",creditdays_code]];
            
            
            if(arr.count != 0) {
                
                //                    if([[[arr objectAtIndex:0] valueForKey:@"credit_limit"] length] > 0 && ![[[arr objectAtIndex:0] valueForKey:@"credit_limit"] isKindOfClass:[NSNull class]])
                NSDictionary *creditLimitDic ;
                for (NSDictionary *dic in arr) {
                    if([dic valueForKey:@"credit_limit"])
                    {
                        creditLimitDic = dic;
                        break;
                    }
                }
                
                
                if([[creditLimitDic valueForKey:@"credit_limit"] length] > 0 )
                {
                    [_creditLimitBtn setTitle:[creditLimitDic valueForKey:@"credit_limit"] forState:UIControlStateNormal];
                    _creditLimitBtnHeightConstraint.constant =  [[CommonSettings sharedInstance] calculateBtnHeight:_creditLimitBtn] + 15;
                    
                    _creditDaysHeightConstraint.constant = 0;
                    _creditDaysBtn.hidden = YES;
                    //_creditLimitBtnBottomConstraint.constant = 0;
                    _creditLimitBtnTopConstraint.constant = 0;
                    
                    [_creditLimitBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                    [_creditLimitBtn setImage:[UIImage imageNamed:@"empty_circle"] forState:UIControlStateNormal];
                    
                }
                else
                {
                    for (int i = 0; i < arr.count; i++) {
                        
                        NSDictionary *dict = [arr objectAtIndex:i];
                        if ([dict valueForKey:@"op_values"]) {
                            creditDaysDetailArray=[[NSArray alloc]initWithArray:[dict valueForKey:@"op_values"]];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if(creditDaysDetailArray.count != 0)
                                {
                                    _creditDaysHeightConstraint.constant = 25;
                                    _creditDaysBtn.hidden = NO;
                                    
                                    [_creditDaysBtn setTitle:[dict valueForKey:@"title"] forState:UIControlStateNormal];
                                }
                                else
                                {
                                    _creditDaysHeightConstraint.constant = 0;
                                    _creditDaysBtn.hidden = YES;
                                }
                                
                            });
                        }
                    }
                }
            }
            
            
            
            arr  = [availablePaymentArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(code == %@)",paytm_code]];
            
            if(arr.count != 0)
            {
                _paytmBtn.hidden = NO;
                _paytmBtnHtConstraint.constant = 25;
                [_paytmBtn setTitle:[[arr objectAtIndex:0] valueForKey:@"title"] forState:UIControlStateNormal];
            }
            else
            {
                _paytmBtn.hidden = YES;
                _paytmBtnHtConstraint.constant = 0;
            }
            
            arr  = [availablePaymentArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(code == %@)",payU_code]];
            
            if(arr.count != 0)
            {
                _payUBtn.hidden = NO;
                _payUBtnHtConstraint.constant = 25;
                [_payUBtn setTitle:[[arr objectAtIndex:0] valueForKey:@"title"] forState:UIControlStateNormal];
            }
            else
            {
                _payUBtn.hidden = YES;
                _payUBtnHtConstraint.constant = 0;
            }
            
            
            arr  = [availablePaymentArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(code == %@)",netbanking_code]];
            
            if(arr.count != 0)
            {
                _netBankingBtn.hidden = NO;
                _netBankingBtnHtConstraint.constant = 25;
                [_netBankingBtn setTitle:[[arr objectAtIndex:0] valueForKey:@"title"] forState:UIControlStateNormal];
            }
            else
            {
                _netBankingBtn.hidden = YES;
                _netBankingBtnHtConstraint.constant = 0;
            }
            
            arr  = [availablePaymentArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(code == %@)",order_replacement_code]];
            
            if(arr.count != 0)
            {
                _orderReplacementBtnHeightConstraint.constant = 25;
                [_orderReplacementBtn setTitle:[[arr objectAtIndex:0] valueForKey:@"title"] forState:UIControlStateNormal];
            }
            else
            {
                _orderReplacementBtnHeightConstraint.constant = 0;
            }
            
            
            
        }
        else
            [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
    }
    else if ([strName isEqualToString:@"SELECTEDPAYMENTMETHOD"]){
        
        if([[response valueForKey:@"status"] intValue] == 1)
        {
            productListArr=[[response valueForKey:@"data"]valueForKey:@"items"];
            AffiliateUser *user=[AffiliateUser new];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSDictionary *userDic = [defaults valueForKey:@"user"];
            user.name= [NSString stringWithFormat:@"%@ %@",[userDic valueForKey:@"firstname"],[userDic valueForKey:@"lastname"]];
            user.emailId=[userDic valueForKey:@"email"];
            //        user.repCode=Rep_Code_Txt.text;
            //            user.repCode=@"12345";
            user.repCode = [defaults valueForKey:REPCODE];
            user.mobNo=[userDic valueForKey:@"mobile"];
            
            PlaceOrder *objPlaceOrder = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PlaceOrder"];
            objPlaceOrder.deliveryDateStr  =[[response valueForKey:@"data"]valueForKey:@"approx_delivery"];
            objPlaceOrder.selectedPaymentOption=selectedPayment;
            objPlaceOrder.bankName = _bankNameTextField.text;
            objPlaceOrder.chequeUTR = _utrNoTextField.text;
            NSString *deliveryStatus = [[response valueForKey:@"data"]valueForKey:@"delivery_status"];
            objPlaceOrder.delivery_status = [NSString stringWithFormat:@"%@", deliveryStatus];
            objPlaceOrder.approx_delivery = [[response valueForKey:@"data"]valueForKey:@"approx_delivery"];
            if ([[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"advance_amount"]integerValue]>0) {
                objPlaceOrder.isAdvanceAmount=YES;
                int advanceAmount=[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"advance_amount"]floatValue];
                objPlaceOrder.advanceAmount=advanceAmount;
            }
            objPlaceOrder.totalAmount=totalAmount;
            objPlaceOrder.userMobile=userMobileNo;
            objPlaceOrder.userEmail=userEmailId;
            objPlaceOrder.productListArray=productListArr;
            objPlaceOrder.isAffiliateUser=isAffiliateUser;
            objPlaceOrder.selectedCreditDays=[NSString stringWithFormat:@"%d",selectedCreditDays];
            objPlaceOrder.productSummaryDic = [[response valueForKey:@"data"]valueForKey:@"summary"];
            objPlaceOrder.objAffUser=user;
            objPlaceOrder.pastOrderId = _pastOrderIdTextField.text;
            [self.navigationController pushViewController:objPlaceOrder animated:YES];
        }
        else
            [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
    }
    else if ([strName isEqualToString:GENERATE_SEND_ORDER_EB_PIN_WS])
    {
        if ([[response valueForKey:@"status"]intValue] == 1 || [response valueForKey:@"status"])
        {
            [APP_DELEGATE decrementEBCount];
            ebPin = [response valueForKey:@"ebpin"];
            [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
            //[self showCODAlert];
        }
        else
        {
            //[[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
    }
    else if ([strName isEqualToString:SHOW_EB_PIN_ALERT_WS])
    {
        if ([[response valueForKey:@"status"]intValue] == 1)
        {
            ebPinAlertShowStatus = YES;
        }
        else
        {
            ebPinAlertShowStatus = NO;
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
    }
}

#pragma mark - textField delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(textField == cust_Mob_No_Txt)
        {
            if(textField.text.length < MOBILE_NO_MAX)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
        }
        
    });
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if(textField == cust_Mob_No_Txt)
    {
        NSUInteger newLength = [textField.text length] + [string length];
        
        if(newLength > MOBILE_NO_MAX){
            
            return NO;
        }
        else{
            return YES;
        }
    }
    return YES;
}
#pragma mark - UIButton Action

-(void)doneButtonDidPressed:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [cust_Mob_No_Txt resignFirstResponder];
    });
}
-(void) okButtonPressed
{
    
}

-(void) cancelButtonPressed
{
    
}
- (IBAction)onClickOfProceed_To_Order:(id)sender
{
    [self.view endEditing:YES];
    if (selectedPayment.length != 0)
    {
        if([selectedPayment isEqualToString:@"cashondelivery"])
        {
            if (ebPinAlertShowStatus)
            {
                //call Generate EB Pin Webservice
                if ([APP_DELEGATE getEBCount] > 0 )
                {
                    [self generateEBPin];
                    [self showCODAlert];
                }
                else
                {
                    [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"You have exceeded max number of tries"];
                }
            }
            else
            {
                //Without EB pin verification
                [self buttonProceedRegularFlow];
            }
        }
        else
        {
            [self buttonProceedRegularFlow];
        }
    }
    else
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please select payment method"];
    }
}

-(void) showCODAlert
{
    [self.popUpScrollView setHidden:NO];
    
    popup1 = [KLCPopup popupWithContentView: self.popUpScrollView
                                   showType:KLCPopupShowTypeSlideInFromTop
                                dismissType:KLCPopupDismissTypeSlideOutToBottom
                                   maskType:KLCPopupMaskTypeDimmed
                   dismissOnBackgroundTouch:NO
                      dismissOnContentTouch:NO];
    self.ebPinTextField.text = @"";
    [popup1 show];
}
-(void) buttonProceedRegularFlow
{
    if (isAffiliateUser)
    {
        if ([self validateTextfields])
        {
            if([selectedPayment isEqualToString:@"custompayment"])
            {
                if([self validateAdvancePaymentTextField])
                    [self setSelectedPaymentMethod];
            }
            else if([selectedPayment isEqualToString:order_replacement_code])
            {
                if([self validateReplaceOrderTextField])
                    [self setSelectedPaymentMethod];
            }
            else
                [self setSelectedPaymentMethod];
        }
    }
    else
    {
        if([selectedPayment isEqualToString:@"custompayment"])
        {
            if([self validateAdvancePaymentTextField])
                [self setSelectedPaymentMethod];
        }
        else if([selectedPayment isEqualToString:order_replacement_code])
        {
            if([self validateReplaceOrderTextField])
                [self setSelectedPaymentMethod];
        }
        else
            [self setSelectedPaymentMethod];
    }
}

- (IBAction)selectADaysButtonPressed:(id)sender {
    [self.view endEditing:YES];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles: nil];
    
    
    
    for (int i = 0; i < [creditDaysDetailArray count]; i++) {
        [actionSheet addButtonWithTitle: [[creditDaysDetailArray objectAtIndex:i]valueForKey:@"label"]];
    }
    
    [actionSheet showInView:self.view];
    
}



-(IBAction)onclickOf_Paytm:(id)sender
{
    selectedPayment=@"paytm_cc";
}

- (IBAction)codBtnClicked:(id)sender {
    [sender setSelected:YES];
    [_advancedPaymentBtn setSelected:NO];
    [_netBankingBtn setSelected:NO];
    [_creditDaysBtn setSelected:NO];
    [_paytmBtn setSelected:NO];
    [_payUBtn setSelected:NO];
    [_orderReplacementBtn setSelected:NO];
    
    
    _advancePaymentViewHtConstraint.constant = 0;
    _UTRHtConstraint.constant = 0;
    _UTRTextFieldHtConstarint.constant = 0;
    selectPaymentViewHeightConstraints.constant = 0;
    _paytmDescriptionHeightConstraint.constant = 0;
    _orderReplacementViewHeightConstraint.constant = 0;
    
    selectedPayment=@"cashondelivery";
    //[self callWSForShowEBPinAlert];
}

- (IBAction)advancePaymentBtnClicked:(id)sender {
    ebPinAlertShowStatus = NO;
    [sender setSelected:YES];
    [_codBtn setSelected:NO];
    [_netBankingBtn setSelected:NO];
    [_creditDaysBtn setSelected:NO];
    [_paytmBtn setSelected:NO];
    [_payUBtn setSelected:NO];
    [_orderReplacementBtn setSelected:NO];
    
    
    // _advancePaymentViewHtConstraint.constant = 153;
    _advancePaymentViewHtConstraint.constant = 102;
    _UTRHtConstraint.constant = 21;
    _UTRTextFieldHtConstarint.constant = 30;
    
    selectPaymentViewHeightConstraints.constant = 0;
    _paytmDescriptionHeightConstraint.constant = 0;
    selectedPayment = @"custompayment";
    _orderReplacementViewHeightConstraint.constant = 0;
    
}
- (IBAction)netBankingBtnClicked:(id)sender {
    ebPinAlertShowStatus = NO;
    [sender setSelected:YES];
    [_advancedPaymentBtn setSelected:NO];
    [_codBtn setSelected:NO];
    [_creditDaysBtn setSelected:NO];
    [_paytmBtn setSelected:NO];
    [_payUBtn setSelected:NO];
    [_orderReplacementBtn setSelected:NO];
    
    selectedPayment=@"billdesk_ccdc_net";
    
    _advancePaymentViewHtConstraint.constant = 0;
    _UTRHtConstraint.constant = 0;
    _UTRTextFieldHtConstarint.constant = 0;
    selectPaymentViewHeightConstraints.constant = 0;
    _paytmDescriptionHeightConstraint.constant = 0;
    _orderReplacementViewHeightConstraint.constant = 0;
    
}
- (IBAction)creditDaysBtnClicked:(id)sender {
    ebPinAlertShowStatus = NO;
    [sender setSelected:YES];
    [_advancedPaymentBtn setSelected:NO];
    [_netBankingBtn setSelected:NO];
    [_codBtn setSelected:NO];
    [_paytmBtn setSelected:NO];
    [_payUBtn setSelected:NO];
    [_orderReplacementBtn setSelected:NO];
    
    selectPaymentViewHeightConstraints.constant = 60;
    selectedPayment=@"banktransfer";
    _advancePaymentViewHtConstraint.constant = 0;
    _UTRHtConstraint.constant = 0;
    _UTRTextFieldHtConstarint.constant = 0;
    
    _paytmDescriptionHeightConstraint.constant = 0;
    _orderReplacementViewHeightConstraint.constant = 0;
    
    if(creditDaysDetailArray.count == 1)
    {
        selectedCreditDays = [[[creditDaysDetailArray objectAtIndex:0]objectForKey:@"value"]intValue];
        [_payMentDaysBtn setTitle:[[creditDaysDetailArray objectAtIndex:0]objectForKey:@"label"] forState:UIControlStateNormal];
    }
    
}


- (IBAction)payUBtnClicked:(id)sender {
    ebPinAlertShowStatus = NO;
    [sender setSelected:YES];
    [_advancedPaymentBtn setSelected:NO];
    [_netBankingBtn setSelected:NO];
    [_creditDaysBtn setSelected:NO];
    [_paytmBtn setSelected:NO];
    [_codBtn setSelected:NO];
    [_orderReplacementBtn setSelected:NO];
    
    selectedPayment=payU_code;
    
    
    _paytmDescriptionHeightConstraint.constant = 0;
    _advancePaymentViewHtConstraint.constant = 0;
    _UTRHtConstraint.constant = 0;
    _UTRTextFieldHtConstarint.constant = 0;
    selectPaymentViewHeightConstraints.constant = 0;
    _orderReplacementViewHeightConstraint.constant = 0;
    
}


- (IBAction)paytmWalletBtnClicked:(id)sender {
    ebPinAlertShowStatus = NO;
    [sender setSelected:YES];
    [_advancedPaymentBtn setSelected:NO];
    [_netBankingBtn setSelected:NO];
    [_creditDaysBtn setSelected:NO];
    [_codBtn setSelected:NO];
    [_payUBtn setSelected:NO];
    
    [_orderReplacementBtn setSelected:NO];
    
    _paytmDescriptionHeightConstraint.constant = 60;
    selectedPayment=@"paytm_cc";
    _advancePaymentViewHtConstraint.constant = 0;
    _UTRHtConstraint.constant = 0;
    _UTRTextFieldHtConstarint.constant = 0;
    selectPaymentViewHeightConstraints.constant = 0;
    _orderReplacementViewHeightConstraint.constant = 0;
    
}
- (IBAction)orderReplacementBtnClicked:(UIButton *)sender {
    ebPinAlertShowStatus = NO;
    [sender setSelected:YES];
    [_advancedPaymentBtn setSelected:NO];
    [_netBankingBtn setSelected:NO];
    [_creditDaysBtn setSelected:NO];
    [_codBtn setSelected:NO];
    [_payUBtn setSelected:NO];
    [_paytmBtn setSelected:NO];
    
    selectedPayment=order_replacement_code;
    
    _paytmDescriptionHeightConstraint.constant = 0;
    _advancePaymentViewHtConstraint.constant = 0;
    _UTRHtConstraint.constant = 0;
    _UTRTextFieldHtConstarint.constant = 0;
    selectPaymentViewHeightConstraints.constant = 0;
    _orderReplacementViewHeightConstraint.constant = 80;
    
}
#pragma mark - EB Pin Alert button Methods
- (IBAction)buttonCancelAction:(id)sender {
    [self.popUpScrollView setHidden:YES];
    [self.popUpScrollView dismissPresentingPopup];
}
- (IBAction)buttonSubmitAction:(id)sender {
    [self.view endEditing:YES];
    if (self.ebPinTextField.text.length != 0)
    {
        if([self.ebPinTextField.text isEqualToString:ebPin])
        {
            ebPin = @"";
            [self.popUpScrollView setHidden:YES];
            [self.popUpScrollView dismissPresentingPopup];
            [self buttonProceedRegularFlow];
        }
        else
        {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"You have entered wrong EB Pin"];
        }
    }
    else
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter EB Pin"];
    }
}

- (IBAction)buttonResendAction:(id)sender {
    [self generateEBPin];
}


#pragma mark - webservice call
-(void) callWSForShowEBPinAlert
{
    Webservice  *callWSForShowEBPinStatus = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        NSString *url = [NSString stringWithFormat:@"%@",SHOW_EB_PIN_ALERT_WS];
        callWSForShowEBPinStatus.delegate =self;
        [callWSForShowEBPinStatus GetWebServiceWithURL:url MathodName:SHOW_EB_PIN_ALERT_WS];
    }
}

-(void)generateEBPin
{
    if ([APP_DELEGATE getEBCount] > 0 )
    {
        NSLog(@"EB count : %ld", [APP_DELEGATE getEBCount]);
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
            [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] valueForKey:@"user"];
            NSString *url = [NSString stringWithFormat:@"%@mobile=%@&email=%@&store_name=%@",GENERATE_SEND_ORDER_EB_PIN_WS,[userDict valueForKey:@"mobile"],[userDict valueForKey:@"email"],[userDict valueForKey:@"firstname"] ];
            callLoginService.delegate =self;
            [callLoginService GetWebServiceWithURL:url MathodName:GENERATE_SEND_ORDER_EB_PIN_WS];
        }
    }
    else
    {
        NSLog(@"EB count : %ld", [APP_DELEGATE getEBCount]);
        [self.popUpScrollView setHidden:YES];
        [self.popUpScrollView dismissPresentingPopup];
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"You have exceeded max number of tries"];
    }
}
-(void)setSelectedPaymentMethod
{
    Webservice  *getCartInfoService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        getCartInfoService.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        if (!(quoteID==0)) {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            NSString *strQuoteId=[NSString stringWithFormat:@"quote_id=%d&payment_method=%@&userid=%d",quoteID,selectedPayment,[CommonSettings getUserID]];
            
            if([selectedPayment isEqualToString:order_replacement_code])
            {
                strQuoteId = [NSString stringWithFormat:@"%@&past_order=%@",strQuoteId,_pastOrderIdTextField.text];
            }
            
            [getCartInfoService webServiceWitParameters:strQuoteId andURL:SET_SELECTED_PAYMENT_METHOD MathodName:@"SELECTEDPAYMENTMETHOD"];
        }
    }
    
}

-(void)getAvailablePaymentRequest
{
    Webservice  *getCartInfoService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        getCartInfoService.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        if (!(quoteID==0)) {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            //                        NSString *strQuoteId=[NSString stringWithFormat:@"quote_id=%d",quoteID];
            //                        [getCartInfoService webServiceWitParameters:strQuoteId andURL:AVAILABLE_PAYMENT_WS
            //                                                         MathodName:@"AVAILABLEPAYMENT"];
            
            
            NSString *strQuoteId=[NSString stringWithFormat:@"quote_id=%d",quoteID];
            NSString *url = [NSString stringWithFormat:@"%@%@",AVAILABLE_PAYMENT_WS,strQuoteId];
            
            [getCartInfoService GetWebServiceWithURL:url MathodName:@"AVAILABLEPAYMENT"];
            // [getCartInfoService webServiceWitParameters:strQuoteId andURL:AVAILABLE_PAYMENT_WS
            //MathodName:@"AVAILABLEPAYMENT"];
        }
    }
    
}


#pragma mark - Utility Methods

-(BOOL)validateTextfields
{
    //    if (![Validation required:cust_Name_Txt withCaption:@"Customer Name"])
    //        return NO;
    ////    if (![Validation required:cust_Mob_No_Txt withCaption:@"Mobile No"])
    ////        return NO;
    //    if (![Validation ValidEmail:cust_Email_Id_Txt.text])
    //    {
    //        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Enter valid email id"];
    //        return NO;
    //    }
    //
    //    if(![Validation checkMobileNumber:cust_Mob_No_Txt])
    //        return NO;
    if (isAffiliateUser)
    {
        //        if (![Validation required:Rep_Code_Txt withCaption:@"Rep code"])
        //            return NO;
        
        if ([selectedPayment isEqualToString:@"banktransfer"]) {
            //                        if ([paymentDaysTxt.text isEqualToString:@""]) {
            if([[_payMentDaysBtn titleForState:UIControlStateNormal] isEqualToString:@""])
            {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please select credit days."];
                return NO;
            }
            
        }
    }
    return YES;
}


-(BOOL)validateReplaceOrderTextField
{
    if (![Validation required:_pastOrderIdTextField withCaption:@"Past Order ID"])
        return NO;
    
    return YES;
}
-(BOOL)validateAdvancePaymentTextField
{
    if (![Validation required:_utrNoTextField withCaption:@"Cheque UTR No# / Cheque No#"])
        return NO;
    if (![Validation required:_bankNameTextField withCaption:@"Bank name"])
        return NO;
    
    return YES;
}
- (IBAction)leftButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightSlideMenuAction:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}


#pragma mark - UIActionsheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    //NSLog(@"%d",buttonIndex);
    
    if (buttonIndex==0)
    {
        return;
    }
    
    if(creditDaysDetailArray.count > 1)
    {
        if ((buttonIndex-1) == 0) {
            [_payMentDaysBtn setTitle:@"" forState:UIControlStateNormal];
            return;
        }
    }
    selectedCreditDays = [[[creditDaysDetailArray objectAtIndex:buttonIndex-1]objectForKey:@"value"]intValue];
    [_payMentDaysBtn setTitle:[[creditDaysDetailArray objectAtIndex:buttonIndex-1]objectForKey:@"label"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
