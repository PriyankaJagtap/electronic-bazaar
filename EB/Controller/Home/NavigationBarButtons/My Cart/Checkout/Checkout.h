//
//  Checkout.h
//  EB
//
//  Created by webwerks on 9/13/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextFieldWithBorder.h"
#import "TPKeyboardAvoidingScrollView.h"
@interface Checkout : BaseNavigationControllerWithBackBtn{
    
    __weak IBOutlet UILabel *total_Lbl;
    __weak IBOutlet UIButton *cash_On_Delivery_Btn;
    __weak IBOutlet UIButton *emi_Payment_Btn;
    __weak IBOutlet UIButton *net_Banking_Btn;
    __weak IBOutlet UIButton *credit_days_Payment_Btn;

    __weak IBOutlet UITextField *cust_Name_Txt;
    __weak IBOutlet UITextField *cust_Email_Id_Txt;
    __weak IBOutlet UITextField *cust_Mob_No_Txt;
    __weak IBOutlet UITextField *Rep_Code_Txt;
    
    __weak IBOutlet NSLayoutConstraint *affUserDetailsBgViewHeight;
    __weak IBOutlet UIView *affUserDetailBgView;
    __weak IBOutlet NSLayoutConstraint *viewBgHeight;
    NSMutableArray *productListArr;
    __weak IBOutlet NSLayoutConstraint *scrollView_Bg_Height;
    
    __weak IBOutlet NSLayoutConstraint *creditDaysPaymentViewWidthConstraints;
    
    __weak IBOutlet NSLayoutConstraint *selectPaymentViewHeightConstraints;
   // __weak IBOutlet UITextField *paymentDaysTxt;
    BOOL ebPinAlertShowStatus;
    
}

//Pass to place order
@property(strong,nonatomic)NSString *userMobileNo;
@property(strong,nonatomic)NSString *userEmailId;
@property(nonatomic)BOOL isAffiliateUser;
@property (weak, nonatomic) IBOutlet UILabel *appDiscountlbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cashOnDeliverBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cashOnDeliverBtnWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cashOnLblHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliverLblheightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditPaymentCenterConstraint;

- (IBAction)leftButtonAction:(id)sender;
- (IBAction)rightSlideMenuAction:(id)sender;
- (IBAction)onClickOfProceed_To_Order:(id)sender;
- (IBAction)onClickOfCash_On_Delivery:(id)sender;
- (IBAction)onClickOfCreditPayment:(id)sender;

//- (IBAction)onClickOfEmi_Throught_CreditCard:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payUBtnBottomConstraint;

- (IBAction)selectADaysButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payUBtnHtConstraint;

- (IBAction)payUBtnClicked:(id)sender;

- (IBAction)onClickOf_NetBanking:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *payUBtn;
@property (weak, nonatomic) IBOutlet UIButton *creditLimitBtn;
@property (weak,nonatomic) IBOutlet UIButton *proceedToYourOrderBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditLimitBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditLimitBtnTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditLimitBtnBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderReplacementBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *orderReplacementBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderReplacementViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *pastOrderIdTextField;
@property (weak, nonatomic) IBOutlet UIButton *payMentDaysBtn;


//COD Alert View
@property (weak, nonatomic) IBOutlet UIView *CODAlertView;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *ebPinTextField;

@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;

@property (weak, nonatomic) IBOutlet UIButton *buttonSubmit;

@property (weak, nonatomic) IBOutlet UIButton *buttonResendEBPin;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *popUpScrollView;


@end
