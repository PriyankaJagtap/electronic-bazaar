//
//  MyCartViewController.h
//  EB
//
//  Created by webwerks on 8/21/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseNavigationController.h"

@interface MyCartViewController : BaseNavigationControllerWithBackBtn<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    NSMutableArray *productFromCartsArr;
    __weak IBOutlet UIButton *leftNavBarButton;
    __weak IBOutlet UIView *objBgView;
    __weak IBOutlet NSLayoutConstraint *bgViewTopSpacing;
    __weak IBOutlet UITextField *coupon_Txt;
    
    
}
@property (weak, nonatomic) IBOutlet UILabel *appDiscountlbl;

@property (strong, nonatomic) IBOutlet UITableView *objCartProductTableview;
@property (strong, nonatomic) IBOutlet UILabel *base_Price_Lbl;
@property (strong, nonatomic) IBOutlet UILabel *vat_Price_Lbl;
@property (strong, nonatomic) IBOutlet UILabel *total_Payble_Lbl;


- (IBAction)onClickOfProceed_To_Checkout:(id)sender;
- (IBAction)rightSlideMenuAction:(id)sender;
- (IBAction)onClickOfApply_Coupon:(id)sender;


@end
