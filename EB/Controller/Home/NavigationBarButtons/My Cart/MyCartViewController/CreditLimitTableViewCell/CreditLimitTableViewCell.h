//
//  CreditLimitTableViewCell.h
//  EB
//
//  Created by webwerks on 06/11/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditLimitTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *totalCreditLimitLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCreditLimitValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *usedCreditLabel;
@property (weak, nonatomic) IBOutlet UILabel *usedCreditValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceCreditLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceCreditValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditLimitLabel;

@end
