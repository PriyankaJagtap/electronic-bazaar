//
//  MyCartViewController.m
//  EB
//
//  Created by webwerks on 8/21/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//4585 46000 5447 827

#import "MyCartViewController.h"
#import "CartProductCell.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "CommonSettings.h"
#import "ProceedToPayment.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "RegistrationViewController.h"
#import "ProductSummaryTableViewCell.h"
#import "CreditLimitTableViewCell.h"

@interface MyCartViewController ()<FinishLoadingData>
{
        UITextField *currentTextField;
        int removedProductIndex;
        UIView *noProductBgView;
        NSDictionary *productDetailDic;
        BOOL updateCartBtnClickedBool;
}


@end

@implementation MyCartViewController
@synthesize objCartProductTableview,base_Price_Lbl,vat_Price_Lbl,total_Payble_Lbl;

- (void)viewDidLoad
{
        [super viewDidLoad];
        [super setViewControllerTitle:@"My Cart"];
        self.objCartProductTableview.contentInset = UIEdgeInsetsMake(-36, 0, -36, 0);
        
        //AffiliateStatus
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"AffiliateStatus"] integerValue] == 4)
        {
                _appDiscountlbl.text = APP_DISCOUNT_LABEL;
        }
        objCartProductTableview.rowHeight = UITableViewAutomaticDimension;
        objCartProductTableview.estimatedRowHeight = 100;
}

-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:YES];
        [super setIsCartScreenValue:1];
        [[[AppDelegate getAppDelegateObj]tabBarObj] unhideTabbar];
        [[[AppDelegate getAppDelegateObj]tabBarObj] TabClickedIndex:4];
}

-(void)leftBarButtonPressed
{
        [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)leftNavBtnClicked:(id)sender
{
        [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

-(void)backButtonAction
{
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
        [super viewDidAppear:YES];
        [CommonSettings sendScreenName:@"MyCartView"];
        
        [noProductBgView removeFromSuperview];
        [self getProductTotal];
}

-(void)viewWillDisappear:(BOOL)animated
{
        [super viewWillDisappear:YES];
        [super setIsCartScreenValue:0];
}

#pragma mark - UITableview DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        if (productDetailDic == nil)
                return 0;
        
        if([[productDetailDic valueForKey:@"data"]valueForKey:@"credit"] && ![[[productDetailDic valueForKey:@"data"]valueForKey:@"credit"] isKindOfClass:[NSNull class]])
        {
                return [productFromCartsArr count] + 2;
        }
        else
                return [productFromCartsArr count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSInteger numberOFIndexes =  [tableView numberOfRowsInSection:indexPath.section];
        
        if(indexPath.row == numberOFIndexes-1)
        {
                static NSString *cellIdentifier=@"ProductSummaryTableViewCell";
                ProductSummaryTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                [cell.checkOutBtn addTarget:self action:@selector(onClickOfProceed_To_Checkout) forControlEvents:UIControlEventTouchUpInside];
                
                NSDictionary *dic = [[productDetailDic valueForKey:@"data"]valueForKey:@"summary"];
                
                [[GradientColorClass sharedInstance] setGradientBackgroundToButton:cell.checkOutBtn];
                
                cell.subTotalLabel.text=[[[productDetailDic valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"subtotal_label"];
                cell.taxLabel.text=[[[productDetailDic valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"tax_label"];
                cell.grandTotalLabel.text=[[[productDetailDic valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"grandtotal_label"];
                
                cell.basePriceLabel.text=[[CommonSettings sharedInstance] formatPrice:[[dic valueForKey:@"subtotal"] floatValue]];
                cell.totalPayableLabel.text=[[CommonSettings sharedInstance] formatPrice:[[dic valueForKey:@"grand_total"] floatValue]];
                cell.vatLabel.text=[[CommonSettings sharedInstance] formatPrice:[[dic valueForKey:@"tax_amount"] floatValue]];
                
                if([dic valueForKey:@"discount_label"] && ![[dic valueForKey:@"discount_label"] isEqualToString:@""])
                {
                        cell.taxLabelTop.constant = 8;
                        cell.discountLabel.text=[dic valueForKey:@"discount_label"];
                        //                                cell.discountAmountLabel.text=[NSString stringWithFormat:@"- Rs %ld",(long)[[dic valueForKey:@"discount_amount"]integerValue]];
                        NSString *discount = [[CommonSettings sharedInstance] formatIntegerPrice:[[dic valueForKey:@"discount_amount"]integerValue]];
                        cell.discountAmountLabel.text = [NSString stringWithFormat:@"- %@",discount];
                }
                else
                {
                        cell.taxLabelTop.constant = 0;
                        cell.discountLabel.text=@"";
                        cell.discountAmountLabel.text=@"";
                }
                return cell;
        }
        
        if(indexPath.row != [productFromCartsArr count])
        {
                static NSString *cellIdentifier=@"CartProductCell";
                CartProductCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                NSString *priceStr = [[CommonSettings sharedInstance] formatPrice:[[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"price"]floatValue]];
            if ([priceStr isEqualToString:@"₹ 0"])
            {
                cell.prodCountTxt.enabled = NO;
                cell.forwordArrowBtn.enabled = NO;
                cell.buttonClose.hidden = YES;
            }
            else
            {
                cell.prodCountTxt.enabled = YES;
                cell.forwordArrowBtn.enabled = YES;
                cell.buttonClose.hidden = NO;
            }
                cell.productPrice.text = priceStr;
                cell.prodDetail.text=[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"name"];
                [cell.productImage sd_setImageWithURL:[NSURL URLWithString:[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"thumbnail"]]];
                
                NSDictionary *dic = [productFromCartsArr objectAtIndex:indexPath.row];
                
                if([dic valueForKey:@"category_type_img"] && ![[dic valueForKey:@"category_type_img"] isEqualToString:@""])
                {
                        [cell.categoryTypeImgView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"category_type_img"]]];
                }
                else
                {
                        cell.categoryTypeImgView.image = nil;
                }
                cell.prodCountTxt.text=[NSString stringWithFormat:@"%ld",(long)[[[productFromCartsArr objectAtIndex:indexPath.row]valueForKey:@"qty"]integerValue]];
                cell.prodCountTxt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
                cell.prodCountTxt.tag = indexPath.row;

                //Set button tag
                cell.buttonClose.tag=cell.decreaseProdCountbtn.tag=cell.increaseProdCountbtn.tag=indexPath.row;
                cell.forwordArrowBtn.tag = indexPath.row;
                
                //Uitableviewcell button action
                [cell.buttonClose addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                //is_inStock
                NSString *is_InStock = [NSString stringWithFormat:@"%@",[[productFromCartsArr objectAtIndex:indexPath.row]objectForKey:@"is_instock"]];
                if ([is_InStock isEqualToString:@"0"])
                {
                        [cell.outOfStockImage setHidden:NO];
                        cell.itemLeftLabel.text = @"";
                }
                else
                {
                        [cell.outOfStockImage setHidden:YES];
                        NSInteger qty = [[[productFromCartsArr objectAtIndex:indexPath.row] valueForKey:@"qty_left"] integerValue];
                        NSInteger max_qty = [[[productFromCartsArr objectAtIndex:indexPath.row] valueForKey:@"max_qty"] integerValue];
                        if (qty > max_qty)
                        {
                                qty = max_qty;
                        }
                        
                        if( qty <= 10 && qty != 0)
                        {
                                cell.itemLeftLabel.text = [NSString stringWithFormat:@"Hurry, Only %ld left!",qty];
                        }
                        else
                        {
                                cell.itemLeftLabel.text = @"";
                        }
                }
                
                //    [cell.increaseProdCountbtn addTarget:self action:@selector(increaseProdPressed:) forControlEvents:UIControlEventTouchUpInside];
                //    [cell.decreaseProdCountbtn addTarget:self action:@selector(decreaseProdPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.forwordArrowBtn addTarget:self action:@selector(forwordArrowBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
        }
        
        else if(indexPath.row == [productFromCartsArr count] &&[[productDetailDic valueForKey:@"data"]valueForKey:@"credit"] && ![[[productDetailDic valueForKey:@"data"]valueForKey:@"credit"] isKindOfClass:[NSNull class]])
                
        {
                static NSString *cellIdentifier=@"CreditLimitTableViewCell";
                CreditLimitTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                NSDictionary *dic = [[productDetailDic valueForKey:@"data"]valueForKey:@"credit"];
                
                cell.creditLimitLabel.text = [dic valueForKey:@"message"];
                if([[dic valueForKey:@"bounce"] integerValue] == 1)
                {
                        cell.creditLimitLabel.textColor = [UIColor redColor];
                }
                else
                        cell.creditLimitLabel.textColor = [UIColor blackColor];
                
                cell.totalCreditLimitLabel.text = [dic valueForKey:@"total_label"];
                
                cell.totalCreditLimitValueLabel.text =[[CommonSettings sharedInstance] formatPrice:[[dic valueForKey:@"total_credit"] floatValue]];
                cell.balanceCreditLabel.text = [dic valueForKey:@"balance_label"];
                cell.balanceCreditValueLabel.text = [[CommonSettings sharedInstance] formatPrice:[[dic valueForKey:@"balance_credit"] floatValue]];
                cell.usedCreditLabel.text = [dic valueForKey:@"utilized_label"];
                cell.usedCreditValueLabel.text = [[CommonSettings sharedInstance] formatPrice:[[dic valueForKey:@"utilized_credit"] floatValue]];
                
                return cell;
        }
        else
                return nil;
}

-(void)doneButtonDidPressed:(id)sender
{
        dispatch_async(dispatch_get_main_queue(), ^{
                [self.view endEditing:YES];
        });
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
        [self.view endEditing:YES];
}

#pragma mark - Cart info webservice
-(void)getProductTotal
{
    Webservice  *getCartInfoService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        getCartInfoService.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        if (quoteID!=0)
        {
            if(!updateCartBtnClickedBool)
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            NSDictionary *dictMycart=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:quoteID],@"QuoteId", nil];
            [getCartInfoService operationRequestToApi:dictMycart url:GET_CART_INFO_WS string:@"GETCARTINFO"];
        }
        else
        {
            [self noProductAvailableView];
        }
    }
}

#pragma mark - Remove product from cart webservice
-(void)closeButtonPressed:(UIButton*)btn
{
    removedProductIndex=(int)btn.tag;
    Webservice  *updateCartWsCall = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        updateCartWsCall.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        int prodID=[[[productFromCartsArr objectAtIndex:btn.tag]valueForKey:@"product_id"]intValue];
        
        if (quoteID != 0)
        {
            updateCartBtnClickedBool = YES;
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            
            [updateCartWsCall webServiceWitParameters:[NSString stringWithFormat:@"quoteId=%d&product_id=%d",quoteID,prodID] andURL:REMOVE_FROM_CART MathodName:@"REMOVEFROMCART"];
        }
        else
        {
            [self noProductAvailableView];
        }
    }
}

#pragma mark - Update to cart webservice
-(void)forwordArrowBtnClicked:(UIButton*)btn
{
        CGPoint buttonPosition = [btn convertPoint:CGPointZero toView:objCartProductTableview];
        NSIndexPath *indexPath = [objCartProductTableview indexPathForRowAtPoint:buttonPosition];
        CartProductCell *Cell = (CartProductCell*)[objCartProductTableview cellForRowAtIndexPath:indexPath];
        
        if([Cell.prodCountTxt.text isEqualToString:@""])
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter quantity" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                return;
        }
        if ([Cell.prodCountTxt.text intValue] == 0)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Quantity cannot be zero" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                return;
        }
        
        [self.view endEditing:YES];
        currentTextField = Cell.prodCountTxt;
        
        Webservice  *updateCartWsCall = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                updateCartWsCall.delegate =self;
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                int qty=[Cell.prodCountTxt.text intValue];
                int prodID=[[[productFromCartsArr objectAtIndex:btn.tag]valueForKey:@"product_id"]intValue];
                
                if (quoteID != 0)
                {
                        updateCartBtnClickedBool = YES;
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        
                        [updateCartWsCall webServiceWitParameters:[NSString stringWithFormat:@"quoteId=%d&product_id=%d&qty=%d",quoteID,prodID,qty] andURL:UPDATETOCART MathodName:@"UPDATECART"];
                }
                else
                {
                        [self noProductAvailableView];
                }
        }
}

#pragma mark - Webservice Delegate
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:@"UPDATECART"])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        currentTextField.text=[response valueForKey:@"qty"];
                        //[CommonSettings setMyCartcount:[response valueForKey:@"cart_total_qty"]];
                        
                        [MyCartClass setMyCartcount:[NSNumber numberWithInteger:[[response valueForKey:@"cart_total_qty"] integerValue]]];
                        [super showCartValue];
                        [self getProductTotal];
                }
                else
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                        [self getProductTotal];
                        
                }
        }
        else if ([strName isEqualToString:@"REMOVEFROMCART"])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Product removed successfully"];
                        // [CommonSettings setMyCartcount:[response valueForKey:@"cart_total_qty"]];
                        
                        [MyCartClass setMyCartcount:[response valueForKey:@"cart_total_qty"]];
                        [super showCartValue];
                        [self getProductTotal];
                }
                else
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Failed to removed product"];
                }
        }
        else if ([strName isEqualToString:@"CUSTOMER_ADDRESS_LIST"])
        {
                NSLog(@"response= %@",response);
                if ([[response valueForKey:@"status"]intValue]==0)
                {
                        [[CommonSettings sharedInstance] showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                else
                {
                        ProceedToPayment  *objProceedToPayment= [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProceedToPayment"];
                        
                        //NSLog(@"Affiliate status %@",[response valueForKey:@"affiliate_status"]);
                        
                        if([NSNull null]==[response valueForKey:@"affiliate_status"] || [[response valueForKey:@"affiliate_status"]integerValue] == 0)
                        {
                                //                [[CommonSettings sharedInstance]showAlertTitle:@"" message:LOGIN_AFFILIATE_USER_MSG];
                                
                                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:LOGIN_AFFILIATE_USER_MSG delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                [alert show];
                                return;
                        }
                        
                        if ([NSNull null]==[response valueForKey:@"affiliate_status"])
                        {
                                objProceedToPayment.isAffiliateUser=0;
                                [self navigateToRegisteration];
                        }
                        else
                        {
                                objProceedToPayment.isAffiliateUser=[[response valueForKey:@"affiliate_status"]integerValue]?:0;
                                if ([NSNull null]==[response valueForKey:@"affiliate_vat_no"])
                                {
                                        objProceedToPayment.isCformUser=0;
                                }
                                else
                                {
                                        objProceedToPayment.isCformUser=1;
                                        objProceedToPayment.billingOptionsArr=[response valueForKey:@"billing_options"];
                                }
                        }
                        NSMutableDictionary *dict = [NSMutableDictionary new];
                        [dict setObject:[response valueForKey:@"gstin_label"] forKey:@"gstin_label"];
                        [dict setObject:[response valueForKey:@"gstin_sub_label"] forKey:@"gstin_sub_label"];
                        [dict setObject:[response valueForKey:@"partner_store_label"] forKey:@"partner_store_label"];
                        
                        if([response valueForKey:REPCODE] && ![[response valueForKey:REPCODE] isKindOfClass:[NSNull class]])
                        {
                                [[NSUserDefaults standardUserDefaults] setObject:[response valueForKey:REPCODE] forKey:REPCODE];
                        }
                        else
                        {
                                [[NSUserDefaults standardUserDefaults] setObject:@"12345" forKey:REPCODE];
                        }
                        objProceedToPayment.titleLabelDict=dict;
                        // electronicVC.isAffiliateUser=YES;
                        response=[response valueForKey:@"address_array"];
                        objProceedToPayment.addressArray=[response valueForKey:@"complete_address"];
                        NSMutableArray *addArray=[[NSMutableArray alloc]init];
                        NSMutableArray *detailAddArray=[[NSMutableArray alloc]init];
                        
                        for (int i=0; i<[[response valueForKey:@"address_array"]count]; i++)
                        {
                                //NSLog(@"re%@",[response objectAtIndex:i]);
//                                [addArray addObject:[[response objectAtIndex:i]valueForKey:@"complete_address"]];
//                                [detailAddArray addObject:[response objectAtIndex:i]];
                                if(![ [[response objectAtIndex:i] valueForKey:@"complete_address"] isKindOfClass:[NSNull class]])
                                {
                                        [addArray addObject:[[response objectAtIndex:i]valueForKey:@"complete_address"]];
                                        [detailAddArray addObject:[response objectAtIndex:i]];
                                }
                        }
                        objProceedToPayment.addressArray=addArray;
                        objProceedToPayment.detailAddressArray=detailAddArray;
                        [self.navigationController pushViewController:objProceedToPayment animated:YES];
                }
        }
        else if([strName isEqualToString:@"ADDCOUPONCODE"])
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]?:@""];
                [self getProductTotal];
        }
        else
        {
                updateCartBtnClickedBool = NO;
                if ([NSNull null]==[response valueForKey:@"data"])
                {
                        [self noProductAvailableView];
                }
                else
                {
                        productFromCartsArr=[[response valueForKey:@"data"]valueForKey:@"items"];
                        if (productFromCartsArr.count==0)
                        {
                                [self noProductAvailableView];
                        }
                        else
                        {
                                productDetailDic = response;
                                [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                                //total_Payble_Lbl.text=[NSString stringWithFormat:@"%ld(app discount .5%)",(long)[[[[response valueForKey:@"data"]valueForKey:@"summary"]valueForKey:@"grand_total"]integerValue]];
                                [objCartProductTableview reloadData];
                        }
                }
                //                [CommonSettings showMyCartCount];
                [super showCartValue];
        }
}

#pragma mark - Alert View]
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
        if (alertView.tag == 500 && buttonIndex == 0)
        {
                [self navigateToRegisteration];
        }
}

#pragma mark - Utility Methods
- (IBAction)rightSlideMenuAction:(id)sender
{
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (IBAction)onClickOfApply_Coupon:(id)sender
{
        [self.view endEditing:YES];
        if (![coupon_Txt.text isEqualToString:@""])
        {
                [self addCouponRequest];
        }
        else
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter coupon code."];
        }
}

-(void)addCouponRequest
{
        Webservice  *addCouponWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                addCouponWs.delegate =self;
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                if (quoteID!=0)
                {
                        updateCartBtnClickedBool = YES;
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        NSString *param=[NSString stringWithFormat:@"quote_id=%d&couponCode=%@",quoteID,coupon_Txt.text];
                        
                        [addCouponWs webServiceWitParameters:param andURL:ADD_COUPON_CODE MathodName:@"ADDCOUPONCODE"];
                }
        }
}

-(void)leftBarButtonItemPressed
{
        [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
        // [self.navigationController popViewControllerAnimated:YES];
}

-(void)noProductAvailableView
{
        noProductBgView=[[UIView alloc]initWithFrame:self.view.bounds];
        if(IS_IPHONE_X)
        {
                noProductBgView.frame=CGRectMake(0, 88, self.view.frame.size.width, self.view.frame.size.height-200);
        }
        else
                noProductBgView.frame=CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64);
        
        [noProductBgView setBackgroundColor:[UIColor whiteColor]];
        UILabel *lblNoProduct=[[UILabel alloc]initWithFrame:CGRectMake((noProductBgView.frame.size.width-240)/2,noProductBgView.frame.size.width/2, 240, 50)];
        lblNoProduct.text=@"No product available in the cart.";
        [lblNoProduct setFont:karlaFontRegular(16)];
        [self.view addSubview:noProductBgView];
        
        [noProductBgView addSubview:lblNoProduct];
}

-(void)rightBarButtonItemPressed
{
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}



-(void)proceedToCheckoutRequest
{
        Webservice  *getCustomerAddress = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                getCustomerAddress.delegate =self;
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                int userId=[[[defaults valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
                if (quoteID!=0)
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        NSString *param=[NSString stringWithFormat:@"userid=%d&quoteId=%d&version=%@",userId,quoteID,[CommonSettings getAppCurrentVersion]];
                        
                        [getCustomerAddress webServiceWitParameters:param andURL:PROCEED_TO_CHECKOUT_WS MathodName:@"CUSTOMER_ADDRESS_LIST"];
                }
        }
}

- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

- (void)onClickOfProceed_To_Checkout
{
        //    [self makePayUPayment];
        //    return;
        
        [CommonSettings addGoogleAnalyticsTrackEvent:@"shopping" label:@"checkout-cart-page"];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int userId=[[[defaults valueForKey:@"user"]valueForKey:@"user_id"]intValue];
        if (userId==0)
        {
                //        [[CommonSettings sharedInstance]showAlertTitle:@"" message:LOGIN_AFFILIATE_USER_MSG];
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:LOGIN_AFFILIATE_USER_MSG delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                alert.tag = 500;
                [alert show];
                return;
        }
        else
        {
                [self proceedToCheckoutRequest];
        }
}

#pragma mark UITextfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
        //objBgView.frame=CGRectMake(0,-256, self.view.frame.size.width, self.view.frame.size.height);
        //    if (isIpad) {
        //        bgViewTopSpacing.constant=200;
        //    }else{
        bgViewTopSpacing.constant=170;
        
        //}
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
        // objBgView.frame=CGRectMake(0,256, self.view.frame.size.width, self.view.frame.size.height);
        //    if(isIpad){
        //        bgViewTopSpacing.constant=80;
        //    }else{
        
        bgViewTopSpacing.constant=64;
        //}
        return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [textField resignFirstResponder];
        bgViewTopSpacing.constant=64;
        return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        NSInteger qty = [[[productFromCartsArr objectAtIndex:textField.tag] valueForKey:@"qty_left"] integerValue];
        NSInteger max_qty = [[[productFromCartsArr objectAtIndex:textField.tag] valueForKey:@"max_qty"] integerValue];
        if (qty > max_qty)
        {
                qty = max_qty;
        }
        NSString *qtyStr = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        if([qtyStr intValue] == 0)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Quantity cannot be zero" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                return NO;
        }
        
        if([qtyStr integerValue] > qty)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Product quantity can not be greater than the available quantity. The available product quantity is %ld",qty] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                return NO;
        }
        return YES;
}

#pragma mark - naviagte to register
-(void)navigateToRegisteration
{
        UIStoryboard *objStoryboard;
        //    if (isIpad){
        //        objStoryboard=[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        //    }else{
        objStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //}
        RegistrationViewController *objc = [objStoryboard instantiateViewControllerWithIdentifier:@"RegistrationViewController"];
        objc.isFromMycartView = YES;
        objc.userdict = [[NSUserDefaults standardUserDefaults] valueForKey:@"user"];
        
        // Clear document directory
        NSString *pushID=[[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_TOKEN"];
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:pushID forKey:@"DEVICE_TOKEN"];
        [self.navigationController pushViewController:objc animated:YES];
}

@end
