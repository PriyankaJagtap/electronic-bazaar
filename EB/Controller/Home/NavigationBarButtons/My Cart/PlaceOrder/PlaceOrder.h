//
//  PlaceOrder.h
//  EB
//
//  Created by webwerks on 9/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BDViewController.h"


#import "AffiliateUser.h"
@interface PlaceOrder : BaseNavigationControllerWithBackBtn<LibraryPaymentStatusProtocol,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
{
    __weak IBOutlet UILabel *msg_Title_Lbl;
    __weak IBOutlet UILabel *msg_subTitle_Lbl;
    __weak IBOutlet UILabel *msg_detail_Lbl;
    __weak IBOutlet UIView *transaction_status_View;
    __weak IBOutlet UIImageView *transaction_Image;
    __weak IBOutlet UIButton *transaction_status_Button;
    __weak IBOutlet UIButton *feedBackButton;
    NSIndexPath *hideLabelIndexPath;
}


- (IBAction)onClickof_Place_Order:(id)sender;
- (IBAction)onClickof_Transaction_Done_Button:(id)sender;
- (IBAction)onClick_FeedbackButton:(id)sender;


@property(strong,nonatomic)AffiliateUser *objAffUser;
@property(strong,nonatomic)NSString *strPaymentOption;
@property(nonatomic)int totalAmount;
@property(strong,nonatomic)NSString *userMobile;
@property(strong,nonatomic)NSString *userEmail;
@property(strong,nonatomic)NSMutableArray *productListArray;
@property(strong,nonatomic)NSString *selectedPaymentOption;
@property(strong,nonatomic)NSString *assisted_by_arm;
@property(strong,nonatomic)NSString *repCode;
@property(strong,nonatomic)NSString *selectedCreditDays;

@property (nonatomic)BOOL isAdvanceAmount;
@property(nonatomic)int advanceAmount;
@property(nonatomic)NSString *delivery_status;
@property(nonatomic)NSString *approx_delivery;

@property(nonatomic)BOOL isAffiliateUser;
@property(nonatomic)BOOL isCreditPaymentSelected;

@property(strong,nonatomic)NSString *bankName;
@property(strong,nonatomic)NSString *chequeUTR;
@property(strong,nonatomic)NSString *deliveryDateStr;

- (IBAction)leftButtonAction:(id)sender;
- (IBAction)rightSlideMenuAction:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarHtConstraint;

@property (nonatomic,strong)NSDictionary *productSummaryDic;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *placeOrderBtn;
@property (nonatomic, strong) NSString *pastOrderId;

@end
