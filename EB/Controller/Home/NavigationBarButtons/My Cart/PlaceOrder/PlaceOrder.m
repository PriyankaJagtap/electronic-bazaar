//
//  PlaceOrder.m
//  EB
//
//  Created by webwerks on 9/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.

#define PAYZAPP_MERCHANT_ID @"40705743590244810830"
#define PAYZAPP_MERCHANT_APP_ID @"1952"
#define PAYZAPP_SECRET_KEY @"15407057435906338541"



#define DEV_PAYZAPP_MERCHANT_ID @"81516121"
#define DEV_PAYZAPP_MERCHANT_APP_ID @"1"


#import "PlaceOrder.h"
#import "PlaceOrderCell.h"
#import "Constant.h"
#import "MFSideMenuContainerViewController.h"
#import "AppDelegate.h"
#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>
#import "CommonSettings.h"
#import "ProductSummaryTableViewCell.h"
#import "AppDelegate.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

//#import "WibmoSDK.h"
//#import "WSConstant.h"
//#import "WSUrlInfo.h"
#import "SSKeychain.h"

#import "PaymentsSDK.h"

//for paytm
#define MARCHANT_ID @"Amiabl08967254049693" //live
#define WEBSITE @"AmiaWAP" //live
#define INDUTRY_TYPE_ID @"Retail109"
#define MARCHANT_KEY @"ae_ayEzTr_Zwcqxy"

#import "PUUIPaymentOptionVC.h"
#import "PUSAHelperClass.h"


//for pay
#define PAY_U_MERCHANT_KEY @"hyrEt4"
//#define PAY_U_MERCHANT_KEY @"gtKFFx"   //sandbox

#define PAY_U @"payubiz"
@interface PlaceOrder ()<FinishLoadingData,PGTransactionDelegate>
{
    
    NSString *messageHash;
    NSString *merchantTransactionID;
    NSString *orderID;
    UINavigationController *nav1;
}

@property (strong, nonatomic) PayUWebServiceResponse *webServiceResponse;
@property (weak, nonatomic) IBOutlet UILabel *deliveryDateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryDateTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryDateBottomConstraint;
@property (strong, nonatomic) PayUModelPaymentParams *paymentParam;

@end

@implementation PlaceOrder
@synthesize totalAmount,userMobile,userEmail,productListArray;
@synthesize isAffiliateUser,selectedPaymentOption,repCode,strPaymentOption;
@synthesize objAffUser,isAdvanceAmount,advanceAmount;
@synthesize paymentParam;


- (void)viewDidLoad {
    [super viewDidLoad];
    transaction_status_View.hidden=YES;
    [super setViewControllerTitle:@"Checkout"];
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 100;
    orderID = @"";
    _tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    _tableView.estimatedSectionHeaderHeight = 150;
    
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.placeOrderBtn];
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:transaction_status_Button];
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:feedBackButton];
    
    
    [self addPaymentResponseNotofication];
    self.paymentParam = [PayUModelPaymentParams new];
    self.webServiceResponse = [PayUWebServiceResponse new];
    [self.navigationController setNavigationBarHidden:YES];
    
    if(_deliveryDateStr == nil || [_deliveryDateStr isEqualToString:@""])
    {
        [self.view layoutIfNeeded];
        _deliveryDateTopConstraint.constant = 0;
        _deliveryDateBottomConstraint.constant = 0;
        [self.view layoutIfNeeded];
    }
    //    _deliveryDateLabel.text = _deliveryDateStr;
    [[AppDelegate getAppDelegateObj].tabBarObj hideTabbar];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.assisted_by_arm = @"";
    //[self showTabBar];
    //[self.delivery_status isEqualToString:@"0"] ? (self.placeOrderBtn.enabled = NO) : (self.placeOrderBtn.enabled = YES);
}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
//Billdesk 
#pragma mark - BDViewcontroller delegate

-(void)paymentStatus:(NSString*)message {
    //NSLog(@"payment status response %@",message);
    
    //    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
    //    id jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    id jsonData=[self convertHTML:message];
    
    //  NSArray *responseComponents=[[jsonData valueForKey:@"message"] componentsSeparatedByString:@"|"];
    //  NSString *statusCode=(NSString*)[responseComponents objectAtIndex:14];
    NSArray *responseComponents=[jsonData componentsSeparatedByString:@"|"];
    NSString *statusCode=(NSString*)[responseComponents objectAtIndex:14];
    
    if ([statusCode isEqualToString:@"0300"]){
        [self getEcommerceDetails];
        dispatch_async(dispatch_get_main_queue(), ^{
            //[self showTransactionAlertForStatus:YES withMessage:nil];
        });
        
    }else if ([statusCode isEqualToString:@"0399"]){
        [self showTransactionAlertForStatus:NO withMessage:@"Invalid Authentication at Bank"];
        
    }else if ([statusCode isEqualToString:@"NA"]){
        [self showTransactionAlertForStatus:NO withMessage:@"Invalid Input in the Request Message"];
        
    }else if ([statusCode isEqualToString:@"0002"]){
        [self showTransactionAlertForStatus:NO withMessage:@"BillDesk is waiting for Response from Bank"];
        
    }else if ([statusCode isEqualToString:@"0001"]){
        [self showTransactionAlertForStatus:NO withMessage:@"Error at BillDesk"];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popToViewController:self animated:YES];
        [self showTabBar];
        
    });
}


-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}


#pragma mark - Utility Methods

- (void)onPurchasesCompleted {
    
    // Assumes a tracker has already been initialized with a property ID, otherwise
    // this call returns null.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createTransactionWithId:@"0_123456" affiliation:@"In-app Store" revenue:[NSNumber numberWithInt:12000] tax:(int64_t)0 shipping:0 currencyCode:@"INR"]build]];
    
    /*   [tracker send:[[GAIDictionaryBuilder createTransactionWithId:@"0_123456",         // (NSString) Transaction ID, should be unique among transactions.
     affiliation:@"In-app Store",     // (NSString) Affiliation
     revenue:(int64_t) 2.16,      // (int64_t) Order revenue (including tax and shipping)
     tax:(int64_t) 0.17,      // (int64_t) Tax
     shipping:(int64_t) 0,         // (int64_t) Shipping
     currencyCode:@"EUR"] build]];     // (NSString) Currency code
     */
}

/*
 -(void)getEcommerceDetails{
 Webservice  *ecommerceDetailsWS = [[Webservice alloc] init];
 BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
 if(!chkInternet){
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
 [alert show];
 }
 else
 {
 ecommerceDetailsWS.delegate =self;
 NSString *strParam=[NSString stringWithFormat:@"email=%@",[CommonSettings getDefaultValueForKey:@"Email"]];
 
 [ecommerceDetailsWS webServiceWitParameters:strParam andURL:GET_ECOMMERRCETRACKING_DETAIL
 MathodName:@"ECOMMERCEDETAIL"];
 }
 }
 */

-(void)getEcommerceDetails{
    
    Webservice  *ecommerceDetailsWS = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        ecommerceDetailsWS.delegate =self;
        NSString *strParam=[NSString stringWithFormat:@"quote_id=%d&assisted_by_arm=%@",[CommonSettings getQuoteID], self.assisted_by_arm];
        
        [ecommerceDetailsWS webServiceWitParameters:strParam andURL:GET_INCREMENTID_FROM_QUOTEID
                                         MathodName:GET_INCREMENTID_FROM_QUOTEID];
    }
    
    
}


-(void)buttonYesAction:(UIButton*)sender
{
    self.assisted_by_arm = @"yes";
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    PlaceOrderCell *cell = (PlaceOrderCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    cell.buttonYes.selected = YES;
    [cell.buttonYes setBackgroundColor:[UIColor colorWithRed:25.0/255.0 green:44.0/255.0 blue:100.0/255.0 alpha:1.0]];
    
    cell.buttonNo.selected = NO;
    [cell.buttonNo setBackgroundColor:[UIColor whiteColor]];
    cell.labelAlertMsg.hidden = YES;
}

-(void)buttonNoAction:(UIButton*)sender
{
    self.assisted_by_arm = @"no";
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    PlaceOrderCell *cell = (PlaceOrderCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    cell.buttonNo.selected = YES;
    [cell.buttonNo setBackgroundColor:[UIColor colorWithRed:25.0/255.0 green:44.0/255.0 blue:100.0/255.0 alpha:1.0]];
    
    cell.buttonYes.selected = NO;
    [cell.buttonYes setBackgroundColor:[UIColor whiteColor]];
    cell.labelAlertMsg.hidden = YES;
}

#pragma mark - UITableview Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [productListArray count] + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowsAmount = [tableView numberOfRowsInSection:[indexPath section]];
    if (indexPath.row == 0)
    {
        static NSString *cellIdentifier=@"PlaceOrderHomeCell";
        PlaceOrderCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.expectedDeliveryLabel.text = _deliveryDateStr;
         [self.delivery_status isEqualToString:@"0"] ? (cell.expectedDeliveryLabel.textColor = UIColor.redColor) : (cell.expectedDeliveryLabel.textColor = UIColor.greenTextColor);

        [cell.buttonYes addTarget:self action:@selector(buttonYesAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.buttonYes.layer.borderWidth = 1;
        cell.buttonYes.layer.borderColor = [[UIColor grayColor]CGColor];
        
        [cell.buttonNo addTarget:self action:@selector(buttonNoAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.buttonNo.layer.borderWidth = 1;
        cell.buttonNo.layer.borderColor = [[UIColor grayColor]CGColor];
        
        cell.buttonYes.selected = NO;
        cell.buttonNo.selected = NO;
        
        
        NSString *myString = @"Were you helped by our sales team while placing this order?";
        
        //Create mutable string from original one
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:myString];
        
        //Fing range of the string you want to change colour
        //If you need to change colour in more that one place just repeat it
        NSRange range = [myString rangeOfString:@"Were you assisted by our ARM (ElectronicsBazaar Sales team) while placing this order?"];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
        
        cell.labelQuestion.attributedText = attString;

        cell.labelAlertMsg.hidden = YES;
        hideLabelIndexPath = indexPath;
        return cell;
    }
    else if (indexPath.row == rowsAmount - 1 )
    {
        static NSString *cellIdentifier=@"ProductSummaryTableViewCell";
        ProductSummaryTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        //        cell.basePriceLabel.text=[NSString stringWithFormat:@"Rs %ld",(long)[[ _productSummaryDic valueForKey:@"subtotal"]integerValue]];
        //        cell.totalPayableLabel.text=[NSString stringWithFormat:@"Rs %ld",(long)[[_productSummaryDic valueForKey:@"grand_total"]integerValue]];
        //        cell.vatLabel.text=[NSString stringWithFormat:@"Rs %ld",(long)[[_productSummaryDic valueForKey:@"tax_amount"]integerValue]];
        
        
        cell.basePriceLabel.text=[[CommonSettings sharedInstance] formatIntegerPrice:[[ _productSummaryDic valueForKey:@"subtotal"]integerValue]];
        cell.totalPayableLabel.text=[[CommonSettings sharedInstance] formatIntegerPrice:[[ _productSummaryDic valueForKey:@"grand_total"]integerValue]];;
        cell.vatLabel.text=[[CommonSettings sharedInstance] formatIntegerPrice:[[ _productSummaryDic valueForKey:@"tax_amount"]integerValue]];;
        
        cell.subTotalLabel.text=[_productSummaryDic valueForKey:@"subtotal_label"];
        cell.taxLabel.text=[_productSummaryDic valueForKey:@"tax_label"];
        cell.grandTotalLabel.text =[_productSummaryDic valueForKey:@"grandtotal_label"];
        
        if([_productSummaryDic valueForKey:@"discount_label"] && ![[_productSummaryDic valueForKey:@"discount_label"] isEqualToString:@""])
        {
            //cell.taxLabelTop.constant = 30;
            cell.discountLabel.text = [_productSummaryDic valueForKey:@"discount_label"];
            //            cell.discountAmountLabel.text=[NSString stringWithFormat:@"- Rs %ld",(long)[[_productSummaryDic valueForKey:@"discount_amount"]integerValue]];
            cell.discountAmountLabel.text=[NSString stringWithFormat:@"- %@",[[CommonSettings sharedInstance] formatIntegerPrice:[[_productSummaryDic valueForKey:@"discount_amount"]integerValue]]];
        }
        else
        {
            //cell.taxLabelTop.constant = 4;
            cell.discountLabel.text=@"";
            cell.discountAmountLabel.text=@"";
        }
        
        return cell;
    }
    else //if(indexPath.row != [productListArray count])
    {
        static NSString *cellIdentifier=@"PlaceOrderCell";
        PlaceOrderCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.product_Name.text=[[productListArray objectAtIndex:indexPath.row - 1 ]valueForKey:@"name"];
        NSInteger productQty=[[[productListArray objectAtIndex:indexPath.row - 1]valueForKey:@"qty"]integerValue];
        //        NSInteger productPrice=[[[productListArray objectAtIndex:indexPath.row]valueForKey:@"price"]integerValue];
        //        cell.product_Price.text=[NSString stringWithFormat:@"%ld",(long)productPrice];
        
        NSString *priceStr = [[CommonSettings sharedInstance] formatPrice:[[[productListArray objectAtIndex:indexPath.row - 1]valueForKey:@"price"]floatValue]];
        cell.product_Price.text= priceStr;
        cell.product_Qty.text=[NSString stringWithFormat:@"%ld",(long)productQty];
        
        if(indexPath.row == 1)
        {
            [self.view layoutIfNeeded];
            cell.viewTopConstraint.constant = 10;
            cell.viewBottomConstraint.constant = 0;
            cell.separatorLabel.hidden = NO;
            [self.view layoutIfNeeded];
        }
        else if (indexPath.row == [productListArray count]-2)
        {
            
            [self.view layoutIfNeeded];
            cell.viewTopConstraint.constant = 0;
            cell.viewBottomConstraint.constant = 10;
            cell.separatorLabel.hidden = YES;
            [self.view layoutIfNeeded];
        }
        else
        {
            [self.view layoutIfNeeded];
            cell.viewTopConstraint.constant = 0;
            cell.viewBottomConstraint.constant = 0;
            cell.separatorLabel.hidden = NO;
            [self.view layoutIfNeeded];
            
        }
        return cell;
    }
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(indexPath.row != [productListArray count])
//    {
//        return 57;
//    }
//    else
//    {
//        if([_productSummaryDic valueForKey:@"discount_label"] && ![[_productSummaryDic valueForKey:@"discount_label"] isEqualToString:@""])
//        {
//            return 210;
//        }
//        else
//        {
//            return 160;
//        }
//    }
//}


#pragma mark - button action methods

- (IBAction)onClickof_Place_Order:(id)sender {
    
    if ([self.delivery_status isEqualToString:@"0"])
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:self.approx_delivery];
        return;
    }
    else
    {
        if ([self.assisted_by_arm isEqualToString:@""])
        {
            PlaceOrderCell *cell = (PlaceOrderCell *)[self.tableView cellForRowAtIndexPath:hideLabelIndexPath];
            cell.labelAlertMsg.hidden = NO;
            [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        }
        else
        {
            [CommonSettings addGoogleAnalyticsTrackEvent:@"shopping" label:@"place-order-checkout"];
            if ([selectedPaymentOption isEqualToString:@"cashondelivery"] || [selectedPaymentOption isEqualToString:@"custompayment"] || [selectedPaymentOption isEqualToString:@"paytm_cc"] || [selectedPaymentOption isEqualToString:PAY_U] || [selectedPaymentOption isEqualToString:@"replace"]) {
                [self callPlaceOrderWs];
            }
            
            else if([selectedPaymentOption isEqualToString:@"banktransfer"]){
                [self callPlaceOrderWithCreditDaysWs];
            }
            
            else if([selectedPaymentOption isEqualToString:@"billdesk_ccdc_net"]){
                [self navigateToBilldeskSdk];
            }
            //else if([selectedPaymentOption isEqualToString:@"payU"])
            // [self makePayUPayment];
        }
    }

    
}



//Transaction Alert button Done/try again pressed

- (IBAction)onClickof_Transaction_Done_Button:(id)sender{
    NSLog(@"done btn clicked");
    AppDelegate *appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    [[appdelegate tabBarObj]TabClickedIndex:0];
}

-(IBAction)onClick_FeedbackButton:(id)sender{
    [[AppDelegate getAppDelegateObj] stopFeedBackTimer];
    [[AppDelegate getAppDelegateObj] displayFeedBackview];
    
    //    NSLog(@"Feed btn clicked");
    //    [feedBackViewTimer invalidate];
    //    rateView.hidden = false;
    //    feedbackTextView.hidden = true;
}


#pragma mark - generate checksum
//generate checksum
+(NSString *)hmac:(NSString *)plainText withKey:(NSString *)key
{
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [plainText cStringUsingEncoding:NSASCIIStringEncoding];
    
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMACData = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    
    const unsigned char *buffer = (const unsigned char *)[HMACData bytes];
    NSString *HMAC = [NSMutableString stringWithCapacity:HMACData.length * 2];
    
    for (int i = 0; i < HMACData.length; ++i)
        HMAC = [HMAC stringByAppendingFormat:@"%02lX", (unsigned long)buffer[i]];
    
    return HMAC;
    
    
}




#pragma mark - Utility Methods
- (IBAction)leftButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightSlideMenuAction:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Show Thankyou view

-(void)showTransactionAlertForStatus:(BOOL)statusVal withMessage:(NSString *)msg{
    transaction_status_View.hidden=NO;
    //  [self.view bringSubviewToFront:transaction_status_View];
    if (statusVal==YES) {
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [appDelegate setEBPinGenerateCount];
        msg_Title_Lbl.text=@"Transaction successful!";
        transaction_Image.image=[UIImage imageNamed:@"tick"];
        [transaction_status_Button setTitle:@"Continue Shopping" forState:UIControlStateNormal];
        transaction_status_Button.tag=1;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *feedbackStatus = [defaults valueForKey:@"feedBackViewStatus"];
        if (![feedbackStatus isEqualToString:@"0"]) {
            [[AppDelegate getAppDelegateObj] setFeedBackTimer];
            feedBackButton.hidden = false;
        }
        else{
            feedBackButton.hidden = TRUE;
        }
        
        [CommonSettings removeMyCart];
        
    }else{
        msg_Title_Lbl.text=@"Transaction Failed!";
        msg_subTitle_Lbl.text=msg;
        msg_detail_Lbl.text=@"Transaction Failed..Please try again!";
        transaction_Image.image=[UIImage imageNamed:@"failed"];
        feedBackButton.hidden = TRUE;
        [transaction_status_Button setTitle:@"Try again" forState:UIControlStateNormal];
        transaction_status_Button.tag=0;
        //for testing
        
        //        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        //        [defaults setObject:0 forKey:@"QuoteID"];
        //        [defaults synchronize];
        
    }
}

#pragma mark - Webservice calls

-(void)callPlaceOrderWs
{
    Webservice  *placeOrderWsCall = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        placeOrderWsCall.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        if (!(quoteID==0)) {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            NSString *strParam;
            if (isAffiliateUser){
                if([selectedPaymentOption isEqualToString:@"custompayment"])
                {
                    strParam=[NSString stringWithFormat:@"repcode=%@&aff_customer_name=%@&aff_customer_email=%@&aff_customer_mobile=%@&quote_id=%d&remote_ip=ios_%@&assisted_by_arm=%@&ap_utr_no=%@&ap_bank_name=%@&version=%@&userid=%d",objAffUser.repCode,objAffUser.name,objAffUser.emailId,objAffUser.mobNo,quoteID,[SSKeychain passwordForService:@"com.electronicsbazaar.EB" account:@"user"],self.assisted_by_arm,_chequeUTR,_bankName,[CommonSettings getAppCurrentVersion],[CommonSettings getUserID]];
                }
                else if([selectedPaymentOption isEqualToString:@"replace"])
                {
                    strParam=[NSString stringWithFormat:@"repcode=%@&aff_customer_name=%@&aff_customer_email=%@&aff_customer_mobile=%@&quote_id=%d&remote_ip=ios_%@&assisted_by_arm=%@&version=%@&past_order=%@&userid=%d",objAffUser.repCode,objAffUser.name,objAffUser.emailId,objAffUser.mobNo,quoteID,[SSKeychain passwordForService:@"com.electronicsbazaar.EB" account:@"user"],self.assisted_by_arm,[CommonSettings getAppCurrentVersion],_pastOrderId,[CommonSettings getUserID]];
                }
                else
                {
                    strParam=[NSString stringWithFormat:@"repcode=%@&aff_customer_name=%@&aff_customer_email=%@&aff_customer_mobile=%@&quote_id=%d&remote_ip=ios_%@&assisted_by_arm=%@&version=%@&userid=%d",objAffUser.repCode,objAffUser.name,objAffUser.emailId,objAffUser.mobNo,quoteID,[SSKeychain passwordForService:@"com.electronicsbazaar.EB" account:@"user"],self.assisted_by_arm,[CommonSettings getAppCurrentVersion],[CommonSettings getUserID]];
                }
            }
            else{
                if([selectedPaymentOption isEqualToString:@"custompayment"])
                {
                    strParam=[NSString stringWithFormat:@"quote_id=%d&app_order=1&remote_ip=ios_%@&assisted_by_arm=%@&ap_utr_no=%@&ap_bank_name=%@&version=%@&userid=%d",quoteID,[SSKeychain passwordForService:@"com.electronicsbazaar.EB" account:@"user"],self.assisted_by_arm,_chequeUTR,_bankName,[CommonSettings getAppCurrentVersion],[CommonSettings getUserID]];
                }
                else if([selectedPaymentOption isEqualToString:@"replace"])
                {
                    strParam=[NSString stringWithFormat:@"quote_id=%d&app_order=1&remote_ip=ios_%@&assisted_by_arm=%@&ap_utr_no=%@&ap_bank_name=%@&version=%@&past_order=%@&userid=%d",quoteID,[SSKeychain passwordForService:@"com.electronicsbazaar.EB" account:@"user"],self.assisted_by_arm,_chequeUTR,_bankName,[CommonSettings getAppCurrentVersion],_pastOrderId,[CommonSettings getUserID]];
                }
                else
                {
                    strParam=[NSString stringWithFormat:@"quote_id=%d&app_order=1&remote_ip=ios_%@&assisted_by_arm=%@&version=%@&userid=%d",quoteID,[SSKeychain passwordForService:@"com.electronicsbazaar.EB" account:@"user"],self.assisted_by_arm,[CommonSettings getAppCurrentVersion],[CommonSettings getUserID]];
                    
                }
            }
            
            NSString *url = [NSString stringWithFormat:@"%@%@",CREATE_ORDER_WS,strParam];
            [placeOrderWsCall GetWebServiceWithURL:url MathodName:@"CREATEORDER"];
            
        }
    }
    
}


-(void)callPlaceOrderWithCreditDaysWs
{
    Webservice  *placeOrderWsCall = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        placeOrderWsCall.delegate =self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        if (!(quoteID==0)) {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            
            NSString *strParam=[NSString stringWithFormat:@"repcode=%@&aff_customer_name=%@&aff_customer_email=%@&aff_customer_mobile=%@&quote_id=%d&field=%@&version=%@&remote_ip=ios_%@&assisted_by_arm=%@&app_order=1&userid=%d",objAffUser.repCode,objAffUser.name,objAffUser.emailId,objAffUser.mobNo,quoteID,self.selectedCreditDays,[CommonSettings getAppCurrentVersion],[SSKeychain passwordForService:@"com.electronicsbazaar.EB" account:@"user"],self.assisted_by_arm,[CommonSettings getUserID]];
            
            
            NSString *url = [NSString stringWithFormat:@"%@%@",CREATE_ORDER_WS,strParam];
            [placeOrderWsCall GetWebServiceWithURL:url MathodName:@"CREATEORDER"];
        }
    }
    
}



#pragma mark - Webservice Finished

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    
    if ([strName isEqualToString:@"ECOMMERCEDETAIL"]) {
        // [self showTransactionAlertForStatus:YES withMessage:nil];
        if ([[response valueForKey:@"status"]intValue]==0) {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Order is not tracking."];
        }else{
            [self onPurchaseCompletedWithDetails:response];
        }
    }
    
    else if ([strName isEqualToString:@"PAYZAPPGENERATEHASH"]){
        NSLog(@"response %@",response);
        //        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:YES];
        if ([[response valueForKey:@"status"]intValue]==1) {
            messageHash=[response valueForKey:@"message"];
            //            [self initializePayment];
        }else{
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Hash key unable to generate"];
        }
        
        
    }else if ([strName isEqualToString:CREATE_ORDER_BY_PAYZAPP]){
        NSLog(@"Createorderpayzapp response:%@",response);
        dispatch_async(dispatch_get_main_queue(), ^{
            //            [self.navigationController dismissViewControllerAnimated:YES completion:^{
            //            }];
        });
        
        if ([[response valueForKey:@"status"]intValue]==0) {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Order is not tracking."];
        }else{
            [self onPurchaseCompletedWithDetails:response];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showTransactionAlertForStatus:YES withMessage:nil];
                orderID = [[response valueForKey:@"data"] valueForKey:@"order_id"];
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                [defaults setObject:orderID forKey:@"Order_ID"];
                [defaults synchronize];
            });
        }
        // [self getEcommerceDetails];
    }
    else if ([strName isEqualToString:GET_INCREMENTID_FROM_QUOTEID]){
        //BILLDESK SUCCESS RESPONSE
        if ([[response valueForKey:@"status"]intValue]==0) {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Order is not tracking."];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showTransactionAlertForStatus:YES withMessage:nil];
                orderID = [[response valueForKey:@"data"] valueForKey:@"order_id"];
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                [defaults setObject:orderID forKey:@"Order_ID"];
                [defaults synchronize];
                [self onPurchaseCompletedWithDetails:response];
            });
        }
    }
    else{
        
        if ([[response valueForKey:@"status"]intValue]==0) {
            NSString *alertMsg=[response valueForKey:@"message"];
            UIAlertView *objAlert=[[UIAlertView alloc]initWithTitle:@"" message:alertMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            objAlert.tag=89;
            [objAlert show];
        }else{
            
            NSString *orderNo=[NSString stringWithFormat:@"%d",[[[response valueForKey:@"data"]valueForKey:@"order_id"]intValue]];
            orderID = orderNo;
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            [defaults setObject:orderID forKey:@"Order_ID"];
            [defaults synchronize];
            
            if ([selectedPaymentOption isEqualToString:@"cashondelivery"]) {
                if (isAdvanceAmount) {
                    
                    NSString *msgParam=[NSString stringWithFormat:@"%@|%@|NA|%d|NA|NA|NA|INR|NA|R|%@|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|%@%@",MERCHANT_ID,orderNo,advanceAmount,SECURITY_ID,BASE_URL,UPDATE_ORDER_PAYMENT];
                    msgParam=[NSString stringWithFormat:@"%@|%@",msgParam,[PlaceOrder hmac:msgParam withKey:CHECKSUM]];
                    
                    BDViewController *objBDViewController=[[BDViewController alloc]initWithMessage:msgParam andToken:@"QP" andEmail:userEmail andMobile:userMobile andAmount:advanceAmount];
                    objBDViewController.delegate = self;
                    [self.navigationController pushViewController:objBDViewController animated:YES];
                }else{
                    [self getEcommerceDetails];
                    //                    dispatch_async(dispatch_get_main_queue(), ^{
                    //                        [self showTransactionAlertForStatus:YES withMessage:nil];
                    //                    });
                }
                
            }
            else if ([selectedPaymentOption isEqualToString:@"paytm_cc"])
            {
                [self createPaytmPayment:response];
            }
            else if ([selectedPaymentOption isEqualToString:PAY_U])
            {
                [self makePayUPayment:response];
            }
            else if([selectedPaymentOption isEqualToString:@"banktransfer"]){
                //billdesk payment
                [self getEcommerceDetails];
                //                dispatch_async(dispatch_get_main_queue(), ^{
                //                    [self showTransactionAlertForStatus:YES withMessage:nil];
                //                });
                
            }else if([selectedPaymentOption isEqualToString:@"custompayment"]){
                [self getEcommerceDetails];
                //                dispatch_async(dispatch_get_main_queue(), ^{
                //                    [self showTransactionAlertForStatus:YES withMessage:nil];
                //                });
                
            }
            else if([selectedPaymentOption isEqualToString:@"replace"]){
                [self getEcommerceDetails];
                //                    dispatch_async(dispatch_get_main_queue(), ^{
                //                            [self showTransactionAlertForStatus:YES withMessage:nil];
                //                    });
                
            }
        }
    }
}

/*
 - (void)initializePayment {
 WSMerchantInfo *aMerchantInfo = [[WSMerchantInfo alloc] init];
 aMerchantInfo.merchantCountryCode = @"IN";
 aMerchantInfo.merchantName = @"MerchantName";
 
 // PRODUCTION
 //aMerchantInfo.merchantID = @"5344117557917234438";
 //aMerchantInfo.merchantAppID = @"7016";
 
 // STAGING
 aMerchantInfo.merchantID = @"81516121";
 aMerchantInfo.merchantAppID = @"1";
 //aMerchantInfo.merchantUrlSchema = @"";
 
 WSTransactionInfo *aTransactionInfo = [[WSTransactionInfo alloc] init];
 aTransactionInfo.transactionCurrency = @"356";
 aTransactionInfo.transactionAmount = @"100";
 aTransactionInfo.merchantAppData = @"AppDATA";
 aTransactionInfo.transactionDescription = @"Transaction from sample merchant for amount 1";
 
 aTransactionInfo.messageHash = messageHash;
 aTransactionInfo.merchantTransactionId = merchantTransactionID;
 
 WSCustomerInfo *aCustomerInfo = [[WSCustomerInfo alloc] init];
 aCustomerInfo.customerEmail = @"someone@enstage.com";
 aCustomerInfo.customerMobile = @"9595379667";
 aCustomerInfo.customerName = @"Preetham Hegde";
 aCustomerInfo.customerDateOfBirth = @"20010101";
 
 WSUrlInfo *aUralInfo = [[WSUrlInfo alloc] init];
 aUralInfo.baseUrl = STAGING_URL;
 
 //aUralInfo.baseUrl = PRODUCTION_URL;
 
 WibmoSDK *aWibmoSDK = [[WibmoSDK alloc] init];
 [aWibmoSDK setUrlInfo:aUralInfo];
 [self.navigationController presentViewController:aWibmoSDK animated:YES completion:^{
 [aWibmoSDK setTransactionInfo:aTransactionInfo];
 [aWibmoSDK setMerchantInfo:aMerchantInfo];
 [aWibmoSDK setCustomerInfo:aCustomerInfo];
 
 [aWibmoSDK setDelegate:self];
 
 aTransactionInfo.supportedPaymentType = @[PAYMENT_TYPE_ALL];
 aTransactionInfo.restrictedPaymentType = @[PAYMENT_TYPE_ALL];
 [aWibmoSDK initializePayment];
 }];
 }
 
 */



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //    if (alertView.tag==89) {
    //        AppDelegate *appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    //        [[appdelegate tabBarObj]TabClickedIndex:2];
    //
    //        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    //        [defaults setObject:0 forKey:@"QuoteID"];
    //        [defaults synchronize];
    //    }
    //    else{
    //
    //        if (buttonIndex==0) {
    //            //Billdesk
    //            [self navigateToBilldeskSdk];
    //
    //        }else if(buttonIndex==1){
    //            //payzapp selected
    //            //generateHashPayZapp
    //           // [self callGenerateHashPayZappWS];
    //            [alertView dismissWithClickedButtonIndex:1 animated:YES];
    //
    //        }
    //
    //    }
}



-(void)navigateToBilldeskSdk{
    
    NSDate *past = [NSDate date];
    NSTimeInterval oldTime = [past timeIntervalSince1970] * 1000;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
    int userId=[[[defaults valueForKey:@"user"]valueForKey:@"user_id"]intValue];
    NSString *uniqueId = [[NSString alloc] initWithFormat:@"%0.0f_%d_%d", oldTime,quoteID,userId];
    
    NSString *msgParam=[NSString stringWithFormat:@"%@|%@|NA|%d|NA|NA|NA|INR|NA|R|%@|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|%@%@",MERCHANT_ID,uniqueId,totalAmount,SECURITY_ID,BASE_URL,UPDATE_ORDER_PAYMENT];
    msgParam=[NSString stringWithFormat:@"%@|%@",msgParam,[PlaceOrder hmac:msgParam withKey:CHECKSUM]];
    
    [self hideTab];
    BDViewController *objBDViewController=[[BDViewController alloc]initWithMessage:msgParam andToken:@"QP" andEmail:userEmail andMobile:userMobile andAmount:totalAmount];
    objBDViewController.delegate = self;
    [self.navigationController pushViewController:objBDViewController animated:YES];
    
}


-(void)callGenerateHashPayZappWS{
    
    Webservice  *objGeneratehash = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
        
        NSDate *past = [NSDate date];
        NSTimeInterval oldTime = [past timeIntervalSince1970] * 1000;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
        // int userId=[[[defaults valueForKey:@"user"]valueForKey:@"user_id"]intValue];
        NSString *uniqueId = [[NSString alloc] initWithFormat:@"%0.0f_%d_%d", oldTime,quoteID,[CommonSettings getUserID]];
        merchantTransactionID=uniqueId;
        NSString *str=[NSString stringWithFormat:@"wpay|%@|%@|%@|ElectronicBazaar|%d|356|*|%@|",PAYZAPP_MERCHANT_ID,PAYZAPP_MERCHANT_APP_ID,uniqueId,totalAmount*100,PAYZAPP_SECRET_KEY];
        
        objGeneratehash.delegate =self;
        NSString *strParam=[NSString stringWithFormat:@"payzappHashKey=%@",str];
        
        [objGeneratehash webServiceWitParameters:strParam andURL:GENERATE_PAYZAPP_HASHKEY
                                      MathodName:@"PAYZAPPGENERATEHASH"];
    }
}



- (void)onPurchaseCompletedWithDetails:(NSArray *)transactionDetailArr
{
    // Assumes a tracker has already been initialized with a property ID, otherwise
    // this call returns null.
    
    int grandTotal=[[[[[transactionDetailArr valueForKey:@"data"]valueForKey:@"tracking_data"]valueForKey:@"ecommerce_tracking"]valueForKey:@"grand_total"]intValue];
    int tax=[[[[[transactionDetailArr valueForKey:@"data"] valueForKey:@"tracking_data"]valueForKey:@"ecommerce_tracking"]valueForKey:@"tax"]intValue];
    int shippingCharges=[[[[[transactionDetailArr valueForKey:@"data"]valueForKey:@"tracking_data"]valueForKey:@"ecommerce_tracking"]valueForKey:@"shipping"]intValue];
    NSString *orderId=[[transactionDetailArr valueForKey:@"data"] valueForKey:@"order_id"];
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:orderId forKey:@"Order_ID"];
    [defaults synchronize];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    //transaction id,revenue,tax,shipping,
    [tracker send:[[GAIDictionaryBuilder createTransactionWithId:orderId             // (NSString) Transaction ID
                                                     affiliation:@"In-app Store"         // (NSString) Affiliation
                                                         revenue:[NSNumber numberWithInt:grandTotal]// (NSNumber) Order revenue (including tax and shipping)
                                                             tax:[NSNumber numberWithInt:tax]                  // (NSNumber) Tax
                                                        shipping:[NSNumber numberWithInt:shippingCharges]                      // (NSNumber) Shipping
                                                    currencyCode:@"INR"] build]];        // (NSString) Currency code
    
    NSArray *products=[[[transactionDetailArr valueForKey:@"data"]valueForKey:@"tracking_data"]valueForKey:@"products"];
    
    
    for (int i=0; i<products.count; i++) {
        
        int productPrice=[[[products objectAtIndex:i]valueForKey:@"price"]intValue];
        int productQty=[[[products objectAtIndex:i]valueForKey:@"qty"]intValue];
        
        [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId:orderId         // (NSString) Transaction ID
                                                                    name:[[products objectAtIndex:i] valueForKey:@"name"]  // (NSString) Product Name
                                                                     sku:[[products objectAtIndex:i] valueForKey:@"sku"]            // (NSString) Product SKU
                                                                category:[[products objectAtIndex:i]valueForKey:@"category_name"]  // (NSString) Product category
                                                                   price:[NSNumber numberWithInt:productPrice]// (NSNumber) Product price
                                                                quantity:[NSNumber numberWithInt:productQty]   // (NSInteger) Product quantity
                                                            currencyCode:@"INR"] build]];    // (NSString) Currency code
        
    }
    
    
}

#pragma mark - PayU Intgration methods

-(void)makePayUPayment:(id)paymentData
{
    
    NSDictionary *dic = [paymentData valueForKey:@"data"];
    
    [[CommonSettings sharedInstance] displayLoading];
    self.view.userInteractionEnabled = NO;
    // paymentParam.key = @"gtKFFx";
    paymentParam.key = PAY_U_MERCHANT_KEY;
    /*paymentParam.transactionID = [PUSAHelperClass getTransactionIDWithLength:15];
     NSLog(@"transactionID %@",paymentParam.transactionID);
     paymentParam.amount = @"10";
     paymentParam.productInfo = @"iPhone";*/
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    paymentParam.transactionID = [NSString stringWithFormat:@"%@",[dic valueForKey:@"order_id"] ] ;
    NSLog(@"transactionID %@",paymentParam.transactionID);
    paymentParam.amount = [NSString stringWithFormat:@"%@",[[[dic valueForKey:@"tracking_data"] valueForKey:@"ecommerce_tracking"] valueForKey:@"grand_total"]];
    paymentParam.productInfo = @"Product_info";
    
    paymentParam.SURL = @"https://payu.herokuapp.com/success";
    paymentParam.FURL = @"https://payu.herokuapp.com/failure";
    paymentParam.firstName = @"Super";
    paymentParam.email = @"maheshgurav@gmail.com";
    //paymentParam.email = [NSString stringWithFormat:@"%@",[defaults valueForKey:@"email"]];
    
    paymentParam.udf1 = @"";
    paymentParam.udf2 = @"";
    paymentParam.udf3 = @"";
    paymentParam.udf4 = @"";
    paymentParam.udf5 = @"";
    
    //self.scrollView = nil;
    
    // Set the environment according to merchant key ENVIRONMENT_PRODUCTION for Production &
    // ENVIRONMENT_MOBILETEST for mobiletest environment:
    //    self.paymentParam.environment = ENVIRONMENT_MOBILETEST;
    // self.paymentParam.environment = ENVIRONMENT_TEST;
    
    self.paymentParam.environment = ENVIRONMENT_PRODUCTION;
    
    // Set this property if you want to give offer:
    //self.paymentParam.offerKey = @"test123@6622"; ////bins@8427,srioffer@8428,cc2@8429,gtkffx@7236
    
    paymentParam.offerKey = @"";
    
    // Set this property if you want to get the stored cards:
    
    //paymentParam.userCredentials = @"gtKFFx:Baalak@gmail.com";
    //int userId=[[[[NSUserDefaults standardUserDefaults] valueForKey:@"user"]valueForKey:@"user_id"]intValue];
    
    paymentParam.userCredentials = [NSString stringWithFormat:@"%@:%@",PAY_U_MERCHANT_KEY,paymentParam.email];
    [PUSAHelperClass generateHashFromServer:self.paymentParam withCompletionBlock:^(PayUModelHashes *hashes, NSString *errorString) {
        NSLog(@"allHasess %@",hashes);
        [self callSDKWithHashes:hashes withError:errorString];
    }];
    
}
-(void)callSDKWithHashes:(PayUModelHashes *) allHashes withError:(NSString *) errorMessage{
    if (errorMessage == nil) {
        self.paymentParam.hashes = allHashes;
        NSLog(@"allHasess %@",allHashes);
    }
    [self callSDKWithOneTap:nil];
}

-(void) callSDKWithOneTap:(NSDictionary *)oneTapDict{
    self.paymentParam.OneTapTokenDictionary = oneTapDict;
    PayUWebServiceResponse *respo = [PayUWebServiceResponse new];
    self.webServiceResponse = [PayUWebServiceResponse new];
    [self.webServiceResponse getPayUPaymentRelatedDetailForMobileSDK:self.paymentParam withCompletionBlock:^(PayUModelPaymentRelatedDetail *paymentRelatedDetails, NSString *errorMessage, id extraParam) {
        
        [[CommonSettings sharedInstance] removeLoading];
        
        if (!errorMessage) {
            [respo callVASForMobileSDKWithPaymentParam:self.paymentParam];
            UIStoryboard *stryBrd = [UIStoryboard
                                     storyboardWithName:@"PUUIMainStoryBoard" bundle:nil];
            
            PUUIPaymentOptionVC *paymentOptionVC = [stryBrd instantiateViewControllerWithIdentifier:VC_IDENTIFIER_PAYMENT_OPTION];
            paymentOptionVC.paymentParam = self.paymentParam;
            paymentOptionVC.paymentRelatedDetail = paymentRelatedDetails;
            
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:paymentOptionVC];
            [self presentViewController:nav animated:NO completion:nil];
            
        }
        else{
            NSLog(@" %@",errorMessage);
        }
    }];
    
}
-(void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)addPaymentResponseNotofication{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseReceived:) name:kPUUINotiPaymentResponse object:nil];
}

-(void)responseReceived:(NSNotification *) notification{
    NSLog(@"Response Received message from server");
    
    self.view.userInteractionEnabled = YES;
    [self dismissViewControllerAnimated:NO completion:^{
        self.view.userInteractionEnabled = YES;
    }];
    
    NSString *strConvertedRespone = [NSString stringWithFormat:@"%@",notification.object];
    NSLog(@"Response Received %@",strConvertedRespone);
    
    NSError *serializationError;
    id JSON = [NSJSONSerialization JSONObjectWithData:[strConvertedRespone dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&serializationError];
    if (serializationError == nil && notification.object) {
        NSLog(@"%@",JSON);
        //PAYUALERT([JSON objectForKey:@"status"], strConvertedRespone);
        if ([[JSON objectForKey:@"status"] isEqual:@"success"]) {
            NSString *merchant_hash = [JSON objectForKey:@"merchant_hash"];
            
            [self getEcommerceDetails];
            //  [self showTransactionAlertForStatus:@"YES" withMessage:[JSON objectForKey:@"status"]];
            if ([[JSON objectForKey:@"card_token"] length] >1 && merchant_hash.length >1 && self.paymentParam) {
                NSLog(@"Saving merchant hash---->");
                [PUSAHelperClass saveOneTapTokenForMerchantKey:self.
                 paymentParam.key withCardToken:[JSON objectForKey:@"card_token"] withUserCredential:self.paymentParam.userCredentials andMerchantHash:merchant_hash withCompletionBlock:^(NSString *message, NSString *errorString) {
                     if (errorString == nil) {
                         NSLog(@"Merchant Hash saved succesfully %@",message);
                     }
                     else{
                         NSLog(@"Error while saving merchant hash %@", errorString);
                     }
                 }];
            }
        }
    }
    else{
        //PAYUALERT(@"Response", strConvertedRespone);
        
        [self showTransactionAlertForStatus:@"NO" withMessage:[JSON objectForKey:@"status"]];
    }
}


#pragma mark - Webservice calling
//quote_id,  tran_id & dataPickUpCode

-(void)createOrderPayzappWithTransactionId:(NSString *)transId andDataPickUpCode:(NSString *)dataPickUpCode{
    
    Webservice  *ecommerceDetailsWS = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    
    if(!chkInternet){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
        ecommerceDetailsWS.delegate =self;
        NSString *strParam=[NSString stringWithFormat:@"quote_id=%d",[CommonSettings getQuoteID]];
        
        [ecommerceDetailsWS webServiceWitParameters:strParam andURL:CREATE_ORDER_BY_PAYZAPP
                                         MathodName:CREATE_ORDER_BY_PAYZAPP];
    }
}

#pragma mark - make PAYTM payment
-(void)createPaytmPayment:(id)paymentData
{
    //[self hideTab];
    
    NSDictionary *dic = [paymentData valueForKey:@"data"];
    PGMerchantConfiguration *mc = [PGMerchantConfiguration defaultConfiguration];
    //Step 2: If you have your own checksum generation and validation url set this here. Otherwise use the default Paytm urls
    mc.checksumGenerationURL = @"http://www.electronicsbazaar.com/generateChecksum.php";
    mc.checksumValidationURL = @"http://www.electronicsbazaar.com/verifyChecksum.php";
    
    //Step 3: Create the order with whatever params you want to add. But make sure that you include the merchant mandatory params
    NSMutableDictionary *orderDict = [NSMutableDictionary new];
    //Merchant configuration in the order object
    orderDict[@"MID"] = MARCHANT_ID;
    orderDict[@"CHANNEL_ID"] = @"WAP";
    orderDict[@"INDUSTRY_TYPE_ID"] = INDUTRY_TYPE_ID;
    orderDict[@"WEBSITE"] =  WEBSITE;
    
    //Order configuration in the order object
    orderDict[@"TXN_AMOUNT"] =[[[dic valueForKey:@"tracking_data"] valueForKey:@"ecommerce_tracking"] valueForKey:@"grand_total"];
    orderDict[@"ORDER_ID"] = [dic valueForKey:@"order_id"];
    orderDict[@"REQUEST_TYPE"] = @"DEFAULT";
    orderDict[@"CUST_ID"] =[[[dic valueForKey:@"tracking_data"] valueForKey:@"ecommerce_tracking"] valueForKey:@"customer_id"];
    
    NSLog(@"order dic %@",orderDict);
    
    PGOrder *order = [PGOrder orderWithParams:orderDict];
    
    //Step 4: Choose the PG server. In your production build dont call selectServerDialog. Just create a instance of the
    //PGTransactionViewController and set the serverType to eServerTypeProduction
    //    [PGServerEnvironment selectServerDialog:self.view completionHandler:^(ServerType type)
    //     {
    PGTransactionViewController *txnController = [[PGTransactionViewController alloc] initTransactionForOrder:order];
    // if (type != eServerTypeNone) {
    txnController.serverType = eServerTypeProduction;
    txnController.merchant = mc;
    txnController.delegate = self;
    [self showController:txnController];
    //}
    //}];
    
}

-(void)showController:(PGTransactionViewController *)controller
{
    //    if (self.navigationController != nil)
    //        [self.navigationController pushViewController:controller animated:YES];
    //    else
    [self presentViewController:controller animated:YES
                     completion:^{
                         
                     }];
}

-(void)removeController:(PGTransactionViewController *)controller
{
    // [self showTabBar];
    //    if (self.navigationController != nil)
    //        [self.navigationController popViewControllerAnimated:YES];
    //    else
    [controller dismissViewControllerAnimated:YES
                                   completion:^{
                                   }];
}

#pragma mark - hide and unhide tab bar
-(void)hideTab
{
    [[AppDelegate getAppDelegateObj].tabBarObj hideTabbar];
    _tabBarHtConstraint.constant = 0;
    [self.view layoutIfNeeded];
    
}

-(void)showTabBar
{
    [[AppDelegate getAppDelegateObj].tabBarObj unhideTabbar];
    _tabBarHtConstraint.constant = 50;
    [self.view layoutIfNeeded];
    
}
#pragma mark - PGTransactionViewController delegate

- (void)didSucceedTransaction:(PGTransactionViewController *)controller
                     response:(NSDictionary *)response
{
    DEBUGLOG(@"ViewController::didSucceedTransactionresponse= %@", response);
    //    NSString *title = [NSString stringWithFormat:@"Your order  was completed successfully. \n %@", response[@"ORDERID"]];
    //    [[[UIAlertView alloc] initWithTitle:title message:[response description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    [self getEcommerceDetails];
    [self removeController:controller];
}

- (void)didFailTransaction:(PGTransactionViewController *)controller error:(NSError *)error response:(NSDictionary *)response
{
    DEBUGLOG(@"ViewController::didFailTransaction error = %@ response= %@", error, response);
    
    if(response)
        [self showTransactionAlertForStatus:NO withMessage:response[@"RESPMSG"]];
    else if (error)
        [self showTransactionAlertForStatus:NO withMessage:error.localizedDescription];
    
    [self removeController:controller];
    
    //    if (response)
    //    {
    //        [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:[response description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    //    }
    //    else if (error)
    //    {
    //        [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    //    }
    
}

- (void)didCancelTransaction:(PGTransactionViewController *)controller error:(NSError*)error response:(NSDictionary *)response
{
    DEBUGLOG(@"ViewController::didCancelTransaction error = %@ response= %@", error, response);
    NSString *msg = nil;
    if (!error) msg = [NSString stringWithFormat:@"Successful"];
    else msg = [NSString stringWithFormat:@"UnSuccessful"];
    
    //    [[[UIAlertView alloc] initWithTitle:@"Transaction Cancel" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    [self showTransactionAlertForStatus:NO withMessage:msg];
    [self removeController:controller];
}

- (void)didFinishCASTransaction:(PGTransactionViewController *)controller response:(NSDictionary *)response
{
    DEBUGLOG(@"ViewController::didFinishCASTransaction:response = %@", response);
}

@end
