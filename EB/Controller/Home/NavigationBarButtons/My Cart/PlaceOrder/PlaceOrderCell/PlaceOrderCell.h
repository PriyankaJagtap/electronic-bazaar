//
//  PlaceOrderCell.h
//  EB
//
//  Created by webwerks on 9/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceOrderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *product_Name;
@property (weak, nonatomic) IBOutlet UILabel *product_Qty;
@property (weak, nonatomic) IBOutlet UILabel *product_Price;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBottomConstraint;
@property (weak, nonatomic) IBOutlet UILabel *separatorLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedDeliveryLabel;

@property (weak, nonatomic) IBOutlet UIButton *buttonYes;
@property (weak, nonatomic) IBOutlet UIButton *buttonNo;
@property (weak, nonatomic) IBOutlet UILabel *labelQuestion;
@property (weak, nonatomic) IBOutlet UILabel *labelAlertMsg;

@end
