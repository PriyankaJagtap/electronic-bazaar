//
//  NotificationViewController.m
//  EB
//
//  Created by webwerks on 9/2/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationCell.h"
#import "MFSideMenuContainerViewController.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "Webservice.h"
#import "ProductDetailsViewController.h"
#import "CommonSettings.h"
#import "HomeDealsViewController.h"

@interface NotificationViewController ()<FinishLoadingData>

@end

@implementation NotificationViewController
@synthesize notificationListTableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setViewControllerTitle:@"Notifications"];
    [super setIsNotificationScreenValue:1];
    // Do any additional setup after loading the view.
    self.notificationListTableView.estimatedRowHeight = 50;
    self.notificationListTableView.rowHeight = UITableViewAutomaticDimension;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    //  dispatch_async(dispatch_get_main_queue(), ^{
    //[[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:1];
    // });
    productDetailsArr=[[NSArray alloc]init];
    //self.notificationListTableView.contentInset = UIEdgeInsetsMake(-36, 0, 50, 0);
    
    [CommonSettings sendScreenName:@"NotificationView"];
    [self getNotificationListFromWs];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [super setIsNotificationScreenValue:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return productDetailsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *CellIdentifier = @"NotificationCell";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[NotificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.lblProductName.text=[[productDetailsArr objectAtIndex:indexPath.row]valueForKey:@"title"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyBoard;
    //    if (isIpad) {
    //        storyBoard= [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    //    }else
    //    {
    storyBoard= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //}
    
    if([[productDetailsArr objectAtIndex:indexPath.row]objectForKey:@"url_type"])
    {
        if([[[productDetailsArr objectAtIndex:indexPath.row]objectForKey:@"url_type"]isEqualToString:@"product"])
        {
            ProductDetailsViewController *objProductDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
            int productId=[[[productDetailsArr objectAtIndex:indexPath.row]valueForKey:@"product_id"]intValue];
            objProductDetailVC.productID=productId;
            [self.navigationController pushViewController:objProductDetailVC animated:YES];
        }
        else  if([[[productDetailsArr objectAtIndex:indexPath.row]objectForKey:@"url_type"]isEqualToString:@"category"])
        {
            ElectronicsViewController *electronicviewObjc = [storyBoard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
            NSString *cat_id = [NSString stringWithFormat:@"%d",[[[productDetailsArr objectAtIndex:indexPath.row]valueForKey:@"category_id"]intValue]];
            
            electronicviewObjc.category_id=cat_id;
            electronicviewObjc.categoryVal= [[productDetailsArr objectAtIndex:indexPath.row]valueForKey:@"category_name"];
            [self.navigationController pushViewController:electronicviewObjc animated:YES];
        }
    }
    else if ([[[productDetailsArr objectAtIndex:indexPath.row]valueForKey:@"type"]isEqualToString:@"app_update"])
    {
        UIAlertView *objAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Click OK to upgrade the app now." delegate:self cancelButtonTitle:@"Not Now" otherButtonTitles:@"Ok", nil];
        objAlert.tag = 100;
        [objAlert show];
        
    }
    else if ([[[productDetailsArr objectAtIndex:indexPath.row]valueForKey:@"type"]isEqualToString:@"link"])
    {
        NSString *string = [[productDetailsArr objectAtIndex:indexPath.row]valueForKey:@"link_url"];
        if ([string rangeOfString:@"deals"].location == NSNotFound)
        {
            NSLog(@"string does not contain deals");
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[productDetailsArr objectAtIndex:indexPath.row]valueForKey:@"link_url"]]];
        } else
        {
            [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:3];
        }
    }
}

#pragma mark - Webwervice Method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    productDetailsArr=[response valueForKey:@"data"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [notificationListTableView reloadData];
    });
}

#pragma mark - Utility Method
-(void)getNotificationListFromWs
{
    Webservice  *callNotificationService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callNotificationService.delegate =self;
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
        NSString *pushID=[[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_TOKEN"];
        NSString *deviceID = pushID?:@"";
        NSString *url = [NSString stringWithFormat:@"%@%@",NOTIFICATION_LIST,deviceID];
        //        [callNotificationService GetWebServiceWithURL:[NSString stringWithFormat:@"%@",NOTIFICATION_LIST] MathodName:@"NOTIFICATIONLIST"];
        [callNotificationService GetWebServiceWithURL:url MathodName:@"NOTIFICATIONLIST"];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100 && buttonIndex == 1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:App_Link]];
    }
}

@end
