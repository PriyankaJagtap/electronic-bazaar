//
//  NotificationViewController.h
//  EB
//
//  Created by webwerks on 9/2/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : BaseNavigationControllerWithBackBtn
{
    NSArray *productDetailsArr;
}

@property (weak, nonatomic) IBOutlet UITableView *notificationListTableView;

@end
