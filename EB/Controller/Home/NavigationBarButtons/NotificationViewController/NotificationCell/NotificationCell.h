//
//  NotificationCell.h
//  EB
//
//  Created by webwerks on 9/2/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell

@property(strong,nonatomic)IBOutlet UILabel *lblProductName;

@end
