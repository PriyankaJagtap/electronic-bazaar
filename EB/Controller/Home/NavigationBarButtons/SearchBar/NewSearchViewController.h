//
//  NewSearchViewController.h
//  EB
//
//  Created by Aishwarya Rai on 04/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Webservice.h"
#import "Constant.h"


#import "BaseNavigationControllerWithBackBtn.h"

@interface NewSearchViewController : BaseNavigationControllerWithBackBtn <UISearchBarDelegate,FinishLoadingData,UITableViewDataSource,UITableViewDelegate,LoginsViewDelegate>
{
    __weak IBOutlet UISearchBar *objSearchbar;
    __weak IBOutlet UITableView *objTableView;
    UILabel *lblNoProducts;
    NSMutableArray *productArr;
    
}
@property(strong,nonatomic)UINavigationController *navController;
@property (nonatomic, strong) NSString* searchText;


- (IBAction)leftSlideMenuAction:(id)sender;

@end
