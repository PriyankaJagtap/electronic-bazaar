//
//  HomeHeaderTableCell.h
//  EB
//
//  Created by Neosoft on 6/5/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeHeaderTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
