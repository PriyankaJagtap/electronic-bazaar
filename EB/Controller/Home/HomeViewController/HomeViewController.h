//
//  HomeViewController.h
//  EB
//
//  Created by webwerks on 7/29/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightMenuViewController.h"
#import "SlideMenuViewController.h"
#import "ElectronicsViewController.h"
#import "MFSideMenu.h"
#import "MenuProductCell.h"
#import "ProductCell.h"
#import "HotProductCell.h"
#import "AddNewProdCell.h"
#import "Constant.h"
#import "FVCustomAlertView.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "HomeViewNavigationController.h"
#import "UIImageView+WebCache.h"

@interface HomeViewController : HomeViewNavigationController<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,FinishLoadingData,UIScrollViewDelegate,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)leftSlideMenuAction:(id)sender;
@property (strong, nonatomic)NSDictionary *notificationDict;
-(void)didbecomeActiveOnNotification;
@property(nonatomic)BOOL isSearch;
@property (weak, nonatomic) IBOutlet UIImageView *popUpImageView;
@property (weak, nonatomic) IBOutlet UIView *popUpView;

@property (nonatomic) BOOL firstTimeBool;
@property (weak, nonatomic) IBOutlet UIButton *suvidhaBtn;
@property (weak, nonatomic) IBOutlet UIView *suvidhaBgView;

@end
