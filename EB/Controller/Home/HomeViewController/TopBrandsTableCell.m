//
//  TopBrandsTableCell.m
//  EB
//
//  Created by Neosoft on 6/6/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "TopBrandsTableCell.h"
#import "TopBrandsCollectionCell.h"

#define BRAND_IMG_SCALE_FACTOR 1.16

@implementation TopBrandsTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - collectionView delegate methods
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    
//    for (UICollectionViewCell *cell in [self.productListCollectionView visibleCells]) {
//        NSIndexPath *indexPath = [self.productListCollectionView indexPathForCell:cell];
//        //NSLog(@"visible index %ld",(long)indexPath.item);
//        self.objPageControl.currentPage = indexPath.item;
//        
//    }
//}
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    return _brandsImageArray.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
        CGFloat width = (kSCREEN_WIDTH-15)/2;
        return CGSizeMake(width, width/BRAND_IMG_SCALE_FACTOR);
   // return CGSizeMake(100, 40);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 5,5);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"TopBrandsCollectionCell";
    TopBrandsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    [[CommonSettings sharedInstance] setBorderToView:cell.bgView];
//    [cell.brandImgView sd_setImageWithURL:[NSURL URLWithString:[[_brandsImageArray objectAtIndex:indexPath.item] valueForKey:@"img_url"]]];
        
        //[[CommonSettings sharedInstance] setBorderToView:cell.bgView];
        [cell.brandImgView sd_setImageWithURL:[NSURL URLWithString:[[_brandsImageArray objectAtIndex:indexPath.item] valueForKey:@"banner_img"]]];
    
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [_delegate brandImageClicked:[[_brandsImageArray objectAtIndex:indexPath.item] valueForKey:@"name"]];
}

@end
