//
//  PaymentOptionTableCell.m
//  EB
//
//  Created by webwerks on 16/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "PaymentOptionTableCell.h"
#import "TopBrandsCollectionCell.h"
#define PAYMENT_IMAGE_SCALE_FACTOR 2.50

@implementation PaymentOptionTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - collectionView delegate methods
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//
//    for (UICollectionViewCell *cell in [self.productListCollectionView visibleCells]) {
//        NSIndexPath *indexPath = [self.productListCollectionView indexPathForCell:cell];
//        //NSLog(@"visible index %ld",(long)indexPath.item);
//        self.objPageControl.currentPage = indexPath.item;
//
//    }
//}
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
        return 1;
        
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
        
        
        return _paymentOptionsArray.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
        CGFloat width = (kSCREEN_WIDTH-20)/5;
        return CGSizeMake(width, ((width-10)/PAYMENT_IMAGE_SCALE_FACTOR) + 10);
        // return CGSizeMake(100, 40);
        
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
        return 5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
        return 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
//        return UIEdgeInsetsMake(5, 5, 5,5);
        return UIEdgeInsetsMake(5, 10, 5, 10);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
        
        static NSString *identifier = @"TopBrandsCollectionCell";
        TopBrandsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
         [cell.brandImgView sd_setImageWithURL:[NSURL URLWithString:[[_paymentOptionsArray objectAtIndex:indexPath.item] valueForKey:@"payment_img"]]];
        cell.contentView.layer.borderWidth= 0.5;
        cell.contentView.layer.borderColor =[UIColor lightGrayColor].CGColor;
        [cell.contentView setClipsToBounds:YES];
        
        return cell;
        
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        [_delegate paymentOptionImageClicked:@""];
}

@end
