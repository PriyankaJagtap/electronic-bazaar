//
//  TopBrandsCollectionCell.h
//  EB
//
//  Created by Neosoft on 6/6/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopBrandsCollectionCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIView *bgView;
@property (nonatomic, weak) IBOutlet UIImageView *brandImgView;


@end
