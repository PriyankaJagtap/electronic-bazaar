//
//  HomeBannerTableViewCell.h
//  EB
//
//  Created by Neosoft on 12/2/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeBannerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *leftBannerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *topRightBannerImageview;
@property (weak, nonatomic) IBOutlet UIImageView *bottomRightBannerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *middleRightBannerImageView;
@property (weak, nonatomic) IBOutlet UIButton *dealsBtn;

@end
