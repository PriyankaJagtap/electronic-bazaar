//
//  EbAdvantagesTableCell.h
//  EB
//
//  Created by Neosoft on 6/8/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EbAdvantagesTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ebAdvantagesImgView;
@property (weak, nonatomic) IBOutlet UIButton *ebAdvantagesBn;

@end
