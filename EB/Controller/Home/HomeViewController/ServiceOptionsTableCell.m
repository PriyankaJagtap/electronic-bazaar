//
//  ServiceOptionsTableCell.m
//  EB
//
//  Created by webwerks on 16/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "ServiceOptionsTableCell.h"
#import "TopBrandsCollectionCell.h"

@implementation ServiceOptionsTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - collectionView delegate methods
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//
//    for (UICollectionViewCell *cell in [self.productListCollectionView visibleCells]) {
//        NSIndexPath *indexPath = [self.productListCollectionView indexPathForCell:cell];
//        //NSLog(@"visible index %ld",(long)indexPath.item);
//        self.objPageControl.currentPage = indexPath.item;
//
//    }
//}
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
        return 1;
        
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
        return _servicesImageArray.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
        CGFloat width = (kSCREEN_WIDTH-30)/5;
        return CGSizeMake(width, width);
        // return CGSizeMake(100, 40);
        
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
        return 5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
        return 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
        return UIEdgeInsetsMake(5, 5, 5,5);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
        
        static NSString *identifier = @"TopBrandsCollectionCell";
        TopBrandsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        //[[CommonSettings sharedInstance] setBorderToView:cell.bgView];
        
        switch (indexPath.item) {
                case 0:
                        [cell.brandImgView sd_setImageWithURL:[[_servicesImageArray objectAtIndex:4] valueForKey:@"img_url"]];
                        break;
                case 1:
                        [cell.brandImgView sd_setImageWithURL:[[_servicesImageArray objectAtIndex:3] valueForKey:@"img_url"]];
                        break;
                case 2:
                        [cell.brandImgView sd_setImageWithURL:[[_servicesImageArray objectAtIndex:2] valueForKey:@"img_url"]];
                        break;
                case 3:
                        [cell.brandImgView sd_setImageWithURL:[[_servicesImageArray objectAtIndex:0] valueForKey:@"img_url"]];
                        break;
                case 4:
                        [cell.brandImgView sd_setImageWithURL:[[_servicesImageArray objectAtIndex:1] valueForKey:@"img_url"]];
                        break;
                
                case 5:
                        [cell.brandImgView sd_setImageWithURL:[[_servicesImageArray objectAtIndex:5] valueForKey:@"img_url"]];
                        break;
                        
                default:
                        break;
        }
       
        
        return cell;
        
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        [_delegate serviceImageClicked:[_servicesOptionsArray objectAtIndex:indexPath.item]];
}


@end
