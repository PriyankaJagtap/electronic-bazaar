//
//  HotSellingBrandsTableCell.h
//  EB
//
//  Created by Neosoft on 6/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotSellingBrandsTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *firstProductTypeImg;

@property (weak, nonatomic) IBOutlet UIButton *firstProductTypeBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondproductTypeBtn;
@property (weak, nonatomic) IBOutlet UILabel *secondProductTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstProductTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *secondProducTypeImg;
@end
