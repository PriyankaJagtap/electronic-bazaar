//
//  HomeBannerCell.h
//  EB
//
//  Created by Neosoft on 12/27/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeBannerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *warrantyImgAspectRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *warrantyImgTop;
@property (weak, nonatomic) IBOutlet UIImageView *warrantyImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewBannerImgAspectRatio;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *motoLenovoImgAspectRatio;
@property (weak, nonatomic) IBOutlet UIImageView *motoLenovoImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *motoLenovoImgTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewBannerImgTopConstraint;


@property (weak, nonatomic) IBOutlet UIImageView *dealsOfTheDayImageView;
@property (weak, nonatomic) IBOutlet UIImageView *topRightBannerImageview;
@property (weak, nonatomic) IBOutlet UIImageView *openBoxMobileImageView;
@property (weak, nonatomic) IBOutlet UIImageView *openBoxLaptopImageView;
@property (weak, nonatomic) IBOutlet UIImageView *registerBannerImageView;
@property (weak, nonatomic) IBOutlet UIButton *dealsBtn;
@property (weak, nonatomic) IBOutlet UIImageView *preOwnedLaptopsImgview;
@property (weak, nonatomic) IBOutlet UIImageView *preOwnedMobilesImgview;
@property (weak, nonatomic) IBOutlet UITextField *mobileNoTextField;
@property (weak, nonatomic) IBOutlet UIImageView *hourlyBannerImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dealsAspectRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *registerBannerTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *patnerProgramTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *patnerProgramImgAspectRatio;
@property (weak, nonatomic) IBOutlet UIImageView *partnerProgramImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dealsTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *registerBannerAspectRatio;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *openBoxLaptopImgAspectRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *openBoxLaptopImgTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *openBoxMobileImgTopCostraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *openBoxMobileImgAspectRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preOwnedLaptopImgAspectRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preOwnedLaptopImgTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preOwnedMobileImgTopCostraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preOwnedMobileImgAspectRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gstBannerImgAspectRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gstBannerTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hourlyDealsImgTopCostraint;
@property (weak, nonatomic) IBOutlet UIImageView *gstBannerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *webViewBannerImg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hourlyDealsImgAspectRatio;
@end
