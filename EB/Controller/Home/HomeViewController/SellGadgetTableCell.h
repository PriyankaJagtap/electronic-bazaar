//
//  SellGadgetTableCell.h
//  EB
//
//  Created by Neosoft on 6/5/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellGadgetTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lenovoAspectRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lenovoTopconstratint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sellGadgetTopConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *lenovoImg;
@property (weak, nonatomic) IBOutlet UIImageView *sellYourGadgetImgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sellYourGadgetHt;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sellGadgetaspectRatio;

@end
