//
//  PreOwnedGuidlinesTableCell.h
//  EB
//
//  Created by Neosoft on 6/5/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreOwnedGuidlinesTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *preOwnedProductGdlinesImageView;
@property (weak, nonatomic) IBOutlet UIImageView *preOwnedMobileGdlinesImageview;
@property (weak, nonatomic) IBOutlet UIImageView *preOwnedLaptopGdlinesImageView;

@property (weak, nonatomic) IBOutlet UIButton *preOwnedProductGdlinesBtn;
@property (weak, nonatomic) IBOutlet UIButton *preOwnedMobileGdlinesBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preOwnedProductTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preOwnedMobileTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preOwnedGdLineMobileImgAspectRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preOwnedGdLineLaptopImgAspectRatio;
@property (weak, nonatomic) IBOutlet UIButton *preOwnedLaptopGdlinesBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preOwnedLaptopTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preOwnedGdLineProductImgAspectRatio;
@end
