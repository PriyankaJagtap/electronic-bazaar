//
//  HomeViewController.m
//  EB
//
//  Created by webwerks on 7/29/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "HomeViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "ProductListViewController.h"
#import "ProductDetailsViewController.h"
#import "ElectronicsViewController.h"
#import "RegistrationViewController.h"
#import "LoginViewController.h"
#import "MyWishlist.h"
#import "CommonSettings.h"
#import "SearchViewController.h"
#import "DealsViewController.h"
#import "HomeDealsViewController.h"
#import "AppDelegate.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "SuperSaleViewController.h"
#import "PartnerProgramViewController.h"
#import "LenovoLoginViewController.h"
#import "WarrantyViewController.h"
#import "BulkOrderViewController.h"
#import "HomeHeaderTableCell.h"
#import "PreOwnedGuidlinesTableCell.h"
#import "SellGadgetTableCell.h"
#import "TopBrandsTableCell.h"
#import "EbAdvantagesTableCell.h"
#import "ProductTableCell.h"
#import "HotSellingBrandsTableCell.h"
#import "GSTViewController.h"
#import "SellYourGadgetLoginViewController.h"
#import "UAEChooseCountryViewController.h"
#import "MyPurchasesViewController.h"
#import "MyAccountViewController.h"
#import "NewSearchViewController.h"
#import "AccessoriesTableCell.h"
#import "ServiceCenterViewController.h"
#import "PaymentOptionTableCell.h"
#import "PopupViewController.h"
#import "SplashScreenViewController.h"
#import "VowDelightLandingPageViewController.h"

#define ROWHEIGHT 150.0
#define SECTION_HEADER_HEIGHT 30.0

#define ASPECT_RATIO_SLIDER_IMG 1.7684
#define PADDING 5.0

#define HEADER_HT_SELL_GADGET_IMG 1007
#define HEADER_HT_LENOVO_IMG 1007

#define HEADER_HT_BOTH_IMG 1200
#define HEADER_HT 880
#define NEW_VERSION @"New version available"
#define UPDATE_MSG @"Important bug fixes for a better experience throughout the app"

#define HOT_SELLING_BRANDS @"Hot Selling Brands"
#define EB_ADVANTAGES @"Eb Advantages" //

#define SELL_YOUR_GADGET @"Sell Your Gadget" //
#define PRE_OWNED_GUIDLINES @"Pre Owned Guidelines"
#define FEATURED_PRODUCTS @"Featured Products"
#define BEST_SELLERS @"BEST SELLER"
#define TOP_BRANDS @"TOP BRANDS"
#define ACCESSORIES @"TOP ACCESSORIES"
#define RECENTLY_VIEWED @"RECENTLY VIEWED"
#define PAYMENT_OPTIONS @"PAYMENT OPTIONS"
#define SERVICE_OPTIONS @"SERVICE OPTIONS"
#define BULK_ORDER @"Bulk Order"

#define BRAND_IMG_SCALE_FACTOR 1.16

#define FREE_SHIPPING @"Free Shipping"
#define ONE_YEAR_WARRANTY @"EB Warranty"
#define EB_SERVICE_CENTER @"Eb Service Center"
#define PRE_OWNED_PRODUCTS @"Certified Pre Owned Products"
#define MICROSOFT_AUTHORIZED_REFURBISHER @"Microsoft Authorized Refurbisher"
#define WIDTH_HEIGHT_RATIO 0.714

//[NSMutableArray arrayWithObjects:@"",@"Hot Selling Brands",@"Recently Viewed", @"Pre Owned Guidelines",@"Featured Products",@"Top Brands",nil];
#import "HomeBannerTableViewCell.h"
#import "HomeBannerCell.h"
#import "ServiceOptionsTableCell.h"
#import "SYGOrderConfirmationViewController.h"
#import "CCAFormVC.h"

@interface HomeViewController ()<TopBrandsDelegate,CustomCollectionViewDelegate,StorePopUpDelegate,AccessoryViewDelegate,ServiceOptionsDelegate,PaymentOptionsDelegate>{
    NSMutableArray *sectionArr;
    NSMutableDictionary *dictHomeDetail;
    NSMutableArray *hotDealsArr;
    NSMutableArray *featuredproductsArr;
    NSMutableArray *categoryArr;
    NSMutableArray *sliderImagesArr;
    UIPageControl *pageControl;
    __weak IBOutlet UISearchBar *searchBar;
    
    //Banner view
    int intMaxLength,intStartPosition,pagecontrol_count;
    UIScrollView *imageSlider;
    NSTimer *timerForImageSlider;
    UIView *containBannerview;
    NSMutableArray *bannerArr;
    UIButton *favouriteButton;
    NSDate *startTime;
    int responseCount;
    
    CGRect keyboardBounds;
    UITextField *mobileNoTextField;
    
    float slideImagesAfterTime;
    NSMutableArray *hotSellingBrandArr;
    NSDictionary *hotSellingDataDic;
    
    NSMutableArray *bestsellersArr;
    NSArray *topBrandsArray;
    NSMutableArray *accessoriesArray;
    NSMutableArray *recentlyViewedArray;
    NSArray *paymentMethodsImagesArray;
    
    int selectedFavBtnTag;
    int selectedCollectionViewTag;
    NSArray *serviceOptionArr;
    NSArray *serviceOptionImagesArr;
    NSDictionary *sygNotificationDic;
    
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationBarHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarHtConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIButton *leftSideMenuBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightSideMenuBtn;
@property (weak, nonatomic) IBOutlet UIButton *buttonFlag;

@end

@implementation HomeViewController
@synthesize isSearch;

static NSString *dealsBannerType = @"home-page";
static NSString *registerBannerType = @"register";
static NSString *gstBannerType = @"gst";
static NSString *motoLenovoBannerType = @"moto_lenovo";
static NSString *webViewBannerType = @"webview";
static NSString *patnerProgramBannerType = @"partner-program";
static NSString *hourlyDealBannerType = @"hourly-deal";
static NSString *openBoxLaptopBannerType = @"open-box-laptop";
static NSString *openBoxMobileBannerType = @"open-box-mobile";
static NSString *preOwnedLaptopBannerType = @"pre-owned-laptop";
static NSString *preOwnedMobileBannerType = @"pre-owned-mobile";
static NSString *ebAdvantagesBannerType = @"eb_advantages";
static NSString *recentlyViewedBannerType = @"recently_viewed";
static NSString *preOwnedGuidlinesBannerType = @"preowned_guidelines";
static NSString *preOwnedProductGuidlinesBannerType = @"preowned_guidelines_product";
static NSString *preOwnedMobileGuidlinesBannerType = @"preowned_guidelines_mobile";
static NSString *preOwnedLaptopGuidlinesBannerType = @"preowned_guidelines_laptop";
static NSString *topBrandsBannerType = @"top_brands";
static NSString *searchBannerType = @"search";
static NSString *keywordBannerType = @"keyword";
static NSString *flagBannerType = @"flag";
static NSString *featureProductsBannerType = @"featured_products";
static NSString *hotSellingBannerType = @"hot_selling";

#pragma mark - Initialization
- (void)viewDidLoad {
    [super viewDidLoad];
    _firstTimeBool = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didbecomeActiveWithdata:)     name:UIApplicationDidBecomeActiveNotification object:nil];
    
    serviceOptionArr = [NSArray arrayWithObjects: FREE_SHIPPING, ONE_YEAR_WARRANTY, EB_SERVICE_CENTER, EB_ADVANTAGES,PRE_OWNED_PRODUCTS, MICROSOFT_AUTHORIZED_REFURBISHER, nil];
    
    //        serviceOptionImagesArr = [NSArray arrayWithObjects:@"blue_free_shipping_icon",@"blue_eb_warranty_icon",@"blue_eb_service_center_icon",@"blue_eb_advantage_icon",@"blue_pre_owned_products", nil];
    
    self.tableView.bounces=YES;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.sectionFooterHeight = 0.0;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popUpViewTapped)];
    [_popUpView addGestureRecognizer:tapped];
    [CommonSettings setUpSearchBar:searchBar];
    [[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:@"Helvetica" size:12.0]];
    
    //[searchBar setScopeBarButtonTitleTextAttributes:karlaFontRegular(16) forState:UIControlStateNormal];
    
    //        searchBar.placeholder = @"Search";
    //    searchBar.placeholder = SELL_GADGET_SEARCH_HINT;
    NSLog(@"home view did load called");
    //        _suvidhaBgView.layer.cornerRadius = 4;
    //        _suvidhaBgView.layer.borderColor = [UIColor whiteColor].CGColor;
    //        _suvidhaBgView.layer.borderWidth = 0.5;
}

-(void)popUpViewTapped
{
    CATransition *transition = [CATransition animation];
    transition.duration = 2.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [_popUpView.layer addAnimation:transition forKey:nil];
    [_popUpView setHidden:YES];
}

-(void)didbecomeActiveWithdata:(NSDictionary *)dict {
    
}
-(void)didbecomeActiveOnNotification{
    if (self.notificationDict!=nil) {
        [self handleNotifications];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
    [[[AppDelegate getAppDelegateObj]tabBarObj] TabClickedIndex:0];
    [[[AppDelegate getAppDelegateObj]tabBarObj] unhideTabbar];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    if (self.notificationDict!=nil) {
        [self handleNotifications];
    }
    else
    {
        [self initialSetUp];
    }
    self.notificationDict=nil;
}

-(void)handleNotifications
{
    if (self.notificationDict!=nil) {
        UIStoryboard *storyBoard;
        storyBoard= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        if([self.notificationDict objectForKey:@"gcm.notification.url_type"])
        {
            if([[self.notificationDict objectForKey:@"gcm.notification.url_type"]isEqualToString:@"product"])
            {
                ProductDetailsViewController *objProductDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
                int productId=[[self.notificationDict valueForKey:@"gcm.notification.product_id"]intValue];
                objProductDetailVC.productID=productId;
                [self.navigationController pushViewController:objProductDetailVC animated:YES];
            }
            else  if([[self.notificationDict objectForKey:@"gcm.notification.url_type"]isEqualToString:@"category"])
            {
                ElectronicsViewController *electronicviewObjc = [storyBoard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
                NSString *cat_id = [NSString stringWithFormat:@"%d",[[self.notificationDict valueForKey:@"gcm.notification.category_id"]intValue]];
                electronicviewObjc.category_id=cat_id;
                electronicviewObjc.categoryVal= [self.notificationDict valueForKey:@"gcm.notification.category_name"];
                [self.navigationController pushViewController:electronicviewObjc animated:YES];
            }
        }
        else if ([[self.notificationDict valueForKey:@"gcm.notification.type"]isEqualToString:@"app_update"])
        {
            UIAlertView *objAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Do you want to update app?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No", nil];
            objAlert.tag = 500;
            [objAlert show];
            
        }
        else if ([[self.notificationDict valueForKey:@"gcm.notification.type"]isEqualToString:@"syg_link"])
        {
            UIAlertView *objAlert=[[UIAlertView alloc]initWithTitle:@"" message:[[[self.notificationDict valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"] delegate:self cancelButtonTitle:nil otherButtonTitles:CONFIRM,NOT_INTERSTED, nil];
            sygNotificationDic = self.notificationDict;
            objAlert.tag = 600;
            [objAlert show];
            
        }
        else if ([[self.notificationDict valueForKey:@"gcm.notification.type"]isEqualToString:@"link"])
        {
            
            NSString *string = [self.notificationDict valueForKey:@"gcm.notification.link_url"] ;
            if ([string rangeOfString:@"deals"].location != NSNotFound) {
                [self navigateToDealsView];
            }
            else  if ([string rangeOfString:@"splash"].location != NSNotFound) {
                
                SplashScreenViewController *vc = [[SplashScreenViewController alloc] initWithNibName:@"SplashScreenViewController" bundle:nil];
                [[[AppDelegate getAppDelegateObj] tabBarObj] hideTabbar];
                [self.navigationController pushViewController:vc animated:YES];
                
            }
            else
            {
                NSLog(@"string does not contain deals");
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self.notificationDict valueForKey:@"gcm.notification.link_url"]]];
            }
        }
    }
}

-(void)initialSetUp
{
    self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
    timerForImageSlider = 0;// Set banner scrolling timer to 0 initially
    [CommonSettings sendScreenName:@"HomeView"];
    [self callAppVersionWs];
}

-(void)callAppVersionWs
{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
    NSLog(@"current version %f",[currentVersion floatValue]);
    
    // Loader
    if(_firstTimeBool)
    {
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:NO];
    }
    
    // Call Home screen webservice
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        responseCount=0;
        startTime = [NSDate date];
        NSLog(@"startTime %@",startTime);
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:GET_APP_VERSION_WS MathodName:GET_APP_VERSION_WS];
    }
}

-(void)displayUpdateAppAlert
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.tabBarObj hideTabbar];
    
    self.view.userInteractionEnabled = NO;
    _navigationBarHtConstraint.constant = 0;
    _leftSideMenuBtn.hidden = YES;
    _rightSideMenuBtn.hidden = YES;
    _logoImageView.hidden = YES;
    _searchBarHtConstraint.constant = 0;
    self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
    
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New Version!!" message: @"A new version of app is available to download" delegate:self cancelButtonTitle:@"Not Now" otherButtonTitles: @"Update Now", nil];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New Version!!" message: @"We have introduced new features in the app, click ok to upgrade to newer version now." delegate:self cancelButtonTitle:@"Not Now" otherButtonTitles: @"Ok", nil];
    alert.tag = 500;
    [alert show];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if ([timerForImageSlider isValid]) {
        [timerForImageSlider invalidate];
    }
    timerForImageSlider = nil;
    [_popUpView setHidden:YES];
}

#pragma mark - IBAction methods

- (IBAction)suvidhaBtnClicked:(id)sender {
    
    WarrantyViewController *objVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([CommonSettings isLoggedInUser])
        
    {
        objVc.webViewUrl = [NSString stringWithFormat:@"https://www.electronicsbazaar.com/shubhlabh?cemail=%@&cpassword=%@",[defaults valueForKey:@"email"],[defaults valueForKey:@"password"]];
    }
    else
    {
        objVc.webViewUrl = @"https://www.electronicsbazaar.com/shubhlabh/";
    }
    
    NSLog(@"webView url %@",objVc.webViewUrl);
    
    objVc.titleLabel = @"Shubh Labh";
    objVc.bannerType = webViewBannerType;
    [self.navigationController pushViewController:objVc animated:YES];
}
- (IBAction)sellYourGadgetBtnClicked:(id)sender {
    [CommonSettings navigateToSellYourGadgetView];
}

- (IBAction)loginToViewPriceBtnClicked:(id)sender {
    [APP_DELEGATE navigateToLoginViewController];
}

- (IBAction)buttonFlagAction:(id)sender
{
    //show the popup:-
    UAEChooseCountryViewController *popUpController=[[UAEChooseCountryViewController alloc] initWithNibName:@"UAEChooseCountryViewController" bundle:nil];
    popUpController.popUpDelegate= self;
    [self addChildViewController:popUpController];
    popUpController.view.frame = self.view.frame;
    
    // [popUpController.view setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:popUpController.view];
    [popUpController didMoveToParentViewController:self];
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^
     {
         popUpController.view.alpha = 1;
         [popUpController indiaRadioButtonAction:nil];
     }
                     completion:nil];
}

#pragma mark - Store Popup delegate method
-(void)storeClicked:(NSString*)storeName{
    if ([storeName isEqualToString:UAE_STROE]) {
        [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:SELECTED_STORE];
        [APP_DELEGATE switchToUAEStoreApplication];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:SELECTED_STORE];
    }
}

#pragma mark - search bar delegate methods
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar1
{
    [searchBar1 setShowsCancelButton:YES animated:YES];
    NewSearchViewController *objSearchVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"SearchViewController"];
    //objSearchVC.navController=self.navigationController;
    [searchBar1 resignFirstResponder];
    [self.navigationController pushViewController:objSearchVC animated:NO];
    //[self presentViewController:objSearchVC animated:YES completion:nil];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar1
{
    [searchBar1 setShowsCancelButton:NO animated:YES];
}

#pragma mark - keyboard notification methods
-(UIToolbar *)setToolBarNumberKeyBoard:(id)sender
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:sender action:@selector(doneButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Done" forState:UIControlStateNormal];
    button.frame = CGRectMake(0 ,0,60,40);
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 40)];
    [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
    
    return toolbar;
}

-(void)doneButtonDidPressed:(id)sender
{
    [mobileNoTextField resignFirstResponder];
    [_tableView setContentOffset:CGPointMake(0, 0)];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sectionArr.count;
    //return 1;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
        CGFloat width = CGRectGetWidth(tableView.bounds);
        //        CGFloat height = [self tableView:tableView heightForHeaderInSection:section];
        
        CGFloat height = kSCREEN_WIDTH/ASPECT_RATIO_SLIDER_IMG;
        
        containBannerview = [[UIView alloc] initWithFrame:CGRectMake(0,0,width,height)] ;
        containBannerview.backgroundColor=[UIColor whiteColor];
        if (sliderImagesArr.count > 0) {
            [self createbannerview:containBannerview];
        }
        else{
            UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
            //..img.contentMode = UIViewContentModeScaleAspectFit;
            [containBannerview addSubview:img];
        }
        return containBannerview;
    }
    else
    {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return kSCREEN_WIDTH/ASPECT_RATIO_SLIDER_IMG;
    }
    else
    {
        return 0.0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    //        if([[sectionArr objectAtIndex:section] isEqualToString:HOT_SELLING_BRANDS])
    //                return [hotSellingDataDic allKeys].count;
    if (section == 0)
    {
        if (serviceOptionImagesArr.count != 0)
            return 1;
        else
            return 0;
    }
    else
        return 1;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
 NSString *strTitle = [sectionArr objectAtIndex:section];
 if (section != 0) {
 return strTitle;
 }
 return nil;
 }*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        static NSString *simpleTableIdentifier = @"ServiceOptionsTableCell";
        ServiceOptionsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[ServiceOptionsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.servicesImageArray = serviceOptionImagesArr;
        cell.servicesOptionsArray = serviceOptionArr;
        cell.delegate = self;
        
        //CGFloat width = (kSCREEN_WIDTH-30)/5;
        cell.objCollectionViewHeightConstraint.constant = cell.objCollectionView.collectionViewLayout.collectionViewContentSize.height;
        
        return cell;
    }
    
    if([[sectionArr objectAtIndex:indexPath.section] isEqualToString:BEST_SELLERS] || [[sectionArr objectAtIndex:indexPath.section] isEqualToString:ACCESSORIES] ||[[sectionArr objectAtIndex:indexPath.section] isEqualToString:RECENTLY_VIEWED])
    {
        static NSString *cellIdentifier=@"AccessoriesTableCell";
        AccessoriesTableCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        if ([[sectionArr objectAtIndex:indexPath.section] isEqualToString:BEST_SELLERS]) {
            cell.accessoryDataArr = [bestsellersArr mutableCopy];
        }
        else  if ([[sectionArr objectAtIndex:indexPath.section] isEqualToString:ACCESSORIES]) {
            cell.accessoryDataArr = [accessoriesArray mutableCopy];
        }
        else  if ([[sectionArr objectAtIndex:indexPath.section] isEqualToString:RECENTLY_VIEWED]) {
            cell.accessoryDataArr = [recentlyViewedArray mutableCopy];
        }
        
        if(cell.accessoryDataArr.count > 2)
            cell.backwardForwardArrowLabel.hidden = NO;
        else
            cell.backwardForwardArrowLabel.hidden = YES;
        
        cell.titleLabel.text = [sectionArr objectAtIndex:indexPath.section];
        cell.delegate = self;
        cell.objCollectionView.tag = indexPath.section;
        [cell.objCollectionView reloadData];
        
        CGFloat cellWidth = (kSCREEN_WIDTH-15)/2;
        CGFloat cellHeight = 140 + (cellWidth * 0.53);
        [self.view layoutIfNeeded];
        //                cell.objCollectionViewHeightConstraint.constant = cell.objCollectionView.collectionViewLayout.collectionViewContentSize.height;
        cell.objCollectionViewHeightConstraint.constant = cellHeight;
        [self.view layoutIfNeeded];
        return cell;
    }
    
    if([[sectionArr objectAtIndex:indexPath.section] isEqualToString:PAYMENT_OPTIONS] )
    {
        static NSString *simpleTableIdentifier = @"PaymentOptionTableCell";
        PaymentOptionTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[PaymentOptionTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.paymentOptionsArray = paymentMethodsImagesArray;
        cell.delegate = self;
        
        //CGFloat width = (kSCREEN_WIDTH-30)/5;
        cell.objCollectionViewHeightConstraint.constant = cell.objCollectionView.collectionViewLayout.collectionViewContentSize.height;
        
        return cell;
    }
    
    if([[sectionArr objectAtIndex:indexPath.section] isEqualToString:TOP_BRANDS])
    {
        static NSString *simpleTableIdentifier = @"TopBrandsTableCell";
        TopBrandsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[TopBrandsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        cell.brandsImageArray = topBrandsArray;
        cell.delegate = self;
        
        CGFloat width = (kSCREEN_WIDTH-15)/2;
        cell.objCollectionViewHeightConstraint.constant = (width/BRAND_IMG_SCALE_FACTOR) + 10 ;
        
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - CustomCollectionViewDelegate
- (void) CollectionViewDidselctProduct:(int)productId
{
    //[self callProductDetailWsWithProductID:productId];
    
    ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    objProductDetailVC.productID=productId;
    [self.navigationController pushViewController:objProductDetailVC animated:YES];
}

- (void) CollectionViewDidSelectAddToCart:(int)productId
{
    [self callAddToCartWebservice:productId];
}

-(void)callAddToCartWebservice:(int)productId
{
    if ([CommonSettings isLoggedInUser])
    {
        Webservice  *callAddToCartService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:YES];
            
            callAddToCartService.delegate =self;
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            int quoteID=[[defaults valueForKey:@"QuoteID"]intValue];
            NSString *quotId;
            if (quoteID==0) {
                quotId=@"";
            }else{
                quotId=[NSString stringWithFormat:@"%d",quoteID];
            }
            NSDictionary *dictAddToCart=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:productId],@"ProductID",quotId,@"QuoteID", @"",WISHLIST_ITEM_ID,nil];
            [callAddToCartService operationRequestToApi:dictAddToCart url:ADD_TO_CART_WS string:@"ADDTOCART"];
        }
    }
    else
    {
        [[CommonSettings sharedInstance] showAlertTitle:@"" message:CART_LOGIN_REQUIRED_MSG];
    }
}

- (void) addProductToWishList:(int)productId withCollectionViewTag:(int)collectionViewTag andFavBtnTag:(int)favBtnTag
{
    selectedCollectionViewTag = collectionViewTag;
    selectedFavBtnTag = favBtnTag;
    
    if ([CommonSettings isLoggedInUser]) {
        
        NSMutableDictionary *productDic;
        if([[sectionArr objectAtIndex:selectedCollectionViewTag] isEqualToString:BEST_SELLERS])
        {
            productDic = [[bestsellersArr objectAtIndex:selectedFavBtnTag] mutableCopy];
        }
        else  if([[sectionArr objectAtIndex:selectedCollectionViewTag] isEqualToString:ACCESSORIES])
        {
            productDic = [[accessoriesArray objectAtIndex:selectedFavBtnTag] mutableCopy];
        }
        else  if([[sectionArr objectAtIndex:selectedCollectionViewTag] isEqualToString:RECENTLY_VIEWED])
        {
            productDic = [[recentlyViewedArray objectAtIndex:selectedFavBtnTag] mutableCopy];
        }
        
        if(![[productDic valueForKey:@"is_favourite"] boolValue])
        {
            [self didselectFavouriteProduct:productId];
            //                        [productDic setValue:[NSNumber numberWithBool:YES] forKey:@"is_favourite"];
            //                        [arrCategoryList replaceObjectAtIndex:btnFav.tag withObject:productDic];
        }
        else
        {
            [self removeProductFromFavourite:productId];
        }
    }else{
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
    }
}

-(void)reloadParticularCollectionViewItem:(NSMutableDictionary *)productDic
{
    AccessoriesTableCell *objCell = (AccessoriesTableCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:selectedCollectionViewTag]];
    [objCell.accessoryDataArr replaceObjectAtIndex:selectedFavBtnTag withObject:productDic];
    
    [objCell.objCollectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedFavBtnTag inSection:0]]];
}

-(void)didselectFavouriteProduct:(int)productId
{
    NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
    Webservice  *addToWishlistService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        if (userId==0) {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
        }else{
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            addToWishlistService.delegate =self;
            [addToWishlistService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%@&productId=%d",ADDTOWISHLIST,userId,productId] MathodName:@"ADDTOWISHLIST"];
        }
    }
}

-(void)removeProductFromFavourite:(int)productId
{
    NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
    Webservice  *addToWishlistService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        if (userId==0) {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
        }else{
            [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
            
            addToWishlistService.delegate =self;
            [addToWishlistService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%@&product_id=%d",REMOVE_FROM_WISHLIST_WS,userId,productId] MathodName:REMOVE_FROM_WISHLIST_WS];
        }
    }
}

#pragma mark- custom methods for banner
-(void)setBannerImages:(HomeBannerCell*)cell
{
    UITapGestureRecognizer *tap_recognizer;
    tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dealsOfTheDayImageClicked:)];
    [cell.dealsOfTheDayImageView addGestureRecognizer:tap_recognizer];
    
    tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(registerBannerImageClicked:)];
    [cell.registerBannerImageView addGestureRecognizer:tap_recognizer];
    
    tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gstBannerImageClicked:)];
    [cell.gstBannerImageView addGestureRecognizer:tap_recognizer];
    
    tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(motoLenovoImageClicked:)];
    [cell.motoLenovoImageView addGestureRecognizer:tap_recognizer];
    
    tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(webViewBannerImageClicked:)];
    [cell.webViewBannerImg addGestureRecognizer:tap_recognizer];
    
    tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topRightBannerImageClicked:)];
    [cell.topRightBannerImageview addGestureRecognizer:tap_recognizer];
    
    tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openBoxLaptopBannerImageClicked:)];
    [cell.openBoxLaptopImageView addGestureRecognizer:tap_recognizer];
    
    tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openBoxMobileBannerImageClicked:)];
    [cell.openBoxMobileImageView addGestureRecognizer:tap_recognizer];
    
    tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(partnerProgramImageClicked:)];
    [cell.partnerProgramImageView addGestureRecognizer:tap_recognizer];
    
    if(bannerArr != nil)
    {
        NSArray *registerImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",registerBannerType]];
        if(registerImgArr.count != 0)
        {
            cell.registerBannerAspectRatio.active = YES;
            cell.registerBannerTopConstraint.constant = 5;
            [cell.registerBannerImageView sd_setImageWithURL:[NSURL URLWithString:[[registerImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.registerBannerTopConstraint.constant = 0;
            cell.registerBannerAspectRatio.active = NO;
        }
        
        NSArray *gstImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",gstBannerType]];
        if(gstImgArr.count != 0)
        {
            cell.gstBannerImgAspectRatio.active = YES;
            cell.gstBannerTopConstraint.constant = 5;
            
            [cell.gstBannerImageView sd_setImageWithURL:[NSURL URLWithString:[[gstImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.gstBannerTopConstraint.constant = 0;
            cell.gstBannerImgAspectRatio.active = NO;
        }
        
        NSArray *motoLenovoImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",motoLenovoBannerType]];
        if(motoLenovoImgArr.count != 0)
        {
            cell.motoLenovoImgAspectRatio.active = YES;
            cell.motoLenovoImgTopConstraint.constant = 5;
            [cell.motoLenovoImageView sd_setImageWithURL:[NSURL URLWithString:[[motoLenovoImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.motoLenovoImgTopConstraint.constant = 0;
            cell.motoLenovoImgAspectRatio.active = NO;
        }
        
        NSArray *webViewBannerImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",webViewBannerType]];
        if(webViewBannerImgArr.count != 0)
        {
            cell.webViewBannerImgAspectRatio.active = YES;
            cell.webViewBannerImgTopConstraint.constant = 5;
            [cell.webViewBannerImg sd_setImageWithURL:[NSURL URLWithString:[[webViewBannerImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.webViewBannerImgTopConstraint.constant = 0;
            cell.webViewBannerImgAspectRatio.active = NO;
        }
        
        tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(warrantyImageClicked:)];
        [cell.warrantyImageView addGestureRecognizer:tap_recognizer];
        NSArray *warrantyImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",@"warranty"]];
        
        if(warrantyImgArr.count != 0)
        {
            cell.warrantyImgTop.constant = 5;
            cell.warrantyImgAspectRatio.active = YES;
            [cell.warrantyImageView sd_setImageWithURL:[NSURL URLWithString:[[warrantyImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.warrantyImgTop.constant = 0;
            cell.warrantyImgAspectRatio.active = NO;
        }
        //sell-your-gadget
        
        NSArray *arr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",hourlyDealBannerType]];
        if(arr.count != 0  && [[arr objectAtIndex:0] objectForKey:@"banner_image"])
        {
            cell.hourlyDealsImgTopCostraint.constant = 5;
            cell.hourlyDealsImgAspectRatio.active = YES;
            [cell.hourlyBannerImageView sd_setImageWithURL:[NSURL URLWithString:[[arr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.hourlyDealsImgTopCostraint.constant = 0;
            cell.hourlyDealsImgAspectRatio.active = NO;
        }
        
        NSArray *openBoxLaptopImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",openBoxLaptopBannerType]];
        if(openBoxLaptopImgArr.count != 0)
        {
            [cell.openBoxLaptopImageView sd_setImageWithURL:[NSURL URLWithString:[[openBoxLaptopImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.openBoxLaptopImgTopConstraint.constant = 0;
            cell.openBoxLaptopImgAspectRatio.active = NO;
        }
        
        NSArray *openBoxMobileImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",openBoxMobileBannerType]];
        if(openBoxMobileImgArr.count != 0)
        {
            [cell.openBoxMobileImageView sd_setImageWithURL:[NSURL URLWithString:[[openBoxMobileImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.openBoxMobileImgTopCostraint.constant = 0;
            cell.openBoxMobileImgAspectRatio.active = NO;
        }
        
        NSArray *preOwnedLaptopImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",preOwnedLaptopBannerType]];
        if(preOwnedLaptopImgArr.count != 0)
        {
            [cell.preOwnedLaptopsImgview sd_setImageWithURL:[NSURL URLWithString:[[preOwnedLaptopImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.preOwnedLaptopImgTopConstraint.constant = 0;
            cell.preOwnedLaptopImgAspectRatio.active = NO;
        }
        
        NSArray *preOwnedMobileImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",preOwnedMobileBannerType]];
        if(preOwnedMobileImgArr.count != 0)
        {
            [cell.preOwnedMobilesImgview sd_setImageWithURL:[NSURL URLWithString:[[preOwnedMobileImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.preOwnedMobileImgTopCostraint.constant = 0;
            cell.preOwnedMobileImgAspectRatio.active = NO;
        }
        
        tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(preOwnedMobilesImageClicked:)];
        [cell.preOwnedMobilesImgview addGestureRecognizer:tap_recognizer];
        
        tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(preOwnedLaptopsImageClicked:)];
        [cell.preOwnedLaptopsImgview addGestureRecognizer:tap_recognizer];
        
        NSArray *dealsImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",dealsBannerType]];
        if(dealsImgArr.count != 0)
        {
            cell.dealsTopConstraint.constant = 5;
            cell.dealsAspectRatio.active = YES;
            [cell.dealsOfTheDayImageView sd_setImageWithURL:[NSURL URLWithString:[[dealsImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.dealsTopConstraint.constant = 0;
            cell.dealsAspectRatio.active = NO;
        }
        
        NSArray *patnerImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",patnerProgramBannerType]];
        if(patnerImgArr.count != 0)
        {
            cell.patnerProgramTopConstraint.constant = 0;
            cell.patnerProgramImgAspectRatio.active = YES;
            [cell.partnerProgramImageView sd_setImageWithURL:[NSURL URLWithString:[[patnerImgArr objectAtIndex:0] valueForKey:@"banner_image"]]];
        }
        else
        {
            cell.patnerProgramTopConstraint.constant = 0;
            cell.patnerProgramImgAspectRatio.active = NO;
        }
    }
}

-(float)getHeaderHeight
{
    //    float partnerImgRatio = 2.12;
    //    float lenovoImgRatio = 1.76;
    //    float registerImgRatio = 2.09;
    //    float dealsImgRatio = 3.87;
    //    float preOwnedImgRatio = 2.7;
    
    float imgWidth = kSCREEN_WIDTH - 10;
    float lenovoImgHt = (imgWidth / 1.76);
    float warrantyImgHt = (imgWidth / 2.09);
    float dealsImgHt = (imgWidth / 3.87);
    float registerImgHt = (imgWidth / 2.09);
    float partnerImgHt = (imgWidth / 2.12);
    //    float preOwnedImgHt = (imgWidth / 2.7)*4;
    float preOwnedImgHt = (imgWidth / 2.7);
    
    float headerHt = 15 ;
    NSArray *sellImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",@"sell-your-gadget"]];
    
    NSArray *lenovoImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",@"lenovo-program"]];
    
    if(sellImgArr.count != 0)
    {
        headerHt += lenovoImgHt +5;
    }
    if (lenovoImgArr.count != 0) {
        headerHt += lenovoImgHt +5;
    }
    NSArray *warrantyImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",@"warranty"]];
    
    if(warrantyImgArr.count != 0)
    {
        headerHt += warrantyImgHt;
    }
    
    NSArray *dealsImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",dealsBannerType]];
    if(dealsImgArr.count != 0)
    {
        headerHt += dealsImgHt + 5;
    }
    
    NSArray *registerImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",registerBannerType]];
    if(registerImgArr.count != 0)
    {
        headerHt += registerImgHt + 5;
    }
    
    NSArray *patnerImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",patnerProgramBannerType]];
    if(patnerImgArr.count != 0)
    {
        headerHt += partnerImgHt + 5;
    }
    
    NSArray *hourlyDealarr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",hourlyDealBannerType]];
    if(hourlyDealarr.count != 0  && [[hourlyDealarr objectAtIndex:0] objectForKey:@"banner_image"])
    {
        headerHt += partnerImgHt + 5;
    }
    
    NSArray *openBoxLaptopImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",openBoxLaptopBannerType]];
    if(openBoxLaptopImgArr.count != 0)
    {
        headerHt += preOwnedImgHt + 5;
    }
    
    NSArray *openBoxMobileImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",openBoxMobileBannerType]];
    if(openBoxMobileImgArr.count != 0)
    {
        headerHt += preOwnedImgHt + 5;
    }
    
    NSArray *preOwnedLaptopImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",preOwnedLaptopBannerType]];
    if(preOwnedLaptopImgArr.count != 0)
    {
        headerHt += preOwnedImgHt + 5;
    }
    
    NSArray *preOwnedMobileImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",preOwnedMobileBannerType]];
    if(preOwnedMobileImgArr.count != 0)
    {
        headerHt += preOwnedImgHt + 5;
    }
    return headerHt;
}

#pragma mark - ViewAll
-(void)viewAllAction:(UIButton *)sender{
    
    UIStoryboard *objStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if ([sender tag] == 1) {
        // HotDeals
        ProductListViewController *prodListObj = [objStoryboard instantiateViewControllerWithIdentifier:@"ProductListViewController"];
        prodListObj.viewAllListTitle.text = @"Hot Deals";
        prodListObj.type = @"hotdeals";
        prodListObj.page_no = @"1";
        prodListObj.page_size = @"10";
        [self.navigationController pushViewController:prodListObj animated:YES];
        
    }
    else if ([sender tag] == 2){
        // bestseller
        ProductListViewController *prodListObj = [objStoryboard instantiateViewControllerWithIdentifier:@"ProductListViewController"];
        prodListObj.viewAllListTitle.text = @"Best Sellers";
        prodListObj.type = @"bestsellers";
        prodListObj.page_no = @"1";
        prodListObj.page_size = @"10";
        [self.navigationController pushViewController:prodListObj animated:YES];
        
    }
    else if ([sender tag] == 3){
        // featuredproducts
        ProductListViewController *prodListObj = [objStoryboard instantiateViewControllerWithIdentifier:@"ProductListViewController"];
        prodListObj.viewAllListTitle.text = @"Featured Products";
        prodListObj.type = @"featuredproducts";
        prodListObj.page_no = @"1";
        prodListObj.page_size = @"10";
        [self.navigationController pushViewController:prodListObj animated:YES];
    }
}

#pragma mark - Slide Menu
- (IBAction)leftSlideMenuAction:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)callCategoryAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case 4:
        {
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"home-banner"
                                                                  action:@"click"
                                                                   label:@"mobiles-app"
                                                                   value:nil] build]];
        }
            break;
        case 3:
        {
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"home-banner"
                                                                  action:@"click"
                                                                   label:@"laptops-app"
                                                                   value:nil] build]];        }
            break;
        case 7:
        {
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"home-banner"
                                                                  action:@"click"
                                                                   label:@"accessories-app"
                                                                   value:nil] build]];
        }
            break;
        default:
            break;
    }
    
    ElectronicsViewController  *electronicVC= [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    
    electronicVC.category_id = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    electronicVC.categoryVal = @"Category";
    [self.navigationController pushViewController:electronicVC animated:YES];
}

- (IBAction)rightSlideMenuAction:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

#pragma mark- Show banner
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

-(void)createbannerview:(UIView *)container {
    imageSlider = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, container.frame.size.width,container.frame.size.height )];
    [container addSubview:imageSlider];
    
    [imageSlider setContentOffset:CGPointMake(0, 0) animated:YES];
    [imageSlider setShowsHorizontalScrollIndicator:NO];
    [imageSlider setScrollEnabled:NO];
    imageSlider.pagingEnabled = YES;
    
    NSMutableArray *imagesArray = [NSMutableArray new];
    
    if (sliderImagesArr.count >0) {
        for (NSMutableDictionary *dict in sliderImagesArr) {
            NSString *imageUrl = [NSString stringWithFormat:@"%@",[dict objectForKey:@"banner_image"]];
            [imagesArray addObject:imageUrl];
        }
    }
    
    // Add page control on home banner
    pageControl = [[UIPageControl alloc] init];
    pageControl.frame = CGRectMake((container.frame.size.width / 2) - 50,container.frame.size.height - 20 , 100, 20);
    pageControl.numberOfPages = imagesArray.count;
    pageControl.pageIndicatorTintColor = [UIColor darkGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor yellowColor];
    
    pageControl.currentPage = 0;
    [container addSubview:pageControl];
    
    [imageSlider setContentSize:CGSizeMake([imagesArray count]*(container.frame.size.width), (container.frame.size.height))];
    
    intMaxLength = [imagesArray count] * (container.frame.size.width);
    
    int xPosition = 0;
    for(int i = 0 ; i < [imagesArray count] ; i++)
    {
        UIImageView *ivImage = [[UIImageView alloc] initWithFrame:CGRectMake(xPosition, 0, (container.frame.size.width), (container.frame.size.height))];
        ivImage.backgroundColor = [UIColor clearColor];
        
        [ivImage sd_setImageWithURL:[NSURL URLWithString:[imagesArray objectAtIndex:i]]];
        
        UITapGestureRecognizer *tap_recognizer;
        tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBannerImageAction:)];
        [imageSlider addGestureRecognizer:tap_recognizer];
        
        [imageSlider addSubview:ivImage];
        
        xPosition += (container.frame.size.width);
    }
    
    intStartPosition = 0;
    pagecontrol_count = 0;
    
    UISwipeGestureRecognizer *recognizer;
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    //[[self view] addGestureRecognizer:recognizer];
    [container addGestureRecognizer:recognizer];
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [container addGestureRecognizer:recognizer];
    
    // [[self view] addGestureRecognizer:recognizer];
    
    //    if (!timerForImageSlider) {
    [timerForImageSlider invalidate];
    timerForImageSlider = nil;
    timerForImageSlider = [NSTimer scheduledTimerWithTimeInterval:slideImagesAfterTime
                                                           target:self
                                                         selector:@selector(imageSlider)
                                                         userInfo:nil
                                                          repeats:YES];
    // }
    // [[NSRunLoop mainRunLoop] addTimer:timerForImageSlider forMode:NSDefaultRunLoopMode];
}

- (void)navigateToElectronicsViewController:(NSString *)categoryId categoryValue:(NSString *)categoryValue {
    ElectronicsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    if ([[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey: @"category_id"] != [NSNull null])
    {
        NSDictionary *dic = [sliderImagesArr objectAtIndex:pagecontrol_count];
        vc.category_id = categoryId;
        vc.categoryVal = categoryValue;
        vc.isSlideBannerClickedFromHomeScreen = YES;
        vc.filterDataStr = [NSString stringWithFormat:@"&%@=%@",[dic valueForKey:@"code"],[dic valueForKey: @"value"]];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(IBAction)clickBannerImageAction:(UITapGestureRecognizer*)sender
{
    DLog(@"%d",pagecontrol_count);
    if (sliderImagesArr.count > pagecontrol_count)
    {
        [CommonSettings eventTrackingForHomeScreen:[[sliderImagesArr objectAtIndex:pagecontrol_count] valueForKey:@"event_click_label"]];
        
        if ([[[sliderImagesArr objectAtIndex: pagecontrol_count] objectForKey: @"banner_type"] isEqualToString:@"product"])
        {
            ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
            if([[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"product_id"] != [NSNull null])
            {
                int prodId = [[[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"product_id"]intValue];
                objProductDetailVC.productID=prodId;
                [self.navigationController pushViewController:objProductDetailVC animated:YES];
            }
        }
        else  if ([[[sliderImagesArr objectAtIndex: pagecontrol_count]objectForKey: @"banner_type"] isEqualToString: @"category"])
        {
            NSString *categoryId = [NSString stringWithFormat:@"%d",[[[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"category_id"]intValue]];
            NSString *categoryValue = [[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"category_name"];
            [self navigateToElectronicsViewController:categoryId categoryValue:categoryValue];

        }
        else  if ([[[sliderImagesArr objectAtIndex: pagecontrol_count]objectForKey: @"banner_type"] isEqualToString: @"EPPCategory"])
        {
            NSString *categoryId = [NSString stringWithFormat:@"%d",[[[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"category_id"]intValue]];
            NSString *categoryValue = [[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"category_name"];
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"is_ibm"] isEqualToString:@"Yes"])
            {
                [self navigateToElectronicsViewController:categoryId categoryValue:categoryValue];
            }
            else
            {
                [APP_DELEGATE navigateToIBMLogin:CATEGORY withCategoryValue:categoryValue withCategoryId:categoryId];
            }
        }
        else if ([[[sliderImagesArr objectAtIndex: pagecontrol_count] objectForKey: @"banner_type"] isEqualToString: @"super_sale"])
        {
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SuperSaleViewController"] animated:YES];
        }
        else  if ([[[sliderImagesArr objectAtIndex: pagecontrol_count] objectForKey: @"banner_type"] isEqualToString: @"super_sale_new"])
        {
            [self navigateToDealsView];
        }
        else if ([[[sliderImagesArr objectAtIndex: pagecontrol_count] objectForKey: @"banner_type"] isEqualToString: searchBannerType])
        {
            ElectronicsViewController *objElectronicsVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
            if([[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey: @"category_id"] != [NSNull null])
            {
                NSDictionary *dic = [sliderImagesArr objectAtIndex:pagecontrol_count];
                int categoryId = [[[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"category_id"]intValue];
                objElectronicsVC.category_id=[NSString stringWithFormat:@"%d",categoryId];
                objElectronicsVC.isSlideBannerClickedFromHomeScreen = YES;
                objElectronicsVC.filterDataStr = [NSString stringWithFormat:@"&%@=%@",[dic valueForKey:@"code"],[dic valueForKey:@"value"]];
                objElectronicsVC.categoryVal=[[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"category_name"];
                
                [self.navigationController pushViewController:objElectronicsVC animated:YES];
            }
        }
        else if ([[[sliderImagesArr objectAtIndex: pagecontrol_count] objectForKey: @"banner_type"] isEqualToString:flagBannerType])
        {
            // [self buttonFlagAction:self];
        }
        else if ([[[sliderImagesArr objectAtIndex: pagecontrol_count] objectForKey: @"banner_type"] isEqualToString: keywordBannerType])
        {
            NSDictionary *dic = [sliderImagesArr objectAtIndex:pagecontrol_count];
            
            [searchBar setShowsCancelButton:YES animated:YES];
            NewSearchViewController *objSearchVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"SearchViewController"];
            objSearchVC.searchText = [dic valueForKey:@"category_name"];
            objSearchVC.navController=self.navigationController;
            
            [self.navigationController pushViewController:objSearchVC animated:YES];
            
            //[self presentViewController:objSearchVC animated:YES completion:nil];
        }
        else if ([[[sliderImagesArr objectAtIndex: pagecontrol_count] objectForKey: @"banner_type"] isEqualToString:webViewBannerType])
        {
            NSDictionary *dic = [sliderImagesArr objectAtIndex:pagecontrol_count];
            
            WarrantyViewController *objVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
            if([[dic valueForKey:@"code"] isEqualToString:@"Shubh Labh"])
            {
                NSString *url = [dic valueForKey:@"value"];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                if([CommonSettings isLoggedInUser])
                {
                    url = [NSString stringWithFormat:@"%@?cemail=%@&cpassword=%@",[dic valueForKey:@"value"],[defaults valueForKey:@"email"],[defaults valueForKey:@"password"]];
                }
                objVc.webViewUrl = url;
            }
            else
            {
                NSString *url = [dic valueForKey:@"value"];
                if([url containsString:@"vowdelight"])
                {
                    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
                    VowDelightLandingPageViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"VowDelightLandingPageViewController"];
                    vc.webViewUrl = [dic valueForKey:@"value"];
                    [self.navigationController pushViewController:vc animated:YES];
                    return;
                }
                objVc.webViewUrl = [dic valueForKey:@"value"];
            }
            
            NSLog(@"webView url %@",objVc.webViewUrl);
            objVc.titleLabel = [dic valueForKey:@"code"];
            objVc.bannerType = webViewBannerType;
            [self.navigationController pushViewController:objVc animated:YES];
        }
    }
}

-(IBAction)handleSwipeFrom:(UISwipeGestureRecognizer*)sender
{
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        if(intMaxLength <=intStartPosition+containBannerview.frame.size.width )
        {
            return;
        }
        else
        {
            if (sliderImagesArr.count-1 > pagecontrol_count) {
                pagecontrol_count ++;
                pageControl.currentPage = pagecontrol_count;
                
            }
            intStartPosition += containBannerview.frame.size.width;
            [imageSlider setContentOffset:CGPointMake(intStartPosition,0) animated:YES];
            pageControl.currentPage = pagecontrol_count;
        }
    }
    else if(sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        if(0 > intStartPosition-(containBannerview.frame.size.width) )
        {
            return;
        }
        else
        {
            if (pagecontrol_count > 0) {
                pagecontrol_count --;
                pageControl.currentPage = pagecontrol_count;
            }            intStartPosition -= (containBannerview.frame.size.width);
            [imageSlider setContentOffset:CGPointMake(intStartPosition,0) animated:YES];
        }
    }
}

-(void)imageSlider
{
    if(intMaxLength <=intStartPosition+(containBannerview.frame.size.width) )
    {
        [imageSlider setContentOffset:CGPointMake(0,0) animated:NO];
        intStartPosition = 0;
        pagecontrol_count =0;
        pageControl.currentPage = pagecontrol_count;
    }
    else
    {
        pagecontrol_count ++;
        intStartPosition += (containBannerview.frame.size.width);
        [imageSlider setContentOffset:CGPointMake(intStartPosition,0) animated:YES];
        pageControl.currentPage = pagecontrol_count;
    }
}

#pragma mark - addToCart
-(void)addBestSellersProductToCart:(UIButton *)sender
{
    if ([sender.accessibilityLabel isEqualToString:@"0"]) {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"This product is out of stock."];
    }else{
        NSString *productImageURL;
        if ([[[bestsellersArr objectAtIndex:[sender tag]] valueForKey:@"product_images"]count]>0){
            productImageURL=[[[bestsellersArr objectAtIndex:[sender tag]] valueForKey:@"product_images"]objectAtIndex:0];
        }
        int productId=[[[bestsellersArr objectAtIndex:[sender tag]] valueForKey:@"product_id"]intValue];
        [self CollectionViewDidSelectAddToCart:productId];
    }
}

-(void)addFeaturedProductToCart:(UIButton *)sender
{
    if ([sender.accessibilityLabel isEqualToString:@"0"]) {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"This product is out of stock."];
    }else{
        NSString *productImageURL;
        if ([[[featuredproductsArr objectAtIndex:[sender tag]] valueForKey:@"product_images"]count]>0){
            productImageURL=[[[featuredproductsArr objectAtIndex:[sender tag]] valueForKey:@"product_images"]objectAtIndex:0];
        }
        int productId=(int)[[[featuredproductsArr objectAtIndex:[sender tag]] valueForKey:@"product_id"]intValue];
        
        [self CollectionViewDidSelectAddToCart:productId];
    }
}

#pragma mark - banner images clicked
-(void)dealsBtnClicked
{
    [self navigateToDealsView];
}

-(void)sellGadgetImageClicked:(UITapGestureRecognizer*)sender
{
    [CommonSettings navigateToSellYourGadgetView];
}

-(void)warrantyImageClicked:(UITapGestureRecognizer*)sender
{
    [CommonSettings eventTrackingForHomeScreen:@"eb-warranty"];
    WarrantyViewController *objVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
    
    objVc.warrantyBannerClicked = YES;
    [self.navigationController pushViewController:objVc animated:YES];
}
-(void)registerBannerImageClicked:(UITapGestureRecognizer*)sender
{
    [CommonSettings eventTrackingForHomeScreen:@"register-now-app"];
    
    RegistrationViewController *electronicviewObjc = [self.storyboard instantiateViewControllerWithIdentifier:@"RegistrationViewController"];
    electronicviewObjc.isFromHomeView = YES;
    [self.navigationController pushViewController:electronicviewObjc animated:YES];
}

-(void)gstBannerImageClicked:(UITapGestureRecognizer*)sender
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
    GSTViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"GSTViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
    
    //    [self addChildViewController:viewController];
    //    viewController.view.frame = self.view.frame;
    //    [self.view addSubview:viewController.view];
    //    viewController.view.alpha = 0;
    //    [viewController didMoveToParentViewController:self];
    //
    //    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^
    //     {
    //         viewController.view.alpha = 1;
    //     }
    //                     completion:nil];
}


-(void)motoLenovoImageClicked:(UITapGestureRecognizer*)sender
{
    NSArray *motoLenovoImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",motoLenovoBannerType]];
    
    //    [CommonSettings eventTrackingForHomeScreen:[[motoLenovoImgArr objectAtIndex:0] valueForKey:@"event_click_label"]];
    ElectronicsViewController *electronicviewObjc = [self.storyboard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    electronicviewObjc.categoryVal = [[motoLenovoImgArr objectAtIndex:0] valueForKey:@"category_name"];
    electronicviewObjc.category_id = [[motoLenovoImgArr objectAtIndex:0] valueForKey:@"category_id"];
    
    [self.navigationController pushViewController:electronicviewObjc animated:YES];
}

-(void)webViewBannerImageClicked:(UITapGestureRecognizer*)sender
{
    NSArray *webViewBannerImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",webViewBannerType]];
    
    // [CommonSettings eventTrackingForHomeScreen:[[webViewBannerImgArr objectAtIndex:0] valueForKey:@"event_click_label"]];
    
    WarrantyViewController *objVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
    objVc.webViewUrl = [[webViewBannerImgArr objectAtIndex:0] valueForKey:@"value"];
    objVc.titleLabel = [[webViewBannerImgArr objectAtIndex:0] valueForKey:@"code"];
    [self.navigationController pushViewController:objVc animated:YES];
}

-(void)topRightBannerImageClicked:(UITapGestureRecognizer*)sender
{
    NSLog(@"top right banner image clicked");
    
    [CommonSettings eventTrackingForHomeScreen:[[bannerArr objectAtIndex:0] valueForKey:@"event_click_label"]];
    
    ElectronicsViewController *electronicviewObjc = [self.storyboard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    
    electronicviewObjc.categoryVal = [[bannerArr objectAtIndex:0] valueForKey:@"category_name"];
    electronicviewObjc.category_id = [[bannerArr objectAtIndex:0] valueForKey:@"category_id"];
    [self.navigationController pushViewController:electronicviewObjc animated:YES];
}

-(void)openBoxMobileBannerImageClicked:(UITapGestureRecognizer*)sender
{
    NSLog(@"bottom right banner image clicked");
    //     [CommonSettings eventTrackingForHomeScreen:[[bannerArr objectAtIndex:2] valueForKey:@"event_click_label"]];
    NSArray *openBoxMobileImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",openBoxMobileBannerType]];
    
    [CommonSettings eventTrackingForHomeScreen:[[openBoxMobileImgArr objectAtIndex:0] valueForKey:@"event_click_label"]];
    ElectronicsViewController *electronicviewObjc = [self.storyboard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    electronicviewObjc.categoryVal = [[openBoxMobileImgArr objectAtIndex:0] valueForKey:@"category_name"];
    electronicviewObjc.category_id = [[openBoxMobileImgArr objectAtIndex:0] valueForKey:@"category_id"];
    
    [self.navigationController pushViewController:electronicviewObjc animated:YES];
}

-(void)openBoxLaptopBannerImageClicked:(UITapGestureRecognizer*)sender
{
    NSLog(@"middle right banner image clicked");
    
    //    [CommonSettings eventTrackingForHomeScreen:[[bannerArr objectAtIndex:1] valueForKey:@"event_click_label"]];
    
    NSArray *openBoxLaptopImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",openBoxLaptopBannerType]];
    
    [CommonSettings eventTrackingForHomeScreen:[[openBoxLaptopImgArr objectAtIndex:0] valueForKey:@"event_click_label"]];
    ElectronicsViewController *electronicviewObjc = [self.storyboard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    electronicviewObjc.categoryVal = [[openBoxLaptopImgArr objectAtIndex:0] valueForKey:@"category_name"];
    electronicviewObjc.category_id = [[openBoxLaptopImgArr objectAtIndex:0] valueForKey:@"category_id"];
    [self.navigationController pushViewController:electronicviewObjc animated:YES];
}

-(void)dealsOfTheDayImageClicked:(UITapGestureRecognizer*)sender
{
    NSLog(@"left banner image clicked");
    //    [CommonSettings eventTrackingForHomeScreen:[[bannerArr objectAtIndex:8] valueForKey:@"event_click_label"]];
    NSArray *dealsImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",dealsBannerType]];
    [CommonSettings eventTrackingForHomeScreen:[[dealsImgArr objectAtIndex:0] valueForKey:@"event_click_label"]];
    [self navigateToDealsView];
}

-(void)hourlyDealsImageClicked:(UITapGestureRecognizer*)sender
{
    NSLog(@"left banner image clicked");
    //    [CommonSettings eventTrackingForHomeScreen:[[bannerArr objectAtIndex:8] valueForKey:@"event_click_label"]];
    
    NSArray *hourlyDealarr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",hourlyDealBannerType]];
    [CommonSettings eventTrackingForHomeScreen:[[hourlyDealarr objectAtIndex:0] valueForKey:@"event_click_label"]];
    [self navigateToDealsView];
}

-(void)preOwnedLaptopsImageClicked:(UITapGestureRecognizer*)sender
{
    NSLog(@"preOwnedLaptops image clicked");
    //     [CommonSettings eventTrackingForHomeScreen:[[bannerArr objectAtIndex:6] valueForKey:@"event_click_label"]];
    
    NSArray *preOwnedLaptopImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",preOwnedLaptopBannerType]];
    NSDictionary *dic = [preOwnedLaptopImgArr objectAtIndex:0];
    
    [CommonSettings eventTrackingForHomeScreen:[dic valueForKey:@"event_click_label"]];
    
    //    ElectronicsViewController *electronicviewObjc = [[UIStoryboard storyboardWithName:@"Product" bundle:nil]instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    ElectronicsViewController *electronicviewObjc = [self.storyboard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    electronicviewObjc.categoryVal = [dic valueForKey:@"category_name"];
    electronicviewObjc.category_id = [dic valueForKey:@"category_id"];
    [self.navigationController pushViewController:electronicviewObjc animated:YES];
}

-(void)preOwnedMobilesImageClicked:(UITapGestureRecognizer*)sender
{
    NSLog(@"preOwnedMobiles image clicked");
    //    [CommonSettings eventTrackingForHomeScreen:[[bannerArr objectAtIndex:7] valueForKey:@"event_click_label"]];
    
    NSArray *preOwnedMobileImgArr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_click_label == %@)",preOwnedMobileBannerType]];
    NSDictionary *dic = [preOwnedMobileImgArr objectAtIndex:0];
    [CommonSettings eventTrackingForHomeScreen:[dic valueForKey:@"event_click_label"]];
    
    
    ElectronicsViewController *electronicviewObjc = [self.storyboard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
    electronicviewObjc.categoryVal = [dic valueForKey:@"category_name"];
    electronicviewObjc.category_id = [dic valueForKey:@"category_id"];
    [self.navigationController pushViewController:electronicviewObjc animated:YES];
}

-(void)partnerProgramImageClicked:(UITapGestureRecognizer*)sender
{
    [CommonSettings eventTrackingForHomeScreen:@"partner-program"];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    PartnerProgramViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"PartnerProgramViewController"];
    
    [self.navigationController pushViewController:objVc animated:YES];
}

-(void)lenovoImageClicked:(UITapGestureRecognizer*)sender
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if([[NSUserDefaults standardUserDefaults] valueForKey:LENOVO_LOGIN])
    {
        [AppDelegate getAppDelegateObj].isFromLenovoLogin = YES;
        [AppDelegate getAppDelegateObj].lenovo_userId = [[[NSUserDefaults standardUserDefaults] valueForKey:LENOVO_USERID] intValue];
        //                SellGadgestCategoryViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"SellGadgestCategoryViewController"];
        //                [self.navigationController pushViewController:objVc animated:YES];
    }
    else
    {
        LenovoLoginViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"LenovoLoginViewController"];
        
        [self.navigationController pushViewController:objVc animated:YES];
    }
}

#pragma mark - nacvigate to deals view
-(void)navigateToDealsView
{
    [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:3];
}

#pragma mark - custom methods
-(void)getHomePopUp
{
   // return;
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:GET_HOME_POP_UP_WS MathodName:GET_HOME_POP_UP_WS];
    }
}

-(void)getBannerImages
{
    // Loader
    //    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:NO];
    
    // Call Home screen webservice
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:GET_HOME_PAGE_BANNER MathodName:@"GET_HOME_PAGE_BANNER"];
    }
}

-(void)getServiceOptions
{
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:GET_HOME_SERVICE_OPTION_WS MathodName:GET_HOME_SERVICE_OPTION_WS];
    }
}

-(void)getBestSellers
{
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
        NSString *url;
        if (userId) {
            url = [NSString stringWithFormat:@"%@%@",GET_HOME_BEST_SELLER_WS,userId];
        }
        else
        {
            url = GET_HOME_BEST_SELLER_WS;
        }
        
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:url MathodName:GET_HOME_BEST_SELLER_WS];
    }
}

-(void)getTopBrands
{
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:GET_HOME_TOP_BRANDS_WS MathodName:GET_HOME_TOP_BRANDS_WS];
    }
}

-(void)getAccessories
{
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
        NSString *url;
        if (userId) {
            url = [NSString stringWithFormat:@"%@%@",GET_HOME_ACCESSORIES_WS,userId];
        }
        else
        {
            url = GET_HOME_ACCESSORIES_WS;
        }
        
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:url MathodName:GET_HOME_ACCESSORIES_WS];
    }
}

-(void)getRecentyViewdProducts
{
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
        NSString *url;
        if (userId) {
            url = [NSString stringWithFormat:@"%@%@",GET_HOME_RECENTLY_VIEWED_WS,userId];
        }
        else
        {
            url = GET_HOME_RECENTLY_VIEWED_WS;
        }
        
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:url MathodName:GET_HOME_RECENTLY_VIEWED_WS];
    }
}

-(void)getPaymentMethods
{
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:GET_HOME_PAYMENTS_METODS_BANNERS_WS MathodName:GET_HOME_PAYMENTS_METODS_BANNERS_WS];
    }
}

-(void)getHotSelling
{
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:GET_HOME_HOT_SELLING_WS MathodName:GET_HOME_HOT_SELLING_WS];
    }
}

#pragma mark - service options delegate method
-(void)serviceImageClicked:(NSString *)name
{
    NSLog(@"service %@ options image clicked ",name);
    WarrantyViewController *objVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
    if([name isEqualToString:FREE_SHIPPING])
    {
        objVc.webViewUrl = @"http://electronicsbazaar.com/mobile-app/free-shipping.html";
        objVc.titleLabel = FREE_SHIPPING;
    }
    else if([name isEqualToString:ONE_YEAR_WARRANTY])
    {
        [CommonSettings eventTrackingForHomeScreen:@"eb-warranty"];
        
        objVc.webViewUrl = @"http://electronicsbazaar.com/mobile-app/warranty-app.html";
        objVc.titleLabel = ONE_YEAR_WARRANTY;
    }
    else if([name isEqualToString:EB_SERVICE_CENTER])
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        ServiceCenterViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"ServiceCenterViewController"];
        [self.navigationController pushViewController:objVc animated:YES];
        return;
    }
    else if ([name isEqualToString:EB_ADVANTAGES]) {
        
        objVc.webViewUrl = @"http://electronicsbazaar.com/mobile-app/bulk-order.html";
        objVc.titleLabel =BULK_ORDER;
    }
    else if ([name isEqualToString:PRE_OWNED_PRODUCTS]) {
        
        objVc.webViewUrl = @"http://electronicsbazaar.com/mobile-app/pre-owned-products.html";
        objVc.titleLabel = PRE_OWNED_PRODUCTS;
    }
    else if ([name isEqualToString:MICROSOFT_AUTHORIZED_REFURBISHER]) {
        objVc.webViewUrl = @"http://www.electronicsbazaar.com/mobile-app/microsoft-authorized-refurbisher.html";
        objVc.titleLabel = MICROSOFT_AUTHORIZED_REFURBISHER;
    }
    [self.navigationController pushViewController:objVc animated:YES];
}

-(void)paymentOptionImageClicked:(NSString *)name
{
    
}
#pragma mark - top brands image clicked
-(void)brandImageClicked:(NSString *)brandName
{
    SearchViewController *objSearchVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"SearchViewController"];
    objSearchVC.searchText = brandName;
    objSearchVC.navController=self.navigationController;
    //        [self presentViewController:objSearchVC animated:YES completion:nil];
    [self.navigationController pushViewController:objSearchVC animated:YES];
}

#pragma mark - HomeWS
-(void)callHomeWS
{
    // Loader
    //    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:NO];
    
    // Call Home screen webservice
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        
        // Check for User_id
        NSString *strUser_id = [[[NSUserDefaults standardUserDefaults]objectForKey:@"user"]objectForKey:@"user_id"];
        
        
        NSString *pushID=[[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_TOKEN"];
        NSString *deviceID = pushID?:@"";
        if (strUser_id.length > 0)
        {
            NSMutableDictionary * dictVal = [NSMutableDictionary new];
            [dictVal setObject:strUser_id forKey:@"user_id"];
            [dictVal setValue:deviceID forKey:@"DEVICE_TOKEN"];
            
            [callLoginService operationRequestToApi:dictVal url:HOMEPAGE_Ws string:@"HomeWS"];
        }
        else
        {
            NSMutableDictionary * dictVal = [NSMutableDictionary new];
            [dictVal setObject:@"" forKey:@"user_id"];
            [dictVal setValue:deviceID forKey:@"DEVICE_TOKEN"];
            [callLoginService operationRequestToApi:dictVal url:HOMEPAGE_Ws string:@"HomeWS"];
        }
    }
}

-(void)receivedResponseForHome:(id)receiveData stringResponse:(NSString*)responseType
{
    // [FVCustomAlertView hideAlertFromView:self.view fading:NO];
    
    NSLog(@"home executionTime = %f", [[NSDate date] timeIntervalSinceDate:startTime]);
    
    if ([responseType isEqualToString:@"success"])
    {
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        dictHomeDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
        
        if ([dictHomeDetail isKindOfClass:[NSDictionary class]])
        {
            NSString *status = [NSString stringWithFormat:@"%ld",(long)[[dictHomeDetail objectForKey:@"status"]integerValue]];
            slideImagesAfterTime = [[dictHomeDetail objectForKey:@"time"]floatValue];
            if ([status isEqualToString:@"1"])
            {
                
                categoryArr = [NSMutableArray new];
                categoryArr = [[dictHomeDetail objectForKey:@"data"]objectForKey:@"categories"];
                sliderImagesArr = [NSMutableArray new];
                sliderImagesArr = [[dictHomeDetail objectForKey:@"data"]objectForKey:@"banners"];
            }
            else{
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Something went wrong with the service."];
            }
        }
        
        ++responseCount;
        NSLog(@"in get home data %d",responseCount);
        [self hideHUD];
    }
    else{
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Something went wrong with the service."];
    }
}

-(void)hideHUD
{
    if(responseCount == 8)
        //if(responseCount == 7)
    {
        sectionArr = [NSMutableArray new];
        if(serviceOptionImagesArr.count != 0)
        {
            [sectionArr addObject:SERVICE_OPTIONS];
        }
        
        if (bestsellersArr.count != 0) {
            [sectionArr addObject:BEST_SELLERS];
        }
        
        if (topBrandsArray.count != 0) {
            [sectionArr addObject:TOP_BRANDS];
        }
        
        if (accessoriesArray.count != 0) {
            [sectionArr addObject:ACCESSORIES];
        }
        
        if (recentlyViewedArray.count != 0) {
            [sectionArr addObject:RECENTLY_VIEWED];
        }
        
        if (paymentMethodsImagesArray.count != 0) {
            [sectionArr addObject:PAYMENT_OPTIONS];
        }
        
        [_tableView reloadData];
        NSLog(@"response count in hud %d",responseCount);
        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
        _firstTimeBool = NO;
        responseCount = 0;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([[defaults valueForKey:@"FirstTimeLanuch"] boolValue])
        {
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:NO] forKey:@"FirstTimeLanuch"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self getHomePopUp];
        }
    }
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:@"ADDTOCART"]){
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            int quoteID=[[response valueForKey:@"quoteId"]intValue];
            [defaults setObject:[NSNumber numberWithInt:quoteID] forKey:@"QuoteID"];
            [defaults synchronize];
            [super incrementMyCartCount:[[response valueForKey:@"qty"] intValue]];
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Product Successfully Added To Cart"];
        }
        else
        {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
    }
    else if([strName isEqualToString:@"ADDTOWISHLIST"] || [strName isEqualToString:REMOVE_FROM_WISHLIST_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==1) {
            // [favouriteButton setImage:[UIImage imageNamed:@"favorate_icon_selected"] forState:UIControlStateNormal];
            NSNumber *isfavBool;
            if([strName isEqualToString:@"ADDTOWISHLIST"])
                isfavBool = [NSNumber numberWithBool:YES];
            else if([strName isEqualToString:REMOVE_FROM_WISHLIST_WS])
                isfavBool = [NSNumber numberWithBool:NO];
            NSMutableDictionary *productDic;
            AccessoriesTableCell *objCell = (AccessoriesTableCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:selectedCollectionViewTag]];
            if([[sectionArr objectAtIndex:selectedCollectionViewTag] isEqualToString:BEST_SELLERS])
            {
                productDic = [[bestsellersArr objectAtIndex:selectedFavBtnTag] mutableCopy];
                [productDic setValue:isfavBool forKey:@"is_favourite"];
                [bestsellersArr replaceObjectAtIndex:selectedFavBtnTag withObject:productDic];
            }
            else  if([[sectionArr objectAtIndex:selectedCollectionViewTag] isEqualToString:ACCESSORIES])
            {
                productDic = [[accessoriesArray objectAtIndex:selectedFavBtnTag] mutableCopy];
                [productDic setValue:isfavBool forKey:@"is_favourite"];
                [accessoriesArray replaceObjectAtIndex:selectedFavBtnTag withObject:productDic];
            }
            else  if([[sectionArr objectAtIndex:selectedCollectionViewTag] isEqualToString:RECENTLY_VIEWED])
            {
                productDic = [[recentlyViewedArray objectAtIndex:selectedFavBtnTag] mutableCopy];
                [productDic setValue:isfavBool forKey:@"is_favourite"];
                [recentlyViewedArray replaceObjectAtIndex:selectedFavBtnTag withObject:productDic];
            }
            [self reloadParticularCollectionViewItem:[productDic mutableCopy]];
        }
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
    }
    else if([strName isEqualToString:@"REMOVEFROMWISHLIST"]){
        if ([[response valueForKey:@"status"]intValue]==1) {
            [favouriteButton setImage:[UIImage imageNamed:@"favorate_icon"] forState:UIControlStateNormal];
        }
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
    }
    else if ([strName isEqualToString:@"GET_HOME_PAGE_BANNER"])
    {
        NSLog(@"banner executionTime = %f", [[NSDate date] timeIntervalSinceDate:startTime]);
        // [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        
        if ([[response valueForKey:@"status"]intValue]==1) {
            bannerArr = [NSMutableArray new];
            bannerArr = [[response objectForKey:@"data"] mutableCopy];
            //"banner_type" = "home-popup";
            NSArray *arr  = [bannerArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(banner_type == %@)",@"home-popup"]];
            if([[[NSUserDefaults standardUserDefaults] valueForKey:@"FirstTimeLanuch"] boolValue]&& arr.count != 0)
            {
                //                [_popUpImageView sd_setImageWithURL:[NSURL URLWithString:[[bannerArr objectAtIndex:5] valueForKey:@"banner_image"]] placeholderImage:nil           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [_popUpImageView sd_setImageWithURL:[NSURL URLWithString:[[arr objectAtIndex:0] valueForKey:@"banner_image"]] placeholderImage:nil           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if(!error){
                        
                        _popUpImageView.image = [CommonSettings imageWithImage:image scaledToMaxWidth:  _popUpImageView.frame.size.width maxHeight:_popUpImageView.frame.size.height];
                    }
                }];
                [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:NO] forKey:@"FirstTimeLanuch"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self dispalyPopupImage];
            }
            else
            {
                _popUpView.hidden = YES;
            }
            ++responseCount;
            NSLog(@"in get home banner data %d",responseCount);
            [self hideHUD];        }
        else
        {
            [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
        [self getTopBrands];
        [self getHotSelling];
        // [self getFeatureProducts];
    }
    else if ([strName isEqualToString:GET_HOME_SERVICE_OPTION_WS])
    {
        NSLog(@"banner executionTime = %f", [[NSDate date] timeIntervalSinceDate:startTime]);
        // [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        
        if ([[response valueForKey:@"status"]intValue]==1) {
            serviceOptionImagesArr = [response valueForKey:@"data"];
        }
        ++responseCount;
        NSLog(@"in get home banner data %d",responseCount);
        [self hideHUD];
    }
    else if ([strName isEqualToString:GET_HOME_BEST_SELLER_WS])
    {
        NSLog(@"banner executionTime = %f", [[NSDate date] timeIntervalSinceDate:startTime]);
        // [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        
        if ([[response valueForKey:@"status"]intValue]==1) {
            bestsellersArr = [[response valueForKey:@"data"] mutableCopy] ;
        }
        ++responseCount;
        NSLog(@"in get home banner data %d",responseCount);
        [self hideHUD];
    }
    else if ([strName isEqualToString:GET_HOME_TOP_BRANDS_WS])
    {
        NSLog(@"banner executionTime = %f", [[NSDate date] timeIntervalSinceDate:startTime]);
        // [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        
        if ([[response valueForKey:@"status"]intValue]==1) {
            topBrandsArray = [[response valueForKey:@"data"] valueForKey:@"brands"];
        }
        ++responseCount;
        NSLog(@"in get home banner data %d",responseCount);
        [self hideHUD];
    }else if ([strName isEqualToString:GET_HOME_ACCESSORIES_WS])
    {
        NSLog(@"banner executionTime = %f", [[NSDate date] timeIntervalSinceDate:startTime]);
        // [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        
        if ([[response valueForKey:@"status"]intValue]==1) {
            accessoriesArray = [[response valueForKey:@"data"] mutableCopy];
        }
        ++responseCount;
        NSLog(@"in get home banner data %d",responseCount);
        [self hideHUD];
    }
    else if ([strName isEqualToString:GET_HOME_RECENTLY_VIEWED_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==1) {
            recentlyViewedArray = [[response valueForKey:@"data"] mutableCopy] ;
        }
        ++responseCount;
        NSLog(@"in get home banner data %d",responseCount);
        [self hideHUD];
    }
    else if ([strName isEqualToString:GET_HOME_PAYMENTS_METODS_BANNERS_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==1) {
            paymentMethodsImagesArray = [response valueForKey:@"data"]  ;
        }
        ++responseCount;
        NSLog(@"in get home banner data %d",responseCount);
        [self hideHUD];
    }
    else if ([strName isEqualToString:GET_HOME_FEATURE_PRODUCTS_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==1) {
            featuredproductsArr = [response valueForKey:@"data"] ;
        }
        ++responseCount;
        NSLog(@"in get home banner data %d",responseCount);
        [self hideHUD];
    }
    else if([strName isEqualToString:GET_APP_VERSION_WS])
    {
        NSLog(@"app version executionTime = %f", [[NSDate date] timeIntervalSinceDate:startTime]);
        
        ++responseCount;
        NSLog(@"in get iphone data %d",responseCount);
        if ([[response valueForKey:@"status"]intValue]==1) {
            NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
            NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
            float version = [currentVersion floatValue];
            NSLog(@"version %f",version);
            NSLog(@"version from backend %f",[[[response valueForKey:@"data"] valueForKey:@"appversion"] floatValue]);
            if(version < [[[response valueForKey:@"data"] valueForKey:@"appversion"] floatValue])
            {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                [self displayUpdateAppAlert];
            }
            else
            {
                startTime = [NSDate date];
                [self callHomeWS];
                [self getServiceOptions];
                [self getBestSellers]; //hide for development URL
                [self getTopBrands];
                [self getAccessories];
                [self getRecentyViewdProducts];
                [self getPaymentMethods];
                
                //[self getBannerImages];
                // [CommonSettings showMyCartCount];
                [super showCartValue];
            }
        }
        else
        {
            [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
        }
    }
    else if ([strName isEqualToString:GET_HOME_POP_UP_WS])
    {
        //return;
        //Hide Vow Delight PopUp
        if ([[response valueForKey:@"status"]intValue] == 1)
        {
            PopupViewController *viewController = [[UIStoryboard storyboardWithName:@"Product" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupViewController"];
            
            viewController.homePopupResponseDic = response;
            [self addChildViewController:viewController];
            viewController.view.frame = self.view.frame;
            [self.view addSubview:viewController.view];
            [self.view bringSubviewToFront:viewController.view];
            viewController.view.alpha = 0;
            [viewController didMoveToParentViewController:self];
            
            [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                viewController.view.alpha = 1;
            }completion:nil];
            double delayInSeconds = [[response valueForKey:@"duration"] doubleValue];
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //code to be executed on the main queue
                [viewController willMoveToParentViewController:nil];
                [viewController removeFromParentViewController];
                [viewController.view removeFromSuperview];
            });
        }
    }
}

-(void)dispalyPopupImage {
    [_popUpView setHidden:NO];
    CATransition *transition = [CATransition animation];
    transition.duration = 2.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [_popUpImageView.layer addAnimation:transition forKey:nil];
    
    double delayInSeconds = 5.0f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        CATransition *transition = [CATransition animation];
        transition.duration = 2.0f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;
        [_popUpView.layer addAnimation:transition forKey:nil];
        [_popUpView setHidden:YES];
    });
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 500)
    {
        if (buttonIndex==1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:App_Link]];
        }
    }
    else if(alertView.tag == 600) {
        if (buttonIndex==0) {
            [self handleSYGNotification:sygNotificationDic];
        }
    }
    else if (alertView.tag == 100)
    {
        switch (buttonIndex) {
            case 0:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:App_Link]];
                break;
            case 1:
            {
                [alertView dismissWithClickedButtonIndex:1 animated:YES];
            }
                break;
            default:
                [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
                break;
        }
    }
    else if (alertView.tag == 200)
    {
        switch (buttonIndex) {
            case 0:
            {
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
                GSTViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"GSTViewController"];
                [self.navigationController pushViewController:viewController animated:YES];
                
            }
                break;
            case 1:
            {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:REMIND_ME_LATER_GSTIN];
                [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:REMIND_ME_LATER_CLICK_DATE];
                
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:SHOW_GSTIN];
                [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
                
            }
                break;
                
            case 2:
            {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:SHOW_GSTIN];
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:REMIND_ME_LATER_GSTIN];
                [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
            }
                break;
                
            default:
                break;
        }
    }
}

-(void)handleSYGNotification:(NSDictionary *)notificationDic
{
    UINavigationController *nav =(UINavigationController *)[[AppDelegate getAppDelegateObj].tabBarObj selectedViewController];
    UIStoryboard *storyBoard;
    storyBoard= [UIStoryboard storyboardWithName:@"Product" bundle:nil];
    if(notificationDic != nil)
    {
        SYGOrderConfirmationViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"SYGOrderConfirmationViewController"];
        vc.orderId = [notificationDic valueForKey:@"gcm.notification.order_id"];
        vc.userId = [notificationDic valueForKey:@"gcm.notification.user_id"];
        [nav pushViewController:vc animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - hotSelling product click actions
-(void)hotSellingFirstBrandClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSArray *arr = hotSellingDataDic.allKeys;
    NSArray *dataArr = [hotSellingDataDic valueForKey:[arr objectAtIndex:btn.tag]];
    ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    if([[dataArr objectAtIndex:0] objectForKey:@"product_id"] != [NSNull null])
    {
        int prodId = [[[dataArr objectAtIndex:0] objectForKey:@"product_id"] intValue];
        objProductDetailVC.productID=prodId;
        [self.navigationController pushViewController:objProductDetailVC animated:YES];
    }
}

-(void)hotSellingSecondBrandClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSArray *arr = hotSellingDataDic.allKeys;
    NSArray *dataArr = [hotSellingDataDic valueForKey:[arr objectAtIndex:btn.tag]];
    ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    if([[dataArr objectAtIndex:1] objectForKey:@"product_id"] != [NSNull null])
    {
        int prodId = [[[dataArr objectAtIndex:1] objectForKey:@"product_id"] intValue];
        objProductDetailVC.productID=prodId;
        [self.navigationController pushViewController:objProductDetailVC animated:YES];
    }
}

#pragma mark - preownedGuidlines btn clicked
-(void)preownedGuidlinesBtnClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    NSString *url;
    NSString *title;
    switch (btn.tag) {
        case 100:
        {
            url = @"http://electronicsbazaar.com/warranty-app/pre-owned-products.html";
            title = @"Pre Owned Product Guidelines";
        }
            break;
        case 101:
        {
            
            url = @"http://electronicsbazaar.com/warranty-app/pre-owned-mobile.html";
            title = @"Pre Owned Mobile Guidelines";
        }
            break;
            
        case 102:
        {
            
            url = @"http://electronicsbazaar.com/warranty-app/pre-owned-laptop.html";
            title = @"Pre Owned Laptop Guidelines";
        }
            break;
        default:
            break;
    }
    
    WarrantyViewController *objVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
    objVc.webViewUrl = url;
    objVc.titleLabel = title;
    [self.navigationController pushViewController:objVc animated:YES];
}

#pragma mark - ebAdvantages
-(void)ebAdvantagesBtnClicked
{
    WarrantyViewController *objVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
    objVc.webViewUrl = @"http://electronicsbazaar.com/warranty-app/eb-advantages.html";
    objVc.titleLabel = @"Eb Advantages";
    [self.navigationController pushViewController:objVc animated:YES];
}
@end
