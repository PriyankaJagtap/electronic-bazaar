//
//  TopBrandsTableCell.h
//  EB
//
//  Created by Neosoft on 6/6/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TopBrandsDelegate <NSObject>

-(void)brandImageClicked:(NSString *)brandName;

@end

@interface TopBrandsTableCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>

@property(strong,nonatomic)NSArray *brandsImageArray;
@property (nonatomic, weak) IBOutlet UICollectionView *objCollectionView;
@property (nonatomic, retain) id <TopBrandsDelegate> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *objCollectionViewHeightConstraint;

@end
