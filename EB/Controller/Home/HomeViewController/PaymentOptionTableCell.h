//
//  PaymentOptionTableCell.h
//  EB
//
//  Created by webwerks on 16/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PaymentOptionsDelegate <NSObject>

-(void)paymentOptionImageClicked:(NSString *)name;

@end

@interface PaymentOptionTableCell : UITableViewCell

@property(strong,nonatomic)NSArray *paymentOptionsArray;
@property (nonatomic, weak) IBOutlet UICollectionView *objCollectionView;
@property (nonatomic, retain) id <PaymentOptionsDelegate> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *objCollectionViewHeightConstraint;
@end
