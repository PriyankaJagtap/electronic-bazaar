//
//  ServiceOptionsTableCell.h
//  EB
//
//  Created by webwerks on 16/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ServiceOptionsDelegate <NSObject>

-(void)serviceImageClicked:(NSString *)name;

@end


@interface ServiceOptionsTableCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>

@property(strong,nonatomic)NSArray *servicesImageArray;
@property(strong,nonatomic)NSArray *servicesOptionsArray;
@property (nonatomic, weak) IBOutlet UICollectionView *objCollectionView;
@property (nonatomic, retain) id <ServiceOptionsDelegate> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *objCollectionViewHeightConstraint;


@end
