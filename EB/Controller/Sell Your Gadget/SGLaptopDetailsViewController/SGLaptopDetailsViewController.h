//
//  SGLaptopDetailsViewController.h
//  EB
//
//  Created by webwerks on 11/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGLaptopDetailsViewController : SellYourGadgetNavigationViewController
@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelLabel;
@property (weak, nonatomic) IBOutlet UILabel *ramLabel;
@property (weak, nonatomic) IBOutlet UILabel *processorLabel;
@property (weak, nonatomic) IBOutlet UILabel *generationLabel;

@property (weak, nonatomic) IBOutlet UILabel *conditionLabel;
@property (weak, nonatomic) IBOutlet UILabel *serialNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *chargerLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *separatorLabel;

@property (nonatomic , strong) NSDictionary *laptopDetailDic;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *generationViewHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modelViewHtConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *generationTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *generationBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *processorViewHtConstraint;


@property (weak, nonatomic) IBOutlet UILabel *generationTextLabel;

@property (weak, nonatomic) IBOutlet UITextField *lenovoOrderIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailIDTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lenovoOrderIDViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitBtnLeadingConstraint;
@property (weak, nonatomic) IBOutlet UIButton *generatePromocodeBtn;
@property (weak, nonatomic) IBOutlet UIView *generatePromoCodeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conditionViewHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailIdViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectProcessorViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *processorBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *objScrollView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@end
