//
//  SGLaptopDetailsViewController.m
//  EB
//
//  Created by webwerks on 11/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SGLaptopDetailsViewController.h"
#import "SellYourLaptopViewController.h"
#import "SellGadgetCheckOutViewController.h"

@interface SGLaptopDetailsViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,FinishLoadingData>
{
        NSArray *processorArr;
        NSArray *pickerArray;
        UIPickerView *picker;
        UIView *pickerBackgroundView;
        CGPoint scrollViewContentOffset;

}
@end

@implementation SGLaptopDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
         [super setViewControllerTitle:@"Your Device"];
    // Do any additional setup after loading the view.
        
        if([CommonSettings checkCorporateUser])
        {
                [self.view layoutIfNeeded];
//                _submitBtnLeadingConstraint.constant = 0;
//                _generatePromocodeBtn.hidden = NO;
//                _lenovoOrderIDViewHeightConstraint.constant = 65;
                
                _submitBtnLeadingConstraint.constant = _generatePromoCodeView.frame.size.width/4;
                _generatePromocodeBtn.hidden = YES;
                
                [self.view layoutIfNeeded];
        }
        else
        {
                [self.view layoutIfNeeded];
                _submitBtnLeadingConstraint.constant = _generatePromoCodeView.frame.size.width/4;
                _generatePromocodeBtn.hidden = YES;
                //_lenovoOrderIDViewHeightConstraint.constant = 0;
                [self.view layoutIfNeeded];
        }
           [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_submitBtn];
        [self setInitialValues];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - set initial values
-(void)setInitialValues
{
        _brandLabel.text = [_laptopDetailDic valueForKey:BRAND];
        if([_laptopDetailDic valueForKey:MODEL])
        {
                _modelLabel.text = [_laptopDetailDic valueForKey:MODEL];
                _modelViewHtConstraint.constant = 38;
        }
        else
        {
                _modelViewHtConstraint.constant = 0;
        }
        
        if([_laptopDetailDic valueForKey:GENERATION])
        {
                _generationLabel.text = [_laptopDetailDic valueForKey:GENERATION];
                _generationTextLabel.hidden = NO;
                _generationViewHtConstraint.constant = 38;
                _separatorLabel.hidden = NO;
        }
        else
        {
                _generationTextLabel.hidden = YES;
                _generationViewHtConstraint.constant = 0;
                _generationTopConstraint.constant = 0;
                _generationBottomConstraint.constant = 0;
                _separatorLabel.hidden = YES;
        }
        
        
        if([_laptopDetailDic valueForKey:PROCESSOR])
        {
                _processorLabel.text = [_laptopDetailDic valueForKey:PROCESSOR];
                _processorViewHtConstraint.constant = 38;
        }
        else
        {
                _processorViewHtConstraint.constant = 0;
        }
        
        
        if([_laptopDetailDic valueForKey:CONDITION])
        {
                _conditionLabel.text = [_laptopDetailDic valueForKey:CONDITION];
                _conditionViewHtConstraint.constant = 38;
        }
        else
        {
                _conditionViewHtConstraint.constant = 0;
        }
        
        _ramLabel.text = [_laptopDetailDic valueForKey:MEMORY];
        _serialNoLabel.text = [_laptopDetailDic valueForKey:IMEI_NO];
        _priceLabel.text = [[CommonSettings sharedInstance] formatIntegerPrice:[[_laptopDetailDic valueForKey:PRICE] integerValue]];
        _chargerLabel.text = [_laptopDetailDic valueForKey:CHARGER];
        
        
        if([CommonSettings checkCorporateUser])
        {
                [self.view layoutIfNeeded];
//                _emailIdViewHeightConstraint.constant = 65;
                _emailIdViewHeightConstraint.constant = 0;
                _lenovoOrderIDViewHeightConstraint.constant = 65;
                // _lenovoOrderIDViewHeightConstraint.constant = 0;
                _selectProcessorViewHeightConstraint.constant = 60;
                [CommonSettings addDropDownArrowImageInBtn:_processorBtn];
                [self.view layoutIfNeeded];
                  [self callGetNewProcessorData];
        }
        else if([_laptopDetailDic valueForKey:PROMOCODE])
        {
                [self.view layoutIfNeeded];
//                _lenovoOrderIDViewHeightConstraint.constant = 65;
                _lenovoOrderIDViewHeightConstraint.constant = 0;
                [self.view layoutIfNeeded];
        }
        
      
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - custom methods
-(void)callGetNewProcessorData
{
        // Loader
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:NO];
        
        // Call Home screen webservice
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:SG_NEW_PROCESSOR_WS MathodName:SG_NEW_PROCESSOR_WS];
        }
}

-(void)calculatePriceBasedOnProcessor:(NSString *)newProcessorStr
{
        // Loader
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:NO];
        
        // Call Home screen webservice
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                NSString *url = [NSString stringWithFormat:@"%@old_laptop=%@&new_laptop=%@",SG_REPLACE_LAPTOP_WS,[_laptopDetailDic valueForKey:PROCESSOR],newProcessorStr];
                [callLoginService GetWebServiceWithURL:url MathodName:SG_REPLACE_LAPTOP_WS];
        }
}


#pragma mark - IBAction methods

- (IBAction)processorBtnClicked:(UIButton *)sender {
        pickerArray = [processorArr valueForKey:@"option_name"];
        [self setPickerView:(int)sender.tag btnTitle:[sender titleForState:UIControlStateNormal]];
}


- (IBAction)backBtnClicked:(id)sender {
        NSArray *viewControllers = [self.navigationController viewControllers];
        for (UIViewController *vc in viewControllers) {
                if([vc isKindOfClass:[SellYourLaptopViewController class]])
                {
                        [self.navigationController popToViewController:vc animated:YES];
                        return;
                }
        }
}
- (IBAction)generatePromoCodeBtnClicked:(id)sender {
        NSLog(@"promocode btn clicked");
}
- (IBAction)rightMenuClicked:(id)sender {
         [CommonSettings displaySellYourGadgetRightMenu:self];
}
- (IBAction)sellNowBtnClicked:(id)sender {
        
        if ([CommonSettings checkCorporateUser]) {
               
//                if([_emailIDTextField.text isEqualToString:@""])
//                {
//                        [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Please enter email ID"];
//                        return;
//                }
//                
//                if([_lenovoOrderIDTextField.text isEqualToString:@""])
//                {
//                        [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Please enter Internal Order ID"];
//                        return;
//                }
                
                if(![_emailIDTextField.text isEqualToString:@""])
                [_laptopDetailDic setValue:_emailIDTextField.text forKey:EMAIL_ID];
                
                if(![_lenovoOrderIDTextField.text isEqualToString:@""])
                [_laptopDetailDic setValue:_lenovoOrderIDTextField.text forKey:INTERNAL_ORDER_ID];

        
        }
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SellGadgetCheckOutViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"SellGadgetCheckOutViewController"];
        objVc.mobileDataDic = _laptopDetailDic;
        objVc.productType = @"Laptop";
        [self.navigationController pushViewController:objVc animated:YES];
}



#pragma mark - picker custom methods
-(void)setPickerView:(int) tag btnTitle:(NSString *)selectedBtnTitle{
        
        if(pickerBackgroundView == nil)
        {
                pickerBackgroundView =[[UIView alloc]init];
                pickerBackgroundView.frame = CGRectMake(0, self.view.frame.size.height - 200
                                                        , self.view.frame.size.width, 200);
                
                
                UIButton *doneButton = [[UIButton alloc]init];
                doneButton.frame =CGRectMake(0, 0, pickerBackgroundView.frame.size.width, 30);
                [doneButton setTitle:@"Done" forState:UIControlStateNormal];
                doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                doneButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
                doneButton.backgroundColor =[UIColor lightGrayColor];
                [doneButton addTarget:self action:@selector(doneTouched:) forControlEvents:UIControlEventTouchUpInside];
                [pickerBackgroundView addSubview:doneButton];
                //processorView.hidden = YES;
                [self.view addSubview:pickerBackgroundView];
        }
        
        picker = [[UIPickerView alloc]init];
        picker.frame = CGRectMake(0, 30, self.view.frame.size.width, 170);
        picker.dataSource = self;
        picker.tag = tag;
        picker.delegate = self;
        
        //        NSUInteger index = [pickerArray indexOfObjectPassingTest:
        //                            ^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop)
        //                            {
        //                                    return [[dict objectForKey:@"option_name"] isEqual:selectedBtnTitle];
        //                            }
        //                            ];
        //        if(index != NSNotFound)
        //                [picker selectRow:index inComponent:0 animated:YES];
        
       
        NSUInteger index = [pickerArray indexOfObject:selectedBtnTitle];
        if(index != NSNotFound)
                [picker selectRow:index inComponent:0 animated:YES];
        
        
        picker.backgroundColor = [UIColor whiteColor];
        picker.showsSelectionIndicator = YES;
        [pickerBackgroundView addSubview:picker];
        [picker reloadAllComponents];
        scrollViewContentOffset = _objScrollView.contentOffset;
        CGPoint contentOffset = _objScrollView.contentOffset;
        contentOffset.y += 40;
        [_objScrollView setContentOffset:contentOffset];
        //[processorPicker setHidden:YES];
}


- (void)doneTouched:(UIBarButtonItem *)sender{
        //[processorPicker setHidden:YES];
        [_objScrollView setContentOffset:scrollViewContentOffset];
        pickerBackgroundView.hidden = YES;
        [pickerBackgroundView removeFromSuperview];
        pickerBackgroundView = nil;
}


#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
        return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
        return [pickerArray count];
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
         [_processorBtn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
        [self calculatePriceBasedOnProcessor:[pickerArray objectAtIndex:row]];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
        return [pickerArray objectAtIndex:row];
}

#pragma mark - request finish method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if([strName isEqualToString:SG_NEW_PROCESSOR_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        processorArr = [[[[response valueForKey:@"data"] valueForKey:@"products"] valueForKey:@"options"] valueForKey:@"Processor"];
                        if(processorArr.count != 0)
                        {
                                [_processorBtn setTitle:[[processorArr objectAtIndex:0] valueForKey:@"option_name"] forState:UIControlStateNormal];
                                [self calculatePriceBasedOnProcessor:[[processorArr objectAtIndex:0] valueForKey:@"option_name"]];
                        }
                }
        }
        else if([strName isEqualToString:SG_REPLACE_LAPTOP_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        _priceLabel.text = [[CommonSettings sharedInstance] formatIntegerPrice:[[response valueForKey:@"price"] integerValue]];
                        [_laptopDetailDic setValue:[NSString stringWithFormat:@"%@",[response valueForKey:@"price"]] forKey:CORP_PRICE];
                }
        }
}
@end
