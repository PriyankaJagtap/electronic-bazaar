//
//  SGHistoryViewController.m
//  EB
//
//  Created by webwerks on 12/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SGHistoryViewController.h"
#import "WarrantyViewController.h"
#import "SGWebViewController.h"
#import <UIKit/UIKit.h>
#import "SellGadgetUser.h"
#import "SaleBackCheckoutViewController.h"


static NSString *CUSTOMER_MSG = @"Your device would be picked up soon & you would be contacted shortly";
@interface SGHistoryViewController ()<FinishLoadingData>

@end



@implementation SGHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
         [super setViewControllerTitle:@"Sales Details"];
        _orderIdLabel.text = _orderId;
        [self getSalesHistoryDeatils];
        
        NSDictionary * linkAttributes = @{NSForegroundColorAttributeName:_trackingDetailBtn.titleLabel.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)};
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:_trackingDetailBtn.titleLabel.text attributes:linkAttributes];
        [_trackingDetailBtn.titleLabel setAttributedText:attributedString];
}


-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        [[APP_DELEGATE tabBarObj] hideTabbar];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - set initial values
-(void)setInitialValues
{
        _brandLabel.text = [_laptopDetailDic valueForKey:BRAND];
        
        if([_laptopDetailDic valueForKey:MODEL])
        {
                _modelLabel.text = [_laptopDetailDic valueForKey:MODEL];
                _modelViewHtConstraint.constant = 38;
        }
        else
        {
                _modelViewHtConstraint.constant = 0;
        }
        
        if([_laptopDetailDic valueForKey:GENERATION])
        {
                _generationLabel.text = [_laptopDetailDic valueForKey:GENERATION];
                _generationViewHtConstraint.constant = 38;
        }
        else
        {
                _generationViewHtConstraint.constant = 0;
                _generationTopConstraint.constant = 0;
                _generationBottomConstraint.constant = 0;

                
        }
        
        if([_laptopDetailDic valueForKey:@"Processor"])
        {
                 _processorLabel.text = [_laptopDetailDic valueForKey:@"Processor"];
                _processorViewHtConstraint.constant = 38;
        }
        else
                _processorViewHtConstraint.constant = 0;
        
        
        if([_laptopDetailDic valueForKey:MEMORY])
        {
                _ramLabel.text = [_laptopDetailDic valueForKey:MEMORY];
                _ramViewHeightConstraint.constant = 38;
        }
        else
                _ramViewHeightConstraint.constant = 0;
        
        
        
        _conditionLabel.text = [_laptopDetailDic valueForKey:@"Condition"];
        _serialNoLabel.text = [_laptopDetailDic valueForKey:IMEI_NO];
        _priceLabel.text = [NSString stringWithFormat:@"Your Device price %@",[[CommonSettings sharedInstance] formatIntegerPrice:[[_laptopDetailDic valueForKey:PRICE] integerValue]]];
        
        _chargerLabel.text = [_laptopDetailDic valueForKey:@"Charger"];
        _statusLabel.text = [_laptopDetailDic valueForKey:@"status"];
        _addressLabel.text = [_laptopDetailDic valueForKey:@"address"];
}

#pragma mark - IBACtion methods
- (IBAction)backBtnClicked:(id)sender {
//        NSArray *viewControllers = [self.navigationController viewControllers];
//        for (UIViewController *vc in viewControllers) {
//                if([vc isKindOfClass:[SellYourLaptopViewController class]])
//                {
//                        [self.navigationController popToViewController:vc animated:YES];
//                }
//        }
        [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)rightMenuClicked:(id)sender {
        [CommonSettings displaySellYourGadgetRightMenu:self];
}
- (IBAction)trackYourOrderBtnClicked:(id)sender {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SGWebViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"SGWebViewController"];
        objVc.webViewUrl = [NSString stringWithFormat:@"https://www.electronicsbazaar.com/%@%@",SG_TRACK_YOUR_ORDER,_orderId];
        objVc.titleLabel = @" ";
            [self.navigationController pushViewController:objVc animated:YES];
}
- (IBAction)saleBackBtnClicked:(id)sender {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        
        SaleBackCheckoutViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"SaleBackCheckoutViewController"];
        [self.navigationController pushViewController:vc animated:YES];
        
}



#pragma mark - get sales history deatils
-(void)getSalesHistoryDeatils
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:YES allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                
                //NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",SELL_GADGET_HISTORY_DETAILS_WS,_orderId] MathodName:SELL_GADGET_HISTORY_DETAILS_WS];
                
        }
}

-(void)getAddressDeatils:(NSString *)addressId
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:YES allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                
                //NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
                NSString *url = [NSString stringWithFormat:@"%@%@",GET_ADDRESS_DATA_WS,addressId];
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:url MathodName:GET_ADDRESS_DATA_WS];
                
        }
}

#pragma mark - response delegate method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:SELL_GADGET_HISTORY_DETAILS_WS]){
                //NSLog(@"%@",response);
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        _laptopDetailDic = [response valueForKey:@"data"];
                        [self setInitialValues];
                        SellGadgetUser *objSellGadgetUser = [SellGadgetUser getSellGadgetUserFromNsuserdefaults];
                        
//                        if(![[[NSUserDefaults standardUserDefaults] valueForKey:IS_RETAILER] boolValue])
                        if(!_isSYGPickUpRequest)
                        {
                                 _reatilerViewHeightConstraint.constant = 0;
                                if([[_laptopDetailDic valueForKey:@"is_order_confirmed"] isEqualToString:@"Yes"])
                                {
                                        _customerMessageLabel.text = CUSTOMER_MSG;
                                        _trackBtnHeightConstraint.constant = 0;

                                        
                                }
                                else
                                {
                                        _customerMessageLabel.text = @"";
                                        _trackBtnHeightConstraint.constant = 25;
                                        _saleBackBtnHeightConstraint.constant = 0;

                                }
                                
                                return;
                        }
                        else
                        {
                                if(![[_laptopDetailDic valueForKey:@"confirmed_by_user_id"] isKindOfClass:[NSNull class]])
                                {
                                        if([[_laptopDetailDic valueForKey:@"confirmed_by_user_id"] isEqualToString:objSellGadgetUser.userId])
                                        {
                                                _saleBackBtnHeightConstraint.constant = 25;
                                                _trackBtnHeightConstraint.constant = 0;
                                        }
                                    else
                                    {
                                        _saleBackBtnHeightConstraint.constant = 0;
                                        _trackBtnHeightConstraint.constant = 25;
                                    }
                            
                                        
                                }
                                
                                
                                
                                if(![[_laptopDetailDic valueForKey:@"awb"] isKindOfClass:[NSNull class]])
                                {
//                                     _trackBtnHeightConstraint.constant = 25;
//                                     _saleBackBtnHeightConstraint.constant = 0;
                                    
                                        
                                        if(![[_laptopDetailDic valueForKey:@"confrm_by_retailer_name_in_bank"] isKindOfClass:[NSNull class]])
                                        {
                                                
                                        _reatilerViewHeightConstraint.constant = 247;
                                        _accountNumberLabel.text = [_laptopDetailDic valueForKey:@"confrm_by_retailer_acct_number"];
                                        _bankNameLabel.text = [_laptopDetailDic valueForKey:@"confrm_by_retailer_bank_name"];
                                        _ifscCodeLabel.text = [_laptopDetailDic valueForKey:@"confrm_by_retailer_ifsc_code"];
                                        _customerNameLabel.text = [_laptopDetailDic valueForKey:@"confrm_by_retailer_name_in_bank"];
                                    
                                        [self getAddressDeatils:[NSString stringWithFormat:@"%@",[_laptopDetailDic valueForKey:@"confrm_by_retailer_address_id"]]];
                                            //added by rj
                                            _trackBtnHeightConstraint.constant = 0;
                                            _saleBackBtnHeightConstraint.constant = 0;
                                        }
                                        else
                                        {
                                                _reatilerViewHeightConstraint.constant = 0;

                                        }
                                }
                                else
                                {
                                        _trackBtnHeightConstraint.constant = 0;
                                       
                                }
                                
                        }
                        
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                
        }
        else if ([strName isEqualToString:GET_ADDRESS_DATA_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        NSString *addressStr = [NSString stringWithFormat:@"%@ %@,%@-%@,\n%@",[[response valueForKey:@"data"]valueForKey:@"firstname"],[[response valueForKey:@"data"] valueForKey:@"street"],[[response valueForKey:@"data"] valueForKey:@"city"],[[response valueForKey:@"data"] valueForKey:@"postcode"],[[response valueForKey:@"data"] valueForKey:@"region"]];
                        _retailerAddressLabel.text = addressStr;
                        
                        _reatilerViewHeightConstraint.constant = 218 + [[CommonSettings sharedInstance] getLabelHeight:_retailerAddressLabel];
                }
        }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
