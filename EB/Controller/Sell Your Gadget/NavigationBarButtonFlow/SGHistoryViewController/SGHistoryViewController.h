//
//  SGHistoryViewController.h
//  EB
//
//  Created by webwerks on 12/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGHistoryViewController : SellYourGadgetNavigationViewController


@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelLabel;
@property (weak, nonatomic) IBOutlet UILabel *ramLabel;
@property (weak, nonatomic) IBOutlet UILabel *processorLabel;
@property (weak, nonatomic) IBOutlet UILabel *generationLabel;

@property (weak, nonatomic) IBOutlet UILabel *conditionLabel;
@property (weak, nonatomic) IBOutlet UILabel *serialNoLabel;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (nonatomic , strong) NSDictionary *laptopDetailDic;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *generationViewHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modelViewHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *processorViewHtConstraint;

@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *trackingDetailBtn;
@property (weak, nonatomic) IBOutlet UILabel *chargerLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ramViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *generationTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *generation;
@property (weak, nonatomic) IBOutlet UILabel *generationSeparatorLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *generationBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saleBackBtnTopConstraint;

@property (weak, nonatomic) IBOutlet UIButton *saleBackBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trackBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saleBackBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *ifscCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *retailerAddressLabel;
@property (weak, nonatomic) IBOutlet UIView *retailerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reatilerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *customerMessageLabel;
@property(nonatomic)BOOL isSYGPickUpRequest;

@end
