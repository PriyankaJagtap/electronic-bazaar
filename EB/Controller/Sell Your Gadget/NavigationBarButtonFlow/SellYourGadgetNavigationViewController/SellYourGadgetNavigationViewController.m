//
//  SellYourGadgetNavigationViewController.m
//  EB
//
//  Created by webwerks on 18/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "SellYourGadgetNavigationViewController.h"
#import "SellYourGadgetLoginSignUpViewController.h"
@interface SellYourGadgetNavigationViewController ()
{
        UIButton *rightMenuButton;
        UINavigationBar *newNavBar;
    UIView *statusBar;
}
@end

@implementation SellYourGadgetNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
         [self styleNavBar];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)styleNavBar {
        // 1. hide the existing nav bar
        [self.navigationController setNavigationBarHidden:YES animated:NO];
        // 2. create a new nav bar and style it
        
        if(IS_IPHONE_X)
        {
                newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 44, CGRectGetWidth(self.view.bounds), 44.0)];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
        }
        else
        {
                newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20, CGRectGetWidth(self.view.bounds), 44.0)];
        }
        [newNavBar setTintColor:[UIColor whiteColor]];
        
        // 3. add a new navigation item w/title to the new nav bar
        UINavigationItem *newItem = [[UINavigationItem alloc] init];
        newItem.title = @"Title";
        [newNavBar setItems:@[newItem]];
        
        
        UIImage *navBackgroundImage = [UIImage imageNamed:@"Nav"];
        [newNavBar setBackgroundImage:navBackgroundImage forBarMetrics:UIBarMetricsDefault];
        [newNavBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor],
           NSFontAttributeName:[UIFont fontWithName:@"Karla-Regular" size:17]}];
        
        
        // BackButtonBlack is an image we created and added to the app’s asset catalog
        UIImage *backButtonImage = [UIImage imageNamed:@"ios_back_btn"];
        
        // any buttons in a navigation bar are UIBarButtonItems, not just regular UIButtons. backTapped: is the method we’ll call when this button is tapped
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:backButtonImage style:UIBarButtonItemStylePlain target:self action:@selector(backTapped:)];
        backBarButtonItem.tintColor = [UIColor whiteColor];

        
        UIImage *rightMenuButtonImage = [UIImage imageNamed:@"white_profile_icon"];
        
        rightMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightMenuButton setImage:rightMenuButtonImage forState:UIControlStateNormal];
        rightMenuButton.showsTouchWhenHighlighted = YES;
        rightMenuButton.frame = CGRectMake(0.0, 0.0, rightMenuButtonImage.size.width, rightMenuButtonImage.size.height);
        
        [rightMenuButton addTarget:self action:@selector(rightBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *rightMenuBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightMenuButton];
        ;
        
        
        
        // the bar button item is actually set on the navigation item, not the navigation bar itself.
        newItem.leftBarButtonItem = backBarButtonItem;
        newItem.rightBarButtonItems = @[rightMenuBarButtonItem];
        [newNavBar setItems:@[newItem]];
        
        // 4. add the nav bar to the main view
        [self.view addSubview:newNavBar];
        
    [[CommonSettings sharedInstance] setStatusBar:statusBar];

}

#pragma mark - set nav title
-(void)setViewControllerTitle:(NSString *)titleStr
{
        [newNavBar.items objectAtIndex:0].title = titleStr;
        if ([titleStr isEqualToString:@""]) {
                [newNavBar.items objectAtIndex:0].leftBarButtonItem = nil;
        }
        
}

#pragma mark - IBAction methods

-(void)backTapped:(UIBarButtonItem *)btn
{
        if ([[newNavBar.items objectAtIndex:0].title isEqualToString:@"Sell Your Gadget"]) {
                 [[APP_DELEGATE tabBarObj] TabClickedIndex:0];
        }
        else
        {
            NSArray *viewControllers = [self.navigationController viewControllers];
            NSUInteger count = [viewControllers count];
            if (count >= 2)
            {
                if ([[viewControllers objectAtIndex:count - 2] isKindOfClass:[SellYourGadgetLoginSignUpViewController class]])
                {
                    [self.navigationController popToViewController:[viewControllers objectAtIndex:count - 3] animated:YES];
                }
                else
                {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
}

-(void)rightBtnTapped:(UIBarButtonItem *)btn
{
          [CommonSettings displaySellYourGadgetRightMenu:self];
}

@end
