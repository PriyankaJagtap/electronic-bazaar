//
//  SellYourGadgetNavigationViewController.h
//  EB
//
//  Created by webwerks on 18/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellYourGadgetNavigationViewController : UIViewController

-(void)setViewControllerTitle:(NSString *)titleStr;
@end
