//
//  SGMenuViewController.m
//  EB
//
//  Created by webwerks on 11/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SGMenuViewController.h"
#import "RightSlideMenuCell.h"
#import "MyPurchasesViewController.h"
#import "SellYourGadgetHomeViewController.h"
#import "SGPurchaseHistoryViewController.h"


static  NSString *HISTORY = @"Sales History";
static  NSString *SIGN_OUT = @"Sign Out";
static NSString *LOGIN_SIGN_UP  = @"Login/Sign Up";
static NSString *SYG_PICKUP_REQUEST  = @"SYG Pickup Request";
@interface SGMenuViewController ()
{
        NSArray *menuArr;
}

@end

@implementation SGMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//        _tableView.rowHeight = UITableViewAutomaticDimension;
//        _tableView.estimatedRowHeight = 50;
        
        if([CommonSettings isUserLoggedInSellYourGadget])
        {
                if([[[NSUserDefaults standardUserDefaults] valueForKey:IS_RETAILER] boolValue])
                {
                        menuArr = [NSArray arrayWithObjects:HISTORY,SYG_PICKUP_REQUEST,SIGN_OUT, nil];
                }
                else
                {
                        menuArr = [NSArray arrayWithObjects:HISTORY,SIGN_OUT, nil];
                }
        }
        else
                menuArr = [NSArray arrayWithObjects:LOGIN_SIGN_UP, nil];
        
}


-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        [_tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return menuArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        
                static NSString *CellIdentifier = @"RightSlideMenuCell";
                RightSlideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                        cell = [[RightSlideMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
        
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.menuTitleLbl.text = [menuArr objectAtIndex:indexPath.row];
        
        
                if([[menuArr objectAtIndex:indexPath.row] isEqualToString:HISTORY] )
                {
                   cell.menuImg.image = [UIImage imageNamed:@"blue_sales_history"];
                }
                else if([[menuArr objectAtIndex:indexPath.row] isEqualToString:SYG_PICKUP_REQUEST] )
                {
                        cell.menuImg.image = [UIImage imageNamed:@"blue_syg_pickup_request_icon"];
                }
                else if([[menuArr objectAtIndex:indexPath.row] isEqualToString:SIGN_OUT])
                {
                        cell.menuImg.image = [UIImage imageNamed:@"blue_logout_icon"];
;
                }
                else  if([[menuArr objectAtIndex:indexPath.row] isEqualToString:LOGIN_SIGN_UP])
                {
                        cell.menuImg.image = [UIImage imageNamed:@"blue_login_icon"];

                }
                 return cell;
       
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
       NSString *selectedMenu = [menuArr objectAtIndex:indexPath.row];
                if([selectedMenu isEqualToString:HISTORY] )
                {
                        if(![self.navigationController.visibleViewController isKindOfClass:[SGPurchaseHistoryViewController class]])
                        {
                            SGPurchaseHistoryViewController *objMyOrder =[storyboard instantiateViewControllerWithIdentifier:@"SGPurchaseHistoryViewController"];
                            objMyOrder.isSelectable=NO;
                            objMyOrder.isSellGadgetHistory = YES;
                            [self.navigationController pushViewController:objMyOrder animated:YES];
                        }
                }
                else if( [selectedMenu isEqualToString:SYG_PICKUP_REQUEST])
                {
                        
                        SGPurchaseHistoryViewController *objMyOrder =[storyboard instantiateViewControllerWithIdentifier:@"SGPurchaseHistoryViewController"];
                        objMyOrder.isSelectable=NO;
                        objMyOrder.isSellGadgetHistory = YES;
                        objMyOrder.isSYGPickUpRequest = YES;
                        [self.navigationController pushViewController:objMyOrder animated:YES];
                }
                else if ([selectedMenu isEqualToString:SIGN_OUT])
                {
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults removeObjectForKey:SELL_GADGET_LOGIN_USERID];
                         [defaults removeObjectForKey:SG_CORPORATE_USER];
                        [defaults removeObjectForKey:IS_RETAILER];
                        
                        // [CommonSettings navigateToSellYourGadgetLoginView];
//                        if(![self.navigationController.visibleViewController isKindOfClass:[SellYourGadgetHomeViewController class]])
                        [CommonSettings navigateToSellYourGadgetView];
                }
                else if ([selectedMenu isEqualToString:LOGIN_SIGN_UP])
                {
                        [CommonSettings navigateToSellYourGadgetLoginView];
                }
                
              //  _menuTableView.hidden = YES;
        [self willMoveToParentViewController:nil];
        [self removeFromParentViewController];
        [self.view removeFromSuperview];

}

@end
