//
//  SGMenuViewController.h
//  EB
//
//  Created by webwerks on 11/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGMenuViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
