//
//  SGPurchaseHistoryViewController.h
//  EB
//
//  Created by webwerks on 18/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGPurchaseHistoryViewController : SellYourGadgetNavigationViewController
<FinishLoadingData>

@property (weak, nonatomic) IBOutlet UILabel *orderPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(nonatomic)BOOL isSelectable;
@property (weak, nonatomic) IBOutlet UILabel *orderStatus;
@property(nonatomic)BOOL isSellGadgetHistory;
@property(nonatomic)BOOL isSYGPickUpRequest;
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backAction:(id)sender;
- (IBAction)rightMenuAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIButton *rightMenuBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;

@end
