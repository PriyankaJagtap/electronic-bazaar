//
//  SellYourGadgetRegistrationViewController.h
//  EB
//
//  Created by Neosoft on 7/28/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellYourGadgetRegistrationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNoTextField;
@property (weak, nonatomic) IBOutlet UILabel *ebPinSentLabel;
- (IBAction)resendEbPinBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *ebPinTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ebPinViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ebPinSentLabelTopConstraint;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ebPinViewBottomConstraint;

- (IBAction)alreadyHaveAccountBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
- (IBAction)submitBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *resendEBPinBtn;

@property (weak, nonatomic) IBOutlet UIButton *individualBtn;
@property (weak, nonatomic) IBOutlet UIButton *corporateBtn;
@end
