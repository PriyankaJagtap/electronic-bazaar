//
//  SellYourGadgetRegistrationViewController.m
//  EB
//
//  Created by Neosoft on 7/28/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellYourGadgetRegistrationViewController.h"
#import "SellGadgetUser.h"

@interface SellYourGadgetRegistrationViewController ()<FinishLoadingData,UITextFieldDelegate>
{
    NSString *ebPin;

}

@end

@implementation SellYourGadgetRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _mobileNoTextField.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
    [_mobileNoTextField addTarget:self
                           action:@selector(mobileValueChanged:)
                 forControlEvents:UIControlEventEditingChanged];
    
    UIColor *color = [UIColor grayColor];
//    _nameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your name " attributes:@{NSForegroundColorAttributeName: color}];
//    _mobileNoTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your mobile number" attributes:@{NSForegroundColorAttributeName: color}];
//    _emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your email id " attributes:@{NSForegroundColorAttributeName: color}];
//    _passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your password" attributes:@{NSForegroundColorAttributeName: color}];
//    _confirmPasswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your confirm password" attributes:@{NSForegroundColorAttributeName: color}];
//    _ebPinTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Ebpin" attributes:@{NSForegroundColorAttributeName: color}];
    
    // set wrap view for text field
//    [self createWrapView:_nameTextField];
//    [self createWrapView:_mobileNoTextField];
//    [self createWrapView:_emailTextField];
//    [self createWrapView:_confirmPasswordTextField];
//    [self createWrapView:_passwordTextField];
    
    [CommonSettings addPaddingAndImageToTextField:_nameTextField withImageName:@"gray_profile_icon"];
    [CommonSettings addPaddingAndImageToTextField:_mobileNoTextField withImageName:@"gray_mobile_icon"];
    [CommonSettings addPaddingAndImageToTextField:_emailTextField withImageName:@"gray_email_icon"];
    [CommonSettings addPaddingAndImageToTextField:_confirmPasswordTextField withImageName:@"gray_pwd_icon"];
    [CommonSettings addPaddingAndImageToTextField:_passwordTextField withImageName:@"gray_pwd_icon"];
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_submitBtn];
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_resendEBPinBtn];
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)createWrapView:(UITextField *)textfield{
    UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, textfield.frame.size.height)];
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = wrapView;
    [textfield setAutocorrectionType:UITextAutocorrectionTypeNo];
    
}


#pragma mark - mobile number check
-(void) mobileValueChanged:(id)sender
{
    if(_mobileNoTextField.text.length == 10)
    {
        [self checkMobileNumberExist];
       
    }
}

-(void)checkMobileNumberExist
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",CHECK_MOBILE_WS,_mobileNoTextField.text] MathodName:CHECK_MOBILE_WS];
        
    }
}


#pragma mark - get EB PIN
-(void)generateEBPin
{
    
    [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        NSString *url = [NSString stringWithFormat:@"%@%@",GENERATE_EB_PIN_WS,_mobileNoTextField.text];
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:url MathodName:@"GENERATE_EB_PIN_DATA"];
    }
}

#pragma mark - emailID check
-(void)checkEmailExist
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",CHECK_EMAIL_WS,_emailTextField.text] MathodName:CHECK_EMAIL_WS];
        
    }
    
}

#pragma mark - TextField delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[scrollView setContentOffset:CGPointMake(scrollView.frame.origin.x, 0) animated:YES];
    
    [textField resignFirstResponder];
    return  YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(textField == _mobileNoTextField)
        {
            if(textField.text.length < 10)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
        }
        else if (textField == _emailTextField)
        {
            if (_emailTextField.text.length == 0) {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
                return;
            }
            if (![CommonSettings validEmail:_emailTextField.text]) {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
                return;
                
            }
            else
            {
                [self checkEmailExist];
            }
        }
        
    });
    
    
    //  [scrollView setContentOffset:CGPointMake(scrollView.frame.origin.x, 0) animated:YES];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if(textField == _emailTextField)
    {
        return YES;
    }
    else if(textField == _mobileNoTextField)
    {
        NSUInteger newLength = [textField.text length] + [string length];
        
        if(newLength > 10){
            
            return NO;
        }
        else{
            return YES;
        }
    }
    return YES;
}


#pragma mark - IBAction methods
- (IBAction)corporateBtnClicked:(UIButton *)sender {
        [_corporateBtn setSelected:YES];
        [_individualBtn setSelected:NO];
        
}
- (IBAction)individualBtnClicked:(UIButton *)sender {
        [_corporateBtn setSelected:NO];
        [_individualBtn setSelected:YES];
       
}


- (IBAction)resendEbPinBtnClicked:(id)sender {
    _ebPinTextField.text = @"";
    [self mobileValueChanged:nil];
}
- (IBAction)submitBtnClicked:(id)sender {
    
    [self.view endEditing:NO];
        
        
    if (_nameTextField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter name."];
        return;
    }
    
    if (_emailTextField.text){
        
        if (_emailTextField.text.length == 0) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
            return;
        }
        if (![CommonSettings validEmail:_emailTextField.text]) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
            return;
            
        }
    }
    if (_mobileNoTextField.text){
        if (_mobileNoTextField.text.length < 10) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit Mobile Number"];
            return;
        }
    }
    if (_ebPinTextField.text.length == 0){
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter EB PIN."];
        return;
    }
    
    if (_passwordTextField.text)
    {
        if (_passwordTextField.text.length == 0) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter password."];
            return;
        }
        if (_confirmPasswordTextField.text.length == 0) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter confirm password."];
            return;
        }
        if (![_passwordTextField.text isEqual:_confirmPasswordTextField.text]) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Confirm Password does not match"];
            return;
        }
    }
    
    [self callRegistrationWS];
}
- (IBAction)alreadyHaveAccountBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)doneButtonDidPressed:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        // [self.view endEditing:YES];
        [_passwordTextField becomeFirstResponder];
    });
}

#pragma mark - RegisterWS

-(void)callRegistrationWS{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@""withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        // Call webservice
       
        NSString *pushID=[defaults valueForKey:@"DEVICE_TOKEN"];
        if(pushID == nil)
            pushID = @"";


            NSString *paramStr = [NSString stringWithFormat:@"email=%@&password=%@&device_type=iphone&name=%@&mobile=%@&ebpin_user=%@&ebpin_server=%@&push_id=%@&version=%@&store_name=%@&verification_code=%@&is_corporate=%d",_emailTextField.text,_passwordTextField.text,_nameTextField.text,_mobileNoTextField.text,_ebPinTextField.text,ebPin,pushID,[CommonSettings getAppCurrentVersion],_nameTextField.text,ebPin,_corporateBtn.isSelected?1:0];
        
        [callLoginService webServiceWitParameters:paramStr andURL:SELL_GADGET_REGISTRATION_WS MathodName:SELL_GADGET_REGISTRATION_WS];
    }
    
}



#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:@"GENERATE_EB_PIN_DATA"]){
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            ebPin = [response valueForKey:@"ebpin"];
            _ebPinSentLabel.text = [response valueForKey:@"message"];
            [self.view layoutIfNeeded];
            _ebPinViewHeightConstraint.constant = 30 ;
            _ebPinSentLabelTopConstraint.constant = 8;
            _ebPinViewBottomConstraint.constant = 8;
            [self.view layoutIfNeeded];
           
        }
        else{
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
        }
    }
    
    else if ([strName isEqualToString:CHECK_MOBILE_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            alert.tag = 500;
            [alert show];
        }
        else if ([[response valueForKey:@"status"]intValue]==1)
        {
            [self generateEBPin];
        }
        
    }
    else if ([strName isEqualToString:SELL_GADGET_REGISTRATION_WS])
    {
    
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            NSDictionary *dictUserDetail = response;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[[dictUserDetail objectForKey:@"user"]objectForKey:@"id"] forKey:SELL_GADGET_LOGIN_USERID];
//            [defaults setValue:_emailTextField.text forKey:SELL_GADGET_LOGIN_USERNAME];
//            [defaults setValue:_passwordTextField.text forKey:SELL_GADGET_LOGIN_USER_PASSWORD];
            
                if ([[[dictUserDetail valueForKey:@"user"] valueForKey:@"is_corporate"] integerValue] == 1) {
                        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:SG_CORPORATE_USER];
                }
                else
                {
                        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:NO] forKey:SG_CORPORATE_USER];
                }
                
            SellGadgetUser *objSellGadgetUser = [[SellGadgetUser alloc] init];
            objSellGadgetUser.email = _emailTextField.text;
            objSellGadgetUser.password = _passwordTextField.text;
            objSellGadgetUser.userId = [[dictUserDetail objectForKey:@"user"] objectForKey:@"id"];
                
                objSellGadgetUser.customerId = [[dictUserDetail objectForKey:@"user"] objectForKey:@"id"];
           // objSellGadgetUser.customerId = [dictUserDetail objectForKey:@"customer_id"];
            [SellGadgetUser saveSellGadgetUserToNsuserdefaults:objSellGadgetUser];
            [CommonSettings navigateToSellYourGadgetView];
        }
        else
        {
            [[AppDelegate getAppDelegateObj] showAlert:@"" withMessage:[response valueForKey:@"message"]];
        
        }
    }
    else if ([strName isEqualToString:CHECK_EMAIL_WS])
    {
        if ([[response valueForKey:@"status"]intValue]==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
    }
}
#pragma mark - Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 500 && buttonIndex == 0) {
        //[CommonSettings navigateToSellYourGadgetLoginView];
    }
}

@end
