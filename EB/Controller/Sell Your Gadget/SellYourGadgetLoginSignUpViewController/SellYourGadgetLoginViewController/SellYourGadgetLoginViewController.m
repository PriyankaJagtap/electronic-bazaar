//
//  SellYourGadgetLoginViewController.m
//  EB
//
//  Created by Neosoft on 7/28/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellYourGadgetLoginViewController.h"
#import "ForgotPassword.h"
#import "SellYourGadgetRegistrationViewController.h"
#import "SellGadgetUser.h"
#import "SGMobileDetailsViewController.h"
#import "SGLaptopDetailsViewController.h"
#import "SellYourGadgetLoginSignUpViewController.h"

@interface SellYourGadgetLoginViewController ()<FinishLoadingData>
{
      UITextField *forgotPwdTxtfield;
}
@end

@implementation SellYourGadgetLoginViewController
@synthesize emailIdTxt, passwordTxt;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UIColor *color = [UIColor grayColor];
////    emailIdTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your email id " attributes:@{NSForegroundColorAttributeName: color}];
//    passwordTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your password" attributes:@{NSForegroundColorAttributeName: color}];
//    
//    // set wrap view for text field
//    [self createWrapView:emailIdTxt];
//    [self createWrapView:passwordTxt];
    
    [CommonSettings addPaddingAndImageToTextField:emailIdTxt withImageName:@"gray_email_icon"];
    [CommonSettings addPaddingAndImageToTextField:passwordTxt withImageName:@"gray_pwd_icon"];
   
    
    SellGadgetUser *objSellGadgetUser = [SellGadgetUser getSellGadgetUserFromNsuserdefaults];
    emailIdTxt.text = objSellGadgetUser.email;
    passwordTxt.text = objSellGadgetUser.password;
       
        
        NSDictionary * linkAttributes = @{NSForegroundColorAttributeName:_registerBtn.titleLabel.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)};
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:_registerBtn.titleLabel.text attributes:linkAttributes];
        [_registerBtn.titleLabel setAttributedText:attributedString];
        
        
//        NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
//        NSArray * objects = [[NSArray alloc] initWithObjects:_registerBtn.titleLabel.textColor, [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
//        NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
//        
//        [_registerBtn setAttributedTitle:linkAttributes forState:UIControlStateNormal];
        
          [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_submitBtn];
        //[self corporateBtnClicked:_corporateBtn];
    [self individualBtnClicked:_individualBtn];
    
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)createWrapView:(UITextField *)textfield{
    UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, textfield.frame.size.height)];
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = wrapView;
    [textfield setAutocorrectionType:UITextAutocorrectionTypeNo];
    
}
#pragma mark - UITextField Delegate


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[textField resignFirstResponder];
    [self.view endEditing:YES];
    return  YES;
}


- (BOOL) textFieldDidChange:(UITextField *)textField
{
    UIButton *btnColor = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnColor addTarget:self action:@selector(btnOKPressedForPwd:) forControlEvents:UIControlEventTouchUpInside];
    btnColor.frame = CGRectMake((forgotPwdTxtfield.frame.size.width- 40), 3, 30, 25);
    btnColor.backgroundColor = [UIColor lightGrayColor];
    [btnColor setTitle:@"OK" forState:UIControlStateNormal];
    [btnColor setTitle:@"OK" forState:UIControlStateSelected];
    btnColor.titleLabel.font = karlaFontRegular(14.0);
    btnColor.layer.cornerRadius = 5.0;
    [forgotPwdTxtfield addSubview:btnColor];
    
    
    return YES;
}


-(void)btnOKPressedForPwd:(UIButton *)sender{
    
    if (forgotPwdTxtfield.text.length > 0) {
        if (![CommonSettings validEmail:forgotPwdTxtfield.text] ) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Incorrect email format."];
            
        }
        else{
            [FVCustomAlertView hideAlertFromView:self.view fading:YES];
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Password is been sent to your email id."];
        }
    }
}


// Check email validity



#pragma mark- IBAction methods
- (IBAction)corporateBtnClicked:(UIButton *)sender {
        [_corporateBtn setSelected:YES];
        [_individualBtn setSelected:NO];
        _storIdTextField.text = nil;
        [self.view layoutIfNeeded];
        _storeIdTextFieldHtConstraint.constant = 30;
        _storeIDTextFieldBottomConstraint.constant = 10;
        [self.view layoutIfNeeded];
}
- (IBAction)individualBtnClicked:(UIButton *)sender {
        [_corporateBtn setSelected:NO];
        [_individualBtn setSelected:YES];
        [self.view layoutIfNeeded];
        _storeIdTextFieldHtConstraint.constant = 0;
        _storeIDTextFieldBottomConstraint.constant = 0;
        [self.view layoutIfNeeded];
}


- (IBAction)showPasswordAction:(id)sender {
    _showPasswordBtn.selected = ! _showPasswordBtn.selected;
    
    if (_showPasswordBtn.selected) {
        passwordTxt.secureTextEntry = NO;
        passwordTxt.text =passwordTxt.text ;
    }
    else{
        passwordTxt.secureTextEntry = YES;
        passwordTxt.text =passwordTxt.text ;
    }
}
- (IBAction)registerNowBtnClicked:(id)sender {
//    SellYourGadgetRegistrationViewController *objVc = [[SellYourGadgetRegistrationViewController alloc] initWithNibName:@"SellYourGadgetRegistrationViewController" bundle:nil];
//    [self.navigationController pushViewController:objVc animated:YES];
        
        SellYourGadgetLoginSignUpViewController *vc = (SellYourGadgetLoginSignUpViewController*)self.parentViewController;
        [vc SignUpBtnClicked:nil];
}

- (IBAction)forgotPasswordAction:(id)sender {
    
    ForgotPassword *objc = [[UIStoryboard storyboardWithName:@"Product" bundle:nil] instantiateViewControllerWithIdentifier:@"ForgotPassword"];
    [self.navigationController pushViewController:objc animated:YES];
    
    
    
}
- (IBAction)navigateToHomeScreen:(id)sender {
    
//    // Navigate from Login screen t home screen.
//    UIStoryboard *storyBoard;
//    
//    storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    
//    UINavigationController *navigationController ;
//    
//    self.tabBarObj = [[ShowTabbars alloc] init];
//    [self.tabBarObj showTabbars];
//    [self.tabBarObj TabClickedIndex:0];
//    
//    
//    SlideMenuViewController *  objMainMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"SlideMenuViewController"];
//    
//    RightMenuViewController*  objRightMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"RightMenuViewController"];
//    
//    MFSideMenuContainerViewController *container =[MFSideMenuContainerViewController
//                                                   containerWithCenterViewController:self.tabBarObj
//                                                   leftMenuViewController:objMainMenuVC
//                                                   rightMenuViewController:objRightMenuVC];
//    
//    [AppDelegate getAppDelegateObj].navigationController=navigationController;
//    [AppDelegate getAppDelegateObj].tabBarObj=self.tabBarObj;
//    [[[UIApplication sharedApplication]delegate].window setRootViewController:container];
//    [navigationController setNavigationBarHidden:YES animated:YES];
        
        [[[AppDelegate getAppDelegateObj] tabBarObj] unhideTabbar];
        [CommonSettings navigateToSellYourGadgetView];
}

- (IBAction)submitAction:(id)sender {
        
        if (_corporateBtn.isSelected && [_storIdTextField.text isEqualToString:@""]) {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter corporate store id"];
                return;
        }
    
    if ([emailIdTxt.text isEqualToString:@""]) {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please insert email id"];
        return;
    }
    
    if(![CommonSettings validEmail: emailIdTxt.text]){
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter valid email id"];
        return;
    }

    
    if([passwordTxt.text isEqualToString:@""]){
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please insert password"];
        return;
    }
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictData = [NSMutableDictionary new];
    [dictData setObject:emailIdTxt.text forKey:@"username"];
    [dictData setObject:passwordTxt.text forKey:@"password"];
    [dictData setObject:@"iphone" forKey:@"device_type"];
        if (_corporateBtn.isSelected) {
                [dictData setObject:_storIdTextField.text forKey:@"corporate_store_id"];
        }
    
    [dictData setObject:@"normal" forKey:@"social_type"];
    [dictData setObject:@"" forKey:@"social_login_id"];
    NSLog(@"device token %@",[defaults valueForKey:@"DEVICE_TOKEN"]);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DEVICE_TOKEN"]);
    NSString *pushID=[defaults objectForKey:@"DEVICE_TOKEN"];
    [dictData setObject:pushID?pushID:@"" forKey:@"push_id"];
    [dictData setObject:@"" forKey:@"firstname"];
    [dictData setObject:@"" forKey:@"lastname"];
    [dictData setObject:@"" forKey:@"dob"];
    [dictData setObject:@"" forKey:@"mobile"];
    [dictData setObject:@"" forKey:@"profile_picture"];
         [dictData setObject:@"" forKey:@"businessType"];

    [self callLoginWS:dictData];
    
}

#pragma mark - LoginWS
-(void)callLoginWS:(NSMutableDictionary *)userdata{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    
    [userdata setValue:[CommonSettings getAppCurrentVersion] forKey:@"version"];
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:userdata url:LOGIN_WS string:@"LoginWS"];
    }
}

-(void)receivedResponseForLogin:(id)receiveData stringResponse:(NSString *)responseType{
    [FVCustomAlertView hideAlertFromView:self.view fading:NO];
    if ([responseType isEqualToString:@"success"]) {
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        NSMutableDictionary *dictUserDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
        
        DLog(@"userDetail %@",jsonObject);
            
        if ([dictUserDetail isKindOfClass:[NSDictionary class]]) {
            NSString *status = [NSString stringWithFormat:@"%ld",(long)[[dictUserDetail objectForKey:@"status"]integerValue]];
            if ([status isEqualToString:@"1"])
            {
                //NSString *tokenVal = [dictUserDetail objectForKey:@"token"]?:@"";
                 
                if([[dictUserDetail valueForKey:@"customer_id"] integerValue] == 5)
                {
                    [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Only Electronics Bazaar users can login from here"];
                    emailIdTxt.text = nil;
                    passwordTxt.text = nil;
                    return;
                }
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:[[dictUserDetail objectForKey:@"user"] objectForKey:@"id"]  forKey:SELL_GADGET_LOGIN_USERID];
                    
                    if ([[[dictUserDetail valueForKey:@"user"] valueForKey:@"is_corporate"] integerValue] == 1) {
                            
                            if([_individualBtn isSelected])
                            {
                                    [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Please login as Corporate user"];
                                    return;
                            }
                            
                          [defaults setValue:[NSNumber numberWithBool:YES] forKey:SG_CORPORATE_USER];
                    }
                    else
                    {
                             [defaults setValue:[NSNumber numberWithBool:NO] forKey:SG_CORPORATE_USER];
                    }
               
                    if ([[[dictUserDetail valueForKey:@"user"] valueForKey:@"is_retailer"] integerValue] == 1) {
                            [defaults setValue:[NSNumber numberWithBool:YES] forKey:IS_RETAILER];
                            
                    }
                    
                SellGadgetUser *objSellGadgetUser = [[SellGadgetUser alloc] init];
                objSellGadgetUser.email = emailIdTxt.text;
                objSellGadgetUser.password = passwordTxt.text;
                objSellGadgetUser.userId = [[dictUserDetail objectForKey:@"user"] objectForKey:@"id"];
                objSellGadgetUser.customerId = [[dictUserDetail objectForKey:@"user"] objectForKey:@"id"];
//                objSellGadgetUser.customerId = [dictUserDetail objectForKey:@"customer_id"];
                [SellGadgetUser saveSellGadgetUserToNsuserdefaults:objSellGadgetUser];
                
                    _storIdTextField.text = @"";
                    emailIdTxt.text = @"";
                    passwordTxt.text = @"";
                    SellYourGadgetLoginSignUpViewController *objVc  = ((SellYourGadgetLoginSignUpViewController *)self.parentViewController);
                    if(objVc.isFromSGMobileDetail)
                    {
                            UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Product" bundle:nil];
                            SGMobileDetailsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SGMobileDetailsViewController"];
                            vc.mobileDetailsDic = objVc.productDic;
                            [self.navigationController pushViewController:vc animated:YES];
                    }
                    else if(objVc.isFromSGLaptopDetail)
                    {
                            UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Product" bundle:nil];
                            SGLaptopDetailsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SGLaptopDetailsViewController"];
                            vc.laptopDetailDic = objVc.productDic;
                            [self.navigationController pushViewController:vc animated:YES];
                    }
                    else
                    {
                            [CommonSettings navigateToSellYourGadgetView];
                    }

            }
            else if ( [status isEqualToString:@"2"])
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[dictUserDetail valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alert show];
                
            }
            else{
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:[dictUserDetail valueForKey:@"message"]];
            }
        }
        else{
            [[AppDelegate getAppDelegateObj]showAlert:@"Sorry" withMessage:@"Login couldn't succeed."];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
