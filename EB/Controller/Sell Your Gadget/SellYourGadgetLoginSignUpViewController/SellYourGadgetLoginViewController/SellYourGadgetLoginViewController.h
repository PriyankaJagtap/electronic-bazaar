//
//  SellYourGadgetLoginViewController.h
//  EB
//
//  Created by Neosoft on 7/28/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellYourGadgetLoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *emailIdTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;
@property (weak, nonatomic) IBOutlet UIButton *showPasswordBtn;
- (IBAction)showPasswordAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn;
- (IBAction)forgotPasswordAction:(id)sender;
- (IBAction)submitAction:(id)sender;
@property (nonatomic) BOOL isSellGadgetBannerClicked;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;

@property (strong,nonatomic)ShowTabbars *tabBarObj;

@property (weak, nonatomic) IBOutlet UIButton *individualBtn;
@property (weak, nonatomic) IBOutlet UIButton *corporateBtn;
@property (weak, nonatomic) IBOutlet UITextField *storIdTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *storeIdTextFieldHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *storeIDTextFieldBottomConstraint;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end
