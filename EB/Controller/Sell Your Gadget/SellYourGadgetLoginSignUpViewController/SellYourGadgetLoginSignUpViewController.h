//
//  SellYourGadgetLoginSignUpViewController.h
//  EB
//
//  Created by Neosoft on 8/8/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellYourGadgetLoginSignUpViewController : UIViewController
- (IBAction)LoginBtnClicked:(id)sender;
- (IBAction)SignUpBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLabelLeadingConstraint;
@property (weak, nonatomic) IBOutlet UIView *loginContainerView;

@property (weak, nonatomic) IBOutlet UIView *signUpContainerView;
@property (nonatomic) BOOL isFromSGMobileDetail;
@property (nonatomic) BOOL isFromSGLaptopDetail;
@property (nonatomic, strong) NSDictionary *productDic;

@end
