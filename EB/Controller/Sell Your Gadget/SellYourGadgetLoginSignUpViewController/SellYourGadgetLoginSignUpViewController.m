//
//  SellYourGadgetLoginSignUpViewController.m
//  EB
//
//  Created by Neosoft on 8/8/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellYourGadgetLoginSignUpViewController.h"
#import "SellYourGadgetLoginViewController.h"
#import "SellYourGadgetRegistrationViewController.h"

@interface SellYourGadgetLoginSignUpViewController ()

@end

@implementation SellYourGadgetLoginSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _loginContainerView.hidden = NO;
    _signUpContainerView.hidden = YES;
    [self displayContentController:[[SellYourGadgetLoginViewController alloc] initWithNibName:@"SellYourGadgetLoginViewController" bundle:nil] toView:_loginContainerView];
    
    [self displayContentController:[[SellYourGadgetRegistrationViewController alloc] initWithNibName:@"SellYourGadgetRegistrationViewController" bundle:nil] toView:_signUpContainerView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        [[APP_DELEGATE tabBarObj] hideTabbar];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBAction methods
- (IBAction)LoginBtnClicked:(id)sender {
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
        _loginContainerView.hidden = NO;
        _signUpContainerView.hidden = YES;
        _bottomLabelLeadingConstraint.constant = 0;
        [self.view layoutIfNeeded];

    }];
}

- (IBAction)SignUpBtnClicked:(id)sender {
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
        _loginContainerView.hidden = YES;
        _signUpContainerView.hidden = NO;
        _bottomLabelLeadingConstraint.constant = kSCREEN_WIDTH/2;
        [self.view layoutIfNeeded];
    }];
    
}


#pragma mark - display views
- (void) displayContentController: (UIViewController*) content toView:(UIView *)toView;
{
    content.view.frame = toView.bounds;
    [toView addSubview:content.view];
    [self addChildViewController:content];
    [content didMoveToParentViewController:self];
}

@end
