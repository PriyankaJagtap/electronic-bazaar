//
//  SellYourMobileViewController.h
//  EB
//
//  Created by webwerks on 08/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellYourMobileViewController : SellYourGadgetNavigationViewController
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UITextField *IMEINoTextField;

@property (weak, nonatomic) IBOutlet UIButton *goodBtn;
@property (weak, nonatomic) IBOutlet UIButton *yesBtn;
@property (weak, nonatomic) IBOutlet UIButton *noBtn;
;
@property (weak, nonatomic) IBOutlet UIButton *memoryBtn;
@property (weak, nonatomic) IBOutlet UIButton *modelBtn;
@property (weak, nonatomic) IBOutlet UIButton *brandBtn;
@property (weak, nonatomic) IBOutlet UIButton *fairBtn;
@property (weak, nonatomic) IBOutlet UIButton *poorBtn;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@end
