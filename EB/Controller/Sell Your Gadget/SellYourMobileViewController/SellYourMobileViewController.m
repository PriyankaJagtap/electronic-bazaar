//
//  SellYourMobileViewController.m
//  EB
//
//  Created by webwerks on 08/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellYourMobileViewController.h"
#import "SellGadgetUser.h"
#import "SGInfoWebViewController.h"
#import "SGMobileDetailsViewController.h"
#import "SellYourGadgetLoginSignUpViewController.h"
#import "SGMenuViewController.h"


@interface SellYourMobileViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,FinishLoadingData>
{
        NSArray *pickerArray;
        UIPickerView *picker;
        UIView *pickerBackgroundView;
        
        NSArray *brandArr;
        NSArray *modelArr;
        // NSArray *memoryArr;
        NSArray *conditionArr;
        NSArray *chargerArr;
        NSArray *sellGadgetBrandDataArray;
        NSDictionary *responseDic;
        NSInteger basePrice;
        NSMutableDictionary *mobileDataDic;
        
        NSInteger *pickerSelectedIndex;
}


@end

@implementation SellYourMobileViewController

- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        [super setViewControllerTitle:@"Sell Your Smartphone"];
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj hideTabbar];
        pickerSelectedIndex = 0;
           [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_submitBtn];
        [self getSellGadgetBrands];
        [CommonSettings addDropDownArrowImageInBtn:_brandBtn];
        [CommonSettings addDropDownArrowImageInBtn:_modelBtn];
        [CommonSettings addDropDownArrowImageInBtn:_memoryBtn];
        [_goodBtn setSelected:YES];
        [_yesBtn setSelected:YES];
        
}

- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}
#pragma mark - get sell gadget category

-(void)getSellGadgetBrands
{
        // Loader
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
        
        // Call Home screen webservice
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@",SELL_GADGET_MOBILE_BRAND_WS] MathodName:SELL_GADGET_MOBILE_BRAND_WS];
                
        }
        
}

-(void)callGetModelWebservice:(NSString *)brandCode
{
        
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                NSString *url = [NSString stringWithFormat:@"%@%@",SG_GET_MOBILE_MODEL_WS,brandCode];
                [callLoginService GetWebServiceWithURL:url MathodName:SG_GET_MOBILE_MODEL_WS];
        }
}


-(void)callGetConditionsWebservice:(NSString *)modelCode
{
        
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                NSString *url = [NSString stringWithFormat:@"%@%@",SG_GET_MOBILE_CONDITIONS_WS,modelCode];
                [callLoginService GetWebServiceWithURL:url MathodName:SG_GET_MOBILE_CONDITIONS_WS];
        }
}

#pragma mark - calculate price
-(void)calculatePrice
{
        basePrice = 0;
        NSArray *filtered;
        NSDictionary *item;
        mobileDataDic = [NSMutableDictionary new];
        
        NSString *modelStr = [_modelBtn titleForState:UIControlStateNormal];
        filtered = [modelArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", modelStr]];
        if(filtered.count > 0)
        {
                item = [filtered objectAtIndex:0];
                basePrice += [[item valueForKey:@"option_price"] integerValue];
                [mobileDataDic setValue:[item valueForKey:@"option_type_id"] forKey:MODEL_ID];
                
                
        }
        [mobileDataDic setValue:modelStr forKey:MODEL];
        
        
        
        if([_yesBtn isSelected])
        {
                if(chargerArr.count > 0)
                {
                        item = [chargerArr objectAtIndex:0];
                        basePrice += [[item valueForKey:@"option_price"] integerValue];
                        
                }
                
                [mobileDataDic setValue:@"Yes" forKey:CHARGER];
        }else if([_noBtn isSelected])
        {
                if(chargerArr.count > 0)
                {
                        item = [chargerArr objectAtIndex:1];
                        basePrice += [[item valueForKey:@"option_price"] integerValue];
                        
                }
                [mobileDataDic setValue:@"No" forKey:CHARGER];
        }
        
        
        
        if([_goodBtn isSelected])
        {
                if(conditionArr.count > 0)
                {
                        item = [conditionArr objectAtIndex:0];
                        
                        basePrice += [[item valueForKey:@"option_price"] integerValue];
                        [mobileDataDic setValue:[item valueForKey:@"option_type_id"] forKey:CONDITION_ID];
                        
                }
                [mobileDataDic setValue:@"Good" forKey:CONDITION];
        }
        else  if([_fairBtn isSelected])
        {
                if(conditionArr.count > 0)
                {
                        item = [conditionArr objectAtIndex:1];
                        basePrice += [[item valueForKey:@"option_price"] integerValue];
                        [mobileDataDic setValue:[item valueForKey:@"option_type_id"] forKey:CONDITION_ID];
                }
                [mobileDataDic setValue:@"Fair" forKey:CONDITION];
        }else if([_poorBtn isSelected])
        {
                if(conditionArr.count > 0)
                {
                        item = [conditionArr objectAtIndex:2];
                        basePrice += [[item valueForKey:@"option_price"] integerValue];
                        [mobileDataDic setValue:[item valueForKey:@"option_type_id"] forKey:CONDITION_ID];
                }
                [mobileDataDic setValue:@"Poor" forKey:CONDITION];
        }
        
        
        //        NSString *memoryStr = [_memoryBtn titleForState:UIControlStateNormal];
        //        filtered = [memoryArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", memoryStr]];
        //        if(filtered.count > 0)
        //        {
        //                item = [filtered objectAtIndex:0];
        //                basePrice += [[item valueForKey:@"option_price"] integerValue];
        //                [mobileDataDic setValue:[item valueForKey:@"option_type_id"] forKey:MEMORY_ID];
        //
        //        }
        //        [mobileDataDic setValue:memoryStr forKey:MEMORY];
        
        
        _totalPriceLabel.text = [NSString stringWithFormat:@"Get upto %@",[[CommonSettings sharedInstance] formatIntegerPrice:basePrice]];
        [mobileDataDic setValue:[NSString stringWithFormat:@"%ld",(long)basePrice] forKey:PRICE];
        [mobileDataDic setValue:[_brandBtn titleForState:UIControlStateNormal] forKey:BRAND];
        
        
}

#pragma mark - textField delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        
        if(textField == _IMEINoTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_IMEI_NUMBER] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                if(newLength <= 15)
                {
                        return [string isEqualToString:filtered];
                }
                
                if(newLength > 15){
                        
                        return NO;
                }
        }
        
        return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
        
        return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
        [textField resignFirstResponder];
        return YES;
}



#pragma mark - request finish method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if([strName isEqualToString:SELL_GADGET_MOBILE_BRAND_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        //                sellCategoryDataArray = [[response valueForKey:@"data"] valueForKey:@"brands"];
                        //                [_brandsCollectionView reloadData];
                        NSArray *brandDataArray = [[response valueForKey:@"data"] valueForKey:@"brands"];
                        sellGadgetBrandDataArray = brandDataArray;
                        brandArr = [brandDataArray valueForKey:@"name"];
                        [_brandBtn setTitle:[brandArr objectAtIndex:0] forState:UIControlStateNormal];
                        
                        [self callGetModelWebservice:[[brandDataArray objectAtIndex:0] valueForKey:@"code"]];
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        else if ([strName isEqualToString:SG_GET_MOBILE_MODEL_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        NSDictionary *dic = [[response valueForKey:@"data"] valueForKey:@"products"];
                        if([[dic valueForKey:@"options"] isKindOfClass:[NSDictionary class]])
                        {
                                modelArr = [[dic valueForKey:@"options"] valueForKey:@"Model"];
                                //memoryArr = [[responseDic valueForKey:@"options"] valueForKey:@"Memory"];
                                //                                conditionArr = [[responseDic valueForKey:@"options"] valueForKey:@"Whatistheconditionoftheitem?"];
                                //
                                //                                chargerArr = [[responseDic valueForKey:@"options"] valueForKey:CHARGER_KEY];
                                //                                [self setInitialValues];
                                
                                if(modelArr.count > 0)
                                        [_modelBtn setTitle:[[modelArr objectAtIndex:0] valueForKey:@"option_name"] forState:UIControlStateNormal];
                                else
                                        [_modelBtn setTitle:@"" forState:UIControlStateNormal];
                                NSString *modelStr = [_modelBtn titleForState:UIControlStateNormal];
                                
                                NSArray *filtered = [modelArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", modelStr]];
                                if(filtered.count > 0)
                                {
                                        NSDictionary *item = [filtered objectAtIndex:0];
                                        [self callGetConditionsWebservice:[item valueForKey:@"option_type_id"]];
                                }
                                
                        }
                        else
                        {
                                modelArr = nil;
                                // memoryArr = nil;
                                // conditionArr = nil;
                                // chargerArr = nil;
                                // [self setInitialValues];
                                
                        }
                        
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                //
        }
        else if ([strName isEqualToString:SG_GET_MOBILE_CONDITIONS_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        responseDic = [[response valueForKey:@"data"] valueForKey:@"products"];
                        if([[responseDic valueForKey:@"options"] isKindOfClass:[NSDictionary class]])
                        {
                              conditionArr = [[responseDic valueForKey:@"options"] valueForKey:@"Whatistheconditionoftheitem?"];
                                
                              chargerArr = [[responseDic valueForKey:@"options"] valueForKey:CHARGER_KEY];
                              [self calculatePrice];
                        }
                        else
                        {
                                
                                conditionArr = nil;
                                chargerArr = nil;
                                [self calculatePrice];
                                
                        }
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }

        }
        
}


-(void)setInitialValues
{
//        if(modelArr.count > 0)
//                [_modelBtn setTitle:[[modelArr objectAtIndex:0] valueForKey:@"option_name"] forState:UIControlStateNormal];
//        else
//                [_modelBtn setTitle:@"" forState:UIControlStateNormal];
        
        //        if(memoryArr.count > 0)
        //                [_memoryBtn setTitle:[[memoryArr objectAtIndex:0] valueForKey:@"option_name"] forState:UIControlStateNormal];
        //        else
        //                [_memoryBtn setTitle:@"" forState:UIControlStateNormal];
        //
        [self calculatePrice];
        
}



#pragma mark - IBACtion methods
- (IBAction)imeiInfoBtnClicked:(id)sender {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SGInfoWebViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"SGInfoWebViewController"];

    viewController.webViewUrl = SYG_IMEI_INFO_CONTENT; //@"http://electronicsbazaar.com/warranty-app/popup-mobile.html";
        [self addChildViewController:viewController];
        viewController.view.frame = self.view.frame;
        [self.view addSubview:viewController.view];
        viewController.view.alpha = 0;
        [viewController didMoveToParentViewController:self];
        
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^
         {
                 viewController.view.alpha = 1;
         }
                         completion:nil];
        
}


- (IBAction)backBtnClicked:(id)sender {
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj unhideTabbar];
        [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)goodBtnClicked:(id)sender {
        [_goodBtn setSelected:YES];
        [_poorBtn setSelected:NO];
        [_fairBtn setSelected:NO];
        [self calculatePrice];
}
- (IBAction)fairBtnClicked:(UIButton *)sender {
        [_poorBtn setSelected:NO];
        [_fairBtn setSelected:YES];
        [_goodBtn setSelected:NO];
        [self calculatePrice];
}
- (IBAction)poorBtnClicked:(UIButton *)sender {
        [_poorBtn setSelected:YES];
        [_fairBtn setSelected:NO];
        [_goodBtn setSelected:NO];
        [self calculatePrice];
        
}
- (IBAction)yesBtnClicked:(id)sender {
        [_yesBtn setSelected:YES];
        [_noBtn setSelected:NO];
        [self calculatePrice];
}
- (IBAction)noBtnClicked:(id)sender {
        [_yesBtn setSelected:NO];
        [_noBtn setSelected:YES];
        [self calculatePrice];
}
- (IBAction)rightMenuBtnClicked:(id)sender {
        
        [CommonSettings displaySellYourGadgetRightMenu:self];
}


- (IBAction)brandBtnClicked:(UIButton *)sender {
        
        _IMEINoTextField.text = nil;
        pickerArray = brandArr;
        [self setPickerView:(int)sender.tag btnTitle:[sender titleForState:UIControlStateNormal]];
}
- (IBAction)modelBtnClicked:(UIButton *)sender {
        pickerArray = [modelArr valueForKey:@"option_name"];
        [self setPickerView:(int)sender.tag btnTitle:[sender titleForState:UIControlStateNormal]];
}
- (IBAction)memoryBtnClicked:(UIButton *)sender {
        //        pickerArray = [memoryArr valueForKey:@"option_name"];
        //        [self setPickerView:(int)sender.tag btnTitle:[sender titleForState:UIControlStateNormal]];
}


- (IBAction)submitBtnClicked:(id)sender {
        [self.view endEditing:YES];
        
        if(_IMEINoTextField.text.length < 15)
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter valid IMEI"];
                return;
        }
        
        [mobileDataDic setValue:_IMEINoTextField.text forKey:IMEI_NO];
        
        [mobileDataDic setValue:[responseDic valueForKey:@"product_id"] forKey:BRAND_PRODUCT_ID];
        
        if([CommonSettings isUserLoggedInSellYourGadget])
        {
                UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Product" bundle:nil];
                SGMobileDetailsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SGMobileDetailsViewController"];
                vc.mobileDetailsDic = mobileDataDic;
                [self.navigationController pushViewController:vc animated:YES];
                
        }
        else
        {
                SellYourGadgetLoginSignUpViewController *objVc = [[SellYourGadgetLoginSignUpViewController alloc] initWithNibName:@"SellYourGadgetLoginSignUpViewController" bundle:nil];
                objVc.isFromSGMobileDetail = YES;
                objVc.productDic = mobileDataDic;
                [self.navigationController pushViewController:objVc animated:YES];
        }
        
}


#pragma mark - picker custom methods
-(void)setPickerView:(int) tag btnTitle:(NSString *)selectedBtnTitle{
        
        if(pickerBackgroundView == nil)
        {
                pickerBackgroundView =[[UIView alloc]init];
                pickerBackgroundView.frame = CGRectMake(0, self.view.frame.size.height - 200
                                                        , self.view.frame.size.width, 200);
                
                
                UIButton *doneButton = [[UIButton alloc]init];
                doneButton.frame =CGRectMake(0, 0, pickerBackgroundView.frame.size.width, 30);
                [doneButton setTitle:@"Done" forState:UIControlStateNormal];
                doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                doneButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
                doneButton.backgroundColor =[UIColor lightGrayColor];
                [doneButton addTarget:self action:@selector(doneTouched:) forControlEvents:UIControlEventTouchUpInside];
                [pickerBackgroundView addSubview:doneButton];
                //processorView.hidden = YES;
                [self.view addSubview:pickerBackgroundView];
        }
        
        picker = [[UIPickerView alloc]init];
        picker.frame = CGRectMake(0, 30, self.view.frame.size.width, 170);
        picker.dataSource = self;
        picker.tag = tag;
        picker.delegate = self;
        
        //                NSUInteger index = [pickerArray indexOfObjectPassingTest:
        //                                    ^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop)
        //                                    {
        //                                            return [[dict objectForKey:@"option_name"] isEqual:selectedBtnTitle];
        //                                    }
        //                                    ];
        
        NSUInteger index = [pickerArray indexOfObject:selectedBtnTitle];
        if(index != NSNotFound)
                [picker selectRow:index inComponent:0 animated:YES];
        
        
        picker.backgroundColor = [UIColor whiteColor];
        picker.showsSelectionIndicator = YES;
        [pickerBackgroundView addSubview:picker];
        [picker reloadAllComponents];
        //[processorPicker setHidden:YES];
}


- (void)doneTouched:(UIBarButtonItem *)sender{
        //[processorPicker setHidden:YES];
        pickerBackgroundView.hidden = YES;
        [pickerBackgroundView removeFromSuperview];
        pickerBackgroundView = nil;
}


#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
        return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
        return [pickerArray count];
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
        
        
        switch (pickerView.tag) {
                case 10:
                {
                        NSString *brandBtnTitleStr = [_brandBtn titleForState:UIControlStateNormal];
                        [_brandBtn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
                        if(![brandBtnTitleStr isEqualToString:[_brandBtn titleForState:UIControlStateNormal]])
                        {
                                NSArray *filtered = [sellGadgetBrandDataArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name == %@)", [_brandBtn titleForState:UIControlStateNormal]]];
                                if(filtered.count != 0)
                                {
                                        NSDictionary *item = [filtered objectAtIndex:0];
                                        [self callGetModelWebservice:[item valueForKey:@"code"]];
                                }
                        }
                }
                        break;
                case 20:
                {
                        NSString *modelBtnTitleStr = [_modelBtn titleForState:UIControlStateNormal];
                        [_modelBtn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
                        if(![modelBtnTitleStr isEqualToString:[_modelBtn titleForState:UIControlStateNormal]])
                        {
                                NSArray *filtered = [modelArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", [_modelBtn titleForState:UIControlStateNormal]]];
                                if(filtered.count != 0)
                                {
                                        NSDictionary *item = [filtered objectAtIndex:0];
                                        [self callGetConditionsWebservice:[item valueForKey:@"option_type_id"]];
                                }
                        }
                        [self calculatePrice];
                }
                        break;
                        
                case 30:
                {
                        [_memoryBtn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
                        [self calculatePrice];
                }
                        break;
                        
                default:
                        break;
        }
        
        
        
        //        if(pickerView.tag == 10)
        //                [_processorBtn setTitle:[[pickerArray objectAtIndex:row]valueForKey:@"option_name"] forState:UIControlStateNormal];
        //        else if(pickerView.tag == 11)
        //                [_ramBtn setTitle:[[pickerArray objectAtIndex:row]valueForKey:@"option_name"] forState:UIControlStateNormal];
        //        else if(pickerView.tag == 12)
        //                [_storageBtn setTitle:[[pickerArray objectAtIndex:row]valueForKey:@"option_name"] forState:UIControlStateNormal];
        //        [self setProductPrice];
        
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
        //        return [[pickerArray objectAtIndex:row] valueForKey:@"option_name"];
        return [pickerArray objectAtIndex:row];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
