//
//  PickUpAddressTableViewCell.m
//  EB
//
//  Created by Neosoft on 2/24/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "PickUpAddressTableViewCell.h"

@implementation PickUpAddressTableViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

- (void)initialize
{
    // This code is only called once
    self.bgView.layer.cornerRadius = 5;
    [self.bgView setClipsToBounds:YES];
    [[CommonSettings sharedInstance] setCornerRadiusToTopLeftAndTopRight:_pickUpAddressLabel];
    self.addNewAddressView.layer.cornerRadius = 5;
    self.addNewAddressView.layer.borderWidth = 0.5;
    self.addNewAddressView.layer.borderColor = [UIColor lightGrayColor].CGColor;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
