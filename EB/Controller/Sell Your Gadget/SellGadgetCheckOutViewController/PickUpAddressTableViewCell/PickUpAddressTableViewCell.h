//
//  PickUpAddressTableViewCell.h
//  EB
//
//  Created by Neosoft on 2/24/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickUpAddressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *pickUpBtn;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *acceptTermsBtn;
@property (weak,nonatomic) IBOutlet UILabel *pickUpAddressLabel;
@property (weak, nonatomic) IBOutlet UITextField *addressLine1TextField;
@property (weak, nonatomic) IBOutlet UITextField *addressLine2TextField;
@property (weak, nonatomic) IBOutlet UITextField *pincodeTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickUpAddressViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *addNewAddressView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickUpBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *sellNowBtn;


@end
