//
//  SellGadgetCheckOutViewController.m
//  EB
//
//  Created by Neosoft on 2/24/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellGadgetCheckOutViewController.h"
#import "BankInfoTableViewCell.h"
#import "SerialTableViewCell.h"
#import "PickUpAddressTableViewCell.h"
#import "SellGadgetTermsAndConditionViewController.h"
#import "ThankYouChekoutViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SellGadgetUser.h"
#import "WarrantyViewController.h"
#import "SGWebViewController.h"

#define ACCEPTABLE_CUSTOMER_NAME_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define ACCEPTABLE_BANK_NAME_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._-"

#define ACCEPTABLE_IFSC_CODE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

#define ACCEPTABLE_BANK_NUMBER_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"

@interface SellGadgetCheckOutViewController ()<UITextFieldDelegate,FinishLoadingData,UIPickerViewDelegate,UIPickerViewDataSource>
{
        NSArray *addressListArr;
        UIPickerView *pickerView;
        UIView *bgPickerView;
        NSArray *pickerArray;
        NSInteger pickerSelectedIndex;
        bool displayBankdetailBool;
        UITextField *searialTxtField;
        UIButton *pickUpBtn;
        UIButton *acceptBtn;
        bool isAddressesAvailable;
        
        NSString *addressId;
        NSString *city;
        NSString *state;
        NSString *country;
        NSArray *bankListArr;
        
        NSInteger bankListSelectedIndex;

        
}
@end

@implementation SellGadgetCheckOutViewController

- (void)viewDidLoad {
        [super viewDidLoad];
        [super setViewControllerTitle:@"Details"];
        // Do any additional setup after loading the view.
        _tblView.estimatedRowHeight = 200;
        _tblView.rowHeight = UITableViewAutomaticDimension;
        pickerSelectedIndex = 0;
        bankListSelectedIndex = 0;
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj hideTabbar];
        [self callAddressListWS];
        
}
-(void)viewDidAppear:(BOOL)animated
{
        [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}


//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.row == 0)
        {
                static NSString *simpleTableIdentifier = @"BankInfoTableViewCell";
                BankInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil) {
                        cell = [[BankInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                        
                }
                // [[CommonSettings sharedInstance] setShadow:cell.bgView];
                // cell.bgView.layer.cornerRadius = 5.0f;
                cell.selectionStyle = UITableViewCellEditingStyleNone;
                if (bankListArr.count > 0) {
                        [cell layoutIfNeeded];
                        
                        NSString *str = [bankListArr objectAtIndex:bankListSelectedIndex];
                        
                        NSString *bankNameStr = [str stringByReplacingOccurrencesOfString:@"," withString:@"\n"];
                        [cell.selectBankDetailsBtn setTitle:bankNameStr forState:UIControlStateNormal];
                        
                        CGFloat selectBankBtnHt = [[CommonSettings sharedInstance] calculateBtnHeight:cell.selectBankDetailsBtn];
                        cell.selectBankDetailBtnHtConstraint.constant = selectBankBtnHt +20;
                        cell.bankListViewHeightConstraint.constant = 82 + selectBankBtnHt;
                        [cell layoutIfNeeded];
                        
                }
                else
                {
                        [cell layoutIfNeeded];
                        cell.bankListViewHeightConstraint.constant = 0;
                        [cell layoutIfNeeded];

                }
                return cell;
        }
        else
        {
                static NSString *simpleTableIdentifier = @"PickUpAddressTableViewCell";
                PickUpAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil) {
                        cell = [[PickUpAddressTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
                        
                }
                //[[CommonSettings sharedInstance] setShadow:cell.bgView];
                
                [[GradientColorClass sharedInstance] setGradientBackgroundToButton:cell.sellNowBtn];
                cell.selectionStyle = UITableViewCellEditingStyleNone;
                if(isAddressesAvailable)
                {
                        [cell.pickUpBtn setTitle:[[addressListArr objectAtIndex:pickerSelectedIndex] valueForKey:@"complete_address"] forState:UIControlStateNormal];
                        [self.view layoutIfNeeded];
                        CGFloat pickUPBtnHt = [[CommonSettings sharedInstance] calculateBtnHeight:cell.pickUpBtn];
                        cell.pickUpBtnHeightConstraint.constant = pickUPBtnHt +20;
                        cell.pickUpAddressViewHeightConstraint.constant = 124 + pickUPBtnHt;
                        [self.view layoutIfNeeded];
                }
                else
                {
                        [self.view layoutIfNeeded];
                        cell.pickUpAddressViewHeightConstraint.constant = 0;
                        [self.view layoutIfNeeded];
                }
                [CommonSettings addUnderLinetoBtnTitle:cell.acceptTermsBtn];
                [cell.pincodeTextField addTarget:self
                                          action:@selector(pincodeValueChanged:)
                                forControlEvents:UIControlEventEditingChanged];
                
                [cell.pickUpBtn addTarget:self action:@selector(pickUpAddressBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
                        if(pickUpBtn == nil)
                        pickUpBtn = cell.pickUpBtn;
                
                [cell.acceptTermsBtn addTarget:self action:@selector(acceptTermsAndConditionBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UIImage *selectedBtnImage = [UIImage imageNamed:@"green_chekbox_selected"];
                UIImage *defaultBtnImage = [UIImage imageNamed:@"gray_chekbox"];
                if(acceptBtn == nil)
                        acceptBtn = cell.acceptBtn;
                
                [acceptBtn setImage:defaultBtnImage forState:UIControlStateNormal];
                [acceptBtn setImage:selectedBtnImage forState:UIControlStateSelected];
                [acceptBtn addTarget:self action:@selector(acceptBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
        }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - get addreList
-(void)callGetBankListWS{
        Webservice  *callAddlistWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callAddlistWs.delegate =self;
               
                SellGadgetUser *objSellGadgetUser = [SellGadgetUser getSellGadgetUserFromNsuserdefaults];
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                NSString *url = [NSString stringWithFormat:@"%@%@&user_id=%@&is_retailer=0",SG_GET_BANK_LIST_WS,objSellGadgetUser.email,objSellGadgetUser.userId];
                
                [callAddlistWs GetWebServiceWithURL:url MathodName:SG_GET_BANK_LIST_WS];
                
        }
}

#pragma mark - get addreList
-(void)callAddressListWS{
        Webservice  *callAddlistWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callAddlistWs.delegate =self;
                int userId;
                if([AppDelegate getAppDelegateObj].isFromLenovoLogin)
                {
                        userId = [AppDelegate getAppDelegateObj].lenovo_userId;
                }
                else
                {
                        SellGadgetUser *objSellGadgetUser = [SellGadgetUser getSellGadgetUserFromNsuserdefaults];
                        userId = [objSellGadgetUser.userId intValue];
                        //            userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
                }
                
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                [callAddlistWs webServiceWitParameters:[NSString stringWithFormat:@"userid=%d",userId] andURL:GET_CUSTOMER_ADDRESS_LIST_WS MathodName:@"ADDRESS_LIST"];
                
        }
}

#pragma mark - request finished method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:@"ADDRESS_LIST"])
        {
                NSLog(@"RESPONSE=%@",response);
                addressListArr=[response valueForKey:@"result"];
                if(addressListArr.count == 0)
                {
                        //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please register with proper address" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                        //            alert.tag = 200;
                        //            [alert show];
                        isAddressesAvailable = NO;
                        
                }
                else
                {
                       // pickerArray = addressListArr;
                        pickerSelectedIndex = 0;
                        isAddressesAvailable = YES;
                }
              
                [self callGetBankListWS];
                
        }
        else if ([strName isEqualToString:SG_GET_BANK_LIST_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        bankListArr = [response valueForKey:@"bank_list"];
                        bankListSelectedIndex = 0;
                }
                
                  [_tblView reloadData];
                
        }
        else if ([strName isEqualToString:SELL_GADGET_CHEKOUT_WS]){
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
                        
                        ThankYouChekoutViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"ThankYouChekoutViewController"];
                        objVc.orderId = [[response valueForKey:@"data"] valueForKey:@"order_id"];
                        
                        [self.navigationController pushViewController:objVc animated:YES];
                        
                        //            [[CommonSettings sharedInstance]showAlertTitle:@"" message:[[response valueForKey:@"data"] valueForKey:@"message"]];
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
}


-(void)receivedResponseForCityData:(id)receiveData stringResponse:(NSString*)responseType
{
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        NSMutableDictionary *dictUserDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
        
        NSLog(@"userDetail %@",jsonObject);
        if([[dictUserDetail valueForKey:@"status"] boolValue])
        {
                city = [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"city"];
                state = [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"default_name"];
                country = [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"country_code"];
                
                if([city isKindOfClass:[NSNull class]] ||[state isKindOfClass:[NSNull class]] ||[country isKindOfClass:[NSNull class]] )
                {
                        city = @"";
                        state = @"";
                        country = @"";
                }
        }
        else
        {
                [APP_DELEGATE showAlert:@"" withMessage:[dictUserDetail valueForKey:@"message"]];
                city = @"";
                state = @"";
                country = @"";
        }
}

#pragma mark - Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
        if (alertView.tag == 200 && buttonIndex == 0) {
                [self.navigationController popViewControllerAnimated:YES];
        }
}


#pragma mark - text field delegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        //    if(textField.tag == 10 && [_productType isEqualToString:@"laptop"])
        //    {
        //        NSUInteger newLength = [textField.text length];
        //        if(newLength == 6 && !displayBankdetailBool)
        //        {
        //            textField.text = [textField.text stringByAppendingString:string];
        //            displayBankdetailBool = YES;
        //            [_tblView reloadData];
        //
        //        }
        //        else
        //        {
        ////            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        ////           if(newString.length == 0)
        ////           {
        ////               textField.text = nil;
        ////            displayBankdetailBool = NO;
        ////            [_tblView reloadData];
        ////           }
        //
        //            if(textField.text.length == 5 && displayBankdetailBool)
        //            {
        //                displayBankdetailBool = NO;
        //                [_tblView reloadData];
        //            }
        //        }
        //
        //        if(newLength > 15){
        //
        //            return NO;
        //        }
        //        else{
        //            return YES;
        //        }
        //
        //    }
        //    else if(textField.tag == 10 && [_productType isEqualToString:@"mobile"])
        //    {
        //        NSUInteger newLength = [textField.text length] + [string length];
        //        if(newLength == 15 && !displayBankdetailBool)
        //        {
        //            textField.text = [textField.text stringByAppendingString:string];
        //            displayBankdetailBool = YES;
        //            [_tblView reloadData];
        //        }
        //        else
        //        {
        //            if(textField.text.length == 14 && displayBankdetailBool)
        //            {
        //                displayBankdetailBool = NO;
        //                [_tblView reloadData];
        //            }
        //        }
        //        if(newLength > 15){
        //
        //            return NO;
        //        }
        //        else{
        //            return YES;
        //        }
        //    }
        //    else
        //    {
        BankInfoTableViewCell *cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        if(textField == cell.customerNameTxtField)
        {
                return [[CommonSettings sharedInstance] allParticularCharInTextFieldWithString:string acceptableCharStr:ACCEPTABLE_CUSTOMER_NAME_CHARACTERS];
        }
        else  if(textField == cell.bankNameTxtField)
        {
                return [[CommonSettings sharedInstance] allParticularCharInTextFieldWithString:string acceptableCharStr:ACCEPTABLE_BANK_NAME_CHARACTERS];
        }
        else if (textField == cell.ifscCodeTxtField)
        {
                return [[CommonSettings sharedInstance] allParticularCharInTextFieldWithString:string acceptableCharStr:ACCEPTABLE_IFSC_CODE_CHARACTERS];
        }
        else if (textField == cell.accountNumberTxtField)
        {
                return [[CommonSettings sharedInstance] allParticularCharInTextFieldWithString:string acceptableCharStr:ACCEPTABLE_BANK_NUMBER_CHARACTERS];
        }
        
        PickUpAddressTableViewCell *objPickUpAddressCell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        
        if(textField == objPickUpAddressCell.pincodeTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 6){
                        
                        return NO;
                }
                else{
                        return YES;
                }
        }
        
        return YES;
        
        //    }
        //    return YES;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
        CGPoint pointInTable = [textField.superview.superview convertPoint:textField.frame.origin toView:_tblView];
        CGPoint contentOffset = _tblView.contentOffset;
        contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
        
        NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
        [_tblView setContentOffset:contentOffset animated:YES];
        return YES;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
        [textField resignFirstResponder];
        if ([textField.superview.superview.superview isKindOfClass:[UITableViewCell class]])
        {
                CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                          toView: _tblView];
                NSIndexPath *indexPath = [_tblView indexPathForRowAtPoint:buttonPosition];
                
                [_tblView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        }
        else if ([textField.superview.superview.superview.superview isKindOfClass:[UITableViewCell class]])
        {
                CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                          toView: _tblView];
                NSIndexPath *indexPath = [_tblView indexPathForRowAtPoint:buttonPosition];
                
                [_tblView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
                
        }
        
        return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
        [textField resignFirstResponder];
        return YES;
}


#pragma mark - pincode value changed
-(void) pincodeValueChanged:(id)sender {
        // your code
        UITextField *pincodeTextField = (UITextField *)sender;
        if(pincodeTextField.text.length  == 6)
        {
                [self getCityFromPincode:pincodeTextField.text];
        }
        
}
-(void)getCityFromPincode:(NSString *)pincode
{
        
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                
                NSDictionary *userdata = [[NSDictionary alloc] initWithObjectsAndKeys:pincode,@"pincode", nil];
                callLoginService.delegate =self;
                [callLoginService operationRequestToApi:userdata url:GET_CITY_FROM_PINCODE string:@"GET_CITY_FROM_PINCODE"];
                
        }
}

#pragma mark - set custom picker
-(void)setPickerView:(int)tag{
        
        if(bgPickerView == nil)
        {
                bgPickerView =[[UIView alloc]init];
                bgPickerView.frame = CGRectMake(0, self.view.frame.size.height - 200
                                                , self.view.frame.size.width, 200);
                
                
                UIButton *doneButton = [[UIButton alloc]init];
                doneButton.frame =CGRectMake(0, 0, bgPickerView.frame.size.width, 30);
                [doneButton setTitle:@"Done" forState:UIControlStateNormal];
                doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                doneButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
                doneButton.backgroundColor =[UIColor lightGrayColor];
                [doneButton addTarget:self action:@selector(doneTouched:) forControlEvents:UIControlEventTouchUpInside];
                [bgPickerView addSubview:doneButton];
                //processorView.hidden = YES;
                [self.view addSubview:bgPickerView];
        }
        //     PickUpAddressTableViewCell *cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        pickerView = [[UIPickerView alloc]init];
        pickerView.frame = CGRectMake(0, 30, self.view.frame.size.width, 170);
        pickerView.dataSource = self;
        pickerView.delegate = self;
        pickerView.tag = tag;
        
        if(tag == 10)
        {
                pickerArray = [addressListArr valueForKey:@"complete_address"];
                [pickerView selectRow:pickerSelectedIndex inComponent:0 animated:YES];

        }
        else
        {
                pickerArray = bankListArr;
                [pickerView selectRow:bankListSelectedIndex inComponent:0 animated:YES];

        }
        
        pickerView.backgroundColor = [UIColor whiteColor];
        pickerView.showsSelectionIndicator = YES;
        [bgPickerView addSubview:pickerView];
        [pickerView reloadAllComponents];
}


- (void)doneTouched:(UIBarButtonItem *)sender{
        
       
        bgPickerView.hidden = YES;
        [bgPickerView removeFromSuperview];
        bgPickerView = nil;
        [_tblView setContentOffset:CGPointMake(0, 0)];
}
#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
        return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
        return [pickerArray count];
}

#pragma mark- Picker View Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
        
        if(pickerView.tag == 10)
        {
                pickerSelectedIndex = row;
                PickUpAddressTableViewCell *cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        
                 [cell.pickUpBtn setTitle:[pickerArray objectAtIndex:pickerSelectedIndex]  forState:UIControlStateNormal];
                

                [self.view layoutIfNeeded];
                CGFloat pickUPBtnHt = [[CommonSettings sharedInstance] calculateBtnHeight:cell.pickUpBtn];
                cell.pickUpBtnHeightConstraint.constant = pickUPBtnHt +20;
                cell.pickUpAddressViewHeightConstraint.constant = 124 + pickUPBtnHt;
                [self.view layoutIfNeeded];
        }
        else
        {
                bankListSelectedIndex = row;
                [_tblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                
        }
        
       // [_tblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
        return [pickerArray objectAtIndex:row];
}


#pragma mark - IBAction methods

- (IBAction)selectBankDetailsBtnClicked:(UIButton *)btn
{
        [self setPickerView:20];
}

- (IBAction)rightMenuBtnClicked:(id)sender {
        [CommonSettings displaySellYourGadgetRightMenu:self];
        
}

-(void)pickUpAddressBtnClicked:(UIButton *)btn
{
        CGPoint pointInTable = [btn.superview.superview convertPoint:btn.frame.origin toView:_tblView];
        CGPoint contentOffset = _tblView.contentOffset;
        
        contentOffset.y = (pointInTable.y - 200);
        
        NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
        [_tblView setContentOffset:contentOffset animated:YES];
        [self setPickerView:10];
}
- (IBAction)backBtnClicked:(id)sender {
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)doneButtonDidPressed:(id)sender
{
        [self.view endEditing:YES];
        //[_tblView setContentOffset:CGPointMake(0, 0)];
        [_tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
}
- (IBAction)chekOutBtnClicked:(id)sender {
        
        //    if([_productType isEqualToString:@"mobile"])
        //    {
        //    if (searialTxtField.text){
        //        if (searialTxtField.text.length < 15) {
        //            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 15 Digit IMEI Number"];
        //            return;
        //        }
        //    }
        //        else
        //        {
        //            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"IMEI Number field is required"];
        //            return;
        //
        //        }
        //    }
        //    else if ([_productType isEqualToString:@"laptop"])
        //    {
        //        if (![Validation required:searialTxtField withCaption:@"Serial"])
        //            return ;
        //    }
        
        if(bankListArr.count == 0)
       
        {
        
        
        BankInfoTableViewCell *objBankInfoCell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        if(objBankInfoCell)
        {
                if (![Validation required:objBankInfoCell.customerNameTxtField withCaption:@"Customer Name"])
                        return ;
                if (![Validation required:objBankInfoCell.bankNameTxtField withCaption:@"Bank Name"])
                        return ;
                
                if (![Validation required:objBankInfoCell.accountNumberTxtField withCaption:@"Account Number"])
                        return ;
                
                if (![Validation required:objBankInfoCell.ifscCodeTxtField withCaption:@"IFSC Code"])
                        return ;
                
        }
        }
        
        if(isAddressesAvailable)
        {
                addressId = [[addressListArr objectAtIndex:pickerSelectedIndex] valueForKey:@"customer_address_id"];
        }
        else
        {
                addressId = @"0";
                PickUpAddressTableViewCell *objPickAddressCell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                if(objPickAddressCell.addressLine1TextField.text.length == 0 && objPickAddressCell.addressLine2TextField.text.length == 0 &&objPickAddressCell.pincodeTextField.text.length == 0 && city.length == 0 &&state.length == 0 && country.length == 0)
                {
                        [[AppDelegate getAppDelegateObj] showAlert:@"" withMessage:@"Please add new address"];
                        return;
                }
                
                if (![Validation required:objPickAddressCell.addressLine1TextField withCaption:@"Address Line 1"])
                        return ;
                
                if (![Validation required:objPickAddressCell.addressLine2TextField withCaption:@"Address Line 2"])
                        return ;
                
                if (![Validation required:objPickAddressCell.pincodeTextField withCaption:@"Pincode"])
                        return ;
                
                if (city.length == 0 || state.length == 0 || country.length == 0 || [country isEqualToString:@"state_code"])
                {
                        [[AppDelegate getAppDelegateObj] showAlert:@"" withMessage:@"Please enter valid pincode"];
                        return ;
                }
        }
        
        
        if([acceptBtn isSelected])
                [self callChekoutWs];
        else
        {
                [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Accept all terms and condition"];
        }
}

- (IBAction)acceptTermsAndConditionBtnClicked:(id)sender {
        
    NSString *url = SYG_TNC_CONTENT; //@"http://electronicsbazaar.com/warranty-app/gadget-policy.html";
        NSString *title = @"Terms & Conditions";
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SGWebViewController *objVc = [storyBoard instantiateViewControllerWithIdentifier:@"SGWebViewController"];
        objVc.webViewUrl = url;
        objVc.titleLabel = title;
        [self.navigationController pushViewController:objVc animated:YES];
        
}
- (void)acceptBtnClicked:(UIButton *)sender {
        sender.selected = !sender.selected;
}
#pragma mark - call checkOut webservice
-(void)callChekoutWs
{
        [self.view endEditing:YES];
        Webservice  *callAddlistWs = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callAddlistWs.delegate =self;
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *customer_id;
                
                int userId;
                if([AppDelegate getAppDelegateObj].isFromLenovoLogin)
                {
                        userId = [AppDelegate getAppDelegateObj].lenovo_userId;
                        customer_id = [defaults valueForKey:LENOVO_CUSTOMER_ID];
                }
                else
                {
                        SellGadgetUser *objSellgadgetUser = [SellGadgetUser getSellGadgetUserFromNsuserdefaults];
                        userId = [objSellgadgetUser.userId intValue];
                        customer_id = objSellgadgetUser.customerId;
                        
                }
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                BankInfoTableViewCell *objBankInfoCell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                PickUpAddressTableViewCell *objPickUpAddresCell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                NSDictionary *bankInfoDic;
                
                if(objBankInfoCell && objBankInfoCell.customerNameTxtField.text.length != 0 && objBankInfoCell.bankNameTxtField.text.length != 0 &&objBankInfoCell.ifscCodeTxtField.text.length != 0 &&objBankInfoCell.accountNumberTxtField.text.length != 0)
                {
                        bankInfoDic = [NSDictionary dictionaryWithObjectsAndKeys:objBankInfoCell.customerNameTxtField.text,@"customer_name",objBankInfoCell.bankNameTxtField.text,@"bank_name",objBankInfoCell.ifscCodeTxtField.text,@"bank_ifsc",
                                       objBankInfoCell.accountNumberTxtField.text,@"bank_account_no",
                                       nil];
                }
                else
                {
                        NSArray *arr = [[bankListArr objectAtIndex:bankListSelectedIndex] componentsSeparatedByString:@","];
                        
                        NSString *customerName = [[[arr objectAtIndex:0] componentsSeparatedByString:@": "] objectAtIndex:1];
                        
                         NSString *bankName = [[[arr objectAtIndex:1] componentsSeparatedByString:@": "] objectAtIndex:1];
                        
                         NSString *bankIfsc = [[[arr objectAtIndex:2] componentsSeparatedByString:@": "] objectAtIndex:1];
                        
                         NSString *bankAccountNumber = [[[arr objectAtIndex:3] componentsSeparatedByString:@": "] objectAtIndex:1];
                        
                        
                        bankInfoDic = [NSDictionary dictionaryWithObjectsAndKeys:customerName,@"customer_name",
                                       bankName,@"bank_name",
                                       bankIfsc,@"bank_ifsc",
                                       bankAccountNumber,@"bank_account_no",
                                       nil];
                }
                
                
                
                NSError * err;
                NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:[NSArray arrayWithObject:bankInfoDic]  options:0 error:&err];
                NSString *bankDetailsStr= [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
                
                NSMutableDictionary *gadgetSpecificationDic;
                if([_productType isEqualToString:@"Mobile"])
                {
                          gadgetSpecificationDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[_mobileDataDic valueForKey:BRAND],BRAND,[_mobileDataDic valueForKey:BRAND_PRODUCT_ID],BRAND_PRODUCT_ID,[_mobileDataDic valueForKey:CONDITION],@"Condition",[_mobileDataDic valueForKey:IMEI_NO],IMEI_NO,[_mobileDataDic valueForKey:PRICE],PRICE,[_mobileDataDic valueForKey:CHARGER],@"Charger", nil];
                }
                else
                {
                          gadgetSpecificationDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[_mobileDataDic valueForKey:BRAND],BRAND,[_mobileDataDic valueForKey:BRAND_PRODUCT_ID],BRAND_PRODUCT_ID,[_mobileDataDic valueForKey:MEMORY],MEMORY,[_mobileDataDic valueForKey:IMEI_NO],IMEI_NO,[_mobileDataDic valueForKey:PRICE],PRICE,[_mobileDataDic valueForKey:CHARGER],@"Charger", nil];
                        
                        if([_mobileDataDic valueForKey:PROMOCODE])
                        {
                                [gadgetSpecificationDic setValue:[_mobileDataDic valueForKey:PROMOCODE] forKey:@"promo_code"];
                        }

                }
                
                if([_mobileDataDic valueForKey:CONDITION])
                        [gadgetSpecificationDic setValue:[_mobileDataDic valueForKey:CONDITION] forKey:@"Condition"];
              
                if([_mobileDataDic valueForKey:MODEL])
                        [gadgetSpecificationDic setValue:[_mobileDataDic valueForKey:MODEL] forKey:MODEL];
                if([_mobileDataDic valueForKey:GENERATION])
                        [gadgetSpecificationDic setValue:[_mobileDataDic valueForKey:GENERATION] forKey:GENERATION];
                if([_mobileDataDic valueForKey:PROCESSOR])
                        [gadgetSpecificationDic setValue:[_mobileDataDic valueForKey:PROCESSOR] forKey:@"Processor"];
           
                jsonData = [NSJSONSerialization  dataWithJSONObject:[NSArray arrayWithObject:gadgetSpecificationDic]  options:0 error:&err];
                NSString *gadgetSpecificationStr= [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
                
                NSString *parameterStr;
              
                if([addressId isEqualToString:@"0"] || (objPickUpAddresCell.addressLine1TextField.text.length != 0 && objPickUpAddresCell.pincodeTextField.text.length != 0 && city.length != 0 &&state.length != 0 && country.length != 0))
                {
                        parameterStr = [NSString stringWithFormat:@"bank_details=%@&gadget_specs=%@&customer_id=%@&postcode=%@&city=%@&country_id=%@&region_id=%@&street=%@&street1=%@",bankDetailsStr,gadgetSpecificationStr,customer_id,objPickUpAddresCell.pincodeTextField.text,city,country,state,objPickUpAddresCell.addressLine1TextField.text,objPickUpAddresCell.addressLine2TextField.text];
                        
                }
                else
                {
                        parameterStr = [NSString stringWithFormat:@"bank_details=%@&gadget_specs=%@&customer_id=%@&address_id=%@",bankDetailsStr,gadgetSpecificationStr,customer_id,addressId];
                        
                        
                }
                NSLog(@"parameterStr %@",parameterStr);
                if([CommonSettings checkCorporateUser])
                {
                        parameterStr = [NSString stringWithFormat:@"%@&corp_price=%@&internal_order_id=%@",parameterStr,[_mobileDataDic valueForKey:CORP_PRICE]?:@"",[_mobileDataDic valueForKey:INTERNAL_ORDER_ID]?:@""];
                }
                [callAddlistWs webServiceWitParameters:parameterStr andURL:SELL_GADGET_CHEKOUT_WS MathodName:SELL_GADGET_CHEKOUT_WS];
                
        }
}


//-(void)callChekoutWs
//{
//        Webservice  *callAddlistWs = [[Webservice alloc] init];
//        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
//        if(!chkInternet)
//        {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//                [alert show];
//        }
//        else
//        {
//                callAddlistWs.delegate =self;
//                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                NSString *customer_id;
//                
//                int userId;
//                if([AppDelegate getAppDelegateObj].isFromLenovoLogin)
//                {
//                        userId = [AppDelegate getAppDelegateObj].lenovo_userId;
//                        customer_id = [defaults valueForKey:LENOVO_CUSTOMER_ID];
//                }
//                else
//                {
//                        SellGadgetUser *objSellgadgetUser = [SellGadgetUser getSellGadgetUserFromNsuserdefaults];
//                        userId = [objSellgadgetUser.userId intValue];
//                        customer_id = objSellgadgetUser.customerId;
//                        
//                }
//                
//                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
//                
//                
//                
//                BankInfoTableViewCell *objBankInfoCell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//                
//                PickUpAddressTableViewCell *objPickUpAddresCell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
//                
//                
//                NSDictionary *bankInfoDic = [NSDictionary dictionaryWithObjectsAndKeys:objBankInfoCell.customerNameTxtField.text,@"customer_name",objBankInfoCell.bankNameTxtField.text,@"bank_name",objBankInfoCell.ifscCodeTxtField.text,@"bank_ifsc",
//                                             objBankInfoCell.accountNumberTxtField.text,@"bank_account_no",
//                                             nil];
//                
//                NSError * err;
//                
//                NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:[_productDetailDic valueForKey:@"option"] options:0 error:&err];
//                NSString *optionStr = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
//                
//                jsonData = [NSJSONSerialization  dataWithJSONObject:[NSArray arrayWithObject:bankInfoDic]  options:0 error:&err];
//                NSString *bankDetailsStr= [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
//                
//                NSString *parameterStr;
//                if([addressId isEqualToString:@"0"] || (objPickUpAddresCell.addressLine1TextField.text.length != 0 && objPickUpAddresCell.pincodeTextField.text.length != 0 && city.length != 0 &&state.length != 0 && country.length != 0))
//                {
//                        parameterStr = [NSString stringWithFormat:@"product_id=%@&price=%@&option=%@&name=%@&bank_details=%@&userid=%@&imei_no=%@&customer_id=%@&postcode=%@&city=%@&country_id=%@&region_id=%@&street=%@&street1=%@",[_productDetailDic valueForKey:@"product_id"],
//                                        [_productDetailDic valueForKey:@"price"],optionStr,[_productDetailDic valueForKey:@"name"],bankDetailsStr,[NSNumber numberWithInt:userId],searialTxtField.text,customer_id,objPickUpAddresCell.pincodeTextField.text,city,country,state,objPickUpAddresCell.addressLine1TextField.text,objPickUpAddresCell.addressLine2TextField.text];
//                }
//                else
//                {
//                        parameterStr = [NSString stringWithFormat:@"product_id=%@&price=%@&option=%@&name=%@&bank_details=%@&address_id=%@&userid=%@&imei_no=%@&customer_id=%@",[_productDetailDic valueForKey:@"product_id"],
//                                        [_productDetailDic valueForKey:@"price"],optionStr,[_productDetailDic valueForKey:@"name"],bankDetailsStr,addressId,[NSNumber numberWithInt:userId],searialTxtField.text,customer_id];
//                }
//                NSLog(@"parameterStr %@",parameterStr);
//                [callAddlistWs webServiceWitParameters:parameterStr andURL:SELL_GADGET_CHEKOUT_WS MathodName:SELL_GADGET_CHEKOUT_WS];
//        }
//}

@end
