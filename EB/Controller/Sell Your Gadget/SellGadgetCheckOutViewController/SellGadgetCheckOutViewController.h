//
//  SellGadgetCheckOutViewController.h
//  EB
//
//  Created by Neosoft on 2/24/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellGadgetCheckOutViewController : SellYourGadgetNavigationViewController


- (IBAction)chekOutBtnClicked:(id)sender;
@property (nonatomic,weak) IBOutlet UITableView *tblView;
@property (nonatomic,strong) NSDictionary *productDetailDic;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblBottomConstraint;
@property (nonatomic,strong) NSString *productType;;
- (IBAction)acceptTermsAndConditionBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;

@property (nonatomic,strong) NSDictionary *mobileDataDic;

@end
