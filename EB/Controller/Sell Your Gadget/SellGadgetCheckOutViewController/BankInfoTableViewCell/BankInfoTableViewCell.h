//
//  BankInfoTableViewCell.h
//  EB
//
//  Created by Neosoft on 2/24/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *customerNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *accountNumberTxtField;
@property (weak, nonatomic) IBOutlet UITextField *ifscCodeTxtField;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak,nonatomic) IBOutlet UILabel *bankInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectBankDetailsBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bankListViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectBankDetailBtnHtConstraint;



@property (weak, nonatomic) IBOutlet UITextField *bankNameTxtField;
@end
