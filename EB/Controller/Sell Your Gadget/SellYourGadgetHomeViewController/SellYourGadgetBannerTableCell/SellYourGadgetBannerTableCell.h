//
//  SellYourGadgetBannerTableCell.h
//  EB
//
//  Created by webwerks on 08/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SellYourGadgetBannerTableCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *objCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *objPageControl;
@property (nonatomic, strong) NSArray *bannerImagesArray;

@end
