//
//  SellYourGadgetBannerTableCell.m
//  EB
//
//  Created by webwerks on 08/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellYourGadgetBannerTableCell.h"
#import "SellGadgetBannerCollectionViewCell.h"
#define BANNER_IMAGE_HT_SCALE_FACTOR 1.76


@implementation SellYourGadgetBannerTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
       

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - collectionView delegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
        
        for (UICollectionViewCell *cell in [self.objCollectionView visibleCells]) {
                NSIndexPath *indexPath = [self.objCollectionView indexPathForCell:cell];
                //NSLog(@"visible index %ld",(long)indexPath.item);
                self.objPageControl.currentPage = indexPath.item;
                
        }
}
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
        return 1;
        
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
        
        return _bannerImagesArray.count;
        //return urlProductImageArray.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
        
        return CGSizeMake(kSCREEN_WIDTH, kSCREEN_WIDTH/BANNER_IMAGE_HT_SCALE_FACTOR);
        
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
        return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
        return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
        return UIEdgeInsetsMake(0, 0, 0,0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
        
        
        static NSString *identifier = @"SellGadgetBannerCollectionViewCell";
        SellGadgetBannerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        NSDictionary *dic = [_bannerImagesArray objectAtIndex:indexPath.item];
        
        if([CommonSettings checkCorporateUser])
                
                [cell.bannerImageView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"banner_image_corp"]]];
        else
                 [cell.bannerImageView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"banner_image"]]];
        return cell;
        
}

@end
