//
//  SellYourGadgetHomeViewController.h
//  EB
//
//  Created by webwerks on 08/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellYourGadgetHomeViewController : SellYourGadgetNavigationViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuTableViewHeightConstraint;

@end
