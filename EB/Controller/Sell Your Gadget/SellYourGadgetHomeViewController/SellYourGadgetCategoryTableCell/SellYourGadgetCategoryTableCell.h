//
//  SellYourGadgetCategoryTableCell.h
//  EB
//
//  Created by webwerks on 08/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellYourGadgetCategoryTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *workImageView;
@property (weak, nonatomic) IBOutlet UILabel *serviceAvailableLabel;
@property (weak, nonatomic) IBOutlet UITextField *pincodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIImageView *laptopcategoryImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laptopCategoryImgAspectRatio;
@property (weak, nonatomic) IBOutlet UIImageView *mobilecategoryImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mobileCategoryImgAspectRatio;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryImagesHtConstarint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laptopImageWidthConstraint;


@end
