//
//  SellYourGadgetHomeViewController.m
//  EB
//
//  Created by webwerks on 08/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellYourGadgetHomeViewController.h"
#import "SellYourGadgetCategoryTableCell.h"
#import "SellYourGadgetBannerTableCell.h"
#import "RightSlideMenuCell.h"
#import "MyPurchasesViewController.h"
#import "SellYourLaptopViewController.h"
#import "SellYourMobileViewController.h"

#define BANNER_IMAGE_HT_SCALE_FACTOR 1.76
#define GREEN_COLOR [UIColor colorWithRed:34/255.0f green:191/255.0f blue:100/255.0f alpha:1.0]

static  NSString *HISTORY = @"History";
static  NSString *SIGN_OUT = @"Sign out";

static NSString *LOGIN_SIGN_UP  = @"Login/Sign Up";

@interface SellYourGadgetHomeViewController ()<FinishLoadingData>
{
        UITextField *pincodeTextField;
        NSArray *menuArr;
        
        bool showCategory;
        bool isServiceAvailable;
        NSString *serviceAvailableMsg;
        CGPoint tableContentOffset;
        NSDictionary *homeResponseDic;
}

@end

@implementation SellYourGadgetHomeViewController

- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        [super setViewControllerTitle:@"Sell Your Gadget"];
        
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 300;
        
        _menuTableView.rowHeight = UITableViewAutomaticDimension;
        _menuTableView.estimatedRowHeight = 50;
        [self callSGHomeWebservice];
       
}


-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        [[APP_DELEGATE tabBarObj] unhideTabbar];

        
        
}
- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)createWrapView:(UITextField *)textfield{
        UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, textfield.frame.size.height)];
        textfield.leftViewMode = UITextFieldViewModeAlways;
        textfield.leftView = wrapView;
}

#pragma mark - textField delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        if(textField == pincodeTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 6){
                        
                        return NO;
                }
                else{
                        return YES;
                }
        }
        
        return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
        
        tableContentOffset = _tableView.contentOffset;
        CGPoint pointInTable = [textField.superview.superview convertPoint:textField.frame.origin toView:_tableView];
        CGPoint contentOffset = _tableView.contentOffset;
        contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
        
        NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
        [_tableView setContentOffset:contentOffset animated:YES];
        return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
        [textField resignFirstResponder];
        [_tableView setContentOffset:tableContentOffset];
        return YES;
}


#pragma mark - UITableViewDataSource Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        if(tableView == _menuTableView)
        {
                return menuArr.count;
        }
        else if(homeResponseDic != nil)
                return 2;
        else
                return 0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        
        if(tableView == _tableView)
        {
                if(indexPath.row == 0)
                        return (kSCREEN_WIDTH/BANNER_IMAGE_HT_SCALE_FACTOR);
                else
                        return UITableViewAutomaticDimension;
        }
        else
                return UITableViewAutomaticDimension;
}
//- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//        static NSString *CellIdentifier = @"SellYourGadgetBannerTableCell";
//        SellYourGadgetBannerTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//        if (cell == nil) {
//                cell = [[SellYourGadgetBannerTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//        }
//        return cell.contentView;
//}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//        return (kSCREEN_WIDTH/BANNER_IMAGE_HT_SCALE_FACTOR) + 20;
//;
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if(tableView == _tableView)
        {
                if(indexPath.row == 0)
                {
                        static NSString *CellIdentifier = @"SellYourGadgetBannerTableCell";
                        SellYourGadgetBannerTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                        if (cell == nil) {
                                cell = [[SellYourGadgetBannerTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                        }
                        
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;

                        
                        cell.bannerImagesArray = [homeResponseDic valueForKey:@"banners"];
                        if([[homeResponseDic valueForKey:@"banners"] count] > 1)
                                cell.objPageControl.numberOfPages = [[homeResponseDic valueForKey:@"banners"] count];
                        else
                                cell.objPageControl.numberOfPages =  0;
                       // [cell.objCollectionView reloadData];
                        return cell;
                }
                else
                {
                        static NSString *cellIdentifier=@"SellYourGadgetCategoryTableCell";
                        SellYourGadgetCategoryTableCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                        
                        if(pincodeTextField.text.length != 0)
                        {
                                cell.pincodeTextField.text = pincodeTextField.text;
                        }
                        pincodeTextField = cell.pincodeTextField;
                        
                         [self createWrapView: cell.pincodeTextField];
                        cell.pincodeTextField.delegate = self;
                        
                        if(isServiceAvailable)
                        {
                                cell.serviceAvailableLabel.textColor = GREEN_COLOR;
                        }
                        else
                        {
                                cell.serviceAvailableLabel.textColor = [UIColor redColor];
                        }
                        
                        if(showCategory)
                        {
                                [self.view layoutIfNeeded];
                                cell.categoryImagesHtConstarint.constant = ((kSCREEN_WIDTH-30)/2)/BANNER_IMAGE_HT_SCALE_FACTOR;
                                [self.view layoutIfNeeded];
                        }
                        else
                        {
                                cell.categoryImagesHtConstarint.constant = 0;
                        }
                        
                        
                        if([CommonSettings checkCorporateUser])
                        {
                                cell.mobilecategoryImageView.hidden = YES;
                                cell.laptopImageWidthConstraint.constant = kSCREEN_WIDTH - 20;
                                [cell.laptopcategoryImageView sd_setImageWithURL:[NSURL URLWithString:[homeResponseDic valueForKey:@"laptop_corp"]]];
                        }
                        else
                        {
                                cell.mobilecategoryImageView.hidden = NO;
                                cell.laptopImageWidthConstraint.constant = ((kSCREEN_WIDTH-30)/2);
                                [cell.laptopcategoryImageView sd_setImageWithURL:[NSURL URLWithString:[homeResponseDic valueForKey:@"laptop"]]];
                        }
                        
                        [cell.workImageView sd_setImageWithURL:[NSURL URLWithString:[homeResponseDic valueForKey:@"working"]]];
                        cell.serviceAvailableLabel.text = serviceAvailableMsg;
                        
                           UITapGestureRecognizer *tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(laptopCategoryImageClicked:)];
                                [cell.laptopcategoryImageView addGestureRecognizer:tap_recognizer];

                                tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mobileCategoryImageClicked:)];
                        
                                [cell.mobilecategoryImageView addGestureRecognizer:tap_recognizer];

                       
                        
                        [cell.mobilecategoryImageView sd_setImageWithURL:[NSURL URLWithString:[homeResponseDic valueForKey:@"mobile"]]];
                
                        pincodeTextField.delegate = self;
                        return cell;
                }
        }
        else
        {
                static NSString *CellIdentifier = @"RightSlideMenuCell";
                RightSlideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                        cell = [[RightSlideMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
                
                cell.menuTitleLbl.text = [menuArr objectAtIndex:indexPath.row];
                return cell;
        }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIStoryboard *ipadStoryboard=[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        if(tableView == _menuTableView)
        {
                NSString *selectedMenu = [menuArr objectAtIndex:indexPath.row];
                if([selectedMenu isEqualToString:HISTORY])
                {
                        MyPurchasesViewController *objMyOrder;
                        if(checkIsIpad)
                                objMyOrder=[ipadStoryboard instantiateViewControllerWithIdentifier:@"MyPurchasesViewController"];
                        else
                                objMyOrder=[storyboard instantiateViewControllerWithIdentifier:@"MyPurchasesViewController"];
                        objMyOrder.isSelectable=NO;
                        objMyOrder.isSellGadgetHistory = YES;
                        [self.navigationController pushViewController:objMyOrder animated:YES];
                }
                else if ([selectedMenu isEqualToString:SIGN_OUT])
                {
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:SELL_GADGET_LOGIN_USERID];
                        // [CommonSettings navigateToSellYourGadgetLoginView];
                }
                else if ([selectedMenu isEqualToString:LOGIN_SIGN_UP])
                {
                        
                        [CommonSettings navigateToSellYourGadgetLoginView];
                }
                
                _menuTableView.hidden = YES;
        }
}


#pragma mark - IBAction methods
- (IBAction)rightMenuBtnClicked:(id)sender {
        
         [CommonSettings displaySellYourGadgetRightMenu:self];
        
//        if([CommonSettings isUserLoggedInSellYourGadget])
//                menuArr = [NSArray arrayWithObjects:HISTORY,SIGN_OUT, nil];
//        else
//                menuArr = [NSArray arrayWithObjects:LOGIN_SIGN_UP, nil];
//        
//        if(_menuTableView.hidden == YES)
//        {
//                _menuTableView.hidden = NO;
//                [_menuTableView reloadData];
//                [self.view layoutIfNeeded];
//                _menuTableViewHeightConstraint.constant = _menuTableView.contentSize.height;
//                [self.view layoutIfNeeded];
//        }
//        else
//        {
//                _menuTableView.hidden = YES;
//        }
}
- (IBAction)backBtnClicked:(id)sender {
        
        //[self.navigationController popViewControllerAnimated:YES];
        [[APP_DELEGATE tabBarObj] TabClickedIndex:0];
}
- (IBAction)submitBtnClicked:(id)sender {
        //[self.view endEditing:YES];
        //[_tableView setContentOffset:tableContentOffset];

        
        if(pincodeTextField.text.length < 6)
        {
                [[CommonSettings sharedInstance] showAlertTitle:@"" message:@"Please enter valid pincode"];
        }
        else if (pincodeTextField.text.length == 6)
        {
                  [self callCheckPincodeWebservice];
        }
        
}

-(void)laptopCategoryImageClicked:(UITapGestureRecognizer*)sender
{
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SellYourLaptopViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SellYourLaptopViewController"];
        [self.navigationController pushViewController:vc animated:YES];
}

-(void)mobileCategoryImageClicked:(UITapGestureRecognizer*)sender
{
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SellYourMobileViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SellYourMobileViewController"];
        [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)pincodeValueChanged:(id)sender {
        
        UITextField *textField = (UITextField *)sender;
        if(textField.text.length == 6)
        {
                [self callCheckPincodeWebservice];
        }
}


#pragma mark - Utility methods
-(void)callSGHomeWebservice
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:SG_HOME_WS MathodName:SG_HOME_WS];
                
        }
}

-(void)callCheckPincodeWebservice
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                NSString *url = [NSString stringWithFormat:@"%@%@",SG_CHECK_PINCODE_WS,pincodeTextField.text];
                [callLoginService GetWebServiceWithURL:url MathodName:SG_CHECK_PINCODE_WS];
                
        }
}


#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if([strName isEqualToString:SG_HOME_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        homeResponseDic = [response valueForKey:@"data"];
                        [_tableView reloadData];
                }
                else{
                        
                }
        }
        else if ([strName isEqualToString:SG_CHECK_PINCODE_WS]){
                //NSLog(@"%@",response);
               
                serviceAvailableMsg = [response valueForKey:@"message"];
                showCategory = YES;
                
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        isServiceAvailable = YES;
                }
                else{
                        isServiceAvailable = NO;
                }
                [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
}
@end
