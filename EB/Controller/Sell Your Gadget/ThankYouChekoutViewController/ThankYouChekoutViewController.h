//
//  ThankYouChekoutViewController.h
//  EB
//
//  Created by Neosoft on 3/2/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThankYouChekoutViewController : SellYourGadgetNavigationViewController


- (IBAction)goToHomeBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (nonatomic, strong) NSString *orderId;
@end
