//
//  ThankYouChekoutViewController.m
//  EB
//
//  Created by Neosoft on 3/2/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "ThankYouChekoutViewController.h"
#import "SellYourGadgetHomeViewController.h"

@interface ThankYouChekoutViewController ()

@end

@implementation ThankYouChekoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _orderIdLabel.text = _orderId;
        [super setViewControllerTitle:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark _ IBOutlet methods
- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)rightMenuBtnClicked:(id)sender {
        [CommonSettings displaySellYourGadgetRightMenu:self];

}

- (IBAction)goToHomeBtnClicked:(id)sender {
    
    AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    [objAppdelegate.tabBarObj unhideTabbar];
        
//        NSArray *viewControllers = [self.navigationController viewControllers];
//        for (UIViewController *vc in viewControllers) {
//                if([vc isKindOfClass:[SellYourGadgetHomeViewController class]])
//                {
//                        [self.navigationController popToViewController:vc animated:YES];
//                        return;
//                }
//        }
        
        
        [CommonSettings navigateToSellYourGadgetView];
   // [objAppdelegate.tabBarObj TabClickedIndex:0];
}
@end
