//
//  SellYourLaptopViewController.m
//  EB
//
//  Created by webwerks on 08/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellYourLaptopViewController.h"
#import "SellGadgetUser.h"
#import "SGInfoWebViewController.h"
#import "SellYourGadgetLoginSignUpViewController.h"
#import "SGLaptopDetailsViewController.h"

#define GENERATION_KEY @"Generation"
#define PROCESSOR_KEY @"Processor"
#define MODEL_KEY @"Model"


@interface SellYourLaptopViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,FinishLoadingData>
{
        NSArray *pickerArray;
        UIPickerView *picker;
        UIView *pickerBackgroundView;
        
        NSArray *laptopBrandsArr;
        NSDictionary *responseDic;
        NSInteger basePrice;
        NSInteger promoCodePrice;
        
        NSArray *modelArr;
        NSArray *generationArr;
        
        NSArray *processorArr;
        NSArray *memoryArr;
        NSArray *conditionArr;
        NSArray *chargerArr;
        NSMutableDictionary *laptopDataDic;
}

@end

@implementation SellYourLaptopViewController

- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        [super setViewControllerTitle:@"Sell Your Laptop"];
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj hideTabbar];
        
        [_workingBtn setSelected:YES];
        [_yesBtn setSelected:YES];
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_submitBtn];
        
        if([CommonSettings checkCorporateUser])
        {
                [self.view layoutIfNeeded];
                _conditionViewHtConstraint.constant = 0;
                _promocodeViewHeightConstraint.constant = 0;
                [self.view layoutIfNeeded];
        }
        else
        {
                [self.view layoutIfNeeded];
                _conditionViewHtConstraint.constant = 60;
                _promocodeViewHeightConstraint.constant = 0;
                 // _promocodeViewHeightConstraint.constant = 0;
                [self.view layoutIfNeeded];
        }
        
        promoCodePrice = 0;
        [self getSellGadgetBrands];
        [CommonSettings addDropDownArrowImageInBtn:_brandBtn];
        [CommonSettings addDropDownArrowImageInBtn:_modelBtn];
        [CommonSettings addDropDownArrowImageInBtn:_generationBtn];
        [CommonSettings addDropDownArrowImageInBtn:_processorBtn];
        [CommonSettings addDropDownArrowImageInBtn:_ramBtn];
        
        
}

- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - get sell gadget category

-(void)getSellGadgetBrands
{
        // Loader
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:NO];
        
        // Call Home screen webservice
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:SELL_GADGET_LAPTOP_BRAND_WS MathodName:SELL_GADGET_LAPTOP_BRAND_WS];
        }
}


-(void)callGetModelWebservice:(NSString *)brandCode
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                
                NSString *url;
                if ([CommonSettings checkCorporateUser]) {
                        url = [NSString stringWithFormat:@"%@%@&is_corporate=1",SG_GET_LAPTOP_MODEL_WS,brandCode];
                }
                else
                {
                        url = [NSString stringWithFormat:@"%@%@",SG_GET_LAPTOP_MODEL_WS,brandCode];
                }
                [callLoginService GetWebServiceWithURL:url MathodName:SG_GET_LAPTOP_MODEL_WS];
        }
}


-(void)callGetConditionsWebservice:(NSString *)modelCode
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                NSString *url;
                if ([CommonSettings checkCorporateUser]) {
                        url = [NSString stringWithFormat:@"%@%@&is_corporate=1",SG_GET_LAPTOP_CONDITIONS_WS,modelCode];
                }
                else
                {
                        url = [NSString stringWithFormat:@"%@%@",SG_GET_LAPTOP_CONDITIONS_WS,modelCode];
                }
                [callLoginService GetWebServiceWithURL:url MathodName:SG_GET_LAPTOP_CONDITIONS_WS];
        }
}

-(void)callApplyPromocodeWebservice
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                NSString *url = [NSString stringWithFormat:@"%@%@&processor=%@",SG_PROMOCODE_WS,_promoCodeTextField.text,[_processorBtn titleForState:UIControlStateNormal]];
                [callLoginService GetWebServiceWithURL:url MathodName:SG_PROMOCODE_WS];
        }
}


#pragma mark - textField delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        if(textField == _serialNoTextField)
        {
                // NSUInteger newLength = [textField.text length] + [string length];
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_SERIAL_NUMBER_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                
                return [string isEqualToString:filtered];
                
                
        }
        else if (textField == _promoCodeTextField)
        {
//                NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
//                if (NSEqualRanges(range, textFieldRange) && [string length] == 0) {
//                        promoCodePrice = 0;
//                        _promocodeLabel.text = nil;
//                        _promocodeViewHeightConstraint.constant = 73;
//                        _submitPromoCodeBtn.enabled = TRUE;
//                        [self calculatePrice];
//                }
                const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
                int isBackSpace = strcmp(_char, "\b");
                
                if (isBackSpace == -8) {
                        NSLog(@"Backspace was pressed");
                        promoCodePrice = 0;
                        _promocodeLabel.text = nil;
                        _promocodeViewHeightConstraint.constant = 73;
                        _submitPromoCodeBtn.enabled = TRUE;
                        [self calculatePrice];
                        if ([laptopDataDic valueForKey:PROMOCODE]) {
                                [laptopDataDic removeObjectForKey:PROMOCODE];
                        }
                }
                
                return YES;
        }
        return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
        
        return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [textField resignFirstResponder];
        return YES;
}
#pragma mark - request finish method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if([strName isEqualToString:SELL_GADGET_LAPTOP_BRAND_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        laptopBrandsArr = [[response valueForKey:@"data"] valueForKey:@"brands"];
                        
                        [_brandBtn setTitle:[[laptopBrandsArr objectAtIndex:0] valueForKey:@"name"] forState:UIControlStateNormal];
                        
                        [self callGetModelWebservice:[[laptopBrandsArr objectAtIndex:0] valueForKey:@"code"]];
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        else if ([strName isEqualToString:SG_GET_LAPTOP_MODEL_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        NSDictionary *dic = [[response valueForKey:@"data"] valueForKey:@"products"];
                        if([[dic valueForKey:@"options"] isKindOfClass:[NSDictionary class]])
                        {
                                
                                [self displayAppleOptions:dic];
                                if ( [[_brandBtn titleForState:UIControlStateNormal] isEqualToString:@"Apple"] && ![CommonSettings checkCorporateUser]) {
                                        
                                        processorArr = nil;
                                        [self.view layoutIfNeeded];
                                        _processorViewHtConstraint.constant = 0;
                                        [self.view layoutIfNeeded];
                                        
                                }
                                else
                                {
                                        processorArr = [[dic valueForKey:@"options"] valueForKey:PROCESSOR_KEY];
                                        
                                        if(processorArr.count > 0)
                                        {
                                                [_processorBtn setTitle:[[processorArr objectAtIndex:0] valueForKey:@"option_name"] forState:UIControlStateNormal];
                                                
                                                promoCodePrice = 0;
                                                if (_promoCodeTextField.text.length != 0) {
                                                        [self submitPromoCodeBtnClicked:nil];
                                                }
                                        }
                                        else
                                                [_processorBtn setTitle:@"" forState:UIControlStateNormal];
                                        
                                        [self.view layoutIfNeeded];
                                        _processorViewHtConstraint.constant = 60;
                                        [self.view layoutIfNeeded];
                                }
                                
                                
                                
                                
                                if([[dic valueForKey:@"options"] valueForKey:GENERATION_KEY]  && [[_brandBtn titleForState:UIControlStateNormal] isEqualToString:@"Apple"])
                                {
                                        //                                        processorArr = nil;
                                        //                                        [self.view layoutIfNeeded];
                                        //                                        _processorViewHtConstraint.constant = 0;
                                        //                                        [self.view layoutIfNeeded];
                                        
                                        if(generationArr.count > 0)
                                                [_generationBtn setTitle:[[generationArr objectAtIndex:0] valueForKey:@"option_name"] forState:UIControlStateNormal];
                                        else
                                                [_generationBtn setTitle:@"" forState:UIControlStateNormal];
                                        NSString *generationStr = [_generationBtn titleForState:UIControlStateNormal];
                                        
                                        NSArray *filtered = [generationArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", generationStr]];
                                        if(filtered.count > 0)
                                        {
                                                NSDictionary *item = [filtered objectAtIndex:0];
                                                [self callGetConditionsWebservice:[item valueForKey:@"option_type_id"]];
                                        }
                                }
                                else
                                {
                                        
                                        generationArr = nil;
                                        modelArr = nil;
                                        
                                        NSString *modelStr = [_processorBtn titleForState:UIControlStateNormal];
                                        
                                        NSArray *filtered = [processorArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", modelStr]];
                                        if(filtered.count > 0)
                                        {
                                                NSDictionary *item = [filtered objectAtIndex:0];
                                                [self callGetConditionsWebservice:[item valueForKey:@"option_type_id"]];
                                        }
                                }
                        }
                        else
                        {
                                processorArr = nil;
                        }
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        else if ([strName isEqualToString:SG_GET_LAPTOP_CONDITIONS_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        responseDic = [[response valueForKey:@"data"] valueForKey:@"products"];
                        if([[responseDic valueForKey:@"options"] isKindOfClass:[NSDictionary class]])
                        {
                                memoryArr = [[responseDic valueForKey:@"options"] valueForKey:@"RAM"];
                                conditionArr = [[responseDic valueForKey:@"options"] valueForKey:@"Whatistheconditionoftheitem?"];
                                chargerArr = [[responseDic valueForKey:@"options"] valueForKey:CHARGER_KEY];
                                [self setInitialValues];
                        }
                        else
                        {
                                
                                memoryArr = nil;
                                conditionArr = nil;
                                chargerArr = nil;
                                [self setInitialValues];
                                
                        }
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        else if ([strName isEqualToString:SG_PROMOCODE_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        _promocodeLabel.text = [response valueForKey:@"message"];
                        _promocodeLabel.textColor = UIColor.greenTextColor;
                        
                        promoCodePrice = [[response valueForKey:@"price"] integerValue];
                        [self calculatePrice];
                        _submitPromoCodeBtn.enabled = FALSE;
//                        _totalPriceLabel.text = [NSString stringWithFormat:@"Get upto %@",[[CommonSettings sharedInstance] formatIntegerPrice:[[response valueForKey:@"new_price"] integerValue]]];
                        [laptopDataDic setValue:_promoCodeTextField.text forKey:PROMOCODE];
//                        [laptopDataDic setValue:[response valueForKey:@"new_price"] forKey:PRICE];
                        [laptopDataDic setValue:[NSString stringWithFormat:@"%ld",(long)basePrice] forKey:PRICE];
                       
                        
                }
                else{
                        //                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                        _promocodeLabel.text = [response valueForKey:@"message"];
                        _promocodeLabel.textColor = UIColor.redColor;
                        if ([laptopDataDic valueForKey:PROMOCODE]) {
                                [laptopDataDic removeObjectForKey:PROMOCODE];
                        }
                        promoCodePrice = 0;
                        [self calculatePrice];
                        _submitPromoCodeBtn.enabled = TRUE;

                        
                }
                
                [self.view layoutIfNeeded];
                _promocodeViewHeightConstraint.constant = 91;
                [self.view layoutIfNeeded];

                
        }
        
        
}
-(void)setInitialValues
{
        
        //        if(processorArr.count > 0)
        //                [_processorBtn setTitle:[[processorArr objectAtIndex:0] valueForKey:@"option_name"] forState:UIControlStateNormal];
        //        else
        //                [_processorBtn setTitle:@"" forState:UIControlStateNormal];
        
        if(memoryArr.count > 0)
                [_ramBtn setTitle:[[memoryArr objectAtIndex:0] valueForKey:@"option_name"] forState:UIControlStateNormal];
        else
                [_ramBtn setTitle:@"" forState:UIControlStateNormal];
        
        
        NSDictionary *dic = [laptopBrandsArr lastObject];
        if([[dic valueForKey:@"name"] isEqualToString:[_brandBtn titleForState:UIControlStateNormal]])
        {
                [self.view layoutIfNeeded];
                _otherBrandViewHtConstraint.constant = 65;
                [self.view layoutIfNeeded];
        }
        else
        {
                [self.view layoutIfNeeded];
                _otherBrandViewHtConstraint.constant = 0;
                [self.view layoutIfNeeded];
        }
        
        [self calculatePrice];
        
}

-(void)displayAppleOptions:(NSDictionary *)dic
{
        if([[dic valueForKey:@"options"] isKindOfClass:[NSDictionary class]])
        {
                if([[dic valueForKey:@"options"] valueForKey:GENERATION_KEY]  && [[_brandBtn titleForState:UIControlStateNormal] isEqualToString:@"Apple"])
                {
                        
                        generationArr = [[dic valueForKey:@"options"] valueForKey:GENERATION_KEY];
                        [self.view layoutIfNeeded];
                        _generationViewHtConstraint.constant = 60;
                        [self.view layoutIfNeeded];
                        if(generationArr.count > 0)
                                [_generationBtn setTitle:[[generationArr objectAtIndex:0] valueForKey:@"option_name"] forState:UIControlStateNormal];
                        else
                                [_generationBtn setTitle:@"" forState:UIControlStateNormal];
                }
                else
                {
                        [self.view layoutIfNeeded];
                        _generationViewHtConstraint.constant = 0;
                        [self.view layoutIfNeeded];
                        generationArr = nil;
                        [_generationBtn setTitle:@"" forState:UIControlStateNormal];
                }
                
                
                if([[dic valueForKey:@"options"] valueForKey:MODEL_KEY] != nil && [[_brandBtn titleForState:UIControlStateNormal] isEqualToString:@"Apple"])
                {
                        modelArr = [[dic valueForKey:@"options"] valueForKey:MODEL_KEY];
                        [self.view layoutIfNeeded];
                        _modelViewHtConstraint.constant = 60;
                        [self.view layoutIfNeeded];
                        if(modelArr.count > 0)
                                [_modelBtn setTitle:[[modelArr objectAtIndex:0] valueForKey:@"option_name"] forState:UIControlStateNormal];
                        else
                                [_modelBtn setTitle:@"" forState:UIControlStateNormal];
                        
                }
                else
                {
                        [self.view layoutIfNeeded];
                        _modelViewHtConstraint.constant = 0;
                        [self.view layoutIfNeeded];
                        modelArr = nil;
                        [_modelBtn setTitle:@"" forState:UIControlStateNormal];
                }
        }
        else
        {
                [self.view layoutIfNeeded];
                _modelViewHtConstraint.constant = 0;
                _generationViewHtConstraint.constant = 0;
                [self.view layoutIfNeeded];
        }
}



#pragma mark - calculate price
-(void)calculatePrice
{
        if ([CommonSettings checkCorporateUser]) {
                [self calculateCorporateUserPrice];
                return;
        }
        
        basePrice = 0;
        NSArray *filtered;
        NSDictionary *item;
        laptopDataDic = [NSMutableDictionary new];
        
                if([_yesBtn isSelected])
                {
                        if(chargerArr.count > 0 && [_workingBtn isSelected])
                        {
                                item = [chargerArr objectAtIndex:0];
                                basePrice += [[item valueForKey:@"option_price"] integerValue];
                        }
                        [laptopDataDic setValue:@"Yes" forKey:CHARGER];
                }else if([_noBtn isSelected] && [_workingBtn isSelected] )
                {
                        if(chargerArr.count > 0)
                        {
                                item = [chargerArr objectAtIndex:1];
                                basePrice += [[item valueForKey:@"option_price"] integerValue];
                        }
                        [laptopDataDic setValue:@"No" forKey:CHARGER];
                }
                
                
                if([_workingBtn isSelected])
                {
                        if(conditionArr.count > 0)
                        {
                                item = [conditionArr objectAtIndex:0];
                                
                                basePrice += [[item valueForKey:@"option_price"] integerValue];
                                
                        }
                        //[laptopDataDic setValue:[item valueForKey:@"option_type_id"] forKey:CONDITION_ID];
                        [laptopDataDic setValue:@"Working" forKey:CONDITION];
                }else  if([_nonWorkingBtn isSelected])
                {
                        //filtered = [conditionArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", @"Not Working"]];
                        
                        if(conditionArr.count > 0)
                        {
                                item = [conditionArr objectAtIndex:1];
                                
                                basePrice += [[item valueForKey:@"option_price"] integerValue];
                                
                        }
                        // [laptopDataDic setValue:[item valueForKey:@"option_type_id"] forKey:CONDITION_ID];
                        [laptopDataDic setValue:@"Not Working" forKey:CONDITION];
                }
       
        
        if(modelArr.count > 0)
        {
                NSString *modelStr = [_modelBtn titleForState:UIControlStateNormal];
                filtered = [modelArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", modelStr]];
                if(filtered.count > 0)
                {
                        item = [filtered objectAtIndex:0];
                        // [laptopDataDic setValue:[item valueForKey:@"option_type_id"] forKey:MODEL_ID];
                        basePrice += [[item valueForKey:@"option_price"] integerValue];
                }
                [laptopDataDic setValue:modelStr forKey:MODEL];
        }
        
        
        if(generationArr.count > 0)
        {
                NSString *generationStr = [_generationBtn titleForState:UIControlStateNormal];
                filtered = [generationArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", generationStr]];
                if(filtered.count > 0)
                {
                        item = [filtered objectAtIndex:0];
                        
                        basePrice += [[item valueForKey:@"option_price"] integerValue];
                }
                [laptopDataDic setValue:generationStr forKey:GENERATION];
        }
        
        NSString *memoryStr = [_ramBtn titleForState:UIControlStateNormal];
        filtered = [memoryArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", memoryStr]];
        if(filtered.count > 0)
        {
                item = [filtered objectAtIndex:0];
                //[laptopDataDic setValue:[item valueForKey:@"option_type_id"] forKey:MEMORY_ID];
                basePrice += [[item valueForKey:@"option_price"] integerValue];
        }
        [laptopDataDic setValue:memoryStr forKey:MEMORY];
        
        
        if( ![[_brandBtn titleForState:UIControlStateNormal] isEqualToString:@"Apple"])
        {
                NSString *processorStr = [_processorBtn titleForState:UIControlStateNormal];
                filtered = [processorArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", processorStr]];
                if(filtered.count > 0)
                {
                        item = [filtered objectAtIndex:0];
                        //[laptopDataDic setValue:[item valueForKey:@"option_type_id"] forKey:MEMORY_ID];
                        
                        basePrice += [[item valueForKey:@"option_price"] integerValue];
                }
                [laptopDataDic setValue:processorStr forKey:PROCESSOR];
        }
        
         if([_workingBtn isSelected])
         {
             basePrice = basePrice + promoCodePrice;
         }
        _totalPriceLabel.text = [NSString stringWithFormat:@"Get upto %@",[[CommonSettings sharedInstance] formatIntegerPrice:basePrice]];
        [laptopDataDic setValue:[NSString stringWithFormat:@"%ld",(long)basePrice] forKey:PRICE];
        [laptopDataDic setValue:[_brandBtn titleForState:UIControlStateNormal] forKey:BRAND];
        
        
        if (promoCodePrice != 0 && [_workingBtn isSelected]) {
                 [laptopDataDic setValue:_promoCodeTextField.text forKey:PROMOCODE];
        }
        else if ([_nonWorkingBtn isSelected])
        {
               if([laptopDataDic valueForKey:PROMOCODE])
               {
                       [laptopDataDic removeObjectForKey:PROMOCODE];
               }
        }
        
}

-(void)calculateCorporateUserPrice
{
        //product_id
        basePrice = 0;
        NSArray *filtered;
        NSDictionary *item;
        laptopDataDic = [NSMutableDictionary new];
     
                
                if([_yesBtn isSelected])
                {
                        [laptopDataDic setValue:@"Yes" forKey:CHARGER];
                }else if([_noBtn isSelected] && [_workingBtn isSelected] )
                {
                        [laptopDataDic setValue:@"No" forKey:CHARGER];
                }
                
                
        
        
        
        
        if(modelArr.count > 0)
        {
                NSString *modelStr = [_modelBtn titleForState:UIControlStateNormal];
                [laptopDataDic setValue:modelStr forKey:MODEL];
        }
        
        
        if(generationArr.count > 0)
        {
                NSString *generationStr = [_generationBtn titleForState:UIControlStateNormal];
                [laptopDataDic setValue:generationStr forKey:GENERATION];
        }
        
        NSString *memoryStr = [_ramBtn titleForState:UIControlStateNormal];
        [laptopDataDic setValue:memoryStr forKey:MEMORY];
        
      
                NSString *processorStr = [_processorBtn titleForState:UIControlStateNormal];
                filtered = [processorArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", processorStr]];
                if(filtered.count > 0)
                {
                        item = [filtered objectAtIndex:0];
                        //[laptopDataDic setValue:[item valueForKey:@"option_type_id"] forKey:MEMORY_ID];
                        
                        basePrice += [[item valueForKey:@"option_price"] integerValue];
                }
        
        [laptopDataDic setValue:processorStr forKey:PROCESSOR];
        _totalPriceLabel.text = [NSString stringWithFormat:@"Get upto %@",[[CommonSettings sharedInstance] formatIntegerPrice:basePrice]];
        [laptopDataDic setValue:[NSString stringWithFormat:@"%ld",(long)basePrice] forKey:PRICE];
        [laptopDataDic setValue:[_brandBtn titleForState:UIControlStateNormal] forKey:BRAND];
        
}
#pragma mark - IBACtion methods

- (IBAction)submitPromoCodeBtnClicked:(id)sender {
        
        if([_nonWorkingBtn isSelected])
                return;
        
        if (_promoCodeTextField.text.length == 0) {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter promo code"];
                return;
        }
        
        [self callApplyPromocodeWebservice];
}
- (IBAction)serialInfoBtnClicked:(id)sender {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SGInfoWebViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"SGInfoWebViewController"];
        
    viewController.webViewUrl = SYG_SERIAL_INFO_CONTENT; //@"http://electronicsbazaar.com/warranty-app/popup-laptop.html";
        [self addChildViewController:viewController];
        viewController.view.frame = self.view.frame;
        [self.view addSubview:viewController.view];
        viewController.view.alpha = 0;
        [viewController didMoveToParentViewController:self];
        
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^
         {
                 viewController.view.alpha = 1;
         }
                         completion:nil];
}


- (IBAction)backBtnClicked:(id)sender {
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj unhideTabbar];
        [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)rightMenuBtnClicked:(id)sender {
        [CommonSettings displaySellYourGadgetRightMenu:self];
}
- (IBAction)brandBtnClicked:(UIButton *)sender {
        _serialNoTextField.text = nil;
        pickerArray = [laptopBrandsArr valueForKey:@"name"];
        [self setPickerView:(int)sender.tag btnTitle:[sender titleForState:UIControlStateNormal]];
}
- (IBAction)modelBtnClicked:(UIButton *)sender {
        pickerArray = [modelArr valueForKey:@"option_name"];
        [self setPickerView:(int)sender.tag btnTitle:[sender titleForState:UIControlStateNormal]];
        
}
- (IBAction)generationBtnClicked:(UIButton *)sender {
        pickerArray = [generationArr valueForKey:@"option_name"];
        [self setPickerView:(int)sender.tag btnTitle:[sender titleForState:UIControlStateNormal]];
}
- (IBAction)processorBtnClicked:(id)sender {
        UIButton *btn = (UIButton *)sender;
        pickerArray = [processorArr valueForKey:@"option_name"];
        [self setPickerView:(int)btn.tag btnTitle:[sender titleForState:UIControlStateNormal]];
}
- (IBAction)ramBtnClicked:(id)sender {
        UIButton *btn = (UIButton *)sender;
        pickerArray = [memoryArr valueForKey:@"option_name"];
        [self setPickerView:(int)btn.tag btnTitle:[sender titleForState:UIControlStateNormal]];
}
- (IBAction)workingBtnClicked:(id)sender {
        [_workingBtn setSelected:YES];
        [_nonWorkingBtn setSelected:NO];
        [self calculatePrice];
}
- (IBAction)nonWorkingBtnClicked:(id)sender {
        [_workingBtn setSelected:NO];
        [_nonWorkingBtn setSelected:YES];
        [self calculatePrice];
}

- (IBAction)yesBtnClicked:(id)sender {
        [_yesBtn setSelected:YES];
        [_noBtn setSelected:NO];
        [self calculatePrice];
}
- (IBAction)noBtnClicked:(id)sender {
        [_yesBtn setSelected:NO];
        [_noBtn setSelected:YES];
        [self calculatePrice];
}
- (IBAction)submitBtnClicked:(id)sender {
        
        [self.view endEditing:YES];
        NSDictionary *dic = [laptopBrandsArr lastObject];
        if([[dic valueForKey:@"name"] isEqualToString:[_brandBtn titleForState:UIControlStateNormal]])
        {
                if(_otherBrandTextField.text.length == 0)
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter brand name"];
                        return;
                }
                
                [laptopDataDic setValue:_otherBrandTextField.text forKey:BRAND];
        }
        
        
        if(_serialNoTextField.text.length == 0)
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter valid serial number"];
                return;
        }
        [laptopDataDic setValue:_serialNoTextField.text forKey:IMEI_NO];
        
        [laptopDataDic setValue:[responseDic valueForKey:@"product_id"] forKey:BRAND_PRODUCT_ID];
        
        
        NSLog(@"laptop dic %@",laptopDataDic);
        if([CommonSettings isUserLoggedInSellYourGadget])
        {
                UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Product" bundle:nil];
                SGLaptopDetailsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SGLaptopDetailsViewController"];
                vc.laptopDetailDic = laptopDataDic;
                [self.navigationController pushViewController:vc animated:YES];
                
        }
        else
        {
                SellYourGadgetLoginSignUpViewController *objVc = [[SellYourGadgetLoginSignUpViewController alloc] initWithNibName:@"SellYourGadgetLoginSignUpViewController" bundle:nil];
                objVc.isFromSGLaptopDetail = YES;
                objVc.productDic = laptopDataDic;
                [self.navigationController pushViewController:objVc animated:YES];
        }
}


#pragma mark - picker custom methods
-(void)setPickerView:(int) tag btnTitle:(NSString *)selectedBtnTitle{
        
        if(pickerBackgroundView == nil)
        {
                pickerBackgroundView =[[UIView alloc]init];
                pickerBackgroundView.frame = CGRectMake(0, self.view.frame.size.height - 200
                                                        , self.view.frame.size.width, 200);
                
                
                UIButton *doneButton = [[UIButton alloc]init];
                doneButton.frame =CGRectMake(0, 0, pickerBackgroundView.frame.size.width, 30);
                [doneButton setTitle:@"Done" forState:UIControlStateNormal];
                doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                doneButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
                doneButton.backgroundColor =[UIColor lightGrayColor];
                [doneButton addTarget:self action:@selector(doneTouched:) forControlEvents:UIControlEventTouchUpInside];
                [pickerBackgroundView addSubview:doneButton];
                //processorView.hidden = YES;
                [self.view addSubview:pickerBackgroundView];
        }
        
        picker = [[UIPickerView alloc]init];
        picker.frame = CGRectMake(0, 30, self.view.frame.size.width, 170);
        picker.dataSource = self;
        picker.tag = tag;
        picker.delegate = self;
        
        //        NSUInteger index = [pickerArray indexOfObjectPassingTest:
        //                            ^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop)
        //                            {
        //                                    return [[dict objectForKey:@"option_name"] isEqual:selectedBtnTitle];
        //                            }
        //                            ];
        //        if(index != NSNotFound)
        //                [picker selectRow:index inComponent:0 animated:YES];
        
        NSUInteger index = [pickerArray indexOfObject:selectedBtnTitle];
        if(index != NSNotFound)
                [picker selectRow:index inComponent:0 animated:YES];
        
        
        picker.backgroundColor = [UIColor whiteColor];
        picker.showsSelectionIndicator = YES;
        [pickerBackgroundView addSubview:picker];
        [picker reloadAllComponents];
        //[processorPicker setHidden:YES];
}


- (void)doneTouched:(UIBarButtonItem *)sender{
        //[processorPicker setHidden:YES];
        pickerBackgroundView.hidden = YES;
        [pickerBackgroundView removeFromSuperview];
        pickerBackgroundView = nil;
}


#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
        return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
        return [pickerArray count];
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
        
        
        switch (pickerView.tag) {
                case 10:
                {
                        NSString *brandBtnTitleStr = [_brandBtn titleForState:UIControlStateNormal];
                        [_brandBtn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
                       
                        _submitPromoCodeBtn.enabled = TRUE;
                      
                        if([[_brandBtn titleForState:UIControlStateNormal] isEqualToString:@"Apple"])
                        {
                                 promoCodePrice = 0;
                                _promocodeViewHeightConstraint.constant = 0;
                                _promoCodeTextField.text = @"";
                                _promocodeLabel.text = @"";
                                if ([laptopDataDic valueForKey:PROMOCODE]) {
                                        [laptopDataDic removeObjectForKey:PROMOCODE];
                                }
                        }
                        else
                        {
                                if ([laptopDataDic valueForKey:PROMOCODE]) {
                                       _promocodeViewHeightConstraint.constant = 91;
                                }
                                else
                                {
                                        _promocodeViewHeightConstraint.constant = 73;
                                }
                               
                        }
                        if(![brandBtnTitleStr isEqualToString:[_brandBtn titleForState:UIControlStateNormal]])
                        {
                                NSArray *filtered = [laptopBrandsArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name == %@)", [pickerArray objectAtIndex:row]]];
                                NSDictionary *item = [filtered objectAtIndex:0];
                                [self callGetModelWebservice:[item valueForKey:@"code"]];
                        }
                }
                        break;
                case 20:
                {
                        
                        [_modelBtn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
                        [self calculatePrice];
                        
                }
                        break;
                case 30:
                {
                        NSString *generationStr = [_generationBtn titleForState:UIControlStateNormal];
                        
                        [_generationBtn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
                        
                        if([[_brandBtn titleForState:UIControlStateNormal] isEqualToString:@"Apple"] && ![[_generationBtn titleForState:UIControlStateNormal] isEqualToString:generationStr])
                        {
                                NSArray *filtered = [generationArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", [_generationBtn titleForState:UIControlStateNormal]]];
                                if(filtered.count > 0)
                                {
                                        NSDictionary *item = [filtered objectAtIndex:0];
                                        [self callGetConditionsWebservice:[item valueForKey:@"option_type_id"]];
                                }
                                
                        }
                        else
                        {
                                [self calculatePrice];
                        }
                }
                        break;
                case 40:
                {
                        NSString *processorBtnTitleStr = [_processorBtn titleForState:UIControlStateNormal];
                        [_processorBtn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
                        _submitPromoCodeBtn.enabled = TRUE;

                        if(![[_brandBtn titleForState:UIControlStateNormal] isEqualToString:@"Apple"])
                        {
                                promoCodePrice = 0;
                                 if (_promoCodeTextField.text.length != 0) {
                                         [self submitPromoCodeBtnClicked:nil];
                                 }
                                if(![processorBtnTitleStr isEqualToString:[_processorBtn titleForState:UIControlStateNormal]])
                                {
                                        NSArray *filtered = [processorArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(option_name == %@)", [_processorBtn titleForState:UIControlStateNormal]]];
                                        if(filtered.count != 0)
                                        {
                                                NSDictionary *item = [filtered objectAtIndex:0];
                                                [self callGetConditionsWebservice:[item valueForKey:@"option_type_id"]];
                                        }
                                }
                        }
                        else
                        {
                                promoCodePrice = 0;
                                [self calculatePrice];
                        }
                        
                }
                        break;
                case 50:
                {
                        [_ramBtn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
                        [self calculatePrice];
                }
                        break;
                        
                default:
                        break;
        }
        
        //        if(pickerView.tag == 10)
        //                [_processorBtn setTitle:[[pickerArray objectAtIndex:row]valueForKey:@"option_name"] forState:UIControlStateNormal];
        //        else if(pickerView.tag == 11)
        //                [_ramBtn setTitle:[[pickerArray objectAtIndex:row]valueForKey:@"option_name"] forState:UIControlStateNormal];
        //        else if(pickerView.tag == 12)
        //                [_storageBtn setTitle:[[pickerArray objectAtIndex:row]valueForKey:@"option_name"] forState:UIControlStateNormal];
        //        [self setProductPrice];
        
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
        //        return [[pickerArray objectAtIndex:row] valueForKey:@"option_name"];
        return [pickerArray objectAtIndex:row];
}

@end
