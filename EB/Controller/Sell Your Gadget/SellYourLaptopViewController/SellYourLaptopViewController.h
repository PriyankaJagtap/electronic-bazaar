//
//  SellYourLaptopViewController.h
//  EB
//
//  Created by webwerks on 08/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellYourLaptopViewController : SellYourGadgetNavigationViewController
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UITextField *serialNoTextField;
@property (weak, nonatomic) IBOutlet UIButton *workingBtn;
@property (weak, nonatomic) IBOutlet UIButton *nonWorkingBtn;
@property (weak, nonatomic) IBOutlet UIButton *yesBtn;
@property (weak, nonatomic) IBOutlet UIButton *noBtn;
@property (weak, nonatomic) IBOutlet UIButton *ramBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *generationViewHtConstraint;
@property (weak, nonatomic) IBOutlet UIButton *processorBtn;
@property (weak, nonatomic) IBOutlet UIButton *generationBtn;
@property (weak, nonatomic) IBOutlet UIButton *modelBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modelViewHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *otherBrandViewHtConstraint;
@property (weak, nonatomic) IBOutlet UIButton *brandBtn;
@property (weak, nonatomic) IBOutlet UITextField *otherBrandTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *processorViewHtConstraint;
@property (weak, nonatomic) IBOutlet UITextField *promoCodeTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *promocodeViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conditionViewHtConstraint;

@property (weak, nonatomic) IBOutlet UILabel *promocodeLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitPromoCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;


@end
