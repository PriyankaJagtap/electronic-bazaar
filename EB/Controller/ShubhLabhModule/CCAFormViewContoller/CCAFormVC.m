//
//  CCAFormVC.m
//  EB
//
//  Created by webwerks on 29/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "CCAFormVC.h"

@interface CCAFormVC ()

@end

@implementation CCAFormVC

#pragma mark - ViewController Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
    [self configUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Other Methods
-(void) initialize
{
    [super setViewControllerTitle:@"Shubh Labh"];
    self.buttonSolePropritorship.selected = YES;
    self.buttonCurrentAccount.selected = YES;
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://electronicsbazaar.com/mobile-app/shubhlabh-agreement.html"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    self.webView.delegate = self;
    [self.webView loadRequest:request ];
    
    dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    
    datePicker=[[UIDatePicker alloc]init];
    datePicker.datePickerMode=UIDatePickerModeDate;
    [self.dateTextField setInputView:datePicker];
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    
    [self.dateTextField setInputAccessoryView:toolBar];
}

-(void)ShowSelectedDate
{
    self.dateTextField.text=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:datePicker.date]];
    [self.dateTextField resignFirstResponder];
}

-(void) configUI
{
    self.businessInformationView.layer.borderWidth = 1;
    self.businessInformationView.layer.borderColor = [UIColor colorWithRed:207/255 green:209/255 blue:211/255 alpha:0.3].CGColor;
    
    self.bankInformationView.layer.borderWidth = 1;
    self.bankInformationView.layer.borderColor = [UIColor colorWithRed:207/255 green:209/255 blue:211/255 alpha:0.3].CGColor;
    
    self.requiredDocumentsView.layer.borderWidth = 1;
    self.requiredDocumentsView.layer.borderColor = [UIColor colorWithRed:207/255 green:209/255 blue:211/255 alpha:0.3].CGColor;
    
    self.businessTradeReferencesView.layer.borderWidth = 1;
    self.businessTradeReferencesView.layer.borderColor = [UIColor colorWithRed:207/255 green:209/255 blue:211/255 alpha:0.3].CGColor;
    
    self.agreemntView.layer.borderWidth = 1;
    self.agreemntView.layer.borderColor = [UIColor colorWithRed:207/255 green:209/255 blue:211/255 alpha:0.3].CGColor;
    
    self.signaturesView.layer.borderWidth = 1;
    self.signaturesView.layer.borderColor = [UIColor colorWithRed:207/255 green:209/255 blue:211/255 alpha:0.3].CGColor;
    
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.buttonSaveData];
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.buttonSubmit];
}
#pragma mark - ButtonActions
- (IBAction)buttonSoleProprietorshipAction:(id)sender {
    self.buttonSolePropritorship.selected = YES;
    self.buttonPartnership.selected = NO;
    self.buttonCorporation.selected = NO;
    self.buttonOther.selected = NO;
}
- (IBAction)buttonPartnershipAction:(id)sender {
    self.buttonSolePropritorship.selected = NO;
    self.buttonPartnership.selected = YES;
    self.buttonCorporation.selected = NO;
    self.buttonOther.selected = NO;
}
- (IBAction)buttonCorporationAction:(id)sender {
    self.buttonSolePropritorship.selected = NO;
    self.buttonPartnership.selected = NO;
    self.buttonCorporation.selected = YES;
    self.buttonOther.selected = NO;
}
- (IBAction)buttonOtherAction:(id)sender {
    self.buttonSolePropritorship.selected = NO;
    self.buttonPartnership.selected = NO;
    self.buttonCorporation.selected = NO;
    self.buttonOther.selected = YES;
}

//Bank Information
- (IBAction)buttonCurrentAccountAction:(id)sender {
    self.buttonCurrentAccount.selected = YES;
    self.buttonCashCreditAccount.selected = NO;
    self.buttonOtherAccount.selected = NO;
}
- (IBAction)buttonCashCreditAccountAction:(id)sender {
    self.buttonCurrentAccount.selected = NO;
    self.buttonCashCreditAccount.selected = YES;
    self.buttonOtherAccount.selected = NO;
}
- (IBAction)buttonOtherAccountAction:(id)sender {
    self.buttonCurrentAccount.selected = NO;
    self.buttonCashCreditAccount.selected = NO;
    self.buttonOtherAccount.selected = YES;
}

-(void)showActionSheet
{

}
//Required Documents
- (IBAction)buttonAadharCardAction:(id)sender {
    [self showActionSheet];
    
}
- (IBAction)buttonPanCardAction:(id)sender {
    
}
- (IBAction)buttonBankStatementAction:(id)sender {
    
}
- (IBAction)buttonPostChequeAction:(id)sender {
    
}

- (IBAction)buttonSubmitAction:(id)sender {
    
}
- (IBAction)buttonSaveDataAction:(id)sender {
}



#pragma mark - web view delegate methods
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [[CommonSettings sharedInstance] removeLoading];
    CGFloat contentHeight = webView.scrollView.contentSize.height;
    //self.webViewHeightConstraint.constant = contentHeight;
    self.agreementViewHeightConstraint.constant = contentHeight - 40;
    NSLog(@"finish");
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[CommonSettings sharedInstance] removeLoading];
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}
@end
