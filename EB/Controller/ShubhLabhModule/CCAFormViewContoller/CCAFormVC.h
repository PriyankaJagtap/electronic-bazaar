//
//  CCAFormVC.h
//  EB
//
//  Created by webwerks on 29/03/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextFieldWithBorder.h"

@interface CCAFormVC : BaseNavigationControllerWithBackBtn <UIWebViewDelegate>
{
    UIDatePicker *datePicker;
    NSDateFormatter *dateFormatter;
}
//Title
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

//Business Information
@property (weak, nonatomic) IBOutlet UIView *businessInformationView;

@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *companyNameTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *GSTNoTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *proprietorTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *billingAddressTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *BACityTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *BAStateTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *BAZipCodeTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *RegCompanyAddressTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *RACityTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *RAStateTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *RAZipCodeTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *emailTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *creditRequestedAmountTextField;

@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *dateTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *natureOfBusinessTextField;

@property (weak, nonatomic) IBOutlet UIButton *buttonSolePropritorship;
@property (weak, nonatomic) IBOutlet UIButton *buttonPartnership;
@property (weak, nonatomic) IBOutlet UIButton *buttonCorporation;
@property (weak, nonatomic) IBOutlet UIButton *buttonOther;

@property (weak, nonatomic) IBOutlet UIButton *buttonDateBusinessCommenced;

//Bank Information
@property (weak, nonatomic) IBOutlet UIView *bankInformationView;

@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *accountNameTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *accountNumberTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *bankNameTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *branchTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *email1TextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *IFSCCodeTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *phoneTextField;

@property (weak, nonatomic) IBOutlet UIButton *buttonCurrentAccount;
@property (weak, nonatomic) IBOutlet UIButton *buttonCashCreditAccount;
@property (weak, nonatomic) IBOutlet UIButton *buttonOtherAccount;

//RequiredDocuments
@property (weak, nonatomic) IBOutlet UIView *requiredDocumentsView;

@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *aadharCardTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *panCardTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *bankStatementTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *postChequeTextField;

@property (weak, nonatomic) IBOutlet UIButton *buttonAadharCard;
@property (weak, nonatomic) IBOutlet UIButton *buttonPanCard;
@property (weak, nonatomic) IBOutlet UIButton *buttonBankStatement;
@property (weak, nonatomic) IBOutlet UIButton *buttonPostCheque;


//BusinessTradeReferences

@property (weak, nonatomic) IBOutlet UIView *businessTradeReferencesView;

@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *companyNameTextFieldBT;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *addressTextField;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *natureOfBusinessTextFieldBT;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *phoneTextFieldBT;

@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *companyNameTf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *addressTf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *natureOfBusinessTf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *phoneTf;

//AgreemntView
@property (weak, nonatomic) IBOutlet UIView *agreemntView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *agreementViewHeightConstraint;

//Signatures

@property (weak, nonatomic) IBOutlet UIView *signaturesView;

@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *signature1Tf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *name1Tf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *date1Tf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *signature2Tf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *name2Tf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *date2Tf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *signature3Tf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *name3Tf;
@property (weak, nonatomic) IBOutlet CustomTextFieldWithBorder *date3Tf;

//ButtonView

@property (weak, nonatomic) IBOutlet UIButton *buttonSubmit;
@property (weak, nonatomic) IBOutlet UIButton *buttonSaveData;


@end
