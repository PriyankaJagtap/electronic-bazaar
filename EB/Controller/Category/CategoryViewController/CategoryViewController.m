//
//  CategoryViewController.m
//  EB
//
//  Created by Aishwarya Rai on 12/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "CategoryViewController.h"
#import "SlideMenuCell.h"
#import "MFSideMenuContainerViewController.h"
#import "ProductListViewController.h"
#import "MFSideMenu.h"
#import "MyTreeNode.h"
#import "MyTreeViewCell.h"
#import "ShowTabbars.h"
#import "MyOrdersViewController.h"
#import "RegistrationViewController.h"
#import "LoginViewController.h"
#import "MyPurchasesViewController.h"
#import "HomeDealsViewController.h"
#import "ContactUs.h"
#import "SellGadgetUser.h"
#import "WarrantyViewController.h"
#import "VerifyGSTINViewController.h"
#import "MyAccountViewController.h"


@interface CategoryViewController ()
{
    NSMutableArray *arrSlideMenu1;
    MyTreeNode *treenode;
}

@end

@implementation CategoryViewController
#pragma mark - ViewController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setViewControllerTitle:@"Categories"];
    [self CallMenuListService];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
    [[[AppDelegate getAppDelegateObj]tabBarObj] unhideTabbar];
    [[[AppDelegate getAppDelegateObj]tabBarObj] TabClickedIndex:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Webservice Methods
-(void)CallMenuListService
{
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:Nil url:MENU_LIST_WS string:@"MenuListWs"];
    }
}

-(void)receivedResponseForMenuList:(id)receiveData stringResponse:(NSString *)responseType
{
    if ([responseType isEqualToString:@"success"])
    {
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        arrSlideMenu1 = [NSMutableArray new];
        arrSlideMenu1 = [jsonObject valueForKey:@"menu_item"];
        if (arrSlideMenu1.count > 0)
        {
            [self drawTreeNodeView:arrSlideMenu1];
        }
    }
}

#pragma mark - TableView Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [treenode descendantCount] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    MyTreeNode *node = [[treenode flattenElements] objectAtIndex:indexPath.row];
    MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier level:[node levelDepth] expanded:node.inclusive];
    
    cell.lblItem.textColor = [UIColor grayTextColor];
    cell.lblItem.font = KarlaFontBold(16.0);
    
    cell.btnPlus.frame = CGRectMake(cell.frame.size.width + 40, 10, 12, 12);
    cell.btnPlus.contentMode = UIViewContentModeScaleAspectFit;
    
    cell.lblItem.text =[NSString stringWithFormat:@"%@",[node.menuDataDict objectForKey:@"name"]] ;
    cell.arrowImage.frame = CGRectMake(10, 10, 18, 18);
    cell.arrowImage.contentMode = UIViewContentModeScaleAspectFit;
    
    if([node.menuDataDict objectForKey:@"sub_cat"])
    {
        cell.btnPlus.hidden = NO;
    }
    else
    {
        cell.btnPlus.hidden = YES;
        cell.arrowImage.image = [UIImage imageNamed:@"blue_dot_icon"];
        cell.arrowImage.frame = CGRectMake(10, 10, 15, 15);
        cell.lblItem.font = karlaFontRegular(15.0);
    }
    
    if(node.inclusive)
    {
        [cell.btnPlus setBackgroundImage:[UIImage imageNamed:@"blue_up_arrow"] forState:UIControlStateNormal];
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        [cell.btnPlus setBackgroundImage:[UIImage imageNamed:@"blue_down_arrow"] forState:UIControlStateNormal];
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Laptops"])
    {
        cell.arrowImage.image = [UIImage imageNamed:@"blue_laptop_icon"];
    }
    else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Mobiles"])
    {
        cell.arrowImage.image = [UIImage imageNamed:@"blue_mobile_icon"];
    }
    else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Accessories"])
    {
        cell.arrowImage.image = [UIImage imageNamed:@"blue_accessories_icon"];
    }
    else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Tablets"])
    {
        cell.arrowImage.image = [UIImage imageNamed:@"blue_tablets_icon"];
    }
    else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Desktops"])
    {
        cell.arrowImage.image = [UIImage imageNamed:@"blue_desktop_icon"];
    }
    else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"EPP Program"])
    {
        cell.lblItem.font = KarlaFontBold(16.0);
        cell.arrowImage.image = [UIImage imageNamed:@"blue_laptop_icon"];
    }
    else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"E-Waste"])
    {
        cell.lblItem.font = KarlaFontBold(16.0);
        cell.arrowImage.image = [UIImage imageNamed:@"blue_ewaste_icon"];
        self.eWasteURL = [node.menuDataDict objectForKey:@"url"];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tblView deselectRowAtIndexPath:[self.tblView indexPathForSelectedRow] animated:YES];
    
    MyTreeNode *node = [[treenode flattenElements] objectAtIndex:indexPath.row];
    
    if (!node.hasChildren)
    {
        NSString *strTagVal = [NSString stringWithFormat:@"%@",[node.menuDataDict objectForKey:@"id"]];// sending cat_id for category screen
        NSString *strCatVal = [NSString stringWithFormat:@"%@",[node.menuDataDict objectForKey:@"name"]];// sending cat_id for category screen
        [self navigateToController:[strTagVal intValue] cat_val:strCatVal withIsIBMUser:[node.menuDataDict objectForKey:@"is_ibm"]];
        [self.slideMenuDelegate SlidingMethod:[strTagVal intValue] cat_name:strCatVal];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
        return;
    }
    node.inclusive = !node.inclusive;
    [treenode flattenElementsWithCacheRefresh:YES];
    [_tblView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}

#pragma mark - Other Methods
- (void)goToElectronicsVC:(NSString *)cat_id catval:(NSString *)catval
{
    ShowTabbars *tabBarController = self.menuContainerViewController.centerViewController;
    UINavigationController *navigationController1 = (UINavigationController *)tabBarController.selectedViewController;
    
    if ([catval isEqualToString:@"E-Waste"])
    {
        WarrantyViewController *objVc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
        objVc.webViewUrl = self.eWasteURL;
        objVc.titleLabel = @"E-Waste";
        objVc.bannerType = @"webview";
        [navigationController1 pushViewController:objVc animated:YES];
    }
    else
    {
        ElectronicsViewController *electronicviewObjc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
        electronicviewObjc.categoryVal = catval;
        electronicviewObjc.category_id = cat_id;
        [navigationController1 pushViewController:electronicviewObjc animated:YES];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
    }
}

-(void)navigateToController:(int)tag cat_val:(NSString *)catval withIsIBMUser:(NSString *) is_ibm
{
    NSString *cat_id = [NSString stringWithFormat:@"%d",tag];
    //check is_ibm
    if ([is_ibm isEqualToString:@"1"])
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"is_ibm"] isEqualToString:@"Yes"])
        {
            [self goToElectronicsVC:cat_id catval:catval];
        }
        else
        {
            [APP_DELEGATE navigateToIBMLogin:CATEGORY withCategoryValue:catval withCategoryId:cat_id];
        }
    }
    else
    {
        [self goToElectronicsVC:cat_id catval:catval];
    }
}

#pragma mark - drawTreeNodeView
-(void)drawTreeNodeView:(NSMutableArray *)treeviewArr
{
    NSString *titleNode = [NSString stringWithFormat:@""];
    treenode = [[MyTreeNode alloc] initWithValue:titleNode];
    
    // treenode = [[MyTreeNode alloc]init];
    //    MyTreeNode *node=[[MyTreeNode alloc]initWithValue:titleNode];
    //    node.value = @"Select Categories";
    //    [treenode addChild:node];
    //    node.inclusive=NO;
    
    for(int i=0;i<treeviewArr.count;i++)
    {
        NSString *titleNode = [NSString stringWithFormat:@"%@",[[treeviewArr objectAtIndex:i]objectForKey:@"name"]];
        MyTreeNode *node=[[MyTreeNode alloc]initWithValue:titleNode];
        node.value = [[treeviewArr objectAtIndex:i]objectForKey:@"name"];
        node.menuDataDict = [treeviewArr objectAtIndex:i];
        NSMutableArray *subMenuArr=[NSMutableArray new];
        subMenuArr = [[treeviewArr objectAtIndex:i]objectForKey:@"sub_cat"];
        
        if (subMenuArr.count > 0)
        {
            for(int j=0;j<subMenuArr.count;j++)
            {
                NSString *submenuChildtitle = [NSString stringWithFormat:@"%@",[[subMenuArr objectAtIndex:j] objectForKey:@"name"]];
                MyTreeNode *childnode=[[MyTreeNode alloc]initWithValue:submenuChildtitle];
                childnode.value = [NSString stringWithFormat:@"%@",[[subMenuArr objectAtIndex:j]objectForKey:@"name"]];
                childnode.menuDataDict = [subMenuArr objectAtIndex:j];
                [node addChild:childnode];
            }
        }
        [treenode addChild:node];
        node.inclusive=NO;
    }
    [_tblView reloadData];
}

@end
