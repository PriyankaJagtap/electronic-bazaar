//
//  CategoryViewController.h
//  EB
//
//  Created by Aishwarya Rai on 12/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "BaseNavigationController.h"
#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "Webservice.h"


@interface CategoryViewController : BaseNavigationController<FinishLoadingData,UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(weak,nonatomic) id slideMenuDelegate;
@property (weak, nonatomic) NSString *eWasteURL;

-(void)navigateToController:(int)tag cat_val:(NSString *)catval;
@end
