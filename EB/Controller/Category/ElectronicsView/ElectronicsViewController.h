//
//  ElectronicsViewController.h
//  EB
//
//  Created by webwerks on 8/4/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"
#import "SlideMenuViewController.h"
#import "FilterViewController.h"
#import "SortByViewController.h"
#import "BaseNavigationControllerWithBackBtn.h"
#import "LoginSignUpViewController.h"

@interface ElectronicsViewController : BaseNavigationControllerWithBackBtn <UITableViewDelegate,filter_product_delegate,UITableViewDataSource,FinishLoadingData,UIActionSheetDelegate,UIScrollViewDelegate,SortByDelegate,LoginsViewDelegate>

@property (strong,nonatomic)NSString *categoryVal;
@property (strong,nonatomic)NSString *category_id;
//@property (strong,nonatomic)NSString *page_no;
@property (strong,nonatomic)NSString *page_size;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLbl;
@property (weak, nonatomic) IBOutlet UIButton *rightSlideMenuBtn;
@property (weak, nonatomic) IBOutlet UIButton *filterBtn;
@property (weak, nonatomic) IBOutlet UIButton *sortBtn;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryImageAspectRatio;
- (IBAction)filterAction:(id)sender;
- (IBAction)sortAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImagView;
- (IBAction)backAction:(id)sender;
- (IBAction)rightSlideAction:(id)sender;


@property (nonatomic) BOOL isSlideBannerClickedFromHomeScreen;
@property (nonatomic, strong) NSString *filterDataStr;
@property(nonatomic) BOOL isFromIBMLoginViewController;
@end
