//
//  ElectronicsViewController.m
//  EB
//
//  Created by webwerks on 8/4/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "ElectronicsViewController.h"
#import "FilterViewController.h"
#import "AppDelegate.h"
#import "ProductListCell.h"
#import "UIWindow+YUBottomPoper.h"
#import "ProductDetailsViewController.h"
#import "CommonSettings.h"
#import "GAIFields.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "SortByViewController.h"

@implementation ElectronicsViewController
{
        NSMutableArray *arrCategoryList;
        NSMutableArray *arrSortList;
        NSMutableArray *arrFilterList;
        NSString *sortByVal;
        NSString *filterAttributeStr;
        NSMutableDictionary *AttributeDict;
        int page_no;
        int totalCount;
        UIButton *buttonFavourite;
        NSMutableString *filterParam;
        NSInteger *totalProduct;
        NSMutableDictionary *paramDict;
        NSMutableArray *optionArray;
        UIView *footerView;
        BOOL isLoading,hasNext;
}

@synthesize productTitleLbl,category_id;

- (void)viewDidLoad
{
        [super viewDidLoad];
        page_no = 1;
        _sortBtn.selected = NO ;
        _filterBtn.selected = NO;
        // [CommonSettings addBorderLayer:_bgView];
        
        NSString *titleString = @"";
        
        // sortByVal = @"popularity";
        if(![_categoryVal isKindOfClass:[NSNull class]])
                productTitleLbl.text =_categoryVal;
        if(_categoryVal)
                titleString = _categoryVal;
        [super setViewControllerTitle:titleString];
        //_titleLabel.text = titleString;
        
        _tblView.estimatedRowHeight = 100;
        _tblView.rowHeight = UITableViewAutomaticDimension;
        
        NSString *str=@"&sortby=popularity";
        if(_isSlideBannerClickedFromHomeScreen)
                filterParam=[[NSMutableString alloc]initWithString:_filterDataStr];
        else
                filterParam=[[NSMutableString alloc]initWithString:str];
        
        paramDict=[[NSMutableDictionary alloc]init];
        optionArray=[[NSMutableArray alloc]init];
        
        [self callCategoryProductWSWithParameter:filterParam]; // call Webservice
        [self initFooterView];
        hasNext = YES;
    if (self.isFromIBMLoginViewController)
    {
        [super setIsFromIBMLoginScreenValue:self.isFromIBMLoginViewController];
        self.isFromIBMLoginViewController = NO;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
        [super viewDidAppear:animated];
        dispatch_async(dispatch_get_main_queue(), ^{
                [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:4];
        });
}

#pragma mark - set table footer view
-(void)initFooterView
{
        footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, kSCREEN_WIDTH, 40.0)];
        
        UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        //    actInd.tag = 10;
        actInd.color = [UIColor darkGrayColor];
        actInd.frame = CGRectMake(150.0, 5.0, 20.0, 20.0);
        actInd.hidesWhenStopped = YES;
        
        UIImageView *rotatingImg = [[UIImageView alloc] initWithFrame:CGRectMake((kSCREEN_WIDTH/2)-15, 5, 30, 30)];
        rotatingImg.image = [UIImage imageNamed:@"rotate_1.png"];
        rotatingImg.tag = 10;
        [footerView addSubview:rotatingImg];
        rotatingImg = nil;
        //    [footerView addSubview:actInd];
        //    actInd = nil;
}

#pragma mark - scrollView delegate method
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
        BOOL endOfTable = (_tblView.contentOffset.y >= ((self.tblView.contentSize.height) - _tblView.frame.size.height));
        if(endOfTable && !isLoading && !scrollView.dragging && !scrollView.decelerating && hasNext)
        {
                isLoading = YES;
                self.tblView.tableFooterView = footerView;
                //        [(UIActivityIndicatorView *)[footerView viewWithTag:10] startAnimating];
                [CommonSettings rotateLayerInfinite:[(UIImageView *)[footerView viewWithTag:10] layer]];
                ;
                [self loadMoreProducts:page_no];
        }
}


-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                           value:@"Category"];
        [[GAI sharedInstance].defaultTracker
         send:[[GAIDictionaryBuilder createScreenView] build]];
        
        if (arrCategoryList.count != 0)
        {
                [self refreshView];
        }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[AppDelegate getAppDelegateObj] tabBarObj] unhideTabbar];
    });
}

- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [arrCategoryList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *CellIdentifier = @"ProductListCell";
        ProductListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
                cell = [[ProductListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // set corner radius to background view
        // [CommonSettings addBorderLayer:cell.bgView];
        //name
        cell.productNameLbl.text = [NSString stringWithFormat:@"%@",[[arrCategoryList objectAtIndex:indexPath.row]objectForKey:@"name"]];
        
        cell.favProdBtn.tag = indexPath.row;
        NSDictionary *dic = [arrCategoryList objectAtIndex:indexPath.row];
        if ([[dic valueForKey:@"is_favourite"] boolValue]) {
                [cell.favProdBtn setSelected:YES];
        }
        else
        {
                [cell.favProdBtn setSelected:NO];
        }
        if(![[dic valueForKey:@"new_launch"] isKindOfClass:[NSNull class]])
        {
                if([[dic valueForKey:@"new_launch"] intValue] == 1)
                {
                        cell.comingSoonView.hidden = NO;
                        cell.comingSoonViewHeightConstraint.constant = 29;
                        //cell.itemLeftLabel.text = @"";
                }
                else
                {
                        cell.comingSoonView.hidden = YES;
                        cell.comingSoonViewHeightConstraint.constant = 0;
                }
        }
        else
        {
                cell.comingSoonView.hidden = YES;
                cell.comingSoonViewHeightConstraint.constant = 0;
        }
        
        [cell.favProdBtn addTarget:self action:@selector(favouriteButtonPresses:) forControlEvents:UIControlEventTouchUpInside];
        buttonFavourite=cell.favProdBtn;
        
        cell.buyNowLbl.hidden = TRUE;
        //    cell.buyNowLbl.tag = indexPath.row;
        //    [cell.buyNowLbl addTarget:self action:@selector(buyNowBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        //
        // Price
        
        NSString *priceStr = [[CommonSettings sharedInstance] formatPrice:[[[arrCategoryList objectAtIndex:indexPath.row]objectForKey:@"price"]floatValue]];
        
        [cell configureCartButtons];
        
        //        if([CommonSettings isLoggedInUser])
        //        {
        
        if ([NSNull null]==[[arrCategoryList objectAtIndex:indexPath.row]objectForKey:@"special_price"])
        {
                cell.productPrevPriceLbl.text=@"";
                cell.productPriceLbl.text=priceStr;
        }
        else
        {
                NSString *strSpecialPrice= [[CommonSettings sharedInstance] formatPrice:[[[arrCategoryList objectAtIndex:indexPath.row]objectForKey:@"special_price"]floatValue]];
                cell.productPriceLbl.text=[NSString stringWithFormat:@"  %@",strSpecialPrice];
                cell.productPrevPriceLbl.text=priceStr;
        }
        //}
        //image
        NSString*imageLink = [NSString stringWithFormat:@"%@",[[arrCategoryList objectAtIndex:indexPath.row]objectForKey:@"image"]];
        [cell.productImg sd_setImageWithURL:[NSURL URLWithString:imageLink]];
        cell.productImg.contentMode = UIViewContentModeScaleAspectFit;
        
        //        int isFav=[[[arrCategoryList objectAtIndex:indexPath.row]valueForKey:@"is_favourite"]intValue];
        //        if (isFav) {
        //                [cell.favProdBtn setImage:[UIImage imageNamed:@"favorate_icon_selected"] forState:UIControlStateNormal];
        //        }else{
        //                [cell.favProdBtn setImage:[UIImage imageNamed:@"favorate_icon"] forState:UIControlStateNormal];
        //        }
        
        NSLog(@"image width %f", cell.productImg.frame.size.width);
        if([dic valueForKey:@"category_type_img"] && ![[dic valueForKey:@"category_type_img"] isEqualToString:@""])
        {
                [cell.categoryTypeImgView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"category_type_img"]]];
        }
        else
        {
                cell.categoryTypeImgView.image = nil;
        }
        
        int isProductinStock=[[[arrCategoryList objectAtIndex:indexPath.row]valueForKey:@"is_instock"]intValue];
        if (isProductinStock)
        {
                cell.outofProductImg.hidden=YES;
                NSInteger qty = [[[arrCategoryList objectAtIndex:indexPath.row] valueForKey:@"qty"] integerValue];
                NSInteger max_qty = [[[arrCategoryList objectAtIndex:indexPath.row] valueForKey:@"max_qty"] integerValue];
                
                if (qty > max_qty)
                {
                        qty = max_qty;
                }
                if( qty <= 10 && qty != 0)
                {
                        cell.itemLeftLabel.text = [NSString stringWithFormat:@"Hurry, Only %ld left!",qty];
                }
                else
                {
                        cell.itemLeftLabel.text = @"";
                }
        }
        else
        {
                cell.outofProductImg.hidden=NO;
                 cell.itemLeftLabel.text = @"";
        }
        return cell;
}

-(void)buyNowBtnClicked:(id)sender
{
        UIButton *btn = (UIButton *)sender;
        
        ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
        
        int prodId = [[[arrCategoryList objectAtIndex:btn.tag]objectForKey:@"product_id"]intValue];
        objProductDetailVC.productID = prodId;
        [self.navigationController pushViewController:objProductDetailVC animated:YES];
}

-(void)favouriteButtonPresses:(UIButton*)btnFav
{
        buttonFavourite = btnFav;
        if ([CommonSettings isLoggedInUser])
        {
                NSMutableDictionary *productDic = [[arrCategoryList objectAtIndex:btnFav.tag] mutableCopy];
                int productId=[[[arrCategoryList objectAtIndex:btnFav.tag]valueForKey:@"product_id"]intValue];
                if(![[productDic valueForKey:@"is_favourite"] boolValue])
                {
                        [self didselectFavouriteProduct:productId];
                }
                else
                {
                        [self removeProductFromFavourite:productId];
                }
        }
        else
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
        }
}

-(void)didselectFavouriteProduct:(int)productId
{
        NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
        Webservice  *addToWishlistService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                if (userId==0)
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
                }
                else
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        addToWishlistService.delegate =self;
                        [addToWishlistService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%@&productId=%d",ADDTOWISHLIST,userId,productId] MathodName:@"ADDTOWISHLIST"];
                }
        }
}

-(void)removeProductFromFavourite:(int)productId
{
        NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
        Webservice  *addToWishlistService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                if (userId==0)
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Login required!"];
                }
                else
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        addToWishlistService.delegate =self;
                        [addToWishlistService GetWebServiceWithURL:[NSString stringWithFormat:@"%@userid=%@&product_id=%d",REMOVE_FROM_WISHLIST_WS,userId,productId] MathodName:REMOVE_FROM_WISHLIST_WS];
                }
        }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        ProductDetailsViewController *objProductDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
        
        int prodId =[[[arrCategoryList objectAtIndex:indexPath.row]objectForKey:@"product_id"]intValue];
        objProductDetailVC.productID=prodId;
        [self.navigationController pushViewController:objProductDetailVC animated:YES];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    CGFloat rowHeight = 120.0;
//    return rowHeight;
//}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
        //    if (indexPath.row == [arrCategoryList count] - 1 ) {
        //        [self loadMoreProducts:page_no];
        //    }
}


-(void)loadMoreProducts:(int)page_no_val
{
        if(hasNext)
        {
                page_no = page_no_val;
                [self callCategoryProductWSWithParameter:filterParam];
        }
}

#pragma mark - IBAction Methods
- (IBAction)loginToViewPriceBtnClicked:(id)sender
{
        [APP_DELEGATE navigateToLogin:CATEGORY];
}

- (IBAction)backAction:(id)sender
{
        if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
           ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self])
        {
                [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
                ShowTabbars *tabBarController = self.menuContainerViewController.centerViewController;
                UINavigationController *navigationController1 = (UINavigationController* )tabBarController.selectedViewController;
                
                
                HomeViewController *homeObjc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
                NSArray *controllers = [NSArray arrayWithObject:homeObjc];
                navigationController1.viewControllers = controllers;
        }
}

- (IBAction)rightSlideAction:(id)sender
{
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

- (IBAction)filterAction:(id)sender
{
        _sortBtn.selected = NO ;
        _filterBtn.selected = YES;
        FilterViewController *filterObjc ;
        /*if(checkIsIpad)
         filterObjc = [[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"FilterViewController"];
         else*/
        
        filterObjc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FilterViewController"];
        
        filterObjc.delegate = self;
        filterObjc.arrFilter = arrFilterList;
        filterObjc.selectedOptionArr=[NSMutableArray new];
        filterObjc.selectedOptionArr=optionArray;
        filterObjc.finalFilterDict=paramDict;
        
        [self.navigationController pushViewController:filterObjc animated:YES];
}

-(void)getFilterParams:(NSMutableDictionary *)productParams andSelectedParam:(NSMutableArray *)param
{
        optionArray=[param mutableCopy];
        paramDict=[productParams mutableCopy];
        //NSLog(@"productParams %@",productParams);
        AttributeDict = [NSMutableDictionary new];
        
        // NSArray *arrCode = [NSArray arrayWithObject:[productParams allKeys]];
        //NSLog(@"arrCode %@",arrCode);
        
        filterParam=[[NSMutableString alloc]init];
        for (int i = 0; i < productParams.count; i++) {
                NSArray *arr = [NSArray arrayWithArray:[productParams objectForKey:[[[productParams allKeys] objectAtIndex:i]mutableCopy]]];
                NSString *str = [[[arr valueForKey:@"value"] componentsJoinedByString:@","]mutableCopy];
                
                // NSString *strParam=[NSString stringWithFormat:@"&%@=%@",[[productParams allKeys] objectAtIndex:i],str];
                
                NSString *strParam=[NSString stringWithFormat:@"&%@=%@",[[[productParams allKeys] objectAtIndex:i]mutableCopy],str];
                
                [filterParam appendString:[NSString stringWithFormat:@"%@",strParam]];
                // [AttributeDict setObject:str forKey:[[productParams allKeys] objectAtIndex:i]];
        }
        page_no = 1;
        [self callCategoryProductWSWithParameter:filterParam];
}

- (IBAction)sortAction:(id)sender
{
        //    filterParam=[[NSMutableString alloc]init];
        //    if ([sender tag] == 2)
        //    {
        //        _sortBtn.selected = YES ;
        //        _filterBtn.selected = NO;
        //
        //        NSArray *arraySortVal = [NSArray arrayWithArray:[arrSortList valueForKey:@"name"]];
        //
        //        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles: nil];
        //
        //
        //        for (int i = 0; i < [arraySortVal count]; i++) {
        //            [actionSheet addButtonWithTitle: [[arrSortList objectAtIndex:i]valueForKey:@"name"]];
        //        }
        //
        //        [actionSheet showInView:self.view];
        //    }
        //
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SortByViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"SortByViewController"];
        viewController.sortDataArray = arrSortList;
        viewController.delegate = self;
        
        [self addChildViewController:viewController];
        viewController.view.frame = self.view.frame;
        [self.view addSubview:viewController.view];
        viewController.view.alpha = 0;
        [viewController didMoveToParentViewController:self];
        
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^
         {
                 viewController.view.alpha = 1;
         }
                         completion:nil];
}

#pragma mark - UIAtionsheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
        if ((buttonIndex-1) == -1)
        {
                return;
        }
        sortByVal = [NSString stringWithFormat:@"%@",[[arrSortList objectAtIndex:buttonIndex-1]objectForKey:@"value"]];
        NSString *strfilter=[NSString stringWithFormat:@"&sortby=%@",sortByVal];
        filterParam=[[NSMutableString alloc]initWithString:strfilter];
        page_no = 1;
        [self callCategoryProductWSWithParameter:filterParam];
}

#pragma mark - sortBy delegate method
-(void)sortProductByValue:(NSString *)value
{
        NSString *strfilter=[NSString stringWithFormat:@"&sortby=%@",value];
        filterParam=[[NSMutableString alloc]initWithString:strfilter];
        page_no = 1;
        [self callCategoryProductWSWithParameter:filterParam];
}

#pragma mark - login view delegate method
-(void)refreshView
{
        [_tblView reloadData] ;
}

#pragma mark - getCategoryProduct
-(void)callCategoryProductWSWithParameter:(NSString *)strparam
{
        if(page_no == 1)
                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                //NSString *strRequestUrl=[NSString stringWithFormat:@"%@category_id=%@&page_no=%@&page_size=%@&sortby=%@&user_id=%@,brands=%@",urlLink,category_id,page_no,page_size,sortType,user_id,brands];
                
                NSString *user_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]objectForKey:@"user"]objectForKey:@"user_id"]];
                if([[[NSUserDefaults standardUserDefaults]objectForKey:@"user"]objectForKey:@"user_id"] == nil)
                        user_id = @"";
                
                NSString *pageNoVal = [NSString stringWithFormat:@"%d",page_no];
                
                NSString *param=[NSString stringWithFormat:@"category_id=%@&page_no=%@&page_size=12&user_id=%@%@",category_id?category_id:@"",pageNoVal,user_id,filterParam];
                
                NSMutableDictionary *dictCategory = [NSMutableDictionary new];
                [dictCategory setObject:param forKey:@"param"];
                NSLog(@"dict_category : %@",dictCategory);
                callLoginService.delegate =self;
                [callLoginService operationRequestToApi:dictCategory url:CATEGORY_PROD_WS string:@"CategoryProductList"];
        }
}

-(void)receivedResponseForCategoryProdList:(id)receiveData stringResponse:(NSString *)responseType
{
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        if ([responseType isEqualToString:@"success"])
        {
                NSData *receiveLoginData = [NSData dataWithData:receiveData];
                id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
                DLog(@"Response of %@ : %@",responseType,jsonObject);
                
                NSString *status = [NSString stringWithFormat:@"%ld",(long)[[jsonObject valueForKey:@"status"]integerValue]];
                
                NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
                totalCount =[[dataDict valueForKey:@"count"]intValue];
            
                if ([status isEqualToString:@"1"])
                {
                    if ([[dataDict valueForKey:@"do_logout"] isEqualToString:@"No"])
                    {
                        if (page_no == 1)
                        {
                            arrCategoryList = [NSMutableArray new];
                            arrSortList = [NSMutableArray new];
                            arrFilterList = [NSMutableArray new];
                            
                            if([[dataDict valueForKey:@"banner"] isEqualToString:@"false"])
                            {
                                [_categoryImageAspectRatio setActive:NO];
                            }
                            else
                            {
                                [_categoryImagView sd_setImageWithURL:[NSURL URLWithString:[dataDict valueForKey:@"banner"]]];
                            }
                            
                            arrCategoryList = [[dataDict valueForKey:@"result"]mutableCopy];
                            
                            arrSortList = [[dataDict valueForKey:@"sort_attributes"] mutableCopy];
                            arrFilterList = [[dataDict valueForKey:@"filter_attributes"]mutableCopy];
                            [_tblView setContentOffset:CGPointZero animated:YES];
                        }
                        else
                        {
                            [arrCategoryList addObjectsFromArray:[dataDict valueForKey:@"result"]];
                            arrSortList = [NSMutableArray arrayWithArray:[dataDict valueForKey:@"sort_attributes"]];
                            arrFilterList = [NSMutableArray arrayWithArray:[dataDict valueForKey:@"filter_attributes"]];
                        }
                        
                        if ([[dataDict valueForKey:@"result"] count]==0 ||  [[dataDict valueForKey:@"result"] count] < 12)
                        {
                            hasNext = NO;
                            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 40)];
                            lbl.textAlignment = NSTextAlignmentCenter;
                            lbl.font = [UIFont systemFontOfSize:15];
                            lbl.textColor = [UIColor blackColor];
                            lbl.text = @"No More Data";
                            
                            page_no = page_no + 1;
                            isLoading = YES;
                            //               [(UIActivityIndicatorView *)[footerView viewWithTag:10] stopAnimating];
                            [[(UIImageView *)[footerView viewWithTag:10] layer] removeAllAnimations];
                            [(UIImageView *)[footerView viewWithTag:10] removeFromSuperview];
                            
                            [footerView addSubview:lbl];
                            [_tblView reloadData];
                        }
                        else
                        {
                            page_no = page_no + 1;
                            isLoading = NO;
                            //                [(UIActivityIndicatorView *)[footerView viewWithTag:10] stopAnimating];
                            [[(UIImageView *)[footerView viewWithTag:10] layer] removeAllAnimations];
                            
                            self.tblView.tableFooterView = nil;
                            [_tblView reloadData];
                        }
                    }
                    else
                    {
                        [self doLogout];
                    }
                }
                else
                {
                    [[CommonSettings sharedInstance]showAlertTitle:@"" message:[dataDict valueForKey:@"message"]];
                    if ([[dataDict valueForKey:@"do_logout"] isEqualToString:@"Yes"])
                    {
                        [self doLogout];
                    }
                }
        }
        DLog(@"Response of %@ : %@",responseType,jsonObject1);
}

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if([strName isEqualToString:@"ADDTOWISHLIST"] || [strName isEqualToString:REMOVE_FROM_WISHLIST_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        // [favouriteButton setImage:[UIImage imageNamed:@"favorate_icon_selected"] forState:UIControlStateNormal];
                        NSNumber *isfavBool;
                        if([strName isEqualToString:@"ADDTOWISHLIST"])
                                isfavBool = [NSNumber numberWithBool:YES];
                        else if([strName isEqualToString:REMOVE_FROM_WISHLIST_WS])
                                isfavBool = [NSNumber numberWithBool:NO];
                        
                        NSMutableDictionary *productDic = [[arrCategoryList objectAtIndex:buttonFavourite.tag] mutableCopy];
                        [productDic setValue:isfavBool forKey:@"is_favourite"];
                        [arrCategoryList replaceObjectAtIndex:buttonFavourite.tag withObject:productDic];
                        
                        [_tblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:buttonFavourite.tag inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
}

-(void) doLogout
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *pushID=[defaults valueForKey:@"DEVICE_TOKEN"];
    NSString *email = [[defaults valueForKey:@"user"] valueForKey:@"email"];
    NSString *password =[[defaults valueForKey:@"user"] valueForKey:@"password"];
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    
    //for sell your gadget
    //                        SellGadgetUser *objSellGadgetUser = [SellGadgetUser getSellGadgetUserFromNsuserdefaults];
    //                        NSString *sellYourGadgetLoginUserID = [defaults valueForKey:SELL_GADGET_LOGIN_USERID];
    
    NSNumber *isRetailer;
    if([defaults valueForKey:IS_RETAILER])
    {
        isRetailer = [defaults valueForKey:IS_RETAILER];
    }
    
    [defaults removePersistentDomainForName:appDomain];
    [defaults setObject:[NSNumber numberWithBool:YES] forKey:SHOW_GSTIN];
    [defaults setObject:pushID forKey:@"DEVICE_TOKEN"];
    [defaults setObject:email forKey:@"email"];
    [defaults setObject:password forKey:@"password"];
    if(isRetailer != nil)
    {
        [defaults setObject:isRetailer forKey:IS_RETAILER];
    }
    
    //for sell your gadget
    //                        [SellGadgetUser saveSellGadgetUserToNsuserdefaults:objSellGadgetUser];
    //                        [defaults setValue:sellYourGadgetLoginUserID forKey:SELL_GADGET_LOGIN_USERID];
    //                        [defaults synchronize];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [APP_DELEGATE setEBPinGenerateCount];
    //[appDelegate navigateToLoginViewController];
    
    [APP_DELEGATE navigateToIBMLogin:CATEGORY withCategoryValue:self.categoryVal withCategoryId: self.category_id];
}

#pragma mark - add no deals view
-(void)addNodealsAvailableView
{
        UIView *noDealsAvailableView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-200)];
        UILabel *emptyDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 200, 40)];
        emptyDataLabel.text = @"Products Not Available";
        emptyDataLabel.textAlignment=NSTextAlignmentCenter;
        emptyDataLabel.alpha = 0.5;
        emptyDataLabel.center = noDealsAvailableView.center;
        emptyDataLabel.textColor = [UIColor blackColor];
        emptyDataLabel.backgroundColor = [UIColor clearColor];
        emptyDataLabel.textAlignment = NSTextAlignmentCenter;
        [noDealsAvailableView setBackgroundColor:[UIColor whiteColor]];
        [noDealsAvailableView addSubview:emptyDataLabel];
        [self.view addSubview:noDealsAvailableView];
}
@end
