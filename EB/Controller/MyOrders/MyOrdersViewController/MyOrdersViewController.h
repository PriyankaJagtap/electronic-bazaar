//
//  MyOrdersViewController.h
//  EB
//
//  Created by webwerks on 8/24/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrdersViewController : BaseNavigationController<UITextFieldDelegate>
{
    NSMutableArray *myOrderResponse;
    __weak IBOutlet UIButton *dropDownSelectButton;

}
- (IBAction)rightSlideMenuPressed:(id)sender;
- (IBAction)leftSlideMenuPressed:(id)sender;
- (IBAction)trackMyOrderPressed:(id)sender;

@property(weak, nonatomic) IBOutlet UIButton *trackOrderBtn;
- (IBAction)dropDownSelect:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trackOrderViewHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderIdViewHtConstraint;
@property (weak, nonatomic) IBOutlet UITextField *orderIdTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailIdTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderViewTopConstraint;
@property (weak, nonatomic) IBOutlet UIButton *trackBtn;

@end
