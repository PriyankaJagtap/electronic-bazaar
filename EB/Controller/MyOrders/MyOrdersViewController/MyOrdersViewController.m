//
//  MyOrdersViewController.m
//  EB
//
//  Created by webwerks on 8/24/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "MyOrdersViewController.h"
#import "MyOrderBgCell.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "Webservice.h"
#import "Constant.h"
#import "FVCustomAlertView.h"
#import "MyOrdersDetails.h"
#import "NIDropDown.h"
#import "CommonSettings.h"



@interface MyOrdersViewController ()<FinishLoadingData,NIDropDownDelegate>
{
        NIDropDown *dropdownDate;
        int orderId;
        int incrementID;
        NSMutableArray *orderDataArr;
        NSMutableArray *orderIdDataArr;
}
@end

@implementation MyOrdersViewController


- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        // [self setUpNavigationBar];
        [super setViewControllerTitle:@"Track Order"];
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_trackOrderBtn];
           [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_trackBtn];
        [self getMyOrderListFromWs];
        myOrderResponse=[[NSMutableArray alloc]init];
        orderDataArr = [NSMutableArray new];
        orderIdDataArr = [NSMutableArray new];
        [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:3];
        
        [[CommonSettings sharedInstance] createWrapView:_orderIdTextField];
        [[CommonSettings sharedInstance] createWrapView:_emailIdTextField];
        [[CommonSettings sharedInstance] createWrapView:_mobileNumberTextField];
        
         _mobileNumberTextField.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        _orderIdTextField.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];

        
}


-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        [self.tabBarController.tabBar setHidden:YES];
        [[[AppDelegate getAppDelegateObj]tabBarObj] TabClickedIndex:3];
        [[[AppDelegate getAppDelegateObj]tabBarObj] unhideTabbar];
        if ([CommonSettings isLoggedInUser]) {
                [self.view layoutIfNeeded];
                _trackOrderViewHtConstraint.constant = 400;
                _orderIdViewHtConstraint.constant = 0;
                _orderViewTopConstraint.constant = 160;
                [self.view layoutIfNeeded];

        }
        else
        {
                [self.view layoutIfNeeded];
                _trackOrderViewHtConstraint.constant = 0;
                _orderIdViewHtConstraint.constant = 180;
                _orderViewTopConstraint.constant = 0;
                [self.view layoutIfNeeded];
        }
}
-(void)viewDidAppear:(BOOL)animated{
        [super viewDidAppear:YES];
        [CommonSettings sendScreenName:@"MyOrderView"];
    
    [[[AppDelegate getAppDelegateObj]tabBarObj] unhideTabbar];
        
}
- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

-(void)getMyOrderListFromWs
{
        Webservice  *callMyOrdersWS = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callMyOrdersWS.delegate =self;
                int userId=(int)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]integerValue];
                if (userId != 0)
                {
                        
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        [callMyOrdersWS webServiceWitParameters:[NSString stringWithFormat:@"userid=%d",userId] andURL:MY_ORDERS MathodName:@"MYORDERS"];
                }else{
                        
                }
        }
}

-(void)setUpNavigationBar
{
        [self.navigationController setNavigationBarHidden:NO];
        self.navigationController.navigationBar.translucent=normal;
        UILabel *titleLabel=[[UILabel alloc]init];
        titleLabel.text = @"Track Order";
        titleLabel.font =karlaFontRegular(18.0);
        titleLabel.frame = CGRectMake(0, 0, 100, 30);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor whiteColor];
        self.navigationItem.titleView = titleLabel;
        UIColor *bgColor=[UIColor colorWithRed:23/255.0f green:70/255.0f blue:135/255.0f alpha:1.0f];
        self.navigationController.navigationBar.barTintColor = bgColor;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        self.navigationController.navigationBar.backItem.title =@"";
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"dropdown_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemPressed)];
        
        [[self navigationItem] setRightBarButtonItem:rightItem];
}

-(void)rightBarButtonItemPressed
{
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

#pragma WebService delegate

-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if([strName isEqualToString:TRACK_ORDER_WITH_ORDERID])
        {
                if ([[response valueForKey:@"status"]intValue]==1) {
                        MyOrdersDetails *objMyOrdersDetail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyOrdersDetails"];
                        
                        objMyOrdersDetail.orderID=[_orderIdTextField.text intValue];
                        objMyOrdersDetail.incrementID = [_orderIdTextField.text intValue];
                        objMyOrdersDetail.isFromOrderView = YES;
                        objMyOrdersDetail.userID = [response valueForKey:@"userid"];
                        [self.navigationController pushViewController:objMyOrdersDetail animated:YES];
                }
                else
                {
                        [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
                }

        }
        else
        {
        if ([[response valueForKey:@"status"]intValue]==1) {
                orderIdDataArr=[[[response valueForKey:@"data"]valueForKey:@"recent_orders"]valueForKey:@"order_id"];
                myOrderResponse=[[[response valueForKey:@"data"]valueForKey:@"recent_orders"]valueForKey:@"increment_id"];
                
                NSArray *arr = [[response valueForKey:@"data"]valueForKey:@"recent_orders"];
                
                for (NSDictionary *dic in arr) {
                        //        NSString *str = [NSString stringWithFormat:@"%@\t\t\t%@",[dic valueForKey:@"order_id"], [dic valueForKey:@"created_at"]];
                        NSString *str = [NSString stringWithFormat:@"%@%@%@",[dic valueForKey:@"increment_id"],@"           ",[dic valueForKey:@"created_at"]];
                        [orderDataArr addObject:str];
                }
        }
        else
        {
                [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
        }
        
        }
}
- (IBAction)dropDownSelect:(id)sender{
        if (dropdownDate==nil) {
                if (myOrderResponse.count==0) {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"No Order Available."];
                }else{
                        CGFloat f= 250.0;
                        dropdownDate = [[NIDropDown alloc] showDropDown:sender withHeight:&f withData:orderDataArr withImages:Nil WithDirection:@"down"];
                        dropdownDate.tag = 11 ;
                        dropdownDate.delegate = self;
                }
        }
}

- (void) getindexValue: (NIDropDown *)sender withindex:(NSIndexPath*)index{
        incrementID =[[myOrderResponse objectAtIndex:index.row]intValue];
        orderId=[[orderIdDataArr objectAtIndex:index.row]intValue];
        [dropDownSelectButton setTitle:[NSString stringWithFormat:@"%d",incrementID] forState:UIControlStateNormal];
        [dropdownDate removeFromSuperview];
        dropdownDate=nil;
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender
{
        
}



#pragma mark UIButton Action

- (IBAction)rightSlideMenuPressed:(id)sender{
        [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}
- (IBAction)leftSlideMenuPressed:(id)sender{
        [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)trackMyOrderPressed:(id)sender{
        if ([dropDownSelectButton.titleLabel.text isEqualToString:@"Select order"]) {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please select order."];
        }else{
                
                MyOrdersDetails *objMyOrdersDetail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyOrdersDetails"];
                
                //        Old_MyOrdersDetails *objMyOrdersDetail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Old_MyOrdersDetails"];
                
                objMyOrdersDetail.orderID=orderId;
                objMyOrdersDetail.incrementID = incrementID;
                
                [self.navigationController pushViewController:objMyOrdersDetail animated:YES];
        }
}
- (IBAction)trackBtnClicked:(UIButton *)sender {
        
        [self.view endEditing:YES];
        
        if(_orderIdTextField.text.length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Order ID."];
                return;
        }

        
        
        if (_emailIdTextField.text.length == 0 && _mobileNumberTextField.text.length == 0) {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Email ID or Mobile No."];
                return;
        }
        
        
        if(_emailIdTextField.text.length != 0)
        {
                if (![CommonSettings validEmail:_emailIdTextField.text]) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter valid email"];
                        return;
                        
                }
        }
        
        
        if(_emailIdTextField.text.length == 0 && _mobileNumberTextField.text.length < 10)
        {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter valid Mobile No."];
                return;

        }
        
        [self callTrackOrderWebservice];
       
}


-(void)doneButtonDidPressed:(id)sender
{
        dispatch_async(dispatch_get_main_queue(), ^{
                [self.view endEditing:YES];
                
        });
}

#pragma mark

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
        [dropdownDate hideDropDown:dropDownSelectButton];
        dropdownDate=nil;
        
}



#pragma mark - custom methods


-(void)callTrackOrderWebservice
{
        Webservice  *callMyOrdersWS = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callMyOrdersWS.delegate =self;
               
                        
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        
                        NSString *url = [NSString stringWithFormat:@"%@email=%@&order_id=%@&mobile=%@",TRACK_ORDER_WITH_ORDERID,_emailIdTextField.text,_orderIdTextField.text,_mobileNumberTextField.text];
                        [callMyOrdersWS GetWebServiceWithURL:url MathodName:TRACK_ORDER_WITH_ORDERID];
                        
                
        }
}




#pragma mark - UItextFieldDelegate method

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [self.view endEditing:YES];
        return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
        
       if(textField == _mobileNumberTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 10){
                        
                        return NO;
                }
                else{
                        return YES;
                }
        }
        
        return YES;
}
@end
