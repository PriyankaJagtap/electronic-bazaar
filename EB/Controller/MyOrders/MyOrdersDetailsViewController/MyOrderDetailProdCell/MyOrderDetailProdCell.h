//
//  MyOrderDetailProdCell.h
//  EB
//
//  Created by webwerks on 9/24/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"



@interface MyOrderDetailProdCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *trackOrderBtn;
@property (weak, nonatomic) IBOutlet UILabel *product_Name;
@property (weak, nonatomic) IBOutlet UILabel *product_Price;
@property (weak, nonatomic) IBOutlet UIImageView *product_Image;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *imeiNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *separatorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *categoryTypeImgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bg_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bg_trail;

@end
