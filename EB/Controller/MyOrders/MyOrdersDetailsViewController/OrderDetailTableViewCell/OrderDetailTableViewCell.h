//
//  OrderDetailTableViewCell.h
//  EB
//
//  Created by Neosoft on 4/11/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIView *bgView;

@property (weak,nonatomic) IBOutlet UILabel *orderIdLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelOrderBtn;
@property (weak,nonatomic) IBOutlet UILabel *priceLabel;
@property (weak,nonatomic) IBOutlet UILabel *statusLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderCancelLabelBottomConstraint;
@property (weak, nonatomic) IBOutlet UILabel *orderCancelLabel;
@end
