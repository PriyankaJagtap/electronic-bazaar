//
//  OrderStatusWithDateTableCell.h
//  EB
//
//  Created by Neosoft on 4/13/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderStatusWithDateTableCell : UITableViewCell
@property (weak,nonatomic) IBOutlet UILabel *dateLabel;
@property (weak,nonatomic) IBOutlet UILabel *statusLabel;
@property (weak,nonatomic) IBOutlet UILabel *regionLabel;
@property (weak,nonatomic) IBOutlet UIView *bgView;


@end
