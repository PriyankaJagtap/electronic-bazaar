//
//  ShippingStatusTableCell.h
//  EB
//
//  Created by Neosoft on 5/11/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShippingStatusTableCell : UITableViewCell
@property (weak,nonatomic) IBOutlet UIImageView *status_img;
@property (weak,nonatomic) IBOutlet UIView *bgView;

@end
