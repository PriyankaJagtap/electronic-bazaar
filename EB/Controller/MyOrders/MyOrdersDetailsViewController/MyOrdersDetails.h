//
//  MyOrdersDetails.h
//  EB
//
//  Created by webwerks on 9/23/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MyOrdersDetails : BaseNavigationControllerWithBackBtn
{
    NSMutableArray *myOrderResponse;
    __weak IBOutlet UILabel *order_Id_Lbl;
    __weak IBOutlet UILabel *status_Lbl;
    __weak IBOutlet UILabel *total_Lbl;
}
@property(nonatomic)int orderID;
@property(nonatomic)int incrementID;
@property(nonatomic)BOOL isFromVowDelight;

@property (weak, nonatomic) IBOutlet UIButton *reprintInvoiceButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reprintInvoiceBtnTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reprintInvoiceBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *objMyOrderTableview;
@property (weak, nonatomic) IBOutlet UILabel *orderCancelLabel;
- (IBAction)onClickOfNavBack:(id)sender;

@property(nonatomic, strong) NSString *userID;

@property(nonatomic)BOOL isFromOrderView;


@end
