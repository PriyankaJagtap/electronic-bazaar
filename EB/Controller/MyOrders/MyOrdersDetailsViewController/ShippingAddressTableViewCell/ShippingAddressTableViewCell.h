//
//  ShippingAddressTableViewCell.h
//  EB
//
//  Created by Neosoft on 4/11/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShippingAddressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end
