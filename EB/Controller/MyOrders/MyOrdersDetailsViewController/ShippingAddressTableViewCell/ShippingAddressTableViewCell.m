//
//  ShippingAddressTableViewCell.m
//  EB
//
//  Created by Neosoft on 4/11/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "ShippingAddressTableViewCell.h"

@implementation ShippingAddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [super awakeFromNib];

   // [self initialize];

}
- (void)initialize
{
    // This code is only called once
    self.bgView.layer.cornerRadius = 5;
    //self.bgView.layer.borderWidth= 2;
    //self.bgView.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor darkGrayColor]);
    [self.bgView setClipsToBounds:YES];
    //[[CommonSettings sharedInstance] setShadow:self.bgView];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
