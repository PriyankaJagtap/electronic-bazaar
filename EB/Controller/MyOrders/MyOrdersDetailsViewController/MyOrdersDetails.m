//
//  MyOrdersDetails.m
//  EB
//
//  Created by webwerks on 9/23/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//


#import "MyOrdersDetails.h"
#import "MyOrderDetailCell.h"
#import "MyOrderAddCell.h"
#import "MyOrderDetailProdCell.h"
#import "Webservice.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "OrderDetailTableViewCell.h"
#import "ShippingAddressTableViewCell.h"
#import "OrderStatusTableViewCell.h"
#import "OrderStatusWithDateTableCell.h"
#import "ShippingStatusTableCell.h"
#import "SelectIMEIViewController.h"
#import "SalesReturnViewController.h"
#import "SalesReturnProductClass.h"
#import "TrackingDetailsViewController.h"
#import "WarrantyViewController.h"


@interface MyOrdersDetails ()<FinishLoadingData,MyOrderDetailProdDelegate,UITableViewDataSource,UITableViewDelegate,SelectIMEIDelegate>
{
        NSInteger status;
        BOOL displayOrderStatus,orderCancel;
        NSArray *orderDataWithDateArr;
        NSDictionary *orderDataDic;
        NSString *orderCancelStr;
}
@end

@implementation MyOrdersDetails
@synthesize orderID,incrementID;
@synthesize objMyOrderTableview;
//static NSString *orderCannotCancelText = @"Your order can't be self cancelled. For further information contact customer care @1800-266-4000";
- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        //NSLog(@"orderid= %d",orderID);
        //    self.objMyOrderTableview.contentInset = UIEdgeInsetsMake(-36, 0, -36, 0);
        //
        [super setViewControllerTitle:@"Order Details"];
        [[GradientColorClass sharedInstance] setGradientBackgroundToButton:self.reprintInvoiceButton];
        self.objMyOrderTableview.estimatedRowHeight = 40;
        self.objMyOrderTableview.rowHeight = UITableViewAutomaticDimension;
        self.objMyOrderTableview.estimatedSectionHeaderHeight = 40;
        self.objMyOrderTableview.sectionHeaderHeight = UITableViewAutomaticDimension;
}

-(void)viewDidAppear:(BOOL)animated
{
        [super viewDidAppear:animated];
        
}
-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:YES];
        [self getMyOrderDetailsFromWs];
      // [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:5];
}

#pragma mark - UITableview Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        
        //if(orderCancel)
        // return 0;
        
        switch (section) {
                case 0:
                        //                        return [[myOrderResponse valueForKey:@"order_items"] count] + 1;
                        return [[myOrderResponse valueForKey:@"order_items"] count];
                        break;
                case 1:
                {
                        if(displayOrderStatus)
                                return orderDataWithDateArr.count;
                }
                        break;
                default:
                        break;
        }
        return 0;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        if(section == 0)
        {
                static NSString *cellIdentifier=@"OrderDetailTableViewCell";
                OrderDetailTableViewCell *cell=(OrderDetailTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                if (cell == nil) {
                        cell = [[OrderDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                }
                
                if(_isFromVowDelight)
                {
                cell.orderIdLabel.text = [NSString stringWithFormat:@"Order ID : %d",orderID];
                }
                else
                {
                cell.orderIdLabel.text = [NSString stringWithFormat:@"Order ID : %d",incrementID];
                }
                if([[myOrderResponse valueForKey:@"status"] isEqualToString:@"canceled"])
                        cell.statusLabel.text= [NSString stringWithFormat:@"Status : cancelled"];
                else
                        cell.statusLabel.text=[NSString stringWithFormat:@"Status : %@",[myOrderResponse valueForKey:@"status"]];
                
                [cell.cancelOrderBtn addTarget:self action:@selector(cancelOrderBtnClicked) forControlEvents:UIControlEventTouchUpInside];
                
                if(orderCancel)
                {
                        [cell.cancelOrderBtn setBackgroundColor:[UIColor lightGrayColor]];
                        cell.cancelOrderBtn.userInteractionEnabled = NO;
                }
                else
                {
                        if([[myOrderResponse valueForKey:@"can_cancel"] boolValue])
                        {
                                [[CommonSettings sharedInstance] setGradiantBackgroundColorToView:cell.cancelOrderBtn];
                                cell.cancelOrderBtn.userInteractionEnabled = YES;
                                
                        }
                        else
                        {
                                [cell.cancelOrderBtn setBackgroundColor:[UIColor lightGrayColor]];
                                cell.cancelOrderBtn.userInteractionEnabled = NO;
                        }
                        
                }
                cell.priceLabel.text = [NSString stringWithFormat:@"Total : %@",[[CommonSettings sharedInstance] formatPrice:[[myOrderResponse valueForKey:@"grand_total"] floatValue]]];
                cell.orderCancelLabel.text = orderCancelStr;
                return cell.contentView;
        }
        else if (section == 1 && displayOrderStatus)
        {
                
                
                if([[[orderDataDic valueForKey:@"data"] valueForKey:@"step"] isEqualToString:@"step2e"])
                {
                        static NSString *cellIdentifier=@"OrderStatusTableViewCell_1";
                        OrderStatusTableViewCell*cell=(OrderStatusTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                        if (cell == nil) {
                                cell = [[OrderStatusTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                        }
                        //                        cell.expectedDeliveryDateLabel.text =[NSString stringWithFormat:@"Expected Delivery Date: %@", [[orderDataDic valueForKey:@"data"] valueForKey:@"delivery_date"]];
                        //                        cell.serviceProviderLabel.text = [NSString stringWithFormat:@"By %@", [[orderDataDic valueForKey:@"data"] valueForKey:@"customer"]];
                        
                        cell.expectedDeliveryDateLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"delivery_date"];
                        cell.serviceProviderLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"customer"];
                        cell.orderDateLabel.text =[[orderDataDic valueForKey:@"data"] valueForKey:@"order_date"];
                        cell.msgLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"message"];
                        
                        cell.shippingDateLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"ship_date"];
                        if([[[orderDataDic valueForKey:@"data"] valueForKey:@"step"] isEqualToString:@"step2"])
                        {
                                cell.shippingImagView.image = [UIImage imageNamed:@"success"];
                        }
                        
                        [cell.trackOrderBtn addTarget:self action:@selector(viewTrackingDetailsBtnClicked) forControlEvents:UIControlEventTouchUpInside];
                        
                        
                        cell.orderLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"text1"];
                        cell.shippedLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"text2"];
                        
                        if([[[orderDataDic valueForKey:@"data"] valueForKey:@"tracking_details"] count] == 0)
                        {
                                [self.view layoutIfNeeded];
                                cell.trackOrderBtnHtConstraint.constant = 0;
                                cell.arrowImgHtConstraint.constant = 0;
                                cell.separaterLabel.hidden = YES;
                                [self.view layoutIfNeeded];
                                
                        }
                        else
                        {
                                [self.view layoutIfNeeded];
                                cell.trackOrderBtnHtConstraint.constant = 35;
                                cell.arrowImgHtConstraint.constant = 16;
                                cell.separaterLabel.hidden = NO;
                                [self.view layoutIfNeeded];
                                
                                
                        }
                        return cell.contentView;
                }
                else
                {
                        static NSString *cellIdentifier=@"OrderStatusTableViewCell";
                        OrderStatusTableViewCell*cell=(OrderStatusTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                        if (cell == nil) {
                                cell = [[OrderStatusTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                        }
                        cell.expectedDeliveryDateLabel.text =[[orderDataDic valueForKey:@"data"] valueForKey:@"delivery_date"];
                        cell.serviceProviderLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"customer"];
                        cell.deliveryDateLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"delivery_date"];
                        cell.shippingDateLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"ship_date"];
                        cell.orderDateLabel.text =[[orderDataDic valueForKey:@"data"] valueForKey:@"order_date"];
                        
                        cell.msgLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"message"];
                        if([[[orderDataDic valueForKey:@"data"] valueForKey:@"step"] isEqualToString:@"step2"])
                        {
                                cell.shippingImagView.image = [UIImage imageNamed:@"success"];
                        }
                        else if ([[[orderDataDic valueForKey:@"data"] valueForKey:@"step"] isEqualToString:@"step3"])
                        {
                                cell.shippingImagView.image = [UIImage imageNamed:@"success"];
                                cell.deliveryImageView.image = [UIImage imageNamed:@"success"];
                        }
                        [cell.trackOrderBtn addTarget:self action:@selector(viewTrackingDetailsBtnClicked) forControlEvents:UIControlEventTouchUpInside];
                        
                        
                        cell.orderLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"text1"];
                        cell.shippedLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"text2"];
                        cell.deliveredLabel.text = [[orderDataDic valueForKey:@"data"] valueForKey:@"text3"];
                        if([[[orderDataDic valueForKey:@"data"] valueForKey:@"tracking_details"] count] == 0)
                        {
                                [self.view layoutIfNeeded];
                                cell.trackOrderBtnHtConstraint.constant = 0;
                                cell.arrowImgHtConstraint.constant = 0;
                                cell.separaterLabel.hidden = YES;
                                [self.view layoutIfNeeded];
                                
                        }
                        else
                        {
                                [self.view layoutIfNeeded];
                                cell.trackOrderBtnHtConstraint.constant = 35;
                                cell.arrowImgHtConstraint.constant = 15;
                                cell.separaterLabel.hidden = NO;
                                [self.view layoutIfNeeded];
                                
                                
                        }
                        return cell.contentView;
                }
        }
        else if ((section == 1 && !displayOrderStatus) || (section == 2))
        {
                static NSString *cellIdentifier=@"ShippingAddressTableViewCell";
                
                ShippingAddressTableViewCell *cell=(ShippingAddressTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                if (cell == nil) {
                        cell = [[ShippingAddressTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                }
                cell.addressLabel.text = [myOrderResponse valueForKey:@"shipping_address"];
                return cell.contentView;
        }
        
        return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        // if(orderCancel)
        //  return 1;
        
        if(displayOrderStatus)
                return 3;
        else
                return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.section == 0)
        {
                if(indexPath.row == [[myOrderResponse valueForKey:@"order_items"] count])
                {
                        static NSString *cellIdentifier=@"ShippingStatusTableCell";
                        ShippingStatusTableCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                        cell.status_img.image=[UIImage imageNamed:[NSString stringWithFormat:@"steps_0%d",[self getStatusValue]]];
                        [[CommonSettings sharedInstance] setCornerRadiusToBottomLeftAndBottomRight:cell.bgView];
                        return cell;
                }
                else
                {
                        static NSString *cellIdentifier=@"MyOrderDetailProdCell";
                        MyOrderDetailProdCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                        NSDictionary *dic = [[myOrderResponse valueForKey:@"order_items"] objectAtIndex:indexPath.row];
                        [cell.product_Image sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"image"]]];
                        cell.product_Price.text = [[CommonSettings sharedInstance] formatPrice:[[dic valueForKey:@"price"] floatValue]];
                        if(![[dic valueForKey:@"name"] isKindOfClass:[NSNull class]])
                                cell.product_Name.text = [dic valueForKey:@"name"];
                        else
                                cell.product_Name.text = @" ";
                        cell.quantityLabel.text = [NSString stringWithFormat:@"Quantity:%@",[dic valueForKey:@"qty"]];
                        
                        if(indexPath.row == 0)
                        {
                                [[CommonSettings sharedInstance] setCornerRadiusToTopLeftAndTopRight:cell.bgView];
                        }
                        
                        NSInteger lastIndex = [tableView numberOfRowsInSection:indexPath.section]-1;
                        
                        if(indexPath.row == lastIndex)
                        {
                                cell.separatorLabel.hidden = YES;
                                [[CommonSettings sharedInstance] setCornerRadiusToBottomLeftAndBottomRight:cell.bgView];
                        }
                        else
                        {
                                cell.separatorLabel.hidden = NO;
                        }
                        
                        if([[dic valueForKey:@"return"] boolValue] && [[dic valueForKey:@"imei"]count] != 0)
                        {
                                cell.trackOrderBtn.hidden = NO;
                                cell.imeiNumberLabel.text =[NSString stringWithFormat:@"IMEI No.:%@",[[dic valueForKey:@"imei"] componentsJoinedByString:@","]] ;
                        }
                        else
                        {
                                cell.trackOrderBtn.hidden = YES;
                                cell.imeiNumberLabel.text = @"";
                                
                        }
                        
                        
                        if([dic valueForKey:@"category_type_img"] && ![[dic valueForKey:@"category_type_img"] isEqualToString:@""])
                        {
                                [cell.categoryTypeImgView sd_setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"category_type_img"]]];
                        }
                        else
                        {
                                cell.categoryTypeImgView.image = nil;
                        }
                        cell.trackOrderBtn.tag = indexPath.row;
                        [cell.trackOrderBtn addTarget:self action:@selector(trackOrderBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
                        cell.bg_trail.constant = 0;
                        cell.bg_width.constant = tableView.frame.size.width;
                        
                        return cell;
                }
        }else if (indexPath.section == 1 && displayOrderStatus){
                static NSString *cellIdentifier=@"OrderStatusWithDateTableCell";
                OrderStatusWithDateTableCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                NSDictionary *dic = [orderDataWithDateArr objectAtIndex:indexPath.row];
                cell.dateLabel.text = [dic valueForKey:@"date"];
                cell.statusLabel.text = [NSString stringWithFormat:@"Status:%@",[dic valueForKey:@"status"]];
                cell.regionLabel.text = [NSString stringWithFormat:@"Region:%@",[dic valueForKey:@"resion"]];
                //        if(indexPath.row == 0)
                //            [[CommonSettings sharedInstance] setCornerRadiusToTopLeftAndTopRight:cell.bgView];
                //
                //        if (indexPath.row == orderDataWithDateArr.count -1)
                //             [[CommonSettings sharedInstance] setCornerRadiusToBottomLeftAndBottomRight:cell.bgView];
                //
                
                return cell;
        }
        
        return nil;
}


#pragma mark- IBActions
- (IBAction)reprintInvoiceBtnClicked:(id)sender {
        
        NSString *url = [NSString stringWithFormat:@"%@%d",REPRINT_INVOICE_WS,orderID];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

-(void)trackOrderBtnClicked:(id)sender
{
        // [self getTrackYourOrderDetails];
        UIButton *btn = (UIButton *)sender;
        NSDictionary *dic = [[myOrderResponse valueForKey:@"order_items"] objectAtIndex:btn.tag];
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SalesReturnProductClass *objSalesReturnProductClass = [[SalesReturnProductClass alloc] init];
        objSalesReturnProductClass.status = [myOrderResponse valueForKey:@"status"];
        objSalesReturnProductClass.returnStatus = [[dic valueForKey:@"return_status"] objectAtIndex:0];
        objSalesReturnProductClass.orderNo = [myOrderResponse valueForKey:@"increment_id"];
        objSalesReturnProductClass.productName = [dic valueForKey:@"name"];
        objSalesReturnProductClass.IMEINo = [NSString stringWithFormat:@"%@",[[dic valueForKey:@"imei"] objectAtIndex:0]];
        objSalesReturnProductClass.price= [dic valueForKey:@"price"];
        objSalesReturnProductClass.shippingAddress = [myOrderResponse valueForKey:@"shipping_address"];
        
        if([[dic valueForKey:@"qty"] intValue] == 1)
        {
                SalesReturnViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"SalesReturnViewController"];
                vc.objSalesReturn = objSalesReturnProductClass;
                [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
                
                SelectIMEIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"SelectIMEIViewController"];
                viewController.IMEINumberDataArr = [dic valueForKey:@"imei"];
                viewController.returnStatusDataArr = [dic valueForKey:@"return_status"];
                viewController.objSalesReturn = objSalesReturnProductClass;
                viewController.deleagte = self;
                [self addChildViewController:viewController];
                viewController.view.frame = self.view.frame;
                [self.view addSubview:viewController.view];
                viewController.view.alpha = 0;
                [viewController didMoveToParentViewController:self];
                
                [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^
                 {
                         viewController.view.alpha = 1;
                 }
                                 completion:nil];
        }
        
}

-(void)cancelOrderBtnClicked
{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure you want to cancel your order?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alert show];
        
        
}

-(void)viewTrackingDetailsBtnClicked
{
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        TrackingDetailsViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"TrackingDetailsViewController"];
        vc.trackingDataArr = [[orderDataDic valueForKey:@"data"] valueForKey:@"tracking_details"];
        [self.navigationController pushViewController:vc animated:YES];
        
}
-(void)cancelOrder
{
        Webservice  *callMyOrdersWS = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callMyOrdersWS.delegate =self;
                NSInteger userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]integerValue];
                if (userId !=0)
                {
                        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        [callMyOrdersWS GetWebServiceWithURL:[NSString stringWithFormat:@"%@?order_id=%d",CANCEL_ORDER_WS,orderID] MathodName:CANCEL_ORDER_WS];
                }
        }
}


-(void)getMyOrderDetailsFromWs
{
        Webservice  *callMyOrdersWS = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callMyOrdersWS.delegate =self;
                       if(_isFromOrderView)
                       {
                               [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                               
//                               [callMyOrdersWS webServiceWitParameters:[NSString stringWithFormat:@"order_id=%d&userid=%@",orderID,_userID] andURL:GETORDERDETAILS MathodName:GETORDERDETAILS];
                                [callMyOrdersWS webServiceWitParameters:[NSString stringWithFormat:@"order_id=%d&userid=%@",incrementID,_userID] andURL:GETORDERDETAILS MathodName:GETORDERDETAILS];
                       }
                       else
                       {
                        
                        NSInteger userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]integerValue];
                        if (userId !=0)
                        {
                                [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                                
                                //[callMyOrdersWS webServiceWitParameters:[NSString stringWithFormat:@"order_id=%d",orderID] andURL:TRACK_MY_ORDER_WS MathodName:@"MYORDERDETAIL"];
                                [callMyOrdersWS webServiceWitParameters:[NSString stringWithFormat:@"order_id=%d&userid=%ld",incrementID,(long)userId] andURL:GETORDERDETAILS MathodName:GETORDERDETAILS];
                                
                        }
                       }
                
        }
}

-(void)getTrackYourOrderDetails
{
        Webservice  *callMyOrdersWS = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callMyOrdersWS.delegate =self;
              
                        
                        // [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:YES allowTap:YES];
                        [callMyOrdersWS GetWebServiceWithURL:[NSString stringWithFormat:@"%@%d",SHIPMENT_WS,incrementID] MathodName:SHIPMENT_WS];
                
                
        }
}
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        //status=[[response valueForKey:@"status"]intValue];
        if([strName isEqualToString:GETORDERDETAILS])
        {
                if ([[response valueForKey:@"status"]intValue]==1) {
                        myOrderResponse=[response valueForKey:@"data"];
                        if (![[myOrderResponse valueForKey:@"can_cancel"] boolValue]){
                                orderCancelStr = [myOrderResponse valueForKey:@"can_cancel_msg"];
                        }
                        //
                        
                        
                        if(![[myOrderResponse valueForKey:@"download"] boolValue])
                        {
                                _reprintInvoiceBtnTopConstraint.constant = 0;
                                _reprintInvoiceBtnHeightConstraint.constant = 0;
                        }
                        else
                        {
                                _reprintInvoiceBtnTopConstraint.constant = 5;
                                _reprintInvoiceBtnHeightConstraint.constant = 30;
                        }
                        OrderDetailTableViewCell *cell = (OrderDetailTableViewCell *)[objMyOrderTableview headerViewForSection:0];
                        if (![[myOrderResponse valueForKey:@"can_cancel"] boolValue]){
                                cell.orderCancelLabel.text = [myOrderResponse valueForKey:@"can_cancel_msg"];
                        }
                        //                         [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
                        //                         [objMyOrderTableview reloadData];
                        [self getTrackYourOrderDetails];
                }
                else
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
                }
                
        }
        else if([strName isEqualToString:@"MYORDERDETAIL"])
        {
                if ([[response valueForKey:@"status"]intValue]==1) {
                        myOrderResponse=[response valueForKey:@"data"];
                        if (![[myOrderResponse valueForKey:@"can_cancel"] boolValue]){
                                orderCancelStr = [myOrderResponse valueForKey:@"can_cancel_msg"];
                        }
                        [objMyOrderTableview reloadData];
                        
                        OrderDetailTableViewCell *cell = (OrderDetailTableViewCell *)[objMyOrderTableview headerViewForSection:0];
                        if (![[myOrderResponse valueForKey:@"can_cancel"] boolValue]){
                                cell.orderCancelLabel.text = [myOrderResponse valueForKey:@"can_cancel_msg"];
                        }
                }
                else
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        else if([strName isEqualToString:CANCEL_ORDER_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1) {
                        orderCancel = YES;
                        orderCancelStr = [response valueForKey:@"message"];
                        [self.view layoutIfNeeded];
                        [objMyOrderTableview reloadData];
                        _reprintInvoiceBtnTopConstraint.constant = 0;
                        _reprintInvoiceBtnHeightConstraint.constant = 0;
                        OrderDetailTableViewCell *cell = (OrderDetailTableViewCell *)[objMyOrderTableview headerViewForSection:0];
                        cell.orderCancelLabel.text = [response valueForKey:@"message"];
                        
                }
                else
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        else if ([strName isEqualToString:SHIPMENT_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1) {
                        displayOrderStatus = YES;
                        // orderDataWithDateArr = [[response valueForKey:@"data"] valueForKey:@"status"];
                        orderDataDic = response;
                        [self.objMyOrderTableview reloadData];
                }
                else
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
}


- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

- (IBAction)onClickOfNavBack:(id)sender
{
        [self.navigationController popViewControllerAnimated:YES];
        
}

#pragma mark - Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
        NSLog(@"btn index %d",buttonIndex);
        if(buttonIndex == 1)
                [self cancelOrder];
}


#pragma mark - get reprint Invoice
-(void)getInvoicePrint
{
        
}

#pragma mark - IMEI delegate method
-(void)IMEISelected:(SalesReturnProductClass *)objSalesReturnProdClass
{
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SalesReturnViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"SalesReturnViewController"];
        vc.objSalesReturn = objSalesReturnProdClass;
        [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - get status value
-(int)getStatusValue
{
        int status = 0;
        if ([[myOrderResponse valueForKey:@"state"]isEqualToString:@"new"])
                status=1;
        else if ([[myOrderResponse valueForKey:@"state"]isEqualToString:@"processing"])
                status=2;
        else if ([[myOrderResponse valueForKey:@"state"]isEqualToString:@"complete"])
                status=4;
        else if ([[myOrderResponse valueForKey:@"state"]isEqualToString:@"canceled"])
                status=0;
        else
                status=3;
        
        return status;
}

#pragma mark - product detail delegate
-(void)productDetailClicked:(NSIndexPath *)indexPath;
{
        NSLog(@"selected index %ld",(long)indexPath.row);
}


@end
