//
//  OrderStatusTableViewCell.h
//  EB
//
//  Created by Neosoft on 4/11/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderStatusTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *expectedDeliveryDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceProviderLabel;
@property (weak, nonatomic) IBOutlet UIImageView *orderImageView;
@property (weak, nonatomic) IBOutlet UILabel *orderDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *deliveryImageView;
@property (weak, nonatomic) IBOutlet UILabel *deliveryDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *shippingImagView;
@property (weak, nonatomic) IBOutlet UILabel *shippingDateLabel;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIButton *trackOrderBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trackOrderBtnHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *arrowImgHtConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImgView;
@property (weak, nonatomic) IBOutlet UILabel *separaterLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippedLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveredLabel;
@property (weak, nonatomic) IBOutlet UILabel *msgLabel;

@end
