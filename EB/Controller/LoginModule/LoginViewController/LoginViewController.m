//
//  LoginViewController.m
//  EB
//
//  Created by webwerks on 8/7/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "LoginViewController.h"
#import "SlideMenuViewController.h"
#import "HomeViewController.h"
#import "RegistrationViewController.h"
#import "Constant.h"
#import "FVCustomAlertView.h"
#import "CommonSettings.h"
#import "GoogleLoginViewController.h"
#import "ForgotPassword.h"
#import <Crashlytics/Crashlytics.h>
#import "ALAlertBanner.h"
#import "NIDropDown.h"
#import "MyWishlist.h"
#import "VowDelightViewController.h"


#define LAPTOP_RETAILER @"laptop_retailer";
#define MOBILE_RETAILER @"mobile_retailer";
#define BOTH_RETAILER @"both";
#define BUSINESS_TYPE @"business_type";

@interface LoginViewController ()<NIDropDownDelegate>
{
    NSMutableDictionary *dictFBUserDetails;
    UITextField *forgotPwdTxtfield;
    NSString *businessType;
    NIDropDown *businessTypeDropDown;
    NSArray *businessTypeArr;
}
@end

@implementation LoginViewController
@synthesize facebookBtn, submitBtn,emailIdTxt, passwordTxt;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set Place holder color
    UIColor *color = [UIColor grayColor];
     NSString *str = @"Email ID";
    emailIdTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName: color}];
    [CommonSettings setAsterixToTextField:emailIdTxt withPlaceHolderName:str];
        
    str = @"Password";
    passwordTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName: color}];
    [CommonSettings setAsterixToTextField:passwordTxt withPlaceHolderName:str];
        
    // set wrap view for text field
    [self createWrapView:emailIdTxt];
    [self createWrapView:passwordTxt];
  
     [[GradientColorClass sharedInstance] setGradientBackgroundToButton:submitBtn];
//    [_businessTypeBtn.layer setCornerRadius:5.0];
    
    
    // add gesture to hide keyboard
    UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
    tapScroll.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapScroll];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults valueForKey:@"email"])
        emailIdTxt.text = [defaults valueForKey:@"email"];
    
    if([defaults valueForKey:@"password"])
        passwordTxt.text = [defaults valueForKey:@"password"];
    
    [self getRegisterationData];

}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=YES;
  
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)navigateToSellGadgetView
{
    [APP_DELEGATE.tabBarObj unhideTabbar];
    
}

-(void)navigateToParticularViewController
{
         LoginSignUpViewController *vc = (LoginSignUpViewController *)self.parentViewController;
        
        if([vc.navigateToViewAfterLogin isEqualToString:PRODUCT_DETAIL] ||[vc.navigateToViewAfterLogin isEqualToString:CATEGORY] || [vc.navigateToViewAfterLogin isEqualToString:SEARCH] ||[vc.navigateToViewAfterLogin isEqualToString:DEALS])
        {
                [self.navigationController popViewControllerAnimated:NO];
                     [vc.delegate refreshView];
                return;
//                [self dismissViewControllerAnimated:NO completion:^{
//                        [vc.delegate refreshView];
//                        return ;
//                }];
        }
          [self.navigationController popViewControllerAnimated:NO];
    //    [self dismissViewControllerAnimated:NO completion:nil];
       
        // Navigate from Login screen t home screen.
        UIStoryboard *storyBoard;
        
        storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        [APP_DELEGATE.tabBarObj unhideTabbar];
        
        if([vc.navigateToViewAfterLogin isEqualToString:MY_PROFILE] )
        {
                [APP_DELEGATE.tabBarObj TabClickedIndex:2];

        }
        else if([vc.navigateToViewAfterLogin isEqualToString:DEALS])
        {
                [APP_DELEGATE.tabBarObj TabClickedIndex:3];
                
        }
        else if([vc.navigateToViewAfterLogin isEqualToString:TRACK_ORDER])
        {
                [APP_DELEGATE.tabBarObj TabClickedIndex:3];

        }
        else if([vc.navigateToViewAfterLogin isEqualToString:MY_CART])
        {
                [APP_DELEGATE.tabBarObj TabClickedIndex:0];
                
        }
        
        
        if([vc.navigateToViewAfterLogin isEqualToString:VOW_DELIGHT])
        {
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
                VowDelightViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"VowDelightViewController"];
                UINavigationController *nav = (UINavigationController *)APP_DELEGATE.tabBarObj.selectedViewController;
                [nav pushViewController:vc animated:YES];
        }
        else if([vc.navigateToViewAfterLogin isEqualToString:MY_WISHLIST])
        {
                MyWishlist *objWishList = [storyBoard instantiateViewControllerWithIdentifier:@"MyWishlist"];
                UINavigationController *nav = (UINavigationController *)APP_DELEGATE.tabBarObj.selectedViewController;
                [nav pushViewController:objWishList animated:YES];
        }
        else if([vc.navigateToViewAfterLogin isEqualToString:MY_CART])
        {
                UINavigationController *nav = (UINavigationController *)APP_DELEGATE.tabBarObj.selectedViewController;
                MyCartViewController  *myCart= [storyBoard instantiateViewControllerWithIdentifier:@"MyCartViewController"];
                [nav pushViewController:myCart animated:NO];
        
        }
       
        
        
        
}
-(void)navigateToHomeViewcontroller
{
    // Set Firsttime value
    [[NSUserDefaults standardUserDefaults] setObject:@"FirstTime" forKey:@"FirstTime"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    // Navigate from Login screen t home screen.
    UIStoryboard *storyBoard;

        storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
  
        UINavigationController *navigationController ;
    
      self.tabBarObj = [[ShowTabbars alloc] init];
      [self.tabBarObj showTabbars];
      [self.tabBarObj TabClickedIndex:0];

    
      SlideMenuViewController *  objMainMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"SlideMenuViewController"];
        
      RightMenuViewController*  objRightMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"RightMenuViewController"];
        
      MFSideMenuContainerViewController *container =[MFSideMenuContainerViewController
                                                       containerWithCenterViewController:self.tabBarObj
                                                       leftMenuViewController:objMainMenuVC
                                                       rightMenuViewController:objRightMenuVC];
        
    [AppDelegate getAppDelegateObj].navigationController=navigationController;
    [AppDelegate getAppDelegateObj].tabBarObj=self.tabBarObj;
    [[[UIApplication sharedApplication]delegate].window setRootViewController:container];
    [navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)createWrapView:(UITextField *)textfield{
    UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, textfield.frame.size.height)];
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = wrapView;
    [textfield setAutocorrectionType:UITextAutocorrectionTypeNo];

}


#pragma mark - UITextField Delegate

-(void)hideKeyBoard:(UIGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}


- (BOOL) textFieldDidChange:(UITextField *)textField
{
    UIButton *btnColor = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnColor addTarget:self action:@selector(btnOKPressedForPwd:) forControlEvents:UIControlEventTouchUpInside];
    btnColor.frame = CGRectMake((forgotPwdTxtfield.frame.size.width- 40), 3, 30, 25);
    btnColor.backgroundColor = [UIColor lightGrayColor];
    [btnColor setTitle:@"OK" forState:UIControlStateNormal];
    [btnColor setTitle:@"OK" forState:UIControlStateSelected];
    btnColor.titleLabel.font = karlaFontRegular(14.0);
    btnColor.layer.cornerRadius = 5.0;
    [forgotPwdTxtfield addSubview:btnColor];
    return YES;
}


-(void)btnOKPressedForPwd:(UIButton *)sender{
    
    if (forgotPwdTxtfield.text.length > 0) {
        if (![self validEmail:forgotPwdTxtfield.text] ) {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Incorrect email format."];

        }
        else{
        [FVCustomAlertView hideAlertFromView:self.view fading:YES];
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Password is been sent to your email id."];
        }
    }
}


// Check email validity

-(BOOL)validEmail:(NSString*)emailString
{
    NSString *emailid = emailString;
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL myStringMatchesRegEx=[emailTest evaluateWithObject:emailid];
    
    if(myStringMatchesRegEx)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark - Skip Login 

- (IBAction)closeLoginAction:(id)sender {
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Want to continue without login?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
//    [alert show];
    [self navigateToHomeViewcontroller];

}
-(IBAction)gotoHomePage:(id)sender
{
    [self navigateToHomeViewcontroller];
}

- (IBAction)skipLoginAction:(id)sender {
    UIStoryboard *objStoryboard;
    objStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RegistrationViewController *objc = [objStoryboard instantiateViewControllerWithIdentifier:@"RegistrationViewController"];
    [self.navigationController pushViewController:objc animated:YES];
}


#pragma mark - Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
       // [self navigateToHomeViewcontroller];
        [self skipLoginAction:nil];
    }
}


#pragma mark - Social Login Methods



#pragma Google Plus

- (IBAction)googlePlusLoginAction:(id)sender {
    GoogleLoginViewController *objc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GoogleLoginViewController"];
    [self.navigationController pushViewController:objc animated:YES];

    //[self signInGoogle];
}

/*
- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth error: (NSError *) error {
    DLog(@"Received error %@ and auth object %@",error, auth);
    [FVCustomAlertView showDefaultDoneAlertOnView:self.view withTitle:@"Done" withBlur:NO allowTap:YES];

    //    [self startProgressHUD];
    if (error) {
        [[AppDelegate getAppDelegateObj] StopProgressHud];
        DLog(@"Sign-In Error: %@", error);
        if ([error code] == -1) {
            DLog(@"Unknown error, but user probably cancelled.");
        } else if([error code] == 400 &&
                  [[error domain] isEqualToString:@"com.google.HTTPStatus"]) {
            DLog(@"400 error, user has probably disconnected using the "
                 "app managment page.");
        }
        [[GPPSignIn sharedInstance] signOut];
    }
    else
    {
        [[AppDelegate getAppDelegateObj] StopProgressHud];
        // getting the access token from auth
        NSString  *accessTocken = [auth valueForKey:@"accessToken"]; // access tocken pass in .pch file
        DLog(@"%@",accessTocken);
        NSString *str=[NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/userinfo?access_token=%@",accessTocken];
        NSString *escapedUrl = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",escapedUrl]];
        NSString *jsonData = [[NSString alloc] initWithContentsOfURL:url usedEncoding:nil error:nil];
        NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
        NSString *userId=[jsonDictionary objectForKey:@"id"];
        DLog(@" user deata %@",jsonData);
        DLog(@"Received Access Token:%@",auth);
        DLog(@"user google user id  %@",userId); //logged in user's email id
        
//        [UserDetailDict setObject:[jsonDictionary objectForKey:@"name"] forKey:@"name"];
//        [UserDetailDict setObject:userId forKey:@"email"];
//        [UserDetailDict setObject:[jsonDictionary objectForKey:@"picture"] forKey:@"profilePicture"];
//        [self socialLogin:userId];
    }
}
*/
/*
- (void)signInGoogle {
    
//  [[GPPSignIn sharedInstance] signOut];
    
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [[AppDelegate getAppDelegateObj] StartProgressHud];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet connection available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@"" withBlur:NO allowTap:YES];
//        [[AppDelegate getAppDelegateObj] StopProgressHud];
        
        
        GPPSignIn *signIn1 = [GPPSignIn sharedInstance];
        signIn1.shouldFetchGooglePlusUser = YES;
        signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
        
        // You previously set kClientId in the "Initialize the Google+ client" step
        signIn1.clientID = kClientId;
        
        // Uncomment one of these two statements for the scope you chose in the previous step
        signIn1.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
        signIn.scopes = @[ @"profile" ];            // "profile" scope
        
        // Optional: declare signIn.actions, see "app activities"
        signIn1.delegate = self;
        
    }
}
*/


#pragma Facebook

- (IBAction)facebookLoginAction:(id)sender {
    [FBSession openActiveSessionWithReadPermissions:@[@"email",@"user_birthday"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         
         [self sessionStateChanged:session state:state error:error];
     }];
    
}


- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    if (!error && state == FBSessionStateOpen)
    {
        //NSLog(@"Session opened");
        
        // COMMON_SETTINGS.userToken = session.accessTokenData.accessToken;
        [self userLoggedIn];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        //NSLog(@"Session closed");
        UIAlertView *connectionAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please wait..." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [connectionAlert show];
    }
    
    // Handle errors
    if (error)
    {
        //NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES)
        {
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            [COMMON_SETTINGS showAlertTitle:alertText message:alertTitle];
        }
        else
        {
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled)
            {
                //NSLog(@"User cancelled login");
            }
            else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession)
            {
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                [COMMON_SETTINGS showAlertTitle:alertText message:alertTitle];
            }
            else
            {
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                [COMMON_SETTINGS showAlertTitle:alertText message:alertTitle];
            }
        }
        
        // [FBSession.activeSession closeAndClearTokenInformation];
    }
}


- (void)userLoggedIn
{
    [FBRequestConnection startWithGraphPath:@"me?fields=id,name,email,first_name,birthday,picture.height(101).width(101),last_name"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error)
                              {
                                  //NSLog(@"user info: %@", result);
                                  if ([result isKindOfClass:[NSDictionary class]])
                                  {
                                      //NSLog(@"%@",result);
                                      NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                                      NSMutableDictionary *dictData = [NSMutableDictionary new];
                                      [dictData setObject:[result valueForKey:@"email"] forKey:@"username"];
                                      [dictData setObject:@"" forKey:@"password"];
                                      [dictData setObject:@"iphone" forKey:@"device_type"];
                                      [dictData setObject:@"fb" forKey:@"social_type"];
                                      [dictData setObject:[result valueForKey:@"id"] forKey:@"social_login_id"];
                                      NSString *pushID=[defaults valueForKey:@"DEVICE_TOKEN"];
                                      [dictData setObject:pushID?:@"" forKey:@"push_id"];
                                      [dictData setObject:[result valueForKey:@"first_name"] forKey:@"firstname"];
                                      [dictData setObject:[result valueForKey:@"last_name"] forKey:@"lastname"];
                                      NSString *strDob=[self formatDateWithString:[result valueForKey:@"birthday"]];
                                      [dictData setObject:strDob forKey:@"dob"];
                                      [dictData setObject:@"" forKey:@"mobile"];
                                      //NSLog(@"%@",[[[result valueForKey:@"picture"]valueForKey:@"data"]valueForKey:@"url"]);
                                      
                                      [dictData setObject:[[[result valueForKey:@"picture"]valueForKey:@"data"]valueForKey:@"url"] forKey:@"profile_picture"];
                                      
                                      // SAVE PROFILE PICTURE in userdefaults
                                      NSString *strProfile = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"profile_picture"]];
                                      [[NSUserDefaults standardUserDefaults]setObject:strProfile forKey:@"profile_picture"];
                                      [[NSUserDefaults standardUserDefaults]synchronize];
                                      [self callLoginWS:dictData];
                                  }
                              }
                              else
                              {
                                  if (FBSession.activeSession.state == FBSessionStateOpen
                                      || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
                                  {
                                      [FBSession.activeSession closeAndClearTokenInformation];
                                  }
                              }
                          }];
}

-(NSString*)formatDateWithString:(NSString*)dateStr
{
    if (dateStr.length>0)
    {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSDate *date=[dateFormatter dateFromString:dateStr];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSString *formattedDate=[dateFormatter stringFromDate:date];
        return formattedDate;
    }
    return @"";
}

#pragma mark Submit
- (IBAction)showPasswordAction:(id)sender {
    _showPasswordBtn.selected = ! _showPasswordBtn.selected;
    
    if (_showPasswordBtn.selected) {
        passwordTxt.secureTextEntry = NO;
        passwordTxt.text =passwordTxt.text ;
    }
    else{
        passwordTxt.secureTextEntry = YES;
        passwordTxt.text =passwordTxt.text ;
    }
}

- (IBAction)forgotPasswordAction:(id)sender {

    ForgotPassword *objc = [[UIStoryboard storyboardWithName:@"Product" bundle:nil] instantiateViewControllerWithIdentifier:@"ForgotPassword"];
    [self.navigationController pushViewController:objc animated:YES];

}

- (IBAction)submitAction:(id)sender {
  
        [self.view endEditing:YES];
        
    if ([emailIdTxt.text isEqualToString:@""]) {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter email ID"];
        return;
    }
    if([passwordTxt.text isEqualToString:@""]){
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter password"];
        return;
    }
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setValue:emailIdTxt.text forKey:@"email"];
        [defaults setValue:passwordTxt.text forKey:@"password"];

    NSMutableDictionary *dictData = [NSMutableDictionary new];
    [dictData setObject:emailIdTxt.text forKey:@"username"];
    [dictData setObject:passwordTxt.text forKey:@"password"];
    [dictData setObject:@"iphone" forKey:@"device_type"];
    
    [dictData setObject:@"normal" forKey:@"social_type"];
    [dictData setObject:@"" forKey:@"social_login_id"];
    NSLog(@"device token %@",[defaults valueForKey:@"DEVICE_TOKEN"]);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DEVICE_TOKEN"]);
    NSString *pushID=[defaults objectForKey:@"DEVICE_TOKEN"];
    [dictData setObject:pushID?:@"" forKey:@"push_id"];
    [dictData setObject:@"" forKey:@"firstname"];
    [dictData setObject:@"" forKey:@"lastname"];
    [dictData setObject:@"" forKey:@"dob"];
    [dictData setObject:@"" forKey:@"mobile"];
    [dictData setObject:@"" forKey:@"profile_picture"];
    
    
    NSString *businessTypeBtnTitile = [_businessTypeBtn titleForState:UIControlStateNormal];
    NSArray *filterBusinessArr =[businessTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(label == %@)",businessTypeBtnTitile ]];
    
    businessType = [NSString stringWithFormat:@"%@",[[filterBusinessArr objectAtIndex:0] valueForKey:@"id"]];
    [dictData setObject:businessType forKey:@"businessType"];
    [self callLoginWS:dictData];
  
}


#pragma mark - NIDropdown

- (void)rel
{
    businessTypeDropDown = nil;
}

- (void)niDropDownDelegateMethod:(NIDropDown *) sender
{
    [self rel];
    
}

- (void) getindexValue: (NIDropDown *)sender withindex:(NSIndexPath*)index{
    DLog(@"index %ld",(long)index.row);
  
}
#pragma mark - get resgisteration data
-(void)getRegisterationData
{
     [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:GET_REGISTRATION_DATA MathodName:@"GET_REGISTRATION_DATA"];
        
    }
}


#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:@"GET_REGISTRATION_DATA"]){
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            businessTypeArr = [response valueForKey:@"business_type"];
            if (businessTypeArr.count != 0)
            {
                [_businessTypeBtn setTitle:[[businessTypeArr objectAtIndex:0] valueForKey:@"label"] forState:UIControlStateNormal];
                businessType = [NSString stringWithFormat:@"%@",[[businessTypeArr objectAtIndex:0] valueForKey:@"id"]];
            }
        }
        else
        {
                [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
        }
        
    }
}


#pragma mark - LoginWS
-(void)callLoginWS:(NSMutableDictionary *)userdata{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    
    [userdata setValue:[CommonSettings getAppCurrentVersion] forKey:@"version"];
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:userdata url:LOGIN_WS string:@"LoginWS"];
    }
}

-(void)receivedResponseForLogin:(id)receiveData stringResponse:(NSString *)responseType{
    [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
    if ([responseType isEqualToString:@"success"]) {
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        NSMutableDictionary *dictUserDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];

        DLog(@"userDetail %@",jsonObject);
        //[CommonSettings setMyCartcount:[NSString stringWithFormat:@"%d",[[dictUserDetail valueForKey:@"cart_total_qty"]integerValue]]];
        
        
        [MyCartClass setMyCartcount:[NSNumber numberWithInteger:[[dictUserDetail valueForKey:@"cart_total_qty"]integerValue]]];
        if ([dictUserDetail isKindOfClass:[NSDictionary class]])
        {
            NSString *status = [NSString stringWithFormat:@"%ld",(long)[[dictUserDetail objectForKey:@"status"]integerValue]];
            if ([status isEqualToString:@"1"])
            {
                //NSString *tokenVal = [dictUserDetail objectForKey:@"token"]?:@"";
                if([[dictUserDetail valueForKey:@"customer_id"] integerValue] == 5)
                {
                    [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Only Electronics Bazaar users can login from here"];
                    emailIdTxt.text = nil;
                    passwordTxt.text = nil;
                    return;
                }

                //If user is IBM User
                if ([[dictUserDetail objectForKey:@"is_ibm"] isEqualToString:@"Yes"])
                {
                    [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Only Electronics Bazaar users can login from here"];
                    [[NSUserDefaults standardUserDefaults] setObject:[dictUserDetail objectForKey:@"is_ibm"] forKey:@"is_ibm"];
                    return;
                }
                else
                {
                    NSString *tokenVal =@"";
                    NSString *loginType = [dictUserDetail objectForKey:@"login_type"];
                    DLog(@"%@",tokenVal);
                    [[NSUserDefaults standardUserDefaults]setObject:tokenVal forKey:@"token"];
                    [[NSUserDefaults standardUserDefaults]setObject:loginType forKey:@"login_type"];
                    [[NSUserDefaults standardUserDefaults]setObject:[dictUserDetail valueForKey:@"customer_id"] forKey:EB_CUSTOMER_ID];
                    [[NSUserDefaults standardUserDefaults] setObject:[dictUserDetail objectForKey:@"is_ibm"] forKey:@"is_ibm"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    NSMutableDictionary *userdict = [NSMutableDictionary new];
                    
                    NSString *uid =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"id"]];
                    NSString *firstname = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"firstname"]];
                    NSString *lastname = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"lastname"]];
                    NSString *dob = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"dob"]];
                    NSString *mobile = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"mobile"]];
                    NSString *profile_picture = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"profile_picture"]];
                    NSString *quote_ID = [NSString stringWithFormat:@"%@",[dictUserDetail objectForKey:@"quoteId"]];
                    
                    // NSString *company_Name=[]
                    
                    NSString *email = @"";
                    if ([loginType isEqualToString:@"normal"])
                    {
                        email = emailIdTxt.text;
                    }
                    else
                    {
                        email =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"email"]];
                    }
                    
                    [userdict setObject:uid?uid:@"" forKey:@"user_id"];
                    [userdict setObject:firstname?firstname:@"" forKey:@"firstname"];
                    [userdict setObject:lastname?lastname:@"" forKey:@"lastname"];
                    [userdict setObject:dob?dob:@"" forKey:@"dob"];
                    [userdict setObject:mobile?mobile:@"" forKey:@"mobile"];
                    [userdict setObject:profile_picture?profile_picture:@"" forKey:@"profile_picture"];
                    [userdict setObject:email forKey:@"email"];
                    [userdict setObject:passwordTxt.text forKey:@"password"];
                    [userdict setObject:[NSNumber numberWithBool:[[[dictUserDetail objectForKey:@"user"] objectForKey:@"is_arm"] boolValue]] forKey:@"is_arm"];
                    
                    NSString *businessType1 = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"business_type"]];
                    
                    if(![businessType1 isEqualToString:@""])
                        [userdict setValue:businessType1 forKey:@"business_type"];
                    
                    
                    if([[dictUserDetail objectForKey:@"user"] objectForKey:@"gstin"])
                    {
                        NSString *gstin = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"gstin"]];
                        if(![gstin isEqualToString:@""])
                        {
                            [userdict setValue:gstin forKey:@"gstin"];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:NO] forKey:SHOW_GSTIN];
                        }
                        else
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:YES] forKey:SHOW_GSTIN];
                    }
                    
                    if([[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"] && ![[[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"] isKindOfClass:[NSNull class]] && ![[[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"] isEqualToString:@""])
                    {
                        NSString *taxVat = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"]];
                        [userdict setValue:taxVat forKey:@"taxvat"];
                    }
                    
                    NSString *referredBy =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"referred_by"]];
                    if(![referredBy isEqualToString:@""])
                        [userdict setValue:referredBy forKey:@"referred_by"];
                    
                    if(![[[dictUserDetail valueForKey:@"user"] valueForKey:@"pincode"] isEqualToString:@""])
                    {
                        [userdict setValue:[[dictUserDetail valueForKey:@"user"] valueForKey:@"pincode"] forKey:@"pincode"];
                    }
                    
                    
                    if(![[[dictUserDetail valueForKey:@"user"] valueForKey:@"store_name"] isKindOfClass:[NSNull class]])
                    {
                        [userdict setValue:[[dictUserDetail valueForKey:@"user"] valueForKey:@"store_name"] forKey:@"storeName"];
                    }
                    //
                    if([[dictUserDetail valueForKey:@"customer_id"] integerValue] == 4)
                    {
                        NSInteger i = [[dictUserDetail valueForKey:@"customer_id"] integerValue];
                        [[NSUserDefaults standardUserDefaults]setObject: [NSNumber numberWithInteger:i]forKey:@"AffiliateStatus"];
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:0] forKey:@"AffiliateStatus"];
                    }
                    
                    [[NSUserDefaults standardUserDefaults]setObject:quote_ID forKey:@"QuoteID"];
                    [[NSUserDefaults standardUserDefaults] setObject:userdict forKey:@"user"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    LoginSignUpViewController *vc = (LoginSignUpViewController*)self.parentViewController;
                    if(vc.navigateToViewAfterLogin != nil)
                    {
                        [self navigateToParticularViewController];
                    }
                    else
                    {
                        [self navigateToHomeViewcontroller];
                    }
                }
            }
            else if ( [status isEqualToString:@"2"])
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[dictUserDetail valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alert show];
                
            }
            else{
                 [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:[dictUserDetail valueForKey:@"message"]];
            }
        }
        else{
            [[AppDelegate getAppDelegateObj]showAlert:@"Sorry" withMessage:@"Login couldn't succeed."];
        }
    }
}


- (IBAction)retailerBtnClicked:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    if(businessTypeArr.count != 0)
    {
        NSArray * arr = [businessTypeArr valueForKey:@"label"];
        if(businessTypeDropDown == nil){
            CGFloat f= 100.0;
            if(arr.count <= 3)
                f = 30 * arr.count;
            businessTypeDropDown = [[NIDropDown alloc] showDropDown:btn withHeight:&f withData:arr withImages:Nil WithDirection:@"down"];
            businessTypeDropDown.delegate = self;
            
        }
        else{
            [businessTypeDropDown hideDropDown:sender];
            [self rel];
        }
        
    }

    
   /* switch (btn.tag) {
        case 100:
        {
            _laptopRetailerBtn.selected = YES;
            _mobileRetailerBtn.selected = NO;
            _bothRetailerBtn.selected = NO;
            businessType = LAPTOP_RETAILER;
            
            
           
            
        }
            break;
        case 101:
        {
            _laptopRetailerBtn.selected = NO;
            _mobileRetailerBtn.selected = YES;
            _bothRetailerBtn.selected = NO;
            businessType = MOBILE_RETAILER;
        }
            break;
            
        case 102:
        {
            _laptopRetailerBtn.selected = NO;
            _mobileRetailerBtn.selected = NO;
            _bothRetailerBtn.selected = YES;

            businessType = BOTH_RETAILER;
        }
            break;
            
        default:
            break;
    }*/
    
}
@end
