//
//  LoginViewController.h
//  EB
//
//  Created by webwerks on 8/7/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//  
#import <UIKit/UIKit.h>

#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "AppDelegate.h"
#import "Webservice.h"
#import <FacebookSDK/FacebookSDK.h>
#import "ShowTabbars.h"
#import "LoginSignUpViewController.h"

//@class GPPSignInButton;
@class MLTableAlert;

static NSString * const kClientId = @"690280904600-lemlgj8pnq2cp6jjgfmoq2pvcp72g9k5.apps.googleusercontent.com";


@interface LoginViewController : UIViewController<FinishLoadingData,FBLoginViewDelegate>{
    //GPPSignIn *signIn;
    //GPPSignInDelegate
}

@property (strong,nonatomic)ShowTabbars *tabBarObj;

@property (weak, nonatomic) IBOutlet UITextField *emailIdTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;
@property (weak, nonatomic) IBOutlet UIButton *showPwdBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgotPwdBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *facebookBtn;
//@property (weak, nonatomic) IBOutlet GPPSignInButton *googlePlusBtn;
@property (weak, nonatomic) IBOutlet UIButton *skipLoginBtn;

- (IBAction)closeLoginAction:(id)sender;

- (IBAction)skipLoginAction:(id)sender;
- (IBAction)googlePlusLoginAction:(id)sender;
- (IBAction)facebookLoginAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *showPasswordBtn;
- (IBAction)showPasswordAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn;
- (IBAction)forgotPasswordAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *mobileRetailerBtn;

- (IBAction)submitAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *bothRetailerBtn;
@property (weak, nonatomic) IBOutlet UIButton *businessTypeBtn;

@property (weak, nonatomic) IBOutlet UIButton *laptopRetailerBtn;
- (IBAction)retailerBtnClicked:(id)sender;
@property (nonatomic) BOOL isSellGadgetBannerClicked;

@end
