//
//  SplashScreenViewController.m
//  EB
//
//  Created by Neosoft on 9/13/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "WebViewController.h"

@interface SplashScreenViewController ()<FinishLoadingData>

@end

@implementation SplashScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self getHomeFlagData];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:@"FirstTimeLanuch"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - get resgisteration data
-(void)getHomeFlagData
{
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        //[FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        NSString *pushID=[[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_TOKEN"];
        NSString *deviceID = pushID?:@"";
        callLoginService.delegate =self;
        NSString *url = [NSString stringWithFormat:@"%@%@&version=%@",HOME_FLG_WS,deviceID,[CommonSettings getAppCurrentVersion]];
            
           // NSString *url = [NSString stringWithFormat:@"%@%@",HOME_FLG_WS,deviceID];
        [callLoginService GetWebServiceWithURL:url MathodName:HOME_FLG_WS];
        
    }
}


#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:HOME_FLG_WS]){
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            [[AppDelegate getAppDelegateObj] setInitialViewController];
        }
        else
        {
            UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"Product" bundle:nil];
            WebViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"WebViewController"];
            vc.webViewUrl = [response valueForKey:@"url"];
            [AppDelegate getAppDelegateObj].window.rootViewController = vc;
        }
        
    }
}

@end
