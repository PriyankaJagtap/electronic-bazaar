//
//  ForgotPassword.h
//  EB
//
//  Created by webwerks on 10/1/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"
@interface ForgotPassword : UIViewController<FinishLoadingData,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *emailIdLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailIdTextField;
@property (weak, nonatomic) IBOutlet UILabel *ebPinLabel;
@property (weak, nonatomic) IBOutlet UITextField *ebPinTextField;
@property (weak, nonatomic) IBOutlet UILabel *mobileNoLabel;
@property (weak, nonatomic) IBOutlet UITextField *mobileNoTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ebPinViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIButton *sendMobileNumberBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendEmailBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendEBPinBtn;
@property (weak, nonatomic) IBOutlet UIButton *goBackBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendMobileNumberBtnWidthConstraint;

- (IBAction)sendMobileNumberBtnClicked:(id)sender;
- (IBAction)sendEBPinBtnClicked:(id)sender;
- (IBAction)sendEmailBtnClicked:(id)sender;

- (IBAction)goBackBtnAction:(id)sender;



@end

