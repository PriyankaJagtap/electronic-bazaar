//
//  ForgotPassword.m
//  EB
//
//  Created by webwerks on 10/1/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

//#define FORGOT_PASSWORD_URL @"http://www.electronicsbazaar.com/customer/account/forgotpassword/"

#import "ForgotPassword.h"
#import "AppDelegate.h"
#import "Validation.h"
#import "CommonSettings.h"
#import "ResetPasswordViewController.h"

@implementation ForgotPassword
{
        NSString *ebPin;
}
@synthesize sendMobileNumberBtn,sendEBPinBtn,sendEmailBtn;
-(void)viewDidLoad{
    [super viewDidLoad];
        [CommonSettings addPaddingAndImageToTextField:_emailIdTextField withImageName:@"gray_email_icon"];
        [CommonSettings addPaddingAndImageToTextField:_mobileNoTextField withImageName:@"gray_mobile_icon"];
          [CommonSettings addPaddingAndImageToTextField:_ebPinTextField withImageName:@""];
        [_mobileNoTextField addTarget:self
                      action:@selector(mobileValueChanged:)
            forControlEvents:UIControlEventEditingChanged];

    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:sendMobileNumberBtn];
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:sendEBPinBtn];
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:sendEmailBtn];
}

-(BOOL)validateEmail
{
    if ([_emailIdTextField.text isEqualToString:@""])
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter email id."];
        return NO;
    }
    if (![Validation ValidEmail:_emailIdTextField.text]) {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter valid email id."];

        return NO;
    }
    return YES;
}



#pragma mark - TextField delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
        
         if(textField == _mobileNoTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 10){
                        
                        return NO;
                }
                else{
                        return YES;
                }
        }
        
        return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - IBAction methods

- (IBAction)sendEmailBtnClicked:(id)sender {
     if([self validateEmail])
     {
             [self callSendMailWs];
     }
}

- (IBAction)goBackBtnAction:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)sendMobileNumberBtnClicked:(id)sender {
        if(_mobileNoTextField.text.length < 10)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                return;
        }
        [self generateEBPin];
}

- (IBAction)sendEBPinBtnClicked:(id)sender {
        if(_ebPinTextField.text.length == 0)
        {
                 [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter EB pin"];
                return;
        }
        
        if(![_ebPinTextField.text isEqualToString:ebPin])
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter correct EB pin"];
                return;
        }
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        ResetPasswordViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"ResetPasswordViewController"];
        vc.mobileNumStr = _mobileNoTextField.text;
        [self.navigationController pushViewController:vc animated:YES];
}



#pragma mark - custom methods
-(void) mobileValueChanged:(id)sender
{
        if(_mobileNoTextField.text.length == 10)
                [self checkMobileNumberExist];
}


-(void)checkMobileNumberExist
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",CHECK_MOBILE_WS,_mobileNoTextField.text] MathodName:CHECK_MOBILE_WS];
        }
}
-(void)callSendMailWs
{
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
                callLoginService.delegate =self;
                NSString *url = [NSString stringWithFormat:@"%@%@",FORGOT_PASSWORD_THROUGH_EMAIL_WS,_emailIdTextField.text];
                [callLoginService GetWebServiceWithURL:url MathodName:FORGOT_PASSWORD_THROUGH_EMAIL_WS];
                
        }
}


-(void)generateEBPin
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                NSString *url = [NSString stringWithFormat:@"%@%@",GENERATE_EB_PIN_WS,_mobileNoTextField.text];
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:url MathodName:@"GENERATE_EB_PIN_DATA"];
        }
}


#pragma mark - response method
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if([strName isEqualToString:FORGOT_PASSWORD_THROUGH_EMAIL_WS])
        {
                _emailIdLabel.text = [response valueForKey:@"message"];
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        _emailIdLabel.textColor = [UIColor colorWithRed:34/255.0f green:191/255.0f blue:100/255.0f alpha:1.0];
                }
                else
                        _emailIdLabel.textColor = [UIColor redColor];
   
        }
        else if ([strName isEqualToString:CHECK_MOBILE_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        _mobileNoLabel.textColor = [UIColor redColor];
                        _mobileNoLabel.text = [response valueForKey:@"message"];
                        _ebPinViewHeightConstraint.constant = 0;

                }
                else
                {
                        _mobileNoLabel.textColor = [UIColor grayColor];
                        _mobileNoLabel.text = @"Please click on SUBMIT button";
                }
                
        }
        else if ([strName isEqualToString:@"GENERATE_EB_PIN_DATA"]){
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        ebPin = [response valueForKey:@"ebpin"];
                        _mobileNoLabel.text = [response valueForKey:@"message"];
                        _ebPinViewHeightConstraint.constant = 120;
                        _sendMobileNumberBtnWidthConstraint.constant = 140;
                    [sendMobileNumberBtn setTitle:@"RESEND EB PIN" forState:UIControlStateNormal];
                }
                else
                         [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
        }

}

@end
