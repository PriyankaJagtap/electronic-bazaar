//
//  ResetPasswordViewController.h
//  EB
//
//  Created by webwerks on 06/11/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (nonatomic, strong) NSString *mobileNumStr;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)submitButtonAction:(id)sender;

@end
