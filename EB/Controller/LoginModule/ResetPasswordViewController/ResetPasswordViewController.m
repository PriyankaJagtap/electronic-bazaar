//
//  ResetPasswordViewController.m
//  EB
//
//  Created by webwerks on 06/11/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()<FinishLoadingData>

@end

@implementation ResetPasswordViewController
@synthesize submitBtn;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:submitBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - IBAction methods
- (IBAction)submitBtnClicked:(id)sender {
        
        if(_passwordTextField.text.length == 0)
        {
                   [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter new password"];
                return;
        }
        
        
        if(_confirmPasswordTextField.text.length == 0)
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter confirm password"];
                return;
        }
        else if(![_confirmPasswordTextField.text isEqualToString:_passwordTextField.text])
        {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Confirm Password does not match"];
                return;
        }
        
        [self callResetPasswordWebservice];
}
- (IBAction)backBtnClicked:(id)sender {
        [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - get resgisteration data
-(void)callResetPasswordWebservice
{
      
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
                callLoginService.delegate =self;
                NSString *url = [NSString stringWithFormat:@"%@%@&mobile=%@",RESET_PASSWORD_OTP_WS,_passwordTextField.text,_mobileNumStr];
                [callLoginService GetWebServiceWithURL:url MathodName:RESET_PASSWORD_OTP_WS];
                
        }
}

#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
         if ([strName isEqualToString:RESET_PASSWORD_OTP_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        alert.tag = 500;
                        [alert show];
                }
                else
                {
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                
        }
}

#pragma mark - Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
        if (alertView.tag == 500 && buttonIndex == 0)
        {
            APP_DELEGATE.isFromIBMLoginScreen ? [APP_DELEGATE navigateToIBMLoginViewController] : [APP_DELEGATE navigateToLoginViewController];
        }
}
- (IBAction)submitButtonAction:(id)sender {
}
@end
