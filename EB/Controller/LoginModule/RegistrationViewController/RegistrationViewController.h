//
//  RegistrationViewController.h
//  EB
//
//  Created by webwerks on 8/7/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FVCustomAlertView.h"
#import "Webservice.h"
#import "NIDropDown.h"
#import "AppDelegate.h"
#import "Constant.h"
@interface RegistrationViewController : UIViewController<FinishLoadingData,NIDropDownDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    
    __weak IBOutlet UIView *termsAndConditionBgView;
}

- (IBAction)termsAndConditionLinkPressed:(id)sender;


- (IBAction)closeButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;


@property (weak, nonatomic) IBOutlet UITextField *firstNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *ddTxt;
@property (weak, nonatomic) IBOutlet UITextField *mmTxt;
@property (weak, nonatomic) IBOutlet UITextField *yyyyTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;
@property (weak, nonatomic) IBOutlet UITextField *confirmPwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *mobileTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;
@property (weak, nonatomic) IBOutlet UIButton *mrBtn;
@property (weak, nonatomic) IBOutlet UIButton *msBtn;
@property (weak, nonatomic) IBOutlet UIButton *generateEBpinBtn;

@property (weak, nonatomic) IBOutlet UIButton *termsandconditionBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *accountAvailBtn;
@property (weak, nonatomic) IBOutlet UIButton *ddBtn;
@property (weak, nonatomic) IBOutlet UIButton *yyyyBtn;
@property (weak, nonatomic) IBOutlet UIButton *mmBtn;
@property (weak, nonatomic) IBOutlet UITextField *gstNumberTextField;

@property (weak, nonatomic) IBOutlet UITextField *panNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *ebPinSendLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ebPinLabelHt;

@property (strong,nonatomic)ShowTabbars *tabBarObj;


- (IBAction)dropDownDate:(id)sender;



- (IBAction)selectGenderAction:(id)sender;
- (IBAction)termsandconditionAction:(id)sender;
- (IBAction)submitAction:(id)sender;
- (IBAction)accountAvailAction:(id)sender;

//Hemant
@property (weak, nonatomic) IBOutlet UITextField *grpTextField;
@property (weak, nonatomic) IBOutlet UIButton *grpBtn;
@property (weak, nonatomic) IBOutlet UITextField *stateTextField;
@property (weak, nonatomic) IBOutlet UIButton *stateBtn;
@property (weak, nonatomic) IBOutlet UITextField *refByTextField;
@property (weak, nonatomic) IBOutlet UIButton *refByBtn;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *affStoreNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *telePhoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *pincodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *landmarkTextField;
@property (weak, nonatomic) IBOutlet UITextField *verificationCodeTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verificationCodeTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verificationCodeBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verificationCodeHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentViewHtConstraint;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *ebPINTextField;
@property (nonatomic) BOOL isFromMycartView;
@property (nonatomic) BOOL isFromHomeView;
@property (weak, nonatomic) IBOutlet UIButton *laptopRetailerBtn;
- (IBAction)retailerBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *mobileRetailerBtn;
@property (nonatomic, strong) NSDictionary *userdict;
@property (weak, nonatomic) IBOutlet UIButton *bothRetailerBtn;

@property (weak, nonatomic) IBOutlet UIButton *businessTypeBtn;


@end
