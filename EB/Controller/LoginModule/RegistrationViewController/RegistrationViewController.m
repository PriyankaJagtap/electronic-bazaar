//
//  RegistrationViewController.m
//  EB
//
//  Created by webwerks on 8/7/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "RegistrationViewController.h"
#import "HomeViewController.h"
#import "SlideMenuViewController.h"
#import "RightMenuViewController.h"
#import "LoginViewController.h"

#define MAX_LENGTH 10

#define LAPTOP_RETAILER @"laptop_retailer";
#define MOBILE_RETAILER @"mobile_retailer";
#define BOTH_RETAILER @"both";
#define BUSINESS_TYPE @"business_type";

@interface RegistrationViewController(){
        NSMutableArray *monthArr;
        NSMutableArray *yearArr;
        NSMutableArray *dateArr;
        
        NSArray *groupArray;
        NSArray *stateArray;
        NSArray *refByArray;
        
        
        // dropdown Object
        NIDropDown *dropdownDate;
        NIDropDown *dropdownMonth;
        NIDropDown *dropdownYear;
        
        NSString *genderVal;
        
        BOOL isTermsSelected;
        
        //hemant
        NIDropDown *groupDropDown;
        NIDropDown *stateDropDown;
        NIDropDown *refByDropDown;
        NIDropDown *businessTypeDropDown;
        
        NSString *ebPin;
        NSString *cityText;
        NSArray *businessTypeArr;
        
        NSString *businessType;
        
}
@end


@implementation RegistrationViewController
@synthesize scrollView,tabBarObj;
@synthesize firstNameTxt, lastNameTxt, ddTxt,mmTxt,yyyyTxt,mobileTxt, passwordTxt,confirmPwdTxt,emailTxt;
@synthesize grpTextField,stateTextField,addressTextField,cityTextField,refByTextField,affStoreNameTextField;
@synthesize ddBtn,mmBtn,yyyyBtn;
@synthesize landmarkTextField,verificationCodeTextField,verificationCodeHtConstraint,verificationCodeTopConstraint,verificationCodeBottomConstraint,scrollContentViewHtConstraint,pincodeTextField;
@synthesize userdict;




#pragma mark - Initialization

- (void)viewDidLoad {
        [super viewDidLoad];
        
        
        isTermsSelected = NO;
        [self.navigationController setNavigationBarHidden:YES];
        
       // [_businessTypeBtn.layer setCornerRadius:5.0];
        
         [[GradientColorClass sharedInstance] setGradientBackgroundToButton:_submitBtn];
        // Set Place holder color
        ebPin = @"";
        UIColor *color = [UIColor grayColor];
        NSString *str = @"First Name";
        [CommonSettings setAsterixToTextField:self.firstNameTxt withPlaceHolderName:str];
        self.firstNameTxt.attributedPlaceholder = [self String:str Range:(int)str.length];
        
        str = @"Store Name";
        self.nameTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.nameTextField withPlaceHolderName:str];
        
        str = @"EB PIN";
        self.ebPINTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.ebPINTextField withPlaceHolderName:str];
        
        str = @"Mobile No.";
        self.mobileTxt.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.mobileTxt withPlaceHolderName:str];
        _telePhoneNumberTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Telephone No. " attributes:@{NSForegroundColorAttributeName: color}];
        
        str = @"Email Id";
        self.emailTxt.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.emailTxt withPlaceHolderName:str];
        str = @"Password";
        self.passwordTxt.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.passwordTxt withPlaceHolderName:str];
        str = @"Confirm Password";
        self.confirmPwdTxt.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.confirmPwdTxt withPlaceHolderName:str];
        str = @"Pincode";
        self.pincodeTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        [CommonSettings setAsterixToTextField:self.pincodeTextField withPlaceHolderName:str];
        str = @"GST Identification Number";
        _gstNumberTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        //[CommonSettings setAsterixToTextField:_gstNumberTextField withPlaceHolderName:str];
        
        
        str = @"PAN Number";
        _panNumberTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        
        verificationCodeTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Verification Code" attributes:@{NSForegroundColorAttributeName: color}];
        mobileTxt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        
        pincodeTextField.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        
        mobileTxt.inputAccessoryView = [CommonSettings setToolBarNumberKeyBoard:self];
        
        [self createWrapView:landmarkTextField];
        [self createWrapView:_gstNumberTextField];
        [self createWrapView:_panNumberTextField];
        
        
        [self createWrapView:_nameTextField];
        [self createWrapView:pincodeTextField];
        [self createWrapView:verificationCodeTextField];
        
        //    verificationCodeTopConstraint.constant =0;
        //    verificationCodeHtConstraint.constant = 0;
        //    [self.view layoutIfNeeded];
        
        [pincodeTextField addTarget:self
                             action:@selector(pincodeValueChanged:)
                   forControlEvents:UIControlEventEditingChanged];
        
        [mobileTxt addTarget:self
                      action:@selector(mobileValueChanged:)
            forControlEvents:UIControlEventEditingChanged];
        
        // Gender val -- No val selected
        _mrBtn.selected = NO;
        _msBtn.selected = NO;
        genderVal = @"";
        
        
        
        
        // set wrap view for text field
        [self createWrapView:firstNameTxt];
        [self createWrapView:_telePhoneNumberTextField];
        
        [self createWrapView:lastNameTxt];
        [self createWrapView:ddTxt];
        [self createWrapView:mmTxt];
        [self createWrapView:yyyyTxt];
        [self createWrapView:mobileTxt];
        [self createWrapView:emailTxt];
        [self createWrapView:passwordTxt];
        [self createWrapView:confirmPwdTxt];
        
        //
        [self createWrapView:grpTextField];
        [self createWrapView:stateTextField];
        [self createWrapView:addressTextField];
        
        [self createWrapView:cityTextField];
        [self createWrapView:refByTextField];
        [self createWrapView:affStoreNameTextField];
        [self createWrapView:_ebPINTextField];
        
        str = @"Address";
        self.addressTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        //addressTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Address" attributes:@{NSForegroundColorAttributeName: color}];
        //    grpTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Affiliate" attributes:@{NSForegroundColorAttributeName: color}];
        
        grpTextField.text = @"Affiliate";
        
        str = @"City";
        self.cityTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        //    cityTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"City" attributes:@{NSForegroundColorAttributeName: color}];
        
        str = @"Aff. Store Name";
        self.affStoreNameTextField.attributedPlaceholder = [self String:str Range:(int)str.length];
        //    affStoreNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Aff. Store Name" attributes:@{NSForegroundColorAttributeName: color}];
        
        
        
        // add gesture to hide keyboard
        //    UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
        //    tapScroll.cancelsTouchesInView = NO;
        //    tapScroll.delegate=self;
        //    [self.view addGestureRecognizer:tapScroll];
        
        
        // Set values Dropdown
        
        monthArr = [NSMutableArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"April",@"May",@"June",@"July",@"Aug",@"Sept",@"Oct",@"Nov",@"Dec", nil];
        
        
        //----- Add year since from 2000 to 2024
        
        NSString *yearString = @"1970";
        yearArr=[[NSMutableArray alloc]init];
        for (int i=0; i<50; i++)
        {
                [yearArr addObject:[NSString stringWithFormat:@"%d",[yearString intValue]+i]];
        }
        
        //------ Add date 1 to 31
        dateArr=[[NSMutableArray alloc]init];
        for (int i=1; i<=31; i++)
        {
                [dateArr addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        if(_isFromMycartView)
        {
                [self setAllFieldValues];
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate.tabBarObj hideTabbar];
        }
        
        if(_isFromHomeView)
        {
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate.tabBarObj hideTabbar];
        }
}


-(NSAttributedString *)String:(NSString *)str Range:(int )Rannge
{
        NSMutableAttributedString *attString =
        [[NSMutableAttributedString alloc]
         initWithString:str];
        
        [attString addAttribute: NSForegroundColorAttributeName
                          value: [UIColor grayColor]
                          range: NSMakeRange(0,Rannge)];
        
        //    [attString addAttribute: NSForegroundColorAttributeName
        //                      value: [UIColor redColor]
        //                      range: NSMakeRange(Rannge,1)];
        
        
        return attString;
        
}

-(void)doneButtonDidPressed:(id)sender
{
        dispatch_async(dispatch_get_main_queue(), ^{
                [self.view endEditing:YES];
                
        });
}
-(void)setAllFieldValues
{
        firstNameTxt.text  = [userdict valueForKey:@"firstname"];
        lastNameTxt.text  = [userdict valueForKey:@"lastname"];
        mobileTxt.text  = [userdict valueForKey:@"mobile"];
        emailTxt.text  = [userdict valueForKey:@"email"];
        
}

-(void) mobileValueChanged:(id)sender
{
        if(mobileTxt.text.length == 10)
                [self checkMobileNumberExist];
}
-(void) pincodeValueChanged:(id)sender {
        // your code
        
        if(pincodeTextField.text.length  == 6)
        {
                [self getCityFromPincode];
        }
        else
        {
                _stateBtn.userInteractionEnabled = YES;
        }
}



-(void)checkMobileNumberExist
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",CHECK_MOBILE_WS,mobileTxt.text] MathodName:CHECK_MOBILE_WS];
                
        }
}


-(void)checkEmailExist
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:[NSString stringWithFormat:@"%@%@",CHECK_EMAIL_WS,emailTxt.text] MathodName:CHECK_EMAIL_WS];
                
        }
        
}
-(void)getCityFromPincode
{
        
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.view withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                
                NSDictionary *userdata = [[NSDictionary alloc] initWithObjectsAndKeys:pincodeTextField.text,@"pincode", nil];
                callLoginService.delegate =self;
                [callLoginService operationRequestToApi:userdata url:GET_CITY_FROM_PINCODE string:@"GET_CITY_FROM_PINCODE"];
                
        }
}
-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:animated];
        isTermsSelected = NO;
        [self.navigationController setNavigationBarHidden:YES];
        
}


-(void)viewDidAppear:(BOOL)animated
{
        [super viewDidAppear:animated];
        [self getRegisterationData];
        
}

#pragma mark - manage UX method


-(void)createWrapView:(UITextField *)textfield{
        UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, textfield.frame.size.height)];
        textfield.leftViewMode = UITextFieldViewModeAlways;
        textfield.leftView = wrapView;
}





#pragma mark - UITextField Delegate

-(void)hideKeyBoard:(UIGestureRecognizer *) sender
{
        //    if (dropdownDate || dropdownMonth || dropdownYear) {
        //        if (dropdownDate) {
        //            [dropdownDate hideDropDown:ddBtn];
        //            [dropdownDate removeFromSuperview];
        //        }
        //        else if (dropdownMonth){
        //            [dropdownMonth hideDropDown:mmBtn];
        //            [dropdownMonth removeFromSuperview];
        //        }
        //        else if (dropdownYear){
        //            [dropdownYear hideDropDown:yyyyBtn];
        //            [dropdownYear removeFromSuperview];
        //        }
        //    }
        [self hideAllDropDown];
        //[scrollView setContentOffset:CGPointMake(scrollView.frame.origin.x, 0) animated:YES];
        
        [self.view endEditing:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
        if ([touch.view isDescendantOfView:dropdownDate]||[touch.view isDescendantOfView:dropdownMonth]||[touch.view isDescendantOfView:dropdownYear]) {
                
                // Don't let selections of auto-complete entries fire the
                // gesture recognizer
                return NO;
        }
        
        return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
        //[scrollView setContentOffset:CGPointMake(scrollView.frame.origin.x, 0) animated:YES];
        
        [textField resignFirstResponder];
        return  YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
        //    if(textField == mobileTxt)
        //    {
        //        if (![self validatePhoneNumber:mobileTxt.text]) {
        //            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please add a valid Mobile Number"];
        //            return;
        //        }
        //    }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                //                if(textField == mobileTxt)
                //                {
                //                        if(textField.text.length < 10)
                //                        {
                //                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                //                                [alert show];
                //                        }
                //                }
                //                else
                if (textField == emailTxt)
                {
                        //                                if (emailTxt.text.length == 0) {
                        //                                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
                        //                                        return;
                        //                                }
                        
                        if ([self validEmail:emailTxt.text]) {
                                [self checkEmailExist];
                        }
                        //                                if (![self validEmail:emailTxt.text]) {
                        //                                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
                        //                                        return;
                        //
                        //                                }
                        //                                else
                        //                                {
                        //                                        [self checkEmailExist];
                        //                                }
                }
                
        });
        
        
        [scrollView setContentOffset:CGPointMake(scrollView.frame.origin.x, 0) animated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
        
        [self hideAllDropDown];
        
        if(textField == mobileTxt)
        {
                if(_nameTextField.text.length == 0)
                {
                        [textField resignFirstResponder];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter store name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [alert show];
                        // [_nameTextField becomeFirstResponder];
                }
        }
        else if (textField == _ebPINTextField)
        {
                if(_nameTextField.text.length == 0)
                {
                        [textField resignFirstResponder];
                        [self displayAlertForPreviousFields];
                        return;
                }
                if(mobileTxt.text.length < 10)
                {
                        [textField resignFirstResponder];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:MOBILE_VALIDATION_TEXT delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [alert show];
                        
                        //[mobileTxt becomeFirstResponder];
                        //return;
                }
        }
        else if (textField == emailTxt)
        {
                if(_nameTextField.text.length == 0 || mobileTxt.text.length == 0)
                {
                        [textField resignFirstResponder];
                        [self displayAlertForPreviousFields];
                        return;
                }
                
                if(_ebPINTextField.text.length == 0)
                {
                        [textField resignFirstResponder];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter EB PIN." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [alert show];
                        
                        // [_ebPINTextField becomeFirstResponder];
                }
        }
        
        else if (textField == pincodeTextField)
        {
                if(_nameTextField.text.length == 0 || mobileTxt.text.length == 0 || _ebPINTextField.text.length == 0)
                {
                        [textField resignFirstResponder];
                        
                        [self displayAlertForPreviousFields];
                        
                        return;
                }
                
                if(emailTxt.text.length == 0)
                {
                        [textField resignFirstResponder];
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter email." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [alert show];
                        
                        // [emailTxt becomeFirstResponder];
                }
                else if (![self validEmail:emailTxt.text])
                {
                        [textField resignFirstResponder];
                        
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
                        return;
                        
                }
                
        }
        else if (textField == passwordTxt)
        {
                if(_nameTextField.text.length == 0 || mobileTxt.text.length == 0 || _ebPINTextField.text.length == 0 || emailTxt.text.length == 0 || pincodeTextField.text.length == 0)
                {
                        [textField resignFirstResponder];
                        
                        [self displayAlertForPreviousFields];
                        return;
                }
                
                
                
                if ([[_refByBtn titleForState:UIControlStateNormal] isEqualToString:[[refByArray objectAtIndex:0] valueForKey:@"asm_name"]])
                {
                        [textField resignFirstResponder];
                        
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please select Referred by."];
                }
                if(pincodeTextField.text.length == 0)
                {
                        [textField resignFirstResponder];
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter pincode." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [alert show];
                        // [pincodeTextField becomeFirstResponder];
                }
        }
        
        else if (textField == confirmPwdTxt)
        {
                
                if(_nameTextField.text.length == 0 || mobileTxt.text.length == 0 || _ebPINTextField.text.length == 0 || emailTxt.text.length == 0 || pincodeTextField.text.length == 0 || [[_refByBtn titleForState:UIControlStateNormal] isEqualToString:[[refByArray objectAtIndex:0] valueForKey:@"asm_name"]])
                {
                        [textField resignFirstResponder];
                        
                        [self displayAlertForPreviousFields];
                        return;
                }
                
                
                if(passwordTxt.text.length == 0)
                {
                        [textField resignFirstResponder];
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter password." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [alert show];
                        //[passwordTxt becomeFirstResponder];
                }
        }
        else if (textField == _gstNumberTextField)
        {
                if(_nameTextField.text.length == 0 || mobileTxt.text.length == 0 || _ebPINTextField.text.length == 0 || emailTxt.text.length == 0 || pincodeTextField.text.length == 0 || passwordTxt.text.length == 0|| [[_refByBtn titleForState:UIControlStateNormal] isEqualToString:[[refByArray objectAtIndex:0] valueForKey:@"asm_name"]] || confirmPwdTxt.text.length == 0)
                {
                        [textField resignFirstResponder];
                        
                        [self displayAlertForPreviousFields];
                        return;
                }
                
                
                if ([[_businessTypeBtn titleForState:UIControlStateNormal] isEqualToString:[[businessTypeArr objectAtIndex:0] valueForKey:@"label"]])
                {
                        [textField resignFirstResponder];
                        
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please select Business Type."];
                }
                
        }
        else if (textField == _panNumberTextField)
        {
                if(_nameTextField.text.length == 0 || mobileTxt.text.length == 0 || _ebPINTextField.text.length == 0 || emailTxt.text.length == 0 || pincodeTextField.text.length == 0 || passwordTxt.text.length == 0|| confirmPwdTxt.text.length == 0 || [[_refByBtn titleForState:UIControlStateNormal] isEqualToString:[[refByArray objectAtIndex:0] valueForKey:@"asm_name"]] || [[_businessTypeBtn titleForState:UIControlStateNormal] isEqualToString:[[businessTypeArr objectAtIndex:0] valueForKey:@"label"]])
                {
                        [textField resignFirstResponder];
                        [self displayAlertForPreviousFields];
                        return;
                }
                
                if (_gstNumberTextField.text.length != 0){
                        if(_gstNumberTextField.text.length < 15)
                        {
                                [textField resignFirstResponder];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please provide valid GST Number" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                [alert show];
                                // [_gstNumberTextField becomeFirstResponder];
                        }
                }
        }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if(textField == _nameTextField)
    {
        NSUInteger newLength = [textField.text length] + [string length];
        
        if(newLength > 30){
            
            return NO;
        }
        else
        {
            return YES;
        }
    }
        else if(textField == emailTxt)
        {
                return YES;
        }
        else if(textField == mobileTxt)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > MAX_LENGTH){
                        
                        return NO;
                }
                else{
                        return YES;
                }
        }
        else if(textField == _gstNumberTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_GST_NUMBER_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                if(newLength <= 15)
                {
                        return [string isEqualToString:filtered];
                }
                
                if(newLength > 15){
                        
                        return NO;
                }
        }
        else if (textField == _panNumberTextField)
        {
                
                NSUInteger newLength = [textField.text length] + [string length];
                NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_PAN_NUMBER_CHARACTERS] invertedSet];
                
                NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
                if(newLength <= 10)
                {
                        return [string isEqualToString:filtered];
                }
                
                if(newLength > 10){
                        
                        return NO;
                }
                
        }
        else if(textField == pincodeTextField)
        {
                NSUInteger newLength = [textField.text length] + [string length];
                
                if(newLength > 6){
                        
                        return NO;
                }
                else{
                        cityTextField.text = nil;
                        return YES;
                }
        }
        else
        {
                if(range.length + range.location > textField.text.length)
                {
                        return NO;
                }
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return newLength <= 25;
        }
        
        return YES;
}




- (IBAction)selectGenderAction:(id)sender {
        if ([sender tag] == 10) {
                _mrBtn.selected = YES;
                _msBtn.selected = NO;
                genderVal = @"Mr.";
        }
        else if([sender tag] == 20){
                _msBtn.selected = YES;
                _mrBtn.selected = NO;
                genderVal = @"Ms.";
        }
        
}

- (IBAction)termsandconditionAction:(id)sender {
        
        if ([sender tag] == 111)
        {
                _termsandconditionBtn.selected = !_termsandconditionBtn.selected;
                isTermsSelected = ! isTermsSelected;
        }
        // [scrollView setContentOffset:CGPointMake(0, scrollView.contentOffset.y)];
        
}



- (IBAction)submitAction:(id)sender {
        
        [self.view endEditing:NO];
        
        if (_nameTextField.text.length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter store name."];
                return;
        }
        
        
        if (mobileTxt.text){
                if (mobileTxt.text.length < 10) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit Mobile Number"];
                        return;
                }
        }
        if (_ebPINTextField.text.length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter EB PIN."];
                return;
        }
        
        if (emailTxt.text){
                
                if (emailTxt.text.length == 0) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter email."];
                        return;
                }
                if (![self validEmail:emailTxt.text]) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check email format."];
                        return;
                        
                }
        }
        
        if (pincodeTextField.text.length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter pincode."];
                return;
        }
        
        if (cityTextField.text.length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter valid pincode."];
                return;
        }
        
        
        if ([_refByBtn titleForState:UIControlStateNormal].length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please select Referred by."];
                return;
        }
        else if ([[_refByBtn titleForState:UIControlStateNormal] isEqualToString:[[refByArray objectAtIndex:0] valueForKey:@"asm_name"]])
        {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please select Referred by."];
                return;
        }
        
        if (passwordTxt.text)
        {
                if (passwordTxt.text.length == 0) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter password."];
                        return;
                }
                if (confirmPwdTxt.text.length == 0) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter confirm password."];
                        return;
                }
                if (![passwordTxt.text isEqual:confirmPwdTxt.text]) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Confirm Password does not match"];
                        return;
                        
                }
                
        }
        
        
        if ([_businessTypeBtn titleForState:UIControlStateNormal].length == 0){
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please select Business Type."];
                return;
        }
        else if ([[_businessTypeBtn titleForState:UIControlStateNormal] isEqualToString:[[businessTypeArr objectAtIndex:0] valueForKey:@"label"]])
        {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please select Business Type."];
                return;
        }
        
        if (_gstNumberTextField.text.length != 0){
                if (_gstNumberTextField.text.length < 15) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide valid GST Number"];
                        return ;
                }
        }
        
        //    if (_panNumberTextField.text.length == 0){
        //        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter PAN number."];
        //        return;
        //    }else if(_panNumberTextField.text.length < 10)
        //    {
        //        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit PAN number."];
        //        return;
        //    }
        
        if (_panNumberTextField.text.length != 0){
                
                if (![CommonSettings validatePanCardNumber:_panNumberTextField.text]) {
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please check PAN  Number format."];
                        return;
                        
                }
        }
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        // Call webservice
        NSMutableDictionary * dict = [NSMutableDictionary new];
        
        NSString *refBy=[[[refByArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(asm_name == %@)", [_refByBtn titleForState:UIControlStateNormal]]] objectAtIndex:0] valueForKey:@"asm_id"];
        
        [dict setObject:refBy forKey:@"asm_id"];
        //[dict setObject:addressTextField.text forKey:@"landmark"];
        [dict setObject:cityTextField.text forKey:@"cus_city"];
        [dict setObject:emailTxt.text forKey:@"email"];
        [dict setObject:passwordTxt.text forKey:@"password"];
        [dict setObject:@"iphone" forKey:@"device_type"];
        NSString *pushID=[defaults valueForKey:@"DEVICE_TOKEN"];
        [dict setObject:pushID?:@"" forKey:@"push_id"];
        [dict setObject:_nameTextField.text forKey:@"name"];
        [dict setObject:_nameTextField.text forKey:@"store_name"];
        [dict setObject:mobileTxt.text forKey:@"mobile"];
        [dict setObject:pincodeTextField.text forKey:@"pincode"];
        NSString *businessTypeBtnTitile = [_businessTypeBtn titleForState:UIControlStateNormal];
        NSArray *filterBusinessArr =[businessTypeArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(label == %@)",businessTypeBtnTitile ]];
        
        businessType = [NSString stringWithFormat:@"%@",[[filterBusinessArr objectAtIndex:0] valueForKey:@"id"]];
        [dict setObject:businessType forKey:@"businessType"];
        [dict setObject:_ebPINTextField.text forKey:@"EB_PIN_USER"];
        [dict setObject:ebPin forKey:@"EB_PIN_SERVER"];
        [dict setObject:_gstNumberTextField.text forKey:@"gstin"];
        [dict setObject:_panNumberTextField.text forKey:@"taxvat"];
        NSLog(@"registeration dic %@",dict);
        [self callRegistrationWS:dict];
}

-(void)displayAlertForPreviousFields
{
        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter previous fields."];
        
}
//hemant
#pragma mark - IBAction methods

- (IBAction)retailerBtnClicked:(id)sender {
        
        UIButton *btn = (UIButton *)sender;
        
        if(_nameTextField.text.length == 0 || mobileTxt.text.length == 0 || _ebPINTextField.text.length == 0 || emailTxt.text.length == 0 || pincodeTextField.text.length == 0 || passwordTxt.text.length == 0|| [[_refByBtn titleForState:UIControlStateNormal] isEqualToString:[[refByArray objectAtIndex:0] valueForKey:@"asm_name"]])
        {
                [self displayAlertForPreviousFields];
                return;
        }
        
        if(confirmPwdTxt.text.length == 0)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter confirm password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                return;
        }
        
        
        if(businessTypeArr.count != 0)
        {
                NSArray * arr = [businessTypeArr valueForKey:@"label"];
                if(businessTypeDropDown == nil){
                        CGFloat f= 100.0;
                        if(arr.count <= 3)
                                f = 30 * arr.count;
                        businessTypeDropDown = [[NIDropDown alloc] showDropDown:btn withHeight:&f withData:arr withImages:Nil WithDirection:@"down"];
                        businessTypeDropDown.delegate = self;
                        
                }
                else{
                        [businessTypeDropDown hideDropDown:sender];
                        [self rel];
                }
                
        }
        
}
- (IBAction)stateBtnClicked:(id)sender {
        [groupDropDown hideDropDown:_grpBtn];
        [groupDropDown removeFromSuperview];
        groupDropDown = nil;
        
        [refByDropDown hideDropDown:_refByBtn];
        [refByDropDown removeFromSuperview];
        refByDropDown = nil;
        [self.view endEditing:NO];
        
        [self hideAllDateDropDown];
        NSArray * arr = [stateArray valueForKey:@"cus_state_value"];
        _stateBtn.tag = 511;
        if(stateDropDown == nil){
                CGFloat f= 100.0;
                if(arr.count <= 3)
                        f = 30 * arr.count;
                stateDropDown = [[NIDropDown alloc] showDropDown:_stateBtn withHeight:&f withData:arr withImages:Nil WithDirection:@"down"];
                stateDropDown.delegate = self;
                stateDropDown.displayDropDownBool = YES;
                
        }
        else{
                [stateDropDown hideDropDown:sender];
                [self rel];
        }
}
- (IBAction)generateEBPinBtnClicked:(id)sender {
        [self.view endEditing:YES];
        [self performSelector:@selector(display)  withObject:nil afterDelay:1.0];
        
        
}
-(void) display{
        //    if (mobileTxt.text.length == 0) {
        //        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please enter Mobile Number."];
        //        return;
        //    }
        //    if (![self validatePhoneNumber:mobileTxt.text]) {
        
        if(mobileTxt.text.length < 10)
        {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Please provide 10 Digit Mobile Number"];
                
        }
        else
        {
                [self generateEBPin];
        }
}

- (IBAction)grpBtnClicked:(id)sender {
        [stateDropDown hideDropDown:_stateBtn];
        [stateDropDown removeFromSuperview];
        stateDropDown = nil;
        
        [refByDropDown hideDropDown:_refByBtn];
        [refByDropDown removeFromSuperview];
        refByDropDown = nil;
        
        [self.view endEditing:NO];
        
        [self hideAllDateDropDown];
        _grpBtn.tag = 510;
        NSArray * arr = [groupArray valueForKey:@"group_name"];
        if(groupDropDown == nil){
                CGFloat f= 100.0;
                if(arr.count <= 3)
                        f = 30 * arr.count;
                
                groupDropDown = [[NIDropDown alloc] showDropDown:_grpBtn withHeight:&f withData:arr withImages:Nil WithDirection:@"down"];
                groupDropDown.delegate = self;
                groupDropDown.displayDropDownBool = YES;
                
        }
        else{
                [groupDropDown hideDropDown:sender];
                [self rel];
        }
}
- (IBAction)refByBtnClicked:(id)sender {
        
        
        if(_nameTextField.text.length == 0 || mobileTxt.text.length == 0 || _ebPINTextField.text.length == 0 || emailTxt.text.length == 0 )
        {
                [self displayAlertForPreviousFields];
                return;
        }
        
        if(pincodeTextField.text.length == 0)
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter pincode." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                return;
        }
        
        NSArray * arr = [refByArray valueForKey:@"asm_name"];
        _refByBtn.tag = 512;
        if(refByDropDown == nil){
                CGFloat f= 100.0;
                if(arr.count <= 3)
                        f = 30 * arr.count;
                refByDropDown = [[NIDropDown alloc] showDropDown:_refByBtn withHeight:&f withData:arr withImages:Nil WithDirection:@"down"];
                
                refByDropDown.displayDropDownBool = YES;
                refByDropDown.delegate = self;
                
        }
        else{
                [refByDropDown hideDropDown:sender];
                [self rel];
        }
}


-(void)hideAllDropDown
{
        [self hideAllDateDropDown];
        [self hideAllGroupDropDown];
}

-(void)hideAllDateDropDown
{
        [dropdownDate hideDropDown:ddBtn];
        [dropdownDate removeFromSuperview];
        dropdownDate = nil;
        
        [dropdownMonth hideDropDown:mmBtn];
        [dropdownMonth removeFromSuperview];
        dropdownMonth = nil;
        
        
        [dropdownYear hideDropDown:yyyyBtn];
        [dropdownYear removeFromSuperview];
        dropdownYear = nil;
        
}

-(void)hideAllGroupDropDown
{
        [groupDropDown hideDropDown:_grpBtn];
        [groupDropDown removeFromSuperview];
        groupDropDown = nil;
        
        [stateDropDown hideDropDown:_stateBtn];
        [stateDropDown removeFromSuperview];
        stateDropDown = nil;
        
        [refByDropDown hideDropDown:_refByBtn];
        [refByDropDown removeFromSuperview];
        refByDropDown = nil;
}

-(void)displayData: (UIButton *)sender withTitle:(NSString *)title
{
        switch (sender.tag) {
                case 510:
                        grpTextField.text = title;
                        break;
                case 511:
                        stateTextField.text = title;
                        break;
                case 512:
                        refByTextField.text = title;
                        break;
                default:
                        break;
        }
}
#pragma mark - Email/ Phone Validation

-(BOOL)validEmail:(NSString*)emailString
{
        NSString *emailid = emailString;
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        BOOL myStringMatchesRegEx=[emailTest evaluateWithObject:emailid];
        
        if(myStringMatchesRegEx)
        {
                return YES;
        }
        else
        {
                return NO;
        }
}

-(BOOL) validatePhoneNumber:(NSString *)enterNumber
{
        NSString *phoneRegex = @"[12356789][0-9]{6}([0-9]{3})?";
        NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
        BOOL matches = [test evaluateWithObject:enterNumber];
        return matches;
}

#pragma mark - Account available

- (IBAction)accountAvailAction:(id)sender {
        //[self.navigationController popToRootViewControllerAnimated:YES];
        if(_isFromMycartView || _isFromHomeView)
        {
                UIStoryboard *storyBoard;
                //        if (isIpad){
                //            storyBoard=[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
                //        }else{
                storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
                //}
                LoginViewController *objLoginVc =[ self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:objLoginVc animated:YES];
        }
        else
                [self.navigationController popViewControllerAnimated:YES];
        
}


#pragma mark - NIDropdown

- (void)rel
{
        dropdownDate = nil;
        dropdownMonth = nil;
        dropdownYear = nil;
        groupDropDown = nil;
        refByDropDown = nil;
        stateDropDown = nil;
        businessTypeDropDown = nil;
}

- (void)niDropDownDelegateMethod:(NIDropDown *) sender
{
        [self rel];
        
}

- (void) getindexValue: (NIDropDown *)sender withindex:(NSIndexPath*)index{
        DLog(@"index %ld",(long)index.row);
        
        //    if(sender.tag == 11){
        //        if (dateArr.count > 0) {
        //            NSString *strDay = [NSString stringWithFormat:@"%@",[dateArr objectAtIndex:(long)index.row]];
        //            ddTxt.text = strDay;
        //        }
        //    }
        //    else if (sender.tag == 22 ){
        //        if (monthArr.count > 0) {
        //            NSString *strMonth = [NSString stringWithFormat:@"%@",[monthArr objectAtIndex:(long)index.row]];
        //            mmTxt.text = strMonth;
        //        }
        //    }
        //    else if (sender.tag == 33 ){
        //        if (yearArr.count > 0) {
        //            NSString *strYear = [NSString stringWithFormat:@"%@",[yearArr objectAtIndex:(long)index.row]];
        //            yyyyTxt.text = strYear;
        //        }
        //    }
        
}



- (IBAction)dropDownDate:(id)sender {
        /* if (dropdownDate || dropdownMonth || dropdownYear) {
         if (dropdownDate) {
         [dropdownDate hideDropDown:sender];
         [dropdownDate removeFromSuperview];
         }
         else if (dropdownMonth){
         [dropdownMonth hideDropDown:sender];
         [dropdownMonth removeFromSuperview];
         }
         else if (dropdownYear){
         [dropdownYear hideDropDown:sender];
         [dropdownYear removeFromSuperview];
         }
         }*/
        [self.view endEditing:NO];
        if(scrollView.contentOffset.y < 100)
                [scrollView setContentOffset:CGPointMake(0, 100) animated:TRUE];
        
        
        //    UIButton *btn = (UIButton *)sender;
        //    CGRect selectedViewFrame = [sender convertRect:btn.bounds toView:nil];
        //
        //    if((selectedViewFrame.origin.y + selectedViewFrame.size.height) >= 100 )
        //    {
        //        [scrollView setContentOffset:CGPointMake(0,scrollView.contentOffset.y +(selectedViewFrame.origin.y -100)+selectedViewFrame.size.height) animated: true];
        //    }
        [self hideAllGroupDropDown];
        
        // --- day val
        if ([sender tag] == 11) {
                [dropdownMonth hideDropDown:sender];
                [dropdownMonth removeFromSuperview];
                dropdownMonth = nil;
                
                [dropdownYear hideDropDown:sender];
                [dropdownYear removeFromSuperview];
                dropdownYear = nil;
                
                if(dropdownDate == nil){
                        
                        CGFloat f= 100.0;
                        dropdownDate = [[NIDropDown alloc] showDropDown:sender withHeight:&f withData:dateArr withImages:Nil WithDirection:@"down"];
                        dropdownDate.tag = 11 ;
                        dropdownDate.delegate = self;
                }
                else{
                        [dropdownDate hideDropDown:sender];
                        [self rel];
                }
        }
        
        // --- month val
        else if ([sender tag] == 22){
                
                [dropdownDate hideDropDown:sender];
                [dropdownDate removeFromSuperview];
                dropdownDate = nil;
                
                [dropdownYear hideDropDown:sender];
                [dropdownYear removeFromSuperview];
                dropdownYear = nil;
                
                if(dropdownMonth == nil){
                        CGFloat f= 100.0;
                        dropdownMonth = [[NIDropDown alloc] showDropDown:sender withHeight:&f withData:monthArr withImages:Nil WithDirection:@"down"];
                        dropdownMonth.tag = 22 ;
                        dropdownMonth.delegate = self;
                }
                else{
                        [dropdownMonth hideDropDown:sender];
                        [self rel];
                }
        }
        
        // --- year val
        else if ([sender tag] == 33){
                
                [dropdownDate hideDropDown:sender];
                [dropdownDate removeFromSuperview];
                dropdownDate = nil;
                
                [dropdownMonth hideDropDown:sender];
                [dropdownMonth removeFromSuperview];
                dropdownMonth = nil;
                
                if(dropdownYear == nil){
                        CGFloat f= 100.0;
                        dropdownYear = [[NIDropDown alloc] showDropDown:sender withHeight:&f withData:yearArr withImages:Nil WithDirection:@"down"];
                        dropdownYear.tag = 33 ;
                        dropdownYear.delegate = self;
                }
                else{
                        [dropdownYear hideDropDown:sender];
                        [self rel];
                }
        }
        else{
                // handle the dropdown
        }
}


#pragma mark - Navigate to homescreen
//-(void)navigateToHomeViewcontroller
//{
//    // Set Firsttime value
//    [[NSUserDefaults standardUserDefaults] setObject:@"FirstTime" forKey:@"FirstTime"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
//
//    // Navigate from Login screen t home screen.
//
//    UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//
//    UINavigationController *navigationController ;
//    HomeViewController *homeObjc;
//    SlideMenuViewController *objMainMenuVC;
//    RightMenuViewController *objRightMenuVC;
//    homeObjc = [storyBoard instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    navigationController = [[UINavigationController alloc] initWithRootViewController:homeObjc];
//
//    objMainMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"SlideMenuViewController"];
//    objMainMenuVC.slideMenuDelegate = homeObjc;
//
//    objRightMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"RightMenuViewController"];
//    objRightMenuVC.rightMenuDelegate = homeObjc;
//
//    MFSideMenuContainerViewController *container =[MFSideMenuContainerViewController
//                                                   containerWithCenterViewController:navigationController
//                                                   leftMenuViewController:objMainMenuVC
//                                                   rightMenuViewController:objRightMenuVC];
//
//    [[[UIApplication sharedApplication]delegate].window setRootViewController:container];
//    [navigationController setNavigationBarHidden:YES animated:YES];
//}

-(void)navigateToHomeViewcontroller
{
        // Set Firsttime value
        [[NSUserDefaults standardUserDefaults] setObject:@"FirstTime" forKey:@"FirstTime"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        // Navigate from Login screen t home screen.
        UIStoryboard *storyBoard;
        //    if (isIpad){
        //        storyBoard=[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        //    }else{
        storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //}
        UINavigationController *navigationController ;
        
        self.tabBarObj = [[ShowTabbars alloc] init];
        //    AppDelegate *appdelegate=[[UIApplication sharedApplication]delegate];
        //
        //    [appdelegate.tabBarObj showTabbars];
        [self.tabBarObj showTabbars];
        [self.tabBarObj TabClickedIndex:0];
        
        SlideMenuViewController *  objMainMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"SlideMenuViewController"];
        
        RightMenuViewController*  objRightMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"RightMenuViewController"];
        
        MFSideMenuContainerViewController *container =[MFSideMenuContainerViewController
                                                       containerWithCenterViewController:self.tabBarObj
                                                       leftMenuViewController:objMainMenuVC
                                                       rightMenuViewController:objRightMenuVC];
        
        [AppDelegate getAppDelegateObj].navigationController=navigationController;
        [AppDelegate getAppDelegateObj].tabBarObj=self.tabBarObj;
        [[[UIApplication sharedApplication]delegate].window setRootViewController:container];
        [navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - RegisterWS

-(void)callRegistrationWS:(NSMutableDictionary*)userdata{
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@""withBlur:NO allowTap:NO];
        [userdata setValue:[CommonSettings getAppCurrentVersion] forKey:@"version"];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService operationRequestToApi:userdata url:REGISTRATION_WS string:@"RegistrationWS"];
        }
        
}

-(void)receivedResponseForRegistration:(id)receiveData stringResponse:(NSString *)responseType
{
        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
        if ([responseType isEqualToString:@"success"]) {
                NSData *receiveLoginData = [NSData dataWithData:receiveData];
                id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
                NSMutableDictionary *dictUserDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
                
                DLog(@"userDetail %@",jsonObject);
                
                if ([dictUserDetail isKindOfClass:[NSDictionary class]]) {
                        NSString *status = [NSString stringWithFormat:@"%ld",(long)[[dictUserDetail objectForKey:@"status"]integerValue]];
                        if ([status isEqualToString:@"1"])
                        {
                                
                                [[NSUserDefaults standardUserDefaults] setObject:[[dictUserDetail objectForKey:@"user"]objectForKey:@"id"] forKey:@"user_id"];
                                
                                [[NSUserDefaults standardUserDefaults]setObject:[dictUserDetail valueForKey:@"customer_id"] forKey:EB_CUSTOMER_ID];
                                NSMutableDictionary *dictData = [NSMutableDictionary new];
                                [dictData setObject:[[dictUserDetail objectForKey:@"user"]objectForKey:@"id"] forKey:@"id"];
                                [dictData setObject:[[dictUserDetail objectForKey:@"user"]objectForKey:@"firstname"] forKey:@"firstname"];
                                [dictData setObject:[[dictUserDetail objectForKey:@"user"]objectForKey:@"lastname"] forKey:@"lastname"];
                                NSString *uid =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"id"]];
                                
                                [dictData setObject:[[dictUserDetail objectForKey:@"user"]objectForKey:@"mobile"] forKey:@"mobile"];
                                [dictData setObject:emailTxt.text forKey:@"email"];
                                [dictData setObject:@"" forKey:@"profile_picture"];
                                [dictData setObject:@"" forKey:@"dob"];
                                [dictData setObject:uid?uid:@"" forKey:@"user_id"];
                                
                                
                                NSString *businessType = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"business_type"]];
                                
                                if(![businessType isEqualToString:@""])
                                        [dictData setValue:businessType forKey:@"business_type"];
                                
                                NSString *gstin = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"gstin"]];
                                if(![gstin isEqualToString:@""])
                                {
                                        [dictData setValue:gstin forKey:@"gstin"];
                                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:NO] forKey:SHOW_GSTIN];
                                }
                                else
                                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:YES] forKey:SHOW_GSTIN];
                                
                                
                                if([[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"] && ![[[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"] isKindOfClass:[NSNull class]] && ![[[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"] isEqualToString:@""])
                                {
                                        NSString *taxVat = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"]];
                                        [dictData setValue:taxVat forKey:@"taxvat"];
                                }
                                
                                
                                NSString *referredBy =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"referred_by"]];
                                if(![referredBy isEqualToString:@""])
                                        [dictData setValue:referredBy forKey:@"referred_by"];
                                
                                //                if(![[[dictUserDetail valueForKey:@"user"] valueForKey:@"pincode"] isKindOfClass:[NSNull class]])
                                
                                if(![[[dictUserDetail valueForKey:@"user"] valueForKey:@"pincode"] isEqualToString:@""])
                                {
                                        [dictData setValue:[[dictUserDetail valueForKey:@"user"] valueForKey:@"pincode"] forKey:@"pincode"];
                                }
                                
                                
                                if(![[[dictUserDetail valueForKey:@"user"] valueForKey:@"store_name"] isKindOfClass:[NSNull class]])
                                {
                                        [dictData setValue:[[dictUserDetail valueForKey:@"user"] valueForKey:@"store_name"] forKey:@"storeName"];
                                }
                                
                                
                                NSString *quote_ID = [NSString stringWithFormat:@"%d",[[dictUserDetail objectForKey:@"quoteId"]intValue]];
                                [[NSUserDefaults standardUserDefaults]setObject:quote_ID forKey:@"QuoteID"];
                                
                                [[NSUserDefaults standardUserDefaults]setObject:dictData forKey:@"user"];
                                [[NSUserDefaults standardUserDefaults]synchronize];
                                /*
                                 */
                                [self navigateToHomeViewcontroller];
                                
                        }
                        else if ([status isEqualToString:@"0"]){
                                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:[dictUserDetail objectForKey:@"message"]];
                        }
                }
                else{
                        [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Something went wrong with the service."];
                }
        }
        
}

-(void)receivedResponseForCityData:(id)receiveData stringResponse:(NSString*)responseType
{
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        NSMutableDictionary *dictUserDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
        
        NSLog(@"userDetail %@",jsonObject);
        if([[dictUserDetail valueForKey:@"status"] boolValue])
        {
                cityTextField.text = [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"city"];
                stateTextField.text = [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"default_name"];
                //_stateBtn.userInteractionEnabled = false;
                
                if([[[dictUserDetail valueForKey:@"address_data"] valueForKey:@"city"] isKindOfClass:[NSNull class]] ||[[[dictUserDetail valueForKey:@"address_data"] valueForKey:@"default_name"] isKindOfClass:[NSNull class]] ||[ [[dictUserDetail valueForKey:@"address_data"] valueForKey:@"country_code"] isKindOfClass:[NSNull class]] )
                {
                        cityTextField.text = @"";
                        stateTextField.text = @"";
                }
        }
        else
        {
                [APP_DELEGATE showAlert:@"" withMessage:[dictUserDetail valueForKey:@"message"]];
                //_stateBtn.userInteractionEnabled = true;
                cityTextField.text = nil;
        }
        //    userDetail {
        //        "address_data" =     {
        //            city = PUNE;
        //            "country_code" = IN;
        //            "default_name" = Maharashtra;
        //            "region_id" = 504;
        //        };
        //        message = "Data retrieved successfully";
        //        status = 1;
        //    }
        //     {
        //        message = "No data found";
        //        status = 0;
        //    }
        
}


- (IBAction)termsAndConditionLinkPressed:(id)sender {
        termsAndConditionBgView.hidden=NO;
        [scrollView setContentOffset:CGPointMake(0, 0)];
        
        
        
}

- (IBAction)closeButtonPressed:(id)sender {
        termsAndConditionBgView.hidden=YES;
}

//hemant
#pragma mark - get resgisteration data
-(void)getRegisterationData
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:self.view fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:GET_REGISTRATION_DATA MathodName:@"GET_REGISTRATION_DATA"];
                
        }
}


#pragma mark - get EB PIN
-(void)generateEBPin
{
        
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@""withBlur:NO allowTap:NO];
        
        Webservice  *callLoginService = [[Webservice alloc] init];
        BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
        if(!chkInternet)
        {
                [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
        }
        else
        {
                NSString *url = [NSString stringWithFormat:@"%@%@",GENERATE_EB_PIN_WS,mobileTxt.text];
                callLoginService.delegate =self;
                [callLoginService GetWebServiceWithURL:url MathodName:@"GENERATE_EB_PIN_DATA"];
        }
}

#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
        if ([strName isEqualToString:@"GET_REGISTRATION_DATA"]){
                //NSLog(@"%@",response);
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        groupArray = [response valueForKey:@"customer_group"];
                        stateArray = [response valueForKey:@"customer_state"];
                        refByArray = [response valueForKey:@"customer_referedby"];
                        businessTypeArr = [response valueForKey:@"business_type"];
                        
                        if (refByArray.count != 0)
                                [_refByBtn setTitle:[[refByArray objectAtIndex:0] valueForKey:@"asm_name"] forState:UIControlStateNormal];
                        
                        if (businessTypeArr.count != 0)
                        {
                                [_businessTypeBtn setTitle:[[businessTypeArr objectAtIndex:0] valueForKey:@"label"] forState:UIControlStateNormal];
                                businessType = [NSString stringWithFormat:@"%@",[[businessTypeArr objectAtIndex:0] valueForKey:@"id"]];
                        }
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
                
        }
        else if ([strName isEqualToString:@"GENERATE_EB_PIN_DATA"]){
                if ([[response valueForKey:@"status"]intValue]==1)
                {
                        ebPin = [response valueForKey:@"ebpin"];
                        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        //            _ebPinLabelHt.constant = 14;
                        
                        _ebPinSendLabel.text = [response valueForKey:@"message"];
                        [_generateEBpinBtn setTitle:@"Resend EB Pin" forState:UIControlStateNormal];
                        //            [alert show];
                        
                }
                else{
                        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[response valueForKey:@"message"]];
                }
        }
        
        else if ([strName isEqualToString:CHECK_MOBILE_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==0)
                {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        //alert.tag = 500;
                        [alert show];
                }
                
        }
        else if ([strName isEqualToString:CHECK_EMAIL_WS])
        {
                if ([[response valueForKey:@"status"]intValue]==0)
                {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                }
                
        }
}
#pragma mark - Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
        if (alertView.tag == 500 && buttonIndex == 0) {
                AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
                [objAppdelegate navigateToLoginViewController];
        }
}

@end

