//
//  LoginSignUpViewController.h
//  EB
//
//  Created by webwerks on 28/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginsViewDelegate <NSObject>

-(void)refreshView;

@end

@interface LoginSignUpViewController : UIViewController

@property (nonatomic) bool registerBtnClicked;
@property (nonatomic, strong) NSString *navigateToViewAfterLogin;
@property (nonatomic, retain) id <LoginsViewDelegate> delegate;

@end
