//
//  LoginSignUpViewController.m
//  EB
//
//  Created by webwerks on 28/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "LoginSignUpViewController.h"

@interface LoginSignUpViewController (){
    UIView *statusBar;
}
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet UIView *loginContainerView;
@property (weak, nonatomic) IBOutlet UIView *signUpContainerView;

@end

@implementation LoginSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        
        if(_registerBtnClicked)
        {
                [self signUpBtnClicked:_signUpBtn];
        }
        else
        {
                 [self loginBtnClicked:_loginBtn];
        }
}

- (void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:animated];
    [APP_DELEGATE.tabBarObj hideTabbar];
    [[CommonSettings sharedInstance] setStatusBar:statusBar];
}


- (void)viewWillDisappear:(BOOL)animated{
        [super viewWillDisappear:animated];
    
     [[CommonSettings sharedInstance] setStatusBar:statusBar];
        [APP_DELEGATE.tabBarObj hideTabbar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - IBAction methods
- (IBAction)loginBtnClicked:(id)sender {

        _loginBtn.backgroundColor = [UIColor whiteColor];
        _signUpBtn.backgroundColor = [UIColor colorForBorder];
        _loginContainerView.hidden = NO;
        _signUpContainerView.hidden = YES;
}

- (IBAction)signUpBtnClicked:(id)sender {
        _signUpBtn.backgroundColor = [UIColor whiteColor];
        _loginBtn.backgroundColor = [UIColor colorForBorder];
        _loginContainerView.hidden = YES;
        _signUpContainerView.hidden = NO;
}
- (IBAction)homeBtnClicked:(id)sender {
        [self navigateToHomeViewcontroller];
}

-(void)navigateToHomeViewcontroller
{
        // Set Firsttime value
        [[NSUserDefaults standardUserDefaults] setObject:@"FirstTime" forKey:@"FirstTime"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        // Navigate from Login screen t home screen.
        UIStoryboard *storyBoard;
        
        storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UINavigationController *navigationController ;
        
        ShowTabbars *tabBarObj = [[ShowTabbars alloc] init];
        [tabBarObj showTabbars];
        [tabBarObj TabClickedIndex:0];
        
        
        SlideMenuViewController *  objMainMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"SlideMenuViewController"];
        
        RightMenuViewController*  objRightMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"RightMenuViewController"];
        
        MFSideMenuContainerViewController *container =[MFSideMenuContainerViewController
                                                       containerWithCenterViewController:tabBarObj
                                                       leftMenuViewController:objMainMenuVC
                                                       rightMenuViewController:objRightMenuVC];
        
        [AppDelegate getAppDelegateObj].navigationController=navigationController;
        [AppDelegate getAppDelegateObj].tabBarObj=tabBarObj;
        [[[UIApplication sharedApplication]delegate].window setRootViewController:container];
        [navigationController setNavigationBarHidden:YES animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
