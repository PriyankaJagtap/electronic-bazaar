//
//  IBMLoginSignUpViewController.h
//  EB
//
//  Created by webwerks on 04/06/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginViewDelegate <NSObject>

-(void)refreshView;

@end

@interface IBMLoginSignUpViewController : UIViewController

@property (nonatomic) bool registerBtnClicked;
@property (nonatomic, strong) NSString *navigateToViewAfterLogin;
@property (nonatomic, strong) NSString *categoryVal;
@property (nonatomic, strong) NSString *category_id;
@property (nonatomic, retain) id <LoginsViewDelegate> delegate;
- (IBAction)loginBtnClicked:(id)sender;
@end
