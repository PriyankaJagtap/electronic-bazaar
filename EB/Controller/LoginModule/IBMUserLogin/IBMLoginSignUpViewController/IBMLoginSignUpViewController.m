//
//  IBMLoginSignUpViewController.m
//  EB
//
//  Created by webwerks on 04/06/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "IBMLoginSignUpViewController.h"

@interface IBMLoginSignUpViewController ()
{
    UIView *statusBar;
}
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet UIView *loginContainerView;
@property (weak, nonatomic) IBOutlet UIView *signUpContainerView;
@end

@implementation IBMLoginSignUpViewController
#pragma mark - ViewController Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    _registerBtnClicked? [self signUpBtnClicked:_signUpBtn] : [self loginBtnClicked:_loginBtn];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//       [CommonSettings setstatusBa];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (@available(iOS 13.0, *)) {
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame];
        statusBar.backgroundColor = [UIColor clearColor];
        [[UIApplication sharedApplication].keyWindow addSubview:statusBar];

    } else {
        // Fallback on earlier versions
        UIView *statusBar=[[UIApplication sharedApplication] valueForKey:@"statusBar"];
        statusBar.backgroundColor = [UIColor clearColor];
        [statusBar setNeedsDisplay];
    }
    [APP_DELEGATE.tabBarObj hideTabbar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction methods
- (IBAction)loginBtnClicked:(id)sender
{
    _loginBtn.backgroundColor = [UIColor whiteColor];
    _signUpBtn.backgroundColor = [UIColor colorForBorder];
    _loginContainerView.hidden = NO;
    _signUpContainerView.hidden = YES;
}

- (IBAction)signUpBtnClicked:(id)sender
{
    _signUpBtn.backgroundColor = [UIColor whiteColor];
    _loginBtn.backgroundColor = [UIColor colorForBorder];
    _loginContainerView.hidden = YES;
    _signUpContainerView.hidden = NO;
}

- (IBAction)homeBtnClicked:(id)sender
{
    [self navigateToHomeViewcontroller];
}

#pragma mark - Other Methods
-(void)navigateToHomeViewcontroller
{
    // Set Firsttime value
    [[NSUserDefaults standardUserDefaults] setObject:@"FirstTime" forKey:@"FirstTime"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    // Navigate from Login screen t home screen.
    UINavigationController *navigationController ;
    ShowTabbars *tabBarObj = [[ShowTabbars alloc] init];
    [tabBarObj showTabbars];
    [tabBarObj TabClickedIndex:0];
    
    MFSideMenuContainerViewController *container =[MFSideMenuContainerViewController
                                                   containerWithCenterViewController:tabBarObj
                                                   leftMenuViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SlideMenuViewController"]
                                                   rightMenuViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RightMenuViewController"]];
    
    [AppDelegate getAppDelegateObj].navigationController = navigationController;
    [AppDelegate getAppDelegateObj].tabBarObj = tabBarObj;
    [[[UIApplication sharedApplication]delegate].window setRootViewController:container];
    [navigationController setNavigationBarHidden:YES animated:YES];
}

@end
