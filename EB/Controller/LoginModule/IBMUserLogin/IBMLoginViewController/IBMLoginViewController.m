//
//  IBMLoginViewController.m
//  EB
//
//  Created by webwerks on 04/06/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "IBMLoginViewController.h"
#import "SlideMenuViewController.h"
#import "HomeViewController.h"
#import "RegistrationViewController.h"
#import "Constant.h"
#import "FVCustomAlertView.h"
#import "CommonSettings.h"
#import "ForgotPassword.h"
#import <Crashlytics/Crashlytics.h>
#import "ALAlertBanner.h"
#import "MyWishlist.h"
#import "VowDelightViewController.h"
#import "SellGadgetUser.h"
#import "IBMLoginSignUpViewController.h"

#define LAPTOP_RETAILER @"laptop_retailer";
#define MOBILE_RETAILER @"mobile_retailer";
#define BOTH_RETAILER @"both";
#define BUSINESS_TYPE @"business_type";

@interface IBMLoginViewController ()
{
    NSMutableDictionary *dictFBUserDetails;
    UITextField *forgotPwdTxtfield;
    NSString *businessType;
    NIDropDown *businessTypeDropDown;
    NSArray *businessTypeArr;
}
@end

@implementation IBMLoginViewController

@synthesize facebookBtn, submitBtn,emailIdTxt, passwordTxt;

#pragma mark - Initialization
- (void)viewDidLoad
{
    [super viewDidLoad];
    APP_DELEGATE.isFromIBMLoginScreen = YES;
    // Set Place holder color
    UIColor *color = [UIColor grayColor];
    NSString *str = @"Email ID";
    emailIdTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName: color}];
    [CommonSettings setAsterixToTextField:emailIdTxt withPlaceHolderName:str];
    
    str = @"Password";
    passwordTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName: color}];
    [CommonSettings setAsterixToTextField:passwordTxt withPlaceHolderName:str];
    
    // set wrap view for text field
    [self createWrapView:emailIdTxt];
    [self createWrapView:passwordTxt];
    
    [[GradientColorClass sharedInstance] setGradientBackgroundToButton:submitBtn];
    
    // add gesture to hide keyboard
    UITapGestureRecognizer *tapScroll = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
    tapScroll.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapScroll];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults valueForKey:@"email"])
        emailIdTxt.text = [defaults valueForKey:@"email"];
    
    if ([defaults valueForKey:@"password"])
        passwordTxt.text = [defaults valueForKey:@"password"];
    
    //[self getRegisterationData];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Other Methods
-(void)navigateToSellGadgetView
{
    [APP_DELEGATE.tabBarObj unhideTabbar];
}

-(void)navigateToParticularViewController
{
    IBMLoginSignUpViewController *vc = (IBMLoginSignUpViewController *)self.parentViewController;
    if ([vc.navigateToViewAfterLogin isEqualToString:CATEGORY])
    {
              ElectronicsViewController *objElectronicsVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
//        objElectronicsVC.isLogin = true
        objElectronicsVC.isFromIBMLoginViewController = YES;
        objElectronicsVC.categoryVal = vc.categoryVal;
        objElectronicsVC.category_id = vc.category_id;
        [self.navigationController pushViewController:objElectronicsVC animated:YES];
//        UINavigationController *nav = (UINavigationController *)APP_DELEGATE.tabBarObj.selectedViewController;
//        [nav pushViewController:vc animated:YES];
        return;
        
//        ElectronicsViewController *objElectronicsVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
//        if ([[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"category_id"] != [NSNull null])
//        {
//            NSDictionary *dic = [sliderImagesArr objectAtIndex:pagecontrol_count];
//            int categoryId = [[[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"category_id"]intValue];
//            objElectronicsVC.category_id=[NSString stringWithFormat:@"%d",categoryId];
//            objElectronicsVC.isSlideBannerClickedFromHomeScreen = YES;
//            objElectronicsVC.filterDataStr = [NSString stringWithFormat:@"&%@=%@",[dic valueForKey:@"code"],[dic valueForKey:@"value"]];
//            objElectronicsVC.categoryVal=[[sliderImagesArr objectAtIndex:pagecontrol_count] objectForKey:@"category_name"];
//            [self.navigationController pushViewController:objElectronicsVC animated:YES];
//        }
    }
    
    
    if ([vc.navigateToViewAfterLogin isEqualToString:PRODUCT_DETAIL] ||[vc.navigateToViewAfterLogin isEqualToString:CATEGORY] || [vc.navigateToViewAfterLogin isEqualToString:SEARCH] ||[vc.navigateToViewAfterLogin isEqualToString:DEALS])
    {
        [self.navigationController popViewControllerAnimated:NO];
        [vc.delegate refreshView];
        return;
    }
    
    [self.navigationController popViewControllerAnimated:NO];
    
    // Navigate from Login screen t home screen.
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    [APP_DELEGATE.tabBarObj unhideTabbar];
    
    if([vc.navigateToViewAfterLogin isEqualToString:MY_PROFILE] )
    {
        [APP_DELEGATE.tabBarObj TabClickedIndex:2];
    }
    else if([vc.navigateToViewAfterLogin isEqualToString:DEALS])
    {
        [APP_DELEGATE.tabBarObj TabClickedIndex:3];
    }
    else if([vc.navigateToViewAfterLogin isEqualToString:TRACK_ORDER])
    {
        [APP_DELEGATE.tabBarObj TabClickedIndex:3];
    }
    else if([vc.navigateToViewAfterLogin isEqualToString:MY_CART])
    {
        [APP_DELEGATE.tabBarObj TabClickedIndex:0];
    }
    
    if ([vc.navigateToViewAfterLogin isEqualToString:VOW_DELIGHT])
    {
        VowDelightViewController *vc = [[UIStoryboard storyboardWithName:@"Product" bundle:nil] instantiateViewControllerWithIdentifier:@"VowDelightViewController"];
        UINavigationController *nav = (UINavigationController *)APP_DELEGATE.tabBarObj.selectedViewController;
        [nav pushViewController:vc animated:YES];
    }
    else if([vc.navigateToViewAfterLogin isEqualToString:MY_WISHLIST])
    {
        MyWishlist *objWishList = [storyBoard instantiateViewControllerWithIdentifier:@"MyWishlist"];
        UINavigationController *nav = (UINavigationController *)APP_DELEGATE.tabBarObj.selectedViewController;
        [nav pushViewController:objWishList animated:YES];
    }
    else if([vc.navigateToViewAfterLogin isEqualToString:MY_CART])
    {
        UINavigationController *nav = (UINavigationController *)APP_DELEGATE.tabBarObj.selectedViewController;
        MyCartViewController  *myCart= [storyBoard instantiateViewControllerWithIdentifier:@"MyCartViewController"];
        [nav pushViewController:myCart animated:NO];
    }
}

-(void)navigateToHomeViewcontroller
{
    // Set Firsttime value
    [[NSUserDefaults standardUserDefaults] setObject:@"FirstTime" forKey:@"FirstTime"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    // Navigate from Login screen t home screen.
    UINavigationController *navigationController ;
    
    self.tabBarObj = [[ShowTabbars alloc] init];
    [self.tabBarObj showTabbars];
    [self.tabBarObj TabClickedIndex:0];
    
    MFSideMenuContainerViewController *container =[MFSideMenuContainerViewController
                                                   containerWithCenterViewController:self.tabBarObj
                                                   leftMenuViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SlideMenuViewController"]
                                                   rightMenuViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RightMenuViewController"]];
    
    [AppDelegate getAppDelegateObj].navigationController = navigationController;
    [AppDelegate getAppDelegateObj].tabBarObj = self.tabBarObj;
    [[[UIApplication sharedApplication]delegate].window setRootViewController:container];
    [navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)createWrapView:(UITextField *)textfield
{
    UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, textfield.frame.size.height)];
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = wrapView;
    [textfield setAutocorrectionType:UITextAutocorrectionTypeNo];
}


#pragma mark - UITextField Delegate
-(void)hideKeyBoard:(UIGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}

- (BOOL) textFieldDidChange:(UITextField *)textField
{
    UIButton *btnColor = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnColor addTarget:self action:@selector(btnOKPressedForPwd:) forControlEvents:UIControlEventTouchUpInside];
    btnColor.frame = CGRectMake((forgotPwdTxtfield.frame.size.width- 40), 3, 30, 25);
    btnColor.backgroundColor = [UIColor lightGrayColor];
    [btnColor setTitle:@"OK" forState:UIControlStateNormal];
    [btnColor setTitle:@"OK" forState:UIControlStateSelected];
    btnColor.titleLabel.font = karlaFontRegular(14.0);
    btnColor.layer.cornerRadius = 5.0;
    [forgotPwdTxtfield addSubview:btnColor];
    return YES;
}

-(void)btnOKPressedForPwd:(UIButton *)sender
{
    if (forgotPwdTxtfield.text.length > 0)
    {
        if (![self validEmail:forgotPwdTxtfield.text] )
        {
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Incorrect email format."];
        }
        else
        {
            [FVCustomAlertView hideAlertFromView:self.view fading:YES];
            [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Password is been sent to your email id."];
        }
    }
}

// Check email validity
-(BOOL)validEmail:(NSString*)emailString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailString]? YES : NO;
}

#pragma mark - Skip Login
//- (IBAction)closeLoginAction:(id)sender
//{
//    [self navigateToHomeViewcontroller];
//}

//-(IBAction)gotoHomePage:(id)sender
//{
//    [self navigateToHomeViewcontroller];
//}

- (IBAction)skipLoginAction:(id)sender
{
    [self.navigationController pushViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RegistrationViewController"] animated:YES];
}

#pragma mark - Alert View
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self skipLoginAction:nil];
    }
}

-(NSString*)formatDateWithString:(NSString*)dateStr
{
    if (dateStr.length>0)
    {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSDate *date=[dateFormatter dateFromString:dateStr];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSString *formattedDate=[dateFormatter stringFromDate:date];
        return formattedDate;
    }
    return @"";
}

#pragma mark - Submit
- (IBAction)showPasswordAction:(id)sender
{
    _showPasswordBtn.selected = ! _showPasswordBtn.selected;
    if (_showPasswordBtn.selected)
    {
        passwordTxt.secureTextEntry = NO;
        passwordTxt.text =passwordTxt.text ;
    }
    else
    {
        passwordTxt.secureTextEntry = YES;
        passwordTxt.text =passwordTxt.text ;
    }
}

- (IBAction)forgotPasswordAction:(id)sender
{
    [self.navigationController pushViewController: [[UIStoryboard storyboardWithName:@"Product" bundle:nil] instantiateViewControllerWithIdentifier:@"ForgotPassword"] animated:YES];
}

- (IBAction)submitAction:(id)sender
{
    [self.view endEditing:YES];
    
    if ([emailIdTxt.text isEqualToString:@""])
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter email ID"];
        return;
    }
    if ([passwordTxt.text isEqualToString:@""])
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Please enter password"];
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:emailIdTxt.text forKey:@"email"];
    [defaults setValue:passwordTxt.text forKey:@"password"];
    
    NSMutableDictionary *dictData = [NSMutableDictionary new];
    [dictData setObject:emailIdTxt.text forKey:@"email"];
    [dictData setObject:passwordTxt.text forKey:@"password"];
    [dictData setObject:@"iphone" forKey:@"device_type"];
    [dictData setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"version"];
    [dictData setObject:[defaults objectForKey:@"DEVICE_TOKEN"]?:@"" forKey:@"push_id"];
    
    [self callLoginWS:dictData];
}

#pragma mark - get resgisteration data
-(void)getRegisterationData
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService GetWebServiceWithURL:GET_REGISTRATION_DATA MathodName:@"GET_REGISTRATION_DATA"];
    }
}

#pragma mark - get response
-(void)RequestFinished:(id)response MethodName:(NSString *)strName
{
    if ([strName isEqualToString:@"GET_REGISTRATION_DATA"])
    {
        //NSLog(@"%@",response);
        if ([[response valueForKey:@"status"]intValue]==1)
        {
            businessTypeArr = [response valueForKey:@"business_type"];
            if (businessTypeArr.count != 0)
            {
                [_businessTypeBtn setTitle:[[businessTypeArr objectAtIndex:0] valueForKey:@"label"] forState:UIControlStateNormal];
                businessType = [NSString stringWithFormat:@"%@",[[businessTypeArr objectAtIndex:0] valueForKey:@"id"]];
            }
        }
        else
        {
            [APP_DELEGATE showAlert:@"" withMessage:[response valueForKey:@"message"]];
        }
    }
}

#pragma mark - LoginWS
-(void)callLoginWS:(NSMutableDictionary *)userdata
{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    
    [userdata setValue:[CommonSettings getAppCurrentVersion] forKey:@"version"];
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:userdata url:IBM_LOGIN_WS string:@"IBMLoginWS"];
    }
}

-(void)receivedResponseForLogin:(id)receiveData stringResponse:(NSString *)responseType
{
    [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
    if ([responseType isEqualToString:@"success"])
    {
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        NSMutableDictionary *dictUserDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
        
        DLog(@"userDetail %@",jsonObject);
        
        [MyCartClass setMyCartcount:[NSNumber numberWithInteger:[[dictUserDetail valueForKey:@"cart_total_qty"]integerValue]]];
        if ([dictUserDetail isKindOfClass:[NSDictionary class]])
        {
            NSString *status = [NSString stringWithFormat:@"%ld",(long)[[dictUserDetail objectForKey:@"status"]integerValue]];
            
            if ([status isEqualToString:@"1"])
            {
                //NSString *tokenVal = [dictUserDetail objectForKey:@"token"]?:@"";
                if([[dictUserDetail valueForKey:@"customer_id"] integerValue] == 5)
                {
                    [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:@"Only Electronics Bazaar users can login from here"];
                    emailIdTxt.text = nil;
                    passwordTxt.text = nil;
                    return;
                }
                //===========================
                //if is_ibm user then remove userdefaults all values
                if ([[dictUserDetail objectForKey:@"is_ibm"] isEqualToString:@"Yes"])
                {
                    isIBMUser = YES;
                    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                    NSString *pushID=[defaults valueForKey:@"DEVICE_TOKEN"];
                    NSString *email = [[defaults valueForKey:@"user"] valueForKey:@"email"];
                    NSString *password =[[defaults valueForKey:@"user"] valueForKey:@"password"];
                    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                    
                    //for sell your gadget
                    SellGadgetUser *objSellGadgetUser = [SellGadgetUser getSellGadgetUserFromNsuserdefaults];
                    NSString *sellYourGadgetLoginUserID = [defaults valueForKey:SELL_GADGET_LOGIN_USERID];
                    NSNumber *isRetailer;
                    if([defaults valueForKey:IS_RETAILER])
                    {
                        isRetailer = [defaults valueForKey:IS_RETAILER];
                    }
                    [defaults removePersistentDomainForName:appDomain];
                    [defaults setObject:[NSNumber numberWithBool:YES] forKey:SHOW_GSTIN];
                    [defaults setObject:pushID forKey:@"DEVICE_TOKEN"];
                    [defaults setObject:email forKey:@"email"];
                    [defaults setObject:password forKey:@"password"];
                    if(isRetailer != nil)
                    {
                        [defaults setObject:isRetailer forKey:IS_RETAILER];
                    }
                    
                    //for sell your gadget
                    [SellGadgetUser saveSellGadgetUserToNsuserdefaults:objSellGadgetUser];
                    [defaults setValue:sellYourGadgetLoginUserID forKey:SELL_GADGET_LOGIN_USERID];
                    [defaults synchronize];
                    [APP_DELEGATE setEBPinGenerateCount];
                    //[appDelegate navigateToLoginViewController];
                    
                    //============ old code after login
                    NSString *tokenVal =@"";
                    NSString *loginType = [dictUserDetail objectForKey:@"login_type"];
                    [[NSUserDefaults standardUserDefaults] setObject:tokenVal forKey:@"token"];
                    [[NSUserDefaults standardUserDefaults] setObject:loginType forKey:@"login_type"];
                    [[NSUserDefaults standardUserDefaults] setObject: [dictUserDetail valueForKey:@"customer_id"] forKey:EB_CUSTOMER_ID];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    NSMutableDictionary *userdict = [NSMutableDictionary new];
                    
                    NSString *uid =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"id"]];
                    NSString *firstname = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"firstname"]];
                    NSString *lastname = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"lastname"]];
                    NSString *dob = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"dob"]];
                    NSString *mobile = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"mobile"]];
                    NSString *profile_picture = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"profile_picture"]];
                    NSString *quote_ID = [NSString stringWithFormat:@"%@",[dictUserDetail objectForKey:@"quoteId"]];
                    
                    // NSString *company_Name=[]
                    
                    NSString *eemail = [loginType isEqualToString:@"normal"]? emailIdTxt.text : [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"email"]];
                    
                    [userdict setObject:uid?uid:@"" forKey:@"user_id"];
                    [userdict setObject:firstname?firstname:@"" forKey:@"firstname"];
                    [userdict setObject:lastname?lastname:@"" forKey:@"lastname"];
                    [userdict setObject:dob?dob:@"" forKey:@"dob"];
                    [userdict setObject:mobile?mobile:@"" forKey:@"mobile"];
                    [userdict setObject:profile_picture?profile_picture:@"" forKey:@"profile_picture"];
                    [userdict setObject:eemail forKey:@"email"];
                    [userdict setObject:passwordTxt.text forKey:@"password"];
                    [userdict setObject:[NSNumber numberWithBool:[[[dictUserDetail objectForKey:@"user"] objectForKey:@"is_arm"] boolValue]] forKey:@"is_arm"];
                    
                    NSString *businessType1 = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"business_type"]];
                    
                    if (![businessType1 isEqualToString:@""])
                        [userdict setValue:businessType1 forKey:@"business_type"];
                    
                    if ([[dictUserDetail objectForKey:@"user"] objectForKey:@"gstin"])
                    {
                        NSString *gstin = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"gstin"]];
                        if(![gstin isEqualToString:@""])
                        {
                            [userdict setValue:gstin forKey:@"gstin"];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:NO] forKey:SHOW_GSTIN];
                        }
                        else
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:YES] forKey:SHOW_GSTIN];
                    }
                    
                    if ([[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"] && ![[[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"] isKindOfClass:[NSNull class]] && ![[[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"] isEqualToString:@""])
                    {
                        NSString *taxVat = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"taxvat"]];
                        [userdict setValue:taxVat forKey:@"taxvat"];
                    }
                    
                    NSString *referredBy =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"referred_by"]];
                    if(![referredBy isEqualToString:@""])
                        [userdict setValue:referredBy forKey:@"referred_by"];
                    
                    if (![[[dictUserDetail valueForKey:@"user"] valueForKey:@"pincode"] isEqualToString:@""])
                    {
                        [userdict setValue:[[dictUserDetail valueForKey:@"user"] valueForKey:@"pincode"] forKey:@"pincode"];
                    }
                    
                    if (![[[dictUserDetail valueForKey:@"user"] valueForKey:@"store_name"] isKindOfClass:[NSNull class]])
                    {
                        [userdict setValue:[[dictUserDetail valueForKey:@"user"] valueForKey:@"store_name"] forKey:@"storeName"];
                    }
                    if ([[dictUserDetail valueForKey:@"customer_id"] integerValue] == 4)
                    {
                        NSInteger i = [[dictUserDetail valueForKey:@"customer_id"] integerValue];
                        [[NSUserDefaults standardUserDefaults]setObject: [NSNumber numberWithInteger:i]forKey:@"AffiliateStatus"];
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:0] forKey:@"AffiliateStatus"];
                    }
                    
                    //Saved IBM user
                    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"is_affiliate"];
                    [[NSUserDefaults standardUserDefaults] setObject:[dictUserDetail objectForKey:@"is_ibm"] forKey:@"is_ibm"];
                    //Save login timestamp
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"IBMLoginTime"];
                    [[NSUserDefaults standardUserDefaults]setObject:quote_ID forKey:@"QuoteID"];
                    [[NSUserDefaults standardUserDefaults]setObject:userdict forKey:@"user"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    IBMLoginSignUpViewController *vc = (IBMLoginSignUpViewController*)self.parentViewController;
                    if(vc.navigateToViewAfterLogin != nil)
                    {
                        [self navigateToParticularViewController];
                    }
                    else
                    {
                        [self navigateToHomeViewcontroller];
                    }
                    
                }
                //===========================
                else
                {
                    [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"Only IBM users can login from here"];
                }
                
            }
            else if ( [status isEqualToString:@"2"])
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[dictUserDetail valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alert show];
            }
            else
            {
                [[AppDelegate getAppDelegateObj]showAlert:@"" withMessage:[dictUserDetail valueForKey:@"message"]];
            }
        }
        else
        {
            [[AppDelegate getAppDelegateObj]showAlert:@"Sorry" withMessage:@"Login couldn't succeed."];
        }
    }
}

@end
