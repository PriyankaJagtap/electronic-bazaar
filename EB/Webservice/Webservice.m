//
//  Webservice.m
//  GLC
//
//  Created by Pooja on 04/09/14.
//  Copyright (c) 2014 pooja. All rights reserved.
//

#import "Webservice.h"
#import "Reachability.h"
#import "FVCustomAlertView.h"
#import "LoginViewController.h"


@implementation Webservice
@synthesize delegate;

#pragma mark - Perform Request

-(void)operationRequestToApi:(NSDictionary*)parameters url:(NSString *)URL string:(NSString*)chkString
{
        [CommonSettings sharedInstance].startTime = [NSDate date];
        _checkStr =chkString;
        if ([chkString isEqualToString:@"LoginWS"])
        {
                NSString *email = [parameters objectForKey:@"username"];
                NSString *password = [parameters objectForKey:@"password"];
                NSString *social_type = [parameters objectForKey:@"social_type"];
                NSString *social_login_id = [parameters objectForKey:@"social_login_id"];
                NSString *device_type = [parameters objectForKey:@"device_type"];
                NSString *push_id = [parameters objectForKey:@"push_id"];
                NSString *firstname = [parameters objectForKey:@"firstname"];
                NSString *lastname = [parameters objectForKey:@"lastname"];
                NSString *dob = [parameters objectForKey:@"dob"];
                
                NSString *mobile = [parameters objectForKey:@"mobile"];
                NSString *businessType = [parameters objectForKey:@"businessType"];
                
                NSString *profile_picture = [parameters objectForKey:@"profile_picture"];
                
                NSString *paramStr;
                
                if ([social_type isEqualToString:@"normal"]) {
                        //  paramStr = [NSString stringWithFormat:@"email=%@&password=%@&device_type=%@",email?email:@"",password?password:@"",device_type];
                        paramStr = [NSString stringWithFormat:@"email=%@&password=%@&social_type=%@&social_login_id=%@&device_type=%@&push_id=%@&firstname=%@&lastname=%@&dob=%@&mobile=%@&profile_picture=%@&version=%@&business_type=%@",email?email:@"",password?password:@"",social_type?social_type:@"",social_login_id?social_login_id:@"",device_type?device_type:@"",push_id?push_id:@"",firstname?firstname:@"",lastname?lastname:@"",dob?dob:@"",mobile?mobile:@"",profile_picture?profile_picture:@"",[parameters valueForKey:@"version"],businessType];
                }
                else{
                        paramStr = [NSString stringWithFormat:@"email=%@&password=%@&social_type=%@&social_login_id=%@&device_type=%@&push_id=%@&firstname=%@&lastname=%@&dob=%@&mobile=%@&profile_picture=%@",email?email:@"",password?password:@"",social_type?social_type:@"",social_login_id?social_login_id:@"",device_type?device_type:@"",push_id?push_id:@"",firstname?firstname:@"",lastname?lastname:@"",dob?dob:@"",mobile?mobile:@"",profile_picture?profile_picture:@""];
                }
                
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                //        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        
        
        
        
        if ([chkString isEqualToString:@"RegistrationWS"])
        {
                NSString *email = [parameters objectForKey:@"email"];
                NSString *password = [parameters objectForKey:@"password"];
                NSString *device_type = [parameters objectForKey:@"device_type"];
                NSString *name = [parameters objectForKey:@"name"];
                NSString *storeName = [parameters objectForKey:@"store_name"];
                
                NSString *mobile = [parameters objectForKey:@"mobile"];
                NSString *city = [parameters objectForKey:@"cus_city"];
                // NSString *affStoreName = [parameters objectForKey:@"affiliate_store"];
                NSString *refBy = [parameters objectForKey:@"asm_id"];
                NSString *ebpin_user = [parameters objectForKey:@"EB_PIN_USER"];
                NSString *ebpin_server = [parameters objectForKey:@"EB_PIN_SERVER"];
                NSString *push_id = [parameters objectForKey:@"push_id"];
                NSString *businessType = [parameters objectForKey:@"businessType"];
                NSString *paramStr = [NSString stringWithFormat:@"email=%@&password=%@&device_type=%@&name=%@&mobile=%@&cus_city=%@&asm_id=%@&ebpin_user=%@&ebpin_server=%@&push_id=%@&version=%@&pincode=%@&store_name=%@&business_type=%@&gstin=%@&taxvat=%@",email,password,device_type,name,mobile,city,refBy,ebpin_user,ebpin_server,push_id,[parameters valueForKey:@"version"],[parameters valueForKey:@"pincode"],storeName,businessType,[parameters objectForKey:@"gstin"],[parameters objectForKey:@"taxvat"]];
                
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        
        if ([chkString isEqualToString:@"IBMLoginWS"])
        {
                NSString *email = [parameters objectForKey:@"email"];
                NSString *password = [parameters objectForKey:@"password"];
                NSString *device_type = [parameters objectForKey:@"device_type"];
                NSString *push_id = [parameters objectForKey:@"push_id"];
                NSString *version = [parameters objectForKey:@"version"];
                
                NSString *paramStr = [NSString stringWithFormat:@"email=%@&password=%@&device_type=%@&push_id=%@&version=%@", email, password, device_type, push_id, version];
                
                
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                //        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        
        if ([chkString isEqualToString:@"IBMRegistrationWS"])
        {
                NSString *email = [parameters objectForKey:@"email"];
                NSString *password = [parameters objectForKey:@"password"];
                NSString *device_type = [parameters objectForKey:@"device_type"];
                NSString *name = [parameters objectForKey:@"name"];
                NSString *mobile = [parameters objectForKey:@"mobile"];
                NSString *is_ibm = [parameters objectForKey:@"is_ibm"];
                NSString *version = [parameters objectForKey:@"version"];
                
                NSString *ebpin_user = [parameters objectForKey:@"ebpin_user"];
                NSString *ebpin_server = [parameters objectForKey:@"ebpin_server"];

                NSString *paramStr = [NSString stringWithFormat:@"email=%@&password=%@&device_type=%@&name=%@&mobile=%@&is_ibm=%@&version=%@&ebpin_user=%@&ebpin_server=%@",email, password, device_type, name, mobile, is_ibm, version, ebpin_user, ebpin_server];
                
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        
        if ([chkString isEqualToString:UAE_REGISTRATION_WS])
        {
                NSString *paramStr = [NSString stringWithFormat:@"store_name=%@&mobile=%@&password=%@&email=%@&manager1=%@&whatsapp1=%@&manager2=%@&whatsapp2=%@&version=%@&device_type=%@&push_id=%@",[parameters valueForKey:@"store_name"],[parameters valueForKey:@"mobile"],[parameters valueForKey:@"password"],[parameters valueForKey:@"email"],[parameters valueForKey:@"manager1"],[parameters valueForKey:@"whatsapp1"],[parameters valueForKey:@"manager2"],[parameters valueForKey:@"whatsapp2"],[parameters valueForKey:@"version"],[parameters valueForKey:@"device_type"],[parameters valueForKey:@"push_id"]];
                
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",UAE_BASE_URL,URL];
                //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        
        
        if ([chkString isEqualToString:UAE_LOGIN_WS]) {
                NSString *paramStr = [NSString stringWithFormat:@"device_type=%@&password=%@&mobile=%@&push_id=%@&version=%@",[parameters valueForKey:@"device_type"],[parameters valueForKey:@"password"],[parameters valueForKey:@"mobile"],[parameters valueForKey:@"push_id"],[parameters valueForKey:@"version"]];
                
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",UAE_BASE_URL,URL];
                //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
                
        }
        if([chkString isEqualToString:@"GET_CITY_FROM_PINCODE"])
        {
                NSString *paramStr = [NSString stringWithFormat:@"pincode=%@",[parameters objectForKey:@"pincode"]];
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                // NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        if ([chkString isEqualToString:@"HomeWS"])
        {
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                
                NSLog(@"Home url %@",urlLink);
                NSString *user_id = @"";
                NSString *paramStr =@"";
                NSString *strRequestUrl = @"";
               
                if (parameters.count > 0) // Check for user_id
                {
                        user_id = [parameters objectForKey:@"user_id"];
                        paramStr = [NSString stringWithFormat:@"user_id=%@&push_id=%@",user_id,[parameters valueForKey:@"DEVICE_TOKEN"]];
                        strRequestUrl=[NSString stringWithFormat:@"%@%@",urlLink,paramStr];
                }
                else{
                        strRequestUrl=[NSString stringWithFormat:@"%@",urlLink];
                }
                
                strRequestUrl=[strRequestUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url=[NSURL URLWithString:strRequestUrl];
                NSLog(@"url is %@",url);
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
                [request setTimeoutInterval:200.0];
                [request setHTTPMethod:@"GET"];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        if ([chkString isEqualToString:@"ViewAllWs"])
        {
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                
                NSString *page_no = [NSString stringWithFormat:@"%@",[parameters objectForKey:@"page_no"]];
                NSString *page_size = [NSString stringWithFormat:@"%@",[parameters objectForKey:@"page_size"]];
                NSString *type = [NSString stringWithFormat:@"%@",[parameters objectForKey:@"type"]];
                
                NSString *strRequestUrl=[NSString stringWithFormat:@"%@type=%@&page_no=%@&page_size=%@",urlLink,type,page_no,page_size];
                
                strRequestUrl=[strRequestUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url=[NSURL URLWithString:strRequestUrl];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
                
                [request setTimeoutInterval:200.0];
                [request setHTTPMethod:@"GET"];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        if ([chkString isEqualToString:@"CategoryProductList"])
        {
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                
                //        NSString *page_no = [NSString stringWithFormat:@"%@",[parameters objectForKey:@"page_no"]];
                //        NSString *page_size = [NSString stringWithFormat:@"%@",[parameters objectForKey:@"page_size"]];
                //        NSString *sortType = [NSString stringWithFormat:@"%@",[parameters objectForKey:@"sortby"]];
                //        NSString *category_id = [NSString stringWithFormat:@"%@",[parameters objectForKey:@"category_id"]];
                NSString *param = [NSString stringWithFormat:@"%@",[parameters objectForKey:@"param"]];
                
                //   NSString *strRequestUrl=[NSString stringWithFormat:@"%@category_id=%@&page_no=%@&page_size=%@&sortby=%@&user_id=%@",urlLink,category_id,page_no,page_size,sortType,user_id];
                NSString *strRequestUrl=[NSString stringWithFormat:@"%@%@",urlLink,param];
                
                strRequestUrl=[strRequestUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url=[NSURL URLWithString:strRequestUrl];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
                
                [request setTimeoutInterval:200.0];
                [request setHTTPMethod:@"GET"];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        if ([chkString isEqualToString:@"MenuListWs"])
        {
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                NSString *strRequestUrl=[NSString stringWithFormat:@"%@",urlLink];
                strRequestUrl=[urlLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url=[NSURL URLWithString:strRequestUrl];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
                
                [request setTimeoutInterval:200.0];
                [request setHTTPMethod:@"GET"];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }//..
        if ([chkString isEqualToString:@"ADDTOCART"]||[chkString isEqualToString:@"ADDTOCARTBUY"])
        {
                int productID = [[parameters objectForKey:@"ProductID"]intValue];
                int quoteID = [[parameters objectForKey:@"QuoteID"]intValue];
                NSString *wishListID = @"";
                if([parameters objectForKey:WISHLIST_ITEM_ID])
                {
                        wishListID = [parameters objectForKey:WISHLIST_ITEM_ID];
                }
             

                NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
                ;
                NSString *paramStr;
                if (quoteID) {
                        paramStr = [NSString stringWithFormat:@"product_id=%d&qty=1&quoteId=%d&userid=%@&wishlist_item_id=%@",productID,quoteID,userId,wishListID
                                    ];
                }else{
                        paramStr = [NSString stringWithFormat:@"product_id=%d&qty=1&quoteId=&userid=%@&wishlist_item_id=%@",productID,userId,wishListID];
                }
                
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
                
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        
        if ([chkString isEqualToString:ADD_BULK_PRODUCTS_CART_WS])
        {
                int productID = [[parameters objectForKey:@"ProductID"]intValue];
                int quoteID = [[parameters objectForKey:@"QuoteID"]intValue];
                int qty = [[parameters objectForKey:@"Qty"]intValue];
                NSString *userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"];
                ;
                NSString *paramStr;
                if (quoteID) {
                        paramStr = [NSString stringWithFormat:@"product_id=%d&qty=%d&quoteId=%d&userid=%@",productID,qty,quoteID,userId
                                    ];
                }else{
                        paramStr = [NSString stringWithFormat:@"product_id=%d&qty=%d&quoteId=&userid=%@",productID,qty,userId];
                }
                
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                //        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
        }
        
        
        if ([chkString isEqualToString:@"GETCARTINFO"]) {
                int quoteID = [[parameters objectForKey:@"QuoteId"]intValue];
                
                NSString *paramStr;
                if (quoteID) {
                        paramStr = [NSString stringWithFormat:@"quoteId=%d",quoteID];
                }else{
                        paramStr = [NSString stringWithFormat:@"quoteId="];
                }
                
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
                
        }
        if ([chkString isEqualToString:@"MYORDER"]) {
                
                NSString *paramStr = [NSString stringWithFormat:@"page_no=%ld&page_size=%@&userid=%@",(long)[[parameters objectForKey:@"page_no"]integerValue],[parameters objectForKey:@"page_size"],[parameters objectForKey:@"user_id"]];
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                // NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
                
        }
        
        
        if ([chkString isEqualToString:SELL_GADGET_HISTORY_WS]) {
                
                NSString *paramStr = [NSString stringWithFormat:@"page_no=%ld&page_size=%@&user_id=%@",(long)[[parameters objectForKey:@"page_no"]integerValue],[parameters objectForKey:@"page_size"],[parameters objectForKey:@"user_id"]];
                NSString *urlLink = [NSString stringWithFormat:@"%@%@",BASE_URL,URL];
                //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:200.0];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[paramStr dataUsingEncoding:NSUTF8StringEncoding]];
                NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                self.connection = connection;
                [connection start];
                
        }
}

-(void)webServiceWitParameters:(NSString*)parameters andURL:(NSString*)url MathodName:(NSString*)methodName;
{
        [CommonSettings sharedInstance].startTime = [NSDate date];
        
        _checkStr=methodName;
        NSString *URLSTR ;
        if([UAEUtilityClass checkUAEStore])
                URLSTR = [NSString stringWithFormat:@"%@%@",UAE_BASE_URL,url];
        else
                URLSTR = [NSString stringWithFormat:@"%@%@",BASE_URL,url];
        
        NSString *strRequestUrl=[URLSTR stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *urlStr=[NSURL URLWithString:strRequestUrl];
        NSLog(@"URL %@",urlStr);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlStr];
        request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
        
        NSData *body = [parameters dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:body];
        
        [request setTimeoutInterval:30.0];
        [request setHTTPMethod:@"POST"];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        self.connection = connection;
        [connection start];
}

-(void)GetWebServiceWithURL:(NSString*)url MathodName:(NSString*)methodName
{
        [CommonSettings sharedInstance].startTime = [NSDate date];
        _checkStr=methodName;
        NSString *URLSTR;
        if([UAEUtilityClass checkUAEStore])
                URLSTR = [NSString stringWithFormat:@"%@%@",UAE_BASE_URL,url];
        else
                URLSTR = [NSString stringWithFormat:@"%@%@",BASE_URL,url];
        
        NSString *strRequestUrl=[URLSTR stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *urlStr=[NSURL URLWithString:strRequestUrl];
        NSLog(@"url %@",urlStr);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlStr];
        request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
        
        [request setTimeoutInterval:30.0];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        self.connection = connection;
        [connection start];
        
}

#pragma mark -  Webservice

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
        return YES;
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
        if ([challenge previousFailureCount] == 0) {
                //NSLog(@"received authentication challenge");
                NSURLCredential *newCredential = [NSURLCredential credentialWithUser:@"admin"
                                                                            password:@"admin123"
                                                                         persistence:NSURLCredentialPersistenceForSession];
                //NSLog(@"credential created");
                [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
                //NSLog(@"responded to authentication challenge");
        }
        else {
                //NSLog(@"previous authentication failure");
        }
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
        self.receivedData = [[NSMutableData alloc]init];
}


-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
        [self.receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
        [self stopProgressHUD];
        DLog(@"error %@",error);
        //  NSString *str= [[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding];
        DLog(@"str %@",str);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Issue loading the data.Please try again after sometime." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
        dispatch_async(dispatch_get_main_queue(), ^{
                
                
                NSString *str= [[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding];
                NSLog(@"%@ web service end time %f",_checkStr,[[NSDate date] timeIntervalSinceDate:[CommonSettings sharedInstance].startTime]);
                NSLog(@"%@",str);
                
                NSError *parseJsonError;
                id serviceResponse=[NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableContainers error:&parseJsonError];
                
                if ([_checkStr isEqualToString:@"LoginWS"]) {
                        [delegate receivedResponseForLogin:self.receivedData stringResponse:@"success"];
                }
                if ([_checkStr isEqualToString:@"IBMLoginWS"]) {
                        [delegate receivedResponseForLogin:self.receivedData stringResponse:@"success"];
                }
                else if ([_checkStr isEqualToString:@"RegistrationWS"]) {
                        [delegate receivedResponseForRegistration:self.receivedData stringResponse:@"success"];
                }
                else if ([_checkStr isEqualToString:@"IBMRegistrationWS"]) {
                        [delegate receivedResponseForRegistration:self.receivedData stringResponse:@"success"];
                }
                else if ([_checkStr isEqualToString:@"GET_CITY_FROM_PINCODE"])
                {
                        [delegate receivedResponseForCityData:self.receivedData  stringResponse:@"success"];
                }
                else if ([_checkStr isEqualToString:@"GET_HOME_PAGE_BANNER"])
                {
                        [delegate RequestFinished:serviceResponse MethodName:@"GET_HOME_PAGE_BANNER"];
                        return ;
                }
                else if ([_checkStr isEqualToString:GET_HOME_SERVICE_OPTION_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_HOME_SERVICE_OPTION_WS];
                        return ;
                }else if ([_checkStr isEqualToString:GET_HOME_BEST_SELLER_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_HOME_BEST_SELLER_WS];
                        return ;
                }
                else if ([_checkStr isEqualToString:GET_HOME_TOP_BRANDS_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_HOME_TOP_BRANDS_WS];
                        return ;
                }
                else if ([_checkStr isEqualToString:GET_HOME_ACCESSORIES_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_HOME_ACCESSORIES_WS];
                        return ;
                }
                else if ([_checkStr isEqualToString:GET_HOME_RECENTLY_VIEWED_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_HOME_RECENTLY_VIEWED_WS];
                        return ;
                }
                else if ([_checkStr isEqualToString:GET_HOME_PAYMENTS_METODS_BANNERS_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_HOME_PAYMENTS_METODS_BANNERS_WS];
                        return ;
                }
                else if ([_checkStr isEqualToString:GET_HOME_HOT_SELLING_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_HOME_HOT_SELLING_WS];
                        return ;
                }
                else if ([_checkStr isEqualToString:GET_HOME_FEATURE_PRODUCTS_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_HOME_FEATURE_PRODUCTS_WS];
                        return ;
                }
                else if ([_checkStr isEqualToString:HOME_FLG_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:HOME_FLG_WS];
                }
                else if ([_checkStr isEqualToString:GSTIN_DETAIL_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GSTIN_DETAIL_WS];
                }
                else if ([_checkStr isEqualToString:FORGOT_PASSWORD_THROUGH_EMAIL_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:FORGOT_PASSWORD_THROUGH_EMAIL_WS];
                }
                else if ([_checkStr isEqualToString:GST_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GST_WS];
                }
                else if([_checkStr isEqualToString:BULK_PRODUCT_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:BULK_PRODUCT_WS];
                }
                else if([_checkStr isEqualToString:BULK_PRODUCT_SEARCH_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:BULK_PRODUCT_SEARCH_WS];
                }
                else if ([_checkStr isEqualToString:ADD_BULK_PRODUCTS_CART_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:ADD_BULK_PRODUCTS_CART_WS];
                }
                else if ([_checkStr isEqualToString:GET_SERVICE_CENTER_DATA_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_SERVICE_CENTER_DATA_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGETS_CATEGORY_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGETS_CATEGORY_WS];
                }
                else if ([_checkStr isEqualToString:GET_HOME_POP_UP_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_HOME_POP_UP_WS];
                        return;
                }
                else if ([_checkStr isEqualToString:SELL_GADGETS_BRANDS_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGETS_BRANDS_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_SEARCH_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_SEARCH_WS];
                }
                else if ([_checkStr isEqualToString:CHECK_MOBILE_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:CHECK_MOBILE_WS];
                }
                else if ([_checkStr isEqualToString:CHECK_EMAIL_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:CHECK_EMAIL_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_MOBILE_MODEL_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_MOBILE_MODEL_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_PRODUCT_DETAIL_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_PRODUCT_DETAIL_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_LAPTOP_DETAIL_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_LAPTOP_DETAIL_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_LAPTOP_DETAIL_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_LAPTOP_DETAIL_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_MOBILE_BRAND_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_MOBILE_BRAND_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_LAPTOP_BRAND_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_LAPTOP_BRAND_WS];
                }
                else if([_checkStr isEqualToString:SELL_GADGET_CHEKOUT_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_CHEKOUT_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_HISTORY_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_HISTORY_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_OTHRES_BRAND_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_OTHRES_BRAND_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_HISTORY_DETAILS_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_HISTORY_DETAILS_WS];
                }
                else if ([_checkStr isEqualToString:SG_ORDER_CONFIRM_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_ORDER_CONFIRM_WS];
                }
                else if ([_checkStr isEqualToString:SHIPMENT_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SHIPMENT_WS];
                }
                else if ([_checkStr isEqualToString:GET_APP_VERSION_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_APP_VERSION_WS];
                        return;
                }
                else if ([_checkStr isEqualToString:GETORDERDETAILS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GETORDERDETAILS];
                        return;
                }
                else if ([_checkStr isEqualToString:TRACK_ORDER_WITH_ORDERID])
                {
                        [delegate RequestFinished:serviceResponse MethodName:TRACK_ORDER_WITH_ORDERID];
                }
                else if ([_checkStr isEqualToString:GET_ADDRESS_DATA_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:GET_ADDRESS_DATA_WS];
                }
                else if ([_checkStr isEqualToString:SALE_BACK_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SALE_BACK_WS];
                }
                
                else if ([_checkStr isEqualToString:@"GET_HOME_PAGE_BANNER_DEALS"])
                {
                        [delegate RequestFinished:serviceResponse MethodName:@"GET_HOME_PAGE_BANNER_DEALS"];
                }
                else if ([_checkStr isEqualToString:@"GENERATE_EB_PIN_DATA"])
                {
                        [delegate RequestFinished:serviceResponse MethodName:@"GENERATE_EB_PIN_DATA"];
                }
                else if([_checkStr isEqualToString:CANCEL_ORDER_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:CANCEL_ORDER_WS];
                }
                else if ([_checkStr isEqualToString:SELL_GADGET_REGISTRATION_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SELL_GADGET_REGISTRATION_WS];
                }
                else if ([_checkStr isEqualToString:RETURN_REASONS_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:RETURN_REASONS_WS];
                }
                else if ([_checkStr isEqualToString:SAVE_RETURN_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SAVE_RETURN_WS];
                }
                else if ([_checkStr isEqualToString:@"HomeWS"]){
                        [delegate receivedResponseForHome:self.receivedData stringResponse:@"success"];
                        return;
                }
                else if ([_checkStr isEqualToString:@"ProductDetailWS"])
                {
                        // [delegate receivedResponseForRegistration:serviceResponse stringResponse:@"success"];
                        [delegate RequestFinished:serviceResponse MethodName:@"ProductDetailWS"];
                }
                else if ([_checkStr isEqualToString:@"ViewAllWs"])
                {
                        [delegate receivedResponseForViewAllProducts:self.receivedData stringResponse:@"success"];
                }
                else if ([_checkStr isEqualToString:@"SUBMIT_FEEDBACK"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"SUBMIT_FEEDBACK"];
                }
                else if ([_checkStr isEqualToString:@"CategoryProductList"]){
                        [delegate receivedResponseForCategoryProdList:self.receivedData stringResponse:@"success"];
                }
                else if ([_checkStr isEqualToString:@"MenuListWs"]){
                        [delegate receivedResponseForMenuList:self.receivedData stringResponse:@"success"];
                        return ;
                }
                else if ([_checkStr isEqualToString:@"MYORDER"]){
                        [delegate receivedResponseForMyOrder:self.receivedData stringResponse:@"success"];
                }
                else if([_checkStr isEqualToString:@"ADDTOCART"]||[_checkStr isEqualToString:@"GETCARTINFO"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"ADDTOCART"];
                }
                else if ([_checkStr isEqualToString:@"CHECKPINCODE"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"CHECKPINCODE"];
                }
                else if ([_checkStr isEqualToString:@"WISHLIST"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"WISHLIST"];
                }
                else if ([_checkStr isEqualToString:@"ADDTOWISHLIST"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"ADDTOWISHLIST"];
                }
                else if ([_checkStr isEqualToString:REMOVE_FROM_WISHLIST_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:REMOVE_FROM_WISHLIST_WS];
                }
                else if ([_checkStr isEqualToString:@"SHARE_WISHLIST"]){
                    [delegate RequestFinished:serviceResponse MethodName:@"SHARE_WISHLIST"];
                }
                else if ([_checkStr isEqualToString:ADD_ALL_TO_CART_WS]){
                    [delegate RequestFinished:serviceResponse MethodName:ADD_ALL_TO_CART_WS];
                }
            
                else if ([_checkStr isEqualToString:@"UPDATECART"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"UPDATECART"];
                        return;
                }
                else if ([_checkStr isEqualToString:@"REMOVEFROMCART"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"REMOVEFROMCART"];
                        return;
                }
                else if ([_checkStr isEqualToString:@"ADDTOCARTBUY"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"ADDTOCARTBUY"];
                }
                else if ([_checkStr isEqualToString:@"MYORDERS"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"MYORDERS"];
                }else if ([_checkStr isEqualToString:@"DASHBOARD"])
                {
                        [delegate RequestFinished:serviceResponse MethodName:@"DASHBOARD"];
                }else if ([_checkStr isEqualToString:@"CUSTOMER_ADDRESS_LIST"])
                {
                        [delegate RequestFinished:serviceResponse MethodName:@"CUSTOMER_ADDRESS_LIST"];
                }
                else if ([_checkStr isEqualToString:@"GETREGION"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"GETREGION"];
                }
                else if ([_checkStr isEqualToString:@"SAVE_BILLING_ADDRESS"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"SAVE_BILLING_ADDRESS"];
                }
                else if ([_checkStr isEqualToString:@"PINCODE"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"PINCODE"];
                }
                else if ([_checkStr isEqualToString:@"GETCOUNTRYLIST"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"GETCOUNTRYLIST"];
                }
                else if ([_checkStr isEqualToString:@"SAVE_SHIPPING_ADDRESS"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"SAVE_SHIPPING_ADDRESS"];
                }
                else if ([_checkStr isEqualToString:@"REMOVE_ADDRESS"]){
                    [delegate RequestFinished:serviceResponse MethodName:@"REMOVE_ADDRESS"];
                }
                else if ([_checkStr isEqualToString:@"AVAILABLEPAYMENT"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"AVAILABLEPAYMENT"];
                }
                else if ([_checkStr isEqualToString:@"CREATEORDER"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"CREATEORDER"];
                }
                else if ([_checkStr isEqualToString:@"SELECTEDPAYMENTMETHOD"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"SELECTEDPAYMENTMETHOD"];
                } else if ([_checkStr isEqualToString:@"CHANGEPASSWORD"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"CHANGEPASSWORD"];
                }
                else if ([_checkStr isEqualToString:@"REMOVEFROMWISHLIST"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"REMOVEFROMWISHLIST"];
                }else if ([_checkStr isEqualToString:@"ADDREVIEWWS"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"ADDREVIEWWS"];
                }
                else if ([_checkStr isEqualToString:@"MYORDERDETAIL"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"MYORDERDETAIL"];
                }
                else if ([_checkStr isEqualToString:EDIT_PROFILE]){
                        [delegate RequestFinished:serviceResponse MethodName:EDIT_PROFILE];
                }
                else if ([_checkStr isEqualToString:@"CREATE_ADDRESS"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"CREATE_ADDRESS"];
                }else if ([_checkStr isEqualToString:@"ADDRESS_LIST"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"ADDRESS_LIST"];
                }
                else if ([_checkStr isEqualToString:@"ADDCOUPONCODE"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"ADDCOUPONCODE"];
                }else if ([_checkStr isEqualToString:@"FORGOTPASSWORD"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"FORGOTPASSWORD"];
                }else if ([_checkStr isEqualToString:@"SEARCHPRODUCTS"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"SEARCHPRODUCTS"];
                }
                else if ([_checkStr isEqualToString:@"COUPONLISTING"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"COUPONLISTING"];
                }else if ([_checkStr isEqualToString:@"REMOVECOUPONCODE"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"REMOVECOUPONCODE"];
                }else if ([_checkStr isEqualToString:@"NOTIFICATIONLIST"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"NOTIFICATIONLIST"];
                }
                else if ([_checkStr isEqualToString:@"ECOMMERCEDETAIL"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"ECOMMERCEDETAIL"];
                }else if ([_checkStr isEqualToString:@"PAYZAPPGENERATEHASH"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"PAYZAPPGENERATEHASH"];
                }else if ([_checkStr isEqualToString:GET_INCREMENTID_FROM_QUOTEID]){
                        [delegate RequestFinished:serviceResponse MethodName:GET_INCREMENTID_FROM_QUOTEID];
                }
                else if ([_checkStr isEqualToString:SAVE_SUPER_SALE_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:SAVE_SUPER_SALE_WS];
                }
                else if ([_checkStr isEqualToString:CREATE_ORDER_BY_PAYZAPP]){
                        [delegate RequestFinished:serviceResponse MethodName:CREATE_ORDER_BY_PAYZAPP];
                }else if ([_checkStr isEqualToString:@"SAVE_BILLING_ADDRESS_ONLY"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"SAVE_BILLING_ADDRESS_ONLY"];
                }else if ([_checkStr isEqualToString:@"GET_REGISTRATION_DATA"]){
                        [delegate RequestFinished:serviceResponse MethodName:@"GET_REGISTRATION_DATA"];
                }
                
                //for Sell Your gadget Screen
                else if ([_checkStr isEqualToString:SG_CHECK_PINCODE_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_CHECK_PINCODE_WS];
                }
                else if ([_checkStr isEqualToString:SG_HOME_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_HOME_WS];
                }
                else if ([_checkStr isEqualToString:SG_GET_MOBILE_MODEL_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_GET_MOBILE_MODEL_WS];
                }
                else if ([_checkStr isEqualToString:SG_GET_LAPTOP_MODEL_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_GET_LAPTOP_MODEL_WS];
                }
                else if ([_checkStr isEqualToString:SG_GET_MOBILE_CONDITIONS_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_GET_MOBILE_CONDITIONS_WS];
                }
                else if ([_checkStr isEqualToString:SG_GET_LAPTOP_CONDITIONS_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_GET_LAPTOP_CONDITIONS_WS];
                }
                else if ([_checkStr isEqualToString:SG_PROMOCODE_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_PROMOCODE_WS];
                }
                else if ([_checkStr isEqualToString:SG_GET_BANK_LIST_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_GET_BANK_LIST_WS];
                }
                else if ([_checkStr isEqualToString:SG_NEW_PROCESSOR_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_NEW_PROCESSOR_WS];
                }
                else if ([_checkStr isEqualToString:SG_REPLACE_LAPTOP_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SG_REPLACE_LAPTOP_WS];
                }
                else if ([_checkStr isEqualToString:SHOW_EB_PIN_ALERT_WS])
                {
                    [delegate RequestFinished:serviceResponse MethodName:SHOW_EB_PIN_ALERT_WS];
                }
                else if ([_checkStr isEqualToString:GENERATE_SEND_ORDER_EB_PIN_WS])
                {
                    [delegate RequestFinished:serviceResponse MethodName:GENERATE_SEND_ORDER_EB_PIN_WS];
                }
            

                //for vow delight
                else if ([_checkStr isEqualToString:VALIDATE_IMEI_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:VALIDATE_IMEI_WS];
                }
                else if ([_checkStr isEqualToString:SAVE_REQUEST_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:SAVE_REQUEST_WS];
                }
                
                //for UAE App
                else if ([_checkStr isEqualToString:UAE_REGISTRATION_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:UAE_REGISTRATION_WS];
                }
                else if ([_checkStr isEqualToString:UAE_LOGIN_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:UAE_LOGIN_WS];
                }
                else if ([_checkStr isEqualToString:UAE_FORGOT_PASSWORD_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:UAE_FORGOT_PASSWORD_WS];
                }
                else if ([_checkStr isEqualToString:UAE_MENU_LIST_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:UAE_MENU_LIST_WS];
                }
                else if ([_checkStr isEqualToString:UAE_HOME_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:UAE_HOME_WS];
                }
                else if ([_checkStr isEqualToString:UAE_MY_ACCOUNT_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:UAE_MY_ACCOUNT_WS];
                }
                else if ([_checkStr isEqualToString:UAE_EDIT_PROFILE_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:UAE_EDIT_PROFILE_WS];
                }
                else if ([_checkStr isEqualToString:UAE_CHANGE_PASSWORD_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:UAE_CHANGE_PASSWORD_WS];
                }
                else if ([_checkStr isEqualToString:UAE_GET_BRANDS_WS]){
                        [delegate RequestFinished:serviceResponse MethodName:UAE_GET_BRANDS_WS];
                }
                
                else if([_checkStr isEqualToString:RESET_PASSWORD_OTP_WS])
                {
                        [delegate RequestFinished:serviceResponse MethodName:RESET_PASSWORD_OTP_WS];
                }
            
                [self stopProgressHUD];
        });
}


#pragma mark -  Progress Hud Delegate methods

-(void)startProgressHUD
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
}

-(void)stopProgressHUD
{
        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
}


@end
