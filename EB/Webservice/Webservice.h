//
//  Webservice.h
//  GLC
//
//  Created by Pooja on 04/09/14.
//  Copyright (c) 2014 pooja. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constant.h"


//delegate to receive response
@protocol FinishLoadingData <NSObject>

@optional

-(void)receivedResponseForLogin:(id)receiveData stringResponse:(NSString*)responseType;

-(void)receivedResponseForRegistration:(id)receiveData stringResponse:(NSString*)responseType;

-(void)receivedResponseForCityData:(id)receiveData stringResponse:(NSString*)responseType;


-(void)receivedResponseForHome:(id)receiveData stringResponse:(NSString*)responseType;

-(void)receivedResponseForViewAllProducts:(id)receiveData stringResponse:(NSString*)responseType;

-(void)receivedResponseForCategoryProdList:(id)receiveData stringResponse:(NSString*)responseType;

-(void)receivedResponseForMenuList:(id)receiveData stringResponse:(NSString*)responseType;
-(void)receivedResponseForMyOrder:(id)receiveData stringResponse:(NSString*)responseType;

//..
-(void)RequestFinished:(id)response MethodName:(NSString *)strName;
@end


@interface Webservice : NSObject<NSURLConnectionDelegate,NSURLConnectionDataDelegate>
@property (retain, nonatomic) NSURLConnection *connection;
@property (retain, nonatomic) NSString *checkStr;
@property (retain, nonatomic) NSMutableData *receivedData;
@property (nonatomic,weak) id<FinishLoadingData> delegate;

-(void)operationRequestToApi:(NSDictionary*)parameters url:(NSString *)URL string:(NSString*)chkString;
-(void)GetWebServiceWithURL:(NSString*)url MathodName:(NSString*)methodName;

-(void)webServiceWitParameters:(NSString*)parameters andURL:(NSString*)url MathodName:(NSString*)methodName;

@end
