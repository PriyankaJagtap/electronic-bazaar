//
//  DealsCell.h
//  EB
//
//  Created by webwerks on 9/10/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface DealsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *product_Img;
@property (weak, nonatomic) IBOutlet UILabel *product_Name;
@property (weak, nonatomic) IBOutlet UILabel *prod_Prev_Price;
@property (weak, nonatomic) IBOutlet UILabel *prod_Price;
@property (weak, nonatomic) IBOutlet UILabel *prod_Discount;

@end
