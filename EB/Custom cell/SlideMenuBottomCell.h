//
//  SlideMenuBottomCell.h
//  EB
//
//  Created by webwerks on 8/6/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuBottomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *menuTitleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *menuImgView;

@end
