//
//  HotProductCell.h
//  EB
//
//  Created by webwerks on 8/3/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface HotProductCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *FavBtn;
@property (weak, nonatomic) IBOutlet UILabel *percent_discountLbl;
@property (strong, nonatomic) IBOutlet UIImageView *product_Img;
@property (weak, nonatomic) IBOutlet UILabel *productname_lbl;
@property (weak, nonatomic) IBOutlet UILabel *actualPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *specialPrice_lbl;
@property (weak, nonatomic) IBOutlet UIImageView *imgOutofStock;

@end
