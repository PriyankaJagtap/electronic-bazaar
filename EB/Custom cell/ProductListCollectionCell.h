//
//  ProductListCollectionCell.h
//  EB
//
//  Created by webwerks on 8/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
@interface ProductListCollectionCell : UICollectionViewCell
{
}

@property (weak, nonatomic) IBOutlet UIImageView *ouOfStockProductImageView;
@property (weak, nonatomic) IBOutlet UIImageView *categoryTypeImageView;

@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@end
