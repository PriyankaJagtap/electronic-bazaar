//
//  SpecificationCell.h
//  EB
//
//  Created by webwerks on 8/18/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecificationCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *productName;
@property(strong,nonatomic)NSString *productTitle;

@end
