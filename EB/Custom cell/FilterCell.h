//
//  FilterCell.h
//  EB
//
//  Created by webwerks on 8/25/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_specification1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_specification2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_specification3;
@property (weak, nonatomic) IBOutlet UILabel *lbl_specification4;
@property (weak, nonatomic) IBOutlet UIButton *favBtn;
@property (weak, nonatomic) IBOutlet UILabel *product_nameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *product_img;

@end
