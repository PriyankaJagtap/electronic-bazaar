//
//  ViewAllProductCell.m
//  EB
//
//  Created by webwerks on 8/19/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "ViewAllProductCell.h"

@implementation ViewAllProductCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
