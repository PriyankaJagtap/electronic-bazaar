//
//  MyOrderDetailCell.m
//  EB
//
//  Created by webwerks on 9/24/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "MyOrderDetailCell.h"
#import "MyOrderDetailProdCell.h"


@implementation MyOrderDetailCell
@synthesize productDetailArr,myOrderTableView;

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [productDetailArr count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"MyOrderProdCell";
    MyOrderDetailProdCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.product_Name.text=[[productDetailArr objectAtIndex:indexPath.row
                             ]valueForKey:@"name"];
    NSString *strURL=[[productDetailArr objectAtIndex:indexPath.row]valueForKey:@"image"];
    
    [cell.product_Image sd_setImageWithURL:[NSURL URLWithString:strURL]];
//    cell.product_Price.text=[NSString stringWithFormat:@"Rs.%@",[[productDetailArr objectAtIndex:indexPath.row]valueForKey:@"price"]];
     cell.product_Price.text=[NSString stringWithFormat:@"Rs.%@",[[productDetailArr objectAtIndex:indexPath.row]valueForKey:@"price"]];
       
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_deleagte productDetailClicked:indexPath];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}
@end
