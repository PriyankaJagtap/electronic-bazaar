//
//  MyOrderDetailCell.h
//  EB
//
//  Created by webwerks on 9/24/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MyOrderDetailProdDelegate <NSObject>
-(void)productDetailClicked:(NSIndexPath *)indexPath;
@end

@interface MyOrderDetailCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UILabel *order_Date_Lbl;
@property (strong, nonatomic) IBOutlet UIImageView *status_Img;

@property (strong, nonatomic) IBOutlet UITableView *myOrderTableView;
@property(strong,nonatomic)NSArray *productDetailArr;
@property (nonatomic)id<MyOrderDetailProdDelegate> deleagte;


@end
