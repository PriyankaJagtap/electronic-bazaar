//
//  MyOrderBgCell.h
//  EB
//
//  Created by webwerks on 8/24/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrderBgCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *order_Date_Lbl;
@property (strong, nonatomic) IBOutlet UIImageView *status_Img;
@property (strong, nonatomic) IBOutlet UITableView *myOrderTableView;
@property(strong,nonatomic)NSArray *productDetailArr;

@property (weak, nonatomic) IBOutlet UIButton *myOrderButton;

@end
