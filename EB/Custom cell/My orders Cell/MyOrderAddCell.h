//
//  MyOrderAddCell.h
//  EB
//
//  Created by webwerks on 9/25/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrderAddCell : UITableViewCell

@property(strong,nonatomic)IBOutlet UILabel *billing_Address_Lbl;
@property(strong,nonatomic)IBOutlet UILabel *shipping_Address_Lbl;

@end
