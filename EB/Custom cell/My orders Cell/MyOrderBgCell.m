//
//  MyOrderBgCell.m
//  EB
//
//  Created by webwerks on 8/24/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "MyOrderBgCell.h"
#import "MyOrderProdCell.h"
@implementation MyOrderBgCell
@synthesize productDetailArr,myOrderTableView;

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[productDetailArr valueForKey:@"order_items"]count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"MyOrderProdCell";
    MyOrderProdCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.product_Name.text=[[[productDetailArr valueForKey:@"order_items"]objectAtIndex:indexPath.row
                        ]valueForKey:@"product_name"];
    NSString *strURL=[[[productDetailArr valueForKey:@"order_items"]objectAtIndex:indexPath.row]valueForKey:@"image"];
    
    [cell.product_Image sd_setImageWithURL:[NSURL URLWithString:strURL]];
    int orderID=[[productDetailArr valueForKey:@"order_id"]intValue];
    cell.order_Id.text=[NSString stringWithFormat:@"%d",orderID];
    cell.delivery_On.text=[productDetailArr valueForKey:@"created_at"];
    
    return cell;
}

@end
