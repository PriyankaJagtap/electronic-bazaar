//
//  MyOrderProdCell.h
//  EB
//
//  Created by webwerks on 8/24/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface MyOrderProdCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *product_Name;
@property (weak, nonatomic) IBOutlet UILabel *order_Id;
@property (weak, nonatomic) IBOutlet UILabel *delivery_On;
@property (weak, nonatomic) IBOutlet UIImageView *product_Image;

@end
