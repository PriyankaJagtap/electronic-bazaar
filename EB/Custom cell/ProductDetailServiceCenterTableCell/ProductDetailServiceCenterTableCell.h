//
//  ProductDetailServiceCenterTableCell.h
//  EB
//
//  Created by Neosoft on 8/23/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailServiceCenterTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIButton *serviceBtn;

@end
