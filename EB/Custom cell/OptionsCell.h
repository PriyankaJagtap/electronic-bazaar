//
//  OptionsCell.h
//  EB
//
//  Created by webwerks on 8/27/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *optionTitle;
@property (weak, nonatomic) IBOutlet UIButton *optionSelected;

@end
