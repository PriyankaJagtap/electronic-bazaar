//
//  ProductTableCell.m
//  EB
//
//  Created by webwerks on 8/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "ProductTableCell.h"
#import "AddNewProdCell.h"
#import "HotProductCell.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "CommonSettings.h"

@implementation ProductTableCell
@synthesize productListCollectionView;
@synthesize isFavProduct,productDetailsArray,productCollectionViewTag,recentlyViewedProductArr;


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
        [super setSelected:selected animated:animated];
        
        // Configure the view for the selected state
}
- (void)awakeFromNib {
        // Initialization code
        //NSLog(@"produc : %@",productDetailsArray);
        [super awakeFromNib];
        
        UICollectionViewFlowLayout *flowLayout =[[UICollectionViewFlowLayout alloc] init];
        //[flowLayout setItemSize:CGSizeMake(170,150)];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        [productListCollectionView setCollectionViewLayout:flowLayout];
        
}


-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
        return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
        return [recentlyViewedProductArr count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
        return CGSizeMake((kSCREEN_WIDTH- 15)/2, ((kSCREEN_WIDTH- 15)/2) +20);//139
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
        return 0.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section;
{
        return UIEdgeInsetsMake(5, 5, 5, 5);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *identifier = @"AddNewProdCell";
        AddNewProdCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell.backgroundColor=[UIColor clearColor];
        
        if(isFavProduct)
        {
                cell.product_name.text=[[recentlyViewedProductArr objectAtIndex:indexPath.row]valueForKey:@"name"];
                
                NSString *productImageURl=[[recentlyViewedProductArr objectAtIndex:indexPath.row]valueForKey:@"image"];
                [cell.product_img sd_setImageWithURL:[NSURL URLWithString:productImageURl]];
                
                NSString *actualPriceStr = [NSString stringWithFormat:@"Rs %ld",(long)[[[recentlyViewedProductArr objectAtIndex:indexPath.row]objectForKey:@"price"] integerValue]];
                cell.product_Special_price.text = actualPriceStr;
                cell.product_price.text=@"";
                
                NSString *is_InStock = [NSString stringWithFormat:@"%@",[[productDetailsArray objectAtIndex:indexPath.row]objectForKey:@"is_instock"]];
                if ([is_InStock isEqualToString:@"0"])
                {
                        [cell.imgOutofStock setHidden:NO];
                }
                else
                {
                        [cell.imgOutofStock setHidden:YES];
                }
        }
        else
        {
                cell.product_name.text=[[recentlyViewedProductArr objectAtIndex:indexPath.row]valueForKey:@"ProductName"];
                NSString *productImageURl=[[recentlyViewedProductArr objectAtIndex:indexPath.row]valueForKey:@"ProductImage"];
                [cell.product_img sd_setImageWithURL:[NSURL URLWithString:productImageURl]];
                
                NSString *actualPriceStr = [NSString stringWithFormat:@"Rs %ld",(long)[[[recentlyViewedProductArr objectAtIndex:indexPath.row]objectForKey:@"ProductPrice"] integerValue]];
                cell.product_Special_price.text = actualPriceStr;
                cell.product_price.text=@"";
                
                NSString *is_InStock = [NSString stringWithFormat:@"%@",[[productDetailsArray objectAtIndex:indexPath.row]objectForKey:@"is_instock"]];
                if ([is_InStock isEqualToString:@"0"])
                {
                        [cell.imgOutofStock setHidden:NO];
                }
                else
                {
                        [cell.imgOutofStock setHidden:YES];
                }
        }
        [cell.addCartBtn addTarget:self action:@selector(cartButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.addCartBtn.tag=indexPath.row;
        cell.bgView.layer.cornerRadius = 5;
        [CommonSettings addBorderLayer: cell.bgView];
        return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        int productId;
        if(isFavProduct)
                productId=[[[recentlyViewedProductArr objectAtIndex:indexPath.item]valueForKey:@"product_id"]intValue];
        else
                productId=[[[recentlyViewedProductArr objectAtIndex:indexPath.item]valueForKey:@"ProductId"]intValue];
        [self.delegate CollectionViewDidselctProduct:productId];
}

-(void)cartButtonPressed:(UIButton*)btn
{
        if ([btn.accessibilityLabel isEqualToString:@"0"])
        {
                [[CommonSettings sharedInstance]showAlertTitle:@"" message:@"This product is out of stock."];
        }
        else
        {
                int productId;
                if(isFavProduct)
                        productId=[[[recentlyViewedProductArr objectAtIndex:btn.tag]valueForKey:@"product_id"]intValue];
                else
                        productId=[[[recentlyViewedProductArr objectAtIndex:btn.tag]valueForKey:@"ProductId"]intValue];
                [self.delegate CollectionViewDidSelectAddToCart:productId];
        }
}

@end
