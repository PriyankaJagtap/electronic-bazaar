//
//  MenuProductCell.h
//  EB
//
//  Created by webwerks on 8/2/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuProductCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
