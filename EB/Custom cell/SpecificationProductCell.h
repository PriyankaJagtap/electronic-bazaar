//
//  SpecificationProductCell.h
//  EB
//
//  Created by webwerks on 8/20/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecificationProductCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelDescription;

@end
