//
//  ProductListCell.h
//  EB
//
//  Created by webwerks on 8/6/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
@interface ProductListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *itemLeftLabel;

@property (weak, nonatomic) IBOutlet UILabel *discount;
@property (weak, nonatomic) IBOutlet UIImageView *productImg;
@property (weak, nonatomic) IBOutlet UIImageView *outofProductImg;

//@property (weak, nonatomic) IBOutlet UIImageView *outOfProductproductImg;

@property (weak, nonatomic) IBOutlet UIButton *buyNowLbl;
@property (weak, nonatomic) IBOutlet UILabel *productNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *productPrevPriceLbl;

@property (weak, nonatomic) IBOutlet UIButton *favProdBtn;
@property (weak, nonatomic) IBOutlet UIImageView *categoryTypeImgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryTypeImgWidthConstraint;

- (IBAction)addFavProductAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *categoryFavProdBtn;
- (IBAction)categoryAddFavProdAction:(id)sender;

//outlets for deals of the day
@property (weak, nonatomic) IBOutlet UIButton *loginToViewPriceBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginToViewPriceBtnHtConstraint;

@property (weak, nonatomic) IBOutlet UILabel *productSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *productMemoryLabel;
@property (weak, nonatomic) IBOutlet UIButton *addToCartBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addToCartBtnHtConstraint;
@property (weak, nonatomic) IBOutlet UILabel *wrongLabel;
@property (weak, nonatomic) IBOutlet UIView *comingSoonView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *comingSoonViewHeightConstraint;

-(void)configureCartButtons;
@end
