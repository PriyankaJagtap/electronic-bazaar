//
//  ReviewCell.h
//  EB
//
//  Created by webwerks on 8/19/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleReview;
@property (strong, nonatomic) IBOutlet UILabel *detailReview;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *date;

@end
