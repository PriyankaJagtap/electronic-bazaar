//
//  CartProductCell.h
//  EB
//
//  Created by webwerks on 8/21/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
@interface CartProductCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UIImageView *outOfStockImage;

@property (strong, nonatomic) IBOutlet UILabel *prodDetail;
@property (strong, nonatomic) IBOutlet UILabel *productPrice;
@property (strong, nonatomic) IBOutlet UIButton *buttonClose;
@property (strong, nonatomic) IBOutlet UIButton *decreaseProdCountbtn;
@property (strong, nonatomic) IBOutlet UIButton *increaseProdCountbtn;
@property (strong, nonatomic) IBOutlet UITextField *prodCountTxt;
@property (weak, nonatomic) IBOutlet UILabel *specialPriceLbl;
@property (weak, nonatomic) IBOutlet UIButton *forwordArrowBtn;
@property (weak, nonatomic) IBOutlet UILabel *itemLeftLabel;
@property (weak, nonatomic) IBOutlet UIImageView *categoryTypeImgView;

@end
