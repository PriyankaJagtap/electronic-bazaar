//
//  CartProductCell.m
//  EB
//
//  Created by webwerks on 8/21/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "CartProductCell.h"

@implementation CartProductCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
