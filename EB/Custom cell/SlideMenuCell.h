//
//  SlideMenuCell.h
//  EB
//
//  Created by webwerks on 8/4/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *menuTitleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *menuIconImage;

@end
