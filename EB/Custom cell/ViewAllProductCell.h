//
//  ViewAllProductCell.h
//  EB
//
//  Created by webwerks on 8/19/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface ViewAllProductCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *product_priceLbl;
@property (weak, nonatomic) IBOutlet UIImageView *product_img;
@property (weak, nonatomic) IBOutlet UIImageView *outOfProduct_img;

@property (weak, nonatomic) IBOutlet UILabel *product_nameLbl;
@property (weak, nonatomic) IBOutlet UIButton *favBtn;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
