//
//  AvailblePaymentOptionTableCell.m
//  EB
//
//  Created by Neosoft on 4/19/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "AvailblePaymentOptionTableCell.h"

@implementation AvailblePaymentOptionTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [[CommonSettings sharedInstance] setBorderToView:_bgView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
