//
//  AvailblePaymentOptionTableCell.h
//  EB
//
//  Created by Neosoft on 4/19/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvailblePaymentOptionTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *freeHomeDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *codLabel;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *verifyOnDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *emiOptionLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emiHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verifyOnDeliveryHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *codHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *homeDeliveryHeightConstraint;
@end
