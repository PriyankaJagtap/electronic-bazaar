//
//  ShareWishlistCell.h
//  EB
//
//  Created by Aishwarya Rai on 31/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareWishlistCell : UITableViewCell

@property (weak,nonatomic) IBOutlet UITextField *emailTF;
@property (weak,nonatomic) IBOutlet UITextField *messageTF;
@property (weak,nonatomic) IBOutlet UIButton *shareWishlistBtn;
@property (weak,nonatomic) IBOutlet UIButton *addAllToCartBtn;


@property (weak,nonatomic) IBOutlet NSLayoutConstraint *viewHeightConst;

@end
