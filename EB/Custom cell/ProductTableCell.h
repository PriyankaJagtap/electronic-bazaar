//
//  ProductTableCell.h
//  EB
//
//  Created by webwerks on 8/14/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//


#define FAV_BTN_TAG 10

@protocol CustomCollectionViewDelegate

- (void) CollectionViewDidselctProduct:(int)productId;
- (void) CollectionViewDidSelectAddToCart:(int)productId;

//-(void) collectionViewdidselectFavouriteProduct:(int)productId;
//-(void)collectionViewDidselectRemoveFavouriteProduct:(int)productId;



@end
#import <UIKit/UIKit.h>

@interface ProductTableCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate
>
{
    NSMutableSet *recentlyViewProductArr;
    
}
@property(strong,nonatomic)NSArray *productDetailsArray;
@property(strong,nonatomic)NSArray *recentlyViewedProductArr;


@property (nonatomic, retain) id <CustomCollectionViewDelegate> delegate;


@property (strong, nonatomic) IBOutlet UICollectionView *productListCollectionView;
@property(nonatomic)BOOL isFavProduct;
@property(nonatomic)int productCollectionViewTag;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *collectionviewHtConstraint;


@end
