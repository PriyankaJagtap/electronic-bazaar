//
//  GradientBackgroundButton.m
//  EB
//
//  Created by webwerks on 28/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "GradientBackgroundButton.h"

@implementation GradientBackgroundButton

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
        
       
}

- (id)initWithCoder:(NSCoder*)coder {
        self = [super initWithCoder:coder];
        
        if (self) {
                
                [self myInit];
        }
        return self;
}

- (void) myInit
{
        self.clipsToBounds = YES;
        self.layer.borderWidth = 18.0f;
        gradient = [CAGradientLayer layer];
        gradient.frame = self.bounds;
        gradient.colors = @[_topColor,_bottomColor];
        CGFloat x = 90;
        CGFloat a = pow(sin((2*M_PI*((x+0.75)/2))),2);
        CGFloat b = pow(sin((2*M_PI*((x+0.0)/2))),2);
        CGFloat c = pow(sin((2*M_PI*((x+0.25)/2))),2);
        CGFloat d = pow(sin((2*M_PI*((x+0.5)/2))),2);
        gradient.startPoint = CGPointMake(a, b);
        gradient.endPoint = CGPointMake(c, d);
        [self.layer insertSublayer:gradient atIndex:0];
}
@end
