//
//  BDViewController.h
//  BillDesk_iOS_sdk
//

@protocol LibraryPaymentStatusProtocol <NSObject>
@required
-(void)paymentStatus:(NSString*)message;

@end
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "BDQuickPayListViewController.h"

@interface BDViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,NSXMLParserDelegate,CardListUpdateProtocol,CLLocationManagerDelegate>
{
    // CLLocationManager *locationManager;
    int heightOfHeader;
    NSString *message;
    NSString *token;
    NSString *email;
    NSString *mobile;
    float amount;
    NSString *merchantID;
    NSString *netBankingUrl;
    NSString *btnfontName;
    NSString *lblfontName;
    float fontSize;
    NSMutableArray *quickPayList;
    float currentLat;
    float currentLong;
    BOOL hideNavBar;
    
    
}
@property(nonatomic,assign)float payableAmount;
@property(strong,nonatomic)NSDictionary *configDictionary;
@property(unsafe_unretained)id<LibraryPaymentStatusProtocol> delegate;

@property(strong,nonatomic)NSMutableArray *debitCardBankDetails;
@property(strong,nonatomic)NSMutableArray *netBankingBankDetails;
@property(strong,nonatomic)NSMutableArray *debitCardBankNames;
@property(strong,nonatomic)NSMutableArray *netBankingBankNames;
@property(nonatomic,assign)BOOL isQuickPay;
@property (strong,nonatomic)CLLocationManager *locationManager;
- (id)initWithMessage:(NSString*)message_ andToken:(NSString*)token_ andEmail:(NSString *)email_ andMobile:(NSString *)mobile_ andAmount:(float)amount_;
- (id)initWithMessage:(NSString*)message_ andToken:(NSString*)token_ andEmail:(NSString *)email_ andMobile:(NSString *)mobile_ andAmount:(float)amount_ setOrientation:(NSInteger)orientation;



@end
