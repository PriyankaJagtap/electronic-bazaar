//
//  WSConstant.h
//  TestPayZappStaging
//
//  Created by WIBMO on 03/02/16.
//  Copyright © 2016 WIBMO. All rights reserved.
//

#define WSConstant_h

#define  PAYMENT_TYPE_ALL  @"*"
#define  PAYMENT_TYPE_VISA  @"w.ds.pt.card_visa"
#define  PAYMENT_TYPE_MASTERCARD  @"w.ds.pt.card_mastercard"
#define  PAYMENT_TYPE_WIBMOWALLET  @"w.ds.pt.card_wallet"
#define  PAYMENT_TYPE_NONE  @"w.ds.pt.none"
#define  TXN_AMOUNT_KNOWN @"true";
#define  CHARGE_LATER @"false";
#define  STAGING_URL                       @"https://wallet.pc.enstage-sas.com/"   // Staging
#define  PRODUCTION_URL                     @"https://www.wibmo.com/"               // Production