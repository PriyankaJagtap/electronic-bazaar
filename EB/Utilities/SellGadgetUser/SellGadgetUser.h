//
//  SellGadgetUser.h
//  EB
//
//  Created by Neosoft on 8/2/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SellGadgetUser : NSObject

@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *customerId;

@property (nonatomic, strong)NSString *email;
+(SellGadgetUser *)getSellGadgetUserFromNsuserdefaults;
+(void)saveSellGadgetUserToNsuserdefaults:(SellGadgetUser *)obj;

@end
