//
//  SellGadgetUser.m
//  EB
//
//  Created by Neosoft on 8/2/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "SellGadgetUser.h"

@implementation SellGadgetUser

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.password forKey:@"password"];
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.customerId forKey:@"customerId"];

}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.email = [decoder decodeObjectForKey:@"email"];
        self.password = [decoder decodeObjectForKey:@"password"];
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.customerId = [decoder decodeObjectForKey:@"customerId"];

    }
    return self;
}

+(SellGadgetUser *)getSellGadgetUserFromNsuserdefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:SELL_GADGET_USER];
    SellGadgetUser *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}
+(void)saveSellGadgetUserToNsuserdefaults:(SellGadgetUser *)obj
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:obj];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:SELL_GADGET_USER];
    [defaults synchronize];
}

@end
