//
//  UIWindow+YUBottomPoper.m
//  YUANBAOAPP
//
//  Created by yxy on 14/11/20.
//  Copyright (c) 2014年 ATAW. All rights reserved.
//

#import "UIWindow+YUBottomPoper.h"
#import "YUDarkGlassView.h"
#import "UIView+YUStyle.h"
#import "yConst.h"
@implementation UIWindow (YUBottomPoper)

#pragma mark showPopWithButtonTitles
-(void)showPopWithButtonTitles:(NSArray *)titles styles:(NSArray *)styles whenButtonTouchUpInSideCallBack:(_int_type_block)callBack{

    YUBottomPopSelctView * globlShareBottomView = [YUBottomPopSelctView share] ;
    if (globlShareBottomView == nil){
    
        return;
        
    }
    if(titles.count != styles.count){
    
        //NSLog(@"#### - ");
        
    }
    
    globlShareBottomView.whenSelectViewTouchUpInside = callBack;
    YUDarkGlassView * glassView  = [YUDarkGlassView share];
    [self addSubview:glassView];
    
    [globlShareBottomView removeFromSuperview];
    
    [globlShareBottomView ySetAutoSizeWithButtonTitles:titles styles:styles];
    [self popSelectView:globlShareBottomView];

}
#pragma mark showPopWithButtonTitles
-(void)showPopWithButtonTitles:(NSArray *)titles styles:(NSArray *)styles deledge:(id<YUBottomPopSelctViewDeledge>)deledge{
    YUBottomPopSelctView * globlShareBottomView = [YUBottomPopSelctView share] ;
    if (globlShareBottomView == nil){
    
        return;
        
    }

    globlShareBottomView.deldge = deledge;
    
    YUDarkGlassView * glassView  = [YUDarkGlassView share];
    
    [self addSubview:glassView];
    [globlShareBottomView ySetAutoSizeWithButtonTitles:titles styles:styles];
    [self popSelectView:globlShareBottomView];
    
}

#pragma mark popSelectView
-(void)popSelectView:(UIView *)view {

    [self addSubview:view];
    [view y_setLeft:0];
    
    [view y_setTop:ScreenHeight];
  
    [UIView animateWithDuration:0.3 animations:^{
        
        [YUDarkGlassView share].alpha = 0.8 ;
         view.frame = CGRectMake(0, ScreenHeight -  view.frame.size.height, view.frame.size.width, view.frame.size.height);
        
    }];
    
   
    
}

#pragma mark disMissPopSelectView
-(void)disMissPopSelectView{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [YUBottomPopSelctView share].frame = CGRectMake(0, ScreenHeight, [YUBottomPopSelctView share].frame.size.width, [YUBottomPopSelctView share].frame.size.height);
        [YUDarkGlassView share].alpha = 0 ;
         
        
    } completion:^(BOOL finished) {
        [[YUDarkGlassView share] removeFromSuperview];
        [[YUBottomPopSelctView share] removeFromSuperview];
        
    }];

}

@end

