//
//  CustomUITextField.m
//  EB
//
//  Created by webwerks on 28/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "CustomUITextField.h"
#import <QuartzCore/QuartzCore.h>

@implementation CustomUITextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (id)initWithCoder:(NSCoder*)coder {
        self = [super initWithCoder:coder];
        
        if (self) {
                self.clipsToBounds = YES;
                [self setRightViewMode:UITextFieldViewModeUnlessEditing];
                UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, self.frame.size.height)];
                self.leftViewMode = UITextFieldViewModeAlways;
                self.leftView = wrapView;
                self.layer.borderWidth = 1.0f;
                self.layer.borderColor = [UIColor colorForBorder].CGColor;
        }
        return self;
}

@end
