//
//  GradientColorClass.m
//  EB
//
//  Created by webwerks on 29/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "GradientColorClass.h"
#import <QuartzCore/QuartzCore.h>

@implementation GradientColorClass

static GradientColorClass *sharedInstance;

+(GradientColorClass*) sharedInstance
{
        if (!sharedInstance)
        {
                
                sharedInstance = [[GradientColorClass alloc] init];
        }
        
        return sharedInstance;
}



-(void)setGradientBackgroundToButton:(UIButton *)btn
{
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = btn.bounds;
        //    gradient.colors = @[(id)[[AppDelegate getAppDelegateObj] colorWithHexString:@"3fc3f5"].CGColor, (id)[[AppDelegate getAppDelegateObj] colorWithHexString:@"0c7ea4"].CGColor];
        
        gradient.colors = @[(id)[[AppDelegate getAppDelegateObj] colorWithHexString:@"13447e"].CGColor, (id)[[AppDelegate getAppDelegateObj] colorWithHexString:@"1d1d53"].CGColor];
        
        CGFloat x = 90;
        CGFloat a = pow(sin((2*M_PI*((x+0.75)/2))),2);
        CGFloat b = pow(sin((2*M_PI*((x+0.0)/2))),2);
        CGFloat c = pow(sin((2*M_PI*((x+0.25)/2))),2);
        CGFloat d = pow(sin((2*M_PI*((x+0.5)/2))),2);
        gradient.startPoint = CGPointMake(a, b);
        gradient.endPoint = CGPointMake(c, d);
        [btn.layer insertSublayer:gradient atIndex:0];
        btn.layer.cornerRadius = btn.bounds.size.height/2;
        btn.clipsToBounds = YES;
        

}
@end
