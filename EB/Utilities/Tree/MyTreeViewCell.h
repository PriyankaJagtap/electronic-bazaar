//
//  MyTreeViewCell.h
//  MyTreeViewPrototype
//
//  Created by Jon Limjap on 4/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface MyTreeViewCell : UITableViewCell {
	UILabel *lblItem;
	UIImageView *arrowImage;
	
	int level;
	BOOL expanded;
}


@property (nonatomic, retain) IBOutlet UITextField *txtNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnMinus;

@property (nonatomic, retain) IBOutlet UILabel *lblItem;
@property (nonatomic, retain) IBOutlet UIImageView *arrowImage;

@property (nonatomic, retain) IBOutlet UIImageView *rowBg;

@property (nonatomic) int level;
@property (nonatomic) BOOL expanded;

- (id)initWithStyle:(UITableViewCellStyle)style 
	reuseIdentifier:(NSString *)reuseIdentifier 
			  level:(NSUInteger)_level 
		   expanded:(BOOL)_expanded; 

@end
