//
//  MyTreeViewCell.m
//  MyTreeViewPrototype
//
//  Created by Jon Limjap on 4/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MyTreeViewCell.h"
#import "AppDelegate.h"


#define IMG_HEIGHT_WIDTH 20
#define CELL_HEIGHT 55
#define SCREEN_WIDTH1 160//320 label width
#define LEVEL_INDENT 24
#define YOFFSET 15
#define XOFFSET 6

@interface MyTreeViewCell (Private)

- (UILabel *)newLabelWithPrimaryColor:(UIColor *)primaryColor 
						selectedColor:(UIColor *)selectedColor 
							 fontSize:(CGFloat)fontSize 
								 bold:(BOOL)bold;

@end


@implementation MyTreeViewCell

@synthesize lblItem, arrowImage;
@synthesize level, expanded;
@synthesize rowBg;

- (id)initWithStyle:(UITableViewCellStyle)style 
	reuseIdentifier:(NSString *)reuseIdentifier 
			  level:(NSUInteger)_level
		   expanded:(BOOL)_expanded {
//
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
		self.level = _level;
		self.expanded = _expanded;

		UIView *content = self.contentView;
        
        
//        [content setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:238.0/255.0 alpha:1.0]];
        
//        self.rowBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row.png"]];
//        [content addSubview:self.rowBg];

        
		self.lblItem = [self newLabelWithPrimaryColor:[UIColor colorWithRed:67.0/255.0 green:73.0/255.0 blue:85.0/255.0 alpha:1.0] selectedColor:[UIColor whiteColor] fontSize:16.0 bold:YES];
		self.lblItem.textAlignment = NSTextAlignmentLeft;
		[content addSubview:self.lblItem];
        
        self.btnPlus = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.btnPlus setFrame:CGRectMake(230, YOFFSET, 12, 12)];
//        [self.btnPlus setBackgroundImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
         [self.btnPlus setBackgroundImage:[UIImage imageNamed:@"blue_down_arrow"] forState:UIControlStateNormal];
        
        
        [content addSubview:self.btnPlus];

        
        self.btnMinus = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.btnMinus setFrame:CGRectMake(276, YOFFSET, 23, 23)];
        [self.btnMinus setBackgroundImage:[UIImage imageNamed:@"minus.png"] forState:UIControlStateNormal];
        [content addSubview:self.btnMinus];

        self.arrowImage = [[UIImageView alloc] init];
        //..
        [self.arrowImage setFrame:CGRectMake(0, 0, 16 , 16)];
        [self.arrowImage setContentMode:UIViewContentModeScaleAspectFit];
        [content addSubview:self.arrowImage];
    }
    return self;
}


#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[lblItem release];
	[arrowImage release];
	
    [super dealloc];
}

#pragma mark -
#pragma mark Other overrides

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
	
    if (!self.editing) {
		
		// get the X pixel spot
        CGFloat boundsX = contentRect.origin.x;
		CGRect frame;
		
        frame = CGRectMake((boundsX + self.level + 1) * LEVEL_INDENT,
						   YOFFSET, 
						   SCREEN_WIDTH1 - (self.level * LEVEL_INDENT),
						   CELL_HEIGHT);
//        if (isIpad){
//            self.lblItem.frame=CGRectMake(40,frame.origin.y,frame.size.width,frame.size.height);
//        }else{
            self.lblItem.frame =CGRectMake(38,frame.origin.y-3,frame.size.width + 40,frame.size.height);
 ;
        //}
        
		self.lblItem.numberOfLines = 0;
        //self.lblItem.lineBreakMode = NSLineBreakByWordWrapping;
        [self.lblItem sizeToFit];
//        //NSLog(@"frame: %@", NSStringFromCGRect(self.valueLabel.frame));
        
        [self.rowBg setFrame:CGRectMake(13, 3, 295, contentRect.size.height - 6)];
        
//		CGRect imgFrame;
//		imgFrame = CGRectMake(((boundsX + self.level + 1) * LEVEL_INDENT) - (IMG_HEIGHT_WIDTH + XOFFSET), 
//							  YOFFSET, 
//							  IMG_HEIGHT_WIDTH, 
//							  IMG_HEIGHT_WIDTH);
//		self.arrowImage.frame = imgFrame;
    }
}

#pragma mark -
#pragma mark Private category

- (UILabel *)newLabelWithPrimaryColor:(UIColor *)primaryColor 
						selectedColor:(UIColor *)selectedColor 
							 fontSize:(CGFloat)fontSize 
								 bold:(BOOL)bold {

	UILabel *newLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	newLabel.backgroundColor = [UIColor clearColor];
	newLabel.opaque = YES;
	newLabel.textColor = primaryColor;
	newLabel.highlightedTextColor = selectedColor;
	newLabel.font = [UIFont fontWithName:@"ArialMT" size:16.0f];
	newLabel.numberOfLines = 0;
	
	return newLabel;
}

@end
