//
//  GradientColorClass.h
//  EB
//
//  Created by webwerks on 29/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GradientColorClass : NSObject
-(void)setGradientBackgroundToButton:(UIButton *)btn;
+(GradientColorClass*) sharedInstance;
@end
