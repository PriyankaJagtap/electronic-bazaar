//
//  CustomView.h
//  Demo
//
//  Created by webwerks on 4/28/15.
//  Copyright (c) 2015 JO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomView : UIView

@property(nonatomic,assign) BOOL isAnimating;
@property(nonatomic, strong) UILabel *titleLbl;
@property(nonatomic, strong) UIButton *closeBtn;

-(void)moveup;
-(void)moveDown;
-(void)setupInterNet;
-(void)setupCustomView:(NSString*)title;

@end
