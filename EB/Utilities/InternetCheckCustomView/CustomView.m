
//
//  CustomView.m
//  Demo
//
//  Created by webwerks on 4/28/15.
//  Copyright (c) 2015 JO. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView{

    UIView *mainView;
}

@synthesize isAnimating;
@synthesize titleLbl;
@synthesize closeBtn;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self setupInterNetCustomView];
    }
    return self;
}


-(void)setupInterNet
{
    mainView = [[UIView alloc] initWithFrame:self.bounds];
    mainView.backgroundColor = [UIColor colorWithRed:194.0f/255.0f green:23.0f/255.0f blue:38.0f/255.0f alpha:1.0f];
    mainView.userInteractionEnabled = YES;
    
    UIImageView *imgview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NoNet"]];
    imgview.frame = CGRectMake(10,25,30,30);
    [mainView addSubview:imgview];
    
    titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(-5,30,250, 20)];
    titleLbl.text = @"No Internet Connection.";
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.numberOfLines = 0;
    titleLbl.textColor =[UIColor whiteColor];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    titleLbl.font = [UIFont fontWithName:@"ScoutCond" size:23.0f];
    [mainView addSubview:titleLbl];

    [self addSubview:mainView];

}

-(void)setupCustomView:(NSString*)title
{
    mainView = [[UIView alloc] initWithFrame:self.bounds];
    mainView.backgroundColor = [UIColor whiteColor];
    mainView.userInteractionEnabled = YES;
    
    titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(10,25,self.frame.size.width-20, 30)];
    titleLbl.text = title;
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.numberOfLines = 0;
    titleLbl.textColor =[UIColor whiteColor];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    titleLbl.font = [UIFont fontWithName:@"ScoutCond" size:20.0f];
    [mainView addSubview:titleLbl];
    
    [self addSubview:mainView];
 
}

-(void)onClickOfCloseBtn
{
    
}

-(void)moveDown
{
    mainView.hidden = NO;
    self.isAnimating = YES;
    [UIView animateWithDuration:0.6 animations:^{
        mainView.frame = CGRectMake(mainView.frame.origin.x,64,self.frame.size.width,64);
    } completion:^(BOOL finished) {
        if (finished)
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(moveup) object:nil];
        [self performSelector:@selector(moveup) withObject:nil afterDelay:2.0f];
    }];
    
}


-(void)moveup
{
    self.isAnimating = YES;
    [UIView animateWithDuration:0.6 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        mainView.frame = CGRectMake(mainView.frame.origin.x,0,self.frame.size.width,64);
    } completion:^(BOOL finished) {
        mainView.hidden = YES;
    }];
}


@end
