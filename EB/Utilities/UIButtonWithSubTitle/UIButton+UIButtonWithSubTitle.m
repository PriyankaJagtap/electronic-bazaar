//
//  UIButton+UIButtonWithSubTitle.m
//  EB
//
//  Created by Neosoft on 4/26/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UIButton+UIButtonWithSubTitle.h"

@implementation UIButton (UIButtonWithSubTitle)
- (void)setupButton:(NSString *)title image:(UIImage *)image subTitle:(NSString *) subTitle {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 5, self.bounds.size.width - 60, 20)];
    titleLabel.font = [UIFont fontWithName:@"Karla-Bold" size:15.0f];
    titleLabel.textColor = [UIColor whiteColor];
    [self addSubview:titleLabel];
    titleLabel.text = title;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(10, 10, 40, 40);

    [self addSubview:imageView];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, self.bounds.size.width - 70, 40)];
    descriptionLabel.numberOfLines = 0;
    descriptionLabel.font = [UIFont fontWithName:@"Karla-Bold" size:12.0f];
    descriptionLabel.textColor = [UIColor whiteColor];
    descriptionLabel.text = subTitle;
    [self addSubview:descriptionLabel];
}
                              
@end
