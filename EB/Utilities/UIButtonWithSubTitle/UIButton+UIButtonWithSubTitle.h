//
//  UIButton+UIButtonWithSubTitle.h
//  EB
//
//  Created by Neosoft on 4/26/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (UIButtonWithSubTitle)
- (void)setupButton:(NSString *)title image:(UIImage *)image subTitle:(NSString *) subTitle;

@end
