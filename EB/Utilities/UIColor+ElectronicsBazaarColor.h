//
//  UIColor+ElectronicsBazaarColor.h
//  EB
//
//  Created by webwerks on 28/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ElectronicsBazaarColor)
+(UIColor *)colorForBorder;
+(UIColor *)selectedMenuBackgroundColor;
+(UIColor *)statusBarColor;
+(UIColor *)blueTextColor;
+(UIColor *)greenTextColor;
+(UIColor *)grayTextColor;
+(UIColor *)gradientBottomColor;
+(UIColor *)gradientTopColor;
@end
