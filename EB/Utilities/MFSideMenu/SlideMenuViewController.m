
//
//  SlideMenuViewController.m
//  GLC
//
//  Created by Pooja on 26/08/14.
//  Copyright (c) 2014 pooja. All rights reserved.
//

#import "SlideMenuViewController.h"
#import "AppDelegate.h"
#import "SlideMenuBottomCell.h"
#import "SlideMenuCell.h"
#import "MFSideMenuContainerViewController.h"
#import "ProductListViewController.h"
#import "MFSideMenu.h"
#import "MyTreeNode.h"
#import "MyTreeViewCell.h"
#import "ShowTabbars.h"
#import "MyOrdersViewController.h"
#import "RegistrationViewController.h"
#import "LoginViewController.h"
#import "MyPurchasesViewController.h"
#import "HomeDealsViewController.h"
#import "ContactUs.h"
#import "SellGadgetUser.h"
#import "WarrantyViewController.h"
#import "VerifyGSTINViewController.h"
#import "MyAccountViewController.h"
#import "MyCartViewController.h"
#import "MyWishlist.h"
#import "VowDelightViewController.h"

//#import "treevi"

#define kFONT IS_IPAD?24.0:16.0
#define kHOME_TAG 1000
#define kSTATICLINKS 2000
#define KSTATICLOGIN 3000

static NSString *webViewBannerType = @"webview";

@interface SlideMenuViewController ()
{
    NSMutableArray *arrSlideMenu1;
    NSMutableArray *staticLinkArr;
    NSMutableArray *iconImageArray;
    NSMutableDictionary *getUserDetailDict;
    NSMutableDictionary *slideMenuDict;
    
    NSMutableDictionary *userdataDict;
    BOOL isUserLoggedIn;
    BOOL showVowDelightBool;
    MyTreeNode *treenode;
}

@end

@implementation SlideMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    iconImageArray = [NSMutableArray arrayWithObjects:@"home_icon", @"laptop_icon", @"mobile_icon", @"tablet_icon", @"camera_icon", @"desktop_icon", @"accesories_icon", @"deals_icon", nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuStateEventOccurred:) name:MFSideMenuStateNotificationEvent object:nil];
    
    _tblView.estimatedRowHeight = 40;
    _tblView.rowHeight = UITableViewAutomaticDimension;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    showVowDelightBool = NO;
    [self CallMenuListService];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Notification Methods
- (void)menuStateEventOccurred:(NSNotification *)notification
{
    MFSideMenuStateEvent event = [[[notification userInfo] objectForKey:@"eventType"] intValue];
    NSLog(@"menu items %@",arrSlideMenu1);
    
    if(event == MFSideMenuStateEventMenuWillOpen)
    {
        userdataDict = [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
        if (userdataDict.count > 0)
        {
            isUserLoggedIn =YES;
            //Profile pic
            NSString *userName;
            
            if([userdataDict valueForKey:@"firstname"])
                userName = [userdataDict valueForKey:@"firstname"];
            _Profilenamelbl.text = userName;
            
            //email
            _emailLbl.hidden = NO;
            _emailLbl.text = [userdataDict objectForKey:@"email"];
            
            // Profile Photo
            NSString *profilePhoto = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"profile_picture"]];
            
            if (profilePhoto.length > 0 && ![profilePhoto isEqualToString:@""] &&![profilePhoto isEqualToString:@"(null)"])
            {
                _ProfileImage.layer.cornerRadius = 20.0;
                _ProfileImage.clipsToBounds = YES;
                [_ProfileImage.layer setBorderColor: [[UIColor whiteColor] CGColor]];
                [_ProfileImage.layer setBorderWidth: 1.0];
                _ProfileImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:profilePhoto]]];;
            }
            else
            {
                _ProfileImage.layer.cornerRadius = 20.0;
                [_ProfileImage.layer setBorderColor: [[UIColor whiteColor] CGColor]];
                [_ProfileImage.layer setBorderWidth: 1.0];
                _ProfileImage.image = [UIImage imageNamed:@"guest_profilePic"];
            }
            
            if([CommonSettings checkIsArm])
            {
                if(showVowDelightBool)
                {
                    //  staticLinkArr = [NSMutableArray arrayWithObjects:@"Verify GSTIN",@"Vow Delight",@"Sell Your Gadget",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];
                    //2
                    staticLinkArr = [NSMutableArray arrayWithObjects:@"Verify GSTIN",@"Vow Delight",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];
                }
                else
                {
                    //  staticLinkArr = [NSMutableArray arrayWithObjects:@"Verify GSTIN",@"Sell Your Gadget",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];
                    //1
                    staticLinkArr = [NSMutableArray arrayWithObjects:@"Verify GSTIN",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];
                }
                
            }
            else
            {
                if(showVowDelightBool)
                {
                    
                    //  staticLinkArr = [NSMutableArray arrayWithObjects:@"Vow Delight",@"Sell Your Gadget",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];
                    //1 : Sell Your Gadget
                    staticLinkArr = [NSMutableArray arrayWithObjects:@"Vow Delight",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];
                }
                else
                {
                    /*
                     staticLinkArr = [NSMutableArray arrayWithObjects:@"Sell Your Gadget",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];*/
                    //2 : Sell Your Gadget
                    staticLinkArr = [NSMutableArray arrayWithObjects:@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];
                }
                
            }
        }
        [_tblView reloadData];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 1 ? staticLinkArr.count : [treenode descendantCount];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 1 ? 1.0 : 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, [self tableView:tableView heightForHeaderInSection:section])];
    
    if (section == 1)
    {
        view.frame = CGRectMake(5.0, 0.0, self.view.frame.size.width- 10, 1);
        //[view setBackgroundColor:[UIColor lightGrayColor]];
        [view setBackgroundColor:[[AppDelegate getAppDelegateObj] colorWithHexString:@"193C64"]];
    }
    else
    {
        //check header height is valid
        if([self tableView:tableView heightForHeaderInSection:section] == 0.0)
        {
            //bail
            return nil;
        }
    }
    return view;
}

- (SlideMenuBottomCell * _Nonnull)getSlideMenuBottomCell:(NSIndexPath * _Nonnull)indexPath tableView:(UITableView * _Nonnull)tableView
{
    static NSString *CellIdentifier = @"SlideMenuBottomCell";
    SlideMenuBottomCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[SlideMenuBottomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: CellIdentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.menuTitleLbl.textColor = [UIColor grayTextColor];
    //         cell.menuTitleLbl.font =isIpad?karlaFontRegular(17):karlaFontRegular(12.0);
    cell.menuTitleLbl.font =karlaFontRegular(16.0);
    cell.menuTitleLbl.text = [staticLinkArr objectAtIndex:indexPath.row];
    
    if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Verify GSTIN"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_verify_gstin_icon"];
    }
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Vow Delight"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_vow_delight_icon"];
    }
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Sell Your Gadget"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_sell_gadget_icon"];
    }
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Deals"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_deal_icon"];
    }
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Track your order"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_track_order_icon"];
    }
    else if ([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Whatsapp Us \"Hi\" for daily deals +919930681777"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_whatsapp_icon"];
    }
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Contact Us"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_contact_us_icon"];
    }
    //WishlistIcon
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"My Wishlist"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_wishlist_icon"];
    }
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Invite Friends"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_invite_friend_icon"];
    }
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Policy"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_legal_icon"];
    }
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Sign Up"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_sign_up_icon"];
    }
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Login"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_login_icon"];
    }
    else if([[staticLinkArr objectAtIndex:indexPath.row] isEqualToString:@"Logout"])
    {
        cell.menuImgView.image = [UIImage imageNamed:@"blue_logout_icon"];
    }
    return cell;
}

- (MyTreeViewCell * _Nonnull)getMyTreeViewCell:(NSIndexPath * _Nonnull)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    MyTreeNode *node = [[treenode flattenElements] objectAtIndex:indexPath.row + 1];
    MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier:CellIdentifier level:[node levelDepth] - 1 expanded:node.inclusive];
    
    cell.lblItem.textColor = [UIColor grayTextColor];
    cell.lblItem.font =karlaFontRegular(15.0);
    
    if (indexPath.row == 0)
    {
        cell.lblItem.text =@"Home" ;
        cell.arrowImage.frame = CGRectMake(10, 10, 18, 18);
        cell.arrowImage.image = [UIImage imageNamed:@"blue_home_icon"];
        cell.arrowImage.contentMode = UIViewContentModeScaleAspectFit;
        cell.btnPlus.hidden = YES;
    }
    else
    {
        cell.lblItem.text =[NSString stringWithFormat:@"%@",[node.menuDataDict objectForKey:@"name"]] ;
        cell.arrowImage.frame = CGRectMake(10, 10, 18, 18);
        cell.arrowImage.contentMode = UIViewContentModeScaleAspectFit;
        if ([node.menuDataDict objectForKey:@"sub_cat"])
        {
            cell.btnPlus.hidden = NO;
        }
        else
        {
            cell.btnPlus.hidden = YES;
            cell.arrowImage.image = [UIImage imageNamed:@"blue_dot_icon"];
            cell.arrowImage.frame = CGRectMake(10, 10, 15, 15);
            cell.lblItem.font =karlaFontRegular(12.0);
        }
        
        if (node.inclusive)
        {
            [cell.btnPlus setBackgroundImage:[UIImage imageNamed:@"blue_up_arrow"] forState:UIControlStateNormal];
            cell.contentView.backgroundColor = [UIColor selectedMenuBackgroundColor];
        }
        else
        {
            [cell.btnPlus setBackgroundImage:[UIImage imageNamed:@"blue_down_arrow"] forState:UIControlStateNormal];
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }
        
        if ([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Laptops"])
        {
            cell.arrowImage.image = [UIImage imageNamed:@"blue_laptop_icon"];
        }
        else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Mobiles"])
        {
            cell.arrowImage.image = [UIImage imageNamed:@"blue_mobile_icon"];
        }
        else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Accessories"])
        {
            cell.arrowImage.image = [UIImage imageNamed:@"blue_accessories_icon"];
        }
        else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Tablets"])
        {
            cell.arrowImage.image = [UIImage imageNamed:@"blue_tablets_icon"];
        }
        else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"Desktops"])
        {
            cell.arrowImage.image = [UIImage imageNamed:@"blue_desktop_icon"];
        }
        else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"EPP Program"])
        {
            cell.lblItem.font = karlaFontRegular(16.0);
            cell.arrowImage.image = [UIImage imageNamed:@"blue_laptop_icon"];
        }
        else if([[node.menuDataDict objectForKey:@"name"] isEqualToString:@"E-Waste"])
        {
            cell.lblItem.font = karlaFontRegular(16.0);
            cell.arrowImage.image = [UIImage imageNamed:@"blue_ewaste_icon"];
            self.eWasteURL = [node.menuDataDict objectForKey:@"url"];
        }
    }
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        return [self getSlideMenuBottomCell:indexPath tableView:tableView];
    }
    else
    {
        return [self getMyTreeViewCell:indexPath];
    }
    return nil;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tblView deselectRowAtIndexPath:[self.tblView indexPathForSelectedRow] animated:YES];
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            // 1111
            [self navigateToController:kHOME_TAG cat_val:nil];
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
        }
        else
        {
            MyTreeNode *node = [[treenode flattenElements] objectAtIndex:indexPath.row + 1];
            if (!node.hasChildren)
            {
                NSString *strTagVal = [NSString stringWithFormat:@"%@",[node.menuDataDict objectForKey:@"id"]];// sending cat_id for category screen
                NSString *strCatVal = [NSString stringWithFormat:@"%@",[node.menuDataDict objectForKey:@"name"]];// sending cat_id for category screen
                
                NSString *is_ibm = [node.menuDataDict objectForKey:@"is_ibm"];
                
                if ([is_ibm isEqualToString:@"1"])
                {
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"is_ibm"] isEqualToString:@"Yes"])
                    {
                        [self navigateToController:[strTagVal intValue] cat_val:strCatVal];
                        [self.slideMenuDelegate SlidingMethod:[strTagVal intValue] cat_name:strCatVal];
                        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
                        return;
                    }
                    else
                    {
                        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
                        [APP_DELEGATE navigateToIBMLogin:CATEGORY withCategoryValue:strCatVal withCategoryId:strTagVal];
                    }
                }
                else
                {
                    [self navigateToController:[strTagVal intValue] cat_val:strCatVal];
                    [self.slideMenuDelegate SlidingMethod:[strTagVal intValue] cat_name:strCatVal];
                    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
                    return;
                }
            }
            node.inclusive = !node.inclusive;
            [treenode flattenElementsWithCacheRefresh:YES];
            [_tblView reloadData];
        }
    }
    else if (indexPath.section == 1)
    {
        if([CommonSettings checkIsArm])
        {
            if(indexPath.row == 0)
            {
                ShowTabbars *tabBarController = self.menuContainerViewController.centerViewController;
                UINavigationController *navigationController = (UINavigationController *)tabBarController.selectedViewController;
                UIStoryboard *storyboard =[UIStoryboard storyboardWithName:@"Product" bundle:nil];
                VerifyGSTINViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VerifyGSTINViewController"];
                [navigationController pushViewController:vc animated:YES];
            }
            
            if(showVowDelightBool)
            {
                switch (indexPath.row)
                {
                    case 1:
                        [self navigateToController:kSTATICLINKS cat_val:@"Vow Delight"];
                        break;
                        //                                        case 2:
                        //                                                [CommonSettings navigateToSellYourGadgetView];
                        //                                                break;
                    case 2:
                        [self navigateToController:kSTATICLINKS cat_val:@"hotdeals"];
                        break;
                    case 3:
                        [self navigateToController:kSTATICLINKS cat_val:@"My Wishlist"];
                        break;
                    case 4:
                        [self navigateToController:kSTATICLINKS cat_val:@"trackorder"];
                        break;
                    case 5:
                        [self shareUsingWhatsApp];
                        break;
                    case 6:
                        [self navigateToController:kSTATICLINKS cat_val:@"Contact Us"];
                        break;
                    case 7:
                        [self handleInviteUs];
                        break;
                    case 8:
                        [self navigateToController:kSTATICLINKS cat_val:@"Policy"];
                        break;
                    case 9:
                    {
                        isUserLoggedIn ? [self navigateToController:kSTATICLINKS cat_val:@"logout"] : [self navigateToController:kSTATICLINKS cat_val:@"register"];
                    }
                        break;
                    case 10 :
                    {
                        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
                        [objAppdelegate.tabBarObj hideTabbar];
                        [objAppdelegate navigateToLoginViewController];
                    }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                //                        if (indexPath.row == 1) {
                //                                [self navigateToController:kSTATICLINKS cat_val:@"Vow Delight"];
                //
                //                        }
                //                                if (indexPath.row == 1)
                //                                {
                //                                        [CommonSettings navigateToSellYourGadgetView];
                //                                }
                if (indexPath.row == 1)
                {
                    [self navigateToController:kSTATICLINKS cat_val:@"hotdeals"];
                }
                else if (indexPath.row == 2)
                {
                    [self navigateToController:kSTATICLINKS cat_val:@"My Wishlist"];
                }
                else if (indexPath.row == 3)
                {
                    [self navigateToController:kSTATICLINKS cat_val:@"trackorder"];
                }
                else if (indexPath.row == 4)
                {
                    [self shareUsingWhatsApp];
                }
                else if (indexPath.row == 5)
                {
                    [self navigateToController:kSTATICLINKS cat_val:@"Contact Us"];
                }
                else if (indexPath.row == 6)
                {
                    [self handleInviteUs];
                }
                else if (indexPath.row == 7)
                {
                    [self navigateToController:kSTATICLINKS cat_val:@"Policy"];
                }
                else if (indexPath.row == 8)
                {
                    isUserLoggedIn ? [self navigateToController:kSTATICLINKS cat_val:@"logout"] : [self navigateToController:kSTATICLINKS cat_val:@"register"];
                }
                else if (indexPath.row == 9)
                {
                    AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
                    [objAppdelegate.tabBarObj hideTabbar];
                    [objAppdelegate navigateToLoginViewController];
                }
            }
        }
        else
        {
            if(showVowDelightBool)
            {
                switch (indexPath.row)
                {
                    case 0:
                        [self navigateToController:kSTATICLINKS cat_val:@"Vow Delight"];
                        break;
                        //                                        case 1:
                        //                                                [CommonSettings navigateToSellYourGadgetView];
                        //                                                break;
                    case 1:
                        [self navigateToController:kSTATICLINKS cat_val:@"hotdeals"];
                        break;
                    case 2:
                        [self navigateToController:kSTATICLINKS cat_val:@"My Wishlist"];
                        break;
                    case 3:
                        [self navigateToController:kSTATICLINKS cat_val:@"trackorder"];
                        break;
                    case 4:
                        [self shareUsingWhatsApp];
                        break;
                    case 5:
                        [self navigateToController:kSTATICLINKS cat_val:@"Contact Us"];
                        break;
                    case 6:
                        [self handleInviteUs];
                        break;
                    case 7:
                        [self navigateToController:kSTATICLINKS cat_val:@"Policy"];
                        break;
                    case 8:
                    {
                        isUserLoggedIn ? [self navigateToController:kSTATICLINKS cat_val:@"logout"] : [self navigateToController:kSTATICLINKS cat_val:@"register"];
                    }
                        break;
                    case 9 :
                    {
                        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
                        [objAppdelegate.tabBarObj hideTabbar];
                        [objAppdelegate navigateToLoginViewController];
                    }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                //                        if (indexPath.row == 0) {
                //                                [self navigateToController:kSTATICLINKS cat_val:@"Vow Delight"];
                //
                //                        }
                //                        else
                //                                if (indexPath.row == 0)
                //                                {
                //[CommonSettings navigateToSellYourGadgetView];
                //                                }
                
                if (indexPath.row == 0)
                {
                    [self navigateToController:kSTATICLINKS cat_val:@"hotdeals"];
                }
                else if (indexPath.row == 1)
                {
                    [self navigateToController:kSTATICLINKS cat_val:@"My Wishlist"];
                }
                else if (indexPath.row == 2)
                {
                    [self navigateToController:kSTATICLINKS cat_val:@"trackorder"];
                }
                else if (indexPath.row == 3)
                {
                    [self shareUsingWhatsApp];
                }
                else if (indexPath.row == 4)
                {
                    [self navigateToController:kSTATICLINKS cat_val:@"Contact Us"];
                }
                else if (indexPath.row == 5)
                {
                    [self handleInviteUs];
                }
                else if (indexPath.row == 6)
                {
                    [self navigateToController:kSTATICLINKS cat_val:@"Policy"];
                }
                else if (indexPath.row == 7)
                {
                    isUserLoggedIn ? [self navigateToController:kSTATICLINKS cat_val:@"logout"] : [self navigateToController:kSTATICLINKS cat_val:@"register"];
                }
                else if (indexPath.row == 8)
                {
                    AppDelegate *objAppdelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
                    [objAppdelegate.tabBarObj hideTabbar];
                    [objAppdelegate navigateToLoginViewController];
                }
            }
        }
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
    }
}

-(void)shareUsingWhatsApp
{
    NSURL *whatsappURL = [NSURL URLWithString:@"https://api.whatsapp.com/send?phone=+919930681777&text=Hi"];
    
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
    else
    {
        [[AppDelegate getAppDelegateObj] showAlert:@"" withMessage:@" WhatsApp is not installed on your device."];
    }
}

-(void)handleInviteUs
{
    ShowTabbars *tabBarController = self.menuContainerViewController.centerViewController;
    UINavigationController *objNavigation = (UINavigationController *)tabBarController.selectedViewController;
    
    NSString *textToshare=@"Check this App: Electronic Bazaar";
    NSArray *objectsToShare = @[textToshare,App_Link];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypePostToWeibo, UIActivityTypePrint, UIActivityTypeSaveToCameraRoll, UIActivityTypeAssignToContact, UIActivityTypeAirDrop];
    
    activityVC.excludedActivityTypes = excludeActivities;
    if (checkIsIpad)
    {
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.width/2, 100, 100) inView:objNavigation.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [self presentViewController:activityVC animated:YES completion:nil];
    }
}

-(void)navigateToController:(int)tag cat_val:(NSString *)catval
{
    ShowTabbars *tabBarController = self.menuContainerViewController.centerViewController;
    UINavigationController *navigationController1 = (UINavigationController *)tabBarController.selectedViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    if (tag == 1000)
    {
        AppDelegate *appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [appdelegate.tabBarObj TabClickedIndex:0];
    }
    else if (tag == 2000)
    {
        if([catval isEqualToString:@"Vow Delight"])
        {
            if ([CommonSettings isLoggedInUser])
            {
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
                VowDelightViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"VowDelightViewController"];
                [navigationController1 pushViewController:vc animated:YES];
            }
            else
            {
                AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [appDelegate navigateToLogin:VOW_DELIGHT];
                return;
            }
        }
        else if ([catval isEqualToString:@"hotdeals"])
        {
            // HotDeals
            dispatch_async(dispatch_get_main_queue(), ^{
                [[AppDelegate getAppDelegateObj].tabBarObj TabClickedIndex:3];
                
            });
        }
        else if ([catval isEqualToString:@"My Wishlist"])
        {
            if (![CommonSettings isLoggedInUser])
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please login to view Wishlist." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                     {
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                             AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                             [appDelegate navigateToLogin:MY_WISHLIST];
                                             return;
                                         });
                                     }];
                [alert addAction:ok];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:alert animated:YES completion:nil];
                });
            }
            else
            {
                //[APP_DELEGATE.tabBarObj TabClickedIndex:2];
                MyWishlist *myWishlist = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyWishlist"];
                [navigationController1 pushViewController:myWishlist animated:YES];
                //[self.navigationController pushViewController:myCart animated:YES];
            }
        }
        else if ([catval isEqualToString:@"featuredproducts"])
        {
            // featuredproducts
            ProductListViewController *prodListObj = [storyboard instantiateViewControllerWithIdentifier:@"ProductListViewController"];
            prodListObj.viewAllListTitle.text = @"Featured Products";
            prodListObj.type = @"featuredproducts";
            prodListObj.page_no = @"1";
            prodListObj.page_size = @"5";
            NSArray *controllers = [NSArray arrayWithObject:prodListObj];
            navigationController1.viewControllers = controllers;
        }
        else if ([catval isEqualToString:@"bestsellers"])
        {
            // featuredproducts
            ProductListViewController *prodListObj = [storyboard instantiateViewControllerWithIdentifier:@"ProductListViewController"];
            prodListObj.viewAllListTitle.text = @"Best Sellers";
            prodListObj.type = @"bestsellers";
            prodListObj.page_no = @"1";
            prodListObj.page_size = @"5";
            
            NSArray *controllers = [NSArray arrayWithObject:prodListObj];
            navigationController1.viewControllers = controllers;
        }
        else if ([catval isEqualToString:@"trackorder"])
        {
            // if ([CommonSettings isLoggedInUser]) {
            MyOrdersViewController *myOrder =[storyboard instantiateViewControllerWithIdentifier:@"MyOrdersViewController"];
            [navigationController1 pushViewController:myOrder animated:YES];
            if(!([navigationController1.visibleViewController isKindOfClass:[MyOrdersViewController class]]))
            {
                [navigationController1 pushViewController:myOrder animated:YES];
            }
            //                        }
            //                        else{
            ////                                AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            ////                                [appDelegate navigateToLoginViewController];
            //                                [APP_DELEGATE navigateToLogin:TRACK_ORDER];
            //                        }
            
        }
        else if ([catval isEqualToString:@"Contact Us"])
        {
            ContactUs *myAddressObj = [storyboard instantiateViewControllerWithIdentifier:@"ContactUs"];
            if(!([navigationController1.visibleViewController isKindOfClass:[ContactUs class]]))
            {
                [navigationController1 pushViewController:myAddressObj animated:YES];
            }
        }
        else if ([catval isEqualToString:@"Policy"])
        {
            NSString *url = @"http://electronicsbazaar.com/mobile-app/privacy-policy.html";
            NSString *title = @"Policy";
            WarrantyViewController *objVc = [storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
            objVc.webViewUrl = url;
            objVc.titleLabel = title;
            objVc.bannerType = @"webview";
            [navigationController1 pushViewController:objVc animated:YES];
        }
        else if ([catval isEqualToString:@"logout"])
        {
            // Clear document directory
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            NSString *pushID=[defaults valueForKey:@"DEVICE_TOKEN"];
            NSString *email = [[defaults valueForKey:@"user"] valueForKey:@"email"];
            NSString *password =[[defaults valueForKey:@"user"] valueForKey:@"password"];
            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
            NSString *is_ibm = [defaults valueForKey:@"is_ibm"];
            //for sell your gadget
            //                        SellGadgetUser *objSellGadgetUser = [SellGadgetUser getSellGadgetUserFromNsuserdefaults];
            //                        NSString *sellYourGadgetLoginUserID = [defaults valueForKey:SELL_GADGET_LOGIN_USERID];
            
            NSNumber *isRetailer;
            if([defaults valueForKey:IS_RETAILER])
            {
                isRetailer = [defaults valueForKey:IS_RETAILER];
            }
            
            [defaults removePersistentDomainForName:appDomain];
            [defaults setObject:[NSNumber numberWithBool:YES] forKey:SHOW_GSTIN];
            [defaults setObject:pushID forKey:@"DEVICE_TOKEN"];
            [defaults setObject:email forKey:@"email"];
            [defaults setObject:password forKey:@"password"];
            if(isRetailer != nil)
            {
                [defaults setObject:isRetailer forKey:IS_RETAILER];
            }
            
            //for sell your gadget
            //                        [SellGadgetUser saveSellGadgetUserToNsuserdefaults:objSellGadgetUser];
            //                        [defaults setValue:sellYourGadgetLoginUserID forKey:SELL_GADGET_LOGIN_USERID];
            //                        [defaults synchronize];
            
            [APP_DELEGATE setEBPinGenerateCount];
            [is_ibm isEqualToString:@"Yes"] ? [APP_DELEGATE navigateToIBMLoginViewController] : [APP_DELEGATE navigateToLoginViewController];
            
        }
        else if ([catval isEqualToString:@"register"])
        {
            [APP_DELEGATE navigateToRegistration];
        }
    }
    
    else if ([catval isEqualToString:@"Login"])
    {
        LoginViewController *objLoginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
        objLoginVC.isSellGadgetBannerClicked = YES;
        [navigationController1 pushViewController:objLoginVC animated:YES];
    }
    else
    {
        if ([catval isEqualToString:@"E-Waste"])
        {
            WarrantyViewController *objVc = [storyboard instantiateViewControllerWithIdentifier:@"WarrantyViewController"];
            objVc.webViewUrl = self.eWasteURL;
            objVc.titleLabel =  @"E-Waste";
            objVc.bannerType = @"webview";
            [navigationController1 pushViewController:objVc animated:YES];
        }
        else
        {
            ElectronicsViewController *electronicviewObjc = [storyboard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
            NSString *cat_id = [NSString stringWithFormat:@"%d",tag];
            electronicviewObjc.categoryVal = catval;
            electronicviewObjc.category_id = cat_id;
            [navigationController1 pushViewController:electronicviewObjc animated:YES];
        }
    }
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
}

#pragma mark - drawTreeNodeView
-(void)drawTreeNodeView:(NSMutableArray *)treeviewArr
{
    treenode = [[MyTreeNode alloc] initWithValue:@"Root"];
    
    for(int i=0;i<treeviewArr.count+1;i++)
    {
        if (i == 0)
        {
            NSString *titleNode = [NSString stringWithFormat:@"Home"];
            MyTreeNode *node=[[MyTreeNode alloc]initWithValue:titleNode];
            node.value = [NSString stringWithFormat:@"%d",kHOME_TAG];
            [treenode addChild:node];
            node.inclusive=NO;
        }
        else
        {
            NSString *titleNode = [NSString stringWithFormat:@"%@",[[treeviewArr objectAtIndex:i-1]objectForKey:@"name"]];
            MyTreeNode *node=[[MyTreeNode alloc]initWithValue:titleNode];
            node.value = [[treeviewArr objectAtIndex:i-1]objectForKey:@"name"];
            node.menuDataDict = [treeviewArr objectAtIndex:i-1];
            NSMutableArray *subMenuArr=[NSMutableArray new];
            subMenuArr = [[treeviewArr objectAtIndex:i-1]objectForKey:@"sub_cat"];
            
            if (subMenuArr.count > 0)
            {
                for(int j=0;j<subMenuArr.count;j++)
                {
                    NSString *submenuChildtitle = [NSString stringWithFormat:@"%@",[[subMenuArr objectAtIndex:j] objectForKey:@"name"]];
                    MyTreeNode *childnode=[[MyTreeNode alloc]initWithValue:submenuChildtitle];
                    childnode.value = [NSString stringWithFormat:@"%@",[[subMenuArr objectAtIndex:j]objectForKey:@"name"]];
                    childnode.menuDataDict = [subMenuArr objectAtIndex:j];
                    [node addChild:childnode];
                    
                }
            }
            
            [treenode addChild:node];
            node.inclusive=NO;
        }
    }
    [_tblView reloadData];
}

#pragma mark - CallMenuListService
-(void)CallMenuListService
{
    //[FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:Nil url:MENU_LIST_WS string:@"MenuListWs"];
    }
}

-(void)receivedResponseForMenuList:(id)receiveData stringResponse:(NSString *)responseType
{
    if ([responseType isEqualToString:@"success"])
    {
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        arrSlideMenu1 = [NSMutableArray new];
        arrSlideMenu1 = [jsonObject valueForKey:@"menu_item"];
        
        if ([[[jsonObject valueForKey:@"static_links"] valueForKey:@"name"] containsObject:@"Vow"])
        {
            showVowDelightBool = YES;
        }
        [[NSUserDefaults standardUserDefaults]setObject:arrSlideMenu1 forKey:@"MenuArray"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        if (arrSlideMenu1.count>0)
        {
            [self setStaticLinkArray];
            [self drawTreeNodeView:arrSlideMenu1];
        }
    }
}

-(void)setStaticLinkArray
{
    isUserLoggedIn = NO;
    userdataDict = [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
    
    if (userdataDict.count > 0)
    {
        isUserLoggedIn =YES;
        //Profile pic
        NSString *userName;
        if([userdataDict valueForKey:@"storeName"])
            userName = [userdataDict valueForKey:@"storeName"];
        _Profilenamelbl.text = userName;
        
        //email
        _emailLbl.hidden = NO;
        _emailLbl.text = [userdataDict objectForKey:@"email"];
        
        // Profile Photo
        NSString *profilePhoto = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"profile_picture"]];
        
        if (profilePhoto.length > 0 && ![profilePhoto isEqualToString:@""] &&![profilePhoto isEqualToString:@"(null)"])
        {
            _ProfileImage.layer.cornerRadius = 20.0;
            _ProfileImage.clipsToBounds = YES;
            [_ProfileImage.layer setBorderColor: [[UIColor whiteColor] CGColor]];
            [_ProfileImage.layer setBorderWidth: 1.0];
            _ProfileImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:profilePhoto]]];;
            
        }
        else
        {
            _ProfileImage.layer.cornerRadius = 20.0;
            [_ProfileImage.layer setBorderColor: [[UIColor whiteColor] CGColor]];
            [_ProfileImage.layer setBorderWidth: 1.0];
            _ProfileImage.image = [UIImage imageNamed:@"guest_profilePic"];
        }
        
        if (showVowDelightBool)
        {
            //                        staticLinkArr = [NSMutableArray arrayWithObjects:@"Vow Delight",@"Sell Your Gadget",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];
            //1: Sell Your Gadget
            staticLinkArr = [NSMutableArray arrayWithObjects: @"Vow Delight", @"Deals", @"My Wishlist", @"Track your order", @"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];
        }
        else
        {
            //                        staticLinkArr = [NSMutableArray arrayWithObjects:@"Sell Your Gadget",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Logout", nil];
            //0: Sell Your Gadget
            staticLinkArr = [NSMutableArray arrayWithObjects: @"Deals", @"My Wishlist", @"Track your order", @"Whatsapp Us \"Hi\" for daily deals +919930681777", @"Contact Us", @"Invite Friends", @"Policy", @"Logout", nil];
        }
    }
    else
    {
        _ProfileImage.layer.cornerRadius = 20.0;
        [_ProfileImage.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        [_ProfileImage.layer setBorderWidth: 1.0];
        _ProfileImage.image = [UIImage imageNamed:@"guest_profilePic"];
        _emailLbl.hidden = YES;
        if (showVowDelightBool)
        {
            //                        staticLinkArr = [NSMutableArray arrayWithObjects:@"Vow Delight",@"Sell Your Gadget",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Sign Up",@"Login", nil];
            //1 Sell Your Gadget
            staticLinkArr = [NSMutableArray arrayWithObjects: @"Vow Delight", @"Deals", @"My Wishlist", @"Track your order", @"Whatsapp Us \"Hi\" for daily deals +919930681777", @"Contact Us", @"Invite Friends", @"Policy", @"Sign Up", @"Login", nil];
        }
        else
        {
            /*
             staticLinkArr = [NSMutableArray arrayWithObjects:@"Sell Your Gadget",@"Deals",@"My Wishlist",@"Track your order",@"Whatsapp Us \"Hi\" for daily deals +919930681777",@"Contact Us",@"Invite Friends",@"Policy",@"Sign Up",@"Login", nil];
             */
            //1: Sell Your Gadget
            staticLinkArr = [NSMutableArray arrayWithObjects: @"Deals", @"My Wishlist", @"Track your order", @"Whatsapp Us \"Hi\" for daily deals +919930681777", @"Contact Us", @"Invite Friends", @"Policy", @"Sign Up", @"Login", nil];
        }
    }
}
@end
