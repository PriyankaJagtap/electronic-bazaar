//
//  RightMenuViewController.h
//  GLC
//
//  Created by Pooja on 10/09/14.
//  Copyright (c) 2014 pooja. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RightMenuOption
-(void)RightSlidingMethod:(int)tag;
@end


@interface RightMenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{

}
@property(weak,nonatomic) id rightMenuDelegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
