//
//  SlideMenuViewController.h
//  GLC
//
//  Created by Pooja on 26/08/14.
//  Copyright (c) 2014 pooja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "Webservice.h"


@protocol slideMenuOption
-(void)SlidingMethod:(int)tag cat_name:(NSString *)cat_nameStr;
@end


@interface SlideMenuViewController : UIViewController<FinishLoadingData,UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(weak,nonatomic) id slideMenuDelegate;
@property (weak, nonatomic) IBOutlet UILabel *Profilenamelbl;
@property (weak, nonatomic) IBOutlet UIImageView *ProfileImage;

@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UIButton *profileNameBtn;
@property (weak, nonatomic) NSString *eWasteURL;

-(void)navigateToController:(int)tag cat_val:(NSString *)catval;

@end
