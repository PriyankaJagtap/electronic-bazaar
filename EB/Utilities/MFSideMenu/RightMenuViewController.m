//
//  RightMenuViewController.m
//  GLC
//
//  Created by Pooja on 10/09/14.
//  Copyright (c) 2014 pooja. All rights reserved.
//
//


#import "RightMenuViewController.h"
#import "AppDelegate.h"
#import "SlideMenuCell.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "RightSlideMenuCell.h"
#import "Constant.h"
#import "ShowTabbars.h"
#import "MyWishlist.h"
#import "MyOrdersViewController.h"
#import "ContactUs.h"

#import "MyAccountViewController.h"
#import "MyAddressViewController.h"
#import "MyPurchasesViewController.h"

#import "CommonSettings.h"
#import "SellGadgetCheckOutViewController.h"
#import "LoginViewController.h"
#import "BulkOrderViewController.h"

#define BULK_ORDER @"Bulk Order"
@interface RightMenuViewController ()
{
    NSMutableArray *arrRightMenu;
    NSArray *arrRightMenuImg;
    
}
@end

@implementation RightMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    /*arrRightMenu = [NSMutableArray arrayWithObjects:@"My Account",PURCAHSE_HISTORY,BULK_ORDER,SALES_HISTORY,@"Addresses",@"Invite Friends",@"Contact Us",nil];
    arrRightMenuImg=[[NSArray alloc]initWithObjects:@"menu_my_account",@"menu_my-order",@"menu_my-order",@"sales_history",@"menu_address",@"menu_invite_friend",@"menu_contact_us",nil];*/
    
//    arrRightMenu = [NSMutableArray arrayWithObjects:@"My Account",PURCAHSE_HISTORY,SALES_HISTORY,@"Addresses",@"Invite Friends",@"Contact Us",nil];
//    arrRightMenuImg=[[NSArray alloc]initWithObjects:@"menu_my_account",@"menu_my-order",@"sales_history",@"menu_address",@"menu_invite_friend",@"menu_contact_us",nil];
    
        arrRightMenu = [NSMutableArray arrayWithObjects:@"My Account",PURCAHSE_HISTORY,@"Addresses",@"Invite Friends",@"Contact Us",nil];
        arrRightMenuImg=[[NSArray alloc]initWithObjects:@"menu_my_account",@"menu_my-order",@"menu_address",@"menu_invite_friend",@"menu_contact_us",nil];
        
    _tableView.estimatedRowHeight = 40;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrRightMenu count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"RightSlideMenuCell";
    
    RightSlideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[RightSlideMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.backgroundColor = [UIColor clearColor];
//    cell.menuImg.image=[UIImage imageNamed:[arrRightMenuImg objectAtIndex:indexPath.row]];
    
    
    //arrRightMenu = [NSMutableArray arrayWithObjects:@"My Account",PURCAHSE_HISTORY,SALES_HISTORY,@"Addresses",@"Invite Friends",@"Contact Us",nil];
    
    
    if([[arrRightMenu objectAtIndex:indexPath.row] isEqualToString:@"My Account"])
    {
        cell.menuImg.image = [UIImage imageNamed:@"blue_my_account_icon"];
    }
    else if([[arrRightMenu objectAtIndex:indexPath.row] isEqualToString:PURCAHSE_HISTORY])
    {
        cell.menuImg.image = [UIImage imageNamed:@"blue_purcahse_history_icon"];
    }
    else if([[arrRightMenu objectAtIndex:indexPath.row] isEqualToString:SALES_HISTORY])
    {
        cell.menuImg.image = [UIImage imageNamed:@"blue_sell_gadget_icon"];
    }else if([[arrRightMenu objectAtIndex:indexPath.row] isEqualToString:@"Addresses"])
    {
        cell.menuImg.image = [UIImage imageNamed:@"blue_address_icon"];
    }else if([[arrRightMenu objectAtIndex:indexPath.row] isEqualToString:@"Invite Friends"])
    {
        cell.menuImg.image = [UIImage imageNamed:@"blue_invite_friend_icon"];
    }else if([[arrRightMenu objectAtIndex:indexPath.row] isEqualToString:@"Contact Us"])
    {
        cell.menuImg.image = [UIImage imageNamed:@"blue_contact_us_icon"];
    }
    
    
    cell.menuTitleLbl.text = [arrRightMenu objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ShowTabbars *tabBarController = self.menuContainerViewController.centerViewController;
    UINavigationController *objNavigation = (UINavigationController *)tabBarController.selectedViewController;
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
     UIStoryboard *ipadStoryboard=[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    switch (indexPath.row) {
 
        case 0:
        {
            
            if (![CommonSettings isLoggedInUser]) {
                AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [appDelegate navigateToLoginViewController];
                return;
            }
            
            MyAccountViewController *myAccObjc ;
            if(checkIsIpad)
                myAccObjc = [ipadStoryboard instantiateViewControllerWithIdentifier:@"MyAccountViewController"];
            else
                    myAccObjc = [storyboard instantiateViewControllerWithIdentifier:@"MyAccountViewController"];
            
            if(!([objNavigation.visibleViewController isKindOfClass:[MyAccountViewController class]]))
            {
                [objNavigation pushViewController:myAccObjc animated:YES];
            }
            
        }
            break;

        
        case 1:
        {
            if ([CommonSettings isLoggedInUser]) {
                MyPurchasesViewController *objMyOrder;
                
                if(checkIsIpad)
                    objMyOrder=[ipadStoryboard instantiateViewControllerWithIdentifier:@"MyPurchasesViewController"];
                else
                    objMyOrder=[storyboard instantiateViewControllerWithIdentifier:@"MyPurchasesViewController"];
                    
                objMyOrder.isSelectable=NO;
                if(indexPath.row == 2)
                    objMyOrder.isSellGadgetHistory = YES;
                else
                    objMyOrder.isSellGadgetHistory = NO;
                
//                if(!([objNavigation.visibleViewController isKindOfClass:[MyPurchasesViewController class]]))
//                {
                    [objNavigation pushViewController:objMyOrder animated:YES];
                //}
            }else{
                
                AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [appDelegate navigateToLoginViewController];
                
            }
            
        }
            break;
//        case 2:
//        {
//            if ([CommonSettings isUserLoggedInSellYourGadget]) {
//                MyPurchasesViewController *objMyOrder;
//                
//                if(checkIsIpad)
//                    objMyOrder=[ipadStoryboard instantiateViewControllerWithIdentifier:@"MyPurchasesViewController"];
//                else
//                    objMyOrder=[storyboard instantiateViewControllerWithIdentifier:@"MyPurchasesViewController"];
//                
//                objMyOrder.isSelectable=NO;
//                if(indexPath.row == 2)
//                    objMyOrder.isSellGadgetHistory = YES;
//                else
//                    objMyOrder.isSellGadgetHistory = NO;
//                
//                //                if(!([objNavigation.visibleViewController isKindOfClass:[MyPurchasesViewController class]]))
//                //                {
//                [objNavigation pushViewController:objMyOrder animated:YES];
//                //}
//            }else{
//                [CommonSettings navigateToSellYourGadgetLoginView];
//            }
//            
//        }
//            break;
            
        case 2:
        {
            if (![CommonSettings isLoggedInUser]) {
                AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [appDelegate navigateToLoginViewController];
                return;
            }
            
            MyAddressViewController *myAddressObj ;
            
            if(checkIsIpad)
                myAddressObj = [ipadStoryboard instantiateViewControllerWithIdentifier:@"MyAddressViewController"];
            else
                myAddressObj = [storyboard instantiateViewControllerWithIdentifier:@"MyAddressViewController"];
                
            if(!([objNavigation.visibleViewController isKindOfClass:[MyAddressViewController class]]))
            {
                [objNavigation pushViewController:myAddressObj animated:YES];
            }
        }
            break;
//        case 5:
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rate This App" message:@"Sending you to the App Store. Please tap \"Reviews\" to rate this app." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No", nil];
//            alert.tag = 999;
//            [alert show];
//        }
//            break;
            
        case 3:
        {
            //NSLog(@"Sharing");
            [self inviteFriendForApp];
            NSString *textToshare=@"Check this App: Electronic Bazaar";
            NSArray *objectsToShare = @[textToshare,App_Link];
            
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            
            NSArray *excludeActivities = @[UIActivityTypePostToWeibo,UIActivityTypePrint,UIActivityTypeSaveToCameraRoll,UIActivityTypeAssignToContact,UIActivityTypeAirDrop];
            
            activityVC.excludedActivityTypes = excludeActivities;
            if (checkIsIpad) {
                UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
                [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.width/2, 100, 100) inView:objNavigation.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                
            }else{
                [self presentViewController:activityVC animated:YES completion:nil];
            }
            
        }
            break;
        case 4:
        {
            ContactUs *myAddressObj = [storyboard instantiateViewControllerWithIdentifier:@"ContactUs"];
            if(!([objNavigation.visibleViewController isKindOfClass:[ContactUs class]]))
            {
                [objNavigation pushViewController:myAddressObj animated:YES];
            }
        }
            break;
            
        default:
            break;
    }
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:nil];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    
}



//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    CGFloat rowHeight = 40.0;
//    return rowHeight;
//}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==999)
    {
        if (buttonIndex==0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:App_Link]];
        }
        
    }
}




#pragma mark - Utility Method

-(void)inviteFriendForApp{
    //    NSString *textToshare=@"Check this App: Electronic Bazaar";
    //    NSString *linkToShare=@"<itunes link>";
    //    NSArray *objectsToShare = @[textToshare,linkToShare];
    //
    //    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    //
    //    NSArray *excludeActivities = @[UIActivityTypePostToWeibo,UIActivityTypePrint,UIActivityTypeSaveToCameraRoll,UIActivityTypeAssignToContact,UIActivityTypeAirDrop];
    //
    //    activityVC.excludedActivityTypes = excludeActivities;
    //    if (isIpad) {
    //       UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
    //        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.width/2, 100, 100) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    //
    //    }else{
    //        
    //    }
    //    [self presentViewController:activityVC animated:YES completion:nil];
}

@end
