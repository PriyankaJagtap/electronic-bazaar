//
//  GradientBackgroundButton.h
//  EB
//
//  Created by webwerks on 28/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE

@interface GradientBackgroundButton : UIButton
{
        CAGradientLayer *gradient;
}

@property (nonatomic) IBInspectable UIColor *topColor;
@property (nonatomic) IBInspectable UIColor *bottomColor;

@end
