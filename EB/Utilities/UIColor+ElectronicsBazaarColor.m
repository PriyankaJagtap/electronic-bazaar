//
//  UIColor+ElectronicsBazaarColor.m
//  EB
//
//  Created by webwerks on 28/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UIColor+ElectronicsBazaarColor.h"

@implementation UIColor (ElectronicsBazaarColor)
+(UIColor *)colorForBorder
{
        return [UIColor colorWithRed:207.0/255 green:208.0/255 blue:211.0/255 alpha:1];
}

+(UIColor *)statusBarColor
{
        return [UIColor colorWithRed:17.0/255.0f green:34.0/255.0f blue:59/255.0f alpha:1.0];
}

+(UIColor *)selectedMenuBackgroundColor
{
        return [UIColor colorWithRed:242.0/255.0f green:242.0/255.0f blue:242.0/255.0f alpha:1.0];
}

+(UIColor *)blueTextColor
{
        return [UIColor colorWithRed:19.0/255.0f green:68.0/255.0f blue:126.0/255.0f alpha:1.0];
}

+(UIColor *)greenTextColor
{
        
        return [UIColor colorWithRed:34/255.0f green:191/255.0f blue:100/255.0f alpha:1.0];
}

+(UIColor *)grayTextColor
{
        return [UIColor colorWithRed:51.0/255.0f green:51.0/255.0f blue:51.0/255.0f alpha:1.0];
}
+(UIColor *)gradientBottomColor
{
        return [UIColor colorWithRed:29.0/255.0f green:29.0/255.0f blue:83.0/255.0f alpha:1.0];
}

+(UIColor *)gradientTopColor
{
        return [UIColor colorWithRed:19.0/255.0f green:68.0/255.0f blue:126.0/255.0f alpha:1.0];
}
@end
