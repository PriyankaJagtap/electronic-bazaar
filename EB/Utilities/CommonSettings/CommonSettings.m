

#define MY_CART_COUNT_KEY @"MY_CART_COUNT"




#import "CommonSettings.h"
#import "Reachability.h"
#import <QuartzCore/QuartzCore.h>
#import "FVCustomAlertView.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "MFSideMenuContainerViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "CustomView.h"
#import "ALAlertBanner.h"
#import "SellYourGadgetLoginViewController.h"

#import "SellYourGadgetLoginSignUpViewController.h"
#import "SellYourGadgetHomeViewController.h"
#import "SGMenuViewController.h"

@interface CommonSettings ()

@property (strong, nonatomic) CustomView *noInterNetView;

@end

@implementation CommonSettings
@synthesize userToken,noInterNetView;

static CommonSettings *sharedInstance;

+(CommonSettings*) sharedInstance
{
        if (!sharedInstance)
        {
                
                sharedInstance = [[CommonSettings alloc] init];
        }
        
        return sharedInstance;
}

-(void)showAlertTitle:(NSString *)title message :(NSString *)message
{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
}

-(void)setUpNavigationBarToNavController:(UINavigationController*)navController WithTitle:(NSString*)title
{
        [navController setNavigationBarHidden:NO];
        navController.navigationBar.translucent=normal;
        UILabel *titleLabel=[[UILabel alloc]init];
        titleLabel.text = title;
        titleLabel.font =karlaFontRegular(18.0);
        titleLabel.frame = CGRectMake(0, 0, 100, 30);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor whiteColor];
        navController.navigationItem.titleView = titleLabel;
        UIColor *bgColor=[UIColor colorWithRed:23/255.0f green:70/255.0f blue:135/255.0f alpha:1.0f];
        navController.navigationBar.barTintColor = bgColor;
        navController.navigationBar.tintColor = [UIColor whiteColor];
        
        navController.navigationBar.backItem.title =@"";
        //    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"dropdown_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemPressed)];
        //
        //    [navController.navigationItem setRightBarButtonItem:rightItem];
}

-(void)rightBarButtonItemPressed
{
        // [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

#pragma mark - Google analytics

//add track event
+(void)addGoogleAnalyticsTrackEvent:(NSString *)category label:(NSString *)label
{
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category action:@"click"  label:label value:nil] build]];
        
}

//Track screens
+(void)sendScreenName:(NSString *)strName
{
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:strName];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

+(void)eventTrackingForHomeScreen:(NSString *)withLabel
{
        if(![[CommonSettings sharedInstance] checkNullValue:withLabel])
        {
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"home-banner"
                                                                      action:@"click"
                                                                       label:withLabel
                                                                       value:nil] build]];
        }
        
}
//Set default value
+(void)setDefaultValue:(NSString *)str forKey:(NSString *)key{
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:str forKey:key];
        [defaults synchronize];
        
}


//Get default value
+(NSString *)getDefaultValueForKey:(NSString *)str{
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        return [defaults valueForKey:str];
}


+(BOOL)isLoggedInUser{
        
        int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
        
        if (userId)
                return YES;
        else
                return NO;
        
}


+(int)getUserID{
        int userId=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"user"]valueForKey:@"user_id"]intValue];
        return userId;
}



+(int)getQuoteID{
        
        int userId=[[[NSUserDefaults standardUserDefaults]valueForKey:@"QuoteID"]intValue];
        return userId;
}



+(NSString*)getUserMailID{
        NSString *userEmailId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"user"]objectForKey:@"email"];
        return userEmailId;
}

+(void)getMyCartCount{
        
        NSString *strcount=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:MY_CART_COUNT_KEY]];
        [APP_DELEGATE.tabBarObj setMyCartBadgeValue:strcount];
        
}
+(void)setMyCartcount:(NSString *)count{
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setValue:count forKey:MY_CART_COUNT_KEY];
        // [APP_DELEGATE.tabBarObj.fourBtn setMyCartBadgeValue:count];
}

+(void)showMyCartCount{
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        //[defaults setValue:count forKey:MY_CART_COUNT_KEY];
        int count=[[defaults valueForKey:MY_CART_COUNT_KEY]integerValue];
        [APP_DELEGATE.tabBarObj setMyCartBadgeValue:[NSString stringWithFormat:@"%d",count]];
        
        
}

+(void)incrementMyCartCount{
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int count=[[defaults valueForKey:MY_CART_COUNT_KEY]intValue];
        NSString *strcount=[NSString stringWithFormat:@"%d",count+1];
        [defaults setValue:[NSNumber numberWithInteger:count+1] forKey:MY_CART_COUNT_KEY];
        [APP_DELEGATE.tabBarObj setMyCartBadgeValue:strcount];
}


+(void)incrementMyCartCountByAmount:(int)amount{
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int count=[[defaults valueForKey:MY_CART_COUNT_KEY]intValue];
        NSString *strcount=[NSString stringWithFormat:@"%d",count+ amount];
        [defaults setValue:[NSNumber numberWithInteger:count+amount] forKey:MY_CART_COUNT_KEY];
        [APP_DELEGATE.tabBarObj setMyCartBadgeValue:strcount];
}

+(void)removeMyCart{
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setValue:[NSNumber numberWithInteger:0] forKey:MY_CART_COUNT_KEY];
        [APP_DELEGATE.tabBarObj setMyCartBadgeValue:@"0"];
}

- (void)onPurchaseCompleted {
        
        // Assumes a tracker has already been initialized with a property ID, otherwise
        // this call returns null.
        id tracker = [[GAI sharedInstance] defaultTracker];
        
        
        
        [tracker send:[[GAIDictionaryBuilder createTransactionWithId:@"0_123456"             // (NSString) Transaction ID
                                                         affiliation:@"In-app Store"         // (NSString) Affiliation
                                                             revenue:@2.16F                  // (NSNumber) Order revenue (including tax and shipping)
                                                                 tax:@0.17F                  // (NSNumber) Tax
                                                            shipping:@0                      // (NSNumber) Shipping
                                                        currencyCode:@"USD"] build]];        // (NSString) Currency code
        
        
        [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId:@"0_123456"         // (NSString) Transaction ID
                                                                    name:@"Space Expansion"  // (NSString) Product Name
                                                                     sku:@"L_789"            // (NSString) Product SKU
                                                                category:@"Game expansions"  // (NSString) Product category
                                                                   price:@1.9F               // (NSNumber) Product price
                                                                quantity:@1                  // (NSInteger) Product quantity
                                                            currencyCode:@"USD"] build]];    // (NSString) Currency code
        
}





#pragma mark - adjust image
+(UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height {
        CGFloat oldWidth = image.size.width;
        CGFloat oldHeight = image.size.height;
        
        CGFloat scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
        
        CGFloat newHeight = oldHeight * scaleFactor;
        CGFloat newWidth = oldWidth * scaleFactor;
        CGSize newSize = CGSizeMake(newWidth, newHeight);
        
        return [CommonSettings imageWithImage:image scaledToSize:newSize];
}

+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size {
        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
                UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
        } else {
                UIGraphicsBeginImageContext(size);
        }
        [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return newImage;
}


#pragma mark - check arm user
+(bool)checkIsArm
{
        //        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        //        NSDictionary *userDic = [defaults valueForKey:@"user"];
        //        if([[userDic valueForKey:@"is_arm"] boolValue])
        //                return true;
        //        else
        return false;
}

#pragma mark - get app current version
+(NSString *)getAppCurrentVersion
{
        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
        
        return infoDictionary[@"CFBundleShortVersionString"];
}
#pragma mark - check for app update.
+(BOOL) needsUpdate{
        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString* appID = infoDictionary[@"CFBundleIdentifier"];
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
        NSData* data = [NSData dataWithContentsOfURL:url];
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if ([lookup[@"resultCount"] integerValue] == 1){
                NSString* appStoreVersion = lookup[@"results"][0][@"version"];
                NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
                if (![appStoreVersion isEqualToString:currentVersion]){
                        NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
                        return YES;
                }
        }
        return NO;
}

+(NSString *) getAppstoreVersion{
        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString* appID = infoDictionary[@"CFBundleIdentifier"];
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
        NSData* data = [NSData dataWithContentsOfURL:url];
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if ([lookup[@"resultCount"] integerValue] == 1){
                NSString* appStoreVersion = lookup[@"results"][0][@"version"];
                return appStoreVersion;
        }
        return @"";
}
#pragma mark - add tool bar
+(UIToolbar *)setToolBarNumberKeyBoard:(id)sender
{
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:sender action:@selector(doneButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Done" forState:UIControlStateNormal];
        button.frame = CGRectMake(0 ,0,60,40);
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 40)];
        [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
        
        return toolbar;
}

#pragma mark - format price
-(NSString *)formatPrice:(float)price
{
        NSString *rupee=@"\u20B9";
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSString *formattedPrice = [formatter stringFromNumber:[NSNumber numberWithFloat:price]];
        return [NSString stringWithFormat:@"%@ %@",rupee,formattedPrice];
        
}

-(NSString *)formatIntegerPrice:(NSInteger)price
{
        NSString *rupee=@"\u20B9";
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSString *formattedPrice = [formatter stringFromNumber:[NSNumber numberWithInteger:price]];
        return [NSString stringWithFormat:@"%@ %@",rupee,formattedPrice];
        
}

#pragma mark - check null value
-(BOOL)checkNullValue:(NSString *)str
{
        if([str isKindOfClass:[NSNull class]])
                return YES;
        else
                return NO;
        
}


#pragma mark - validate pan number
+(BOOL) validatePanCardNumber: (NSString *) cardNumber {
        NSString *emailRegex = @"^[A-Z]{5}[0-9]{4}[A-Z]$";
        NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        return [cardTest evaluateWithObject:cardNumber];
}

#pragma mark - validate pan number
+(BOOL) validateGSTNumber: (NSString *) gstNumber {
        NSString *emailRegex = @"^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]$[0-9]$[A-Z]$[0-9]$";
        NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        return [cardTest evaluateWithObject:gstNumber];
}


#pragma mark - set shadow to the view
-(void)setShadow:(UIView *)view
{
        view.layer.masksToBounds = NO;
        view.layer.shadowOffset = CGSizeMake(0, 0);
        view.layer.shadowRadius = 2;
        view.layer.shadowOpacity = 0.5;
        view.layer.shadowColor=[UIColor darkGrayColor].CGColor;
}

#pragma mark - set label
-(void)setLabelBorder_Inset:(UILabel *)label
{
        label.layer.cornerRadius = 1;
        label.layer.borderWidth = 1;
        label.layer.borderColor = [UIColor darkGrayColor].CGColor;
        UIEdgeInsets myLabelInsets = {10, 10, 10, 10};
        [label drawTextInRect:UIEdgeInsetsInsetRect(label.frame, myLabelInsets)];
        
}

#pragma mark - set border to view
+(void)addBorderLayer:(UIView *)view
{
        view.layer.borderColor = [UIColor lightGrayColor].CGColor;
        view.layer.borderWidth = 0.5;
}
#pragma mark - valid cutomer name
-(BOOL)allParticularCharInTextFieldWithString:(NSString *)string acceptableCharStr:(NSString *)acceptableCharStr
{
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:acceptableCharStr] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
}



#pragma mark - get btn height
-(CGFloat)calculateBtnHeight:(UIButton *)btn
{
        CGSize constraintSize;
        constraintSize.width = btn.frame.size.width;
        
        constraintSize.height = MAXFLOAT;
        CGSize size = [[btn titleForState:UIControlStateNormal] sizeWithFont:btn.titleLabel.font constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
        return size.height;
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
        CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
        CGSize size;
        NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
        CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:@{NSFontAttributeName:label.font}
                                                      context:context].size;
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
        return size.height;
}
#pragma mark - get time
-(void)timeForWebservice
{
        
}


-(void)showAlertForInternet
{
        
        [noInterNetView removeFromSuperview];
        noInterNetView = [[CustomView alloc] initWithFrame:CGRectMake(0.0, -64.0,APP_DELEGATE.window.frame.size.width, 64.0)];
        [noInterNetView setupInterNet];
        [APP_DELEGATE.window.rootViewController.view.superview addSubview:noInterNetView];
        if (self.noInterNetView.isAnimating)
        {
                [noInterNetView moveup];
        }
        [self.noInterNetView moveDown];
}

+ (void)rotateLayerInfinite:(CALayer *)layer
{
        CABasicAnimation *rotation;
        rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        rotation.fromValue = [NSNumber numberWithFloat:0];
        rotation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
        rotation.duration = 1.0f; // Speed
        rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
        [layer removeAllAnimations];
        [layer addAnimation:rotation forKey:@"Spin"];
}


-(void)setBorderToView:(UIView *)view
{
        
        view.layer.cornerRadius = 5;
        view.layer.borderWidth= 0.5;
        view.layer.borderColor =[UIColor lightGrayColor].CGColor;
        [view setClipsToBounds:YES];
        [self setShadow:view];
        
}

+(BOOL)checkCorporateUser
{
        
        
        if([[NSUserDefaults standardUserDefaults] valueForKey:SG_CORPORATE_USER] != nil)
        {
                if([[[NSUserDefaults standardUserDefaults] valueForKey:SG_CORPORATE_USER] boolValue])
                        return true;
                else
                        return false;
        }else
        {
                return false;
        }
}


-(void)setCornerRadiusToTopLeftAndTopRight:(UIView *)view
{
        UIBezierPath *maskPath = [UIBezierPath
                                  bezierPathWithRoundedRect:view.bounds
                                  byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                  cornerRadii:CGSizeMake(5, 5)
                                  ];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        
        maskLayer.frame = view.bounds;
        maskLayer.path = maskPath.CGPath;
        
        view.layer.mask = maskLayer;
        
}

-(void)setCornerRadiusToBottomLeftAndBottomRight:(UIView *)view
{
        UIBezierPath *maskPath = [UIBezierPath
                                  bezierPathWithRoundedRect:view.bounds
                                  byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                  cornerRadii:CGSizeMake(5, 5)
                                  ];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        
        maskLayer.frame = view.bounds;
        maskLayer.path = maskPath.CGPath;
        
        view.layer.mask = maskLayer;
}

-(void)createWrapView:(UITextField *)textfield{
        UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, textfield.frame.size.height)];
        textfield.leftViewMode = UITextFieldViewModeAlways;
        textfield.leftView = wrapView;
        [textfield setAutocorrectionType:UITextAutocorrectionTypeNo];
        
}
-(void)setGradiantBackgroundColorToView:(UIView *)view
{
        view.layer.cornerRadius = 5.0f;
        view.backgroundColor = [UIColor clearColor];
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = view.layer.bounds;
        
        //    gradientLayer.colors = [NSArray arrayWithObjects:
        //                            (id)[UIColor colorWithRed:255.0/255.0f green:157.0/255.0f blue:42.0/255.0f alpha:0.1f].CGColor,
        //                            (id)[UIColor colorWithRed:248.0/255.0f green:97.0/255.0f blue:44.0/255.0f alpha:0.5f].CGColor,
        //
        //                            nil];
        
        gradientLayer.colors = [NSArray arrayWithObjects:
                                (id)[UIColor colorWithRed:241.0/255.0f green:101.0/255.0f blue:31.0/255.0f alpha:0.4f].CGColor,
                                (id)[UIColor colorWithRed:245.0/255.0f green:91.0/255.0f blue:14.0/255.0f alpha:0.8f].CGColor,
                                
                                nil];
        
        gradientLayer.locations = [NSArray arrayWithObjects:
                                   [NSNumber numberWithFloat:0.0f],
                                   [NSNumber numberWithFloat:1.0f],
                                   nil];
        
        gradientLayer.cornerRadius = view.layer.cornerRadius;
        [view.layer addSublayer:gradientLayer];
}

#pragma mark - display custom popup
-(void)displayCustomPopup:(NSString *)title subTitle:(NSString *)subTitle
{
        ALAlertBanner *banner = [ALAlertBanner alertBannerForView:APP_DELEGATE.window.rootViewController.view style:ALAlertBannerStyleNotify position:ALAlertBannerPositionTop title:title subtitle:subTitle tappedBlock:^(ALAlertBanner *alertBanner) {
                [alertBanner hide];
        }];
        banner.secondsToShow = 3.5;
        banner.showAnimationDuration = 0.25;
        banner.hideAnimationDuration = 0.2;
        [banner show];
        
        return;
}


#pragma mark - display activityIndicator
-(void)displayLoading
{
        [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
}

-(void)removeLoading
{
        [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
}



#pragma mark - custom methods for sell your gadget module
+(BOOL)isUserLoggedInSellYourGadget{
        
        int userId= [[[NSUserDefaults standardUserDefaults] valueForKey:SELL_GADGET_LOGIN_USERID] intValue];
        
        if (userId)
                return YES;
        else
                return NO;
        
}

+(void)addDropDownArrowImageInBtn:(UIButton *)btn
{
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(btn.frame.size.width-17, (btn.frame.size.height-12)/2, 12, 12)];
        img.image = [UIImage imageNamed:@"dropdown"];
        [btn addSubview:img];
        [[btn layer] setCornerRadius:5.0f];
        
        [[btn layer] setBorderWidth:0.5f];
        [[btn layer] setBorderColor:[UIColor grayColor].CGColor];
}
+(void)addUnderLinetoBtnTitle:(UIButton *)btn
{
        NSDictionary * linkAttributes = @{NSForegroundColorAttributeName:btn.titleLabel.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)};
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:btn.titleLabel.text attributes:linkAttributes];
        [btn.titleLabel setAttributedText:attributedString];

}

+(void)displaySellYourGadgetRightMenu:(UIViewController *)parentView

{
        for (UIViewController *vc in parentView.childViewControllers) {
                if([vc isKindOfClass:[SGMenuViewController class]])
                {
                        [vc willMoveToParentViewController:nil];
                        [vc removeFromParentViewController];
                        [vc.view removeFromSuperview];

                        return;
                }
        }
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Product" bundle:nil];
        SGMenuViewController *viewController;
        viewController = [storyBoard instantiateViewControllerWithIdentifier:@"SGMenuViewController"];
        [parentView addChildViewController:viewController];
        CGFloat ht = 0;
        if([CommonSettings isUserLoggedInSellYourGadget])
        {
                if([[[NSUserDefaults standardUserDefaults] valueForKey:IS_RETAILER] boolValue])
                        ht = 150;
                else
                        ht = 100;
        }
        else
                ht = 50;
        viewController.view.frame = CGRectMake(150, 58, kSCREEN_WIDTH- 150, ht);
        [parentView.view addSubview:viewController.view];
        viewController.view.alpha = 0;
        [viewController didMoveToParentViewController:parentView];
        
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^
         {
                 viewController.view.alpha = 1;
         }
                         completion:nil];
        
        
}
//+(void)navigateToSellYourGadgetView
//{
//        UINavigationController *nav = (UINavigationController*)APP_DELEGATE.tabBarObj.selectedViewController;
//        [APP_DELEGATE.tabBarObj unhideTabbar];
//        UIStoryboard *storyboard1=[UIStoryboard storyboardWithName:@"Product" bundle:nil];
//        SellYourGadgetHomeViewController *objVc = [storyboard1 instantiateViewControllerWithIdentifier:@"SellYourGadgetHomeViewController"];
//
//        [nav pushViewController:objVc animated:YES];
//
//}

+(void)navigateToSellYourGadgetLoginView
{
        UINavigationController *nav = (UINavigationController*)APP_DELEGATE.tabBarObj.selectedViewController;
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj hideTabbar];
        SellYourGadgetLoginSignUpViewController *objVc = [[SellYourGadgetLoginSignUpViewController alloc] initWithNibName:@"SellYourGadgetLoginSignUpViewController" bundle:nil];
        [nav pushViewController:objVc animated:YES];
}

+(void)navigateToSellYourGadgetSearchView
{
        UINavigationController *nav = (UINavigationController*)APP_DELEGATE.tabBarObj.selectedViewController;
        AppDelegate *objAppdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [objAppdelegate.tabBarObj hideTabbar];
}

+(void)setUpSearchBar:(UISearchBar *)searchBar
{
        searchBar.placeholder = SELL_GADGET_SEARCH_HINT;
//        searchBar.backgroundColor = [[AppDelegate getAppDelegateObj] colorWithHexString:APP_THEME_COLOR];
        searchBar.backgroundColor = [UIColor gradientBottomColor];
        [searchBar setBackgroundImage:[[UIImage alloc]init]];
        
}
+(BOOL)validEmail:(NSString*)emailString
{
        NSString *emailid = emailString;
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        BOOL myStringMatchesRegEx=[emailTest evaluateWithObject:emailid];
        if(myStringMatchesRegEx)
        {
                return YES;
        }
        else
        {
                return NO;
        }
}

+(void)addPaddingAndImageToTextField:(UITextField *)textField withImageName:(NSString *)imageName
{
        UIView *wrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, textField.frame.size.height)];
        textField.leftViewMode = UITextFieldViewModeAlways;
        textField.leftView = wrapView;
        [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
        
        UIView *rightWrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 25, textField.frame.size.height)];
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 7, 15, 15)];
        img.image = [UIImage imageNamed:imageName];
        [rightWrapView addSubview:img];
        textField.rightViewMode = UITextFieldViewModeAlways;
        textField.rightView = rightWrapView;
}

+(void)setAsterixToTextField:(UITextField *)textFieldName withPlaceHolderName:(NSString *)placeHolderName
{
        
        NSMutableAttributedString *attriburedString = [[NSMutableAttributedString alloc] initWithString: placeHolderName];
        [attriburedString addAttribute: NSForegroundColorAttributeName
                                 value: [UIColor grayColor]
                                 range: NSMakeRange(0,placeHolderName.length)];
        NSAttributedString *asterix = [[NSAttributedString alloc] initWithString:@"*" attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
        [attriburedString appendAttributedString:asterix];
        textFieldName.attributedPlaceholder = attriburedString;
}


+(BOOL)checkIsIphoneX
{
        NSLog(@"height %f",UIScreen.mainScreen.bounds.size.height);
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone && UIScreen.mainScreen.bounds.size.height == 812)  {
                return YES;
        }
        else
                return NO;
        
}

-(void)setStatusBar:(UIView *)statusBar{
    
    if (@available(iOS 13.0, *)) {
             statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame];
             [[UIApplication sharedApplication].keyWindow addSubview:statusBar];

         } else {
             // Fallback on earlier versions
             statusBar= [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
             [statusBar setNeedsDisplay];
         }
         
          if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                         statusBar.backgroundColor = [UIColor statusBarColor];
         }
}
@end
