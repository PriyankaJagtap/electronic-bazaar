
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonSettings : NSObject

@property (nonatomic,strong)NSDate *startTime;
+(CommonSettings*) sharedInstance;
@property(strong,nonatomic)NSString *userToken;

-(void)showAlertTitle:(NSString *)title message :(NSString *)message;

-(void)setStatusBar:(UIView *)view;


+(void)sendScreenName:(NSString *)strName;
+(void)eventTrackingForHomeScreen:(NSString *)withLabel;
+(void)setDefaultValue:(NSString *)str forKey:(NSString *)key;
+(NSString *)getDefaultValueForKey:(NSString *)str;



+(BOOL)isLoggedInUser;
+(int)getUserID;
+(NSString*)getUserMailID;
+(int)getQuoteID;


//My cart
+(void)getMyCartCount;
+(void)incrementMyCartCount;
+(void)incrementMyCartCountByAmount:(int)amount;
+(void)removeMyCart;
+(void)setMyCartcount:(NSString *)count;
+(void)showMyCartCount;

//to adjust image
+(UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height;
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size;


+(BOOL) needsUpdate;
+(NSString *) getAppstoreVersion;
+(UIToolbar *)setToolBarNumberKeyBoard:(id)sender;
-(NSString *)formatPrice:(float)price;
-(void)setShadow:(UIView *)view;
-(BOOL)allParticularCharInTextFieldWithString:(NSString *)string acceptableCharStr:(NSString *)acceptableCharStr;


+(void)addGoogleAnalyticsTrackEvent:(NSString *)category label:(NSString *)label;
+(NSString *)getAppCurrentVersion;
-(void)setLabelBorder_Inset:(UILabel *)label;
-(BOOL)checkNullValue:(NSString *)str;
-(void)showAlertForInternet;
+ (void)rotateLayerInfinite:(CALayer *)layer;
-(void)setBorderToView:(UIView *)view;
-(void)displayCustomPopup:(NSString *)title subTitle:(NSString *)subTitle;
-(void)setCornerRadiusToTopLeftAndTopRight:(UIView *)view;
-(void)setCornerRadiusToBottomLeftAndBottomRight:(UIView *)view;
-(void)createWrapView:(UITextField *)textfield;
-(void)setGradiantBackgroundColorToView:(UIView *)view;
-(void)displayLoading;
-(void)removeLoading;
+(void)addBorderLayer:(UIView *)view;
+(void)addUnderLinetoBtnTitle:(UIButton *)btn;
-(CGFloat)calculateBtnHeight:(UIButton *)btn;
+(BOOL) validatePanCardNumber: (NSString *) cardNumber;
+(BOOL) validateGSTNumber: (NSString *) gstNumber;

//custom methods for sell your gadget module
+(BOOL)isUserLoggedInSellYourGadget;
+(void)addDropDownArrowImageInBtn:(UIButton *)btn;
+(void)navigateToSellYourGadgetView;
+(void)navigateToSellYourGadgetLoginView;
+(BOOL)validEmail:(NSString*)emailString;
+(void)navigateToSellYourGadgetSearchView;
+(void)setUpSearchBar:(UISearchBar *)searchBar;
+(void)addPaddingAndImageToTextField:(UITextField *)textField withImageName:(NSString *)imageName;
+(void)setAsterixToTextField:(UITextField *)textFieldName withPlaceHolderName:(NSString *)placeHolderName;
+(bool)checkIsArm;
-(NSString *)formatIntegerPrice:(NSInteger)price;
+(void)displaySellYourGadgetRightMenu:(UIViewController *)parentView;
+(BOOL)checkCorporateUser;
+(BOOL)checkIsIphoneX;
- (CGFloat)getLabelHeight:(UILabel*)label;

@end
