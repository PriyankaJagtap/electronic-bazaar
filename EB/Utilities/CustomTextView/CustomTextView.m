//
//  CustomTextView.m
//  EB
//
//  Created by Neosoft on 8/3/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "CustomTextView.h"

@implementation CustomTextView

@synthesize placeHolderLabel;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self addPlaceHolderLabel];
        [self setupView];
    }
    return self;
}

- (void)setupView
{
    self.tintColor = [UIColor darkGrayColor];
    self.textColor = [UIColor blackColor];
    //self.layer.cornerRadius = 5.0f;
    
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
}

-(void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}

- (void)addPlaceHolderLabel
{
    self.placeHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 5.0, CGRectGetWidth(self.frame)-20.0, 40.0)];
    self.placeHolderLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.placeHolderLabel.numberOfLines = 0;
    self.placeHolderLabel.text = @"*Condition/storage capacity/working or non working";

    self.placeHolderLabel.textColor = [UIColor lightGrayColor];
    self.placeHolderLabel.font = [UIFont fontWithName:@"Karla" size:14.0];
    self.placeHolderLabel.textAlignment = NSTextAlignmentLeft;
    self.placeHolderLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addSubview:self.placeHolderLabel];
    
    // setup constraints
    // left constraint
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.placeHolderLabel
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:10.0]];
    // right constraint
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.placeHolderLabel
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:10.0]];
    // top constraint
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.placeHolderLabel
                                                     attribute:NSLayoutAttributeTopMargin
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self attribute:NSLayoutAttributeTopMargin
                                                    multiplier:1.0 constant:5.0]];
    // height constraint
    [self.placeHolderLabel addConstraint:[NSLayoutConstraint constraintWithItem:self.placeHolderLabel
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1.0 constant:40.0]];
}


- (BOOL)becomeFirstResponder
{
    [self setPlaceHolderLabelVisible:NO];
    return [super becomeFirstResponder];
}

- (BOOL)resignFirstResponder
{
    [self setPlaceHolderLabelVisible:YES];
    return [super resignFirstResponder];
}

- (void)setPlaceHolderLabelVisible:(BOOL)visible
{
    // if the placeholder needs to be displayed, but it is already visible
    // OR the label is hidden, and needs to be hidden, just ignore this method
    if ((visible == [self isPlaceHolderLabelVisible]) || self.text.length > 0) {
        return;
    }
    
    // animate the label upwards
    [self animatePlaceHolderLabelUpwards:visible];
    
    // animate the alpha value of the label
    //    [UIView animateWithDuration:0.3 animations:^{
    //        self.placeHolderLabel.alpha = visible ? 1.0 : 0.0;
    //    }];
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    if (self.text.length > 0) {
        self.placeHolderLabel.alpha = 0.0;
    } else {
        self.placeHolderLabel.alpha = 1.0;
    }
}

- (BOOL)isPlaceHolderLabelVisible
{
    return self.placeHolderLabel.alpha == 1;
}


- (void)animatePlaceHolderLabelUpwards:(BOOL)visible
{
    [UIView animateWithDuration:0.3 animations:^{
        if (visible) {
            self.placeHolderLabel.alpha = 1.0;
            self.placeHolderLabel.transform = CGAffineTransformIdentity;
        } else {
            self.placeHolderLabel.alpha = 0.0;
            self.placeHolderLabel.transform = CGAffineTransformMakeTranslation(0.0, -10.0);
        }
    }];
}
@end
