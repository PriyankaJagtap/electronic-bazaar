//
//  CustomTextView.h
//  EB
//
//  Created by Neosoft on 8/3/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextView : UITextView
@property (nonatomic, strong) UILabel *placeHolderLabel;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;


@end
