

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Validation : NSObject

+(BOOL)required:(UITextField *)field withCaption:(NSString*)strCaption;

+(BOOL)requiredTextView:(UITextView *)textView withCaption:(NSString*)strCaption;

+(BOOL)ValidEmail:(NSString *)checkString;

+(BOOL) ValidateMobileNumber: (NSString *) candidate;

+(BOOL)CheckfoSpaces:(NSString *)checkString;

+(BOOL) ValidateAlphaNumeric: (NSString *) candidate;

+(BOOL)ValidatePasswordField:(UITextField *)textField;

+(BOOL)validatelength:(UITextField *)textfield;

+(BOOL)validatelengthForPassword:(UITextField *)textfield;
+(BOOL)checkMobileNumber:(UITextField *)txtField;
+(BOOL)checkFirstName:(UITextField *)txtField;
+(BOOL)checkLastName:(UITextField *)txtField;


@end
