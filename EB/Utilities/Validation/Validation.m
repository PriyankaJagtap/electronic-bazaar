

#import "Validation.h"
#import "CommonSettings.h"
#import "Constant.h"
@implementation Validation

+(BOOL)required:(UITextField *)field withCaption:(NSString*)strCaption
{
    if([field.text length]==0)
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[NSString stringWithFormat:@"%@ field is required.",strCaption]];
        return false;
    }
    return YES;
}

+(BOOL)checkMobileNumber:(UITextField *)txtField
{
    if([txtField.text length] < 10)
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:MOBILE_VALIDATION_TEXT];
        return NO;
    }
    
    return YES;

}

+(BOOL)checkFirstName:(UITextField *)txtField
{
    if([txtField.text length] < 3)
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:FIRST_NAME_VALIDATION_TEXT];
        return NO;
    }
    
    return YES;
    
}

+(BOOL)checkLastName:(UITextField *)txtField
{
    if([txtField.text length] < 3)
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:LAST_NAME_VALIDATION_TEXT];
        return NO;
    }
    
    return YES;
    
}


+(BOOL)requiredTextView:(UITextView *)textView withCaption:(NSString*)strCaption
{
    if([textView.text length]==0)
    {
        [[CommonSettings sharedInstance]showAlertTitle:@"" message:[NSString stringWithFormat:@"%@ field is required.",strCaption]];
        return false;
    }
    return YES;
}

+(BOOL)ValidEmail:(NSString *)checkString
{
    if([checkString length] <=0)
        return FALSE;
     BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
     NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
     NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
     NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
     NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
     return [emailTest evaluateWithObject:checkString];
}

+(BOOL) ValidateMobileNumber: (NSString *) candidate
{
    if([candidate length] <=0)
        return FALSE;
    NSString *regex = @"^([0-9]{10,12})$";
    
    NSPredicate *rp = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    if ([rp evaluateWithObject:candidate])
    {
        return TRUE;
    }
    
    return FALSE;
    
}

+(BOOL)CheckfoSpaces:(NSString *)checkString
{
    NSString *trimmed = [checkString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([trimmed length] <=0)
    {
        return NO;
    }

    
    return YES;
}

+(BOOL) ValidateAlphaNumeric: (NSString *) candidate
{
    NSString *regex = @"^[a-zA-Z0-9'-~&$ ]*$";

    NSPredicate *rp = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
	
	if ([rp evaluateWithObject:candidate])
	{
        return TRUE;
    }
	else
	{
		return FALSE;
    }
    
}
+(BOOL)validatelength:(UITextField *)textfield
{
    if([textfield.text length]<=20)
    {
        return true;
    }
    else
    {
        return false;
    }
    
}
+(BOOL)validatelengthForPassword:(UITextField *)textfield
{
    if([textfield.text length]<=6)
    {
        return true;
    }
    else
    {
        return false;
    }
    
}

+(BOOL)ValidatePasswordField:(UITextField *)textField
{
    NSString *string = textField.text;
    
    NSString *specialCharacterString = @"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'";
    NSString *numberString = @"0123456789";
    NSCharacterSet *specialCharacterSet = [NSCharacterSet characterSetWithCharactersInString:specialCharacterString];
    NSCharacterSet *numberCharacterSet = [NSCharacterSet characterSetWithCharactersInString:numberString];
    
    specialCharacterString = numberString = Nil;
    
    if ([string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length && [string.lowercaseString rangeOfCharacterFromSet:numberCharacterSet].length)
    {
        return TRUE;
    }
    return FALSE;
}



@end
