//
//  UILabel+UILabelWithDashLineAtMiddleOfText.h
//  EB
//
//  Created by Neosoft on 5/25/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (UILabelWithDashLineAtMiddleOfText)
-(void)setUpDashLineAtTheMiddleOfText;
@end
