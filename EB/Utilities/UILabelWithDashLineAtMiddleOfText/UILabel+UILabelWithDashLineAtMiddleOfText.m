//
//  UILabel+UILabelWithDashLineAtMiddleOfText.m
//  EB
//
//  Created by Neosoft on 5/25/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "UILabel+UILabelWithDashLineAtMiddleOfText.h"

@implementation UILabel (UILabelWithDashLineAtMiddleOfText)
-(void)setUpDashLineAtTheMiddleOfText
{
    UILabel *dashLinLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height/2, self.bounds.size.width, 0.5)];
    dashLinLabel.backgroundColor = self.textColor;
    [self addSubview:dashLinLabel];

}
@end
