//
//  GoogleLoginViewController.h
//  EB
//
//  Created by webwerks on 8/27/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowTabbars.h"
@interface GoogleLoginViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *webview;
    NSMutableData *receivedData;
    ShowTabbars *tabBarObj;
}

@property (nonatomic, retain) IBOutlet UIWebView *webview;
@property (nonatomic, retain) NSString *isLogin;
@property (assign, nonatomic) Boolean isReader;


@end
