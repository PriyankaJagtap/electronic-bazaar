//
//  CustomTextFieldWithBorder.m
//  EB
//
//  Created by Neosoft on 8/8/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "CustomTextFieldWithBorder.h"

@implementation CustomTextFieldWithBorder

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)drawRect:(CGRect)rect{
    

}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
}

-(void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}


@end
