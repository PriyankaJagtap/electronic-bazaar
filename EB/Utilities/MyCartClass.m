//
//  MyCartClass.m
//  EB
//
//  Created by webwerks on 29/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "MyCartClass.h"

@implementation MyCartClass


+(NSString *)getMyCartCount{
        
        NSString *strcount;
        if([[NSUserDefaults standardUserDefaults]valueForKey:MY_CART_COUNT_KEY])
                strcount =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:MY_CART_COUNT_KEY]];
        else
                strcount = @"";
        return strcount;
        
}
+(void)setMyCartcount:(NSNumber *)count{
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setValue:count forKey:MY_CART_COUNT_KEY];
        [defaults synchronize];
}


@end



