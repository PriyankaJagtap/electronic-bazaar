//
//  NIDropDown.h
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@class NIDropDown;
@protocol NIDropDownDelegate
- (void) niDropDownDelegateMethod: (NIDropDown *) sender;

@optional
- (void) getindexValue: (NIDropDown *)sender withindex:(NSIndexPath*)index;
-(void) displayData: (UIButton *)sender withTitle:(NSString *)title;

@end

@interface NIDropDown : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
}
@property (nonatomic, retain) id <NIDropDownDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;
@property (nonatomic) BOOL displayDropDownBool;

-(void)hideDropDown:(UIButton *)b;
-(id)showDropDown:(UIButton *)b withHeight:(CGFloat *)height withData:(NSArray *)arr withImages :(NSArray *)imgArr WithDirection:(NSString *)direction;


@end
