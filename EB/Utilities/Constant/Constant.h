//
//  Constant.h
//  EB
//
//  Created by webwerks on 7/30/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#ifndef EB_Constant_h
#define EB_Constant_h
#define UPDATE_ORDER_PAYMENT @"static/updateOrderPaymentIos"

#pragma mark:- UAE Webservice Constants
//#define UAE_BASE_URL @"https://www.electronicsbazaar.com/customapiv7/"
#define UAE_BASE_URL @"http://180.149.246.49/electronicsbazaar/customapiv7/"
#define UAE_REGISTRATION_WS @"customer/register/"
#define UAE_LOGIN_WS @"customer/login/"
#define UAE_FORGOT_PASSWORD_WS @"customer/forgotPassword?mobile="
#define UAE_USER @"UAEUser"
#define UAE_LOGIN_USER_MOBILE @"UAELoginUserMobile"
#define UAE_LOGIN_USER_PASSWORD @"UAELoginUserPassword"
#define UAE_MENU_LIST_WS @"catalog/getmenuitems"
#define UAE_HOME_WS @"home/homepage"
#define UAE_MY_ACCOUNT_WS @"customer/dashboard?userid="
#define UAE_EDIT_PROFILE_WS @"customer/editProfile?"
#define UAE_CHANGE_PASSWORD_WS @"customer/changeCustomerPassword?"
#define UAE_GET_BRANDS_WS @"catalog/getCategoryBrands?category_id="

#pragma mark:- Vow Delight Webservices
#define VALIDATE_IMEI_WS @"vow/validateimei?imei="
#define SAVE_REQUEST_WS @"vow/saverequest?orders="

#pragma mark:- India Webservice Constants
//latest - 6 ** Updated date: 14/07/2016
#define APP_THEME_COLOR @"3292cf"

#define BASE_URL @"https://www.electronicsbazaar.com/customapiv6/"  // Live
//#define BASE_URL @"http://150.242.140.40/eb/customapiv6/"  // Development

#define GSTIN_DETAIL_WS @"customer/gstdetails?gstin="
#define HOME_FLG_WS @"home/flag?device_type=iPhone&push_id="
#define DEMO_SERVER_URL @"http://180.149.246.49/electronicsbazaar/customapiv6/"
#define LOGIN_WS                        @"customer/login/"
#define REGISTRATION_WS                 @"customer/registernew/"
#define IBM_REGISTRATION_WS                 @"customer/ibmsignup?"
#define IBM_LOGIN_WS                        @"customer/login?"
#define HOMEPAGE_Ws                     @"home/homepage?"
//#define NSLog(...)                       //

#define PRODUCT_DETAIL_WS               @"catalog/getProduct?"
#define VIEWALL_WS                      @"catalog/homepageViewAll?"
#define CATEGORY_PROD_WS                @"catalog/getCategoryProduct?"
#define MENU_LIST_WS                    @"catalog/getmenuitems"
#define ADD_TO_CART_WS                  @"cart/addtocart"
#define ADD_BULK_PRODUCTS_CART_WS       @"cart/addtocart"
#define GET_CART_INFO_WS                @"cart/getCartInfo"
#define CHECK_PINCODE                   @"catalog/getPincodeChecker?"
#define GETWISHLIST                     @"customer/getCustomerWishlist?"
#define ADDTOWISHLIST                   @"cart/addToWishlist?"
#define SHARE_WISHLIST_WS               @"customer/sharewishlist?"
#define ADD_ALL_TO_CART_WS              @"customer/addalltocartwishlist?"
#define GETORDERDETAILS                 @"customer/getOrderDetails"
#define CANCEL_ORDER_WS                 @"customer/cancel"


#define UPDATETOCART                    @"cart/updateToCart"
#define REMOVE_FROM_CART                @"cart/removeFromCart"
#define MY_ORDERS                       @"customer/myOrders"
#define DASHBOARD_WS                    @"customer/dashboard"
#define GETCUSTOMER_ADDRESS_LIST_WS     @"customer/getCustomerAddressList"
#define GET_REGION_LISTING_WS           @"customer/getRegionListing"
#define SAVE_BILLING_ADDRESS_WS         @"checkout/saveBillingAddress"
#define PINCODE_WS                      @"checkout/getCountryRegionCityFromPincode"
#define COUNTRYLIST_WS                  @"customer/getCountryList"
#define SAVE_SHIPPING_ADDRESS_WS        @"checkout/saveShippingAddress"
#define AVAILABLE_PAYMENT_WS            @"checkout/getAvailablePaymentMethods?"
#define CREATE_ORDER_WS                 @"checkout/createOrder?"
#define SET_SELECTED_PAYMENT_METHOD     @"checkout/setSelectedPaymentMethods"
#define CHANGE_PASSWORD_WS              @"customer/changeCustomerPassword"
#define WRITE_A_REVIEW_WS               @"/catalog/addProductReview"
#define SUBMIT_FEEDBACK_WS              @"feedback/sendfeedback?"
#define REMOVE_FROM_WISHLIST_WS         @"cart/removeFromWishlist?"
#define ADD_REVIEW_WS                   @"catalog/addProductReview"
#define TRACK_MY_ORDER_WS               @"customer/trackOrder"
#define PROCEED_TO_CHECKOUT_WS          @"checkout/proceedToCheckout"
#define ADD_ADDRESS_WS                  @"customer/createCustomerAddress"
#define EDIT_PROFILE                    @"customer/editProfile?"
#define CREATE_CUSTOMER_ADDRESS         @"customer/createCustomerAddress"
#define GET_CUSTOMER_ADDRESS_LIST_WS    @"customer/getCustomerAddressList"
#define REMOVE_ADDRESS                  @"customer/deleteAddress?"
#define ADD_COUPON_CODE                 @"checkout/addCouponCode"
#define FORGOT_PASSWORD_WS              @"customer/forgotPassword"
#define SEARCH_PRODUCTS                 @"catalog/searchProducts"
#define NOTIFICATION_LIST               @"customer/getNotificationList?device_type=iPhone&push_id="
#define GET_ECOMMERRCETRACKING_DETAIL   @"checkout/getEcommerceTrackingAndroidIos"
#define GET_INCREMENTID_FROM_QUOTEID    @"checkout/getIncrementIdFromQuoteId"
#define CREATE_ORDER_BY_PAYZAPP         @"checkout/createOrderPayZapp"
#define GENERATE_PAYZAPP_HASHKEY        @"static/generateHashPayZapp"
//#define CREATE_ORDER_PAYZAPP @"checkout/createOrderPayZapp"
//#define ECOMMERCE_TRACKING_NEW @"checkout/getEcommerceTrackingAndroidIosBillDesk"
#define GET_CITY_FROM_PINCODE  @"checkout/getCountryRegionCityFromPincode"
#define CHECK_MOBILE_WS  @"customer/checkUsersMobileNumberExists?mobile="
#define CHECK_EMAIL_WS  @"customer/checkUsersEmailExists?email="

#define GET_HOME_PAGE_BANNER @"home/homepagebannernew"

#define GET_HOME_FEATURE_PRODUCTS_WS @"catalog/getfeaturedproducts"
#define GET_HOME_HOT_SELLING_WS @"catalog/gethotselling"

//For Home page
#define GET_HOME_SERVICE_OPTION_WS @"home/getServiceBanners"
#define GET_HOME_BEST_SELLER_WS @"home/bestseller?user_id="
#define GET_HOME_TOP_BRANDS_WS @"home/brands"
#define GET_HOME_ACCESSORIES_WS @"home/getTopAccessories?user_id="
#define GET_HOME_RECENTLY_VIEWED_WS @"customer/getRecentlyViewed?user_id="
#define GET_HOME_PAYMENTS_METODS_BANNERS_WS @"home/getPaymentMethodsBanners"
#define TRACK_ORDER_WITH_ORDERID @"track/trackyourorderid?"
#define GET_HOME_POP_UP_WS @"home/popup"


//#define GST_WS @"customer/gistin?"
#define GST_WS @"customer/gistindetails?"


#define GET_HOME_PAGE_BANNER_DEALS @"home/dealsproductscollection"

#define COUPON_LISTING          @"cart/getCouponListingPage"
#define REMOVE_COUPON           @"checkout/removeCouponCode"
#define GENERATE_EB_PIN_WS  @"customer/generateEbpin?mobile="
#define GENERATE_SEND_ORDER_EB_PIN_WS  @"customer/sendOrderEbPin?"
#define SHOW_EB_PIN_ALERT_WS  @"customer/enableEBpin"
#define GET_REGISTRATION_DATA  @"customer/getRegistrationData"
#define SAVE_SUPER_SALE_WS @"catalog/savesupersale?"
//for the sell Gadgets ws
#define SELL_GADGETS_CATEGORY_WS @"gadget/category?customer_id="
#define SELL_GADGETS_BRANDS_WS @"gadget/brands?code="
#define SELL_GADGET_MOBILE_MODEL_WS @"gadget/products/?"
#define SELL_GADGET_PRODUCT_DETAIL_WS @"gadget/mobile/?"
#define SELL_GADGET_CHEKOUT_WS @"gadget/save"
#define SELL_GADGET_LAPTOP_DETAIL_WS @"gadget/laptop/?"
#define SELL_GADGET_MOBILE_BRAND_WS @"gadget/mobilebrands?customer_id="
#define SELL_GADGET_LAPTOP_BRAND_WS @"gadget/laptopbrands?customer_id="
#define SELL_GADGET_HISTORY_WS @"gadget/list?"
#define GET_SERVICE_CENTER_DATA_WS @"service/getservicedata?city="
#define SELL_GADGET_HISTORY_DETAILS_WS @"gadget/detail?order_id="
#define SHIPMENT_WS @"track/trackyourorder?order_id="
#define BULK_PRODUCT_WS @"bulk/productCollection"
#define BULK_PRODUCT_SEARCH_WS @"bulk/productCollection?product_name="
#define RETURN_REASONS_WS @"customer/returnreason"
#define SAVE_RETURN_WS @"customer/savereturn?"
#define REPRINT_INVOICE_WS @"https://www.electronicsbazaar.com/operations/index/getOrderInvoicePrint?orderId="
#define FORGOT_PASSWORD_THROUGH_EMAIL_WS @"customer/forgotPassword?email="
#define RESET_PASSWORD_OTP_WS @"customer/resetPasswordOtp?newpassword="
#define SALE_BACK_WS @"gadget/saveRetailersData"

//TEST URL
#define DOWNLOAD_LIST_WITH_PRICE_WS @"stock/download"

#define GET_APP_VERSION_WS @"static/getIphoneAppVersion"


//sell your gadget webservices
#define SYG_TNC_CONTENT @"http://electronicsbazaar.com/warranty-app/gadget-policy.html";
#define SYG_IMEI_INFO_CONTENT @"http://electronicsbazaar.com/warranty-app/popup-mobile.html";
#define SYG_SERIAL_INFO_CONTENT @"http://electronicsbazaar.com/warranty-app/popup-laptop.html";

#define SG_CHECK_PINCODE_WS @"gadget/checkpincode?pincode="
#define SG_HOME_WS @"gadget/home"
#define SG_GET_MOBILE_MODEL_WS @"gadget/mobile?code="
#define SG_GET_LAPTOP_MODEL_WS @"gadget/laptop?code="
#define SG_TRACK_YOUR_ORDER @"gadget/index/track?ticket_id="
#define SG_GET_MOBILE_CONDITIONS_WS @"gadget/mobile?product_id="
#define SG_GET_LAPTOP_CONDITIONS_WS @"gadget/laptop?product_id="
#define SG_PROMOCODE_WS @"gadget/validateStaticCode?promo_code="
#define SG_GET_BANK_LIST_WS @"gadget/getBankList?email="
#define SG_NEW_PROCESSOR_WS @"gadget/newlaptop"
#define SG_REPLACE_LAPTOP_WS @"gadget/replacelaptop?"
#define SG_ORDER_CONFIRM_WS @"gadget/orderconfirm?"
#define GET_ADDRESS_DATA_WS @"customer/getAddressData?address_id="

#define BRAND_PRODUCT_ID @"brand_product_id"
#define MODEL_ID @"model_id"
#define MEMORY_ID @"memory_id"
#define CONDITION_ID  @"condition_id"
#define IMEI_NO  @"imei_no"
#define PRICE  @"price"
#define BRAND  @"brand"
#define MODEL  @"model"
#define MEMORY  @"memory"
#define CONDITION  @"condition"
#define GENERATION @"generation"
#define PROCESSOR @"processor"
#define PROMOCODE @"promoCode"
#define INTERNAL_ORDER_ID @"internalOrderID"
#define CHARGER @"charger"
#define CHARGER_KEY @"Arethecompatiblechargerandbatteryincluded?"
#define EMAIL_ID @"emailID"
#define CORP_PRICE @"corpPrice"
#define MY_CART_COUNT_KEY @"MY_CART_COUNT"
#define WISHLIST_ITEM_ID @"wishlist_item_id"
#define REPCODE @"repcode"
#define CONFIRM @"Confirm"
#define NOT_INTERSTED @"Not Intersted"


#define kSCREEN_WIDTH [UIScreen mainScreen].bounds.size.width


#define SALES_HISTORY @"Sales History"
#define PURCAHSE_HISTORY @"Your Purchase history"
#define ADD_TO_CART_POPUP_MSG @"Do you want to submit your GST Identification number?"
#define MOBILE_VALIDATION_TEXT @"Please provide 10 digit mobile number"
#define STORE_NAME_VALIDATION_TEXT @"Store name not exceed than 30 characters"
#define LAST_NAME_VALIDATION_TEXT @"Invalid last name. No. of characters should be atleast 3"
#define FIRST_NAME_VALIDATION_TEXT @"Invalid first name. No. of characters should be atleast 3"
#define LENOVO_LOGIN @"Lenovo_Login"
#define LENOVO_USERID @"lenovo_user_id"
#define EB_CUSTOMER_ID @"eb_customer_id"
#define LENOVO_CUSTOMER_ID @"lenovo_customer_id"

#define TERMS_AND_CONDITION_URL         @"http://www.electronicsbazaar.com/terms-and-conditions"
//hemant
#define LOGIN_AFFILIATE_USER_MSG @"Only Affiliate User is allowed for checkout please login as Affiliate User."
#define APP_DISCOUNT_LABEL @"(app discount .5%)"

#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define ACCEPTABLE_GST_NUMBER_CHARACTERS @"1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define ACCEPTABLE_PAN_NUMBER_CHARACTERS @"1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#define ACCEPTABLE_IMEI_NUMBER @"1234567890"
#define ACCEPTABLE_SERIAL_NUMBER_CHARACTERS @"1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

#define App_Link                @"itms-apps://itunes.apple.com/us/app/electronicsbazaar.com/id1064879270?ls=1&mt=8"
//#define App_Link                @"https://itunes.apple.com/us/app/electronicsbazaar.com/id1064879270?ls=1&mt=8"


//for sell your gadget module
#define SELL_GADGET_LOGIN_USERNAME @"SellGadgetLoginUsername"
#define SELL_GADGET_LOGIN_USER_PASSWORD @"SellGadgetLoginUserPassword"
#define SELL_GADGET_LOGIN_USERID @"SellGadgetLoginUserID"
#define SELL_GADGET_USER @"SellGadgetUser"

#define SELL_GADGET_CUSTOMER_ID @"sell_gadget_customer_id"
#define SELL_GADGET_REGISTRATION_WS @"gadget/register"
#define SELL_GADGET_OTHRES_BRAND_WS @"gadget/sendemail"
#define SELL_GADGET_SEARCH_WS @"catalog/searchGadgets?"
//#define SELL_GADGET_SEARCH_HINT @"Search your gadget here..."
#define SELL_GADGET_SEARCH_HINT @"Search entire store here..."
#define SG_CORPORATE_USER @"sgCorporateUser"
#define IS_RETAILER @"is_retailer"


#define CART_LOGIN_REQUIRED_MSG @"Please sign in for adding product to cart."
//For Login Flow
#define MY_CART @"MyCart"
#define MY_PROFILE @"MyProfile"
#define MY_WISHLIST @"MyWishlist"
#define TRACK_ORDER @"TrackOrder"
#define VOW_DELIGHT @"VowDelight"


#define PRODUCT_DETAIL @"ProductDetail"
#define CATEGORY @"Category"
#define SEARCH @"Search"
#define DEALS @"Deals"

//Billdesk credentials
#define MERCHANT_ID             @"EBAZAAR"
#define SECURITY_ID             @"ebazaar"
#define CHECKSUM                @"RmRDbqOtz8hU"
#define CREATE_ORDER_PAYTM_WS                 @"checkout/createOrderPayTm"
#define PAYMENT_GATEWAY_URL     @"https://pgi.billdesk.com/pgidsk/PGIMerchantPayment"
//Contact Us url
#define CONTACT_UR_URL          @"static/contactUsHtml"

#define SHOW_GSTIN @"showGSTIN"
#define REMIND_ME_LATER_GSTIN @"remindMeLaterGSTIN"
#define REMIND_ME_LATER_CLICK_DATE @"remindMeLaterDate"


//app theme color

//UAE
#define SELECTED_STORE @"selectedStore"
#define UAE_STROE @"UAEStore"
#define INDIAN_STORE @"IndianStore"


// Device Check
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define IS_IPHONE_4S ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )480 ) < DBL_EPSILON )

#define ScreenHeight (int)[[UIScreen mainScreen] bounds].size.height
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))



#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_X  (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)


// Define Font Size
#define FONT_SIZE_1 IS_IPAD?20.0f:13.0f
#define FONT_SIZE_2 IS_IPAD?20.0f:15.0f//12
#define FONT_SIZE_3 IS_IPAD?24.0f:18.0f //24
#define FONT_SIZE_4 IS_IPAD?26.0f:16.0f//26
#define FONT_SIZE_5 IS_IPAD?27.0f:17.0f
#define FONT_SIZE_6 IS_IPAD?35.0f:20.0f

#define LOG_IT

#ifdef LOG_IT
#	define DLog(fmt, ...) //NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#	define DLog(...)
#endif

//Initialize class
#define COMMON_SETTINGS [CommonSettings sharedInstance]

// Define Font Type
static inline UIFont *karlaFontRegular(CGFloat size) {
    if (size==0) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            size=FONT_SIZE_3;
        }
        else{
            size=FONT_SIZE_2;
        }
    }
    return [UIFont fontWithName:@"Karla-Regular" size:size];
}


static inline UIFont *KarlaFontBold(CGFloat size) {
    if (size==0) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            size=FONT_SIZE_3;
        }
        else{
            size=FONT_SIZE_2;
        }
    }
    return [UIFont fontWithName:@"Karla-Bold" size:size];
}




#define ADDRESS_ACTIONSHEET_TAG 1
#define STATE_ACTIONSHEET_TAG 2
#define COUNTRY_ACTIONSHEET_TAG 3
#define CFORM_ACTIONSHEET_TAG 4



//App delegate
#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define MAIN_STORYBOARD ((UIStoryboard*) [UIStoryboard storyboardWithName:@"Main" bundle:nil])

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#endif
