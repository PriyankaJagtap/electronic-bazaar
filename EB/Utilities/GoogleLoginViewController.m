//
//  GoogleLoginViewController.m
//  EB
//
//  Created by webwerks on 8/27/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "GoogleLoginViewController.h"

#import "SlideMenuViewController.h"
#import "HomeViewController.h"
#import "Constant.h"
#import "FVCustomAlertView.h"
#import "CommonSettings.h"
#import "GoogleLoginViewController.h"




NSString *client_id =@"931767690950-i481dsncq9onst8thtsptdfcvguakc8l.apps.googleusercontent.com";
NSString *secret = @"aBhHAI5gLnpmjJs3O7arSX3X";
NSString *callbakc =  @"http://localhost";;
NSString *scope = @"https://www.googleapis.com/auth/userinfo.email+https://www.googleapis.com/auth/userinfo.profile+https://www.google.com/reader/api/0/subscription";
NSString *visibleactions = @"http://schemas.google.com/AddActivity";



@interface GoogleLoginViewController ()<FinishLoadingData>

@end

@implementation GoogleLoginViewController
@synthesize webview,isLogin,isReader;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpNavigationBar];
    NSString *url = [NSString stringWithFormat:@"https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=%@&redirect_uri=%@&scope=%@&data-requestvisibleactions=%@",client_id,callbakc,scope,visibleactions];
    
    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];

}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [FVCustomAlertView hideAlertFromView:[AppDelegate getAppDelegateObj].window fading:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebview delegate

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    //    [indicator startAnimating];
    if ([[[request URL] host] isEqualToString:@"localhost"]) {
        
        // Extract oauth_verifier from URL query
        NSString* verifier = nil;
        NSArray* urlParams = [[[request URL] query] componentsSeparatedByString:@"&"];
        for (NSString* param in urlParams) {
            NSArray* keyValue = [param componentsSeparatedByString:@"="];
            NSString* key = [keyValue objectAtIndex:0];
            if ([key isEqualToString:@"code"]) {
                verifier = [keyValue objectAtIndex:1];
                //NSLog(@"verifier %@",verifier);
                break;
            }
        }
        
        if (verifier) {
            NSString *data = [NSString stringWithFormat:@"code=%@&client_id=%@&client_secret=%@&redirect_uri=%@&grant_type=authorization_code", verifier,client_id,secret,callbakc];
            NSString *url = [NSString stringWithFormat:@"https://accounts.google.com/o/oauth2/token"];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
            NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            
            receivedData = [[NSMutableData alloc] init];
            
        } else {
            // ERROR!
        }
        
        [webView removeFromSuperview];
        
        return NO;
    }
    return YES;
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data

{
    [receivedData appendData:data];
    //NSLog(@"verifier %@",receivedData);
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:[NSString stringWithFormat:@"%@", error]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;

    NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *tokenData = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    //  WebServiceSocket *dconnection = [[WebServiceSocket alloc] init];
    //   dconnection.delegate = self;
    
    NSString *accessTokenStr=[tokenData objectForKey:@"access_token"];
    NSString *str=[NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/userinfo?access_token=%@",accessTokenStr];
    
    NSString *escapedUrl = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",escapedUrl]];
    NSString *jsonData = [[NSString alloc] initWithContentsOfURL:url usedEncoding:nil error:nil];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    //NSString *userId=[jsonDictionary objectForKey:@"id"];

    
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self.navigationController popViewControllerAnimated:NO];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        NSMutableDictionary *dictData = [NSMutableDictionary new];
        [dictData setObject:[result valueForKey:@"email"] forKey:@"username"];
        [dictData setObject:@"" forKey:@"password"];
        [dictData setObject:@"iphone" forKey:@"device_type"];
        [dictData setObject:@"g" forKey:@"social_type"];
        [dictData setObject:[result valueForKey:@"id"]?:@"" forKey:@"social_login_id"];
        NSString *pushId=[defaults valueForKey:@"DEVICE_TOKEN"];
        [dictData setObject:pushId?:@"" forKey:@"push_id"];
        [dictData setObject:[result valueForKey:@"given_name"] forKey:@"firstname"];
        [dictData setObject:[result valueForKey:@"family_name"] forKey:@"lastname"];
       // NSString *strDob=[self formatDateWithString:[result valueForKey:@"birthday"]];
        //[dictData setObject:strDob forKey:@"dob"];
       // [dictData setObject:@"" forKey:@"mobile"];
        
        [dictData setObject:[result valueForKey:@"picture"] forKey:@"profile_picture"];
        // SAVE PROFILE PICTURE in userdefaults
        NSString *strProfile = [NSString stringWithFormat:@"%@",[result valueForKey:@"picture"]];
        [[NSUserDefaults standardUserDefaults]setObject:strProfile forKey:@"profile_picture"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [self callLoginWS:dictData];

        
//        UIAlertView *userDetails=[[UIAlertView alloc]initWithTitle:@"User details" message:[NSString stringWithFormat:@"Name:%@ emailId:%@",[jsonDictionary valueForKey:@"given_name"],[jsonDictionary valueForKey:@"email"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [userDetails show];
    });
}

-(void)setUpNavigationBar
{
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent=NO;
    UILabel *titleLabel=[[UILabel alloc]init];
    titleLabel.text = @"Sign Up With Google";
    
    titleLabel.font=karlaFontRegular(18);
   // titleLabel.font =[UIFont fontWithName:@"Karla-Regular" size:18.0];
    titleLabel.frame = CGRectMake(0, 0, 100, 30);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabel;
    UIColor *bgColor=[UIColor colorWithRed:22/255.0f green:70/255.0f blue:134/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = bgColor;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    self.navigationController.navigationBar.backItem.title =@"";
    self.navigationController.navigationBar.backItem.hidesBackButton=YES;
    
}

#pragma mark - LoginWS
-(void)callLoginWS:(NSMutableDictionary *)userdata{
    [FVCustomAlertView showDefaultLoadingAlertOnView:[AppDelegate getAppDelegateObj].window withTitle:@"" withBlur:NO allowTap:NO];
    
    Webservice  *callLoginService = [[Webservice alloc] init];
    BOOL chkInternet = [[AppDelegate getAppDelegateObj] checkInternetConnectivity];
    if(!chkInternet)
    {
        [FVCustomAlertView hideAlertFromView:self.view fading:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        callLoginService.delegate =self;
        [callLoginService operationRequestToApi:userdata url:LOGIN_WS string:@"LoginWS"];
    }
}

-(void)navigateToHomeViewcontroller
{
    
    // Set Firsttime value
    [[NSUserDefaults standardUserDefaults] setObject:@"FirstTime" forKey:@"FirstTime"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    // Navigate from Login screen t home screen.
    
    UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UINavigationController *navigationController ;
    
    tabBarObj = [[ShowTabbars alloc] init];
    
    [tabBarObj showTabbars];
    [tabBarObj TabClickedIndex:0];
    
    
    SlideMenuViewController *  objMainMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"SlideMenuViewController"];
    
    RightMenuViewController*  objRightMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"RightMenuViewController"];
    
    MFSideMenuContainerViewController *container =[MFSideMenuContainerViewController
                                                   containerWithCenterViewController:tabBarObj
                                                   leftMenuViewController:objMainMenuVC
                                                   rightMenuViewController:objRightMenuVC];
    
    [AppDelegate getAppDelegateObj].navigationController=navigationController;
    [AppDelegate getAppDelegateObj].tabBarObj=tabBarObj;
    [[[UIApplication sharedApplication]delegate].window setRootViewController:container];
    [navigationController setNavigationBarHidden:YES animated:YES];
    
//    [[[UIApplication sharedApplication]delegate].window setRootViewController:container];
//    [navigationController setNavigationBarHidden:YES animated:YES];
    
}



-(void)receivedResponseForLogin:(id)receiveData stringResponse:(NSString *)responseType{
    [FVCustomAlertView hideAlertFromView:self.view fading:NO];
    if ([responseType isEqualToString:@"success"]) {
        NSData *receiveLoginData = [NSData dataWithData:receiveData];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:receiveLoginData options:kNilOptions error:nil];
        NSMutableDictionary *dictUserDetail = [NSMutableDictionary dictionaryWithDictionary:jsonObject];
        
        DLog(@"userDetail %@",jsonObject);
        
        if ([dictUserDetail isKindOfClass:[NSDictionary class]]) {
            NSString *status = [NSString stringWithFormat:@"%ld",(long)[[dictUserDetail objectForKey:@"status"]integerValue]];
            if ([status isEqualToString:@"1"])
            {
              //  NSString *tokenVal = [dictUserDetail objectForKey:@"token"];
                NSString *loginType = [dictUserDetail objectForKey:@"login_type"];
                DLog(@"%@",tokenVal);
             //   [[NSUserDefaults standardUserDefaults]setObject:tokenVal forKey:@"token"];
                [[NSUserDefaults standardUserDefaults]setObject:loginType forKey:@"login_type"];
                
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                NSMutableDictionary *userdict = [NSMutableDictionary new];
                
                NSString *uid =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"id"]];
                NSString *firstname = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"firstname"]];
                NSString *lastname = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"lastname"]];
                NSString *dob = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"dob"]];
                NSString *mobile = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"mobile"]];
                NSString *profile_picture = [NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"profile_picture"]];
                
                
                
                NSString *email = @"";
                if ([loginType isEqualToString:@"normal"]) {
//                    email = emailIdTxt.text;
                }
                else{
                    email =[NSString stringWithFormat:@"%@",[[dictUserDetail objectForKey:@"user"] objectForKey:@"email"]];
                }
                
                [userdict setObject:uid?uid:@"" forKey:@"user_id"];
                [userdict setObject:firstname?firstname:@"" forKey:@"firstname"];
                [userdict setObject:lastname?lastname:@"" forKey:@"lastname"];
                [userdict setObject:dob?dob:@"" forKey:@"dob"];
                [userdict setObject:mobile?mobile:@"" forKey:@"mobile"];
                [userdict setObject:profile_picture?profile_picture:@"" forKey:@"profile_picture"];
                [userdict setObject:email forKey:@"email"];
                NSString *quote_ID = [NSString stringWithFormat:@"%@",[dictUserDetail objectForKey:@"quoteId"]];
                [[NSUserDefaults standardUserDefaults]setObject:quote_ID forKey:@"QuoteID"];

                [[NSUserDefaults standardUserDefaults] setObject:userdict forKey:@"user"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [self navigateToHomeViewcontroller];
                
            }
            else{
                [[AppDelegate getAppDelegateObj]showAlert:@"Sorry" withMessage:@"Please check your email/Password."];
            }
            
        }
        else{
            [[AppDelegate getAppDelegateObj]showAlert:@"Sorry" withMessage:@"Login couldn't succeed."];
        }
        
    }
    
}


@end
