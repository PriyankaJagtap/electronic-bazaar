//
//  ShowTabbars.h
//  CustomTabbar
//
//  Created by webwerks on 13/12/13.
//  Copyright (c) 2013 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton+Badge.h"



@interface ShowTabbars : UITabBarController
{
    /*
    UIButton *firstBtn;
    UIImageView *firstImgView;
    UIImage *firstImg;
    UIImage *firstImgSelected;
    
    UIButton *secondBtn;
    UIImageView *secondImgView;
    UIImage *secondImg;
    UIImage *secondImgSelected;
    
    UIButton *thirdBtn;
    UIImageView *thirdImgView;
    UIImage *thirdImg;
    UIImage *thirdImgSelected;
    
    UIButton *fourBtn;
    UIImageView *fourImgView;
    UIImage *fourImg;
    UIImage *fourImgSelected;
    
    UIButton *fiveBtn;
    UIImageView *fiveImgView;
    UIImage *fiveImg;
    UIImage *fiveImgSelected;
    */
    
    UIButton *firstBtn;
    UIButton *secondBtn;
    UIButton *thirdBtn;
    UIButton *fourBtn;
    UIButton *fiveBtn;

    UIView *view;
    
    NSString *str1;
    NSString *str2;
    NSString *str3;
    NSString *str4;
    NSString *str5;
}

-(void)setMyCartBadgeValue:(NSString *)strBadgeVal;



@property(nonatomic,strong) UIView *tabView;

-(void)TabClickedIndex:(int)index;
-(void)showTabbars;
-(void)hideTabbar;
-(void)unhideTabbar;
-(void)deSelectAllTabs;


@end
