//
//  ShowTabbars.m
//  CustomTabbar
//
//  Created by Shival on 13/12/13.
//  Copyright (c) 2013 Neosoft. All rights reserved.
//

#import "ShowTabbars.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "MyCartViewController.h"
#import "MyWishlist.h"
#import "DealsViewController.h"
#import "NotificationViewController.h"
#import "CommonSettings.h"
#import "HomeDealsViewController.h"
#import "MyAccountViewController.h"
#import "MyOrdersViewController.h"
#import "CategoryViewController.h"
#import "TrackingDetailsViewController.h"

#define checkIpad UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad

typedef enum
{
        kNOTIFICATION = 0,
        kHOME = 2,
        kMYCART = 3,
        kDEAL = 4
        
}TABS;

@implementation ShowTabbars
{
        //SearchViewController *fourViewObj;
        //    ShopDetailsViewController *fourViewObj;
}
@synthesize tabView;

UIStoryboard *storyBoard;
UIImageView *imageViewTabBackRound;
NSMutableArray *myViewControllersArray;

-(void)showTabbars
{
        myViewControllersArray = [[NSMutableArray alloc] init];
        storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        str1 = @"tab_home";
        str2 = @"tab_category";
        str3 = @"tab_profile";
        //str4 = @"tab_deals";
        str4 = @"tab_trackOrder";
        
        HomeViewController *secObj = [storyBoard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        UINavigationController *navObject1 = [[UINavigationController alloc] initWithRootViewController:secObj];
        
        CategoryViewController *secondViewObj = [storyBoard instantiateViewControllerWithIdentifier:@"CategoryViewController"];
        UINavigationController *navObject2 = [[UINavigationController alloc] initWithRootViewController:secondViewObj];
        
        MyAccountViewController *objMyOrder = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyAccountViewController"];
        
        UINavigationController *navObject3 = [[UINavigationController alloc] initWithRootViewController:objMyOrder];
        
//        UIStoryboard  *dealsStoryBoard= [UIStoryboard storyboardWithName:@"Product" bundle:nil];
//        HomeDealsViewController *fiveViewObj = [dealsStoryBoard instantiateViewControllerWithIdentifier:@"HomeDealsViewController"];
//        UINavigationController *navObj4 = [[UINavigationController alloc] initWithRootViewController:fiveViewObj];
    
        MyOrdersViewController *myOrder =[storyBoard instantiateViewControllerWithIdentifier:@"MyOrdersViewController"];
        UINavigationController *navObject5 = [[UINavigationController alloc]initWithRootViewController:myOrder];
        
        [myViewControllersArray addObject:navObject1];
        [myViewControllersArray addObject:navObject2];
        [myViewControllersArray addObject:navObject3];
        //[myViewControllersArray addObject:navObj4];
        [myViewControllersArray addObject:navObject5];
        
        
        navObject1.tabBarItem.tag=0;
        navObject2.tabBarItem.tag=1;
        navObject3.tabBarItem.tag=2;
        //navObj4.tabBarItem.tag=3;
        navObject5.tabBarItem.tag = 3;
        
        self.viewControllers = myViewControllersArray;
        tabView=[self CustomTabView];
        [self.view addSubview:tabView];
        //[self.view setBackgroundColor:[UIColor clearColor]];
        //self.tabBar.backgroundColor=[UIColor clearColor];
        [self.view setBackgroundColor:[UIColor whiteColor]];
        self.tabBar.backgroundColor=[UIColor whiteColor];
        self.tabBar.hidden=YES;
        
}




-(void)hideTabbar
{
        tabView.hidden = YES;
}

-(void)unhideTabbar
{
        tabView.hidden = NO;
}

-(UIView *)CustomTabView
{
//        float widthBtn = [AppDelegate getAppDelegateObj].window.frame.size.width/5;
    float widthBtn = [AppDelegate getAppDelegateObj].window.frame.size.width/4;

        int yValues;
        
        //    if (isIpad)
        //        yValues=80;
        //    else
        //        yValues=48;
        
        //    if (checkIpad)
        //        yValues=80;
        //    else
        yValues=48;
        UIView *bgView;
        if(IS_IPHONE_X)
        {
                bgView=[[UIView alloc]initWithFrame:CGRectMake(0, [AppDelegate getAppDelegateObj].window.frame.size.height-34-yValues,[AppDelegate getAppDelegateObj].window.frame.size.width,yValues)];
        }
        else
        {
                bgView=[[UIView alloc]initWithFrame:CGRectMake(0, [AppDelegate getAppDelegateObj].window.frame.size.height-yValues,[AppDelegate getAppDelegateObj].window.frame.size.width,yValues)];
        }
        
        
        [bgView setBackgroundColor:[UIColor whiteColor]];
        bgView.userInteractionEnabled=YES;
        bgView.tag=1111;
        
        imageViewTabBackRound  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:str1]];
        imageViewTabBackRound.frame=CGRectMake(0,0,[AppDelegate getAppDelegateObj].window.frame.size.width,48);
        imageViewTabBackRound.contentMode=UIViewContentModeScaleAspectFill;
        imageViewTabBackRound.backgroundColor = [UIColor whiteColor];
        [bgView addSubview:imageViewTabBackRound];
        
        
        firstBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        firstBtn.frame=CGRectMake(0,1,widthBtn,48);
        firstBtn.tag=0;
        firstBtn.selected=NO;
        [firstBtn addTarget:self action:@selector(TabClicked:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:firstBtn];
        
        secondBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        secondBtn.frame=CGRectMake(widthBtn,1,widthBtn,48);
        secondBtn.tag=1;
        secondBtn.selected=NO;
        [secondBtn addTarget:self action:@selector(TabClicked:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:secondBtn];
        
        
        
        thirdBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        thirdBtn.frame=CGRectMake(widthBtn*2,1,widthBtn,48);
        thirdBtn.tag=2;
        [thirdBtn addTarget:self action:@selector(TabClicked:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:thirdBtn];
        
        
        
//        fourBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//        fourBtn.frame=CGRectMake(widthBtn*3,1,widthBtn,48);
//        fourBtn.tag=3;
//        fourBtn.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
//        [fourBtn addTarget:self action:@selector(TabClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [bgView addSubview:fourBtn];
    
        
        fiveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//        fiveBtn.frame=CGRectMake(widthBtn*4,1,widthBtn,48);
        fiveBtn.frame=CGRectMake(widthBtn*3,1,widthBtn,48);
        fiveBtn.tag=3;
        
        fiveBtn.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        [fiveBtn addTarget:self action:@selector(TabClicked:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:fiveBtn];
        
        
        return bgView;
}

-(void)TabClicked:(id)sender
{
        UIButton *btn=(UIButton *)sender;
        
        [self TabClickedIndex:(int)[btn tag]];
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
        [self TabClickedIndex:(int)[item tag]];
}

-(void)TabClickedIndex:(int)index
{
        
        if (index==2)
        {
                if (![CommonSettings isLoggedInUser])
                {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please login to View Profile." preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                             {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                             AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                                             [appDelegate navigateToLogin:MY_PROFILE];
                                                             return;
                                                     });
                                             }];
                        [alert addAction:ok];
                        dispatch_async(dispatch_get_main_queue(), ^{
                                [self presentViewController:alert animated:YES completion:nil];
                        });
                        
                        return;
                        
                }
        }
        //    else if (index==4) {
        //        if (![CommonSettings isLoggedInUser]) {
        //            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please login to View Orders." preferredStyle:UIAlertControllerStyleAlert];
        //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
        //                                 {
        //                                     dispatch_async(dispatch_get_main_queue(), ^{
        //                                         AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        //                                         [appDelegate navigateToLogin:TRACK_ORDER];
        //                                         return;
        //
        //                                     });
        //
        //                                 }];
        //            [alert addAction:ok];
        //            dispatch_async(dispatch_get_main_queue(), ^{
        //                [self presentViewController:alert animated:YES completion:nil];
        //            });
        //                return;
        //
        //        }
        //
        //    }
        
//        if(index!=5)
    if(index!=4)
   {
                UINavigationController *viewObj =  [myViewControllersArray objectAtIndex:index];
                
                
                //            BaseNavigationController *viewObj =  (BaseNavigationController *)[myViewControllersArray objectAtIndex:index];
                
                NSArray *arr =  viewObj.viewControllers;
                if([arr count]>0)
                {
                        [viewObj popToViewController:[arr objectAtIndex:0] animated:YES];
                }
        }
        
        switch (index)
        {
                case 0:
                        imageViewTabBackRound.image  = [UIImage imageNamed:str1];
                        self.selectedIndex=0;
                        self.tabBarItem.tag=1;
                        break;
                case 1:
                        imageViewTabBackRound.image  = [UIImage imageNamed:str2];
                        self.selectedIndex=1;
                        self.tabBarItem.tag=2;
                        break;
                case 2:
                        imageViewTabBackRound.image  = [UIImage imageNamed:str3];
                        self.selectedIndex=2;
                        self.tabBarItem.tag=3;
                        
                        break;
                case 3:
                        imageViewTabBackRound.image  = [UIImage imageNamed:str4];
                        self.selectedIndex=3;
                        self.tabBarItem.tag=4;
                        break;
//                case 4:
//                        imageViewTabBackRound.image  = [UIImage imageNamed:str5];
//                        self.selectedIndex=4;
//                        self.tabBarItem.tag=5;
//                        break;
                case 4:
                        [self deSelectAllTabs];
                        self.selectedIndex=4;
                        self.tabBarItem.tag=5;
                        break;
                        
                default:
                        break;
                        
        }
}

-(void)deSelectAllTabs
{
        imageViewTabBackRound.image  = [UIImage imageNamed:@"default"];
}

-(void)setMyCartBadgeValue:(NSString *)strBadgeVal{
        thirdBtn.badgeValue=strBadgeVal;
}


//-(void)postNotificationWithProductID:(int)productID{
//    [self.navigationController pushViewController:<#(UIViewController *)#> animated:<#(BOOL)#>]
//}


@end
