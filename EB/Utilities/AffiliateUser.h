//
//  AffiliateUser.h
//  EB
//
//  Created by webwerks on 12/5/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AffiliateUser : NSObject
@property(strong,nonatomic) NSString *repCode;
@property(strong,nonatomic) NSString *name;
@property(strong,nonatomic) NSString *emailId;
@property(strong,nonatomic) NSString *mobNo;
@property(strong,nonatomic) NSString *selectedDays;




@end
//epCode,cust_Name_Txt.text,cust_Email_Id_Txt.text,cust_Mob_No_Txt.text,quoteID];