//
//  UnderLineLabel.m
//  EB
//
//  Created by webwerks on 08/01/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "UnderLineLabel.h"
#import <UIKit/UIKit.h>

@implementation UnderLineLabel


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect {
//    // Drawing code
//        UILabel *underLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height/2, self.bounds.size.width, 1)];
//        underLineLabel.backgroundColor = self.textColor;
//        [self addSubview:underLineLabel];
//
//}
//
//
//
//-(void)awakeFromNib
//{
//        [super awakeFromNib];
//        UILabel *underLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height/2, self.bounds.size.width, 1)];
//        underLineLabel.backgroundColor = self.textColor;
//        [self addSubview:underLineLabel];
//
//}

- (id)initWithCoder:(NSCoder*)coder {
        self = [super initWithCoder:coder];
        
        if (self) {
                UILabel *underLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height/2, self.bounds.size.width, 1)];
                underLineLabel.backgroundColor = self.textColor;
                [self addSubview:underLineLabel];
        }
        
        return self;
}

@end
