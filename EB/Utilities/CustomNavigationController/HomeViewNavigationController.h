//
//  HomeViewNavigationController.h
//  EB
//
//  Created by webwerks on 29/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewNavigationController : UIViewController
-(void)incrementMyCartCount:(int)withQty;
-(void)showCartValue;
@end
