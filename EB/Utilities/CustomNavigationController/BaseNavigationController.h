//
//  BaseNavigationController.h
//  EB
//
//  Created by webwerks on 26/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UIViewController
-(void)incrementMyCartCount:(int)withQty;
+(BaseNavigationController*) sharedInstance;
-(void)setViewControllerTitle:(NSString *)titleStr;
-(void)showCartValue;
@end
