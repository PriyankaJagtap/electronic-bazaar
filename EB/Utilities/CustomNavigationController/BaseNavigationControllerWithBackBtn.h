//
//  BaseNavigationControllerWithBackBtn.h
//  EB
//
//  Created by webwerks on 27/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationControllerWithBackBtn : UIViewController
-(void)incrementMyCartCount:(int)withQty;
+(BaseNavigationControllerWithBackBtn*) sharedInstance;
-(void)setViewControllerTitle:(NSString *)titleStr;
-(void)showCartValue;
-(void)setIsNotificationScreenValue:(BOOL)isNotificationView;
-(void)setIsCartScreenValue:(BOOL)isCartView;
-(void) setIsFromIBMLoginScreenValue: (BOOL) isFromIBMLoginScreen;
@end
