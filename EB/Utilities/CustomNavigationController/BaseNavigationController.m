//
//  BaseNavigationController.m
//  EB
//
//  Created by webwerks on 26/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "BaseNavigationController.h"
#import "MyCartViewController.h"
#import "NotificationViewController.h"

@interface BaseNavigationController ()
{
    UIButton *cartButton;
    UIButton *notificationButton;
    UINavigationBar *newNavBar;
    UIView *statusBar;
}

@end

@implementation BaseNavigationController

static BaseNavigationController *sharedInstance;

+(BaseNavigationController*) sharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[BaseNavigationController alloc] init];
    }
    return sharedInstance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self styleNavBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    //        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    //        int count=[[defaults valueForKey:@"MY_CART_COUNT"]intValue];
    //        NSString *strcount=[NSString stringWithFormat:@"%d",count+1];
    //        cartBarButtonItem.badgeValue=strcount;
    //cartButton.badgeValue = @"12";
    [self showCartValue];
}

- (void)styleNavBar {
    // 1. hide the existing nav bar
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    // 2. create a new nav bar and style it
    
    if(IS_IPHONE_X)
    {
        newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 44, CGRectGetWidth(self.view.bounds), 44.0)];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    else
    {
        newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20, CGRectGetWidth(self.view.bounds), 44.0)];
    }
    [newNavBar setTintColor:[UIColor whiteColor]];
    
    // 3. add a new navigation item w/title to the new nav bar
    UINavigationItem *newItem = [[UINavigationItem alloc] init];
    newItem.title = @"Title";
    [newNavBar setItems:@[newItem]];
    
    UIImage *navBackgroundImage = [UIImage imageNamed:@"Nav"];
    [newNavBar setBackgroundImage:navBackgroundImage forBarMetrics:UIBarMetricsDefault];
    [newNavBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Karla-Regular" size:17]}];
    
    // BackButtonBlack is an image we created and added to the app’s asset catalog
    UIImage *backButtonImage = [UIImage imageNamed:@"menu-btn"];
    
    // any buttons in a navigation bar are UIBarButtonItems, not just regular UIButtons. backTapped: is the method we’ll call when this button is tapped
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:backButtonImage style:UIBarButtonItemStylePlain target:self action:@selector(backTapped:)];
    
    UIImage *notificationButtonImage = [UIImage imageNamed:@"white_notification_icon"];
    
    notificationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [notificationButton setImage:notificationButtonImage forState:UIControlStateNormal];
    notificationButton.showsTouchWhenHighlighted = YES;
    notificationButton.frame = CGRectMake(0.0, 0.0, notificationButtonImage.size.width, notificationButtonImage.size.height);
    
    [notificationButton addTarget:self action:@selector(notificationBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *notificationBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:notificationButton];
    ;
    
    UIImage *cartButtonImage = [UIImage imageNamed:@"nav_cart"];
    
    cartButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cartButton setImage:cartButtonImage forState:UIControlStateNormal];
    cartButton.showsTouchWhenHighlighted = YES;
    cartButton.frame = CGRectMake(0.0, 0.0, cartButtonImage.size.width, cartButtonImage.size.height);
    
    [cartButton addTarget:self action:@selector(cartBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *cartBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cartButton];
    
    // the bar button item is actually set on the navigation item, not the navigation bar itself.
    newItem.leftBarButtonItem = backBarButtonItem;
    newItem.rightBarButtonItems = @[cartBarButtonItem,notificationBarButtonItem];
    [newNavBar setItems:@[newItem]];
    
    // 4. add the nav bar to the main view
    [self.view addSubview:newNavBar];
     [[CommonSettings sharedInstance] setStatusBar:statusBar];

}

#pragma mark - IBAction methods
-(void)backTapped:(UIBarButtonItem *)btn
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

-(void)notificationBtnTapped:(UIBarButtonItem *)btn
{
    //[APP_DELEGATE.tabBarObj TabClickedIndex:1];
    NotificationViewController  *myCart= [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    [self.navigationController pushViewController:myCart animated:NO];
}
-(void)cartBtnTapped:(UIBarButtonItem *)btn
{
    if (![CommonSettings isLoggedInUser]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please login to view Cart." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                     [appDelegate navigateToLogin:MY_CART];
                                     return;
                                     
                                 });
                                 
                             }];
        [alert addAction:ok];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
        return;
    }
    else{
        //[APP_DELEGATE.tabBarObj TabClickedIndex:2];
        MyCartViewController  *myCart= [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyCartViewController"];
        [self.navigationController pushViewController:myCart animated:NO];
    }
    
}

-(void)setViewControllerTitle:(NSString *)titleStr
{
    [newNavBar.items objectAtIndex:0].title = titleStr;
    //self.navigationController.title = titleStr;
    
}

-(void)showCartValue
{
    cartButton.badgeValue = [MyCartClass getMyCartCount];
}

-(void)incrementMyCartCount:(int)withQty{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    int count=[[defaults valueForKey:MY_CART_COUNT_KEY]intValue];
    NSString *strcount=[NSString stringWithFormat:@"%d",count+withQty];
    [defaults setValue:[NSNumber numberWithInteger:count+withQty] forKey:MY_CART_COUNT_KEY];
    cartButton.badgeValue = strcount;
}

@end
