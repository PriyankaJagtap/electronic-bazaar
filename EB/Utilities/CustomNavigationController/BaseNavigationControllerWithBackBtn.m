//
//  BaseNavigationControllerWithBackBtn.m
//  EB
//
//  Created by webwerks on 27/12/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "BaseNavigationControllerWithBackBtn.h"
#import "NotificationViewController.h"
#import "MyCartViewController.h"

@interface BaseNavigationControllerWithBackBtn ()
{
        UIButton *cartButton;
        UINavigationBar *newNavBar;
        UIButton *notificationButton;
        BOOL isNotificationScreen;
        BOOL isMyCartScreen;
        BOOL isFromIBMcreen;
    UIView *statusBar;
}
@end

@implementation BaseNavigationControllerWithBackBtn

static BaseNavigationControllerWithBackBtn *sharedInstance;

+(BaseNavigationControllerWithBackBtn*) sharedInstance
{
        if (!sharedInstance)
        {
                
                sharedInstance = [[BaseNavigationControllerWithBackBtn alloc] init];
        }
        
        return sharedInstance;
}

- (void)viewDidLoad {
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        [self styleNavBar];
    
        isMyCartScreen = 0;
        isNotificationScreen = 0;
}

- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
        [super viewWillAppear:animated];
        [self showCartValue];

}

- (void)styleNavBar {
        // 1. hide the existing nav bar
        [self.navigationController setNavigationBarHidden:YES animated:NO];
        // 2. create a new nav bar and style it
        
        if(IS_IPHONE_X)
        {
                newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 44, CGRectGetWidth(self.view.bounds), 44.0)];
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
        }
        else
        {
        newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20, CGRectGetWidth(self.view.bounds), 44.0)];
        [newNavBar setTintColor:[UIColor whiteColor]];
        }
        
        
        // 3. add a new navigation item w/title to the new nav bar
        UINavigationItem *newItem = [[UINavigationItem alloc] init];
        newItem.title = @"Title";
        [newNavBar setItems:@[newItem]];
        
        
        UIImage *navBackgroundImage = [UIImage imageNamed:@"Nav"];
        [newNavBar setBackgroundImage:navBackgroundImage forBarMetrics:UIBarMetricsDefault];
        [newNavBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor],
           NSFontAttributeName:[UIFont fontWithName:@"Karla-Regular" size:17]}];
        
        
        
        // BackButtonBlack is an image we created and added to the app’s asset catalog
        UIImage *backButtonImage = [UIImage imageNamed:@"ios_back_btn"];
        
        // any buttons in a navigation bar are UIBarButtonItems, not just regular UIButtons. backTapped: is the method we’ll call when this button is tapped
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:backButtonImage style:UIBarButtonItemStylePlain target:self action:@selector(backTapped:)];
        backBarButtonItem.tintColor = [UIColor whiteColor];
        
         UIImage *notificationButtonImage = [UIImage imageNamed:@"white_notification_icon"];
        
        notificationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [notificationButton setImage:notificationButtonImage forState:UIControlStateNormal];
        notificationButton.showsTouchWhenHighlighted = YES;
        notificationButton.frame = CGRectMake(0.0, 0.0, notificationButtonImage.size.width, notificationButtonImage.size.height);
        
        [notificationButton addTarget:self action:@selector(notificationBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *notificationBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:notificationButton];
        ;
        UIImage *cartButtonImage = [UIImage imageNamed:@"nav_cart"];
        
        cartButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cartButton setImage:cartButtonImage forState:UIControlStateNormal];
        cartButton.showsTouchWhenHighlighted = YES;
        cartButton.frame = CGRectMake(0.0, 0.0, cartButtonImage.size.width, cartButtonImage.size.height);
        
        [cartButton addTarget:self action:@selector(cartBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *cartBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cartButton];
        
        // the bar button item is actually set on the navigation item, not the navigation bar itself.
        newItem.leftBarButtonItem = backBarButtonItem;
        newItem.rightBarButtonItems = @[cartBarButtonItem,notificationBarButtonItem];
        [newNavBar setItems:@[newItem]];
        
        
        // 4. add the nav bar to the main view
        [self.view addSubview:newNavBar];
[[CommonSettings sharedInstance] setStatusBar:statusBar];

}



#pragma mark - IBAction methods

-(void)backTapped:(UIBarButtonItem *)btn
{
    if (isNotificationScreen == 1) {
        [[APP_DELEGATE tabBarObj] TabClickedIndex:0];
    }
    else if (isMyCartScreen == 1){
        [[APP_DELEGATE tabBarObj] TabClickedIndex:0];
        
    }
    else if (isFromIBMcreen)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)notificationBtnTapped:(UIBarButtonItem *)btn
{
    if (isNotificationScreen == 0) {
        //[APP_DELEGATE.tabBarObj TabClickedIndex:1];
        NotificationViewController  *navView= [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NotificationViewController"];
        //[self.navigationController presentViewController:navView animated:nil completion:nil];
        [self.navigationController pushViewController:navView animated:NO];
    }
    else if (isNotificationScreen == 1){
        
    }
    
}
-(void)cartBtnTapped:(UIBarButtonItem *)btn
{
    if (![CommonSettings isLoggedInUser]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please login to view Cart." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                      [appDelegate navigateToLogin:MY_CART];
                                     return;
                                     
                                 });
                                 
                             }];
        [alert addAction:ok];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
        return;
    }
    else{
        if (isMyCartScreen == 0) {
            //[APP_DELEGATE.tabBarObj TabClickedIndex:2];
            MyCartViewController  *myCart= [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MyCartViewController"];
            //[self.navigationController presentViewController:myCart animated:nil completion:nil];
            [self.navigationController pushViewController:myCart animated:NO];
        }
    }
}

-(void)setViewControllerTitle:(NSString *)titleStr
{
        [newNavBar.items objectAtIndex:0].title = titleStr;
        
}

-(void)setIsNotificationScreenValue:(BOOL)isNotificationView{
    isNotificationScreen = isNotificationView;
}
-(void)setIsCartScreenValue:(BOOL)isCartView{
    isMyCartScreen = isCartView;
}

-(void) setIsFromIBMLoginScreenValue: (BOOL) isFromIBMLoginScreen
{
    isFromIBMcreen = isFromIBMLoginScreen;
}

-(void)showCartValue
{
        cartButton.badgeValue = [MyCartClass getMyCartCount];
}


-(void)incrementMyCartCount:(int)withQty{
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        int count=[[defaults valueForKey:MY_CART_COUNT_KEY]intValue];
        NSString *strcount=[NSString stringWithFormat:@"%d",count+withQty];
        [defaults setValue:[NSNumber numberWithInteger:count+withQty] forKey:MY_CART_COUNT_KEY];
        cartButton.badgeValue = strcount;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
