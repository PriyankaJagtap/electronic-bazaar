//
//  Address.h
//  EB
//
//  Created by webwerks on 22/05/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Address : NSObject
/*
{
        city = Narasinghpur;
        company = ABCD;
        "complete_address" = "and, Narasinghpur, Madhya Pradesh, India, 487551";
        "country_id" = IN;
        "created_at" = "2018-01-07 16:48:53";
        "customer_address_id" = 27809;
        fax = 9867447955;
        firstname = ABCD;
        gstin = 27ABCDS1235A4A4;
        "is_default_billing" = 0;
        "is_default_shipping" = 0;
        lastname = ".";
        postcode = 487551;
        region = "Madhya Pradesh";
        "region_id" = 503;
        street = and;
        telephone = 9867447955;
        "updated_at" = "2018-04-20 10:36:16";
}
*/
/*
 "customer_address_id":"26572”,
 "created_at":"2017-12-07 11:25:05”,
 "updated_at":"2018-05-22 10:40:33”,
 "city":"PUNE CITY”,
 "company":"Rakshita's Mobile Store”,
 "country_id":"IN”,
 "firstname":"Rakshita's Mobile Store”,
 "gstin":"27ABCDS1235A4A4”,
 "lastname":".”,
 "postcode":"411030”,
 "region":"Maharashtra”,
 "region_id":"504”,
 "street":"18 Jasmine, Pestom Sagar,\nRoad No. 4, Chembur”,
 "telephone":"9867447955”,
 "is_default_billing":false,
 "is_default_shipping":false,
 "complete_address":"18 Jasmine, Pestom Sagar,, Road No. 4, Chembur, PUNE CITY, Maharashtra, India, 411030”
 */
@property (nonatomic) NSString *city;
@property (nonatomic) NSString *company;
@property (nonatomic) NSString *complete_address;
@property (nonatomic) NSString *country_id;
@property (nonatomic) NSString *created_at;
@property (nonatomic) NSString *customer_address_id;
@property (nonatomic) NSString *fax;
@property (nonatomic) NSString *firstname;
@property (nonatomic) NSString *gstin;
@property (nonatomic) BOOL is_default_billing;
@property (nonatomic) BOOL is_default_shipping;
@property (nonatomic) NSString *lastname;
@property (nonatomic) NSString *postcode;
@property (nonatomic) NSString *region;
@property (nonatomic) NSString *region_id;
@property (nonatomic) NSString *street;
@property (nonatomic) NSString *telephone;
@property (nonatomic) NSString *updated_at;

@end
