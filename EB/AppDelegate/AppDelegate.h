//
//  AppDelegate.h
//  EB
//
//  Created by webwerks on 7/29/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//


//#define isIpad UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad

#define isIpad UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPhone
#define checkIsIpad UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad

#import <UIKit/UIKit.h>
#import "MFSideMenuContainerViewController.h"
#import "HomeViewController.h"
#import "MyCartViewController.h"
#import "ElectronicsViewController.h"
#import "SlideMenuViewController.h"
#include "RightMenuViewController.h"
#import "ShowTabbars.h"
#import "Constant.h"
#import "GAI.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>{
    NSString *strFirstTime;
    NSDictionary *notificationDetailsDict;
}

@property (nonatomic) NSInteger ebPinGenerateCount;
-(NSInteger) getEBCount;
-(void) decrementEBCount;
-(void) setEBPinGenerateCount;

@property(strong,nonatomic)ShowTabbars *tabBarObj;
@property(strong,nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, strong) id<GAITracker> tracker;
@property(nonatomic,strong)MFSideMenuContainerViewController *container;
@property(nonatomic) BOOL firstTimeLaunch;
@property (weak, nonatomic) UIViewController *paymentOptionVC;
@property(strong, nonatomic) NSTimer *gstinRemindMeLaterTimer;

@property(nonatomic) BOOL isFromIBMLoginScreen;
// user detail :
@property (strong, nonatomic)NSMutableDictionary *dictUserDetial;

+ (AppDelegate*)getAppDelegateObj;

-(BOOL)checkInternetConnectivity;


-(void) showAlert :(NSString *) title withMessage :(NSString *) message;
-(UIColor*)colorWithHexString:(NSString*)hex;
-(void)StartProgressHud;
-(void)StopProgressHud;

-(void)setFeedBackTimer;
-(void)stopFeedBackTimer;
-(void)displayFeedBackview;

-(void)navigateToRegistration;
-(void)navigateToLoginViewController;
-(void)navigateToIBMLoginViewController;
-(void)navigateToLogin:(NSString *)viewToNavigateAfterLogin;
-(void)navigateToProductlistView;
+(void)setCustomNavigation:(UIViewController *)controller;
+(void)setCustomNavigationWithBackButton:(UIViewController *)controller;
-(void)switchToUAEStoreApplication;
-(void)switchToINDIAStoreApplication;
-(void)setInitialViewController;
@property (nonatomic) int lenovo_userId;
@property (nonatomic) BOOL isFromLenovoLogin;
-(void)handleNotification:(NSDictionary *)userInfo;
-(void)navigateToIBMLogin:(NSString *)viewToNavigateAfterLogin withCategoryValue:(NSString *) categoryValue withCategoryId:(NSString *) categoryId;
@end

