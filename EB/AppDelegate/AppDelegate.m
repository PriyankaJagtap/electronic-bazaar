
//  EB
//
//  Created by webwerks on 7/29/15.
//  Copyright (c) 2015 webwerks. All rights reserved.
//

#import "AppDelegate.h"
#import "FVCustomAlertView.h"
#import "Reachability.h"
#import <FacebookSDK/FacebookSDK.h>
//#import <GooglePlus/GooglePlus.h>
#import "LoginViewController.h"
#import "RegistrationViewController.h"
#import "ProductDetailsViewController.h"
#import "CommonSettings.h"
#import "SGInfoWebViewController.h"
#import "FeedbackViewController.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "SSKeychain.h"
#import <UserNotifications/UserNotifications.h>

#import "UAELoginSignUpViewController.h"
#import "UAEProductListingViewController.h"
#import "UAESideMenuView.h"
#import "UAEProductDetailsViewController.h"
#import "UAECheckoutViewController.h"
#import "UAEHomeViewController.h"
#import "SplashScreenViewController.h"
#import "LoginSignUpViewController.h"
#import "Firebase.h"
#import "SYGOrderConfirmationViewController.h"
#import "IBMLoginSignUpViewController.h"

@interface AppDelegate ()<UNUserNotificationCenterDelegate,FIRMessagingDelegate>
{
        NSTimer *feedbackTimer;
}

@property(nonatomic, assign) BOOL okToWait;
@property(nonatomic, copy) void (^dispatchHandler)(GAIDispatchResult result);

@end

@implementation AppDelegate
@synthesize dictUserDetial;
@synthesize navigationController;
@synthesize container;

/******* Set your tracking ID here *******/
//static NSString *const kTrackingId = @"UA-36864600-4";
static NSString *const kAllowTracking = @"allowTracking";
//static NSString *const kTrackingId = @"UA-72126266-1";
//static NSString *const kTrackingId = @"UA-72162369-1";
//UA-72162369-1

//23 Dec Given by Hemant Manjara
static NSString *const kTrackingId = @"UA-71810029-1";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
        [Fabric with:@[[Crashlytics class]]];
        [FBLoginView class];
        [FIRApp configure];
        [FIRMessaging messaging].delegate = self;
        
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max)
        {
                [self setNotificationCategory];
        }
        
        [self configureNotifications];
        [self setupGoggleAnalytics];
        [self generateUniqueDeviceIdentifier];
        
        //-- Set Notification
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        self.window.backgroundColor = [UIColor whiteColor];
        
        if(launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey])
        {
                //[self application:application didReceiveRemoteNotification:launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]];
                
                NSDictionary *notificationDict = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
                notificationDetailsDict = notificationDict;
        }
        SplashScreenViewController *vc = [[SplashScreenViewController alloc] initWithNibName:@"SplashScreenViewController" bundle:nil];
        [self.window setRootViewController:vc];
        [self.window makeKeyAndVisible];
        [self setEBPinGenerateCount];
        return YES;
}

-(void)setInitialViewController
{
        //        if(([[[NSUserDefaults standardUserDefaults] objectForKey:SELECTED_STORE] isEqualToString:UAE_STROE])){
        //                [self switchToUAEStoreApplication];
        //        }
        //        else{
        //                [self switchToINDIAStoreApplication];
        //        }
        [self switchToINDIAStoreApplication];
}

-(void)switchToUAEStoreApplication
{
        UAEHomeViewController *uaeHomeVC = [[UAEHomeViewController alloc] initWithNibName:@"UAEHomeViewController" bundle:nil];
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:uaeHomeVC];
        
        UAESideMenuView *uaeSideMenu = [[UAESideMenuView alloc] initWithNibName:@"UAESideMenuView" bundle:nil];
        container=[MFSideMenuContainerViewController  containerWithCenterViewController:self.navigationController leftMenuViewController:uaeSideMenu rightMenuViewController:nil];
        [self.window setRootViewController:container];
}

-(void)switchToINDIAStoreApplication
{
        SlideMenuViewController *objMainMenuVC;
        RightMenuViewController *objRightMenuVC;
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        // Check for First time
        strFirstTime = [[NSUserDefaults standardUserDefaults] objectForKey:@"FirstTime"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:@"FirstTimeLanuch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        UIStoryboard *storyBoard;
        storyBoard= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.tabBarObj = [[ShowTabbars alloc] init];
        
        if(![[NSUserDefaults standardUserDefaults] objectForKey:SHOW_GSTIN])
        {
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:YES] forKey:SHOW_GSTIN];
        }
        
        [self.tabBarObj showTabbars];
        [self.tabBarObj TabClickedIndex:0];
        objMainMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"SlideMenuViewController"];
        objRightMenuVC = [storyBoard instantiateViewControllerWithIdentifier:@"RightMenuViewController"];

        container=[MFSideMenuContainerViewController  containerWithCenterViewController:self.tabBarObj leftMenuViewController:objMainMenuVC
                                                                rightMenuViewController:objRightMenuVC];
        
        //        container=[MFSideMenuContainerViewController  containerWithCenterViewController:navObj leftMenuViewController:objMainMenuVC
        //                                                                rightMenuViewController:objRightMenuVC];
        
        [self.window setRootViewController:container];
        if (notificationDetailsDict != nil)
        {
                [self takeActionWhenUserTapOnNotification:notificationDetailsDict];
        }
        notificationDetailsDict = nil;
}

//-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
//    // We will add content here soon.
//
//    NSLog(@"background fectch called");
//}

-(void)scheduleGSTIN_RemindMeLaterTimer
{
        _gstinRemindMeLaterTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateGstinRemindLater) userInfo:nil repeats:NO];
}

-(void)updateGstinRemindLater
{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [_gstinRemindMeLaterTimer invalidate];
        _gstinRemindMeLaterTimer = nil;
        [defaults setObject:[NSNumber numberWithBool:YES] forKey:SHOW_GSTIN];
        [defaults setObject:[NSNumber numberWithBool:NO] forKey:REMIND_ME_LATER_GSTIN];
        [defaults synchronize];
}

-(void)configureNotifications
{
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max)
        {
                UIUserNotificationType allNotificationTypes =
                (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
                UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
                [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        }
        else
        {
                // iOS 10 or later
#if defined(__IPHONE_10_0) && IPHONE_OS_VERSION_MAX_ALLOWED >= IPHONE_10_0
                
                // For iOS 10 display notification (sent via APNS)
                [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
                UNAuthorizationOptions authOptions =
                UNAuthorizationOptionAlert
                | UNAuthorizationOptionSound
                | UNAuthorizationOptionBadge;
                [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions
                 completionHandler:^(BOOL granted, NSError * _Nullable error) {
                         if(granted)
                         {
                                 [self setNotificationCategory];
                         } } ];
                
                // For iOS 10 data message (sent via FCM)
                FIRMessaging.messaging.delegate = self;
#endif
        }
        [[UIApplication sharedApplication] registerForRemoteNotifications];
}

-(void)setupGoggleAnalytics
{
        [GAI sharedInstance].trackUncaughtExceptions = YES; // Enable
        // GOOGLE ANALYTICS ****//
        // Optional: automatically send uncaught exceptions to Google Analytics.
        [GAI sharedInstance].trackUncaughtExceptions = YES;
        // Optional: set Google Analytics dispatch interval
        [GAI sharedInstance].dispatchInterval = 60; //every to 2 mintues
        // Optional: set Logger to VERBOSE for debug information.
        [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
        // Initialize tracker. Replace with your tracking ID.
        [[GAI sharedInstance] trackerWithTrackingId:kTrackingId];
        // GOOGLE ANALYTICS INITIALIZATION END ****//
}

// We'll try to dispatch any hits queued for dispatch as the app goes into the background.
- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
        //        if (notificationDetailsDict != nil) {
        //                [self takeActionWhenUserTapOnNotification:notificationDetailsDict];
        //        }
        //        notificationDetailsDict = nil;
        //        application.applicationIconBadgeNumber=0;
}

-(void)takeActionWhenUserTapOnNotification:(NSDictionary*)dict
{
        dispatch_async(dispatch_get_main_queue(), ^{
                HomeViewController *objHomeVC;
                if([[self.navigationController visibleViewController] isKindOfClass:[HomeViewController class]])
                {
                        objHomeVC = (HomeViewController *)[self.navigationController visibleViewController];
                        objHomeVC.notificationDict=dict;
                }
                else
                {
                        UINavigationController *selectedItemnavController=[self.tabBarObj.viewControllers objectAtIndex:0];
                        objHomeVC= [[selectedItemnavController viewControllers] objectAtIndex:0];
                        objHomeVC.notificationDict=dict;
                        [self tabBarObj].selectedIndex=0;
                        [[self tabBarObj] TabClickedIndex:0];
                }
                
                [objHomeVC didbecomeActiveOnNotification];
        });
}

//-(void)application:(UIApplication *)application
//performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
//    //[self sendHitsInBackground];
//    completionHandler(UIBackgroundFetchResultNewData);
//}
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
        //    if (notificationSettings.types != UIUserNotificationTypeNone) {
        //        NSLog(@"didRegisterUser");
        [application registerForRemoteNotifications];
        // }
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
        //NSLog(@"the generated device token string is : %@",deviceToken);
        
        NSString * deviceTokenString = [[[[deviceToken description]
                                          stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                         stringByReplacingOccurrencesOfString: @">" withString: @""]
                                        stringByReplacingOccurrencesOfString: @" " withString: @""];
        
        NSLog(@"the generated device token string is : %@",deviceTokenString);
        //        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        //        // [defaults setValue:[NSString stringWithFormat:@"%@",deviceTokenString] forKey:@"DEVICE_TOKEN"];
        //        [defaults setValue:deviceTokenString forKey:@"DEVICE_TOKEN"];
        
        // NSLog(@"device token %@",[defaults valueForKey:@"DEVICE_TOKEN"]);
        //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Device Token" message:[[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_TOKEN"] delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
        
        //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Device Token" message:deviceTokenString delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
        //    [alert show];
        
        FIRMessaging.messaging.APNSToken = deviceToken;
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
        //handle the actions
        if ([identifier isEqualToString:@"declineAction"])
        {
        }
        else if ([identifier isEqualToString:@"answerAction"])
        {
        }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
        //NSLog(@"Failed to get token, error: %@", error);
        //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@",error] delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
        //    [alert show];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
        NSLog(@"userinfo : %@",userInfo);
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max)
        {
                [self handleNotification:userInfo];
        }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
        if (alertView.tag == 500)
        {
                if (buttonIndex == 0)
                {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:App_Link]];
                }
        }
        else if(alertView.tag == 600)
        {
                if (buttonIndex == 0)
                {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:App_Link]];
                }
        }
        else
        {
                if(buttonIndex == 0)
                {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:App_Link]];
                }
        }
}

-(void)navigateToProductlistView
{
        UIStoryboard *storyBoard;
        //    if (isIpad) {
        //        storyBoard= [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        //    }else
        //    {
        storyBoard= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //}
        ProductDetailsViewController *objProductDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
        objProductDetailVC.productID=110;
        self.tabBarObj.selectedIndex=2;
        UINavigationController *selectedItemnavController=[self.tabBarObj.viewControllers objectAtIndex:2];
        [selectedItemnavController pushViewController:objProductDetailVC animated:YES];
}

-(void)navigateToLoginViewController
{
        LoginSignUpViewController *objVc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginSignUpViewController"];
        navigationController=[[UINavigationController alloc]initWithRootViewController:objVc];
        [self.window setRootViewController:navigationController];
        [self.window makeKeyAndVisible];
}

-(void)navigateToIBMLoginViewController
{
    IBMLoginSignUpViewController *objVc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"IBMLoginSignUpViewController"];
    navigationController=[[UINavigationController alloc]initWithRootViewController:objVc];
    [self.window setRootViewController:navigationController];
    [self.window makeKeyAndVisible];
}

-(void)navigateToLogin:(NSString *)viewToNavigateAfterLogin
{
        LoginSignUpViewController *objVc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginSignUpViewController"];
        objVc.navigateToViewAfterLogin = viewToNavigateAfterLogin;
        UINavigationController *nav = (UINavigationController *)self.tabBarObj.selectedViewController;
        objVc.delegate = nav.visibleViewController;
        [self.tabBarObj hideTabbar];
        [nav pushViewController:objVc animated:NO];
}



-(void)navigateToIBMLogin:(NSString *)viewToNavigateAfterLogin withCategoryValue:(NSString *) categoryValue withCategoryId:(NSString *) categoryId
{
    IBMLoginSignUpViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"IBMLoginSignUpViewController"];
    vc.navigateToViewAfterLogin = viewToNavigateAfterLogin;
    UINavigationController *nav = (UINavigationController *)self.tabBarObj.selectedViewController;
    vc.delegate = nav.visibleViewController;
    vc.categoryVal = categoryValue;
    vc.category_id = categoryId;
    [self.tabBarObj hideTabbar];
    [nav pushViewController:vc animated:NO];
}

-(void)navigateToRegistration
{
        LoginSignUpViewController *objVc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginSignUpViewController"];
        objVc.registerBtnClicked = YES;
        navigationController=[[UINavigationController alloc]initWithRootViewController:objVc];
        [self.window setRootViewController:navigationController];
        [self.window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
        if ([[url scheme] hasPrefix:@"fb"])
        {
                return [FBSession.activeSession handleOpenURL:url];
        }
        //return [FBSession.activeSession handleOpenURL:url];
        return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
        if ([[url absoluteString] rangeOfString:@"com.neosofttech.EB"].location ==NSNotFound)
        {
                return [FBSession.activeSession handleOpenURL:url];
        }
        //        else
        //        {
        //                return [ [GPPSignIn sharedInstance]  handleURL:url
        //                                             sourceApplication:sourceApplication
        //                                                    annotation:annotation];
        //        }
        return YES;
}

//- (BOOL)application:(UIApplication *)iApplication openURL:(NSURL *)iURL sourceApplication:(NSString *)iSourceApplication
//         annotation:(id)iAnnotation {
//    if (iURL && [[iURL scheme] isEqualToString:@"pz81516121"]) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:PROCESS_INAPP_PAYMENT object:[iURL absoluteString]];
//    }
//
//    return YES;
//}

- (void)applicationWillResignActive:(UIApplication *)application
{
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// Shared Instance of Appdelegate
+ (AppDelegate*)getAppDelegateObj
{
        return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark -
-(void)generateUniqueDeviceIdentifier
{
        //    int rangeLow = 31;
        //    int rangeHigh = 60;
        //    //int randomNumber = arc4random() % (rangeHigh-rangeLow+1) + rangeLow;
        //    int randomNumber = arc4random();
        //    NSLog(@"random number %d and uuid %@",randomNumber,HomeViewController.GetUUID);
        
        if([SSKeychain passwordForService:@"com.electronicsbazaar.EB" account:@"user"] == nil)
        {
                NSString *UUID = AppDelegate.GetUUID;
                [SSKeychain setPassword:UUID forService:@"com.electronicsbazaar.EB" account:@"user"];
                NSString *retrieveuuid = [SSKeychain passwordForService:@"com.electronicsbazaar.EB" account:@"user"];
                NSLog(@"retrieveuuid %@",retrieveuuid);
        }
        NSLog(@"unique Id %@",[SSKeychain passwordForService:@"com.electronicsbazaar.EB" account:@"user"]);
}

+ (NSString *)GetUUID
{
        CFUUIDRef theUUID = CFUUIDCreate(NULL);
        CFStringRef string = CFUUIDCreateString(NULL, theUUID);
        CFRelease(theUUID);
        return (__bridge NSString *)string;
}

#pragma mark - FCMMessaging Delegates
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
        NSLog(@"FCM registration token: %@", fcmToken);
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setValue:fcmToken forKey:@"DEVICE_TOKEN"];
}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//
//        NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
//        [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
//
//        NSLog(@"userInfo=>%@", userInfo);
//}

//- (void)messaging:(nonnull FIRMessaging *)messaging
//didReceiveMessage:(nonnull FIRMessagingRemoteMessage *)remoteMessage
////NS_SWIFT_NAME(messaging(_:didReceive:))
////__IOS_AVAILABLE(10.0);
//{
//        NSLog(@"called did receive");
//
//}
///// The callback to handle data message received via FCM for devices running iOS 10 or above.
//- (void)applicationReceivedRemoteMessage:(nonnull FIRMessagingRemoteMessage *)remoteMessage
////NS_SWIFT_NAME(application(received:))
////__deprecated_msg("Use FIRMessagingDelegate’s -messaging:didReceiveMessage:");
//{
//
//        NSLog(@"called did receive");
//
//}

#pragma mark - for IOS 10 notification
//====================For iOS 10====================
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
        //Called when a notification is delivered to a foreground app.
        NSLog(@"Userinfo %@",notification.request.content.userInfo);
        // [self handleNotification:notification.request.content.userInfo isFromDidReceive:NO];
        completionHandler(UNNotificationPresentationOptionAlert);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler
{
        //Called to let your app know which action was selected by the user for a given notification.
        
        NSLog(@"Userinfo in receive %@",response.notification.request.content.userInfo);
        [self handleNotification:response.notification.request.content.userInfo ];
}

-(void)setNotificationCategory
{
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        UNNotificationAction *confirmAction = [UNNotificationAction actionWithIdentifier:CONFIRM
                                                                                   title:@"Confirm"
                                                                                 options:UNNotificationActionOptionForeground];
        UNNotificationAction *notInterstedAction = [UNNotificationAction actionWithIdentifier:NOT_INTERSTED
                                                                                        title:@"Not Intersted"
                                                                                      options:UNNotificationActionOptionDestructive];
        NSArray *notificationActions = @[confirmAction, notInterstedAction ];
        
        // create a category
        UNNotificationCategory *notificationCategory = [UNNotificationCategory categoryWithIdentifier:@"sygNotificationCategory" actions:notificationActions intentIdentifiers:@[] options:UNNotificationCategoryOptionNone];
        NSSet *categories = [NSSet setWithObject:notificationCategory];
        
        // registration
        [center setNotificationCategories:categories];
}

#pragma mark - handle notification
-(void)handleNotification:(NSDictionary *)userInfo
{
        NSLog(@"user info %@",userInfo);
        UIApplicationState applicationState = [[UIApplication sharedApplication] applicationState];
        
        if (applicationState == UIApplicationStateInactive )
        {
                //                UIStoryboard *storyBoard;
                //                storyBoard= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                //                HomeViewController *objHomeVC;
                //                if([[self.navigationController visibleViewController] isKindOfClass:[HomeViewController class]])
                //                {
                //                        objHomeVC = (HomeViewController *)[self.navigationController visibleViewController];
                //                        objHomeVC.notificationDict=userInfo;
                //                }
                //                else
                //                {
                //                        UINavigationController *selectedItemnavController=[self.tabBarObj.viewControllers objectAtIndex:0];
                //                        HomeViewController *objHomeVC= [[selectedItemnavController viewControllers] objectAtIndex:0];
                //                        objHomeVC.notificationDict=userInfo;
                //                        [self tabBarObj].selectedIndex=0;
                //                        [[self tabBarObj] TabClickedIndex:0];
                //                }
                //                [objHomeVC didbecomeActiveOnNotification];
                
                [self handlePushNotificationResponse:userInfo];
        }
        //        else if (applicationState == UIApplicationStateBackground)
        //        {
        //
        //                [self takeActionWhenUserTapOnNotification:userInfo];
        //        }
        else if(applicationState==UIApplicationStateActive)
        {
                [self handlePushNotificationResponse:userInfo];
        }
}

-(void)handlePushNotificationResponse:(NSDictionary *)notificationDict
{
        if (notificationDict!=nil)
        {
                UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UINavigationController *nav = (UINavigationController *)[self.tabBarObj selectedViewController];
                if([notificationDict objectForKey:@"gcm.notification.url_type"])
                {
                        if([[notificationDict objectForKey:@"gcm.notification.url_type"]isEqualToString:@"product"])
                        {
                                ProductDetailsViewController *objProductDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
                                int productId=[[notificationDict valueForKey:@"gcm.notification.product_id"]intValue];
                                objProductDetailVC.productID=productId;
                                [nav pushViewController:objProductDetailVC animated:YES];
                        }
                        else  if([[notificationDict objectForKey:@"gcm.notification.url_type"]isEqualToString:@"category"])
                        {
                                ElectronicsViewController *electronicviewObjc = [storyBoard instantiateViewControllerWithIdentifier:@"ElectronicsViewController"];
                                NSString *cat_id = [NSString stringWithFormat:@"%d",[[notificationDict valueForKey:@"gcm.notification.category_id"]intValue]];
                                electronicviewObjc.category_id=cat_id;
                                electronicviewObjc.categoryVal= [notificationDict valueForKey:@"gcm.notification.category_name"];
                                [nav pushViewController:electronicviewObjc animated:YES];
                        }
                }
                else if ([[notificationDict valueForKey:@"gcm.notification.type"]isEqualToString:@"app_update"])
                {
                        UIAlertView *objAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Do you want to update app?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No", nil];
                        objAlert.tag = 500;
                        [objAlert show];
                }
                else if ([[notificationDict valueForKey:@"gcm.notification.type"]isEqualToString:@"syg_link"])
                {
                        UIAlertView *objAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Do you want to update app?" delegate:self cancelButtonTitle:nil otherButtonTitles:CONFIRM,NOT_INTERSTED, nil];
                        objAlert.tag = 600;
                        [objAlert show];
                }
                else if ([[notificationDict valueForKey:@"gcm.notification.type"]isEqualToString:@"link"])
                {
                        NSString *string = [notificationDict valueForKey:@"gcm.notification.link_url"] ;
                        if ([string rangeOfString:@"deals"].location != NSNotFound)
                        {
                                [self.tabBarObj TabClickedIndex:3];
                        }
                        else  if ([string rangeOfString:@"splash"].location != NSNotFound)
                        {
                                SplashScreenViewController *vc = [[SplashScreenViewController alloc] initWithNibName:@"SplashScreenViewController" bundle:nil];
                                [[self tabBarObj] hideTabbar];
                                [nav pushViewController:vc animated:YES];
                        }
                        else
                        {
                                NSLog(@"string does not contain deals");
                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[notificationDict valueForKey:@"gcm.notification.link_url"]]];
                        }
                }
        }
}

#pragma mark - Color HexToString
-(UIColor*)colorWithHexString:(NSString*)hex
{
        NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
        
        // String should be 6 or 8 characters
        if ([cString length] < 6) return [UIColor grayColor];
        
        // strip 0X if it appears
        if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
        
        if ([cString length] != 6) return  [UIColor grayColor];
        
        // Separate into r, g, b substrings
        NSRange range;
        range.location = 0;
        range.length = 2;
        NSString *rString = [cString substringWithRange:range];
        
        range.location = 2;
        NSString *gString = [cString substringWithRange:range];
        
        range.location = 4;
        NSString *bString = [cString substringWithRange:range];
        
        // Scan values
        unsigned int r, g, b;
        [[NSScanner scannerWithString:rString] scanHexInt:&r];
        [[NSScanner scannerWithString:gString] scanHexInt:&g];
        [[NSScanner scannerWithString:bString] scanHexInt:&b];
        
        return [UIColor colorWithRed:((float) r / 255.0f)
                               green:((float) g / 255.0f)
                                blue:((float) b / 255.0f)
                               alpha:1.0f];
}

#pragma mark - Progress Hud
-(void)StartProgressHud
{
        if ([FVCustomAlertView currentView])
        {
                [[FVCustomAlertView currentView]removeFromSuperview];
        }
        
        [FVCustomAlertView showDefaultLoadingAlertOnView:self.window withTitle:@"Loading..." withBlur:YES allowTap:YES];
}

-(void)StopProgressHud
{
        if ([FVCustomAlertView currentView])
        {
                [[FVCustomAlertView currentView]removeFromSuperview];
        }
        [FVCustomAlertView showDefaultDoneAlertOnView:self.window withTitle:@"Done" withBlur:YES allowTap:YES];
}

#pragma mark -
-(BOOL)checkInternetConnectivity
{
        Reachability *reach = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [reach currentReachabilityStatus];
        if(networkStatus == NotReachable)
        {
                return FALSE;
        }
        else
        {
                return TRUE;
        }
}

/*
 // This method sends hits in the background until either we're told to stop background processing,
 // we run into an error, or we run out of hits.  We use this to send any pending Google Analytics
 // data since the app won't get a chance once it's in the background.
 - (void)sendHitsInBackground {
 self.okToWait = YES;
 __weak AppDelegate *weakSelf = self;
 __block UIBackgroundTaskIdentifier backgroundTaskId =
 [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
 weakSelf.okToWait = NO;
 }];
 
 if (backgroundTaskId == UIBackgroundTaskInvalid) {
 return;
 }
 
 self.dispatchHandler = ^(GAIDispatchResult result) {
 // If the last dispatch succeeded, and we're still OK to stay in the background then kick off
 // again.
 if (result == kGAIDispatchGood && weakSelf.okToWait ) {
 [[GAI sharedInstance] dispatchWithCompletionHandler:weakSelf.dispatchHandler];
 } else {
 [[UIApplication sharedApplication] endBackgroundTask:backgroundTaskId];
 }
 };
 [[GAI sharedInstance] dispatchWithCompletionHandler:self.dispatchHandler];
 }
 */

#pragma mark - AlertView
-(void) showAlert :(NSString *) title withMessage :(NSString *) message
{
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert1 show];
}

#pragma mark -Custom Navigation Bar
+(void)setCustomNavigation:(UIViewController *)controller
{
        controller.navigationController.navigationBar.hidden = NO;
        controller.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:17.0/255.0 green:127.0/255.0 blue:206.0/255.0 alpha:1.0];
        //controller.navigationController.navigationBar.titleTextAttributes =@{NSForegroundColorAttributeName:[UIColor whiteColor]};
        controller.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
        [controller.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                            forBarMetrics:UIBarMetricsDefault];
        controller.navigationController.navigationBar.translucent=NO;
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 0, 20, 20);
        [backButton addTarget:controller action:@selector(backButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        backButton.showsTouchWhenHighlighted = YES;
        
        UIImage *backButtonImage = [UIImage imageNamed:@"leftSlidemenuimg"];
        [backButton setImage:backButtonImage forState:UIControlStateNormal];
        
        // backButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        UIButton *backButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton2.frame = CGRectMake(20, 0, 100, 33);
        [backButton2 setUserInteractionEnabled:NO];
        // [backButton2 addTarget:controller action:@selector(backButton2Tapped) forControlEvents:UIControlEventTouchUpInside];
        // backButton2.showsTouchWhenHighlighted = YES;
        
        UIImage *backButtonImage2 = [UIImage imageNamed:@"Login_logo"];
        [backButton2 setImage:backButtonImage2 forState:UIControlStateNormal];
        //  backButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
        
        UIBarButtonItem *backBarButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:backButton2];
        //Added Right button
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightButton.frame = CGRectMake(0, 0, 30, 30);
        //[rightButton addTarget:controller action:@selector(cartButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        rightButton.showsTouchWhenHighlighted = YES;
        [rightButton setImage:[UIImage imageNamed:@"cart_white_icon"] forState:UIControlStateNormal];
        
        // backButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
        
        UIBarButtonItem *cartButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        controller.navigationItem.leftBarButtonItems = @[backBarButtonItem,backBarButtonItem2];
        controller.navigationItem.rightBarButtonItem=cartButtonItem;
        
}

+(void)setCustomNavigationWithBackButton:(UIViewController *)controller
{
        controller.navigationController.navigationBar.hidden = NO;
        controller.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:17.0/255.0 green:127.0/255.0 blue:206.0/255.0 alpha:1.0];
        controller.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
        [controller.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                                            forBarMetrics:UIBarMetricsDefault];
        controller.navigationController.navigationBar.translucent=NO;
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 0, 18, 18);
        [backButton addTarget:controller action:@selector(backButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        backButton.showsTouchWhenHighlighted = YES;
        
        UIImage *backButtonImage = [UIImage imageNamed:@"ios_back_btn"];
        [backButton setImage:backButtonImage forState:UIControlStateNormal];
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightButton.frame = CGRectMake(0, 0, 30, 30);
        [rightButton addTarget:controller action:@selector(cartButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        rightButton.showsTouchWhenHighlighted = YES;
        [rightButton setImage:[UIImage imageNamed:@"cart_white_icon"] forState:UIControlStateNormal];
        UIBarButtonItem *cartButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        controller.navigationItem.rightBarButtonItem=cartButtonItem;
        controller.navigationItem.leftBarButtonItems = @[backBarButtonItem];
        [controller.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Karla-Bold" size:17.0]}];
}

#pragma mark - setFeedBackTimer
-(void)setFeedBackTimer
{
        feedbackTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(displayFeedBackview) userInfo:nil repeats:NO];
}

-(void)stopFeedBackTimer
{
        [feedbackTimer invalidate];
}

-(void)displayFeedBackview
{
        NSLog(@"displayFeedBackview called");
        FeedbackViewController *objFeedbackViewVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FeedbackViewController"];
        
        UINavigationController *nav = (UINavigationController *)_tabBarObj.selectedViewController;
        
        UIViewController *objVisibleVC = nav.visibleViewController;
        [objVisibleVC addChildViewController:objFeedbackViewVC];
        objFeedbackViewVC.view.frame = objVisibleVC.view.frame;
        [objVisibleVC.view addSubview:objFeedbackViewVC.view];
        objFeedbackViewVC.view.alpha = 0;
        [objFeedbackViewVC didMoveToParentViewController:objVisibleVC];
        
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                objFeedbackViewVC.view.alpha = 1;
        } completion:nil];
}

-(NSInteger) getEBCount
{
        return self.ebPinGenerateCount;
}

-(void) decrementEBCount
{
        self.ebPinGenerateCount = self.ebPinGenerateCount - 1;
}
-(void) setEBPinGenerateCount
{
        self.ebPinGenerateCount = 3;
}
@end
