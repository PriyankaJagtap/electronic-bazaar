//
//  NotificationViewController.m
//  iOS10PushNotificationUI
//
//  Created by webwerks on 21/02/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import "NotificationViewController.h"
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import "UIImageView+WebCache.h"
#import "Constant.h"
//#import "AppDelegate.h"
//#import "SYGOrderConfirmationViewController.h"
//#import "SellYourGadgetNavigationViewController.h"

@interface NotificationViewController () <UNNotificationContentExtension>

@property IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any required interface initialization here.
}

- (void)didReceiveNotification:(UNNotification *)notification {
    //self.label.text = notification.request.content.body;
        self.label.text = @" ";
        //NSLog(@"userInfo value %@",notification.request.content.userInfo);
        NSDictionary *userInfoDic = notification.request.content.userInfo;
        if([userInfoDic valueForKey:@"gcm.notification.image"])
        {
                [self.imageView sd_setImageWithURL:[NSURL URLWithString:userInfoDic[@"gcm.notification.image"]]];
        }

}


//- (void)didReceiveNotificationResponse:(UNNotificationResponse *)response completionHandler:(void (^)(UNNotificationContentExtensionResponseOption option))completion
//{
////       if([response.actionIdentifier isEqualToString:CONFIRM])
////       {
////               NSLog(@"confirm btn clciked");
////               UINavigationController *nav =(UINavigationController *)[[AppDelegate getAppDelegateObj].tabBarObj selectedViewController];
////               UIStoryboard *storyBoard;
////               storyBoard= [UIStoryboard storyboardWithName:@"Product" bundle:nil];
////               SYGOrderConfirmationViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"SYGOrderConfirmationViewController"];
////               [nav pushViewController:vc animated:YES];
////       }
////        else if ([response.actionIdentifier isEqualToString:NOT_INTERSTED])
////        {
////                NSLog(@"not intersted btn clciked");
////        }
////
//
//
//
//}
@end
