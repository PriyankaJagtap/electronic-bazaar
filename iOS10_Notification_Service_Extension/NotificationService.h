//
//  NotificationService.h
//  iOS10_Notification_Service_Extension
//
//  Created by webwerks on 21/02/18.
//  Copyright © 2018 webwerks. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
